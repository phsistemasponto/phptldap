package acessoLdap


import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.userdetails.GrailsUserDetailsService
import grails.transaction.Transactional

import java.util.logging.Logger

import org.springframework.dao.DataAccessException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.ldap.userdetails.LdapUserDetailsService

import acesso.Usuario

class PrePopulateUserDetailsService implements GrailsUserDetailsService  {
     
   // Logger logger = Logger.getLogger(getClass())
    LdapUserDetailsService ldapUserDetailsService
     
    static final List NO_ROLES = [new SimpleGrantedAuthority(SpringSecurityUtils.NO_ROLE)]
     
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        return loadUserByUsername(username, true)
    }
     
    @Override
    @Transactional  // Services in Grails are transactional by default but it is normal class so we put this annotation
    public UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException, DataAccessException {
                 
        Usuario user = Usuario.findByUsername(username)
        def authorities = user.getAuthorities().collect {new SimpleGrantedAuthority(it.authority)}
         
        return new LdapUserDetails(user.username, user.password, user.enabled,
            !user.accountExpired, !user.passwordExpired,
            !user.accountLocked, authorities ?: NO_ROLES, user.id,
            user.username)   
    }
}
