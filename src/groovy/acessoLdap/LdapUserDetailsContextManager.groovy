package acessoLdap



import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional

import org.springframework.context.annotation.Role
import org.springframework.ldap.core.DirContextAdapter
import org.springframework.ldap.core.DirContextOperations
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper

import acesso.Usuario
import acesso.UsuarioPerfil

class LdapUserDetailsContextMapper implements UserDetailsContextMapper {
	
	   private static final List NO_ROLES = [new SimpleGrantedAuthority(SpringSecurityUtils.NO_ROLE)]
			
	   @Override
	   @Transactional
	   public LdapUserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<SimpleGrantedAuthority> authority) {
			
		   Usuario user = Usuario.findByUsername(username)
			
		   if(!user) {
				   // Create new user and save to the database
				   user = new User()
				   user.username = username
				   user.password = "123" // doesn't matter
				   user.save(flush: true, failOnError: true)
										
				   UsuarioPerfil userRole = new UsuarioPerfil()
				   userRole.user = user
				   userRole.role = Role.findByAuthority('USER') // Default role in my app
				   userRole.save(flush: true, failOnError: true)
		   }
							 
		   def authorities = user.getAuthorities().collect { new SimpleGrantedAuthority(it.authority) }
		   def userDetails = new LdapUserDetails(username, user.password, user.enabled, false,
					   false, false, authorities, user.id, username)
		   return userDetails
	   }
	
		
	   @Override
	   public void mapUserToContext(UserDetails arg0, DirContextAdapter arg1) {}
}
