
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Mapeamentos</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Mapeamento']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Url</th>
		
          <th>Config Attribute</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${mapeamentoInstanceList}" status="i" var="mapeamentoInstance">
        <tr>
		
          <td>${fieldValue(bean: mapeamentoInstance, field: "url")}</td>
		
          <td>${fieldValue(bean: mapeamentoInstance, field: "configAttribute")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${mapeamentoInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/mapeamento/delete', ${mapeamentoInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${mapeamentoInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${mapeamentoInstanceTotal}</span>
    </div>
  </body>
</html>