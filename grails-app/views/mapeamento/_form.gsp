<g:javascript src="mapeamento/mapeamento.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${mapeamentoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${mapeamentoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${mapeamentoInstance.id}">
		<form action="${request.contextPath}/mapeamento/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${mapeamentoInstance?.id}" />
			<g:hiddenField name="version" value="${mapeamentoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/mapeamento/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Url:</label>
				<div class="controls">
					<g:textField name="url" required="" value="${mapeamentoInstance?.url}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Config Attribute:</label>
				<div class="controls">
					<g:textField name="configAttribute" required="" value="${mapeamentoInstance?.configAttribute}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>