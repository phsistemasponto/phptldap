	<g:javascript src="periodo/periodo.js"/>
	<g:javascript src="jquery.meiomask.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${periodoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${periodoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${periodoInstance.id}">
		<form action="${request.contextPath}/periodo/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${periodoInstance?.id}" />
			<g:hiddenField name="version" value="${periodoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/periodo/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscPr" maxlength="80" required="" value="${periodoInstance?.dscPr}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Apelido:</label>
				<div class="controls">
					<g:textField name="apelPr" maxlength="40" value="${periodoInstance?.apelPr}"/>
					
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Data inicio:</label>
				<div class="controls">
					<g:textField name="dtIniPr"  value="${periodoInstance?.dtIniPrFormatada}"  />
					
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Data Fim:</label>
				<div class="controls">
					<g:textField name="dtFimPr" value="${periodoInstance?.dtFimPrFormatada}"  />
				</div>
			</div>



			<div class="row-fluid">	
		       <div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Periodo Fechado?</label>
						<div class="controls">							
							<g:checkBox name="idHabilitada" value="${periodoInstance?.idStsPr}" checked="${periodoInstance?.idStsPr == 'F'}" /></td>
						</div>
					</div>             
				</div>		
				
				
			</div>		
			
		</fieldset>
		
		<hr>
		<h4>Filiais</h4>
		<br />
		<div>
			<g:if test="${periodoInstance.id}">
				<table class="table table-striped table-bordered table-condensed"
					id="filiaisTable">
					<thead>
						<tr>
							<th style="width: 50%;">Filial</th>
							<th style="width: 15%;">Possui Período</th>
							<th style="width: 15%;">Status</th>
							<th style="width: 20%;"></th>
						</tr>
					</thead>
	
					<tbody>
						<g:each in="${filiais?}" status="i" var="item">
							<tr id="rowTableFilial${i}" class="even">
								<td><g:hiddenField name="filialId" value="${item?.filial?.id}" />
									<g:hiddenField name="temFilialPeriodo" value="${item?.temFilial}" />
									<g:if test="${item?.temFilial=='true' }">
										<g:hiddenField name="statusPeriodo" value="${item?.statusPeriodo}" />
									</g:if>
									<g:else>
										<g:hiddenField name="statusPeriodo" value="X" />
									</g:else>
									<g:textField name="descricaoFilial" style="width:90%" value='${item?.filial?.dscFil+" - "+item?.filial?.unicodFilial}' readOnly="true" />
								</td>
								
								<td>
									<g:if test="${item?.temFilial=='true'}">
										<g:checkBox name="temFilial${i}" value="${true}" onChange="atualizaFilialBotao(${i});" disabled="disabled"/>
									</g:if>
									<g:else>  
										<g:checkBox name="temFilial${i}" value="${false}"  onChange="atualizaFilialBotao(${i});"/>
									</g:else>										
								</td>
								<td>
									<g:if test="${item?.temFilial=='true' }">
										<g:if test="${item?.statusPeriodo == 'A' }">
											<a class="btn btn-mini btn-warning" onClick="fechaReabrePeriodo(${i});" id="botao${i}">Aberto</a>
										</g:if>
										<g:else>
									    	<a class="btn btn-mini btn-success" onClick="fechaReabrePeriodo(${i});" id="botao${i}">Fechado</a>
									    </g:else>	
									</g:if>
									<g:else>
										<a class="btn btn-disabled btn-mini" onClick="fechaReabrePeriodo(${i});" id="botao${i}">Aberto</a>
									</g:else>
								</td>
								<td/>
							</tr>
						</g:each>
					</tbody>
				</table>
			
			</g:if>
			<g:else>
				<table class="table table-striped table-bordered table-condensed"
					id="filiaisTable">
					<thead>
						<tr>
							<th style="width: 50%;">Filial</th>
							<th style="width: 50%;">&nbsp; <button class="btn" type="button" onClick="marcarTodos();">Marcar Todos</button> <button class="btn" type="button" onClick="desmarcarTodos();">Desmarcar Todos</button></th>
						</tr>
					</thead>
	
					<tbody>
						<g:each in="${filiais?}" status="i" var="item">
							<tr id="rowTableFilial${i}" class="even">
								<td><g:hiddenField name="filialId" value="${item?.filial?.id}" />
									<g:hiddenField name="temFilialPeriodo" value="${item?.temFilial}" />
									<g:textField name="descricaoFilial" style="width:90%" value='${item?.filial?.dscFil+" - "+item?.filial?.unicodFilial}' readOnly="true" />
								</td>
								<td><g:checkBox name="temFilial${i}" value="${item?.temFilial}" onChange="atualizaFilial(${i});"/></td>
								<td/>
							</tr>
						</g:each>
					</tbody>
				</table>
			</g:else>
		</div>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>