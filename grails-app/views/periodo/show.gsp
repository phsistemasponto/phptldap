
<%@ page import="cadastros.Periodo" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'periodos.label', default: 'Periodos')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-periodos" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-periodos" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list periodos">
			
				<g:if test="${periodosInstance?.dscPr}">
				<li class="fieldcontain">
					<span id="dscPr-label" class="property-label"><g:message code="periodos.dscPr.label" default="Dsc Pr" /></span>
					
						<span class="property-value" aria-labelledby="dscPr-label"><g:fieldValue bean="${periodosInstance}" field="dscPr"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${periodosInstance?.apelPr}">
				<li class="fieldcontain">
					<span id="apelPr-label" class="property-label"><g:message code="periodos.apelPr.label" default="Apel Pr" /></span>
					
						<span class="property-value" aria-labelledby="apelPr-label"><g:fieldValue bean="${periodosInstance}" field="apelPr"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${periodosInstance?.dtIniPr}">
				<li class="fieldcontain">
					<span id="dtIniPr-label" class="property-label"><g:message code="periodos.dtIniPr.label" default="Dt Ini Pr" /></span>
					
						<span class="property-value" aria-labelledby="dtIniPr-label"><g:formatDate date="${periodosInstance?.dtIniPr}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${periodosInstance?.dtFimPr}">
				<li class="fieldcontain">
					<span id="dtFimPr-label" class="property-label"><g:message code="periodos.dtFimPr.label" default="Dt Fim Pr" /></span>
					
						<span class="property-value" aria-labelledby="dtFimPr-label"><g:formatDate date="${periodosInstance?.dtFimPr}" /></span>
					
				</li>
				</g:if>
			
			
				<g:if test="${periodosInstance?.filial}">
				<li class="fieldcontain">
					<span id="filial-label" class="property-label"><g:message code="periodos.filial.label" default="Filial" /></span>
					
						<span class="property-value" aria-labelledby="filial-label"><g:link controller="filial" action="show" id="${periodosInstance?.filial?.id}">${periodosInstance?.filial?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${periodosInstance?.idStsBh}">
				<li class="fieldcontain">
					<span id="idStsBh-label" class="property-label"><g:message code="periodos.idStsBh.label" default="Id Sts Bh" /></span>
					
						<span class="property-value" aria-labelledby="idStsBh-label"><g:fieldValue bean="${periodosInstance}" field="idStsBh"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${periodosInstance?.idStsPr}">
				<li class="fieldcontain">
					<span id="idStsPr-label" class="property-label"><g:message code="periodos.idStsPr.label" default="Id Sts Pr" /></span>
					
						<span class="property-value" aria-labelledby="idStsPr-label"><g:fieldValue bean="${periodosInstance}" field="idStsPr"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${periodosInstance?.id}" />
					<g:link class="edit" action="edit" id="${periodosInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
