
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
  	<g:javascript src="periodo/periodo.js"/>
  	
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Periodo</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Periodo']" /></g:link>
    </div>
    
    <form action="${request.contextPath}/periodo/list" method="POST" id="formPesquisa">
    	<div class="well">
  			<h3>Filtro</h3>
 			<div class="control-group">
				<label class="control-label">Tipo de filtro:</label>				
				<div class="controls">
					<g:select name="tipoFiltro" optionKey="id" optionValue="nome" 
							value="${params.tipoFiltro}"
							style="width:100px;"
							onchange="alteraFiltro()"
							from="${[ [id:'0', nome:'...'],
									  [id:'1', nome:'Descrição'], 
									  [id:'2', nome:'Data'] ]}"/>
					<g:textField name="filtro" maxlength="250" value="${params.filtro}" style="margin-left: 10px; width: 400px; display: none;"/>
					<g:textField name="filtroData" maxlength="10" value="${params.filtroData}" style="margin-left: 10px; width: 100px; display: none;"/>
					<input type="hidden" id="order" name="order"/>
					<input type="hidden" id="sortType" name="sortType"/>
					<input type="hidden" id="max" name="max" value="${params.max}"/>
					<input type="hidden" id="offset" name="offset" value="${params.offset}"/>
					<g:submitButton name="filtrar" class="btn btn-medium btn-primary" value="Filtrar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>
           
    <hr/>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>
          	<span>Período</span>
          	<g:if test="${order != 'dscPr'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('dscPr', 'asc')"/></g:if>
          	<g:if test="${order == 'dscPr' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('dscPr', 'desc')"/></g:if>
          	<g:if test="${order == 'dscPr' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('dscPr', 'asc')"/></g:if>
          </th>
          <th>
          	<span>Data Início</span>
          	<g:if test="${order != 'dtIniPr'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('dtIniPr', 'asc')"/></g:if>
          	<g:if test="${order == 'dtIniPr' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('dtIniPr', 'desc')"/></g:if>
          	<g:if test="${order == 'dtIniPr' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('dtIniPr', 'asc')"/></g:if>
          </th>
          <th>
          	<span>Data Fim</span>
          	<g:if test="${order != 'dtFimPr'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('dtFimPr', 'asc')"/></g:if>
          	<g:if test="${order == 'dtFimPr' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('dtFimPr', 'desc')"/></g:if>
          	<g:if test="${order == 'dtFimPr' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('dtFimPr', 'asc')"/></g:if>
          </th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${periodoInstanceList}" status="i" var="periodoInstance">
        <tr>
          <td>${fieldValue(bean: periodoInstance, field: "dscPr")}</td>
	      <td>${fieldValue(bean: periodoInstance, field: "dtIniPrFormatada")}</td>	
	      <td>${fieldValue(bean: periodoInstance, field: "dtFimPrFormatada")}</td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${periodoInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/periodo/delete', ${periodoInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${periodoInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${periodoInstanceTotal}</span>
    </div>
  </body>
</html>