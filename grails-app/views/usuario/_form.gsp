

	<g:if test="${request.message}">
		<div class="alert alert-info">${request.message}</div>
	</g:if>
	
	<g:if test="${request.error}">
    	<div class="alert alert-error" role="status">${request.error}</div>
    </g:if>

	<g:hasErrors bean="${usuarioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${usuarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${usuarioInstance.id}">
		<form action="${request.contextPath}/usuario/update" class="well form-horizontal" method="POST" id="formFunc">
			<g:hiddenField name="id" value="${usuarioInstance?.id}" />
			<g:hiddenField name="version" value="${usuarioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/usuario/save" class="well form-horizontal" method="POST" id="formFunc">
	</g:else>	
			
		<g:hiddenField name="sequenciaFilial" value="${sequenciaFilial}" />
		<g:hiddenField name="sequenciaSetor" value="${sequenciaSetor}" />
		<g:hiddenField name="sequenciaFuncionario" value="${sequenciaFuncionario}" />
		<g:hiddenField name="sequenciaGrupo" value="${sequenciaGrupo}" />	
		<fieldset>
			
			<div class="control-group">
				<label class="control-label">Login:</label>
				<div class="controls">
					<g:textField name="username" required="" value="${usuarioInstance?.username}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Nome do Usuário:</label>
				<div class="controls">
					<g:textField name="nome" required="" value="${usuarioInstance?.nome}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Email:</label>
				<div class="controls">
					<g:textField name="email" required="" value="${usuarioInstance?.email}"/>
				</div>
			</div>
			
			<g:if test="${!usuarioInstance.id}">
				<div class="control-group">
					<label class="control-label">Senha:</label>
					<div class="controls">
						<g:passwordField name="password" required="" value="${usuarioInstance?.password}"/>
					</div>
				</div>
			</g:if>
			
			<div class="control-group">
				<label class="control-label">Acesso:</label>
				<div class="controls">
					<g:select id="acesso" name="acesso" from="${acesso.GrupoAcesso.listOrderByDescricao()}" optionKey="id" value="${usuarioInstance?.acesso?.id}" 
						style="width:220px;" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Funcionário:</label>
				<div class="controls">
					<g:textField name="funcionarioId" maxlength="5" style="width:50px" onBlur="buscaIdFuncionario();" 
						value="${usuarioInstance?.funcionario?.crcFunc}"/>&nbsp; 
					<i class="icon-search" onclick="buscarLovFuncionario();" style="cursor: pointer" title="Procurar Funcionário" id="funcionarioBotaoBusca"></i> &nbsp;
					<g:textField name="funcionarioNome" maxlength="450" readOnly="true" style="width:450px" 
						value="${usuarioInstance?.funcionario?.nomFunc}"/>
				</div>
			</div>
			
			<table id="checkBoxes" class="table">
				<th>
		          <th style="width:8%;"></th>
		          <th style="width:12%;"></th>
		          <th style="width:8%;"></th>
		          <th style="width:12%;"></th>
		          <th style="width:60%;"></th>
				</th>


				<tr>
					<td>Conta Expirada:</td>
					<td>
						<g:checkBox name="accountExpired" value="${usuarioInstance?.accountExpired}" />
					</td>
					<td>Conta Bloqueada:</td> 
					<td><g:checkBox name="accountLocked" value="${usuarioInstance?.accountLocked}" /></td>
				</tr>
				
				<tr>
					<td>Usuário Ativo:</td>
					<td><g:checkBox name="enabled" value="${usuarioInstance?.enabled}" /></td>
					<td>Senha expirada:</td>
					<td><g:checkBox name="passwordExpired" value="${usuarioInstance?.passwordExpired}" /></td>
				</tr>
				
			</table>
		</fieldset>
		
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab1" data-toggle="tab">Filiais</a></li>
				<li><a href="#tab2" data-toggle="tab">Setores</a></li>
				<li><a href="#tab3" data-toggle="tab" style="display:none;">Funcionários</a></li>
				<li><a href="#tab4" data-toggle="tab">Grupos</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">		
				
					<div class="control-group">
						<label class="control-label">Filial:</label>
						<div class="controls">
							<g:textField name="filialId" maxlength="5" style="width:50px" onBlur="buscaIdFilial();"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarMultiLovFilialSemFiltrarPorUsuario();"
								style="cursor: pointer" title="Procurar Filial" id="filialBotaoBusca"></i> 
							&nbsp;
							<g:textField name="filialNome" maxlength="450" readOnly="true" style="width:450px" />
							<a class="btn btn-medium" onClick="incluirFilial();">Incluir</a>
						</div>
					</div>
							
					<table class="table table-striped table-bordered table-condensed" id="filialTable">
						<thead>
							<tr>
								<th style="width: 15%;">Código</th>
								<th style="width: 75%;">Filial</th>
								<th style="width: 10%;">&nbsp; </th>
							</tr>
						</thead>
		
						<tbody>
							<g:each in="${filiais?}" status="i" var="item">
								<tr id="rowTableFilial${i}" class="even">
									<td>
										<g:hiddenField name="filialIdTable" value="${item?.filial?.id}" />
										<g:textField name="filialIdDisplay" style="width:90%" value="${item?.filial?.id}" readOnly="true" />
									</td>
									<td>
										<g:textField name="filialNomeTable" style="width:90%" value="${item?.filial?.dscFil}" readOnly="true" />										
									</td>
									<td>
										<i class="icon-remove" onclick="excluirFilial(${i});" style="cursor: pointer" title="Excluir registro"></i>
									</td>
								</tr>
							</g:each>
						</tbody>
					</table>									
				</div>
				<div class="tab-pane" id="tab2">

					<div class="control-group">
						<label class="control-label">Setor:</label>
						<div class="controls">
							<g:textField name="setorId" maxlength="5" style="width:50px" onBlur="buscaIdSetor();"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovSetor();"
								style="cursor: pointer" title="Procurar Setor" id="setorBotaoBusca"></i> 
							&nbsp;
							<g:textField name="setorNome" maxlength="450" readOnly="true" style="width:450px" />
							<a class="btn btn-medium" onClick="incluirSetor();">Incluir</a>
						</div>
					</div>
							
					<table class="table table-striped table-bordered table-condensed" id="setorTable">
						<thead>
							<tr>
								<th style="width: 15%;">Código</th>
								<th style="width: 75%;">Setor</th>
								<th style="width: 10%;">&nbsp; </th>
							</tr>
						</thead>
		
						<tbody>
							<g:each in="${setores?}" status="i" var="item">
								<tr id="rowTableSetor${i}" class="even">
									<td>
										<g:hiddenField name="setorIdTable" value="${item?.setor?.id}" />
										<g:textField name="setorIdDisplay" style="width:90%" value="${item?.setor?.id}" readOnly="true" />
									</td>
									<td>
										<g:textField name="setorNomeTable" style="width:90%" value="${item?.setor?.dscSetor}" readOnly="true" />										
									</td>
									<td>
										<i class="icon-remove" onclick="excluirSetor(${i});" style="cursor: pointer" title="Excluir registro"></i>
									</td>
								</tr>
							</g:each>
						</tbody>
					</table>	
				
				</div>
				<div class="tab-pane" id="tab3">	
				
					<div id="formPesquisa">
				    	<div class="well">
				 			<div class="control-group">
								<label class="control-label">Filtro:</label>
								<div class="controls">
									<input type="hidden" name="filtroId" id="filtroId">
									<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
									<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
									<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
									<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
									<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
									<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
									<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
									<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
									
									<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
									<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
									<button type="button" name="btnFiltro" class="btn btn-medium btn-primary" style="margin-left: 10px;" 
										onclick="consultaFiltroPadraoAjax()">OK</button>
								</div>
							</div>
						</div>
					</div>
					
					<div id="funcionarios">
						<table class="table table-striped table-bordered table-condensed" id="funcionarioTable">
							<thead>
								<tr>
										<th style="width: 20%;">Matrícula</th>
										<th style="width: 70%;">Funcionário</th>
										<th style="width: 10%;"></th>
								</tr>
							</thead>
							<tbody>
									<g:each in="${funcionarios?}" status="i" var="item">
										<tr id="rowTableFuncionario" class="even">
											<td>
												<g:hiddenField name="funcionarioIdTable" value="${item?.funcionario?.id}" />
												<g:textField name="funcionarioMatriculaTable" style="width:90%" value="${item?.funcionario?.mtrFunc}" readOnly="true"/></td>
											<td>
												<g:textField name="funcionarioNomeTable" style="width:90%" value="${item?.funcionario?.nomFunc}" readOnly="true" />
											</td>
											<td>
												<i class="icon-remove" onclick="excluirFuncionario(this);" style="cursor: pointer" title="Excluir registro"></i>
											</td>
										</tr>
									</g:each>
							</tbody>
						</table>
					</div>			
				</div>
				<div class="tab-pane" id="tab4">

					<div class="control-group">
						<label class="control-label">Grupo:</label>
						<div class="controls">
							<g:textField name="grupoId" maxlength="5" style="width:50px" 
								onBlur="buscaId('cadastros.Grupo', 'id', 'dscGrupo', 'grupoId', 'grupoDescricao', false);"/>
							&nbsp; 
							<i class="icon-search" onclick="showBuscaInterna('grupoId', 'grupoDescricao', 'cadastros.Grupo', 'id', 'dscGrupo', false);"
								style="cursor: pointer" title="Procurar Grupo" id="grupoBotaoBusca"></i> 
							&nbsp;
							<g:textField name="grupoDescricao" maxlength="450" readOnly="true" style="width:450px" />
							<a class="btn btn-medium" onClick="incluirGrupo();">Incluir</a>
						</div>
					</div>
							
					<table class="table table-striped table-bordered table-condensed" id="grupoTable">
						<thead>
							<tr>
								<th style="width: 15%;">Código</th>
								<th style="width: 75%;">Grupo</th>
								<th style="width: 10%;">&nbsp; </th>
							</tr>
						</thead>
		
						<tbody>
							<g:each in="${grupos?}" status="i" var="item">
								<tr id="rowTableGrupo${i}" class="even">
									<td>
										<g:hiddenField name="grupoIdTable" value="${item?.grupoFuncionario?.id}" />
										<g:textField name="grupoIdDisplay" style="width:90%" value="${item?.grupoFuncionario?.id}" readOnly="true" />
									</td>
									<td>
										<g:textField name="grupoNomeTable" style="width:90%" value="${item?.grupoFuncionario?.dscGrupo}" readOnly="true" />										
									</td>
									<td>
										<i class="icon-remove" onclick="excluirGrupo(${i});" style="cursor: pointer" title="Excluir registro"></i>
									</td>
								</tr>
							</g:each>
						</tbody>
					</table>	
				
				</div>
			</div>
		</div>		
		<button type="button" name="salvar" class="btn btn-large btn-primary" style="margin-left: 10px;" onclick="document.getElementById('formFunc').submit();">
			Salvar
		</button>
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>
	
	<script>
		$j('#setorId').attr('disabled', false);
	</script>
	
	<g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>