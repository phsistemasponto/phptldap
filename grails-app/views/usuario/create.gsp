<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
  	<g:javascript src="jquery.meiomask.js"/>
  	<g:javascript src="comum/lov.js"/>
  	<g:javascript src="usuario/usuario.js"/>
  	<g:javascript src="funcionario/funcionario.js"/>
  	<g:javascript src="filial/filial.js"/> 	
    <div class="titulo">
    	<h3><g:message code="default.create.label" args="['Usuario']" /></h3>
    </div>
    <g:render template="form"/>
  </body>
</html>