
<html>
  <head>
    <meta name="layout" content="main">
    <g:javascript src="comum/lov.js"/>
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${request.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${request.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Usuários</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Usuário']" /></g:link>
    </div>
       
    <form action="${request.contextPath}/usuario/list" method="POST" id="formPesquisa">
    	<div class="well">
  			<h3>Filtro</h3>
  			<div class="control-group">
				<label class="control-label">Tipo de filtro:</label>				
				<div class="controls">
					<g:select name="tipoFiltro" optionKey="id" optionValue="nome" 
							value="${params.tipoFiltro}"
							style="width:100px;"
							from="${[ [id:'0', nome:'...'],
									  [id:'1', nome:'Código'], 
									  [id:'2', nome:'Login'],
									  [id:'3', nome:'Nome'] ]}"/>
					<g:textField name="filtro" maxlength="250" value="${params.filtro}" style="margin-left: 10px; width: 400px;"/>
					<input type="hidden" id="order" name="order"/>
					<input type="hidden" id="sortType" name="sortType"/>
					<input type="hidden" id="max" name="max" value="${params.max}"/>
					<input type="hidden" id="offset" name="offset" value="${params.offset}"/>
					<g:submitButton name="filtrar" class="btn btn-medium btn-primary" value="Filtrar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>
          	Nome
          	<g:if test="${order != 'nome'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('nome', 'asc')"/></g:if>
          	<g:if test="${order == 'nome' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('nome', 'desc')"/></g:if>
          	<g:if test="${order == 'nome' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('nome', 'asc')"/></g:if>
          </th>
          <th>
          	Email
          	<g:if test="${order != 'email'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('email', 'asc')"/></g:if>
          	<g:if test="${order == 'email' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('email', 'desc')"/></g:if>
          	<g:if test="${order == 'email' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('email', 'asc')"/></g:if>
          </th>
          <th>
          	Ativo
          	<g:if test="${order != 'enabled'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('enabled', 'asc')"/></g:if>
          	<g:if test="${order == 'enabled' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('enabled', 'desc')"/></g:if>
          	<g:if test="${order == 'enabled' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('enabled', 'asc')"/></g:if>
          </th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${usuarioInstanceList}" status="i" var="usuarioInstance">
        <tr>
          <td>${fieldValue(bean: usuarioInstance, field: "nome")}</td>
          <td>${fieldValue(bean: usuarioInstance, field: "email")}</td>
          <td><g:formatBoolean boolean="${usuarioInstance.enabled}" /></td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${usuarioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/usuario/delete', ${usuarioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${usuarioInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${usuarioInstanceTotal}</span>
    </div>
  </body>
</html>