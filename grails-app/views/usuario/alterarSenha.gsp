
<html>
  <head>
    <meta name="layout" content="main">
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="usuario/usuario.js"/>
  </head>

  <body>
    <g:if test="${request.message}">
    <div class="alert alert-info" role="status">${request.message}</div>
    </g:if>

    <g:if test="${request.error}">
    <div class="alert alert-error" role="status">${request.error}</div>
    </g:if>

    <div class="titulo">
      <h2>Alterar senha</h2>
    </div>
    
    <br/><br/><br/>
    
    <form action="${request.contextPath}/usuario/alterarSenha" method="POST" id="formAlterarSenha">
    
    	<div class="control-group">
			<label class="control-label">Login:</label>
			<div class="controls">
				<g:if test="${usuarioReiniciar}">
					<g:textField name="login" maxlength="80" required="" value="${usuarioReiniciar}" readonly="true"/>
				</g:if>
				<g:else>
					<g:textField name="login" maxlength="80" required="" value=" "/>
				</g:else>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Senha:</label>
			<div class="controls">
				<g:passwordField name="senha" maxlength="80" required="" value=""/>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Confirmar senha:</label>
			<div class="controls">
				<g:passwordField name="confirmSenha" maxlength="80" required="" value=""/>
			</div>
		</div>
		
		<button type="button" class="btn btn-medium btn-primary" onclick="alterarSenha();">Salvar</button>
		
	</form>
	
  </body>
</html>