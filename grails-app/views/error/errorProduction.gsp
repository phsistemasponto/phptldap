<!doctype html>
<html>
	<head>
		<title>Grails Runtime Exception</title>
		<meta name="layout" content="main">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'errors.css')}" type="text/css">
	</head>
	<body>
		<div>
			<img src="${resource(dir:'images',file:'erro-icon.png')}" style='max-height: 60px;vertical-align: middle;'>
			<span style='vertical-align: middle;display: inline-block;height: 100%;'><h1>Houve um erro na operação do sistema</h1></span>
		</div>
		<br>
		<a class="btn" href="${request.contextPath}/"><b>Voltar para início</b></a>
	</body>
</html>