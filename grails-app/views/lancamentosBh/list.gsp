
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Lançamentos Banco de Horas</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Lançamentos Banco de Horas']" /></g:link>
    </div>
    
    <form action="${request.contextPath}/lancamentosBh/list" method="POST" id="formPesquisa">
    	<div class="well">
  			<h3>Filtro</h3>
  			<div class="control-group">
				<label class="control-label">Tipo de filtro:</label>				
				<div class="controls">
					<g:select name="tipoFiltro" optionKey="id" optionValue="nome" 
							value="${params.tipoFiltro}"
							style="width:100px;"
							from="${[ [id:'0', nome:'...'],
									  [id:'1', nome:'Código'], 
									  [id:'2', nome:'Crachá funcionário'] ]}"/>
					<g:textField name="filtro" maxlength="250" value="${params.filtro}" style="margin-left: 10px; width: 400px;"/>
					<input type="hidden" id="order" name="order"/>
					<input type="hidden" id="sortType" name="sortType"/>
					<input type="hidden" id="max" name="max" value="${params.max}"/>
					<input type="hidden" id="offset" name="offset" value="${params.offset}"/>
					<g:submitButton name="filtrar" class="btn btn-medium btn-primary" value="Filtrar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>Funcionário</th>
          <th>Data lançamento</th>
          <th>Tipo lançamento</th>
          <th>Horas</th>
          <th>Observacao</th>
	      <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${lancamentosBhInstanceList}" status="i" var="lancBh">
        <tr>
          <td>${lancBh.funcionario}</td>
          <td>${lancBh.dtLancExibicao}</td>
          <td>${lancBh.idTpLanc}</td>
          <td>${lancBh.nrQtdeHrExibicao}</td>
          <td>${lancBh.observacao}</td>
		  <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${lancBh.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/lancamentosBh/delete', ${lancBh.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${lancamentosBhInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${lancamentosBhInstanceTotal}</span>
    </div>
  </body>
</html>