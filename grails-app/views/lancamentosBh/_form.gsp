

	<g:javascript src="jquery.meiomask.js"/>
  	<g:javascript src="comum/lov.js"/>
  	<g:javascript src="lancamentosBh/lancamentosBh.js"/>
  	
	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${lancamentosBhInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${lancamentosBhInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${lancamentosBhInstance.id}">
		<form action="${request.contextPath}/lancamentosBh/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${lancamentosBhInstance?.id}" />
			<g:hiddenField name="version" value="${lancamentosBhInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/lancamentosBh/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>
		
			<div class="control-group">
				<label class="control-label">Data:</label>
				<div class="controls">
					<g:if test="${!lancamentosBhInstance?.id}">
						<g:textField name="dtLancamento" value="${lancamentosBhInstance?.dtLancExibicao}" style="width:80px" />
					</g:if>
					<g:else>
					  	<g:textField name="dtLancamento" value="${lancamentosBhInstance?.dtLancExibicao}" style="width:80px" readonly="true"/>
					</g:else>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Funcionário:</label>
				<div class="controls">
					<g:if test="${!lancamentosBhInstance?.id}">
						<g:textField name="funcionarioId" maxlength="5" required="" value="${lancamentosBhInstance?.funcionario?.id}" style="width:50px"
							onBlur="buscaIdFuncionario();"/>&nbsp;
					  	<i class="icon-search" onclick="buscarLovFuncionario();" style="cursor:pointer" title="Procurar Funcionario"></i>&nbsp;
					</g:if>
					<g:else>
					  	<g:textField name="funcionarioId" maxlength="5" required="" value="${lancamentosBhInstance?.funcionario?.id}" style="width:50px"
							onBlur="buscaIdFuncionario();" readonly="true"/>&nbsp;
					  	<i class="icon-search"></i>&nbsp;
					</g:else>
					<g:textField name="funcionarioDescricao" value="${lancamentosBhInstance?.funcionario?.nomFunc}" readonly="true" style="width:450px"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Horas:</label>
				<div class="controls">
					<g:textField name="horas" value="${lancamentosBhInstance?.nrQtdeHrExibicao}" style="width:80px" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Ocorrência:</label>
				<div class="controls">
					<g:textField name="ocorrenciaId" maxlength="5" required="" value="${lancamentosBhInstance?.ocorrencias?.id}" style="width:50px"
						onBlur="buscaIdOcorrencia();"/>&nbsp;
				  	<i class="icon-search" onclick="buscarLovOcorrencia();" style="cursor:pointer" title="Procurar Ocorrência"></i>&nbsp;
					<g:textField name="ocorrenciaDescricao" value="${lancamentosBhInstance?.ocorrencias?.dscOcor}" readonly="true" style="width:450px"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Lançamento:</label>
				<div class="controls">
					<g:select name="idTpLanc" optionKey="id" optionValue="nome" 
							value="${lancamentosBhInstance?.idTpLanc}"
							style="width:100px;"
							from="${[ [id:'C', nome:'Crédito'],
									  [id:'D', nome:'Débito'] ]}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Observação:</label>
				<div class="controls">
					<g:textArea name="observacao" value="${lancamentosBhInstance?.observacao}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>
	
	<g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>