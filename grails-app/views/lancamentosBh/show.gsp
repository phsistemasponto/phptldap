
<%@ page import="cadastros.LancamentosBh" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'lancamentosBh.label', default: 'LancamentosBh')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-lancamentosBh" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-lancamentosBh" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list lancamentosBh">
			
				<g:if test="${lancamentosBhInstance?.idTpLanc}">
				<li class="fieldcontain">
					<span id="idTpLanc-label" class="property-label"><g:message code="lancamentosBh.idTpLanc.label" default="Id Tp Lanc" /></span>
					
						<span class="property-value" aria-labelledby="idTpLanc-label"><g:fieldValue bean="${lancamentosBhInstance}" field="idTpLanc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${lancamentosBhInstance?.dtRef}">
				<li class="fieldcontain">
					<span id="dtRef-label" class="property-label"><g:message code="lancamentosBh.dtRef.label" default="Dt Ref" /></span>
					
						<span class="property-value" aria-labelledby="dtRef-label"><g:formatDate date="${lancamentosBhInstance?.dtRef}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${lancamentosBhInstance?.observacao}">
				<li class="fieldcontain">
					<span id="observacao-label" class="property-label"><g:message code="lancamentosBh.observacao.label" default="Observacao" /></span>
					
						<span class="property-value" aria-labelledby="observacao-label"><g:fieldValue bean="${lancamentosBhInstance}" field="observacao"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${lancamentosBhInstance?.dtLanc}">
				<li class="fieldcontain">
					<span id="dtLanc-label" class="property-label"><g:message code="lancamentosBh.dtLanc.label" default="Dt Lanc" /></span>
					
						<span class="property-value" aria-labelledby="dtLanc-label"><g:formatDate date="${lancamentosBhInstance?.dtLanc}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${lancamentosBhInstance?.funcionario}">
				<li class="fieldcontain">
					<span id="funcionario-label" class="property-label"><g:message code="lancamentosBh.funcionario.label" default="Funcionario" /></span>
					
						<span class="property-value" aria-labelledby="funcionario-label"><g:link controller="funcionario" action="show" id="${lancamentosBhInstance?.funcionario?.id}">${lancamentosBhInstance?.funcionario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${lancamentosBhInstance?.nrQtdeHr}">
				<li class="fieldcontain">
					<span id="nrQtdeHr-label" class="property-label"><g:message code="lancamentosBh.nrQtdeHr.label" default="Nr Qtde Hr" /></span>
					
						<span class="property-value" aria-labelledby="nrQtdeHr-label"><g:fieldValue bean="${lancamentosBhInstance}" field="nrQtdeHr"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${lancamentosBhInstance?.ocorrencias}">
				<li class="fieldcontain">
					<span id="ocorrencias-label" class="property-label"><g:message code="lancamentosBh.ocorrencias.label" default="Ocorrencias" /></span>
					
						<span class="property-value" aria-labelledby="ocorrencias-label"><g:link controller="ocorrencias" action="show" id="${lancamentosBhInstance?.ocorrencias?.id}">${lancamentosBhInstance?.ocorrencias?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${lancamentosBhInstance?.usuario}">
				<li class="fieldcontain">
					<span id="usuario-label" class="property-label"><g:message code="lancamentosBh.usuario.label" default="Usuario" /></span>
					
						<span class="property-value" aria-labelledby="usuario-label"><g:link controller="usuario" action="show" id="${lancamentosBhInstance?.usuario?.id}">${lancamentosBhInstance?.usuario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${lancamentosBhInstance?.id}" />
					<g:link class="edit" action="edit" id="${lancamentosBhInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
