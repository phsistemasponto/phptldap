<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
  <div class="titulo">
      <h3><g:message code="default.create.label" args="['Lançamento Banco de Horas']" /></h3>
  </div>
    <g:render template="form"/>
  </body>
</html>