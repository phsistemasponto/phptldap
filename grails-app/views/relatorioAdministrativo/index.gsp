
<%@page import="cadastros.Justificativas"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    <g:javascript src="reports/administrativo.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Relatórios administrativos</h3>
    </div>
    
    <g:jasperForm controller="relatorioAdministrativo" action="generateReport" jasper="administrativo" class="form-horizontal" id="formPesquisa">
    
    	<div class="well">
    	
    		<div class="well" style="border: 1px solid #859CA4; background-color: #859CA4;">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					</div>
				</div>
			</div>
			
		    
	    	<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Data início:</label>
				<div class="controls">
					<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Data fim:</label>
				<div class="controls">
					<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" />
				</div>
			</div>  
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Relatório:</label>
				<div class="controls">
					<g:select name="relatorio" id="relatorio" optionKey="id" optionValue="nome" 
							value="${params.relatorio}"
							style="width:200px;"
							from="${[ [id:'', nome:'...'],
									  [id:'0', nome:'Levantamento de funcionários presentes'],
									  [id:'1', nome:'Levantamento de quantidade horas trabalhadas'],
									  [id:'2', nome:'Levantamento de faltas (Justificadas, não justificadas)'],
									  [id:'3', nome:'Levantamento das ocorrências sofridas pelo funcionário'],
									  [id:'4', nome:'Levantamento de funcionários saíram depois do horário (Tiveram extras)'],
									  [id:'5', nome:'Levantamento de funcionários (Entraram mais cedo)']  ]}"/>
				</div>
			</div>
			
			<div class="control-group" id="verificacaoPresencaDiv" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Verificação:</label>
				<div class="controls">
					<g:select name="verificacaoPresenca" id="verificacaoPresenca" optionKey="id" optionValue="nome" 
							value="${params.verificacaoPresenca}"
							style="width:200px;"
							from="${[ [id:'', nome:'...'],
									  [id:'0', nome:'somente dias de folga'],
									  [id:'1', nome:'somente dias de trabalho'] ]}"/>
				</div>
			</div>
			
			<div class="control-group" id="agrupadoPorFilialDiv" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Agrupado por filial:</label>
				<div class="controls">
					<input type="checkbox" name="agrupadoPorFilial" id="agrupadoPorFilial"/>
				</div>
			</div>
			
			<div class="control-group" id="diferencaHorasDiv" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Diferença horas:</label>
				<div class="controls">
					<g:textField name="diferencaHoras" id="diferencaHoras" value="${params.diferencaHoras}" style="width:80px" />
				</div>
			</div>
				    
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('PDF'); else return false;">Gerar em PDF</button>
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('XLS'); else return false;">Gerar em Excel</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

	<div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>