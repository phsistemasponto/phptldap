
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="geracaoConsultas/geracaoConsultas.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    
    <h3>Geração de consultas</h3>
    
    <br>
    
    <g:form controller="geracaoConsultas" action="executeSql" class="form-horizontal" id="formConsulta">
    
    	<button class="btn btn-medium btn-info" type="button" onclick="executarSQL();">Executar SQL</button>
    	<button class="btn btn-medium btn-info" type="button" onclick="exportarExcel();">Exportar para EXCEL</button>
    	
    	<br><br><br>
    	
    	<div class="tabbable" id="tab">
		    <ul class="nav nav-tabs">
			    <li class="active" id="liSQL"><a href="#tabSQL" data-toggle="tab">SQL</a></li>
			    <li id="liResultado"><a href="#tabResultado" data-toggle="tab">Resultado</a></li>
			    <li id="liHistorico"><a href="#tabHistorico" data-toggle="tab">Histórico</a></li>
		    </ul>
         	<div class="tab-content">
         		
         		<div class="tab-pane active" id="tabSQL">
         			<textarea rows="10" cols="700" id="inputSql" name="inputSql" style="width: 90%;">
         			</textarea>
         		</div>
         		
         		<div class="tab-pane" id="tabResultado">
         			<div style="max-width: 100%; max-height: 400px; overflow: auto;">
	         			<table class="table table-striped table-bordered table-condensed" id="resultadoSql">
							<thead></thead>
							<tbody></tbody>
						</table>
					</div>
         		</div>
         		
         		<div class="tab-pane" id="tabHistorico">
         			<select id="selectHistorico" style="width: 90%; height: 200px; overflow: auto;" multiple="multiple">
         				<g:each in="${consultas}" var="consulta">
         					<option ondblclick="copiaSqlHistorico();">${consulta}</option>
         				</g:each>
         			</select>
         		</div>
         		
         	</div>
         </div>
			
    </g:form>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

	<div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>