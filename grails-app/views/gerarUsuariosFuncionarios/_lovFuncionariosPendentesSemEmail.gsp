<div id="divLovFuncionariosPendentesSemEmail" style="display: none;" title="Funcionários sem usuário e sem e-mail">
<g:form id="formFuncionariosPendentesSemEmail" name="formFuncionariosPendentesSemEmail" controller="gerarUsuariosFuncionarios" action="atualizarEmail">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>

    <table class="table table-striped table-bordered table-condensed">
   		<thead>
   			<tr>
   				<th style="width: 70%">Funcionario</th>
   				<th style="width: 30%">E-mail</th>
   			</tr>
   		</thead>
   		<tbody>
   			<g:each in="${funcionariosSemEmailPendentes}" var="funcionario">
   				<tr>
   					<td>${funcionario}</td>
   					<td>
   						<input type="text" id="emailFunc-${funcionario.id}" name="emailFunc-${funcionario.id}"/>
					</td>
   				</tr>
   			</g:each>
   		</tbody>
   	</table>
   	
   	<br><br>
   	
   	<g:submitButton name="salvar" class="btn btn-large btn-primary" value="Salvar" />
    
</g:form>
</div>