<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Geração de usuários para funcionários</h3>
    </div>
    
    <g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="gerarUsuariosFuncionarios/gerarUsuariosFuncionarios.js"/>
       
    <g:form class="well form-horizontal" controller="gerarUsuariosFuncionarios" action="gerarUsuarios">
    
    	<div class="well">
    		<span style="font-weight: bold;">Existe(m) ${funcionariosPendentes.size()} funcionário(s) para ser(em) criado(s) usuário(s).</span>
    		<a href="#" onclick="showFuncionariosPendentes();">Exibir</a>
    		
    		<br><br><br>
    		
    		<div class="control-group">
				<label class="control-label">Gerar login:</label>
				<div class="controls">
					<g:select name="opcaoLogin" id="opcaoLogin" optionKey="id" optionValue="nome" style="width:200px;"
						from="${[ [id:'0', nome:'Por e-mail'],
								  [id:'1', nome:'Por Primeiro nome + primeira letra']  ]}"/>
				</div>
			</div>
			
			<br>
			
			<g:submitButton name="salvar" class="btn btn-large btn-primary" value="Gerar" />
			
    	</div>
    	
    	<div class="well">
    		<span style="font-weight: bold;">Existe(m) ${funcionariosSemEmailPendentes.size()} funcionário(s) sem usuário e sem e-mail configurado.</span>
    		<a href="#" onclick="showFuncionariosPendentesSemEmail();">Exibir</a>
    	</div>
    	
    </g:form>
    
    <br/>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    <g:render template="/gerarUsuariosFuncionarios/lovFuncionariosPendentes"/>
    <g:render template="/gerarUsuariosFuncionarios/lovFuncionariosPendentesSemEmail"/>
    
    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>