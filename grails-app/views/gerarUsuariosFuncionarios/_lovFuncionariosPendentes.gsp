<div id="divLovFuncionariosPendentes" style="display: none;" title="Funcionários sem usuário">
<g:form id="formFuncionariosPendentes" name="formFuncionariosPendentes">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>

    <table class="table table-striped table-bordered table-condensed">
   		<thead>
   			<tr>
   				<th style="width: 70%">Funcionario</th>
   				<th style="width: 30%">E-mail</th>
   			</tr>
   		</thead>
   		<tbody>
   			<g:each in="${funcionariosPendentes}" var="funcionario">
   				<tr>
   					<td>${funcionario}</td>
   					<td>${funcionario.emlFunc}</td>
   				</tr>
   			</g:each>
   		</tbody>
   	</table>
    
</g:form>
</div>