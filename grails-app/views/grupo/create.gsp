<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
  	<g:javascript src="jquery.meiomask.js"/>
  	<g:javascript src="comum/lov.js"/>
  	<g:javascript src="usuario/usuario.js"/>
  	<g:javascript src="funcionario/funcionario.js"/>
  	<g:javascript src="filial/filial.js"/> 	
    <h2><g:message code="default.create.label" args="['Grupo']" /></h2>
    <g:render template="form"/>
  </body>
</html>