

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${grupoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${grupoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${grupoInstance.id}">
		<form action="${request.contextPath}/grupo/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${grupoInstance?.id}" />
			<g:hiddenField name="version" value="${grupoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/grupo/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscGrupo" maxlength="80" required="" value="${grupoInstance?.dscGrupo}"/>
				</div>
			</div>
			
			<fieldset>
				<legend>Funcionários</legend>
				
				<div id="formPesquisa">
			    	<div class="well">
			 			<div class="control-group">
							<label class="control-label">Filtro:</label>
							<div class="controls">
								<input type="hidden" name="filtroId" id="filtroId">
								<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
								<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
								<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
								<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
								<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
								<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
								<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
								<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
								
								<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
								<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
								<button type="button" name="btnFiltro" class="btn btn-medium btn-primary" style="margin-left: 10px;" 
									onclick="consultaFiltroPadraoAjax()">OK</button>
							</div>
						</div>
					</div>
				</div>
				
				<div id="funcionarios">
					<table class="table table-striped table-bordered table-condensed" id="funcionarioTable">
						<thead>
							<tr>
									<th style="width: 20%;">Matrícula</th>
									<th style="width: 70%;">Funcionário</th>
									<th style="width: 10%;"></th>
							</tr>
						</thead>
						<tbody>
								<g:each in="${funcionarios?}" status="i" var="item">
									<tr id="rowTableFuncionario" class="even">
										<td>
											<g:hiddenField name="funcionarioIdTable" value="${item?.funcionario?.id}" />
											<g:textField name="funcionarioMatriculaTable" style="width:90%" value="${item?.funcionario?.mtrFunc}" readOnly="true"/></td>
										<td>
											<g:textField name="funcionarioNomeTable" style="width:90%" value="${item?.funcionario?.nomFunc}" readOnly="true" />
										</td>
										<td>
											<i class="icon-remove" onclick="excluirFuncionario(this);" style="cursor: pointer" title="Excluir registro"></i>
										</td>
									</tr>
								</g:each>
						
						</tbody>
					</table>
				</div>	
			</fieldset>		

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		
	</form>
	
	<g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>