

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${menuInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${menuInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${menuInstance.id}">
		<form action="${request.contextPath}/menu/update" class="well form-horizontal" method="POST" enctype="multipart/form-data">
			<g:hiddenField name="id" value="${menuInstance?.id}" />
			<g:hiddenField name="version" value="${menuInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/menu/save" class="well form-horizontal" method="POST" enctype="multipart/form-data">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Nome:</label>
				<div class="controls">
					<g:textField name="nome" maxlength="120" required="" value="${menuInstance?.nome}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Config Attribute:</label>
				<div class="controls">
					<g:textArea name="configAttribute" cols="40" rows="5" maxlength="255" required="" value="${menuInstance?.configAttribute}" noSelection="${['null':'--']}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Menu:</label>
				<div class="controls">
					<g:checkBox name="menu" value="${menuInstance?.menu}" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">É Atalho:</label>
				<div class="controls">
					<g:checkBox name="atalho" value="${menuInstance?.atalho}" onclick="clickAtalho();"/>
				</div>
			</div>
			
			<div id="controlesAtalho" style="display: none;">
			
				<div class="control-group">
					<label class="control-label">Hint:</label>
					<div class="controls">
						<g:textField name="comandoAtalho" maxlength="10" value="${menuInstance?.comandoAtalho}"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Ícone:</label>
					<div class="controls" id="controlsIcone">
						<input type="hidden" id="mudouIcone" name="mudouIcone" value="false"/>
						<div style="height: 30px; width: 30px; border: 1px solid black;" id="divIcone">
							<g:if test="${possuiIcone}">
								<img src="${createLink(controller:'menu', action:'image', id: menuInstance.id)}"/>
							</g:if>
						</div>
						<g:if test="${!possuiIcone}">
							<input type="file" id="files" name="files" />
						</g:if>
						<g:else>
							<button id="removIcone" type="button" onclick="removerIcone();">Remover ícone</button>
						</g:else>
					</div>
				</div>
				
			</div>

			<div class="control-group">
				<label class="control-label">Ordem:</label>
				<div class="controls">
					<g:textField name="ordem" required="" value="${fieldValue(bean: menuInstance, field: 'ordem')}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Pai:</label>
				<div class="controls">
					<g:select id="pai" name="pai.id" from="${acesso.Menu.list()}" optionKey="id"  value="${menuInstance?.pai?.id}" noSelection="${['null':'--']}"  class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Url:</label>
				<div class="controls">
					<g:textField name="url" value="${menuInstance?.url}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>
	
	<script>

		var $j = jQuery.noConflict();

		function clickAtalho() {
			if ($j('#atalho').is(':checked')) {
	    		$j('#controlesAtalho').show();
		    } else {
		    	$j('#controlesAtalho').hide();
			}
		}

		var file = document.getElementById('files'); 
		if (file != null) {
			file.addEventListener('change', handleFileSelect, false);
		}

		function handleFileSelect(evt) {
		    var files = evt.target.files;

		    // Loop through the FileList and render image files as thumbnails.
		    for (var i = 0, f; f = files[i]; i++) {

		      // Only process image files.
		      if (!f.type.match('image.*')) {
		        continue;
		      }
		      
		      var reader = new FileReader();

		      // Closure to capture the file information.
		      reader.onload = (function(theFile) {
		        return function(e) {
		          // Render thumbnail.
		          document.getElementById('divIcone').innerHTML = ['<img class="thumb" src="', e.target.result,
		                            '" title="', escape(theFile.name), '"/>'].join('');
		          document.getElementById('mudouIcone').value = 'true';
		          
		        };
		      })(f);

		      // Read in the image file as a data URL.
		      reader.readAsDataURL(f);
		    }
		}

		function removerIcone() {
			$j('#divIcone img').remove();
			$j('#removIcone').remove();
			$j('#controlsIcone').append('<input type=\"file\" id=\"files\" name=\"files\" />');
			$j('#mudouIcone').val('true');

			var file = document.getElementById('files'); 
			if (file != null) {
				file.addEventListener('change', handleFileSelect, false);
			}
		}

		if ($j('#atalho').is(':checked')) {
    		$j('#controlesAtalho').show();
	    }

	</script>