
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Menus</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Menu']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Nome</th>
		
          <th>Config Attribute</th>
		
          <th>Menu</th>
		
          <th>Ordem</th>
		
          <th>Pai</th>
		
          <th>Url</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${menuInstanceList}" status="i" var="menuInstance">
        <tr>
		
          <td>${fieldValue(bean: menuInstance, field: "nome")}</td>
		
          <td>${fieldValue(bean: menuInstance, field: "configAttribute")}</td>
		
          <td><g:formatBoolean boolean="${menuInstance.menu}" /></td>
		
          <td>${fieldValue(bean: menuInstance, field: "ordem")}</td>
		
          <td>${fieldValue(bean: menuInstance, field: "pai")}</td>
		
          <td>${fieldValue(bean: menuInstance, field: "url")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${menuInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/menu/delete', ${menuInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${menuInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${menuInstanceTotal}</span>
    </div>
  </body>
</html>