
<%@ page import="cadastros.FuncionarioHoraExtra" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'funcionarioHoraExtra.label', default: 'FuncionarioHoraExtra')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-funcionarioHoraExtra" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-funcionarioHoraExtra" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list funcionarioHoraExtra">
			
				<g:if test="${funcionarioHoraExtraInstance?.dtFimVig}">
				<li class="fieldcontain">
					<span id="dtFimVig-label" class="property-label"><g:message code="funcionarioHoraExtra.dtFimVig.label" default="Dt Fim Vig" /></span>
					
						<span class="property-value" aria-labelledby="dtFimVig-label"><g:formatDate date="${funcionarioHoraExtraInstance?.dtFimVig}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHoraExtraInstance?.configHoraExtra}">
				<li class="fieldcontain">
					<span id="configHoraExtra-label" class="property-label"><g:message code="funcionarioHoraExtra.configHoraExtra.label" default="Config Hora Extra" /></span>
					
						<span class="property-value" aria-labelledby="configHoraExtra-label"><g:link controller="configHoraExtra" action="show" id="${funcionarioHoraExtraInstance?.configHoraExtra?.id}">${funcionarioHoraExtraInstance?.configHoraExtra?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHoraExtraInstance?.dtIniVig}">
				<li class="fieldcontain">
					<span id="dtIniVig-label" class="property-label"><g:message code="funcionarioHoraExtra.dtIniVig.label" default="Dt Ini Vig" /></span>
					
						<span class="property-value" aria-labelledby="dtIniVig-label"><g:formatDate date="${funcionarioHoraExtraInstance?.dtIniVig}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHoraExtraInstance?.filial}">
				<li class="fieldcontain">
					<span id="filial-label" class="property-label"><g:message code="funcionarioHoraExtra.filial.label" default="Filial" /></span>
					
						<span class="property-value" aria-labelledby="filial-label"><g:link controller="filial" action="show" id="${funcionarioHoraExtraInstance?.filial?.id}">${funcionarioHoraExtraInstance?.filial?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHoraExtraInstance?.funcionario}">
				<li class="fieldcontain">
					<span id="funcionario-label" class="property-label"><g:message code="funcionarioHoraExtra.funcionario.label" default="Funcionario" /></span>
					
						<span class="property-value" aria-labelledby="funcionario-label"><g:link controller="funcionario" action="show" id="${funcionarioHoraExtraInstance?.funcionario?.id}">${funcionarioHoraExtraInstance?.funcionario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${funcionarioHoraExtraInstance?.id}" />
					<g:link class="edit" action="edit" id="${funcionarioHoraExtraInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
