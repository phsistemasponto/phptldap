
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>FuncionarioHoraExtras</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['FuncionarioHoraExtra']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>Dt Fim Vig</th>
          <th>Config Hora Extra</th>
          <th>Dt Ini Vig</th>
          <th>Filial</th>
          <th>Funcionario</th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${funcionarioHoraExtraInstanceList}" status="i" var="funcionarioHoraExtraInstance">
        <tr>
		
          <td>${fieldValue(bean: funcionarioHoraExtraInstance, field: "dtFimVig")}</td>
		
          <td>${fieldValue(bean: funcionarioHoraExtraInstance, field: "configHoraExtra")}</td>
		
          <td><g:formatDate date="${funcionarioHoraExtraInstance.dtIniVig}" /></td>
		
          <td>${fieldValue(bean: funcionarioHoraExtraInstance, field: "filial")}</td>
		
          <td>${fieldValue(bean: funcionarioHoraExtraInstance, field: "funcionario")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${funcionarioHoraExtraInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/funcionariohoraextra/delete', ${funcionarioHoraExtraInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${funcionarioHoraExtraInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${funcionarioHoraExtraInstanceTotal}</span>
    </div>
  </body>
</html>