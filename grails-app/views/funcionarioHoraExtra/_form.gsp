

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${funcionarioHoraExtraInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${funcionarioHoraExtraInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">"${error.field}"</g:if><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>
	<form class="well form-horizontal" method="POST">
		<g:hiddenField name="funcionarioId" value="${funcionarioId}" />
		<g:hiddenField name="filialId" value="${filialId}" />
		<fieldset>
			
			<g:if test="${acao =='edit'}">
				<div class="control-group">
					<label class="control-label">Hora Extra:</label>
					<div class="controls">
						<g:select id="configHoraExtraId" name="configHoraExtraId" from="${cadastro.ConfigHoraExtra.list()}" optionKey="id" required="" value="" class="many-to-one" readOnly="true"/>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label">Dt Início Vigência:</label>
					<div class="controls">
						<g:textField name="dtIniVig"  value="${funcionarioHoraExtraInstance?.dataInicioFormatada}"  disabled="true"/>					
					</div>
				</div>
			
			</g:if>
			<g:else>
				<div class="control-group">
					<label class="control-label">Hora Extra:</label>
					<div class="controls">
						<g:select id="configHoraExtraId" name="configHoraExtraId" from="${cadastro.ConfigHoraExtra.list()}" optionKey="id" required="true"  class="many-to-one"/>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label">Dt Início Vigência:</label>
					<div class="controls">
						<g:textField name="dtIniVig"  required="tru"/>					
					</div>
				</div>
			</g:else>	
			<div class="control-group">
				<label class="control-label">Data Final Vigência:</label>
				<div class="controls">
					<g:textField name="dtFimVig"  value="${funcionarioHoraExtraInstance?.dataFimFormatada}"  />					
				</div>
			</div>


 
		</fieldset>
		<hr>
		<g:if test="${acao =='edit'}">
			<a class="btn btn-large btn-primary" href="#" onClick="Modalbox.hide({afterHide: confirmarEdicaoHoraExtra() });">${message(code: 'default.button.save.label')}</a>
		</g:if>
		<g:else>
			<a class="btn btn-large btn-primary" href="#" onClick="Modalbox.hide({afterHide: confirmarAdicaoHoraExtra() });">${message(code: 'default.button.save.label')}</a>
		</g:else>
	</form>