
<%@page import="cadastros.Ocorrencias"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    <g:javascript src="reports/gerenciais.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h2>Relatório gerenciais</h2>
    </div>
    
    <br/><br/><br/>
    
    <g:jasperForm controller="relatorioGerenciais" action="generateReport" jasper="gerenciais" class="form-horizontal" id="formPesquisa" target="_blank">
    
    	<div class="well">
    	
    		<div class="well" style="border: 1px solid #859CA4; background-color: #859CA4;">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					</div>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Relatório:</label>
				<div class="controls">
					<g:select name="relatorio" id="relatorio" optionKey="id" optionValue="nome" 
							value="${params.relatorio}"
							style="width:200px;"
							from="${[ [id:'', nome:'...'],
									  [id:'0', nome:'Banco horas'],
									  [id:'1', nome:'Fechamento'],
									  [id:'2', nome:'Total Horas'],
									  [id:'3', nome:'Média de Horas Extras'],
									  [id:'4', nome:'Média de Compensação'],
									  [id:'5', nome:'Listagem de batidas INC'],
									  [id:'6', nome:'Excesso de Jornada']  ]}"/>
				</div>
			</div>
			
			<div class="control-group" id="divPesquisa" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Pesquisa:</label>
				<div class="controls">
					<g:select name="pesquisa" id="pesquisa" optionKey="id" optionValue="nome" 
							value="${params.pesquisa}"
							style="width:200px;"
							from="${[ [id:'', nome:'...'],
									  [id:'0', nome:'Horas'],
									  [id:'1', nome:'Valores em moeda'],
									  [id:'2', nome:'Pagamento'],
									  [id:'3', nome:'> Quant. de BH Positivas'],
									  [id:'4', nome:'> Quant. de BH Negativas']  ]}"/>
				</div>
			</div>
			
			<div class="control-group" id="divValorEmMoeda" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Valor em moeda:</label>
				<div class="controls">
					<input type="checkbox" name="valorEmMoeda" id="valorEmMoeda"/>
				</div>
			</div>
			
			<div class="control-group" id="divDtInicio" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Data início:</label>
				<div class="controls">
					<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" />
				</div>
			</div>
			
			<div class="control-group" id="divDtFim" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Data fim:</label>
				<div class="controls">
					<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" />
				</div>
			</div> 
			
			<div class="control-group" id="divPrimeirosDias" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Primeiros dias:</label>
				<div class="controls">
					<g:textField name="primeirosDias" value="${params.primeirosDias}" style="width:80px" />
				</div>
			</div> 
			
			<div class="control-group" id="divTipoValores" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Relatório:</label>
				<div class="controls">
					<g:select name="tipoValores" id="tipoValores" optionKey="id" optionValue="nome" 
							value="${params.pesquisa}"
							style="width:200px;"
							from="${[ [id:'', nome:'...'],
									  [id:'0', nome:'Valores Horas'],
									  [id:'1', nome:'Valores em moeda']  ]}"/>
				</div>
			</div>
			
			<div class="control-group" id="divAgrupamento" style="display: none;">
				<label class="control-label" style="font-weight: bold;">Agrupamento:</label>
				<div class="controls">
					<g:select name="agrupamento" id="agrupamento" optionKey="id" optionValue="nome" 
							value="${params.agrupamento}"
							style="width:200px;"
							from="${[ [id:'', nome:'...'],
									  [id:'0', nome:'Filial'],
									  [id:'1', nome:'Setor']  ]}"/>
				</div>
			</div>
			
			<div id="divPlanilha" style="display: none;">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Quantidade meses:</label>
					<div class="controls">
						<g:textField name="meses" value="${params.meses}" style="width:50px" onchange="mudaMeses();"/>&nbsp;
					</div>
				</div>
				<table class="table table-striped table-bordered table-condensed" id="horarioTable" style="margin-top: 5px; width: 150px;">
		    		<thead>
		    			<tr>
		    				<th style="width:15px;"></th>
		    				<th>Período</th>
		    			</tr>
		    		</thead>
		    		<tbody id="tbodyPeriodos">
			    		<g:each in="${periodos}" var="periodo">
			    			<tr id="rowTablePeriodo${periodo.id}">
			    				<td>
			    					<input type="checkbox" name="periodosId" id="periodosId" value="${periodo.id}" checked="checked"/>
			    				</td>
			    				<td>${periodo.apelPr}</td>
			    			</tr>
			    		</g:each>
		    		</tbody>
		    	</table>
			</div>
			
			<div id="divOcorrencia" style="display: none;">
				<table class="table table-striped table-bordered table-condensed" id="horarioTable" style="margin-top: 5px;">
		    		<thead>
		    			<tr>
		    				<th>Seq</th>
		    				<th>Ocorrência</th>
		    				<th>&nbsp;</th>
		    			</tr>
		    		</thead>
		    		<g:each in="${Ocorrencias.findAll('from Ocorrencias o where o.formulaOcorBh is not null and o.formulaOcorBh <> \'\' ')}" var="ocorrencia">
			    		<tbody>
			    			<tr id="rowTableOcorrencia${ocorrencia.id}">
			    				<td>
			    					${ocorrencia.id}
			    					<input type="hidden" name="ocorrenciasId" id="ocorrenciasId" value="${ocorrencia.id}">
			    				</td>
			    				<td>${ocorrencia.dscOcor}</td>
			    				<td>
			    					<i class="icon-remove" onclick="excluirOcorrencia(${ocorrencia.id});" style="cursor: pointer" title="Excluir registro"></i>
			    				</td>
			    			</tr>
			    		</tbody>
		    		</g:each>
		    	</table>
		    	<input type="hidden" name="ocorrenciasId" id="ocorrenciasId" value="0">
		    </div>
			
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('XLS'); else return false;">Gerar em Excel</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>