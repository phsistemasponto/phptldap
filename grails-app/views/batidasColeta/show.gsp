
<%@ page import="cadastros.BatidasColeta" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'batidasColeta.label', default: 'BatidasColeta')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-batidasColeta" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-batidasColeta" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list batidasColeta">
			
				<g:if test="${batidasColetaInstance?.arquivo}">
				<li class="fieldcontain">
					<span id="arquivo-label" class="property-label"><g:message code="batidasColeta.arquivo.label" default="Arquivo" /></span>
					
						<span class="property-value" aria-labelledby="arquivo-label"><g:link controller="arquivoBatidaColeta" action="show" id="${batidasColetaInstance?.arquivo?.id}">${batidasColetaInstance?.arquivo?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${batidasColetaInstance?.creation}">
				<li class="fieldcontain">
					<span id="creation-label" class="property-label"><g:message code="batidasColeta.creation.label" default="Creation" /></span>
					
						<span class="property-value" aria-labelledby="creation-label"><g:formatDate date="${batidasColetaInstance?.creation}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${batidasColetaInstance?.relogio}">
				<li class="fieldcontain">
					<span id="relogio-label" class="property-label"><g:message code="batidasColeta.relogio.label" default="Relogio" /></span>
					
						<span class="property-value" aria-labelledby="relogio-label"><g:link controller="relogio" action="show" id="${batidasColetaInstance?.relogio?.id}">${batidasColetaInstance?.relogio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${batidasColetaInstance?.usuario}">
				<li class="fieldcontain">
					<span id="usuario-label" class="property-label"><g:message code="batidasColeta.usuario.label" default="Usuario" /></span>
					
						<span class="property-value" aria-labelledby="usuario-label"><g:link controller="usuario" action="show" id="${batidasColetaInstance?.usuario?.id}">${batidasColetaInstance?.usuario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${batidasColetaInstance?.id}" />
					<g:link class="edit" action="edit" id="${batidasColetaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
