

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${batidasColetaInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${batidasColetaInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${batidasColetaInstance.id}">
		<form action="${request.contextPath}/batidascoleta/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${batidasColetaInstance?.id}" />
			<g:hiddenField name="version" value="${batidasColetaInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/batidascoleta/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Arquivo:</label>
				<div class="controls">
					<g:select id="arquivo" name="arquivo.id" from="${cadastros.ArquivoBatidaColeta.list()}" optionKey="id" required="" value="${batidasColetaInstance?.arquivo?.id}" class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Creation:</label>
				<div class="controls">
					<g:datePicker name="creation" precision="day"  value="${batidasColetaInstance?.creation}"  />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Relogio:</label>
				<div class="controls">
					<g:select id="relogio" name="relogio.id" from="${cadastros.Relogio.list()}" optionKey="id" required="" value="${batidasColetaInstance?.relogio?.id}" class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Usuario:</label>
				<div class="controls">
					<g:select id="usuario" name="usuario.id" from="${acesso.Usuario.list()}" optionKey="id" required="" value="${batidasColetaInstance?.usuario?.id}" class="many-to-one"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>