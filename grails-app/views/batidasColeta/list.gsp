
<html>
  <head>
    <meta name="layout" content="main">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'fileuploader.css')}" type="text/css">
    <g:javascript src="jquery-1.8.0.min.js"/>
    <g:javascript src="fileuploader.js"/>
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
  	<g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
	<g:javascript src="batidasColeta/batidasColeta.js"/>
	
    <g:if test="${flash.message}">
    	<div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    	<div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Coletas de batida</h3>
    </div>
    <form action="${request.contextPath}/batidasColeta/processar" class="well form-horizontal" method="POST" id="formBatidas">
    
	    <fieldset>
	    	
	    	<input type="checkbox" id="importarIndividual" name="importarIndividual" onclick="alteraControleBatida();"/> Importar individual
	    	<br><br>
			
	    	<div class="row-fluid">	
			    <div class="span12">
				    <div class="controls-horizontal">
						<g:textField name="dataInicioInput" maxlength="10" style="width:100px" placeholder="Data Inicio"/>
						&emsp;
						<g:textField name="dataFimInput" maxlength="10" style="width:100px" placeholder="Data Fim"/>
						<a href="#" class="btn btn-medium" onClick="incluirDatas();">Incluir</a>
					</div>	
				</div>
			</div>
			
			<br>			
			<div class="row-fluid">
				<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
					<thead>
						<tr class="text-center">
							<th><h4>Filial</h4></th>
							<th class="span1 "><input type="checkbox" class="check_all"/> </th>
							<th class="span2"><h4>Dt início </h4></th>
							<th class="span2"><h4>Dt fim </h4></th>
						</tr>
					</thead>

					<tbody>
						<g:each in="${filiais?}" status="i" var="filial">
							<tr id="rowTableFilial${i}" class="even">
								<td>
									<g:hiddenField name="filialId" value="${filial?.id}" />
									<g:textField name="descricaoFilial" style="width:90%" value='${filial?.dscFil+" - "+filial?.unicodFilial}' readOnly="true" />
								</td>
								<td>
									<input class="ckeck_act" type='checkbox' name="temFilial${filial?.id}" value='true' checked="checked"/>
								</td>
								<td>
									<g:textField name="dtInicioFilial" style="width:90%" readOnly="true" />
								</td>
								<td>
									<g:textField name="dtFimFilial" style="width:90%" readOnly="true" />
								</td>
								
							</tr>
						</g:each>
					</tbody>
				</table>
				
				<table class="table table-striped table-bordered table-condensed" id="funcionarioTable">
					<thead>
						<tr class="text-center">
							<th><h4>Funcionário</h4></th>
							<th class="span2"><h4>Dt início </h4></th>
							<th class="span2"><h4>Dt fim </h4></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								Funcionário:
								<g:textField name="funcionarioId" maxlength="5" style="width:50px" onBlur="buscaIdFuncionario();" value="${params.funcionarioId}"/>&nbsp; 
								<i class="icon-search" onclick="buscarLovFuncionario();" style="cursor: pointer" 
									title="Procurar Funcionário" id="funcionarioBotaoBusca"></i> &nbsp;
								<g:textField name="funcionarioNome" maxlength="450" readOnly="true" style="width:450px" value="${params.funcionarioNome}"/>
							</td>
							<td>
								<g:textField name="dtInicioFuncionario" style="width:90%" readOnly="true" />
							</td>
							<td>
								<g:textField name="dtFimFuncionario" style="width:90%" readOnly="true" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<br>
			
			<button type="button" class="btn btn-medium btn-primary btn-info pull-right" onclick="processarBatidas();">Processar</button>
			
		</fieldset>
    		
    </form>
    
    <g:uploadForm action="${request.contextPath}/batidasColeta/save" class="well form-horizontal">
    
    	<fieldset>
      		<div>
				<table class="table table-striped table-bordered table-condensed" id="relogiosTable">
					<thead>
						<tr>
							<th><h4>Nome<h4></h4></th>
							<th class="span2"><h4>Upload</h4></th>
							<th class="span2"><h4>Qt linhas</h4></th>
							<th class="span2"><h4>Status</h4></th>
						</tr>
					</thead>

					<tbody>
						<g:each in="${relogios?}" status="i" var="relogio">
							<tr id="rowTableRelogio${i}" class="even">
								<td>
									<g:hiddenField name="relogioId${i}" value="${relogio?.id}" />
									<g:textField name="descricaoRelogio${i}" style="width:90%" value='${relogio?.dscRel}' readOnly="true" />
								</td>
								<td>
									<div id="file-uploader${i}">       
									<noscript>          
									    <p>Please enable JavaScript to use file uploader.</p>
									</noscript>
									</div>
								</td>
								<td>
									<span id="qtLinhas${i}"></span>
								</td>
								<td>
									<span id="sts${i}"></span>
								</td>
							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</div>	
		</fieldset>
		
		<hr>
    
    </g:uploadForm>
    
    <div id="divLogBatidas" style="display: none;" title="Log processamento">
    	<table id="tableResultLog" class="table table-striped table-bordered table-condensed">
    		<thead>
    			<tr>
    				<th>Data batida</th>
    				<th>Data log</th>
    				<th>Funcionário</th>
    				<th>Mensagem</th>
    			</tr>
    		</thead>
    		<tbody>
    		</tbody>
    	</table>
    	
    	<form action="${request.contextPath}/batidasColeta/salvarLog" class="well form-horizontal" method="POST" id="formSalvarLog">
    		<input type="hidden" id="conteudoArquivo" name="conteudoArquivo"/>
    		<button type="submit" class="btn btn-medium btn-primary">Salvar arquivo</button>
    	</form>
    	
    	
    </div>
    
    <script>   
    
        function createUploader(){
            
            for (i=0;i<5;i++) {
                var campo = document.getElementById('file-uploader'+i);
                if (campo != null) {
                	var uploader = new qq.FileUploader({
                        element: campo,
                        action: "${createLink(controller: 'batidasColeta', action: 'uploadImage')}",
                        onSubmit: function(id, fileName){
                            var idUploader = this.element.id;
                            var indice = idUploader.substring(idUploader.length-1);
                        	var relogioId = document.getElementById('relogioId'+indice).value;
                            this.params.relogioId = relogioId;
                        },
                		onComplete: function(id, fileName, responseJSON){
                			var idUploader = this.element.id;
                            var indice = idUploader.substring(idUploader.length-1);
                            var sucesso = responseJSON.success
                            if (sucesso) {
                            	var qtLinhas = document.getElementById('qtLinhas'+indice);
                            	qtLinhas.innerHTML = '<input type=\"text\" readonly=\"true\" value=\"' + responseJSON.quantidadeLinhas + '\" style=\"width:90%\">';

								var sts = document.getElementById('sts'+indice);
								sts.innerHTML = '<a class=\"btn btn-mini btn-success\" href=\"#\">Concluído</a>'
                            	
                            	$j('.qq-upload-success').hide();
                            }
                    	}
                    });
                } else {
                    break;
                }
            }            
        	
            
                       
        }
        
        // in your app create uploader as soon as the DOM is ready
        // don't wait for the window to load  
        window.onload = createUploader;     
    </script>
    <script>
	$(function(){
		$(".check_all").click(function(){
			if($(this).is(":checked")){
				$(".ckeck_act").prop("checked",true);
			}else{
				$(".ckeck_act").prop("checked",false);
			}
		})
	})
    </script>
    
    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

  </body>
</html>