<g:javascript src="classificacao/classificacao.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${classificacaoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${classificacaoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${classificacaoInstance.id}">
		<form action="${request.contextPath}/classificacao/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${classificacaoInstance?.id}" />
			<g:hiddenField name="version" value="${classificacaoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/classificacao/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscClass" maxlength="30" required="" value="${classificacaoInstance?.dscClass}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Classificação Dia:</label>
				<div class="controls">				                                       
					<g:select name="idTpDia" id="idTpDia" optionKey="id" optionValue="nome" 
										value="${classificacaoInstance?.idTpDia}"
										style="width:150px;"
										from="${[ [id:'TR', nome:'Trabalho'],
												  [id:'FG', nome:'Folga'],
												  [id:'DS', nome:'DSR - Domingo'],
												  [id:'CO', nome:'Compensado'],
												  [id:'FE', nome:'Feriado'] ]}"/>				                    
                   								
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Apelido:</label>
				<div class="controls">
					<g:textField name="aplClass" maxlength="80" required="" value="${classificacaoInstance?.aplClass}"style="width:80px"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>