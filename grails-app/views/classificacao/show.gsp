
<%@ page import="cadastros.Classificacao" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'classificacao.label', default: 'Classificacao')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-classificacao" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-classificacao" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list classificacao">
			
				<g:if test="${classificacaoInstance?.dscClass}">
				<li class="fieldcontain">
					<span id="dscClass-label" class="property-label"><g:message code="classificacao.dscClass.label" default="Dsc Class" /></span>
					
						<span class="property-value" aria-labelledby="dscClass-label"><g:fieldValue bean="${classificacaoInstance}" field="dscClass"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${classificacaoInstance?.idTpDia}">
				<li class="fieldcontain">
					<span id="idTpDia-label" class="property-label"><g:message code="classificacao.idTpDia.label" default="Id Tp Dia" /></span>
					
						<span class="property-value" aria-labelledby="idTpDia-label"><g:fieldValue bean="${classificacaoInstance}" field="idTpDia"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${classificacaoInstance?.aplClass}">
				<li class="fieldcontain">
					<span id="aplClass-label" class="property-label"><g:message code="classificacao.aplClass.label" default="Apl Class" /></span>
					
						<span class="property-value" aria-labelledby="aplClass-label"><g:fieldValue bean="${classificacaoInstance}" field="aplClass"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${classificacaoInstance?.id}" />
					<g:link class="edit" action="edit" id="${classificacaoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
