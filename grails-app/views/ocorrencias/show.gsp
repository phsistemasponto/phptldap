
<%@ page import="cadastros.Ocorrencias" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'ocorrencias.label', default: 'Ocorrencias')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-ocorrencias" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-ocorrencias" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list ocorrencias">
			
				<g:if test="${ocorrenciasInstance?.dscOcor}">
				<li class="fieldcontain">
					<span id="dscOcor-label" class="property-label"><g:message code="ocorrencias.dscOcor.label" default="Dsc Ocor" /></span>
					
						<span class="property-value" aria-labelledby="dscOcor-label"><g:fieldValue bean="${ocorrenciasInstance}" field="dscOcor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ocorrenciasInstance?.idTpOcor}">
				<li class="fieldcontain">
					<span id="idTpOcor-label" class="property-label"><g:message code="ocorrencias.idTpOcor.label" default="Id Tp Ocor" /></span>
					
						<span class="property-value" aria-labelledby="idTpOcor-label"><g:fieldValue bean="${ocorrenciasInstance}" field="idTpOcor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ocorrenciasInstance?.formulaOcor}">
				<li class="fieldcontain">
					<span id="formulaOcor-label" class="property-label"><g:message code="ocorrencias.formulaOcor.label" default="Formula Ocor" /></span>
					
						<span class="property-value" aria-labelledby="formulaOcor-label"><g:fieldValue bean="${ocorrenciasInstance}" field="formulaOcor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ocorrenciasInstance?.formulaOcorBh}">
				<li class="fieldcontain">
					<span id="formulaOcorBh-label" class="property-label"><g:message code="ocorrencias.formulaOcorBh.label" default="Formula Ocor Bh" /></span>
					
						<span class="property-value" aria-labelledby="formulaOcorBh-label"><g:fieldValue bean="${ocorrenciasInstance}" field="formulaOcorBh"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ocorrenciasInstance?.indiceExt}">
				<li class="fieldcontain">
					<span id="indiceExt-label" class="property-label"><g:message code="ocorrencias.indiceExt.label" default="Indice Ext" /></span>
					
						<span class="property-value" aria-labelledby="indiceExt-label"><g:fieldValue bean="${ocorrenciasInstance}" field="indiceExt"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ocorrenciasInstance?.indiceBh}">
				<li class="fieldcontain">
					<span id="indiceBh-label" class="property-label"><g:message code="ocorrencias.indiceBh.label" default="Indice Bh" /></span>
					
						<span class="property-value" aria-labelledby="indiceBh-label"><g:fieldValue bean="${ocorrenciasInstance}" field="indiceBh"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ocorrenciasInstance?.tipohoraextras}">
				<li class="fieldcontain">
					<span id="tipohoraextras-label" class="property-label"><g:message code="ocorrencias.tipohoraextras.label" default="Tipohoraextras" /></span>
					
						<span class="property-value" aria-labelledby="tipohoraextras-label"><g:link controller="tipohoraextras" action="show" id="${ocorrenciasInstance?.tipohoraextras?.id}">${ocorrenciasInstance?.tipohoraextras?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${ocorrenciasInstance?.id}" />
					<g:link class="edit" action="edit" id="${ocorrenciasInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
