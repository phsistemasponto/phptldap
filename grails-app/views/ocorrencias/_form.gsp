<g:javascript src="ocorrencias/ocorrencias.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${ocorrenciasInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${ocorrenciasInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${ocorrenciasInstance.id}">
		<form action="${request.contextPath}/ocorrencias/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${ocorrenciasInstance?.id}" />
			<g:hiddenField name="version" value="${ocorrenciasInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/ocorrencias/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscOcor" maxlength="60" required="" value="${ocorrenciasInstance?.dscOcor}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Tipo de Ocorrência:</label>
				<div class="controls">
				   <select name="idTpOcor">
                     <option value="P">Proventos</option>
                     <option value="D">Descontos</option>
                     <option value="A">Adcional Noturno</option>
                     <option value="S">Descanso Semanal</option>
                     
                  </select>					
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Formula da ocorrência:</label>
				<div class="controls">
					<g:textField name="formulaOcor" maxlength="150" value="${ocorrenciasInstance?.formulaOcor}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Formula Banco horas:</label>
				<div class="controls">
					<g:textField name="formulaOcorBh" maxlength="150" value="${ocorrenciasInstance?.formulaOcorBh}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Indice Extra:</label>
				<div class="controls">
					<g:textField name="indiceExt" value="${fieldValue(bean: ocorrenciasInstance, field: 'indiceExt')}"style="width:50px" class="span2"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Indice BH.:</label>
				<div class="controls">
					<g:textField name="indiceBh" value="${fieldValue(bean: ocorrenciasInstance, field: 'indiceBh')}"style="width:50px" class="span2"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Tipo Extra:</label>
				<div class="controls">
					<g:select id="tipohoraextras" name="tipohoraextras.id" from="${cadastros.Tipohoraextras.list()}" optionKey="id" value="${ocorrenciasInstance?.tipohoraextras?.id}" class="many-to-one" noSelection="['null': '']"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>