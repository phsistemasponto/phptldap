
<%@ page import="cadastros.Tipofunc" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipofunc.label', default: 'Tipofunc')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipofunc" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipofunc" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipofunc">
			
				<g:if test="${tipofuncInstance?.dsctpfunc}">
				<li class="fieldcontain">
					<span id="dsctpfunc-label" class="property-label"><g:message code="tipofunc.dsctpfunc.label" default="Dsctpfunc" /></span>
					
						<span class="property-value" aria-labelledby="dsctpfunc-label"><g:fieldValue bean="${tipofuncInstance}" field="dsctpfunc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipofuncInstance?.idtpfunc}">
				<li class="fieldcontain">
					<span id="idtpfunc-label" class="property-label"><g:message code="tipofunc.idtpfunc.label" default="Idtpfunc" /></span>
					
						<span class="property-value" aria-labelledby="idtpfunc-label"><g:fieldValue bean="${tipofuncInstance}" field="idtpfunc"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${tipofuncInstance?.id}" />
					<g:link class="edit" action="edit" id="${tipofuncInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
