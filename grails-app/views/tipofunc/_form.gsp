<g:javascript src="tipofunc/tipofunc.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${tipofuncInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${tipofuncInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${tipofuncInstance.id}">
		<form action="${request.contextPath}/tipofunc/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${tipofuncInstance?.id}" />
			<g:hiddenField name="version" value="${tipofuncInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/tipofunc/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dsctpfunc" maxlength="20" required="" value="${tipofuncInstance?.dsctpfunc}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Tipo Processo:</label>
				<div class="controls">
				<select name="idtpfunc">
					<option value="P">Padrão</option>
					<option value="L">Livre</option>
					<option value="S">Semiflexivel</option>
					<option value="D">Dispensado</option>
				</select>
					
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>