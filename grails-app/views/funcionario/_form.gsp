
	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="funcionario/funcionario.js"/>
	<g:javascript src="filial/filial.js"/>
	<g:javascript src="setor/setor.js"/>
	<g:javascript src="tipofunc/tipofunc.js"/>
	<g:javascript src="cargo/cargo.js"/>
	<g:javascript src="uniorg/uniorg.js"/>
	<g:javascript src="beneficio/beneficio.js"/>
	<g:javascript src="horario/horario.js"/>
	<g:javascript src="comum/lov.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${funcionarioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${funcionarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${funcionarioInstance.id}">
		<form action="${request.contextPath}/funcionario/update" class="well form-horizontal" method="POST" enctype="multipart/form-data">
	</g:if>
	<g:else>
		<form action="${request.contextPath}/funcionario/save" class="well form-horizontal" method="POST" enctype="multipart/form-data">
	</g:else>	
		<fieldset>
			<div class="control-group">

				<label class="control-label">Código:</label>
				<div class="controls">
					<g:textField name="id" value="${funcionarioInstance?.id}" readonly="true" style="width:100px"/>
					&nbsp;					
					Nome:
					<g:textField name="nomFunc" maxlength="80" required="true" value="${funcionarioInstance?.nomFunc}" style="width:300px"/>
					&nbsp;					
					Situação:
					<g:textField name="codStFun" readOnly="true" value="${funcionarioInstance?.situacaoFunc?.dscstfunc}"/>
				</div>
			</div>
						
		    <div class="tabbable"> <!-- Only required for left/right tabs -->
			    <ul class="nav nav-tabs">
				    <li class="active"><a href="#tab1" data-toggle="tab">Dados Pessoais</a></li>
				    <li><a href="#tab2" data-toggle="tab">Dados Contratuais</a></li>
				    <li><a href="#tab3" data-toggle="tab">Horários</a></li>
				    <li><a href="#tab4" data-toggle="tab">Horas extras</a></li>
				    <li><a href="#tab5" data-toggle="tab">Benefícios</a></li>
				    <g:if test="${exibeDocumento}">
				    	<li><a href="#tab6" data-toggle="tab">Documentos/endereços</a></li>
				    </g:if>
			    </ul>
			    <div class="tab-content">
			    
			    	<!--
			    	  --
			    	  -- TAB DADOS PESSOAIS
			    	  -- 
			    	 -->
			    
				    <div class="tab-pane active" id="tab1">
						<div class="control-group">
							<label class="control-label">Matrícula:</label>
							<div class="controls">
								<g:textField name="mtrFunc" maxlength="12" value="${funcionarioInstance?.mtrFunc}"/>
								&nbsp;Crachá:
								<g:textField name="crcFunc" required="" value="${fieldValue(bean: funcionarioInstance, field: 'crcFunc')}"/>
							</div>
						</div>
						
						
						<div class="control-group">
							<label class="control-label">Filial:</label>				
							<div class="controls">
								<g:if test="${!funcionarioInstance?.id}">
									<g:textField name="filialId" maxlength="5" required="" value="${funcionarioInstance?.filial?.id}" style="width:50px" onBlur="buscaIdFilial();"/>&nbsp;
								  	<i class="icon-search" onclick="buscarLovFilial();" style="cursor:pointer" title="Procurar Filial"></i>&nbsp;
								</g:if>
								<g:else>
									<g:textField name="filialId" value="${funcionarioInstance?.filial?.id}" style="width:50px" readonly="true"/>&nbsp;
								  	<i class="icon-search"></i>&nbsp;
								</g:else>
								<g:textField name="filialNome" value="${funcionarioInstance?.filial?.dscFil}" readonly="true" style="width:50%"/>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Departamento:</label>
							<div class="controls">
								<g:if test="${!funcionarioInstance?.id}">
									<g:textField name="setorId" maxlength="5" required="" value="${funcionarioInstance?.setor?.id}" style="width:50px" 
										onBlur="buscaIdSetorComFilial();" disabled="true"/>&nbsp;
								  	<i class="icon-search" onclick="buscarLovSetor();" style="cursor:pointer" title="Procurar Setor" id="setorBotaoBusca"></i>&nbsp;
								</g:if>
								<g:else>
									<g:textField name="setorId" value="${funcionarioInstance?.setor?.id}" style="width:50px" readonly="true"/>&nbsp;
								  	<i class="icon-search"></i>&nbsp;
								</g:else>
								<g:textField name="setorDescricao" value="${funcionarioInstance?.setor?.dscSetor}" readonly="true" style="width:50%"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">Tipo Funcionário:</label>
							<div class="controls">
								<g:textField name="tipoFuncionarioId" maxlength="5" required="" value="${funcionarioInstance?.tipoFunc?.id}" style="width:50px"
									onBlur="buscaIdTipoFuncionario();"/>&nbsp;
							  	<i class="icon-search" onclick="buscarLovTipoFuncionario();" style="cursor:pointer" title="Procurar Setor" 
							  		id="tipoFuncionarioBotaoBusca"></i>&nbsp;
								<g:textField name="tipoFuncionarioDescricao" value="${funcionarioInstance?.tipoFunc?.dsctpfunc}" readonly="true" style="width:50%"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">Cargo:</label>
							<div class="controls">
								<g:if test="${!funcionarioInstance?.id}">
									<g:textField name="cargoId" maxlength="5" required="" value="${funcionarioInstance?.cargo?.id}" style="width:50px"
										onBlur="buscaIdCargo();"/>&nbsp;
								  	<i class="icon-search" onclick="buscarLovCargo();" style="cursor:pointer" title="Procurar Cargo"></i>&nbsp;
							  	</g:if>
							  	<g:else>
							  		<g:textField name="cargoId" value="${funcionarioInstance?.cargo?.id}" style="width:50px" readonly="true"/>&nbsp;
								  	<i class="icon-search"></i>&nbsp;
							  	</g:else>
								<g:textField name="cargoDescricao" value="${funcionarioInstance?.cargo?.dscCargo}" readonly="true" style="width:50%"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">Sub.Uniorg:</label>
							<div class="controls">
								<g:if test="${!funcionarioInstance?.id}">
									<g:textField name="subUniorgId" maxlength="5" value="${funcionarioInstance?.uniorg?.id}" style="width:50px"
										onBlur="buscaIdUniorg();"/>&nbsp;
								  	<i class="icon-search" onclick="buscarLovUniorg();" style="cursor:pointer" title="Procurar Sub.Uniorg"></i>&nbsp;
								</g:if>
								<g:else>
									<g:textField name="subUniorgId" value="${funcionarioInstance?.uniorg?.id}" style="width:50px" readonly="true"/>&nbsp;
								  	<i class="icon-search"></i>&nbsp;
								</g:else>
								<g:textField name="subUniorgDescricao" value="${funcionarioInstance?.uniorg?.dscUniorg}" readonly="true" style="width:50%"/>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Email:</label>
							<div class="controls">
								<g:textField name="emlFunc"   maxlength="100" value="${funcionarioInstance?.emlFunc}"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">PIS:</label>
							<div class="controls">
								<g:textField name="pisFunc" onkeypress="return SomenteNumero(event);" maxlength="15" value="${funcionarioInstance?.pisFunc}"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">Data Nascimento:</label>
							<div class="controls">
								<g:textField name="dataNscFunc"  value="${funcionarioInstance?.dtNascExibicao}" style="width:80px" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Foto:</label>
							<div class="controls" id="controlsFoto">
								<input type="hidden" id="mudouFoto" name="mudouFoto" value="false"/>
								<div style="height: 150px; width: 150px; border: 1px solid black;" id="divFoto">
									<g:if test="${possuiFoto}">
										<img src="${createLink(controller:'funcionario', action:'image', id: funcionarioInstance.id)}"/>
									</g:if>
									<g:else>
										<video id="video" autoplay width="150" height="150" style="background: url(${resource(dir:'images',file:'user.jpg')}) no-repeat center center;"></video>
										<canvas id="canvas" width="0" height="0"></canvas>
										<input type="hidden" id="canvasContent" name="canvasContent"/>
									</g:else>
								</div>
								<g:if test="${!possuiFoto}">
									<button id="snap" type="button">Capturar foto</button><br/>
									<input type="file" id="files" name="files" />
								</g:if>
								<g:else>
									<button id="removFoto" type="button" onclick="removerFoto();">Remover foto</button>
								</g:else>
							</div>
						</div>
						
				    </div>
				    
				    <!--
			    	  --
			    	  -- TAB DADOS CONTRATUAIS
			    	  -- 
			    	 -->
				    
				    <div class="tab-pane" id="tab2">
						<div class="control-group">
							<label class="control-label">Admissão:</label>
							<div class="controls">
								<g:textField name="dataAdm"  value="${funcionarioInstance?.dtAdmExibicao}" style="width:80px" />
								&nbsp;
								<span class="help-inline">Rescisão:</span>
								<g:textField name="dataResc" value="${funcionarioInstance?.dtRescExibicao}" style="width:80px" />
							</div>
						</div>
						<g:if test="${exibeSalario}">
							<div class="control-group">
								<label class="control-label">Vr Sal:</label>
								<div class="controls">
									<g:textField name="vrSal" required="" style="width:80px" value="${funcionarioInstance?.vrSalExibicao}" />
									&nbsp;
									<g:checkBox name="adcNot" value="${funcionarioInstance?.idAdcNot}" 
										checked="${funcionarioInstance?.idAdcNot == 'S'}"/>
									<span class="help-inline">Calcula Adicional Noturno?</span>
								</div>
							</div>
						</g:if>
						<div class="control-group">
							<label class="control-label">(DSR) Descanso Semanal Remunerado:</label>
							<div class="controls">
								<g:textField name="dsrRemunerado" required="" style="width:50px" 
									value="${fieldValue(bean: funcionarioInstance, field: 'dsrRemuneradoExibicao')}"/>
							</div>
						</div>
						<div class="control-group">
							<div class="row show-grid">
								<div class="span4" data-original-title="">
									<label class="checkbox inline">
								    	<g:checkBox name="obrigIntervalo" value="${funcionarioInstance?.idObrigIntervalo}" 
											checked="${funcionarioInstance?.idObrigIntervalo == 'S'}"/> Bate Intervalo para Refeição
								    </label>
								</div>
								<div class="span4" data-original-title="">
									<label class="checkbox inline">
								    	<g:checkBox name="intervFlex" value="${funcionarioInstance?.idIntervFlex}" 
											checked="${funcionarioInstance?.idIntervFlex == 'S'}"/> Intervalo Flexível
								    </label>
								</div>
							</div>
	
							<div class="row show-grid">
								<div class="span4" data-original-title="">
								    <label class="checkbox inline">
								    	<g:checkBox name="sensivelFeriado" value="${funcionarioInstance?.idSensivelFeriado}" 
											checked="${funcionarioInstance?.idSensivelFeriado == 'S'}"/> Sensível a Feriado
								    </label>
								</div>
								<div class="span4" data-original-title="">
								    <label class="checkbox inline">
								    	<g:checkBox name="rrt" value="${funcionarioInstance?.idRrt}" 
											checked="${funcionarioInstance?.idRrt == 'S'}"/> Repouso Remunerado Trabalhado
								    </label>
								</div>
							</div>
						</div>
				    </div>
				    
				    <!--
			    	  --
			    	  -- TAB HORÁRIOS
			    	  -- 
			    	 -->
				    
				    <div class="tab-pane" id="tab3">
				    	<div class="well">
				    	
				    		<div class="control-group">
								<label class="control-label">Horário:</label>				
								<div class="controls">
									<g:textField name="horarioId" maxlength="5" style="width:50px" onBlur="buscaIdHorario();"/>
									&nbsp;
								  	<i class="icon-search" onclick="buscarLovHorario();" style="cursor:pointer" title="Procurar Horário">
								  	</i>
									&nbsp;
									<g:textField name="horarioDescricao" maxlength="450" required="" readOnly="true" style="width:450px"/>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Data início:</label>
								<div class="controls">
									<g:textField name="dataInicioHorario" style="width:80px"/>
									&nbsp;					
									Índice:
									<g:textField name="indiceHorario" style="width:30px" />
									<a id="incHorario" class="btn btn-medium" onClick="incluirHorario();">Incluir</a>
									<a id="viewIndice" class="btn btn-medium" 
										onClick="visualizaIndice('#horarioId', '#dataInicioHorario', '#indiceHorario');">Visualiza o índice</a>
								</div>
							</div>
							
							<div id="horario">
								<table class="table table-striped table-bordered table-condensed" id="horarioTable">
									<thead>
										<tr>
											<th style="width: 30%;">Horário</th>
											<th style="width: 20%;">Data início</th>
											<th style="width: 20%;">Data fim</th>
											<th style="width: 20%;">Índice</th>
											<th style="width: 10%;"></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><input type="hidden" id="sequencialTempHorario" name="sequencialTempHorario" value="0"/></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<g:each in="${horarios?}" status="i" var="horario">
											<tr id="rowTableTempHorario${i+1}" class="even">
												<td>
													<input type="text" id="nomeHorario" name="nomeHorario" value="${horario?.horario?.dscHorario}" readonly style="width:80%" />
													<input type="hidden" id="idHorario" name="idHorario" value="${horario?.horario?.id}"/>
													<input type="hidden" id="sequencialTempHorario" name="sequencialTempHorario" value="${horario?.sequencia}"/>
												</td>
												<td>
													<input type="text" id="dtInicioHorario" name="dtInicioHorario" value="${horario?.dataInicioFormatada}" 
														readonly style="width:80%"/>
												</td>
												<td>
													<input type="text" id="dtFimHorario" name="dtFimHorario" value="${horario?.dataFimFormatada}" readonly style="width:80%"/>
												</td>
												<td>
													<input type="text" id="idcHorario" name="idcHorario" value="${horario?.nrIndHorario}" readonly style="width:80%"/>
												</td>
												<td>
													<g:if test="${i == (horarios.size()-1)}">
														<i class="icon-remove" onclick="excluirHorario(${i+1});" style="cursor: pointer" title="Excluir registro"></i>
													</g:if>
												</td>
											</tr>
										</g:each>
									</tbody>
								</table>
							</div>
							
				    	</div>
				    </div>
				    
				    <!--
			    	  --
			    	  -- TAB HORAS EXTRAS
			    	  -- 
			    	 -->
				    
				    <div class="tab-pane" id="tab4">
				    	<div class="well">
				    	
				    		<div class="control-group">
								<label class="control-label">Configuração hora extra:</label>				
								<div class="controls">
									<g:textField name="configHoraExtraId" maxlength="5" style="width:50px" onBlur="buscaIdConfiguracaoHoraExtra();"/>
									&nbsp;
								  	<i class="icon-search" onclick="buscarLovConfiguracaoHoraExtra();" style="cursor:pointer" title="Procurar Configuração Hora Extra">
								  	</i>
									&nbsp;
									<g:textField name="configuracaoHoraExtraDescricao" maxlength="450" required="" readOnly="true" style="width:450px"/>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Data início:</label>
								<div class="controls">
									<g:textField name="dataInicioConfiguracaoHoraExtra" style="width:80px"/>
									<a id="incConfiguracaoHoraExtra" class="btn btn-medium" onClick="incluirConfiguracaoHoraExtra();">Incluir</a>
								</div>
							</div>
							
							<div id="configHoraExtra">
								<table class="table table-striped table-bordered table-condensed" id="configuracaoHoraExtraTable">
									<thead>
										<tr>
											<th style="width: 30%;">Configuração Hora Extra</th>
											<th style="width: 30%;">Data início</th>
											<th style="width: 30%;">Data fim</th>
											<th style="width: 10%;"></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><input type="hidden" id="sequencialTempConfiguracaoHoraExtra" name="sequencialTempConfiguracaoHoraExtra" value="0"/></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<g:each in="${horasExtras?}" status="i" var="horaExtra">
											<tr id="rowTableTempConfiguracaoHoraExtra${i+1}" class="even">
												<td>
													<input type="text" id="nomeConfiguracaoHoraExtra" name="nomeConfiguracaoHoraExtra" 
														value="${horaExtra?.configHoraExtra?.descricao}" readonly style="width:80%" />
													<input type="hidden" id="idConfiguracaoHoraExtra" name="idConfiguracaoHoraExtra" 
														value="${horaExtra?.configHoraExtra?.id}"/>
													<input type="hidden" id="sequencialTempConfiguracaoHoraExtra" name="sequencialTempConfiguracaoHoraExtra" 
														value="${horaExtra?.sequencia}"/>
												</td>
												<td>
													<input type="text" id="dtInicioConfigHoraExtra" name="dtInicioConfigHoraExtra" value="${horaExtra?.dataInicioFormatada}" 
														readonly style="width:80%"/>
												</td>
												<td>
													<input type="text" id="dtFimConfigHoraExtra" name="dtFimConfigHoraExtra" value="${horaExtra?.dataFimFormatada}" 
														readonly style="width:80%"/>
												</td>
												<td>
													<g:if test="${i == (horasExtras.size()-1)}">
														<i class="icon-remove" onclick="excluirConfiguracaoHoraExtra(${i+1});" style="cursor: pointer" title="Excluir registro"></i>
													</g:if>
												</td>
											</tr>
										</g:each>
									</tbody>
								</table>
							</div>
				    	
				    	</div>
				    </div>
				    
				    <!--
			    	  --
			    	  -- TAB BENEFICIOS
			    	  -- 
			    	 -->
				    
				    <div class="tab-pane" id="tab5">
				    	<div class="well">
				    	
				    		<div class="control-group">
								<label class="control-label">Benefício:</label>				
								<div class="controls">
									<g:textField name="beneficioId" maxlength="5" style="width:50px" onBlur="buscaIdBeneficio();"/>
									&nbsp;
								  	<i class="icon-search" onclick="buscarLovBeneficio();" style="cursor:pointer" title="Procurar Benefício">
								  	</i>
									&nbsp;
									<g:textField name="beneficioDescricao" maxlength="450" required="" readOnly="true" style="width:450px"/>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Data início:</label>
								<div class="controls">
									<g:textField name="dataInicioBeneficio" style="width:80px"/>
									
									&nbsp;
									Quantidade
									<g:textField name="beneficioQuantidade" style="width:80px"/>
									
									&nbsp;
									Valor
									<g:textField name="beneficioValor" style="width:80px" readOnly="true"/>
									
									&nbsp;
									<a id="incBeneficio" class="btn btn-medium" onClick="incluirBeneficio();">Incluir</a>
								</div>
							</div>
							
							<div id="beneficio">
								<table class="table table-striped table-bordered table-condensed" id="configuracaoBeneficioTable">
									<thead>
										<tr>
											<th style="width: 30%;">Benefício</th>
											<th style="width: 20%;">Quantidade</th>
											<th style="width: 20%;">Data início</th>
											<th style="width: 20%;">Data fim</th>
											<th style="width: 10%;"></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><input type="hidden" id="sequencialTempBeneficio" name="sequencialTempBeneficio" value="0"/></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<g:each in="${beneficios?}" status="i" var="beneficio">
											<tr id="rowTableTempBeneficio${i+1}" class="even">
												<td>
													<input type="text" id="nomeBeneficio" name="nomeBeneficio" 
														value="${beneficio?.beneficio?.descricao}" readonly style="width:80%" />
													<input type="hidden" id="idBeneficio" name="idBeneficio" 
														value="${beneficio?.beneficio?.id}"/>
													<input type="hidden" id="sequencialTempBeneficio" name="sequencialTempBeneficio" 
														value="${i+1}"/>
												</td>
												<td>
													<input type="text" id="qtBeneficio" name="qtBeneficio" value="${beneficio?.quantidade}" 
														readonly style="width:80%"/>
												</td>
												<td>
													<input type="text" id="dtInicioBeneficio" name="dtInicioBeneficio" value="${beneficio?.dataInicioFormatada}" 
														readonly style="width:80%"/>
												</td>
												<td>
													<input type="text" id="dtFimBeneficio-${i+1}" name="dtFimBeneficio-${i+1}" value="${beneficio?.dataFimFormatada}" 
														readonly style="width:80%"/>
												</td>
												<td>
													<i class="icon-remove" onclick="excluirBeneficio(${i+1});" style="cursor: pointer" title="Excluir registro"></i>
												</td>
											</tr>
										</g:each>
									</tbody>
								</table>
							</div>
							
				    	</div>
				    </div>
				    
				    <!--
			    	  --
			    	  -- TAB DOCUMENTOS
			    	  -- 
			    	 -->
				    
				    <div class="tab-pane" id="tab6">
				    	<div class="well">
				    		<h5>Carteira de Identidade</h5>
			    			<div class="control-group">
								<label class="control-label">Número:</label>
								<div class="controls">
									<g:textField name="docRgNr" maxlength="15" value="${funcionarioInstance?.docRgNr}"
										style="width:150px;" />&nbsp;
									Org. Exp:
									<g:textField name="docRgEmi" maxlength="15" value="${fieldValue(bean: funcionarioInstance, field: 'docRgEmi')}"
										style="width:150px;"/>&nbsp;
									UF:
									<g:textField name="docRgEmiUf" maxlength="2" value="${fieldValue(bean: funcionarioInstance, field: 'docRgEmiUf')}"
										style="width:50px;"/>&nbsp;
									Emissão:
									<g:textField name="docRgDataEmi" maxlength="2" value="${fieldValue(bean: funcionarioInstance, field: 'docRgDtEmiExibicao')}"
										style="width:80px;"/>&nbsp;
								</div>
							</div>	
				    	</div>
				    	<div class="well">
				    		<h5>Carteira de Trabalho</h5>
			    			<div class="control-group">
								<label class="control-label">Número:</label>
								<div class="controls">
									<g:textField name="docCrtPrf" maxlength="15" value="${funcionarioInstance?.docCrtPrf}"
										style="width:150px;" />&nbsp;
									Série:
									<g:textField name="docCrtPrfSer" maxlength="15" value="${fieldValue(bean: funcionarioInstance, field: 'docCrtPrfSer')}"
										style="width:150px;"/>&nbsp;
									UF:
									<g:textField name="docCrtPrfUf" maxlength="2" value="${fieldValue(bean: funcionarioInstance, field: 'docCrtPrfUf')}"
										style="width:50px;"/>&nbsp; 
									Emissão:
									<g:textField name="docCrtPrfDataEmi" maxlength="2" value="${fieldValue(bean: funcionarioInstance, field: 'docCrtPrfEmiExibicao')}"
										style="width:80px;"/>&nbsp;
								</div>
							</div>	
				    	</div>
						<div class="control-group">
							<label class="control-label">CPF:</label>
							<div class="controls">
								<g:textField name="docCpf" value="${funcionarioInstance?.docCpf}"/>
								&nbsp;
								<span class="help-inline">Registro profissional:</span>
								<g:textField name="docRegPrf" value="${funcionarioInstance?.docRegPrf}"/>
							</div>
						</div>
						<div class="well">
							<h5>Endereço</h5>
							<div class="control-group">
								<label class="control-label">Logradouro:</label>
								<div class="controls">
									<g:textField name="dscLog" value="${funcionarioInstance?.dscLog}" style="width:600px"/>
									&nbsp;
									<span class="help-inline">Nº:</span>
									<g:textField name="nrLog" value="${funcionarioInstance?.nrLog}" style="width:80px"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Complemento:</label>
								<div class="controls">
									<g:textField name="cmpLog" value="${funcionarioInstance?.cmpLog}" style="width:400px"/>
									&nbsp;
									<span class="help-inline">Bairro:</span>
									<g:textField name="baiLog" value="${funcionarioInstance?.baiLog}" style="width:280px"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">CEP:</label>
								<div class="controls">
									<g:textField name="cepLog" value="${funcionarioInstance?.cepLog}" style="width:200px"/>
								</div>
							</div>
						</div>
						<div class="well">
							<h5>Telefones</h5>
							<div class="control-group">
								<label class="control-label">Residencial:</label>
								<div class="controls">
									<g:textField name="fonResFun" value="${funcionarioInstance?.fonResFun}" style="width:200px"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Celular:</label>
								<div class="controls">
									<g:textField name="fonCelFun" value="${funcionarioInstance?.fonCelFun}" style="width:200px"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Comercial:</label>
								<div class="controls">
									<g:textField name="fonComFun" value="${funcionarioInstance?.fonComFun}" style="width:200px"/>
								</div>
							</div>
						</div>
				    	
				    </div>

			    </div>
		    </div>

		</fieldset>
		<hr>
		<g:if test="${params.returnFluxo}">
			<button type="button" class="btn btn-large" onclick="redirectEspelhoPonto();">Retornar</button>
		</g:if>
		<g:else>
			<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		</g:else>
		<g:submitButton name="salvar" class="btn btn-large btn-info" value="${message(code: 'default.button.save.label')}" />
		
		<g:render template="/busca/lovBuscaInterna"/>
		
	</form>
	
	<form class="form-horizontal formEspelhoPonto" action="${request.contextPath}/espelhoPonto/exibir" action="exibir" method="POST" id="formEspelhoPonto">
		<input type="hidden" id="id" name="id" value="${params.funcionarioId}"/>
		<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
		<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
		<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
		<g:each in="${params.keySet()}" status="i" var="p">
	   		<g:if test="${p.startsWith('formPesquisa_')}">
	   			<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
	   		</g:if>
	     </g:each>
	</form>
	
	<g:render template="/horario/lovIndiceHorario"/>
	
	<script type="text/javascript">

		function configComponentFoto() {
			// Grab elements, create settings, etc.
			var canvas = document.getElementById("canvas"),
				context = canvas.getContext("2d"),
				video = document.getElementById("video"),
				videoObj = { "video": true },
				errBack = function(error) {
					console.log("Video capture error: ", error.code); 
				};

			// Put video listeners into place
			if(navigator.getUserMedia) { // Standard
				navigator.getUserMedia(videoObj, function(stream) {
					video.src = stream;
					video.play();
				}, errBack);
			} else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
				navigator.webkitGetUserMedia(videoObj, function(stream){
					video.src = window.webkitURL.createObjectURL(stream);
					video.play();
				}, errBack);
			} else if(navigator.mozGetUserMedia) { // WebKit-prefixed
				navigator.mozGetUserMedia(videoObj, function(stream){
					video.src = window.URL.createObjectURL(stream);
					video.play();
				}, errBack);
			}

			// Trigger photo take
			document.getElementById("snap").addEventListener("click", function() {
				document.getElementById('canvas').width = 150;
				document.getElementById('canvas').height = 150;
				context.drawImage(video, 0, 0, 150, 150);
				document.getElementById('video').remove();
				document.getElementById('canvasContent').value = canvas.toDataURL('image/png');
				document.getElementById('mudouFoto').value = 'true';
			});
		}

		function removerFoto() {
			$j('#divFoto img').remove();
			$j('#removFoto').remove();

			$j('#divFoto').append('<video id=\"video\" autoplay width=\"150\" height=\"150\"></video>');
			$j('#divFoto').append('<canvas id=\"canvas\" width=\"0\" height=\"0\"></canvas>');
			$j('#divFoto').append('<input type=\"hidden\" id=\"canvasContent\" name=\"canvasContent\"/>');

			$j('#controlsFoto').append('<button id=\"snap\" type=\"button\">Capturar foto</button>');
			$j('#controlsFoto').append('<input type=\"file\" id=\"files\" name=\"files\" />');

			$j('#mudouFoto').val('true');

			configComponentFoto();
			
		}

		// Put event listeners into place
		window.addEventListener("DOMContentLoaded", configComponentFoto, false);

	</script>
	
	
	