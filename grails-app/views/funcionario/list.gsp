
<html>
  <head>
    <meta name="layout" content="main">
    <g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Funcionários</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Funcionario']" /></g:link>
    </div>
    
    <form action="${request.contextPath}/funcionario/list" method="POST" id="formPesquisa">
    	<div class="well">
  			<h3>Filtro</h3>
 			<div class="control-group">
				<label class="control-label">Tipo de filtro:</label>				
				<div class="controls">
					<g:select name="tipoFiltro" optionKey="id" optionValue="nome" 
							value="${params.tipoFiltro}"
							style="width:100px;"
							from="${[ [id:'0', nome:'...'],
									  [id:'1', nome:'Matrícula'], 
									  [id:'2', nome:'Nome'], 
									  [id:'3', nome:'Crachá'], 
									  [id:'4', nome:'PIS'] ]}"/>
					<g:textField name="filtro" maxlength="250" value="${params.filtro}" style="margin-left: 10px; width: 400px;"/>
					<input type="hidden" id="order" name="order"/>
					<input type="hidden" id="sortType" name="sortType"/>
					<input type="hidden" id="max" name="max" value="${params.max}"/>
					<input type="hidden" id="offset" name="offset" value="${params.offset}"/>
					<g:submitButton name="filtrar" class="btn btn-medium btn-primary" value="Filtrar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>
           
    <hr/>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>
          	<span>Id</span>
          	<g:if test="${order != 'id'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('id', 'asc')"/></g:if>
          	<g:if test="${order == 'id' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('id', 'desc')"/></g:if>
          	<g:if test="${order == 'id' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('id', 'asc')"/></g:if>
          </th>
          <th>
          	<span>Matrícula</span>
          	<g:if test="${order != 'mtrFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('mtrFunc', 'asc')"/></g:if>
          	<g:if test="${order == 'mtrFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('mtrFunc', 'desc')"/></g:if>
          	<g:if test="${order == 'mtrFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('mtrFunc', 'asc')"/></g:if>
          </th>
          <th>
          	<span>Crachá</span>
          	<g:if test="${order != 'crcFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('crcFunc', 'asc')"/></g:if>
          	<g:if test="${order == 'crcFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('crcFunc', 'desc')"/></g:if>
          	<g:if test="${order == 'crcFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('crcFunc', 'asc')"/></g:if>
          </th>
          <th>
          	<span>Nome</span>
          	<g:if test="${order != 'nomFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('nomFunc', 'asc')"/></g:if>
          	<g:if test="${order == 'nomFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('nomFunc', 'desc')"/></g:if>
          	<g:if test="${order == 'nomFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('nomFunc', 'asc')"/></g:if>
          </th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${funcionarioInstanceList}" status="i" var="funcionarioInstance">
        <tr>
          <td>${funcionarioInstance?.id}</td>
          <td>${funcionarioInstance?.mtrFunc}</td>
          <td>${funcionarioInstance?.crcFunc}</td>
          <td>${funcionarioInstance?.nomFunc}</td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${funcionarioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/funcionario/delete', ${funcionarioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${funcionarioInstanceTotal}" params="${params}"  />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${funcionarioInstanceTotal}</span>
    </div>
    
  </body>
</html>