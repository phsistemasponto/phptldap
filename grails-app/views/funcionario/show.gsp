
<%@ page import="cadastros.Funcionario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'funcionario.label', default: 'Funcionario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-funcionario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-funcionario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list funcionario">
			
				<g:if test="${funcionarioInstance?.mtrFunc}">
				<li class="fieldcontain">
					<span id="mtrFunc-label" class="property-label"><g:message code="funcionario.mtrFunc.label" default="Mtr Func" /></span>
					
						<span class="property-value" aria-labelledby="mtrFunc-label"><g:fieldValue bean="${funcionarioInstance}" field="mtrFunc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.crcFunc}">
				<li class="fieldcontain">
					<span id="crcFunc-label" class="property-label"><g:message code="funcionario.crcFunc.label" default="Crc Func" /></span>
					
						<span class="property-value" aria-labelledby="crcFunc-label"><g:fieldValue bean="${funcionarioInstance}" field="crcFunc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.nomFunc}">
				<li class="fieldcontain">
					<span id="nomFunc-label" class="property-label"><g:message code="funcionario.nomFunc.label" default="Nom Func" /></span>
					
						<span class="property-value" aria-labelledby="nomFunc-label"><g:fieldValue bean="${funcionarioInstance}" field="nomFunc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.idObrigIntervalo}">
				<li class="fieldcontain">
					<span id="idObrigIntervalo-label" class="property-label"><g:message code="funcionario.idObrigIntervalo.label" default="Id Obrig Intervalo" /></span>
					
						<span class="property-value" aria-labelledby="idObrigIntervalo-label"><g:fieldValue bean="${funcionarioInstance}" field="idObrigIntervalo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.docCrtPrf}">
				<li class="fieldcontain">
					<span id="docCrtPrf-label" class="property-label"><g:message code="funcionario.docCrtPrf.label" default="Doc Crt Prf" /></span>
					
						<span class="property-value" aria-labelledby="docCrtPrf-label"><g:fieldValue bean="${funcionarioInstance}" field="docCrtPrf"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.docCrtPrfSer}">
				<li class="fieldcontain">
					<span id="docCrtPrfSer-label" class="property-label"><g:message code="funcionario.docCrtPrfSer.label" default="Doc Crt Prf Ser" /></span>
					
						<span class="property-value" aria-labelledby="docCrtPrfSer-label"><g:fieldValue bean="${funcionarioInstance}" field="docCrtPrfSer"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.emlFunc}">
				<li class="fieldcontain">
					<span id="emlFunc-label" class="property-label"><g:message code="funcionario.emlFunc.label" default="Eml Func" /></span>
					
						<span class="property-value" aria-labelledby="emlFunc-label"><g:fieldValue bean="${funcionarioInstance}" field="emlFunc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.idSensivelFeriado}">
				<li class="fieldcontain">
					<span id="idSensivelFeriado-label" class="property-label"><g:message code="funcionario.idSensivelFeriado.label" default="Id Sensivel Feriado" /></span>
					
						<span class="property-value" aria-labelledby="idSensivelFeriado-label"><g:fieldValue bean="${funcionarioInstance}" field="idSensivelFeriado"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.idAdcNot}">
				<li class="fieldcontain">
					<span id="idAdcNot-label" class="property-label"><g:message code="funcionario.idAdcNot.label" default="Id Adc Not" /></span>
					
						<span class="property-value" aria-labelledby="idAdcNot-label"><g:fieldValue bean="${funcionarioInstance}" field="idAdcNot"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.idIntervFlex}">
				<li class="fieldcontain">
					<span id="idIntervFlex-label" class="property-label"><g:message code="funcionario.idIntervFlex.label" default="Id Interv Flex" /></span>
					
						<span class="property-value" aria-labelledby="idIntervFlex-label"><g:fieldValue bean="${funcionarioInstance}" field="idIntervFlex"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.pisFunc}">
				<li class="fieldcontain">
					<span id="pisFunc-label" class="property-label"><g:message code="funcionario.pisFunc.label" default="Pis Func" /></span>
					
						<span class="property-value" aria-labelledby="pisFunc-label"><g:fieldValue bean="${funcionarioInstance}" field="pisFunc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.cargo}">
				<li class="fieldcontain">
					<span id="cargo-label" class="property-label"><g:message code="funcionario.cargo.label" default="Cargo" /></span>
					
						<span class="property-value" aria-labelledby="cargo-label"><g:link controller="cargo" action="show" id="${funcionarioInstance?.cargo?.id}">${funcionarioInstance?.cargo?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.codStFun}">
				<li class="fieldcontain">
					<span id="codStFun-label" class="property-label"><g:message code="funcionario.codStFun.label" default="Cod St Fun" /></span>
					
						<span class="property-value" aria-labelledby="codStFun-label"><g:fieldValue bean="${funcionarioInstance}" field="codStFun"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.depto}">
				<li class="fieldcontain">
					<span id="depto-label" class="property-label"><g:message code="funcionario.depto.label" default="Depto" /></span>
					
						<span class="property-value" aria-labelledby="depto-label"><g:fieldValue bean="${funcionarioInstance}" field="depto"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.dsrRemunerado}">
				<li class="fieldcontain">
					<span id="dsrRemunerado-label" class="property-label"><g:message code="funcionario.dsrRemunerado.label" default="Dsr Remunerado" /></span>
					
						<span class="property-value" aria-labelledby="dsrRemunerado-label"><g:fieldValue bean="${funcionarioInstance}" field="dsrRemunerado"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.dtAdm}">
				<li class="fieldcontain">
					<span id="dtAdm-label" class="property-label"><g:message code="funcionario.dtAdm.label" default="Dt Adm" /></span>
					
						<span class="property-value" aria-labelledby="dtAdm-label"><g:formatDate date="${funcionarioInstance?.dtAdm}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.dtNscFunc}">
				<li class="fieldcontain">
					<span id="dtNscFunc-label" class="property-label"><g:message code="funcionario.dtNscFunc.label" default="Dt Nsc Func" /></span>
					
						<span class="property-value" aria-labelledby="dtNscFunc-label"><g:formatDate date="${funcionarioInstance?.dtNscFunc}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.dtResc}">
				<li class="fieldcontain">
					<span id="dtResc-label" class="property-label"><g:message code="funcionario.dtResc.label" default="Dt Resc" /></span>
					
						<span class="property-value" aria-labelledby="dtResc-label"><g:formatDate date="${funcionarioInstance?.dtResc}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.filial}">
				<li class="fieldcontain">
					<span id="filial-label" class="property-label"><g:message code="funcionario.filial.label" default="Filial" /></span>
					
						<span class="property-value" aria-labelledby="filial-label"><g:link controller="filial" action="show" id="${funcionarioInstance?.filial?.id}">${funcionarioInstance?.filial?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.horario}">
				<li class="fieldcontain">
					<span id="horario-label" class="property-label"><g:message code="funcionario.horario.label" default="Horario" /></span>
					
						<span class="property-value" aria-labelledby="horario-label"><g:link controller="horario" action="show" id="${funcionarioInstance?.horario?.id}">${funcionarioInstance?.horario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.seqTipFunc}">
				<li class="fieldcontain">
					<span id="seqTipFunc-label" class="property-label"><g:message code="funcionario.seqTipFunc.label" default="Seq Tip Func" /></span>
					
						<span class="property-value" aria-labelledby="seqTipFunc-label"><g:link controller="tipoFunc" action="show" id="${funcionarioInstance?.seqTipFunc?.id}">${funcionarioInstance?.seqTipFunc?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.seqUniorg}">
				<li class="fieldcontain">
					<span id="seqUniorg-label" class="property-label"><g:message code="funcionario.seqUniorg.label" default="Seq Uniorg" /></span>
					
						<span class="property-value" aria-labelledby="seqUniorg-label"><g:fieldValue bean="${funcionarioInstance}" field="seqUniorg"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.setor}">
				<li class="fieldcontain">
					<span id="setor-label" class="property-label"><g:message code="funcionario.setor.label" default="Setor" /></span>
					
						<span class="property-value" aria-labelledby="setor-label"><g:link controller="setor" action="show" id="${funcionarioInstance?.setor?.id}">${funcionarioInstance?.setor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioInstance?.vrSal}">
				<li class="fieldcontain">
					<span id="vrSal-label" class="property-label"><g:message code="funcionario.vrSal.label" default="Vr Sal" /></span>
					
						<span class="property-value" aria-labelledby="vrSal-label"><g:fieldValue bean="${funcionarioInstance}" field="vrSal"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${funcionarioInstance?.id}" />
					<g:link class="edit" action="edit" id="${funcionarioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
