
<%@ page import="cadastros.ParamSistema" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'paramSistema.label', default: 'ParamSistema')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-paramSistema" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-paramSistema" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list paramSistema">
			
				<g:if test="${paramSistemaInstance?.mascaraPeriodo}">
				<li class="fieldcontain">
					<span id="mascaraPeriodo-label" class="property-label"><g:message code="paramSistema.mascaraPeriodo.label" default="Mascara Periodo" /></span>
					
						<span class="property-value" aria-labelledby="mascaraPeriodo-label"><g:fieldValue bean="${paramSistemaInstance}" field="mascaraPeriodo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.mascaraDataBanco}">
				<li class="fieldcontain">
					<span id="mascaraDataBanco-label" class="property-label"><g:message code="paramSistema.mascaraDataBanco.label" default="Mascara Data Banco" /></span>
					
						<span class="property-value" aria-labelledby="mascaraDataBanco-label"><g:fieldValue bean="${paramSistemaInstance}" field="mascaraDataBanco"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.mascaraMoedaBanco}">
				<li class="fieldcontain">
					<span id="mascaraMoedaBanco-label" class="property-label"><g:message code="paramSistema.mascaraMoedaBanco.label" default="Mascara Moeda Banco" /></span>
					
						<span class="property-value" aria-labelledby="mascaraMoedaBanco-label"><g:fieldValue bean="${paramSistemaInstance}" field="mascaraMoedaBanco"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.dtFimcioperiodo}">
				<li class="fieldcontain">
					<span id="dtFimcioperiodo-label" class="property-label"><g:message code="paramSistema.dtFimcioperiodo.label" default="Dt Fimcioperiodo" /></span>
					
						<span class="property-value" aria-labelledby="dtFimcioperiodo-label"><g:fieldValue bean="${paramSistemaInstance}" field="dtFimcioperiodo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.dtInicioperiodo}">
				<li class="fieldcontain">
					<span id="dtInicioperiodo-label" class="property-label"><g:message code="paramSistema.dtInicioperiodo.label" default="Dt Inicioperiodo" /></span>
					
						<span class="property-value" aria-labelledby="dtInicioperiodo-label"><g:fieldValue bean="${paramSistemaInstance}" field="dtInicioperiodo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.evAtraso1}">
				<li class="fieldcontain">
					<span id="evAtraso1-label" class="property-label"><g:message code="paramSistema.evAtraso1.label" default="Ev Atraso1" /></span>
					
						<span class="property-value" aria-labelledby="evAtraso1-label"><g:fieldValue bean="${paramSistemaInstance}" field="evAtraso1"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.evAtraso2}">
				<li class="fieldcontain">
					<span id="evAtraso2-label" class="property-label"><g:message code="paramSistema.evAtraso2.label" default="Ev Atraso2" /></span>
					
						<span class="property-value" aria-labelledby="evAtraso2-label"><g:fieldValue bean="${paramSistemaInstance}" field="evAtraso2"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.evAtrasos}">
				<li class="fieldcontain">
					<span id="evAtrasos-label" class="property-label"><g:message code="paramSistema.evAtrasos.label" default="Ev Atrasos" /></span>
					
						<span class="property-value" aria-labelledby="evAtrasos-label"><g:fieldValue bean="${paramSistemaInstance}" field="evAtrasos"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.evFalta_total}">
				<li class="fieldcontain">
					<span id="evFalta_total-label" class="property-label"><g:message code="paramSistema.evFalta_total.label" default="Ev Faltatotal" /></span>
					
						<span class="property-value" aria-labelledby="evFalta_total-label"><g:fieldValue bean="${paramSistemaInstance}" field="evFalta_total"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.ev_1falta}">
				<li class="fieldcontain">
					<span id="ev_1falta-label" class="property-label"><g:message code="paramSistema.ev_1falta.label" default="Ev1falta" /></span>
					
						<span class="property-value" aria-labelledby="ev_1falta-label"><g:fieldValue bean="${paramSistemaInstance}" field="ev_1falta"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paramSistemaInstance?.ev_2falta}">
				<li class="fieldcontain">
					<span id="ev_2falta-label" class="property-label"><g:message code="paramSistema.ev_2falta.label" default="Ev2falta" /></span>
					
						<span class="property-value" aria-labelledby="ev_2falta-label"><g:fieldValue bean="${paramSistemaInstance}" field="ev_2falta"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${paramSistemaInstance?.id}" />
					<g:link class="edit" action="edit" id="${paramSistemaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
