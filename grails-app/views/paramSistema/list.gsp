
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>ParamSistemas</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['ParamSistema']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Mascara Periodo</th>
		
          <th>Dt Fim periodo</th>
		
          <th>Dt Inicio periodo</th>
		
          <th>Ev Atraso1</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${paramSistemaInstanceList}" status="i" var="paramSistemaInstance">
        <tr>
		
          <td>${fieldValue(bean: paramSistemaInstance, field: "mascPeriodo")}</td>
		
          <td>${fieldValue(bean: paramSistemaInstance, field: "dtFimPeriodo")}</td>
		
          <td>${fieldValue(bean: paramSistemaInstance, field: "dtInicioPeriodo")}</td>
		
          <td>${fieldValue(bean: paramSistemaInstance, field: "ev1Atraso")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${paramSistemaInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/paramSistema/delete', ${paramSistemaInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${paramSistemaInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${paramSistemaInstanceTotal}</span>
    </div>
  </body>
</html>