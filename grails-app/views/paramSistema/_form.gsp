	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="paramSistema/paramSistema.js"/>
	<g:javascript src="ocorrencias/ocorrencias.js"/>
	<g:javascript src="comum/lov.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${paramSistemaInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${paramSistemaInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>
	<fieldset>

	<g:if test="${paramSistemaInstance.id}">
		<form action="${request.contextPath}/paramSistema/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${paramSistemaInstance?.id}" />
			<g:hiddenField name="version" value="${paramSistemaInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/paramSistema/save" class="well form-horizontal" method="POST">
	</g:else>
	
		<div class="tabbable"> <!-- Only required for left/right tabs -->
		    <ul class="nav nav-tabs">
		    	<li class="active"><a href="#tab1" data-toggle="tab">Principal</a></li>
			    <li><a href="#tab2" data-toggle="tab">Períodos</a></li>
			    <li><a href="#tab3" data-toggle="tab">Faltas</a></li>
			    <li><a href="#tab4" data-toggle="tab">Atrasos</a></li>
			    <li><a href="#tab5" data-toggle="tab">Saídas antecipadas</a></li>
				<li><a href="#tab6" data-toggle="tab">DSR (Descanso semanal remunerado)</a></li>
				<li><a href="#tab7" data-toggle="tab">Adicional norturno</a></li>
		    </ul>
         	<div class="tab-content">
         	
         		<!-- Tab principal -->
         		<div class="tab-pane active" id="tab1">				    
    				<div class="control-group">
						<label class="control-label">Intervalo para outra hora:</label>
						<div class="controls">
							<g:textField name="horaIntervaloParaOutraFormatada" maxlength="5" style="width:60px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'horaIntervaloParaOutraFormatada')}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Intervalo para folga:</label>
						<div class="controls">
							<g:textField name="horaParaIntervaloFolgaFormatada" maxlength="5" style="width:60px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'horaParaIntervaloFolgaFormatada')}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento RRT:</label>
						<div class="controls">
							<g:textField name="evRrt" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#evRrt', '#evRrtNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'evRrt')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('evRrt', 'evRrtNome');" style="cursor: pointer" 
								title="Procurar Evento" id="eventoRrtBotaoBusca"></i> 
							&nbsp;
							<g:textField name="evRrtNome" maxlength="450" readOnly="true" style="width:450px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'evRrtNome')}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Quantidade de dias para corrigir as inconsistências:</label>
						<div class="controls">
							<g:textField name="nrDias" value="${fieldValue(bean: paramSistemaInstance, field: 'nrDias')}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento pagamento BH:</label>
						<div class="controls">
							<g:textField name="evPagamentoBh" maxlength="6" style="width:60px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'evPagamentoBh')}"/>
						</div>
					</div>
			    </div>
			    
			    <!-- Tab Períodos -->
			    <div class="tab-pane" id="tab2">				    
    				<div class="control-group">
						<label class="control-label">Inicio do período:</label>
						<div class="controls">
							<g:textField name="dtInicioPeriodo" maxlength="5" style="width:60px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'dtInicioPeriodo')}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Fim do período:</label>
						<div class="controls">
							<g:textField name="dtFimPeriodo" maxlength="5" style="width:60px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'dtFimPeriodo')}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Máscara período:</label>
						<div class="controls">
							<g:textField name="mascPeriodo" maxlength="14" style="width:200px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'mascPeriodo')}"/>
						</div>
					</div>
			    </div>
			    
			    <!-- Tab faltas -->
			    <div class="tab-pane" id="tab3">
			    	<div class="control-group">
						<label class="control-label">Para meia falta considera falta integral?:</label>
						<div class="controls">
							<g:checkBox name="consideraFaltaIntegral" value="${paramSistemaInstance?.consideraFaltaIntegral}" 
								checked="${paramSistemaInstance?.consideraFaltaIntegral == 'S'}" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="evFaltaTotal" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#evFaltaTotal', '#evFaltaTotalNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'evFaltaTotal')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('evFaltaTotal', 'evFaltaTotalNome');" style="cursor: pointer" 
								title="Procurar Evento" id="evFaltaTotalBotaoBusca"></i> 
							&nbsp;
							<g:textField name="evFaltaTotalNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'evFaltaTotalNome')}"/>
							&nbsp;
							<g:checkBox name="checkFaltaTotal" value="${paramSistemaInstance?.checkFaltaTotal}" 
								checked="${paramSistemaInstance?.checkFaltaTotal == 'S'}"
								onClick="habilitaEvento('#checkFaltaTotal', '#evFaltaTotal');"/>
							&nbsp;
							Falta geral
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="ev1Falta" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#ev1Falta', '#ev1FaltaNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev1Falta')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('ev1Falta', 'ev1FaltaNome');" style="cursor: pointer" 
								title="Procurar Evento" id="ev1FaltaBotaoBusca"></i> 
							&nbsp;
							<g:textField name="ev1FaltaNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev1FaltaNome')}"/>
							&nbsp;
							<g:checkBox name="check1Falta" value="${paramSistemaInstance?.check1Falta}" 
								checked="${paramSistemaInstance?.check1Falta == 'S'}"
								onClick="habilitaEvento('#check1Falta', '#ev1Falta');" />
							&nbsp;
							Falta 1 expediente
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="ev2Falta" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#ev2Falta', '#ev2FaltaNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev2Falta')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('ev2Falta', 'ev2FaltaNome');" style="cursor: pointer" 
								title="Procurar Evento" id="ev2FaltaBotaoBusca"></i> 
							&nbsp;
							<g:textField name="ev2FaltaNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev2FaltaNome')}"/>
							&nbsp;
							<g:checkBox name="check2Falta" value="${paramSistemaInstance?.check2Falta}" 
								checked="${paramSistemaInstance?.check2Falta == 'S'}"
								onClick="habilitaEvento('#check2Falta', '#ev2Falta');" />
							&nbsp;
							Falta 2 expediente
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Para falta 2 expediente considerar sempre o evento do 1 expediente?:</label>
						<div class="controls">
							<g:checkBox name="falta2event1" value="${paramSistemaInstance?.falta2event1}" 
								checked="${paramSistemaInstance?.falta2event1 == 'S'}"/>
						</div>
					</div>
			    </div>
			    
			    <!-- Tab atrasos -->
			    <div class="tab-pane" id="tab4">
			    	<div class="control-group">
						<label class="control-label">Descontar atraso pela hora extra?:</label>
						<div class="controls">
							<g:checkBox name="descontarAtrasoPelaExtra" value="${paramSistemaInstance?.descontarAtrasoPelaExtra}" 
								checked="${paramSistemaInstance?.descontarAtrasoPelaExtra == 'S'}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="evAtraso" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#evAtraso', '#evAtrasoNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'evAtraso')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('evAtraso', 'evAtrasoNome');" style="cursor: pointer" 
								title="Procurar Evento" id="evAtrasoBotaoBusca"></i> 
							&nbsp;
							<g:textField name="evAtrasoNome" maxlength="450" readOnly="true" style="width:450px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'evAtrasoNome')}"/>
							&nbsp;
							<g:checkBox name="checkAtraso" value="${paramSistemaInstance?.checkAtraso}" 
								checked="${paramSistemaInstance?.checkAtraso == 'S'}"
								onClick="habilitaEvento('#checkAtraso', '#evAtraso');" />
							&nbsp;
							Atraso geral
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="ev1Atraso" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#ev1Atraso', '#ev1AtrasoNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev1Atraso')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('ev1Atraso', 'ev1AtrasoNome');" style="cursor: pointer" 
								title="Procurar Evento" id="ev1AtrasoBotaoBusca"></i> 
							&nbsp;
							<g:textField name="ev1AtrasoNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev1AtrasoNome')}"/>
							&nbsp;
							<g:checkBox name="check1Atraso" value="${paramSistemaInstance?.check1Atraso}" 
								checked="${paramSistemaInstance?.check1Atraso == 'S'}"
								onClick="habilitaEvento('#check1Atraso', '#ev1Atraso');" />
							&nbsp;
							Atraso 1 expediente
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="ev2Atraso" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#ev2Atraso', '#ev2AtrasoNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev2Atraso')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('ev2Atraso', 'ev2AtrasoNome');" style="cursor: pointer" 
								title="Procurar Evento" id="ev2AtrasoBotaoBusca"></i> 
							&nbsp;
							<g:textField name="ev2AtrasoNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev2AtrasoNome')}"/>
							&nbsp;
							<g:checkBox name="check2Atraso" value="${paramSistemaInstance?.check2Atraso}" 
								checked="${paramSistemaInstance?.check2Atraso == 'S'}"
								onClick="habilitaEvento('#check2Atraso', '#ev2Atraso');" />
							&nbsp;
							Atraso 2 expediente
						</div>
					</div>
			    </div>
			    
			    <!-- Tab saidas antecipadas -->
			    <div class="tab-pane" id="tab5">
			    	<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="evSaida" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#evSaida', '#evSaidaNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'evSaida')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('evSaida', 'evSaidaNome');" style="cursor: pointer" 
								title="Procurar Evento" id="evSaidaBotaoBusca"></i> 
							&nbsp;
							<g:textField name="evSaidaNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'evSaidaNome')}"/>
							&nbsp;
							<g:checkBox name="checkSaida" value="${paramSistemaInstance?.checkSaida}" 
								checked="${paramSistemaInstance?.checkSaida == 'S'}"
								onClick="habilitaEvento('#checkSaida', '#evSaida');" />
							&nbsp;
							Saída antecipada geral
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="ev1Saida" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#ev1Saida', '#ev1SaidaNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev1Saida')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('ev1Saida', 'ev1SaidaNome');" style="cursor: pointer" 
								title="Procurar Evento" id="ev1SaidaBotaoBusca"></i> 
							&nbsp;
							<g:textField name="ev1SaidaNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev1SaidaNome')}"/>
							&nbsp;
							<g:checkBox name="check1Saida" value="${paramSistemaInstance?.check1Saida}" 
								checked="${paramSistemaInstance?.check1Saida == 'S'}"
								onClick="habilitaEvento('#check1Saida', '#ev1Saida');"/>
							&nbsp;
							Saída antecipada 1 expediente
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento</label>
						<div class="controls">
							<g:textField name="ev2Saida" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#ev2Saida', '#ev2SaidaNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev2Saida')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('ev2Saida', 'ev2SaidaNome');" style="cursor: pointer" 
								title="Procurar Evento" id="ev2SaidaBotaoBusca"></i> 
							&nbsp;
							<g:textField name="ev2SaidaNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'ev2SaidaNome')}"/>
							&nbsp;
							<g:checkBox name="check2Saida" value="${paramSistemaInstance?.check2Saida}" 
								checked="${paramSistemaInstance?.check2Saida == 'S'}"
								onClick="habilitaEvento('#check2Saida', '#ev2Saida');" />
							&nbsp;
							Saída antecipada 2 expediente
						</div>
					</div>
			    </div>
			    
			    <!-- Tab DSR -->
			    <div class="tab-pane" id="tab6">
			    	<div class="control-group">
						<label class="control-label">Evento Provento</label>
						<div class="controls">
							<g:textField name="evDsr" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#evDsr', '#evDsrNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'evDsr')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('evDsr', 'evDsrNome');" style="cursor: pointer" 
								title="Procurar Evento" id="evDsrBotaoBusca"></i> 
							&nbsp;
							<g:textField name="evDsrNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'evDsrNome')}"/>
							&nbsp;
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Evento desconto</label>
						<div class="controls">
							<g:textField name="evDsrd" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#evDsrd', '#evDsrdNome');"
								value="${fieldValue(bean: paramSistemaInstance, field: 'evDsrd')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('evDsrd', 'evDsrdNome');" style="cursor: pointer" 
								title="Procurar Evento" id="evDsrdBotaoBusca"></i> 
							&nbsp;
							<g:textField name="evDsrdNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'evDsrdNome')}"/>
							&nbsp;
						</div>
					</div>
			    </div>
			    
			    <!-- Tab Adicional noturno -->
			    <div class="tab-pane" id="tab7">
			    	<div class="control-group">
						<label class="control-label">Evento adicional</label>
						<div class="controls">
							<g:textField name="evAdicionalNoturno" maxlength="5" style="width:50px" onBlur="buscaIdOcorrencia('#evAdicionalNoturno', '#evAdicionalNoturnoNome');" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'evAdicionalNoturno')}"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovOcorrencia('evAdicionalNoturno', 'evAdicionalNoturnoNome');" style="cursor: pointer" 
								title="Procurar Evento" id="evAdicionalNoturnoBotaoBusca"></i> 
							&nbsp;
							<g:textField name="evAdicionalNoturnoNome" maxlength="450" readOnly="true" style="width:450px"
								value="${fieldValue(bean: paramSistemaInstance, field: 'evAdicionalNoturnoNome')}"/>
							&nbsp;
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Hora início:</label>
						<div class="controls">
							<g:textField name="horaInicioAdicionalNoturnoFormatada" maxlength="5" style="width:60px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'horaInicioAdicionalNoturnoFormatada')}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Hora fim:</label>
						<div class="controls">
							<g:textField name="horaFimAdicionalNoturnoFormatada" maxlength="5" style="width:60px" 
								value="${fieldValue(bean: paramSistemaInstance, field: 'horaFimAdicionalNoturnoFormatada')}"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Cálculo do adicional noturno normal?:</label>
						<div class="controls">
							<g:checkBox name="adicionalNoturnoNormal" value="${paramSistemaInstance?.adicionalNoturnoNormal}" 
								checked="${paramSistemaInstance?.adicionalNoturnoNormal == 'S'}"/>
						</div>
					</div>
			    </div>
			    
			</div>
			
		</div>

		</fieldset>	
		
		<hr>
		<h4>Filiais</h4>
		<br />
		<div>
			<g:if test="${paramSistemaInstance.id}">
				<table class="table table-striped table-bordered table-condensed"
					id="filiaisTable">
					<thead>
						<tr>
							<th style="width: 50%;">Filial</th>
							<th style="width: 10%;">Existe </th>
							<th style="width: 40%;">&nbsp; <button class="btn" type="button" onClick="marcarTodos();">Marcar Todos</button> <button class="btn" type="button" onClick="desmarcarTodos();">Desmarcar Todos</button></th>
						</tr>
					</thead>
	
					<tbody>
						<g:each in="${filiais?}" status="i" var="item">
							<tr id="rowTableFilial${i}" class="even">
								<td><g:hiddenField name="filialId" value="${item?.filial?.id}" />
									<g:hiddenField name="temFilialPeriodo" value="${item?.temFilial}" />
									<g:if test="${item?.temFilial=='true' }">
										<g:hiddenField name="statusPeriodo" value="${item?.statusPeriodo}" />
									</g:if>
									<g:else>
										<g:hiddenField name="statusPeriodo" value="X" />
									</g:else>
									<g:textField name="descricaoFilial" style="width:90%" value='${item?.filial?.dscFil+" - "+item?.filial?.unicodFilial}' readOnly="true" />
								</td>
								
								<td>
									<g:if test="${item?.temFilial=='true' }">
										<input type='checkbox' name="temFilial${i}" value='true' checked="checked"  onChange="atualizaFilialBotao(${i});"/>
									</g:if>
									<g:else>
										<input type='checkbox' name="temFilial${i}" value='false'  onChange="atualizaFilialBotao(${i});"/>
									</g:else>
								</td>
								<td/>
							</tr>
						</g:each>
					</tbody>
				</table>
			
			</g:if>
			<g:else>
				<table class="table table-striped table-bordered table-condensed"
					id="filiaisTable">
					<thead>
						<tr>
							<th style="width: 50%;">Filial</th>
							<th style="width: 10%;">Existe </th>
							<th style="width: 40%;">&nbsp; <button class="btn" type="button" onClick="marcarTodos();">Marcar Todos</button> <button class="btn" type="button" onClick="desmarcarTodos();">Desmarcar Todos</button></th>
						</tr>
					</thead>
	
					<tbody>
						<g:each in="${filiais?}" status="i" var="item">
							<tr id="rowTableFilial${i}" class="even">
								<td><g:hiddenField name="filialId" value="${item?.filial?.id}" />
									<g:hiddenField name="temFilialPeriodo" value="${item?.temFilial}" />
									<g:textField name="descricaoFilial" style="width:90%" value='${item?.filial?.dscFil+" - "+item?.filial?.unicodFilial}' readOnly="true" />
								</td>
								<td><g:checkBox name="temFilial${i}" value="${item?.temFilial}" onChange="atualizaFilial(${i});"/></td>
								<td/>
							</tr>
						</g:each>
					</tbody>
				</table>
			</g:else>
		</div>
		<hr>
    
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		
		<g:render template="/busca/lovBuscaInterna"/>
		
	</form>