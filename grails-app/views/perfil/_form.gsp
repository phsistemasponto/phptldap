<g:javascript src="perfil/perfil.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${perfilInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${perfilInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${perfilInstance.id}">
		<form action="${request.contextPath}/perfil/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${perfilInstance?.id}" />
			<g:hiddenField name="version" value="${perfilInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/perfil/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Authority:</label>
				<div class="controls">
					<g:textField name="authority" required="" value="${perfilInstance?.authority}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Texto Exibição:</label>
				<div class="controls">
					<g:textField name="textoExibicao" required="" value="${perfilInstance?.textoExibicao}" maxSize="50" style="width: 300px;" />
				</div>
			</div>			

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>