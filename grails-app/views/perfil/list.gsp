
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Perfil do Menu</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Perfil']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Authority</th>
		
          <th>Texto Exibicao</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${perfilInstanceList}" status="i" var="perfilInstance">
        <tr>
		
          <td>${fieldValue(bean: perfilInstance, field: "authority")}</td>
		
          <td>${fieldValue(bean: perfilInstance, field: "textoExibicao")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${perfilInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/perfil/delete', ${perfilInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${perfilInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${perfilInstanceTotal}</span>
    </div>
  </body>
</html>