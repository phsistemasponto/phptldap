	<g:javascript src="horario/horario.js"/>
	<g:javascript src="comum/lov.js"/>
	<g:javascript src="jquery.meiomask.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${horarioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${horarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${horarioInstance.id}">
		<form action="${request.contextPath}/horario/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${horarioInstance?.id}" />
			<g:hiddenField name="version" value="${horarioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/horario/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>
			<g:hiddenField name="sequencia"  value="${sequencia}" />
			
			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscHorario" maxlength="80" required="" value="${horarioInstance?.dscHorario}"/>
				</div>
			</div>

			<g:if test="${horarioInstance.id}">
				<div class="control-group">
					<label class="control-label">Data Início:</label>
					<div class="controls">				     
						<g:textField name="dtIniVig"  value="${horarioInstance?.dtIniVigFormatada}"  readonly="true" disabled="true"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Carga Horária:</label>
					<div class="controls">
						<g:textField name="cargaHorariaTotalHoras" maxlength="80" required="" value="${horarioInstance?.cargaHorariaTotalHoras}" readOnly="true"/>
						<g:hiddenField name="cargaHorariaTotal"  value="${horarioInstance?.cargaHorariaTotal}"/>
					</div>
				</div>
				
			</g:if>
			<g:else>
				<div class="control-group">
					<label class="control-label">Data Início:</label>
					<div class="controls">				     
						<g:textField name="dtIniVig"  value="${horarioInstance?.dtIniVigFormatada}" value="05/01/2015" />
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Carga Horária:</label>
					<div class="controls">
						<g:textField name="cargaHorariaTotalHoras" maxlength="80" required="" value="00:00" readOnly="true"/>
						<g:hiddenField name="cargaHorariaTotal"  value="0"/>
					</div>
				</div>
				
			</g:else>
			
						
			

		</fieldset>
		
		<hr />
		<h4>Itens do Horário</h4>
		<br/>

		<div class="control-group">
			<label class="control-label">Classificação:</label>
			<div class="controls">			
				<g:select name="classificacao" from="${cadastros.Classificacao.list(sort: 'dscClass')}" optionKey="id" optionValue="dscClass" style="width:200px" onChange="habilitarHorario();"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Horário:</label>
			<div class="controls">
				<g:textField name="horarioId" maxlength="5" value="" style="width:50px" onBlur="buscaIdPerfilHorario();" />
				<i class="icon-search" onclick="buscarLovPerfilHorario();" style="cursor: pointer" title="Procurar Horário" id="itemHorarioBotaoBusca"></i> &nbsp;
				<g:textField name="horarioDescricao" maxlength="450" value="" readOnly="true" style="width:450px" />
				<g:hiddenField name="horarioCargaHoraria" />	
				<g:hiddenField name="horarioCargaHorariaHoras" />
				<a class="btn btn-medium" onClick="incluirItemHorario();">Incluir</a>
			</div>	
		</div>
		
		<div id="itemhorario">
			<table class="table table-striped table-bordered table-condensed"
				id="itemHorarioTable">
				<thead>
					<tr>
							<th style="width: 10%;">Sequência</th>
							<th style="width: 10%;">Dia</th>
							<th style="width: 30%;">Horário</th>
							<th style="width: 30%;">Classificação</th>
							<th style="width: 10%;">Carga Horária</th>
							<th style="width: 10%;"></th>
					</tr>
				</thead>
				<tbody>
						<g:each in="${itemHorarios?}" status="i" var="item">
							<tr id="rowTableItemHorario${i+1}" class="even">
								<td><g:textField name="seqhorario" style="width:90%"
										value="${item?.seqitemhorario}" readOnly="true"/></td>
								<td><g:hiddenField name="dataOcorrencia" value="${item?.dtinivigFormatada}" />
									<g:hiddenField name="nrDiaSemana" value="${item?.nrdiasemana}" />
									<g:textField name="diaSemana" style="width:90%" value="${item?.diaSemana}" readOnly="true"/></td>
								<td><g:hiddenField name="idPerfilhorario" value="${item?.perfilhorario?.id}" />
									<g:textField name="dscPerfilhorario" style="width:90%" value="${item?.perfilhorario?.dscperfil}" readOnly="true"/>
								</td>
								<td><g:hiddenField name="idClassificacao" value="${item?.classificacao?.id}" />
									<g:textField name="dscClassificacao" style="width:90%"
										value="${item?.classificacao?.dscClass}" readOnly="true" />	
								</td>
								<td><g:hiddenField name="cargaHoraria" value="${item?.qtdechefetiva}" />
									<g:textField name="cargaHorariaHoras" style="width:90%" value="${item?.cargaHorariaHoras}" readOnly="true" />
								</td>
								<td>
									<g:if test="${i == quantidadeRegistros}">
										<i class="icon-remove" onclick="excluirItemHorario(${i+1});" style="cursor: pointer" title="Excluir registro"></i>
									</g:if>
									<g:else>
									</g:else>
								</td>
							</tr>
						</g:each>
				
				</tbody>
			</table>
		</div>			
		
		<hr>
		
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		
		<g:render template="/busca/lovBuscaInterna"/>
		
	</form>