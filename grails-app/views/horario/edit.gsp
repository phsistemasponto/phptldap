<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
    <h2><g:message code="default.edit.label" args="['Horario']" /></h2>
	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="comum/lov.js"/>
	<g:javascript src="horario/horario.js"/>
    <g:render template="form"/>
  </body>
</html>