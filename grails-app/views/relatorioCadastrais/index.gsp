
<%@page import="cadastros.Ocorrencias"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    <g:javascript src="reports/cadastrais.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Relatórios cadastrais</h3>
    </div>    
    <g:jasperForm controller="relatorioCadastrais" action="generateReport" jasper="cadastrais" class="form-horizontal" id="formPesquisa">
    
    	<div class="well">
    	
    		<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Relatório:</label>
				<div class="controls">
					<g:select name="relatorio" id="relatorio" optionKey="id" optionValue="nome" 
							value="${params.relatorio}"
							style="width:200px;"
							from="${[ [id:'0', nome:'Filial'],
									  [id:'1', nome:'Setor'],
									  [id:'2', nome:'Cargo'],
									  [id:'3', nome:'Justificativas']  ]}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Ordenação:</label>
				<div class="controls">
					<g:select name="ordenacao" id="ordenacao" optionKey="id" optionValue="nome" 
							value="${params.ordenacao}"
							style="width:200px;"
							from="${[ [id:'1', nome:'Código'],
									  [id:'2', nome:'Descrição'] ]}"/>
				</div>
			</div>
				    
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('PDF'); else return false;">Gerar em PDF</button>
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('XLS'); else return false;">Gerar em Excel</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

	<div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>