<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<title>PHPONTO - Sistema de Controle de Ponto Eletrônico</title>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="stylesheet" href="${resource(dir:'css',file:'bootstrap.css')}" />
		<link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
		<link rel="stylesheet" href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
		<link rel="stylesheet" href="${resource(dir:'css',file:'jquery-ui-1.8.23.custom.css')}" />
		<link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-ico" />
		<g:layoutHead />
	</head>
	<script type="text/javascript" src="${resource(dir:'js',file:'jquery-1.8.0.min.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'js',file:'jquery-ui-1.8.23.custom.min.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'js',file:'jquery.ui.mouse.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'js',file:'jquery.ui.draggable.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'js',file:'bootstrap.min.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'js',file:'application.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'js',file:'calculadora.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'js',file:'shortcut.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'js/ui/i18n',file:'jquery.ui.datepicker-pt-BR.js')}"></script>
	<script type="text/javascript">var context = "${request.contextPath}";</script>
	<body>	
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner_topo">
				<div class="container-fluid">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</a>
					<div class="logo_phponto">
						<span class="logo_img"><img src="${resource(dir:'images',file:'logoPHponto.png')}" /></span>
						<span class="logo_txt"><a class="brand" href="${request.contextPath}/"><img src="${resource(dir:'images',file:'phpontowebtexto.png')}" /></a></span>
						
						
						
					</div>
					<div class="nav-collapse">
						<!--  <ul class="nav">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="#about">Sobre</a></li>
						<li><a href="#contact">Contato</a></li>
						<li><g:link controller="logout" action="index">Logout
						<i class="icon-remove" style="cursor:pointer" title="Logout"></i>
						</g:link>
						</li>
						</ul>-->
						<p class="navbar-text pull-right bem_vindo">
								Bem vindo, <a href="#" class="name_user"><g:usuario/></a> - <g:link controller="logout" action="index">Sair</g:link><br/>
								<span>Último login: ${session?.ultimoLogin}</span><br/>								
						</p>
						<p class="pull-right bem_vindo_sair">
								<g:link controller="logout" action="index" class="btn btn-primary">Sair <i class="icon-remove icon-white"></i></g:link>
																
						</p>						
					</div>
				</div>
			</div>
			
			<sec:ifNotGranted roles="ROLE_ESPELHO_PONTO_FUNCIONARIO">
				<div class="navbar navbar-static" id="navbarExample" style="margin-bottom: 0px;">
					<div style="background-color: #00577f !important;box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.62);">
						<div class="container-fluid">
							<div style="width: auto;" class="container">
								<g:menuApp/>
							</div>
						</div>
					</div>
				</div>
				<div class="navbar navbar-static" id="navbarExample">
					<div style="background-color: #fff !important;">
						<div class="container-fluid">
							<div style="width: auto;" class="container">
								<g:atalhosApp/>
							</div>
						</div>
					</div>
				</div>
			</sec:ifNotGranted>
			
		</div>
        <div id="corpo" class="container corpoComun">
        
        	<g:alertasApp />
        
        	<g:layoutBody />
        	
        	<g:hotkeysApp/>
        	
			<hr>
			<footer>
				<div class=".row-fluid">
					<div class="span10"><p>Copyright&copy; 2012 - Todos os direitos reservados</p></div>
					<div class="span1"><img src="${resource(dir:'images',file:'ph.gif')}" class="ph"/></div>
				</div>
			</footer>
			
			<div id="divCalculadora" style="display:none;">
				<table>
				   <tr>
				      <td align="center">
				         <input type="checkbox" id="resultDecimal"/> Resultado em decimal  
				      </td>
				   </tr>
				   <tr>
				      <td align="center" valign="middle">
				      	<textarea rows="1" cols="1" class="displayCalc" id="displayCalc"></textarea>
				      	<button type="button" id="calcTecla1" class="btn btn-small btnCalc" onclick="clickBtnCalc('1');"
				      		style="font-size: 30px;font-weight: 900;">1</button>
				      	<button type="button" id="calcTecla2" class="btn btn-small btnCalc" onclick="clickBtnCalc('2');"
				      		style="font-size: 30px;font-weight: 900;">2</button>
				      	<button type="button" id="calcTecla3" class="btn btn-small btnCalc" onclick="clickBtnCalc('3');"
				      		style="font-size: 30px;font-weight: 900;">3</button>
				      	<button type="button" id="calcTeclaPlus" class="btn btn-small btnCalc btn-inverse" onclick="clickBtnCalcOper('+');" 
				      		style="font-size: 40px;font-weight: 900;padding-bottom: 12px;">+</button>
				      	<div style="margin-top: 5px;">
				      	<button type="button" id="calcTecla4" class="btn btn-small btnCalc" onclick="clickBtnCalc('4');"
				      		style="font-size: 30px;font-weight: 900;">4</button>
				      	<button type="button" id="calcTecla5" class="btn btn-small btnCalc" onclick="clickBtnCalc('5');"
				      		style="font-size: 30px;font-weight: 900;">5</button>
				      	<button type="button" id="calcTecla6" class="btn btn-small btnCalc" onclick="clickBtnCalc('6');"
				      		style="font-size: 30px;font-weight: 900;">6</button>
				      	<button type="button" id="calcTeclaMinus" class="btn btn-small btnCalc btn-inverse" onclick="clickBtnCalcOper('-');"
				      		style="font-size: 40px;font-weight: 900;padding-bottom: 12px;">-</button>
				      	</div>
				      	<div style="margin-top: 5px;">
				      	<button type="button" id="calcTecla7" class="btn btn-small btnCalc" onclick="clickBtnCalc('7');"
				      		style="font-size: 30px;font-weight: 900;">7</button>
				      	<button type="button" id="calcTecla8" class="btn btn-small btnCalc" onclick="clickBtnCalc('8');"
				      		style="font-size: 30px;font-weight: 900;">8</button>
				      	<button type="button" id="calcTecla9" class="btn btn-small btnCalc" onclick="clickBtnCalc('9');"
				      		style="font-size: 30px;font-weight: 900;">9</button>
				      	<button type="button" id="calcTeclaMult" value="*" class="btn btn-small btnCalc btn-inverse" onclick="clickBtnCalcOper('*');"
				      		style="font-size: 40px;font-weight: 900;padding-top: 20px;">*</button>
				      	</div>
				      	<div style="margin-top: 5px;">
				      	<button type="button" id="calcTeclaInativa1" class="btn btn-small btnCalc btn-info" onclick="limparCalculadora();"
				      		style="font-size: 30px;font-weight: 900;">C</button>
				      	<button type="button" id="calcTecla0" class="btn btn-small btnCalc" onclick="clickBtnCalc('0');"
				      		style="font-size: 30px;font-weight: 900;">0</button>
				      	<button type="button" id="calcTeclaSeparator" class="btn btn-small btnCalc btn-inverse" onclick="clickBtnCalc(':');"
				      		style="font-size: 30px;font-weight: 900;">:</button>
				      	<button type="button" id="calcTeclaEquals" class="btn btn-small btnCalc btn-warning" onclick="clickBtnCalcResult();"
				      		style="font-size: 30px;font-weight: 900;padding-bottom: 12px;">=</button>
				      	</div>
				      	  
				      </td>
				   </tr>
				</table>
			</div>
        </div>
        
    	<div id="spinner" class="spinner" style="display:none;">Carregando...</div>
	</body>
</html>