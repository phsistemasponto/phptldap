

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${feriadosNacionaisInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${feriadosNacionaisInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${feriadosNacionaisInstance.id}">
		<form action="${request.contextPath}/feriadosNacionais/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${feriadosNacionaisInstance?.id}" />
			<g:hiddenField name="version" value="${feriadosNacionaisInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/feriadosNacionais/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Dia:</label>
				<div class="controls">
					<g:textField name="dia" required="" value="${fieldValue(bean: feriadosNacionaisInstance, field: 'dia')}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Mês:</label>
				<div class="controls">
					<g:textField name="mes" required="" value="${fieldValue(bean: feriadosNacionaisInstance, field: 'mes')}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscFer" maxlength="80" required="" value="${feriadosNacionaisInstance?.dscFer}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>