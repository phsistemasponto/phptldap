
<%@ page import="cadastros.FeriadosNacionais" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'feriadosNacionais.label', default: 'FeriadosNacionais')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-feriadosNacionais" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-feriadosNacionais" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list feriadosNacionais">
			
				<g:if test="${feriadosNacionaisInstance?.dscFer}">
				<li class="fieldcontain">
					<span id="dscFer-label" class="property-label"><g:message code="feriadosNacionais.dscFer.label" default="Dsc Fer" /></span>
					
						<span class="property-value" aria-labelledby="dscFer-label"><g:fieldValue bean="${feriadosNacionaisInstance}" field="dscFer"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${feriadosNacionaisInstance?.dia}">
				<li class="fieldcontain">
					<span id="dia-label" class="property-label"><g:message code="feriadosNacionais.dia.label" default="Dia" /></span>
					
						<span class="property-value" aria-labelledby="dia-label"><g:fieldValue bean="${feriadosNacionaisInstance}" field="dia"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${feriadosNacionaisInstance?.mes}">
				<li class="fieldcontain">
					<span id="mes-label" class="property-label"><g:message code="feriadosNacionais.mes.label" default="Mes" /></span>
					
						<span class="property-value" aria-labelledby="mes-label"><g:fieldValue bean="${feriadosNacionaisInstance}" field="mes"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${feriadosNacionaisInstance?.id}" />
					<g:link class="edit" action="edit" id="${feriadosNacionaisInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
