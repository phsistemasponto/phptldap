
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>FiltroUsuarios</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['FiltroUsuario']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Filtro Cargo</th>
		
          <th>Filtro Departamento</th>
		
          <th>Filtro Filial</th>
		
          <th>Filtro Funcionario</th>
		
          <th>Filtro Horario</th>
		
          <th>Filtro Sub Uniorg</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${filtroUsuarioInstanceList}" status="i" var="filtroUsuarioInstance">
        <tr>
		
          <td>${fieldValue(bean: filtroUsuarioInstance, field: "filtroCargo")}</td>
		
          <td>${fieldValue(bean: filtroUsuarioInstance, field: "filtroDepartamento")}</td>
		
          <td>${fieldValue(bean: filtroUsuarioInstance, field: "filtroFilial")}</td>
		
          <td>${fieldValue(bean: filtroUsuarioInstance, field: "filtroFuncionario")}</td>
		
          <td>${fieldValue(bean: filtroUsuarioInstance, field: "filtroHorario")}</td>
		
          <td>${fieldValue(bean: filtroUsuarioInstance, field: "filtroSubUniorg")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${filtroUsuarioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/filtrousuario/delete', ${filtroUsuarioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${filtroUsuarioInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${filtroUsuarioInstanceTotal}</span>
    </div>
  </body>
</html>