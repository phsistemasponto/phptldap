
<%@ page import="cadastros.FiltroUsuario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'filtroUsuario.label', default: 'FiltroUsuario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-filtroUsuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-filtroUsuario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list filtroUsuario">
			
				<g:if test="${filtroUsuarioInstance?.filtroCargo}">
				<li class="fieldcontain">
					<span id="filtroCargo-label" class="property-label"><g:message code="filtroUsuario.filtroCargo.label" default="Filtro Cargo" /></span>
					
						<span class="property-value" aria-labelledby="filtroCargo-label"><g:fieldValue bean="${filtroUsuarioInstance}" field="filtroCargo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filtroUsuarioInstance?.filtroDepartamento}">
				<li class="fieldcontain">
					<span id="filtroDepartamento-label" class="property-label"><g:message code="filtroUsuario.filtroDepartamento.label" default="Filtro Departamento" /></span>
					
						<span class="property-value" aria-labelledby="filtroDepartamento-label"><g:fieldValue bean="${filtroUsuarioInstance}" field="filtroDepartamento"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filtroUsuarioInstance?.filtroFilial}">
				<li class="fieldcontain">
					<span id="filtroFilial-label" class="property-label"><g:message code="filtroUsuario.filtroFilial.label" default="Filtro Filial" /></span>
					
						<span class="property-value" aria-labelledby="filtroFilial-label"><g:fieldValue bean="${filtroUsuarioInstance}" field="filtroFilial"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filtroUsuarioInstance?.filtroFuncionario}">
				<li class="fieldcontain">
					<span id="filtroFuncionario-label" class="property-label"><g:message code="filtroUsuario.filtroFuncionario.label" default="Filtro Funcionario" /></span>
					
						<span class="property-value" aria-labelledby="filtroFuncionario-label"><g:fieldValue bean="${filtroUsuarioInstance}" field="filtroFuncionario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filtroUsuarioInstance?.filtroHorario}">
				<li class="fieldcontain">
					<span id="filtroHorario-label" class="property-label"><g:message code="filtroUsuario.filtroHorario.label" default="Filtro Horario" /></span>
					
						<span class="property-value" aria-labelledby="filtroHorario-label"><g:fieldValue bean="${filtroUsuarioInstance}" field="filtroHorario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filtroUsuarioInstance?.filtroSubUniorg}">
				<li class="fieldcontain">
					<span id="filtroSubUniorg-label" class="property-label"><g:message code="filtroUsuario.filtroSubUniorg.label" default="Filtro Sub Uniorg" /></span>
					
						<span class="property-value" aria-labelledby="filtroSubUniorg-label"><g:fieldValue bean="${filtroUsuarioInstance}" field="filtroSubUniorg"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filtroUsuarioInstance?.filtroTipoFuncionario}">
				<li class="fieldcontain">
					<span id="filtroTipoFuncionario-label" class="property-label"><g:message code="filtroUsuario.filtroTipoFuncionario.label" default="Filtro Tipo Funcionario" /></span>
					
						<span class="property-value" aria-labelledby="filtroTipoFuncionario-label"><g:fieldValue bean="${filtroUsuarioInstance}" field="filtroTipoFuncionario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filtroUsuarioInstance?.nomeFiltro}">
				<li class="fieldcontain">
					<span id="nomeFiltro-label" class="property-label"><g:message code="filtroUsuario.nomeFiltro.label" default="Nome Filtro" /></span>
					
						<span class="property-value" aria-labelledby="nomeFiltro-label"><g:fieldValue bean="${filtroUsuarioInstance}" field="nomeFiltro"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filtroUsuarioInstance?.usuario}">
				<li class="fieldcontain">
					<span id="usuario-label" class="property-label"><g:message code="filtroUsuario.usuario.label" default="Usuario" /></span>
					
						<span class="property-value" aria-labelledby="usuario-label"><g:link controller="usuario" action="show" id="${filtroUsuarioInstance?.usuario?.id}">${filtroUsuarioInstance?.usuario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${filtroUsuarioInstance?.id}" />
					<g:link class="edit" action="edit" id="${filtroUsuarioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
