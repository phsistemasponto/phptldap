

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${filtroUsuarioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${filtroUsuarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${filtroUsuarioInstance.id}">
		<form action="${request.contextPath}/filtrousuario/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${filtroUsuarioInstance?.id}" />
			<g:hiddenField name="version" value="${filtroUsuarioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/filtrousuario/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Filtro Cargo:</label>
				<div class="controls">
					<g:textField name="filtroCargo" value="${filtroUsuarioInstance?.filtroCargo}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filtro Departamento:</label>
				<div class="controls">
					<g:textField name="filtroDepartamento" value="${filtroUsuarioInstance?.filtroDepartamento}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filtro Filial:</label>
				<div class="controls">
					<g:textField name="filtroFilial" value="${filtroUsuarioInstance?.filtroFilial}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filtro Funcionario:</label>
				<div class="controls">
					<g:textField name="filtroFuncionario" value="${filtroUsuarioInstance?.filtroFuncionario}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filtro Horario:</label>
				<div class="controls">
					<g:textField name="filtroHorario" value="${filtroUsuarioInstance?.filtroHorario}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filtro Sub Uniorg:</label>
				<div class="controls">
					<g:textField name="filtroSubUniorg" value="${filtroUsuarioInstance?.filtroSubUniorg}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filtro Tipo Funcionario:</label>
				<div class="controls">
					<g:textField name="filtroTipoFuncionario" value="${filtroUsuarioInstance?.filtroTipoFuncionario}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Nome Filtro:</label>
				<div class="controls">
					<g:textField name="nomeFiltro" value="${filtroUsuarioInstance?.nomeFiltro}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Usuario:</label>
				<div class="controls">
					<g:select id="usuario" name="usuario.id" from="${acesso.Usuario.list()}" optionKey="id" required="" value="${filtroUsuarioInstance?.usuario?.id}" class="many-to-one"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>