
<%@page import="cadastros.TipoDeBeneficio"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    <g:javascript src="reports/beneficios.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Relatório de Benefícios</h3>
    </div>
        
    <g:jasperForm controller="relatorioBeneficios" action="generateReport" jasper="beneficios" class="form-horizontal" id="formPesquisa">
    
    	<div class="well">
    	
    		<div class="well" style="border: 1px solid #859CA4; background-color: #859CA4;">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					</div>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Tipo Benefício:</label>
				<div class="controls">
					<g:select name="tipoBeneficio" id="tipoBeneficio" optionKey="id" 
							value="${params.tipoRelatorio}"
							style="width:300px;"
							noSelection="['':'..']" 
							from="${TipoDeBeneficio.findAll()}"
							onchange="mudaTipoBeneficio()"/>
				</div>
			</div>
			
			<table class="table table-striped table-bordered table-condensed" id="beneficioTable" style="margin-top: 5px; width: 500px;">
	    		<thead>
	    			<tr>
	    				<th style="width:15px;"></th>
	    				<th>Benefício</th>
	    			</tr>
	    		</thead>
	    		<tbody id="tbodyBeneficio">
	    		</tbody>
	    	</table>
	    	
	    	<table class="table table-striped table-bordered table-condensed" id="naturezaBeneficioTable" style="margin-top: 5px; width: 500px;">
	    		<tbody id="tbodyNaturezaBeneficio">
	    			<tr>
	    				<td style="width:15px;"><input type="checkbox" name="naturezaBeneficio" id="naturezaBeneficio" value="1"/></td>
	    				<td>Benefícios Normais</td>
	    			</tr>
	    			<tr>
	    				<td style="width:15px;"><input type="checkbox" name="naturezaBeneficio" id="naturezaBeneficio" value="2"/></td>
	    				<td>Benefícios Extras</td>
	    			</tr>
	    		</tbody>
	    	</table>
	    	
	    	<table class="table table-striped table-bordered table-condensed" id="dataTable" style="margin-top: 5px; width: 500px;">
	    		<thead>
	    			<tr>
	    				<th style="width:15px;"></th>
	    				<th>Data</th>
	    			</tr>
	    		</thead>
	    		<tbody id="tbodyDatas">
	    		</tbody>
	    	</table>
	    	
	    	<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Data início:</label>
				<div class="controls">
					<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Data fim:</label>
				<div class="controls">
					<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Ordenação:</label>
				<div class="controls">
					<g:select name="ordenacao" id="ordenacao" optionKey="id" optionValue="nome" 
							value="${params.ordenacao}"
							style="width:200px;"
							from="${[ [id:'1', nome:'Código'],
									  [id:'2', nome:'Descrição'] ]}"/>
				</div>
			</div>
				    
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('PDF'); else return false;">Gerar em PDF</button>
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('XLS'); else return false;">Gerar em Excel</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>