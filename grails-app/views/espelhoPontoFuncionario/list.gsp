<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="espelhoPonto/espelhoPonto.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    
    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Espelho de ponto</h3>
    </div>
    <form action="${request.contextPath}/espelhoPontoFuncionario/list" method="POST" id="formPesquisa" class="form-horizontal">
    	<div class="well">
	    	<div class="row-fluid">
	    		<div class="span6">
						<div class="controls-horizontal">
							<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" placeholder="Data Inicio" required="required"/>&emsp;
							<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" placeholder="Data Fim" required="required"/>&emsp;
							<g:submitButton name="btnFiltro" class="btn btn-medium btn-info" value="Listar" />	
						</div>
				</div>
			</div>
		</div>
	</form>
           
    <hr/>
    
    <g:if test="${dadosPrincipais != null && dadosPrincipais.size > 0}">
    
    <div class="well">
    
    	<table>
    		<tr>
    			<td rowspan="6" style="padding-right: 30px;">
    				<div style="height: 150px; width: 150px; border: 1px solid black;" id="divFoto">
						<img src="${createLink(controller:'funcionario', action:'image', id: funcionario.id)}"/>
					</div>
    			</td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>Período:</h4></td>
    			<td style="font-size:14px;"><i>${params.dataInicio} - ${params.dataFim}</i></td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>Funcionario:</h4></td>
    			<td style="font-size:14px;"><i>${funcionario.crcFunc} - ${funcionario.nomFunc}</i></td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>PIS:</h4></td>
    			<td tyle="font-size:14px;"><i>${funcionario.pisFunc}</i></td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>Filial:</h4></td>
    			<td tyle="font-size:14px;"><i>${funcionario.filial.dscFantFil}</i></td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>Cargo:</h4></td>
    			<td tyle="font-size:14px;"><i>${funcionario.cargo.dscCargo}</i></td>
    		</tr>
    	</table>
    </div>
    
    <br/>
    
    <table class="table table-bordered table-condensed" id="tabelaPrincipal">
    	<thead>
	    		<tr>
					    		<th>Data</th>
					    		<th>Dia</th>
					    		<th>Horario</th>
					    		<th>Entra1</th>
					    		<th>Saida1</th>
					    		<th>Entra2</th>
					    		<th>Saida2</th>
					    		<th>Observações</th>
					    		
					    		<th>Saldo BH</th>
					    		<th>Desconto</th>
					    		<th>Extras</th>
					    		<th>Adicional</th>
					    		<th style="text-align:center;">Abonos</th>
					    	</tr>
				    	</thead>
				    	<tbody>
				    		<g:each in="${dadosPrincipais}" status="i" var="dado">
				    			<g:if test="${dado.observacaoFuncionario == ''}">
				          			<tr id="linhaDado-${dado.identificador}">
				          		</g:if>
				          		<g:elseif test="${dado.statusObservacaoFuncionario == 'A'}">
				          			<tr id="linhaDado-${dado.identificador}"  style="background-color: #fcf8e3">
				          		</g:elseif>
				          		<g:elseif test="${dado.statusObservacaoFuncionario == 'F'}">
				          			<tr id="linhaDado-${dado.identificador}" style="background-color: #dff0d8">
						        </g:elseif>
						          <td class="linhaDadoDataBatida">${dado?.dataBatida}</td>
						          <td>${dado?.diaBatida}</td>
						          <td>${dado?.horario}</td>
						          <td>${dado?.entrada1}</td>
						          <td>${dado?.saida1}</td>
						          <td>${dado?.entrada2}</td>
						          <td>${dado?.saida2}</td>
						          <td>${dado?.observacao}</td>
						          <td></td>
						          <td></td>
						          <td></td>
						          <td></td>						          
						          <td style="text-align:center;">
						          		<g:if test="${dado.observacaoFuncionario == ''}">
						          			<button class="ph" onclick="showLovIncluirObservacao('${dado?.dataBatida}')">
							          			<i class="icon-plus"></i>
							          		</button>
						          		</g:if>
						          		<g:elseif test="${dado.statusObservacaoFuncionario == 'A'}">
						          			<button class="ph" onclick="showLovIncluirObservacao('${dado?.dataBatida}')">
							          			<i class="icon-envelope"></i>
							          		</button>
						          		</g:elseif>
						          		<g:elseif test="${dado.statusObservacaoFuncionario == 'F'}">
						          			<button class="ph" onclick="showLovIncluirObservacao('${dado?.dataBatida}')">
							          			<i class="icon-ok"></i>
							          		</button>
						          		</g:elseif>
						          </td>
					        	</tr>
					      	</g:each>
				    	</tbody>
				    </table>
    			</td>
    			
    		</tr>
    	</tbody>
    </table>
    
    <g:render template="/espelhoPonto/lovIncluirObservacaoFuncionario"/>
    
    </g:if>
    
    
    
  </body>
</html>