	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="agenda/agenda.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${agendaInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${agendaInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${agendaInstance.id}">
		<form action="${request.contextPath}/agenda/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${agendaInstance?.id}" />
			<g:hiddenField name="version" value="${agendaInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/agenda/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>
		
			<g:each in="${mapHorarios.keySet()}" var="h">
				<g:hiddenField name="diaSalvo-${h}" value="${mapHorarios[h].id}" />
			</g:each>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="512" required="" value="${agendaInstance?.descricao}"/>
				</div>
			</div>
			
			<table class="table table-striped table-bordered table-condensed" id="agendamentoTable">
				<thead>
	    			<tr>
	    				<th>Dia da semana</th>
	    				<th>Grupo Horário</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    			<tr>
	    				<td>Domingo</td>
	    				<td>
	    					<g:select id="grupoHorario-0" name="grupoHorario-0" from="${cadastros.GrupoHorario.list()}" 
	    						optionKey="id" noSelection="['':'..']"/>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td>Segunda</td>
	    				<td>
	    					<g:select id="grupoHorario-1" name="grupoHorario-1" from="${cadastros.GrupoHorario.list()}" 
	    						optionKey="id" noSelection="['':'..']"/>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td>Terça</td>
	    				<td>
	    					<g:select id="grupoHorario-2" name="grupoHorario-2" from="${cadastros.GrupoHorario.list()}" 
	    						optionKey="id" noSelection="['':'..']"/>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td>Quarta</td>
	    				<td>
	    					<g:select id="grupoHorario-3" name="grupoHorario-3" from="${cadastros.GrupoHorario.list()}" 
	    						optionKey="id" noSelection="['':'..']"/>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td>Quinta</td>
	    				<td>
	    					<g:select id="grupoHorario-4" name="grupoHorario-4" from="${cadastros.GrupoHorario.list()}" 
	    						optionKey="id" noSelection="['':'..']"/>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td>Sexta</td>
	    				<td>
	    					<g:select id="grupoHorario-5" name="grupoHorario-5" from="${cadastros.GrupoHorario.list()}" 
	    						optionKey="id" noSelection="['':'..']"/>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td>Sábado</td>
	    				<td>
	    					<g:select id="grupoHorario-6" name="grupoHorario-6" from="${cadastros.GrupoHorario.list()}" 
	    						optionKey="id" noSelection="['':'..']"/>
	    				</td>
	    			</tr>
	    		</tbody>
			</table>
			
			<fieldset>
				<legend>Filiais</legend>
				
				<button type="button" class="btn btn-medium btn-primary" onclick="marcarTodos();">Marcar todos</button>
				<button type="button" class="btn btn-medium btn-primary" onclick="desmarcarTodos();">Desmarcar todos</button>
				
				<br/>
				
				<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Filial</th>
						</tr>
					</thead>
					<tbody>
						<g:set var="fs" bean="filialService"/>
						<g:each in="${fs.filiaisDoUsuario()}" var="f" status="i">
							<tr>
								<td>
									<g:if test="${agendaInstance.id != null && cadastros.AgendaFilial.findByAgendaAndFilial(agendaInstance, f) != null}">
										<input type="checkbox" name="filial" id="filial" value="${f.id}" checked="checked"/>
									</g:if>
									<g:else>
										<input type="checkbox" name="filial" id="filial" value="${f.id}"/>
									</g:else>
								</td>
								<td>${f}</td>
							</tr>
						</g:each>
					</tbody>
				</table>
				
			</fieldset>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>