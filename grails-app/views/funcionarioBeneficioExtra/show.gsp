
<%@ page import="cadastros.FuncionarioBeneficioExtra" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'funcionarioBeneficioExtra.label', default: 'FuncionarioBeneficioExtra')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-funcionarioBeneficioExtra" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-funcionarioBeneficioExtra" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list funcionarioBeneficioExtra">
			
				<g:if test="${funcionarioBeneficioExtraInstance?.dtBeneficio}">
				<li class="fieldcontain">
					<span id="dtBeneficio-label" class="property-label"><g:message code="funcionarioBeneficioExtra.dtBeneficio.label" default="Dt Beneficio" /></span>
					
						<span class="property-value" aria-labelledby="dtBeneficio-label"><g:formatDate date="${funcionarioBeneficioExtraInstance?.dtBeneficio}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioBeneficioExtraInstance?.dtTransacao}">
				<li class="fieldcontain">
					<span id="dtTransacao-label" class="property-label"><g:message code="funcionarioBeneficioExtra.dtTransacao.label" default="Dt Transacao" /></span>
					
						<span class="property-value" aria-labelledby="dtTransacao-label"><g:formatDate date="${funcionarioBeneficioExtraInstance?.dtTransacao}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioBeneficioExtraInstance?.quantidade}">
				<li class="fieldcontain">
					<span id="quantidade-label" class="property-label"><g:message code="funcionarioBeneficioExtra.quantidade.label" default="Quantidade" /></span>
					
						<span class="property-value" aria-labelledby="quantidade-label"><g:fieldValue bean="${funcionarioBeneficioExtraInstance}" field="quantidade"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioBeneficioExtraInstance?.beneficio}">
				<li class="fieldcontain">
					<span id="beneficio-label" class="property-label"><g:message code="funcionarioBeneficioExtra.beneficio.label" default="Beneficio" /></span>
					
						<span class="property-value" aria-labelledby="beneficio-label"><g:link controller="beneficio" action="show" id="${funcionarioBeneficioExtraInstance?.beneficio?.id}">${funcionarioBeneficioExtraInstance?.beneficio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioBeneficioExtraInstance?.funcionario}">
				<li class="fieldcontain">
					<span id="funcionario-label" class="property-label"><g:message code="funcionarioBeneficioExtra.funcionario.label" default="Funcionario" /></span>
					
						<span class="property-value" aria-labelledby="funcionario-label"><g:link controller="funcionario" action="show" id="${funcionarioBeneficioExtraInstance?.funcionario?.id}">${funcionarioBeneficioExtraInstance?.funcionario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${funcionarioBeneficioExtraInstance?.id}" />
					<g:link class="edit" action="edit" id="${funcionarioBeneficioExtraInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
