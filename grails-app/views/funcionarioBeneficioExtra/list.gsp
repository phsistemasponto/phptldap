
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>BenefÃ­cios Extras de FuncionÃ¡rios</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>FuncionÃ¡rio</th>
          <th>Beneficio</th>
		  <th>Data Beneficio</th>
		  <th>Quantidade</th>
		  <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${funcionarioBeneficioExtraInstanceList}" status="i" var="funcionarioBeneficioExtraInstance">
        <tr>
		  <td>${fieldValue(bean: funcionarioBeneficioExtraInstance, field: "funcionario")}</td>
		  <td>${fieldValue(bean: funcionarioBeneficioExtraInstance, field: "beneficio")}</td>
          <td><g:formatDate date="${funcionarioBeneficioExtraInstance.dtBeneficio}" format="dd/MM/yyyy" /></td>
		  <td>${fieldValue(bean: funcionarioBeneficioExtraInstance, field: "quantidade")}</td>
		  <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${funcionarioBeneficioExtraInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/funcionarioBeneficioExtra/delete', ${funcionarioBeneficioExtraInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${funcionarioBeneficioExtraInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${funcionarioBeneficioExtraInstanceTotal}</span>
    </div>
  </body>
</html>