	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionarioBeneficioExtra/funcionarioBeneficioExtra.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="beneficio/beneficio.js"/>
    

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${funcionarioBeneficioExtraInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${funcionarioBeneficioExtraInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${funcionarioBeneficioExtraInstance.id}">
		<form action="${request.contextPath}/funcionarioBeneficioExtra/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${funcionarioBeneficioExtraInstance?.id}" />
			<g:hiddenField name="version" value="${funcionarioBeneficioExtraInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/funcionarioBeneficioExtra/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>
		
			<div class="control-group">
				<label class="control-label">FuncionÃ¡rio:</label>
				<div class="controls">
					<g:textField name="funcionarioId" maxlength="5" style="width:50px" onBlur="buscaIdFuncionario();" 
						value="${params.funcionarioId}"/>&nbsp; 
					<i class="icon-search" onclick="buscarLovFuncionario();" style="cursor: pointer" title="Procurar FuncionÃ¡rio" id="funcionarioBotaoBusca"></i> &nbsp;
					<g:textField name="funcionarioNome" maxlength="450" readOnly="true" style="width:450px" 
						value="${params.funcionarioNome}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">BenefÃ­cio:</label>				
				<div class="controls">
					<g:textField name="beneficioId" maxlength="5" style="width:50px" onBlur="buscaIdBeneficio();" value="${params.beneficioId}"/>&nbsp;
					<i class="icon-search" onclick="buscarLovBeneficio();" style="cursor:pointer" title="Procurar BenefÃ­cio"></i>&nbsp;
					<g:textField name="beneficioDescricao" maxlength="450" readOnly="true" style="width:450px" value="${params.beneficioDescricao}"/>&nbsp;
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Valor:</label>		
				<div class="controls">
					<g:textField name="beneficioValor" style="width:80px" readOnly="true" value="${params.beneficioValor}"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Quantidade:</label>		
				<div class="controls">
					<g:textField name="beneficioQuantidade" style="width:80px" value="${params.beneficioQuantidade}" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Data BenefÃ­cio:</label>				
				<div class="controls">
					<g:textField name="dataBeneficio" style="width:80px" value="${params.dataBeneficio}" />
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>
	
	<g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>