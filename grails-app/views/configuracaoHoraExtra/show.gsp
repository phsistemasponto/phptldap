
<%@ page import="cadastros.ConfiguracaoHoraExtra" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'configuracaoHoraExtra.label', default: 'Configuração Horas Extras')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-configuracaoHoraExtra" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-configuracaoHoraExtra" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list configuracaoHoraExtra">
			
				<g:if test="${configuracaoHoraExtraInstance?.descricao}">
				<li class="fieldcontain">
					<span id="descricao-label" class="property-label"><g:message code="configuracaoHoraExtra.descricao.label" default="Descricao" /></span>
					
						<span class="property-value" aria-labelledby="descricao-label"><g:fieldValue bean="${configuracaoHoraExtraInstance}" field="descricao"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${configuracaoHoraExtraInstance?.horaExtraAntesExpediente}">
				<li class="fieldcontain">
					<span id="horaExtraAntesExpediente-label" class="property-label"><g:message code="configuracaoHoraExtra.horaExtraAntesExpediente.label" default="Hora Extra Antes Expediente" /></span>
					
						<span class="property-value" aria-labelledby="horaExtraAntesExpediente-label"><g:fieldValue bean="${configuracaoHoraExtraInstance}" field="horaExtraAntesExpediente"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${configuracaoHoraExtraInstance?.horaExtraIntervaloRefeicao}">
				<li class="fieldcontain">
					<span id="horaExtraIntervaloRefeicao-label" class="property-label"><g:message code="configuracaoHoraExtra.horaExtraIntervaloRefeicao.label" default="Hora Extra Intervalo Refeicao" /></span>
					
						<span class="property-value" aria-labelledby="horaExtraIntervaloRefeicao-label"><g:fieldValue bean="${configuracaoHoraExtraInstance}" field="horaExtraIntervaloRefeicao"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${configuracaoHoraExtraInstance?.itens}">
				<li class="fieldcontain">
					<span id="itens-label" class="property-label"><g:message code="configuracaoHoraExtra.itens.label" default="Itens" /></span>
					
						<g:each in="${configuracaoHoraExtraInstance.itens}" var="i">
						<span class="property-value" aria-labelledby="itens-label"><g:link controller="itensConfiguracaoHoraExtra" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${configuracaoHoraExtraInstance?.id}" />
					<g:link class="edit" action="edit" id="${configuracaoHoraExtraInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
