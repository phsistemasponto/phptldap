	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="configuracaoHoraExtra/configuracaoHoraExtra.js"/>
	<g:javascript src="tipoHoraExtra/tipoHoraExtra.js"/>
	<g:javascript src="classificacao/classificacao.js"/>
	<g:javascript src="comum/lov.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${configuracaoHoraExtraInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${configuracaoHoraExtraInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${configuracaoHoraExtraInstance.id}">
		<form action="${request.contextPath}/configuracaoHoraExtra/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${configuracaoHoraExtraInstance?.id}" />
			<g:hiddenField name="version" value="${configuracaoHoraExtraInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/configuracaoHoraExtra/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descricao:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="40" required="" value="${configuracaoHoraExtraInstance?.descricao}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Autoriza hora extra no intervalo para refeição:</label>
				<div class="controls">
					<g:checkBox name="horaExtraIntervaloRefeicao" value="${configuracaoHoraExtraInstance?.horaExtraIntervaloRefeicao}" 
								checked="${configuracaoHoraExtraInstance?.horaExtraIntervaloRefeicao == 'S'}" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Autoriza hora extra antes do 1 expediente:</label>
				<div class="controls">
					<g:checkBox name="horaExtraAntesExpediente" value="${configuracaoHoraExtraInstance?.horaExtraAntesExpediente}" 
								checked="${configuracaoHoraExtraInstance?.horaExtraAntesExpediente == 'S'}" />
				</div>
			</div>
			
			<fieldset>
				<legend>Adicionar item</legend>
				
				<div class="control-group">
					<label class="control-label">Classificação</label>
					<div class="controls">
						<g:textField name="classificacaoId" maxlength="5" style="width:50px" 
							onBlur="buscaIdClassificacao('#classificacaoId', '#classificacaoNome', suprimirClassificacoesUsadas());"/>
						&nbsp; 
						<i class="icon-search" onclick="buscarLovClassificacao('classificacaoId', 'classificacaoNome', suprimirClassificacoesUsadas());" style="cursor: pointer" 
							title="Procurar Classificação" id="classificacaoBotaoBusca"></i> 
						&nbsp;
						<g:textField name="classificacaoNome" maxlength="450" readOnly="true" style="width:450px"/>
					</div>
				</div>
				
				<div class="row-fluid">
					
					<div class="span3 bgcolor">
						<div class="control-group">
							<label class="control-label">Tolerância:</label>
							<div class="controls">
								<g:textField name="tolerancia" maxlength="5" style="width:60px"/>
							</div>
						</div>
					</div>
					
					<div class="span3 bgcolor">
						<div class="control-group">
							<label class="control-label">Limite por dia:</label>
							<div class="controls">
								<g:textField name="limitePorDia" maxlength="5" style="width:60px"/>
							</div>
						</div>
					</div>
					
					<div class="span3 bgcolor">
						<div class="control-group">
							<label class="control-label">Dia específico da semana:</label>
							<div class="controls">
								<g:select name="diaEspecificoSemana" optionKey="id" optionValue="nome" 
									from="${[ [id:'1', nome:'Domingo'], 
											  [id:'2', nome:'Segunda'], 
											  [id:'3', nome:'Terça'], 
											  [id:'4', nome:'Quarta'], 
											  [id:'5', nome:'Quinta'], 
											  [id:'6', nome:'Sexta'], 
											  [id:'7', nome:'Sábado'] ]}"/>
							</div>
						</div>
					</div>
					
				</div>
				
				<div class="control-group">
					<label class="control-label">Tipo de configuração:</label>
					<div class="controls">
						<g:select name="tipoConfiguracao" optionKey="id" optionValue="nome" style="width:400px" 
							from="${[ [id:'1', nome:'CONFIGURACAO DE HE POR INTERVALO DE HORAS'], 
									  [id:'2', nome:'CONFIGURACAO DE HE POR QTDE DE HORAS TRABALHADAS'] ]}"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Quantidade horas para descontar:</label>
					<div class="controls">
						<g:textField name="qtdadeHorasDescontar" maxlength="5" style="width:60px"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Limites de horas para desconto:</label>
					<div class="controls">
						<g:textField name="limiteHorasDesconto" maxlength="5" style="width:60px"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Limites de quantidade efetuada para desconto:</label>
					<div class="controls">
						<g:textField name="limiteQuantidadeEfetuadaDesconto" maxlength="5" style="width:60px"/>
					</div>
				</div>
				
				<div class="well">
				
					<div class="control-group">
						<label class="control-label">Tipo hora extra</label>
						<div class="controls">
							<g:textField name="tipoHoraExtraId" maxlength="5" style="width:50px" 
								onBlur="buscaIdTipoHora('#tipoHoraExtraId', '#tipoHoraExtraNome', suprimirTipoHorasUsadas());"/>
							&nbsp; 
							<i class="icon-search" onclick="buscarLovTipoHora('tipoHoraExtraId', 'tipoHoraExtraNome', suprimirTipoHorasUsadas());" style="cursor: pointer" 
								title="Procurar Tipo Hora Extra" id="tipoHoraExtraBotaoBusca"></i> 
							&nbsp;
							<g:textField name="tipoHoraExtraNome" maxlength="450" readOnly="true" style="width:450px"/>
						</div>
					</div>
					
					<div class="row-fluid">
					
						<div class="span3 bgcolor">
							<div class="control-group">
								<label class="control-label">Hora início:</label>
								<div class="controls">
									<g:textField name="horaIni" maxlength="5" style="width:60px"/>
								</div>
							</div>
						</div>
						
						<div class="span4 bgcolor">
							<div class="control-group">
								<label class="control-label">Hora fim:</label>
								<div class="controls">
									<g:textField name="horaFim" maxlength="5" style="width:60px"/>
									<a id="incPercent" class="btn btn-medium" onClick="incluirPercentual();">Incluir</a>
								</div>
							</div>
						</div>
						
					</div>
					
					<div id="percentuais">
						<table class="table table-striped table-bordered table-condensed" id="percentualTable">
							<thead>
								<tr>
									<th style="width: 30%;">Tipo hora</th>
									<th style="width: 30%;">Hora início</th>
									<th style="width: 30%;">Hora fim</th>
									<th style="width: 10%;"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input type="hidden" id="sequencialTemp" name="sequencialTemp" value="0"/></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					
				</div>
				
				<a id="incItem" class="btn btn-medium" onClick="incluirItem();">Incluir</a>
				
				<br/>
				<br/>
				
				<div id="itens">
					<table class="table table-striped table-bordered table-condensed" id="itemTable">
						<thead>
							<tr>
								<th style="width: 45%;">Classificação</th>
								<th style="width: 15%;">Tolerância</th>
								<th style="width: 15%;">Limite por dia</th>
								<th style="width: 15%;">Dia semana</th>
								<th style="width: 10%;"></th>
							</tr>
						</thead>
						<tbody>
							<tr id="rowTableItem0">
								<td><input id="sequencialItem" name="sequencialItem" type="hidden" value="0"/></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							
							
							<g:each in="${itensConfiguracaoHora}" status="i" var="item">
								<tr id="rowTableItem${i+1}" class="even" style="background-color: #00AFFF">
									<td>
										<g:textField name="classificacaoNomeItem" value="${item?.classificacao?.dscClass}" readOnly="true"/>
										<g:hiddenField name="classificacaoIdItem" value="${item?.classificacao?.id}"/>
										<g:hiddenField name="sequencialItem" value="${i+1}"/>
									</td>
									<td>
										<g:textField name="toleranciaItem" value="${item?.toleranciaFormatada}" readOnly="true"/>
									</td>
									<td>
										<g:textField name="limitePorDiaItem" value="${item?.limitePorDiaFormatada}" readOnly="true"/>
									</td>
									<td>
										<g:textField name="diaEspecificoSemanaItem" value="${item?.diaEspecificoSemana}" readOnly="true"/>
										<g:hiddenField name="diaEspecificoSemanaItemValue" value="${item?.diaEspecificoSemana}"/>
										<g:hiddenField name="tipoConfiguracaoItem" value="${item?.tipoConfiguracao}"/>
										<g:hiddenField name="qtdadeHorasDescontarItem" value="${item?.qtdadeHorasDescontarFormatada}"/>
										<g:hiddenField name="limiteHorasDescontoItem" value="${item?.limiteHorasDescontoFormatada}"/>
										<g:hiddenField name="limiteQuantidadeEfetuadaDescontoItem" value="${item?.limiteQuantidadeEfetuadaDescontoFormatada}"/>
									</td>
									<td>
										<i class="icon-remove" onclick="excluirItem(${i+1});" style="cursor: pointer" title="Excluir registro"></i>
									</td>
								</tr>
								<tr>
									<td colspan="5" style="padding:20px">
										<table>
											<thead>
												<tr>
													<th>Tipo Hora</th>
													<th>Hora Inicio</th>
													<th>Hora fim</th>
												</tr>
											</thead>
											<tbody>
												<g:each in="${item.percentuais}" status="j" var="perc">
												
													<tr>
														<td>
															<g:textField name="nomeTipoHe${j+1}item${i+1}" value="${perc?.tipoHora?.dscTpHorExt}" readOnly="true"/>
															<g:hiddenField name="idTipoHe${j+1}item${i+1}" value="${perc?.tipoHora?.id}" readOnly="true"/>
															<g:hiddenField name="sequencial${j+1}item${i+1}" value="${i+1}" readOnly="true"/>
														</td>
														<td>
															<g:textField name="horaIni${j+1}item${i+1}" value="${perc?.horaInicioFormatada}" readOnly="true"/>
														</td>
														<td>
															<g:textField name="horaFim${j+1}item${i+1}" value="${perc?.horaFimFormatada}" readOnly="true"/>
														</td>
													</tr>
												
												</g:each>
											</tbody>
										</table>
									</td>
								</tr>
							</g:each>
							
							
							
						</tbody>
					</table>
				</div>
				
			</fieldset>

		</fieldset>
		
		<hr>
		
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		
		<g:render template="/busca/lovBuscaInterna"/>
		
	</form>