
	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="grupoHorario/grupoHorario.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${grupoHorarioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${grupoHorarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${grupoHorarioInstance.id}">
		<form action="${request.contextPath}/grupoHorario/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${grupoHorarioInstance?.id}" />
			<g:hiddenField name="version" value="${grupoHorarioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/grupoHorario/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="512" required="true" value="${grupoHorarioInstance?.descricao}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Horário:</label>
				<div class="controls">
					<g:textField name="hora" maxlength="10" maxlength="5" style="width:60px"/>
					<button type="button" class="btn btn-small btn-primary" onclick="incluirHorario();">Incluir</button>
				</div>
			</div>
			
			<fieldset>
				
				<div class="control-group">
					<div class="controls">
						<select id="horarios" name="horarios" multiple="multiple" style="width: 200px; height: 150px;">
							<g:each in="${horarios}" var="h">
								<option val="${h}" selected="selected">${h}</option>
							</g:each>
						</select>
					</div>
				</div>
				
			</fieldset>
			

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>