
	<g:javascript src="parametroCriticaExtras/parametroCriticaExtras.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${parametroCriticaExtrasInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${parametroCriticaExtrasInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${parametroCriticaExtrasInstance.id}">
		<form action="${request.contextPath}/parametroCriticaExtras/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${parametroCriticaExtrasInstance?.id}" />
			<g:hiddenField name="version" value="${parametroCriticaExtrasInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/parametroCriticaExtras/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">
					<g:checkBox name="admExtra50" value="${parametroCriticaExtrasInstance?.admExtra50}" checked="${parametroCriticaExtrasInstance?.admExtra50 == 'S'}" />
				</label>
				<div class="controls" style="padding-top: 8px;">
				    Se é do administrativo, pagar extra 50%?					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">
					<g:checkBox name="admExtra100" value="${parametroCriticaExtrasInstance?.admExtra100}" checked="${parametroCriticaExtrasInstance?.admExtra100 == 'S'}" />
				</label>
				<div class="controls" style="padding-top: 8px;">
				    Se é do administrativo, pagar extra 100%?					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">
					<g:checkBox name="naoAdmExtra50" value="${parametroCriticaExtrasInstance?.naoAdmExtra50}" checked="${parametroCriticaExtrasInstance?.naoAdmExtra50 == 'S'}" />
				</label>
				<div class="controls" style="padding-top: 8px;">
				    Se não é do administrativo, pagar extra 50%?
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">
					<g:checkBox name="naoAdmExtra100" value="${parametroCriticaExtrasInstance?.naoAdmExtra100}" checked="${parametroCriticaExtrasInstance?.naoAdmExtra100 == 'S'}" />
				</label>
				<div class="controls" style="padding-top: 8px;">
				    Se não é do administrativo, pagar extra 100%?
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Se banco anterior positivo, descontar compensação de:</label>
				<div class="controls">
					<input type="radio" name="bancoNegMaiorComp" id="bancoNegMaiorComp" value="0" ${parametroCriticaExtrasInstance?.bancoNegMaiorComp == 0 ? 'checked' : ''}> &nbsp; Só do banco anterior <br>
					<input type="radio" name="bancoNegMaiorComp" id="bancoNegMaiorComp" value="1" ${parametroCriticaExtrasInstance?.bancoNegMaiorComp == 1 ? 'checked' : ''}> &nbsp; Só das Extras 1 do período <br>
					<input type="radio" name="bancoNegMaiorComp" id="bancoNegMaiorComp" value="2" ${parametroCriticaExtrasInstance?.bancoNegMaiorComp == 2 ? 'checked' : ''}> &nbsp; Primeiro do banco anterior, completando com as extras 1 do período, quando for o caso <br>
					<input type="radio" name="bancoNegMaiorComp" id="bancoNegMaiorComp" value="3" ${parametroCriticaExtrasInstance?.bancoNegMaiorComp == 3 ? 'checked' : ''}> &nbsp; Primeiro das extras 1 do período, completando com o banco anterior, quando for o caso <br>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Se banco anterior negativo e extras 1 maior que a compensação:</label>
				<div class="controls">
					<input type="radio" name="bancoPosDescontoComp" id="bancoPosDescontoComp" value="0" ${parametroCriticaExtrasInstance?.bancoPosDescontoComp == 0 ? 'checked' : ''}> &nbsp; Descontar compensação das extras 1 e pagar o restante em folha <br>
					<input type="radio" name="bancoPosDescontoComp" id="bancoPosDescontoComp" value="1" ${parametroCriticaExtrasInstance?.bancoPosDescontoComp == 1 ? 'checked' : ''}> &nbsp; Descontar compensação das extras 1 e cobrir banco anterior negativo com o restante <br>
				</div>
			</div>
			
			<fieldset>
				<legend>Filiais</legend>
				
				<button type="button" class="btn btn-medium btn-primary" onclick="marcarTodos();">Marcar todos</button>
				<button type="button" class="btn btn-medium btn-primary" onclick="desmarcarTodos();">Desmarcar todos</button>
				
				<br/><br/>
				
				<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Filial</th>
						</tr>
					</thead>
					<tbody>
						<g:set var="fs" bean="filialService"/>
						<g:each in="${fs.filiaisDoUsuario()}" var="f" status="i">
							<tr>
								<td>
									<g:if test="${parametroCriticaExtrasInstance.id != null && cadastros.ParametroCriticaExtrasFilial.findByParametroCriticaExtrasAndFilial(parametroCriticaExtrasInstance, f) != null}">
										<input type="checkbox" name="filial" id="filial" value="${f.id}" checked="checked"/>
									</g:if>
									<g:else>
										<g:if test="${parametroCriticaExtrasInstance.id != null && cadastros.ParametroCriticaExtrasFilial.findByFilial(f) != null}">
											<input type="checkbox" name="filial" id="filial" value="${f.id}" disabled="disabled" 
												alt="Já utilizada em outro parâmetro"/>
										</g:if>
										<g:else>
											<input type="checkbox" name="filial" id="filial" value="${f.id}"/>
										</g:else>
									</g:else>
								</td>
								<td>${f}</td>
							</tr>
						</g:each>
					</tbody>
				</table>
				
			</fieldset>
			
		</fieldset>
		
		<hr>
		
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		
</form>