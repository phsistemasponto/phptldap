
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Parametro de Crítica das Extras</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Parametro de Crítica das Extras']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>Adm Extra50</th>
          <th>Adm Extra100</th>
          <th>Nao Adm Extra50</th>
          <th>Nao Adm Extra100</th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${parametroCriticaExtrasInstanceList}" status="i" var="parametroCriticaExtrasInstance">
        <tr>
          <td>${fieldValue(bean: parametroCriticaExtrasInstance, field: "admExtra50")}</td>
          <td>${fieldValue(bean: parametroCriticaExtrasInstance, field: "admExtra100")}</td>
          <td>${fieldValue(bean: parametroCriticaExtrasInstance, field: "naoAdmExtra50")}</td>
          <td>${fieldValue(bean: parametroCriticaExtrasInstance, field: "naoAdmExtra100")}</td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${parametroCriticaExtrasInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/parametroCriticaExtras/delete', ${parametroCriticaExtrasInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${parametroCriticaExtrasInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${parametroCriticaExtrasInstanceTotal}</span>
    </div>
  </body>
</html>