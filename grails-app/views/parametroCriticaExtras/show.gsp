
<%@ page import="cadastros.ParametroCriticaExtras" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'parametroCriticaExtras.label', default: 'ParametroCriticaExtras')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-parametroCriticaExtras" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-parametroCriticaExtras" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list parametroCriticaExtras">
			
				<g:if test="${parametroCriticaExtrasInstance?.admExtra50}">
				<li class="fieldcontain">
					<span id="admExtra50-label" class="property-label"><g:message code="parametroCriticaExtras.admExtra50.label" default="Adm Extra50" /></span>
					
						<span class="property-value" aria-labelledby="admExtra50-label"><g:fieldValue bean="${parametroCriticaExtrasInstance}" field="admExtra50"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroCriticaExtrasInstance?.admExtra100}">
				<li class="fieldcontain">
					<span id="admExtra100-label" class="property-label"><g:message code="parametroCriticaExtras.admExtra100.label" default="Adm Extra100" /></span>
					
						<span class="property-value" aria-labelledby="admExtra100-label"><g:fieldValue bean="${parametroCriticaExtrasInstance}" field="admExtra100"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroCriticaExtrasInstance?.naoAdmExtra50}">
				<li class="fieldcontain">
					<span id="naoAdmExtra50-label" class="property-label"><g:message code="parametroCriticaExtras.naoAdmExtra50.label" default="Nao Adm Extra50" /></span>
					
						<span class="property-value" aria-labelledby="naoAdmExtra50-label"><g:fieldValue bean="${parametroCriticaExtrasInstance}" field="naoAdmExtra50"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroCriticaExtrasInstance?.naoAdmExtra100}">
				<li class="fieldcontain">
					<span id="naoAdmExtra100-label" class="property-label"><g:message code="parametroCriticaExtras.naoAdmExtra100.label" default="Nao Adm Extra100" /></span>
					
						<span class="property-value" aria-labelledby="naoAdmExtra100-label"><g:fieldValue bean="${parametroCriticaExtrasInstance}" field="naoAdmExtra100"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroCriticaExtrasInstance?.bancoNegMaiorComp}">
				<li class="fieldcontain">
					<span id="bancoNegMaiorComp-label" class="property-label"><g:message code="parametroCriticaExtras.bancoNegMaiorComp.label" default="Banco Neg Maior Comp" /></span>
					
						<span class="property-value" aria-labelledby="bancoNegMaiorComp-label"><g:fieldValue bean="${parametroCriticaExtrasInstance}" field="bancoNegMaiorComp"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroCriticaExtrasInstance?.bancoPosDescontoComp}">
				<li class="fieldcontain">
					<span id="bancoPosDescontoComp-label" class="property-label"><g:message code="parametroCriticaExtras.bancoPosDescontoComp.label" default="Banco Pos Desconto Comp" /></span>
					
						<span class="property-value" aria-labelledby="bancoPosDescontoComp-label"><g:fieldValue bean="${parametroCriticaExtrasInstance}" field="bancoPosDescontoComp"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroCriticaExtrasInstance?.filiais}">
				<li class="fieldcontain">
					<span id="filiais-label" class="property-label"><g:message code="parametroCriticaExtras.filiais.label" default="Filiais" /></span>
					
						<g:each in="${parametroCriticaExtrasInstance.filiais}" var="f">
						<span class="property-value" aria-labelledby="filiais-label"><g:link controller="parametroCriticaExtrasFilial" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${parametroCriticaExtrasInstance?.id}" />
					<g:link class="edit" action="edit" id="${parametroCriticaExtrasInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
