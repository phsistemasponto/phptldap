
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>TipoDeBeneficios</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['TipoDeBeneficio']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Descricao</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${tipoDeBeneficioInstanceList}" status="i" var="tipoDeBeneficioInstance">
        <tr>
		
          <td>${fieldValue(bean: tipoDeBeneficioInstance, field: "descricao")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${tipoDeBeneficioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/tipoDeBeneficio/delete', ${tipoDeBeneficioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${tipoDeBeneficioInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${tipoDeBeneficioInstanceTotal}</span>
    </div>
  </body>
</html>