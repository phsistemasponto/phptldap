

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${tipoDeBeneficioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${tipoDeBeneficioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${tipoDeBeneficioInstance.id}">
		<form action="${request.contextPath}/tipoDeBeneficio/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${tipoDeBeneficioInstance?.id}" />
			<g:hiddenField name="version" value="${tipoDeBeneficioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/tipoDeBeneficio/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descricao:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="80" required="" value="${tipoDeBeneficioInstance?.descricao}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>