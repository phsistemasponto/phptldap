
	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="jquery.maskMoney.js" />
	<g:javascript src="comum/lov.js"/>
	<g:javascript src="beneficioParam/beneficioParam.js"/>
	
	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${beneficioParamInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${beneficioParamInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${beneficioParamInstance.id}">
		<form action="${request.contextPath}/beneficioParam/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${beneficioParamInstance?.id}" />
			<g:hiddenField name="version" value="${beneficioParamInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/beneficioParam/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>
		
			<div class="control-group">
				<label class="control-label">Tipo De Benefício:</label>
				<div class="controls">
					<g:select id="tipoDeBeneficio" name="tipoDeBeneficio.id" from="${cadastros.TipoDeBeneficio.list()}" optionKey="id" required="" value="${beneficioParamInstance?.tipoDeBeneficio?.id}" class="many-to-one"/>
				</div>
			</div>
			
			<!-- CABECALHO -->
			
			<div class="row-fluid">	
			
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label">Benefício para incremento:</label>
						<div class="controls">
							<g:select name="idBenIncremento" id="idBenIncremento" optionKey="id" optionValue="nome" 
												value="${beneficioParamInstance.idBenIncremento}"
												style="width:100px;"
												from="${[ [id:'M', nome:'Mensal'], [id:'D', nome:'Diário'] ]}"/>
						</div>
					</div>
				</div>
				
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label">Desconto Diário:</label>
						<div class="controls">
							<g:checkBox name="idBenDescontoDiario" value="${beneficioParamInstance?.idBenDescontoDiario}" 
										checked="${beneficioParamInstance?.idBenDescontoDiario == 'S'}" />
						</div>
					</div>
				</div>
			
			</div>
			
			
			<!-- MES ANTERIOR -->
			
			<div class="control-group">
				<label class="control-label">Verifica Mês Anterior:</label>
				<div class="controls">
					<g:checkBox name="idVerificaMesAnterior" value="${beneficioParamInstance?.idVerificaMesAnterior}" 
								checked="${beneficioParamInstance?.idVerificaMesAnterior == 'S'}"/>
				</div>
			</div>
			
			<div id="divMesAnterior" class="well">
			
			
				<div class="row-fluid">	
					<div class="span2 bgcolor">	
						<div class="control-group">
							<label class="control-label">Verifica Quantidade faltas:</label>
							<div class="controls">
								<g:checkBox name="idVerificaQtFaltas" value="${beneficioParamInstance?.idVerificaQtFaltas}" 
											checked="${beneficioParamInstance?.idVerificaQtFaltas == 'S'}" />
							</div>
						</div>
					</div>
					<div class="span1 bgcolor">
						<div class="control-group">
							<label class="control-label">Quantas:</label>
							<div class="controls">
								<g:textField name="qtFaltas" required="" value="${fieldValue(bean: beneficioParamInstance, field: 'qtFaltas')}"
									style="width: 50px;"/>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row-fluid">
						
					<div class="span8 bgcolor">
						<div class="control-group">
							<label class="control-label">Ocorrência</label>
							<div class="controls">
								<g:textField name="ocorrenciaId" maxlength="5" style="width:50px" onBlur="buscaOcorrenciaId();"/>
								&nbsp; 
								<i class="icon-search" onclick="buscaOcorrenciaLov();" style="cursor: pointer" title="Procurar Ocorrência" id="ocorrenciaBotaoBusca"></i> 
								&nbsp;
								<g:textField name="ocorrenciaNome" maxlength="350" readOnly="true" style="width:350px"/>
							</div>
						</div>
					</div>
					
					<div class="span4 bgcolor">
						<div class="control-group">
							<label class="control-label">Quantidade:</label>
							<div class="controls">
								<g:textField name="quantidadeOcorrencia" maxlength="3" style="width:60px"/>
								<a class="btn btn-medium" onClick="incluirOcorrencia();">Incluir</a>
							</div>
						</div>
					</div>
					
				</div>
				
				<table class="table table-striped table-bordered table-condensed" id="ocorrenciasTable">
					<thead>
						<tr>
							<th>Ocorrência</th>
							<th>Quantidade</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<g:each in="${beneficioParamInstance.ocorrencias}" status="i" var="o">
							<tr id="rowTableItemOcorrencia-${i}">
								<td>${o.ocorrencias.dscOcor}</td>
								<td>${o.quantidade}</td>
								<td>
									<input type="hidden" name="idOcorrencia" id="idOcorrencia" value="${o.ocorrencias.id}"/>
									<input type="hidden" name="qtdadeOcorrencia" id="qtdadeOcorrencia" value="${o.quantidade}"/>
									<i class="icon-remove" onclick="excluirOcorrencia(${i});" style="cursor: pointer" title="Excluir registro"></i>
								</td>
							</tr>
						</g:each>
					</tbody>
				</table>
				
				<div class="well">
					<div class="control-group">
						<label class="control-label">Verifica Quantidade faltas abonadas:</label>
						<div class="controls">
							<g:checkBox name="idVerificaFaltasAbo" value="${beneficioParamInstance?.idVerificaFaltasAbo}" 
										checked="${beneficioParamInstance?.idVerificaFaltasAbo == 'S'}" />
						</div>
					</div>
					
					<div class="row-fluid">
						
						<div class="span8 bgcolor">
							<div class="control-group">
								<label class="control-label">Justificativa</label>
								<div class="controls">
									<g:textField name="justificativaId" maxlength="5" style="width:50px" onBlur="buscaJustificativaId();"/>
									&nbsp; 
									<i class="icon-search" onclick="buscaJustificativaLov();" style="cursor: pointer" title="Procurar Tipo Hora Extra" id="tipoHoraExtraBotaoBusca"></i> 
									&nbsp;
									<g:textField name="justificativaNome" maxlength="350" readOnly="true" style="width:350px"/>
								</div>
							</div>
						</div>
						
						<div class="span4 bgcolor">
							<div class="control-group">
								<label class="control-label">Quantidade:</label>
								<div class="controls">
									<g:textField name="quantidade" maxlength="3" style="width:60px"/>
									<a id="incPercent" class="btn btn-medium" onClick="incluirJustificativaAbono();">Incluir</a>
								</div>
							</div>
						</div>
						
					</div>
					
					<table class="table table-striped table-bordered table-condensed" id="justificativasTable">
						<thead>
							<tr>
								<th>Justificativa</th>
								<th>Quantidade</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${beneficioParamInstance.justificativasAbono}" status="i" var="just">
								<tr id="rowTableItemJustificativa-${i}">
									<td>${just.justificativas.dscJust}</td>
									<td>${just.quantidade}</td>
									<td>
										<input type="hidden" name="idJustAbono" id="idJustAbono" value="${just.justificativas.id}"/>
										<input type="hidden" name="qtdadeJustAbono" id="qtdadeJustAbono" value="${just.quantidade}"/>
										<i class="icon-remove" onclick="excluirJustificativa(${i});" style="cursor: pointer" title="Excluir registro"></i>
									</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					
				</div>
				
				
				<div class="row-fluid">	
					<div class="span2 bgcolor">	
						<div class="control-group">
							<label class="control-label">Verifica Extras:</label>
							<div class="controls">
								<g:checkBox name="idVerificaExtras" value="${beneficioParamInstance?.idVerificaExtras}" 
											checked="${beneficioParamInstance?.idVerificaExtras == 'S'}" />
							</div>
						</div>
					</div>
					<div class="span1 bgcolor">
						<div class="control-group">
							<label class="control-label">Quantas:</label>
							<div class="controls">
								<g:textField name="qtExtras" required="" value="${fieldValue(bean: beneficioParamInstance, field: 'qtExtrasExibicao')}"
									style="width: 50px;"/>
							</div>
						</div>
					</div>
				</div>
			
			</div>
			
			<!-- MES ATUAL -->
			
			<div class="control-group">
				<label class="control-label">Verifica Mês Atual/Posterior:</label>
				<div class="controls">
					<g:checkBox name="idVerificaMesProcesso" value="${beneficioParamInstance?.idVerificaMesProcesso}" 
								checked="${beneficioParamInstance?.idVerificaMesProcesso == 'S'}" />
				</div>
			</div>
			
			<div id="divMesAtual" class="well">
				<div class="control-group">
					<label class="control-label">Verifica Feriado:</label>
					<div class="controls">
						<g:checkBox name="idVerificaFeriado" value="${beneficioParamInstance?.idVerificaFeriado}" 
									checked="${beneficioParamInstance?.idVerificaFeriado == 'S'}" />
					</div>
				</div>
	
				<div class="control-group">
					<label class="control-label">Verifica Folgas:</label>
					<div class="controls">
						<g:checkBox name="idVerificaFolgaComp" value="${beneficioParamInstance?.idVerificaFolgaComp}" 
									checked="${beneficioParamInstance?.idVerificaFolgaComp == 'S'}" />
					</div>
				</div>
	
				<div class="control-group">
					<label class="control-label">Verifica Afastamentos:</label>
					<div class="controls">
						<g:checkBox name="idVerificaAfastamentos" value="${beneficioParamInstance?.idVerificaAfastamentos}" 
									checked="${beneficioParamInstance?.idVerificaAfastamentos == 'S'}" />
					</div>
				</div>
			</div>

		</fieldset>
		
		<fieldset>
			<legend>Filiais</legend>
			
			<button type="button" class="btn btn-medium btn-primary" onclick="marcarTodos();">Marcar todos</button>
			<button type="button" class="btn btn-medium btn-primary" onclick="desmarcarTodos();">Desmarcar todos</button>
			
			<br/>
			<br/>
			
			<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th>Filial</th>
					</tr>
				</thead>
				<tbody>
					<g:set var="fs" bean="filialService"/>
					<g:each in="${fs.filiaisDoUsuario()}" var="f" status="i">
						<tr>
							<td>
								<g:if test="${beneficioParamInstance.id != null && cadastros.BeneficioParamFilial.findByBeneficioParamAndFilial(beneficioParamInstance, f) != null}">
									<input type="checkbox" name="filial" id="filial" value="${f.id}" checked="checked"/>
								</g:if>
								<g:else>
									<input type="checkbox" name="filial" id="filial" value="${f.id}"/>
								</g:else>
							</td>
							<td>${f}</td>
						</tr>
					</g:each>
				</tbody>
			</table>
			
		</fieldset>
		
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>
	
	<g:render template="/busca/lovBuscaInterna"/>