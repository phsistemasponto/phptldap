
<%@ page import="cadastros.BeneficioParam" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'beneficioParam.label', default: 'BeneficioParam')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-beneficioParam" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-beneficioParam" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list beneficioParam">
			
				<g:if test="${beneficioParamInstance?.verbaFolha}">
				<li class="fieldcontain">
					<span id="verbaFolha-label" class="property-label"><g:message code="beneficioParam.verbaFolha.label" default="Verba Folha" /></span>
					
						<span class="property-value" aria-labelledby="verbaFolha-label"><g:fieldValue bean="${beneficioParamInstance}" field="verbaFolha"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idFuncioDemitido}">
				<li class="fieldcontain">
					<span id="idFuncioDemitido-label" class="property-label"><g:message code="beneficioParam.idFuncioDemitido.label" default="Id Funcio Demitido" /></span>
					
						<span class="property-value" aria-labelledby="idFuncioDemitido-label"><g:fieldValue bean="${beneficioParamInstance}" field="idFuncioDemitido"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idBenIncremento}">
				<li class="fieldcontain">
					<span id="idBenIncremento-label" class="property-label"><g:message code="beneficioParam.idBenIncremento.label" default="Id Ben Incremento" /></span>
					
						<span class="property-value" aria-labelledby="idBenIncremento-label"><g:fieldValue bean="${beneficioParamInstance}" field="idBenIncremento"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idBenDescontoDiario}">
				<li class="fieldcontain">
					<span id="idBenDescontoDiario-label" class="property-label"><g:message code="beneficioParam.idBenDescontoDiario.label" default="Id Ben Desconto Diario" /></span>
					
						<span class="property-value" aria-labelledby="idBenDescontoDiario-label"><g:fieldValue bean="${beneficioParamInstance}" field="idBenDescontoDiario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idVerificaMesAnterior}">
				<li class="fieldcontain">
					<span id="idVerificaMesAnterior-label" class="property-label"><g:message code="beneficioParam.idVerificaMesAnterior.label" default="Id Verifica Mes Anterior" /></span>
					
						<span class="property-value" aria-labelledby="idVerificaMesAnterior-label"><g:fieldValue bean="${beneficioParamInstance}" field="idVerificaMesAnterior"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idVerificaQtFaltas}">
				<li class="fieldcontain">
					<span id="idVerificaQtFaltas-label" class="property-label"><g:message code="beneficioParam.idVerificaQtFaltas.label" default="Id Verifica Qt Faltas" /></span>
					
						<span class="property-value" aria-labelledby="idVerificaQtFaltas-label"><g:fieldValue bean="${beneficioParamInstance}" field="idVerificaQtFaltas"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idVerificaFaltasAbo}">
				<li class="fieldcontain">
					<span id="idVerificaFaltasAbo-label" class="property-label"><g:message code="beneficioParam.idVerificaFaltasAbo.label" default="Id Verifica Faltas Abo" /></span>
					
						<span class="property-value" aria-labelledby="idVerificaFaltasAbo-label"><g:fieldValue bean="${beneficioParamInstance}" field="idVerificaFaltasAbo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idVerificaExtras}">
				<li class="fieldcontain">
					<span id="idVerificaExtras-label" class="property-label"><g:message code="beneficioParam.idVerificaExtras.label" default="Id Verifica Extras" /></span>
					
						<span class="property-value" aria-labelledby="idVerificaExtras-label"><g:fieldValue bean="${beneficioParamInstance}" field="idVerificaExtras"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idVerificaMesProcesso}">
				<li class="fieldcontain">
					<span id="idVerificaMesProcesso-label" class="property-label"><g:message code="beneficioParam.idVerificaMesProcesso.label" default="Id Verifica Mes Processo" /></span>
					
						<span class="property-value" aria-labelledby="idVerificaMesProcesso-label"><g:fieldValue bean="${beneficioParamInstance}" field="idVerificaMesProcesso"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idVerificaFeriado}">
				<li class="fieldcontain">
					<span id="idVerificaFeriado-label" class="property-label"><g:message code="beneficioParam.idVerificaFeriado.label" default="Id Verifica Feriado" /></span>
					
						<span class="property-value" aria-labelledby="idVerificaFeriado-label"><g:fieldValue bean="${beneficioParamInstance}" field="idVerificaFeriado"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idVerificaFolgaComp}">
				<li class="fieldcontain">
					<span id="idVerificaFolgaComp-label" class="property-label"><g:message code="beneficioParam.idVerificaFolgaComp.label" default="Id Verifica Folga Comp" /></span>
					
						<span class="property-value" aria-labelledby="idVerificaFolgaComp-label"><g:fieldValue bean="${beneficioParamInstance}" field="idVerificaFolgaComp"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.idVerificaAfastamentos}">
				<li class="fieldcontain">
					<span id="idVerificaAfastamentos-label" class="property-label"><g:message code="beneficioParam.idVerificaAfastamentos.label" default="Id Verifica Afastamentos" /></span>
					
						<span class="property-value" aria-labelledby="idVerificaAfastamentos-label"><g:fieldValue bean="${beneficioParamInstance}" field="idVerificaAfastamentos"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.qtExtras}">
				<li class="fieldcontain">
					<span id="qtExtras-label" class="property-label"><g:message code="beneficioParam.qtExtras.label" default="Qt Extras" /></span>
					
						<span class="property-value" aria-labelledby="qtExtras-label"><g:fieldValue bean="${beneficioParamInstance}" field="qtExtras"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.qtFaltas}">
				<li class="fieldcontain">
					<span id="qtFaltas-label" class="property-label"><g:message code="beneficioParam.qtFaltas.label" default="Qt Faltas" /></span>
					
						<span class="property-value" aria-labelledby="qtFaltas-label"><g:fieldValue bean="${beneficioParamInstance}" field="qtFaltas"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioParamInstance?.tipoDeBeneficio}">
				<li class="fieldcontain">
					<span id="tipoDeBeneficio-label" class="property-label"><g:message code="beneficioParam.tipoDeBeneficio.label" default="Tipo De Beneficio" /></span>
					
						<span class="property-value" aria-labelledby="tipoDeBeneficio-label"><g:link controller="tipoDeBeneficio" action="show" id="${beneficioParamInstance?.tipoDeBeneficio?.id}">${beneficioParamInstance?.tipoDeBeneficio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${beneficioParamInstance?.id}" />
					<g:link class="edit" action="edit" id="${beneficioParamInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
