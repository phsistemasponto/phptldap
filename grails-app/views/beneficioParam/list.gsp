
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Parâmetros de Benefícios</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Parâmetro de Benefício']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		  <th>Tipo de benefício</th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${beneficioParamInstanceList}" status="i" var="beneficioParamInstance">
        <tr>
          <td>${beneficioParamInstance.tipoDeBeneficio.descricao}</td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${beneficioParamInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/beneficioParam/delete', ${beneficioParamInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${beneficioParamInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${beneficioParamInstanceTotal}</span>
    </div>
  </body>
</html>