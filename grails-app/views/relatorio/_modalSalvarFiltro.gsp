<div id="divModalSalvarFiltro" style="display: none;" title="Salvar filtro">
<g:form id="formSalvarFiltroReport" class="form-filtro-report">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	);
	
	$j('.inputLovPadrao').keypress(
		function(e){
			if (e.keyCode == 13) {
				e.preventDefault();
				aplicaFiltroPadrao();
			}
		}
	);
	</script>
	
	<div class="control-group">
		<label class="control-label" style="font-weight: bold;">Filtro:</label>
		<div class="controls">
			<g:textField name="filtroNomeReport" value="${params.filtroNomeReport}" style="width:350px" />
		</div>
	</div>
	
	<br>
	
	<input type="button" class="btn btn-medium btn-primary" value="Salvar filtro" onclick="salvaFiltroReport();" />
	
</g:form>
</div>