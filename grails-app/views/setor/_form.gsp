<g:javascript src="setor/setor.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${setorInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${setorInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${setorInstance.id}">
		<form action="${request.contextPath}/setor/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${setorInstance?.id}" />
			<g:hiddenField name="version" value="${setorInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/setor/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscSetor" maxlength="80" required="" value="${setorInstance?.dscSetor}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Apelido:</label>
				<div class="controls">
					<g:textField name="aplSetor" maxlength="80" value="${setorInstance?.aplSetor}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filial:</label>
				<div class="controls">
					<g:set var="fs" bean="filialService"/>
					<g:select id="filial" name="filial.id" from="${fs.filiaisDoUsuario()}" optionKey="id" required="" value="${setorInstance?.filial?.id}" class="many-to-one"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>