
<html>
  <head>
    <meta name="layout" content="main">
    <g:javascript src="comum/lov.js"/>
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Setor</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Setor']" /></g:link>
    </div>
       
    <form action="${request.contextPath}/setor/list" method="POST" id="formPesquisa">
    	<div class="well">
  			<h3>Filtro</h3>
  			<div class="control-group">
				<label class="control-label">Tipo de filtro:</label>				
				<div class="controls">
					<g:select name="tipoFiltro" optionKey="id" optionValue="nome" 
							value="${params.tipoFiltro}"
							style="width:100px;"
							from="${[ [id:'0', nome:'...'],
									  [id:'1', nome:'Código'], 
									  [id:'2', nome:'Descrição'] ]}"/>
					<g:textField name="filtro" maxlength="250" value="${params.filtro}" style="margin-left: 10px; width: 400px;"/>
					<input type="hidden" id="order" name="order"/>
					<input type="hidden" id="sortType" name="sortType"/>
					<g:submitButton name="filtrar" class="btn btn-medium btn-primary" value="Filtrar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>		
          <th>
          	Descrição
          	<g:if test="${order != 'dscSetor'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('dscSetor', 'asc')"/></g:if>
          	<g:if test="${order == 'dscSetor' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('dscSetor', 'desc')"/></g:if>
          	<g:if test="${order == 'dscSetor' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('dscSetor', 'asc')"/></g:if>
          </th>
		  <th>
		  	Filial
		  	<g:if test="${order != 'filial.dscFil'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('filial.dscFil', 'asc')"/></g:if>
          	<g:if test="${order == 'filial.dscFil' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('filial.dscFil', 'desc')"/></g:if>
          	<g:if test="${order == 'filial.dscFil' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('filial.dscFil', 'asc')"/></g:if>
		  </th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${setorInstanceList}" status="i" var="setorInstance">
        <tr>
          <td>${fieldValue(bean: setorInstance, field: "dscSetor")}</td>
          <td>${fieldValue(bean: setorInstance, field: "filial")}</td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${setorInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/setor/delete', ${setorInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${setorInstanceTotal}" params="${params}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${setorInstanceTotal}</span>
    </div>
  </body>
</html>