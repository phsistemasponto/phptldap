
<%@ page import="cadastros.AdcNoturno" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'adcNoturno.label', default: 'AdcNoturno')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-adcNoturno" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-adcNoturno" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list adcNoturno">
			
				<g:if test="${adcNoturnoInstance?.dscAdc}">
				<li class="fieldcontain">
					<span id="dscAdc-label" class="property-label"><g:message code="adcNoturno.dscAdc.label" default="Dsc Adc" /></span>
					
						<span class="property-value" aria-labelledby="dscAdc-label"><g:fieldValue bean="${adcNoturnoInstance}" field="dscAdc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${adcNoturnoInstance?.hrInicio}">
				<li class="fieldcontain">
					<span id="hrInicio-label" class="property-label"><g:message code="adcNoturno.hrInicio.label" default="Hr Inicio" /></span>
					
						<span class="property-value" aria-labelledby="hrInicio-label"><g:fieldValue bean="${adcNoturnoInstance}" field="hrInicio"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${adcNoturnoInstance?.hrFim}">
				<li class="fieldcontain">
					<span id="hrFim-label" class="property-label"><g:message code="adcNoturno.hrFim.label" default="Hr Fim" /></span>
					
						<span class="property-value" aria-labelledby="hrFim-label"><g:fieldValue bean="${adcNoturnoInstance}" field="hrFim"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${adcNoturnoInstance?.minTol}">
				<li class="fieldcontain">
					<span id="minTol-label" class="property-label"><g:message code="adcNoturno.minTol.label" default="Min Tol" /></span>
					
						<span class="property-value" aria-labelledby="minTol-label"><g:fieldValue bean="${adcNoturnoInstance}" field="minTol"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${adcNoturnoInstance?.ocorrencias}">
				<li class="fieldcontain">
					<span id="ocorrencias-label" class="property-label"><g:message code="adcNoturno.ocorrencias.label" default="Ocorrencias" /></span>
					
						<span class="property-value" aria-labelledby="ocorrencias-label"><g:link controller="ocorrencias" action="show" id="${adcNoturnoInstance?.ocorrencias?.id}">${adcNoturnoInstance?.ocorrencias?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${adcNoturnoInstance?.id}" />
					<g:link class="edit" action="edit" id="${adcNoturnoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
