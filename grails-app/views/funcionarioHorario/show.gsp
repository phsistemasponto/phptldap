
<%@ page import="cadastros.FuncionarioHorario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'funcionarioHorario.label', default: 'FuncionarioHorario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-funcionarioHorario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-funcionarioHorario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list funcionarioHorario">
			
				<g:if test="${funcionarioHorarioInstance?.dtFimVig}">
				<li class="fieldcontain">
					<span id="dtFimVig-label" class="property-label"><g:message code="funcionarioHorario.dtFimVig.label" default="Dt Fim Vig" /></span>
					
						<span class="property-value" aria-labelledby="dtFimVig-label"><g:formatDate date="${funcionarioHorarioInstance?.dtFimVig}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHorarioInstance?.nrIndHorario}">
				<li class="fieldcontain">
					<span id="nrIndHorario-label" class="property-label"><g:message code="funcionarioHorario.nrIndHorario.label" default="Nr Ind Horario" /></span>
					
						<span class="property-value" aria-labelledby="nrIndHorario-label"><g:fieldValue bean="${funcionarioHorarioInstance}" field="nrIndHorario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHorarioInstance?.dtIniVig}">
				<li class="fieldcontain">
					<span id="dtIniVig-label" class="property-label"><g:message code="funcionarioHorario.dtIniVig.label" default="Dt Ini Vig" /></span>
					
						<span class="property-value" aria-labelledby="dtIniVig-label"><g:formatDate date="${funcionarioHorarioInstance?.dtIniVig}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHorarioInstance?.filial}">
				<li class="fieldcontain">
					<span id="filial-label" class="property-label"><g:message code="funcionarioHorario.filial.label" default="Filial" /></span>
					
						<span class="property-value" aria-labelledby="filial-label"><g:link controller="filial" action="show" id="${funcionarioHorarioInstance?.filial?.id}">${funcionarioHorarioInstance?.filial?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHorarioInstance?.funcionario}">
				<li class="fieldcontain">
					<span id="funcionario-label" class="property-label"><g:message code="funcionarioHorario.funcionario.label" default="Funcionario" /></span>
					
						<span class="property-value" aria-labelledby="funcionario-label"><g:link controller="funcionario" action="show" id="${funcionarioHorarioInstance?.funcionario?.id}">${funcionarioHorarioInstance?.funcionario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${funcionarioHorarioInstance?.horario}">
				<li class="fieldcontain">
					<span id="horario-label" class="property-label"><g:message code="funcionarioHorario.horario.label" default="Horario" /></span>
					
						<span class="property-value" aria-labelledby="horario-label"><g:link controller="horario" action="show" id="${funcionarioHorarioInstance?.horario?.id}">${funcionarioHorarioInstance?.horario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${funcionarioHorarioInstance?.id}" />
					<g:link class="edit" action="edit" id="${funcionarioHorarioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
