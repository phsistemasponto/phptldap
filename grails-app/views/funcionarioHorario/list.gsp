
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>FuncionarioHorarios</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['FuncionarioHorario']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Dt Fim Vig</th>
		
          <th>Nr Ind Horario</th>
		
          <th>Dt Ini Vig</th>
		
          <th>Filial</th>
		
          <th>Funcionario</th>
		
          <th>Horario</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${funcionarioHorarioInstanceList}" status="i" var="funcionarioHorarioInstance">
        <tr>
		
          <td>${fieldValue(bean: funcionarioHorarioInstance, field: "dtFimVig")}</td>
		
          <td>${fieldValue(bean: funcionarioHorarioInstance, field: "nrIndHorario")}</td>
		
          <td><g:formatDate date="${funcionarioHorarioInstance.dtIniVig}" /></td>
		
          <td>${fieldValue(bean: funcionarioHorarioInstance, field: "filial")}</td>
		
          <td>${fieldValue(bean: funcionarioHorarioInstance, field: "funcionario")}</td>
		
          <td>${fieldValue(bean: funcionarioHorarioInstance, field: "horario")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${funcionarioHorarioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/funcionariohorario/delete', ${funcionarioHorarioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${funcionarioHorarioInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${funcionarioHorarioInstanceTotal}</span>
    </div>
  </body>
</html>