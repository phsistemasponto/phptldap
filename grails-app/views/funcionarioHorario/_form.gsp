

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${funcionarioHorarioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${funcionarioHorarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${funcionarioHorarioInstance.id}">
		<form action="${request.contextPath}/funcionariohorario/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${funcionarioHorarioInstance?.id}" />
			<g:hiddenField name="version" value="${funcionarioHorarioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/funcionariohorario/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Dt Fim Vig:</label>
				<div class="controls">
					<g:datePicker name="dtFimVig" precision="day"  value="${funcionarioHorarioInstance?.dtFimVig}" default="none" noSelection="['': '']" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Nr Ind Horario:</label>
				<div class="controls">
					<g:textField name="nrIndHorario" value="${fieldValue(bean: funcionarioHorarioInstance, field: 'nrIndHorario')}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Dt Ini Vig:</label>
				<div class="controls">
					<g:datePicker name="dtIniVig" precision="day"  value="${funcionarioHorarioInstance?.dtIniVig}"  />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filial:</label>
				<div class="controls">
					<g:set var="fs" bean="filialService"/>
					<g:select id="filial" name="filial.id" from="${fs.filiaisDoUsuario()}" optionKey="id" required="" value="${funcionarioHorarioInstance?.filial?.id}" class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Funcionario:</label>
				<div class="controls">
					<g:select id="funcionario" name="funcionario.id" from="${cadastro.Funcionario.list()}" optionKey="id" required="" value="${funcionarioHorarioInstance?.funcionario?.id}" class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Horario:</label>
				<div class="controls">
					<g:select id="horario" name="horario.id" from="${cadastro.Horario.list()}" optionKey="id" required="" value="${funcionarioHorarioInstance?.horario?.id}" class="many-to-one"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>