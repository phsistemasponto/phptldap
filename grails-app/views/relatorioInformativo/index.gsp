<%@page import="cadastros.Classificacao"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    <g:javascript src="reports/informativo.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Relatórios informativos</h3>
    </div>
    
    <g:jasperForm controller="relatorioInformativo" action="generateReport" jasper="informativo" class="form-horizontal" id="formPesquisa">
    
    	<div class="well">
    	
    		<div class="well" style="border: 1px solid #859CA4; background-color: #859CA4;">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					</div>
				</div>
			</div>
			
		    <div class="control-group">
				<label class="control-label" style="font-weight: bold;">Data início:</label>
				<div class="controls">
					<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Data fim:</label>
				<div class="controls">
					<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" />
				</div>
			</div>  
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Relatório:</label>
				<div class="controls">
					<g:select name="relatorio" id="relatorio" optionKey="id" optionValue="nome" 
							value="${params.relatorio}"
							style="width:200px;"
							from="${[ [id:'', nome:'...'],
									  [id:'0', nome:'Conferência da folha'],
									  [id:'1', nome:'Informativo de fechamento da folha p/ funcionário'],
									  [id:'2', nome:'Conferência de funcionários que tiveram'],
									  [id:'3', nome:'Conferência de funcionários que não tiveram'],
									  [id:'4', nome:'Conferência de excesso de Jornada'],
									  [id:'5', nome:'Relatório de produtividade']  ]}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Agrupamento:</label>
				<div class="controls">
					<g:select name="agrupamento" id="agrupamento" optionKey="id" optionValue="nome" 
							value="${params.agrupamento}"
							style="width:200px;"
							from="${[ [id:'0', nome:'Analítico'],
									  [id:'1', nome:'Sintético']  ]}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Classificação:</label>
				<div class="controls">
					<g:select id="classificacao" name="classificacao" from="${Classificacao.findAll()}" optionKey="idTpDia" style="width:220px;" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Ordenação:</label>
				<div class="controls">
					<g:select name="ordenacao" id="ordenacao" optionKey="id" optionValue="nome" 
							value="${params.ordenacao}"
							style="width:200px;"
							from="${[ [id:'1', nome:'Código'],
									  [id:'2', nome:'Descrição'] ]}"/>
				</div>
			</div>
			
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('PDF'); else return false;">Gerar em PDF</button>
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('XLS'); else return false;">Gerar em Excel</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

	<div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>