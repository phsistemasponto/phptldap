
	<g:javascript src="feriadosRegionais/feriadosRegionais.js"/>
	<g:javascript src="jquery.meiomask.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${feriadosRegionaisInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${feriadosRegionaisInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${feriadosRegionaisInstance.id}">
		<form action="${request.contextPath}/feriadosRegionais/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${feriadosRegionaisInstance?.id}" />
			<g:hiddenField name="version" value="${feriadosRegionaisInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/feriadosRegionais/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Data:</label>
				<div class="controls">
					<g:textField name="dataFeriado" required="" value="${feriadosRegionaisInstance?.dtFerExibicao}" style="width: 80px;"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Tipo:</label>
				<div class="controls">
					<g:select name="tipo" optionKey="id" optionValue="nome" 
							value="${feriadosRegionaisInstance?.tipo}"
							onchange="alteraTipoFeriado();"
							from="${[ [id:'F', nome:'Fixo'],
									  [id:'M', nome:'Móvel'] ]}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Parcial:</label>
				<div class="controls">
					<g:checkBox name="idParcial" value="${feriadosRegionaisInstance?.idParcial}" 
						checked="${feriadosRegionaisInstance?.idParcial == 'S'}" disabled="disabled"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscFer" maxlength="80" required="" value="${feriadosRegionaisInstance?.dscFer}"/>
				</div>
			</div>

		</fieldset>

		<hr>
		<h4>Filiais</h4>
		<br />
		<div>
			<g:if test="${feriadosRegionaisInstance.id}">
				<table class="table table-striped table-bordered table-condensed"
					id="filiaisTable">
					<thead>
						<tr>
							<th style="width: 65%;">Filial</th>
							<th style="width: 15%;">Possui Feriado</th>
							<th style="width: 20%;"></th>
						</tr>
					</thead>
	
					<tbody>
						<g:each in="${filiais?}" status="i" var="item">
							<tr id="rowTableFilial${i}" class="even">
								<td><g:hiddenField name="filialId" value="${item?.filial?.id}" />
									<g:hiddenField name="temFilialFeriadoRegional" value="${item?.temFilial}" />
									<g:textField name="descricaoFilial" style="width:90%" value='${item?.filial?.dscFil+" - "+item?.filial?.unicodFilial}' readOnly="true" />
								</td>
								
								<td>
									<g:if test="${item?.temFilial=='true'}">
										<g:checkBox name="temFilial${i}" value="${true}" onChange="atualizaFilial(${i});"/>
									</g:if>
									<g:else>  
										<g:checkBox name="temFilial${i}" value="${false}"  onChange="atualizaFilial(${i});"/>
									</g:else>										
								</td>
								<td/>
							</tr>
						</g:each>
					</tbody>
				</table>
			
			</g:if>
			<g:else>
				<table class="table table-striped table-bordered table-condensed"
					id="filiaisTable">
					<thead>
						<tr>
							<th style="width: 50%;">Filial</th>
							<th style="width: 50%;">&nbsp; <button class="btn" type="button" onClick="marcarTodos();">Marcar Todos</button> <button class="btn" type="button" onClick="desmarcarTodos();">Desmarcar Todos</button></th>
						</tr>
					</thead>
	
					<tbody>
						<g:each in="${filiais?}" status="i" var="item">
							<tr id="rowTableFilial${i}" class="even">
								<td><g:hiddenField name="filialId" value="${item?.filial?.id}" />
									<g:hiddenField name="temFilialFeriadoRegional" value="${item?.temFilial}" />
									<g:textField name="descricaoFilial" style="width:90%" value='${item?.filial?.dscFil+" - "+item?.filial?.unicodFilial}' readOnly="true" />
								</td>
								<td><g:checkBox name="temFilial${i}" value="${item?.temFilial}" onChange="atualizaFilial(${i});"/></td>
								<td/>
							</tr>
						</g:each>
					</tbody>
				</table>
			</g:else>
		</div>
		<hr>
		
		
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>