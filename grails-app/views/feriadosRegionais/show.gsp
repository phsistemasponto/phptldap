
<%@ page import="cadastros.FeriadosRegionais" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'feriadosRegionais.label', default: 'FeriadosRegionais')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-feriadosRegionais" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-feriadosRegionais" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list feriadosRegionais">
			
				<g:if test="${feriadosRegionaisInstance?.dscFer}">
				<li class="fieldcontain">
					<span id="dscFer-label" class="property-label"><g:message code="feriadosRegionais.dscFer.label" default="Dsc Fer" /></span>
					
						<span class="property-value" aria-labelledby="dscFer-label"><g:fieldValue bean="${feriadosRegionaisInstance}" field="dscFer"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${feriadosRegionaisInstance?.tipo}">
				<li class="fieldcontain">
					<span id="tipo-label" class="property-label"><g:message code="feriadosRegionais.tipo.label" default="Tipo" /></span>
					
						<span class="property-value" aria-labelledby="tipo-label"><g:fieldValue bean="${feriadosRegionaisInstance}" field="tipo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${feriadosRegionaisInstance?.dtFer}">
				<li class="fieldcontain">
					<span id="dtFer-label" class="property-label"><g:message code="feriadosRegionais.dtFer.label" default="Dt Fer" /></span>
					
						<span class="property-value" aria-labelledby="dtFer-label"><g:formatDate date="${feriadosRegionaisInstance?.dtFer}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${feriadosRegionaisInstance?.id}" />
					<g:link class="edit" action="edit" id="${feriadosRegionaisInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
