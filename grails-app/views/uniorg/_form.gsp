

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${uniorgInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${uniorgInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${uniorgInstance.id}">
		<form action="${request.contextPath}/uniorg/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${uniorgInstance?.id}" />
			<g:hiddenField name="version" value="${uniorgInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/uniorg/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Dsc Uniorg:</label>
				<div class="controls">
					<g:textField name="dscUniorg" maxlength="80" required="" value="${uniorgInstance?.dscUniorg}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>