

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${tipohoraextrasInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${tipohoraextrasInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${tipohoraextrasInstance.id}">
		<form action="${request.contextPath}/tipohoraextras/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${tipohoraextrasInstance?.id}" />
			<g:hiddenField name="version" value="${tipohoraextrasInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/tipohoraextras/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscTpHorExt" maxlength="30" required="" value="${tipohoraextrasInstance?.dscTpHorExt}"/>
				</div>
			</div>

			
			<div class="control-group">
				<label class="control-label">Tipo da Extra:</label>
				<div class="controls">
				<select name="tipExt">
                 <option value="D">Diurno</option>
				 <option value="N">Noturno</option>					
  				</select>
				
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Arquivo Acjef:</label>
				<div class="controls">
					<g:textField name="nmAcjef" maxlength="10" required="" value="${tipohoraextrasInstance?.nmAcjef}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Indice da extra:</label>
				<div class="controls">
					<g:textField name="vrIndHorExt" required="" value="${fieldValue(bean: tipohoraextrasInstance, field: 'vrIndHorExt')}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>