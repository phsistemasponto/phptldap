
<%@ page import="cadastros.Tipohoraextras" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipohoraextras.label', default: 'Tipohoraextras')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipohoraextras" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipohoraextras" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipohoraextras">
			
				<g:if test="${tipohoraextrasInstance?.dscTpHorExt}">
				<li class="fieldcontain">
					<span id="dscTpHorExt-label" class="property-label"><g:message code="tipohoraextras.dscTpHorExt.label" default="Dsc Tp Hor Ext" /></span>
					
						<span class="property-value" aria-labelledby="dscTpHorExt-label"><g:fieldValue bean="${tipohoraextrasInstance}" field="dscTpHorExt"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipohoraextrasInstance?.formulaTpExt}">
				<li class="fieldcontain">
					<span id="formulaTpExt-label" class="property-label"><g:message code="tipohoraextras.formulaTpExt.label" default="Formula Tp Ext" /></span>
					
						<span class="property-value" aria-labelledby="formulaTpExt-label"><g:fieldValue bean="${tipohoraextrasInstance}" field="formulaTpExt"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipohoraextrasInstance?.tipExt}">
				<li class="fieldcontain">
					<span id="tipExt-label" class="property-label"><g:message code="tipohoraextras.tipExt.label" default="Tip Ext" /></span>
					
						<span class="property-value" aria-labelledby="tipExt-label"><g:fieldValue bean="${tipohoraextrasInstance}" field="tipExt"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipohoraextrasInstance?.nmAcjef}">
				<li class="fieldcontain">
					<span id="nmAcjef-label" class="property-label"><g:message code="tipohoraextras.nmAcjef.label" default="Nm Acjef" /></span>
					
						<span class="property-value" aria-labelledby="nmAcjef-label"><g:fieldValue bean="${tipohoraextrasInstance}" field="nmAcjef"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipohoraextrasInstance?.vrIndHorExt}">
				<li class="fieldcontain">
					<span id="vrIndHorExt-label" class="property-label"><g:message code="tipohoraextras.vrIndHorExt.label" default="Vr Ind Hor Ext" /></span>
					
						<span class="property-value" aria-labelledby="vrIndHorExt-label"><g:fieldValue bean="${tipohoraextrasInstance}" field="vrIndHorExt"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${tipohoraextrasInstance?.id}" />
					<g:link class="edit" action="edit" id="${tipohoraextrasInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
