
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Tipo Horas Extras</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Tipohoraextras']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Descrição</th>
		          	
          <th>Tipo Extra</th>
		
          <th>arquivo acjf</th>
		
          <th>Indice a extra</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${tipohoraextrasInstanceList}" status="i" var="tipohoraextrasInstance">
        <tr>
		
          <td>${fieldValue(bean: tipohoraextrasInstance, field: "dscTpHorExt")}</td>
		
          		
          <td>${fieldValue(bean: tipohoraextrasInstance, field: "tipExt")}</td>
		
          <td>${fieldValue(bean: tipohoraextrasInstance, field: "nmAcjef")}</td>
		
          <td>${fieldValue(bean: tipohoraextrasInstance, field: "vrIndHorExt")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${tipohoraextrasInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/tipohoraextras/delete', ${tipohoraextrasInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${tipohoraextrasInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${tipohoraextrasInstanceTotal}</span>
    </div>
  </body>
</html>