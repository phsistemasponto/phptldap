<g:javascript src="municipio/municipio.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${municipioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${municipioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${municipioInstance.id}">
		<form action="${request.contextPath}/municipio/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${municipioInstance?.id}" />
			<g:hiddenField name="version" value="${municipioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/municipio/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Código:</label>
				<div class="controls">
					<g:textField name="codMun" maxlength="12" required="" value="${municipioInstance?.codMun}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Descricao:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="80" required="" value="${municipioInstance?.descricao}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Sigla:</label>
				<div class="controls">
					<g:textField name="sgl_uf" maxlength="2" value="${municipioInstance?.sgl_uf}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>