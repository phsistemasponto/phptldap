<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
  
    <div class="titulo">
      <h3>Agendamento de processo</h3>
    </div>
    
	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="agendamentoProcesso/agendamentoProcesso.js"/>
	
	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${administracaoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${administracaoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	
	<form action="${request.contextPath}/agendamentoProcesso/save" class="well form-horizontal" method="POST">
		
		<fieldset>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Dia da semana:</label>
				<div class="controls">
					<g:select name="diaSemana" id="diaSemana" optionKey="id" optionValue="nome" 
							value="${params.diaSemana}"
							style="width:200px;"
							from="${[ [id:'0', nome:'Domingo'],
									  [id:'1', nome:'Segunda'],
									  [id:'2', nome:'Terça'],
									  [id:'3', nome:'Quarta'],
									  [id:'4', nome:'Quinta'],
									  [id:'5', nome:'Sexta'],
									  [id:'6', nome:'Sábado']  ]}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Hora:</label>
				<div class="controls">
					<g:textField name="hora" id="hora" value="${params.hora}" style="width:80px" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Filiais:</label>
				<div class="controls">
					<g:set var="fs" bean="filialService"/>
					<g:select name="filiais" from="${fs.filiaisDoUsuario()}" optionKey="id" multiple="true"/>
				</div>
			</div>
			
			<button type="button" class="btn btn-medium btn-primary" onclick="incluir();">Incluir</button>
			
			<br><br><br>
			
			<table class="table table-striped table-bordered table-condensed" id="agendamentoTable">
	    		<thead>
	    			<tr>
	    				<th>Dia da semana</th>
	    				<th>Hora</th>
	    				<th>&nbsp;</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    		<g:each in="${agendamentos}" var="agendamento" status="j">
		    		<tr id="rowTableAgendamento-${j+1}" class="even" style="background-color: #00AFFF">
	    				<td style="background-color: #00AFFF">
	    					<g:textField name="diaSemanaDisplay" value="${agendamento.diaSemanaExibicao}" readOnly="true"/>
	    					<input type="hidden" name="diaSemanaId-${j+1}" id="diaSemanaId" value="${agendamento.diaSemana}">
	    				</td>
	    				<td style="background-color: #00AFFF">
	    					<g:textField name="horaExecucaoDisplay" value="${agendamento.horaExecucaoExibicao}" readOnly="true"/>
	    					<input type="hidden" name="horaExecucaoId-${j+1}" id="horaExecucaoId" value="${agendamento.horaExecucaoExibicao}">
	    				</td>
	    				<td style="background-color: #00AFFF">
	    					<i class="icon-remove" onclick="excluirAgendamento(${j+1});" style="cursor: pointer" title="Excluir registro"></i>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td colspan="3" style="padding:20px">
	    					<table class="table table-striped table-bordered table-condensed">
	    						<thead>
	    							<tr>
	    								<th>Filial</th>
	    							</tr>
	    						</thead>
	    						<tbody>
	    							<g:each in="${agendamento.filiais}" var="filial">
	    								<tr>
	    									<td>
	    										${filial.filial}
	    										<input type="hidden" name="filialId-${j+1}" id="filialId-${j+1}" value="${filial.filial.id}"/>
	    									</td>
	    								</tr>
	    							</g:each>
	    						</tbody>
	    					</table>
	    				</td>
	    			</tr>
	    		</g:each>
	    	</table>
			
		</fieldset>
		
		
		
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<a class="btn btn-large" href="${request.contextPath}/">Retornar</a>
		
	</form>
	
	
  </body>
</html>