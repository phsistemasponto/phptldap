
<html>
  <head>
  	<meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="batidaPonto/batidaPonto.js"/>
  	
    <g:if test="${request.message}">
    <div class="alert alert-info" role="status">${request.message}</div>
    </g:if>

    <g:if test="${request.error}">
    <div class="alert alert-error" role="status">${request.error}</div>
    </g:if>

    
    
    <form action="${request.contextPath}/batidaPonto/baterPonto" method="POST" id="formBatidaPonto" class="well">
    
    	<div class="titulo">
	      <h3>Batida de ponto</h3>
	    </div>
	    
	    <div style="border: 1px solid blue; width:100%; height: 0px;"></div>
	    
	    <br/>
    
    	<div class="control-group">
			<label class="control-label">PIS:</label>
			<div class="controls">
				<input type="text" name="pis" id="pis"/>
			</div>
		</div>
		
		<button type="button" class="btn btn-medium btn-primary" onclick="baterPonto();">Bater ponto</button>
		
	</form>
	
  </body>
</html>