
<html>
  <head>
  	<meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="batidaPonto/batidaPonto.js"/>
  	
    <g:if test="${request.message}">
    <div class="alert alert-info" role="status">${request.message}</div>
    </g:if>

    <g:if test="${request.error}">
    <div class="alert alert-error" role="status">${request.error}</div>
    </g:if>

    <g:if test="${funcionarioNome}">
    	<h3>${funcionarioNome}</h3><br>
    	<h4>${horarioBatida}</h4><br>
    </g:if>
    
    <button id="btnRetornar" type="button" class="btn btn-medium btn-primary" onclick="javascript: history.go(-1);">Retornar</button>
    
    <script>
    	setTimeout(function() { $j('#btnRetornar').click(); }, 1000*10);
	</script>
	
  </body>
</html>