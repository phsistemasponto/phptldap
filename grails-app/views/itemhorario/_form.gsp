

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${itemhorarioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${itemhorarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${itemhorarioInstance.id}">
		<form action="${request.contextPath}/itemhorario/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${itemhorarioInstance?.id}" />
			<g:hiddenField name="version" value="${itemhorarioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/itemhorario/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Idvira:</label>
				<div class="controls">
					<g:textField name="idvira" maxlength="2" required="" value="${itemhorarioInstance?.idvira}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Dtinivig:</label>
				<div class="controls">
					<g:datePicker name="dtinivig" precision="day"  value="${itemhorarioInstance?.dtinivig}"  />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Classificacao:</label>
				<div class="controls">
					<g:select id="classificacao" name="classificacao.id" from="${cadastros.Classificacao.list()}" optionKey="id" required="" value="${itemhorarioInstance?.classificacao?.id}" class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Horario:</label>
				<div class="controls">
					<g:select id="horario" name="horario.id" from="${cadastros.Horario.list()}" optionKey="id" required="" value="${itemhorarioInstance?.horario?.id}" class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Nrdiasemana:</label>
				<div class="controls">
					<g:textField name="nrdiasemana" required="" value="${fieldValue(bean: itemhorarioInstance, field: 'nrdiasemana')}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Perfilhorario:</label>
				<div class="controls">
					<g:select id="perfilhorario" name="perfilhorario.id" from="${cadastros.Perfilhorario.list()}" optionKey="id" required="" value="${itemhorarioInstance?.perfilhorario?.id}" class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Qtdechefetiva:</label>
				<div class="controls">
					<g:textField name="qtdechefetiva" required="" value="${fieldValue(bean: itemhorarioInstance, field: 'qtdechefetiva')}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Seqitemhorario:</label>
				<div class="controls">
					<g:textField name="seqitemhorario" required="" value="${fieldValue(bean: itemhorarioInstance, field: 'seqitemhorario')}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>