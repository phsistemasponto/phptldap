
<%@ page import="cadastros.Itemhorario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'itemhorario.label', default: 'Itemhorario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-itemhorario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-itemhorario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list itemhorario">
			
				<g:if test="${itemhorarioInstance?.idvira}">
				<li class="fieldcontain">
					<span id="idvira-label" class="property-label"><g:message code="itemhorario.idvira.label" default="Idvira" /></span>
					
						<span class="property-value" aria-labelledby="idvira-label"><g:fieldValue bean="${itemhorarioInstance}" field="idvira"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemhorarioInstance?.dtinivig}">
				<li class="fieldcontain">
					<span id="dtinivig-label" class="property-label"><g:message code="itemhorario.dtinivig.label" default="Dtinivig" /></span>
					
						<span class="property-value" aria-labelledby="dtinivig-label"><g:formatDate date="${itemhorarioInstance?.dtinivig}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemhorarioInstance?.classificacao}">
				<li class="fieldcontain">
					<span id="classificacao-label" class="property-label"><g:message code="itemhorario.classificacao.label" default="Classificacao" /></span>
					
						<span class="property-value" aria-labelledby="classificacao-label"><g:link controller="classificacao" action="show" id="${itemhorarioInstance?.classificacao?.id}">${itemhorarioInstance?.classificacao?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemhorarioInstance?.horario}">
				<li class="fieldcontain">
					<span id="horario-label" class="property-label"><g:message code="itemhorario.horario.label" default="Horario" /></span>
					
						<span class="property-value" aria-labelledby="horario-label"><g:link controller="horario" action="show" id="${itemhorarioInstance?.horario?.id}">${itemhorarioInstance?.horario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemhorarioInstance?.nrdiasemana}">
				<li class="fieldcontain">
					<span id="nrdiasemana-label" class="property-label"><g:message code="itemhorario.nrdiasemana.label" default="Nrdiasemana" /></span>
					
						<span class="property-value" aria-labelledby="nrdiasemana-label"><g:fieldValue bean="${itemhorarioInstance}" field="nrdiasemana"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemhorarioInstance?.perfilhorario}">
				<li class="fieldcontain">
					<span id="perfilhorario-label" class="property-label"><g:message code="itemhorario.perfilhorario.label" default="Perfilhorario" /></span>
					
						<span class="property-value" aria-labelledby="perfilhorario-label"><g:link controller="perfilhorario" action="show" id="${itemhorarioInstance?.perfilhorario?.id}">${itemhorarioInstance?.perfilhorario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemhorarioInstance?.qtdechefetiva}">
				<li class="fieldcontain">
					<span id="qtdechefetiva-label" class="property-label"><g:message code="itemhorario.qtdechefetiva.label" default="Qtdechefetiva" /></span>
					
						<span class="property-value" aria-labelledby="qtdechefetiva-label"><g:fieldValue bean="${itemhorarioInstance}" field="qtdechefetiva"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemhorarioInstance?.seqitemhorario}">
				<li class="fieldcontain">
					<span id="seqitemhorario-label" class="property-label"><g:message code="itemhorario.seqitemhorario.label" default="Seqitemhorario" /></span>
					
						<span class="property-value" aria-labelledby="seqitemhorario-label"><g:fieldValue bean="${itemhorarioInstance}" field="seqitemhorario"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${itemhorarioInstance?.id}" />
					<g:link class="edit" action="edit" id="${itemhorarioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
