
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Itemhorarios</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Itemhorario']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Idvira</th>
		
          <th>Dtinivig</th>
		
          <th>Classificacao</th>
		
          <th>Horario</th>
		
          <th>Nrdiasemana</th>
		
          <th>Perfilhorario</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${itemhorarioInstanceList}" status="i" var="itemhorarioInstance">
        <tr>
		
          <td>${fieldValue(bean: itemhorarioInstance, field: "idvira")}</td>
		
          <td><g:formatDate date="${itemhorarioInstance.dtinivig}" /></td>
		
          <td>${fieldValue(bean: itemhorarioInstance, field: "classificacao")}</td>
		
          <td>${fieldValue(bean: itemhorarioInstance, field: "horario")}</td>
		
          <td>${fieldValue(bean: itemhorarioInstance, field: "nrdiasemana")}</td>
		
          <td>${fieldValue(bean: itemhorarioInstance, field: "perfilhorario")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${itemhorarioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/itemhorario/delete', ${itemhorarioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${itemhorarioInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${itemhorarioInstanceTotal}</span>
    </div>
  </body>
</html>