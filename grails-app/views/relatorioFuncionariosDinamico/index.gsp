
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Relatório de funcionários</h3>
    </div>    
    <g:jasperForm controller="relatorioFuncionariosDinamico" action="generateReport" jasper="funcionariosDinamico" class="form-horizontal" id="formPesquisa">
    
    	<div class="well">
    	
    		<div class="well" style="border: 1px solid #859CA4; background-color: #859CA4;">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					</div>
				</div>
			</div>
			
			<table class="table table-striped table-bordered table-condensed">
	    		<tbody>
	    			<tr>
	    				<td width="1%"><input type="checkbox" name="matricula" id="matricula" checked="checked"/></td>
	    				<td width="49%">Matricula</td>
	    				<td width="1%"><input type="checkbox" name="setor" id="setor"/></td>
	    				<td width="49%">Setor</td>
	    			</tr>
	    			<tr>
	    				<td><input type="checkbox" name="cracha" id="cracha" checked="checked"/></td>
	    				<td>Crachá</td>
	    				<td><input type="checkbox" name="nascimento" id="nascimento"/></td>
	    				<td>Nascimento</td>
	    			</tr>
	    			<tr>
	    				<td><input type="checkbox" name="nome" id="nome" checked="checked"/></td>
	    				<td>Nome do funcionário</td>
	    				<td><input type="checkbox" name="admissao" id="admissao"/></td>
	    				<td>Admissão</td>
	    			</tr>
	    			<tr>
	    				<td><input type="checkbox" name="filial" id="filial"/></td>
	    				<td>Filial</td>
	    				<td><input type="checkbox" name="rescisao" id="rescisao"/></td>
	    				<td>Rescisão</td>
	    			</tr>
	    			<tr>
	    				<td><input type="checkbox" name="categoria" id="categoria"/></td>
	    				<td>Categoria</td>
	    				<td><input type="checkbox" name="funcao" id="funcao"/></td>
	    				<td>Função</td>
	    			</tr>
	    			<tr>
	    				<td><input type="checkbox" name="cargo" id="cargo"/></td>
	    				<td>Cargo</td>
	    			</tr>
	    		</tbody>
	    	</table>
	    	
	    	<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Ordenação:</label>
				<div class="controls">
					<g:select name="ordenacao" id="ordenacao" optionKey="id" optionValue="nome" 
							value="${params.ordenacao}"
							style="width:200px;"
							from="${[ [id:'1', nome:'Código'],
									  [id:'2', nome:'Descrição'] ]}"/>
				</div>
			</div>
				    
			<button class="btn btn-medium btn-primary" onclick="generateReport('PDF')">Gerar em PDF</button>
			<button class="btn btn-medium btn-primary" onclick="generateReport('XLS')">Gerar em Excel</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>