
<%@ page import="cadastros.Cid" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cid.label', default: 'Cid')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-cid" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-cid" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list cid">
			
				<g:if test="${cidInstance?.codCid}">
				<li class="fieldcontain">
					<span id="codCid-label" class="property-label"><g:message code="cid.codCid.label" default="Cod Cid" /></span>
					
						<span class="property-value" aria-labelledby="codCid-label"><g:fieldValue bean="${cidInstance}" field="codCid"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cidInstance?.dscCid}">
				<li class="fieldcontain">
					<span id="dscCid-label" class="property-label"><g:message code="cid.dscCid.label" default="Dsc Cid" /></span>
					
						<span class="property-value" aria-labelledby="dscCid-label"><g:fieldValue bean="${cidInstance}" field="dscCid"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${cidInstance?.id}" />
					<g:link class="edit" action="edit" id="${cidInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
