
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Cids</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Cid']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Cod Cid</th>
		
          <th>Dsc Cid</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${cidInstanceList}" status="i" var="cidInstance">
        <tr>
		
          <td>${fieldValue(bean: cidInstance, field: "codCid")}</td>
		
          <td>${fieldValue(bean: cidInstance, field: "dscCid")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${cidInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/cid/delete', ${cidInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${cidInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${cidInstanceTotal}</span>
    </div>
  </body>
</html>