
<%@ page import="cadastros.Filial" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'filial.label', default: 'Filial')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-filial" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-filial" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list filial">
			
				<g:if test="${filialInstance?.dscFil}">
				<li class="fieldcontain">
					<span id="dscFil-label" class="property-label"><g:message code="filial.dscFil.label" default="Dsc Fil" /></span>
					
						<span class="property-value" aria-labelledby="dscFil-label"><g:fieldValue bean="${filialInstance}" field="dscFil"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.cnpjFil}">
				<li class="fieldcontain">
					<span id="cnpjFil-label" class="property-label"><g:message code="filial.cnpjFil.label" default="Cnpj Fil" /></span>
					
						<span class="property-value" aria-labelledby="cnpjFil-label"><g:fieldValue bean="${filialInstance}" field="cnpjFil"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.dscFantFil}">
				<li class="fieldcontain">
					<span id="dscFantFil-label" class="property-label"><g:message code="filial.dscFantFil.label" default="Dsc Fant Fil" /></span>
					
						<span class="property-value" aria-labelledby="dscFantFil-label"><g:fieldValue bean="${filialInstance}" field="dscFantFil"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.dscLog}">
				<li class="fieldcontain">
					<span id="dscLog-label" class="property-label"><g:message code="filial.dscLog.label" default="Dsc Log" /></span>
					
						<span class="property-value" aria-labelledby="dscLog-label"><g:fieldValue bean="${filialInstance}" field="dscLog"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.cmpLog}">
				<li class="fieldcontain">
					<span id="cmpLog-label" class="property-label"><g:message code="filial.cmpLog.label" default="Cmp Log" /></span>
					
						<span class="property-value" aria-labelledby="cmpLog-label"><g:fieldValue bean="${filialInstance}" field="cmpLog"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.nmLog}">
				<li class="fieldcontain">
					<span id="nmLog-label" class="property-label"><g:message code="filial.nmLog.label" default="Nm Log" /></span>
					
						<span class="property-value" aria-labelledby="nmLog-label"><g:fieldValue bean="${filialInstance}" field="nmLog"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.baiLog}">
				<li class="fieldcontain">
					<span id="baiLog-label" class="property-label"><g:message code="filial.baiLog.label" default="Bai Log" /></span>
					
						<span class="property-value" aria-labelledby="baiLog-label"><g:fieldValue bean="${filialInstance}" field="baiLog"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.cepLog}">
				<li class="fieldcontain">
					<span id="cepLog-label" class="property-label"><g:message code="filial.cepLog.label" default="Cep Log" /></span>
					
						<span class="property-value" aria-labelledby="cepLog-label"><g:fieldValue bean="${filialInstance}" field="cepLog"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.atvFil}">
				<li class="fieldcontain">
					<span id="atvFil-label" class="property-label"><g:message code="filial.atvFil.label" default="Atv Fil" /></span>
					
						<span class="property-value" aria-labelledby="atvFil-label"><g:fieldValue bean="${filialInstance}" field="atvFil"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.unicodFilial}">
				<li class="fieldcontain">
					<span id="unicodFilial-label" class="property-label"><g:message code="filial.unicodFilial.label" default="Unicod Filial" /></span>
					
						<span class="property-value" aria-labelledby="unicodFilial-label"><g:fieldValue bean="${filialInstance}" field="unicodFilial"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.idHabilitada}">
				<li class="fieldcontain">
					<span id="idHabilitada-label" class="property-label"><g:message code="filial.idHabilitada.label" default="Id Habilitada" /></span>
					
						<span class="property-value" aria-labelledby="idHabilitada-label"><g:fieldValue bean="${filialInstance}" field="idHabilitada"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.idPgHrextAdm}">
				<li class="fieldcontain">
					<span id="idPgHrextAdm-label" class="property-label"><g:message code="filial.idPgHrextAdm.label" default="Id Pg Hrext Adm" /></span>
					
						<span class="property-value" aria-labelledby="idPgHrextAdm-label"><g:fieldValue bean="${filialInstance}" field="idPgHrextAdm"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.municipio}">
				<li class="fieldcontain">
					<span id="municipio-label" class="property-label"><g:message code="filial.municipio.label" default="Municipio" /></span>
					
						<span class="property-value" aria-labelledby="municipio-label"><g:link controller="municipio" action="show" id="${filialInstance?.municipio?.id}">${filialInstance?.municipio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${filialInstance?.empresa}">
				<li class="fieldcontain">
					<span id="empresa-label" class="property-label"><g:message code="filial.empresa.label" default="Empresa" /></span>
					
						<span class="property-value" aria-labelledby="empresa-label"><g:link controller="empresa" action="show" id="${filialInstance?.empresa?.id}">${filialInstance?.empresa?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${filialInstance?.id}" />
					<g:link class="edit" action="edit" id="${filialInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
