
<html>
  <head>
    <meta name="layout" content="main">
    <g:javascript src="comum/lov.js"/>
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Filiais</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Filial']" /></g:link>
    </div>
       
    <form action="${request.contextPath}/filial/list" method="POST" id="formPesquisa">
    	<div class="well">
  			<h3>Filtro</h3>
  			<div class="control-group">
				<label class="control-label">Tipo de filtro:</label>				
				<div class="controls">
					<g:select name="tipoFiltro" optionKey="id" optionValue="nome" 
							value="${params.tipoFiltro}"
							style="width:100px;"
							from="${[ [id:'0', nome:'...'],
									  [id:'1', nome:'Código'], 
									  [id:'2', nome:'Descrição'] ]}"/>
					<g:textField name="filtro" maxlength="250" value="${params.filtro}" style="margin-left: 10px; width: 400px;"/>
					<input type="hidden" id="order" name="order"/>
					<input type="hidden" id="sortType" name="sortType"/>
					<input type="hidden" id="max" name="max" value="${params.max}"/>
					<input type="hidden" id="offset" name="offset" value="${params.offset}"/>
					<g:submitButton name="filtrar" class="btn btn-medium btn-primary" value="Filtrar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>
          	Descrição
          	<g:if test="${order != 'dscFil'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('dscFil', 'asc')"/></g:if>
          	<g:if test="${order == 'dscFil' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('dscFil', 'desc')"/></g:if>
          	<g:if test="${order == 'dscFil' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('dscFil', 'asc')"/></g:if>
          </th>
          <th>
          	Cnpj
          	<g:if test="${order != 'cnpjFil'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('cnpjFil', 'asc')"/></g:if>
          	<g:if test="${order == 'cnpjFil' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('cnpjFil', 'desc')"/></g:if>
          	<g:if test="${order == 'cnpjFil' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('cnpjFil', 'asc')"/></g:if>
          </th>		
          <th>
          	Sigla
          	<g:if test="${order != 'unicodFilial'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('unicodFilial', 'asc')"/></g:if>
          	<g:if test="${order == 'unicodFilial' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('unicodFilial', 'desc')"/></g:if>
          	<g:if test="${order == 'unicodFilial' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('unicodFilial', 'asc')"/></g:if>
          </th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${filialInstanceList}" status="i" var="filialInstance">
        <tr>
          <td>${fieldValue(bean: filialInstance, field: "dscFil")}</td>
          <td>${fieldValue(bean: filialInstance, field: "cnpjFil")}</td>
          <td>${fieldValue(bean: filialInstance, field: "unicodFilial")}</td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${filialInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/filial/delete', ${filialInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${filialInstanceTotal}" params="${params}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${filialInstanceTotal}</span>
    </div>
  </body>
</html>