<g:javascript src="filial/filial.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${filialInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${filialInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>
	<g:if test="${filialInstance.id}">
		<form action="${request.contextPath}/filial/update" class="well" method="POST">
			<g:hiddenField name="id" value="${filialInstance?.id}" />
			<g:hiddenField name="version" value="${filialInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/filial/save" class="well" method="POST">
	</g:else>	
			<div class="row" style=" height: 60px;">	
		       <div class="span3  ">
		       		<label>Cnpj Fil:</label>
					<g:textField class="span3" name="cnpjFil" maxlength="60" required="" value="${filialInstance?.cnpjFil}" placeholder="Cnpj Fil"/>
			   </div>		
				<div class="span2">
					<label>Sigla:</label>
					<g:textField class="span2" name="unicodFilial" maxlength="12" value="${filialInstance?.unicodFilial}" placeholder="Sigla"/>
				</div>
				<div class="span3  ">
					<label>Descrição:</label>
					<g:textField name="dscFil" class="span3" maxlength="80" required="" value="${filialInstance?.dscFil}" placeholder="Descrição"/>
				</div>
				<div class="span3  ">
					<label>Nome Fantasia:</label>
					<g:textField name="dscFantFil" class="span3" maxlength="80" required="" value="${filialInstance?.dscFantFil}" placeholder="Nome Fantasia"/>
				</div>
            </div>
            <div class="row" style=" height: 60px;">		
	            <div class="span3  ">
            		<label>Bairro:</label>
					<g:textField name="baiLog" class="span3" maxlength="60" value="${filialInstance?.baiLog}" placeholder="Bairro"/>
				</div>
	
	
				<div class="span3  ">
					<label>Endereço:</label>				
					<g:textField name="dscLog" class="span3" maxlength="60" required="" value="${filialInstance?.dscLog}" placeholder="Endereço"/>
				</div>
			    <div class="span3  ">
			    	<label>Complemento:</label>
					<g:textField name="cmpLog" class="span3" maxlength="60" value="${filialInstance?.cmpLog}" placeholder="Complemento"/>
				</div>		
					
				<div class="span2">
					<label>Número:</label>
					<g:textField name="nmLog" class="span2" maxlength="6" required="" value="${filialInstance?.nmLog}" placeholder="Número"/>
				</div>             
			</div>
			<div class="row" style=" height: 60px;">			
				<div class="span3  ">
					<label>Cep Log:</label>
					<g:textField name="cepLog" class="span3" maxlength="15" value="${filialInstance?.cepLog}" placeholder="Cep Log"/>
				</div>
			    <div class="span2  ">
			    	<label style="opacity:0">:</label>
					Filial ativa&emsp;<g:checkBox name="idHabilitada" value="${filialInstance?.idHabilitada}" checked="${filialInstance?.idHabilitada == 'S'}" /></td>
				</div>		
					
				<div class="span3">
					<label style="opacity:0">:</label>
					Pagamento de horas extras?&emsp;<g:checkBox name="idPgHrextAdm" value="${filialInstance?.idPgHrextAdm}" checked="${filialInstance?.idPgHrextAdm == 'S'}" /></td>             
				</div>
						
				
				
	
				<div class="span3  ">
						<label>Atividade da Filial:</label>
						<g:textField class="span3" name="atvFil" maxlength="40" required="" value="${filialInstance?.atvFil}" placeholder="Atividade da Filial"/>
				</div>
		   </div>
		   <div class="row" style="height: 60px;">
	           <div class="span4">
	           		<label>Municipio:</label>
					<g:select id="municipio"name="municipio.id" from="${cadastros.Municipio.list()}" optionKey="id" value="${filialInstance?.municipio?.id}" class="span4 many-to-one" noSelection="['null': '']"/>
			   </div>
				<div class="span4">
					<label>Empresa:</label>
					<g:select id="empresa" name="empresa.id" from="${cadastros.Empresa.list()}" optionKey="id" required="" value="${filialInstance?.empresa?.id}" class="span4 many-to-one"/>
				</div>
			</div>
      	<hr>
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		<g:submitButton name="salvar" class="btn btn-large  btn-info" value="${message(code: 'default.button.save.label')}" />
	</form>