<div id="divLovPadrao" style="display: none;" title="Filtro padrão">
<g:form>

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	);
	
	$j('.inputLovPadrao').keypress(
		function(e){
			if (e.keyCode == 13) {
				e.preventDefault();
				aplicaFiltroPadrao();
			}
		}
	);
	</script>
	
	<table>
		<tr>
			<td>Filtro</td>
			<td>
				<g:textField name="lovFiltroId" style="width:40px" onBlur="buscaIdFiltroUsuario('#lovFiltroId');" class="inputLovPadrao"/>&nbsp; 
				<i class="icon-search" style="cursor: pointer" id="filtroBusca" 
					onclick="showBuscaInterna('lovFiltroId', 'nomeFiltro', 'cadastros.FiltroUsuario', 'id', 'nomeFiltro');"></i>
				<input type="hidden" name="lovFiltroIdBanco" id="lovFiltroIdBanco">
				<g:textField name="nomeFiltro" style="width:175px"/>&nbsp;
			</td>
		</tr>
		<tr>
			<td>Filial</td>
			<td>
				<g:textField name="filtroFilialId" style="width:250px" class="inputLovPadrao"/>&nbsp; 
				<i class="icon-search" style="cursor: pointer" id="filtroBusca"
					onclick="showBuscaInterna('filtroFilialId', '', 'cadastros.Filial', 'id', 'dscFil');"></i>
			</td>
		</tr>
		<tr>
			<td>Cargo</td>
			<td>
				<g:textField name="filtroCargoId" style="width:250px" class="inputLovPadrao"/>&nbsp; 
				<i class="icon-search" style="cursor: pointer" id="filtroBusca"
					onclick="showBuscaInterna('filtroCargoId', '', 'cadastros.Cargo', 'id', 'dscCargo');"></i>
			</td>
		</tr>
		<tr>
			<td>Departamento</td>
			<td>
				<g:textField name="filtroDepartamentoId" style="width:250px" class="inputLovPadrao"/>&nbsp; 
				<i class="icon-search" style="cursor: pointer" id="filtroBusca"
					onclick="showBuscaInterna('filtroDepartamentoId', '', 'cadastros.Setor', 'id', 'dscSetor');"></i>
			</td>
		</tr>
		<tr>
			<td>Funcionário</td>
			<td>
				<g:textField name="filtroFuncionarioId" style="width:250px" class="inputLovPadrao"/>&nbsp; 
				<i class="icon-search" style="cursor: pointer" id="filtroBusca"
					onclick="showBuscaInterna('filtroFuncionarioId', '', 'cadastros.Funcionario', 'id', 'nomFunc');"></i>
			</td>
		</tr>
		<tr>
			<td>Tipo funcionário</td>
			<td>
				<g:textField name="filtroTipoFuncionarioId" style="width:250px" class="inputLovPadrao"/>&nbsp; 
				<i class="icon-search" style="cursor: pointer" id="filtroBusca"
					onclick="showBuscaInterna('filtroTipoFuncionarioId', '', 'cadastros.Tipofunc', 'id', 'dsctpfunc');"></i>
			</td>
		</tr>
		<tr>
			<td>Horário</td>
			<td>
				<g:textField name="filtroHorarioId" style="width:250px" class="inputLovPadrao"/>&nbsp; 
				<i class="icon-search" style="cursor: pointer" id="filtroBusca"
					onclick="showBuscaInterna('filtroHorarioId', '', 'cadastros.Horario', 'id', 'dscHorario');"></i>
			</td>
		</tr>
		<tr>
			<td>Sub-Uniorg</td>
			<td>
				<g:textField name="filtroSubUniOrgId" style="width:250px" class="inputLovPadrao"/>&nbsp; 
				<i class="icon-search" style="cursor: pointer" id="filtroBusca"
					onclick="showBuscaInterna('filtroSubUniOrgId', '', 'cadastros.Uniorg', 'id', 'dscUniorg');"></i>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="checkbox" id="filtroNaoListaDemitidos" name="filtroNaoListaDemitidos" checked="checked"> Não listar funcionários demitidos
			</td>
		</tr>
	</table>
	
	<br>
	
	<input type="button" class="btn btn-medium btn-primary" value="Salvar filtro" onclick="salvaFiltroPadrao()" />
	<input type="button" class="btn btn-medium btn-primary" value="OK" onclick="aplicaFiltroPadrao()" />
	
</g:form>
</div>