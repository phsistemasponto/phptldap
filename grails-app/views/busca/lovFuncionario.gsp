<g:form>

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	<div class="row-fluid">
		<div class="control-group">
			<label class="control-label">Buscar por:</label>
			<div class="controls">
				<g:textField name="dscBusca" maxlength="60" />
				<input type="button" value="Buscar"
					onClick="buscarLovResultadoFuncionario();" style="width: 120px;" />
				<input type="button" disabled="true" value="Finalizar" id="botaoFinalizar" name="botaoFinalizar"
					onClick="finalizarSelecaoFuncionario();" style="width: 120px;" />

			</div>
		</div>
	</div>
    <br>
    <div id="resultadoBusca" >
    </div>
</g:form>