<div id="divLovBuscaInterna" style="display: none;" title="Filtro">
<g:form id="formBusca" name="formBusca">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>

    <g:hiddenField name="campoIdRetorno" value="${campoIdRetorno}"/>
    <g:hiddenField name="campoDescricaoRetorno" value="${campoDescricaoRetorno}"/>   
    <g:hiddenField name="classeBusca" value="${classeBusca}"/>
    <g:hiddenField name="campoId" value="${campoId}"/>
    <g:hiddenField name="campoDescricao" value="${campoDescricao}"/>
    <g:hiddenField name="multi" value="${multi}"/>
    <g:hiddenField name="callback" value="${callback}"/>
    <g:hiddenField name="filtraFiliaisUsuario" value="${filtraFiliaisUsuario}"/>
    
    <!-- Campos especificos -->
    <g:hiddenField name="tipoJustificativaEspelhoPonto" value="${tipoJustificativaEspelhoPonto}"/>
    
    <g:hiddenField name="filtraFilialEspecifica" value="${filtraFilialEspecifica}"/>
    <g:hiddenField name="filialEspecifica" value="${filialEspecifica}"/>
     
    <label>Buscar por:</label>
    
    <g:textField name="dscBusca" maxlength="60"/>
    <input type="button" value="Buscar" onClick="buscarLovInternoResultado();" style="width: 120px;">
    
    <br>
    
    <input type="button" id="btnLovInternoMarcaTodos" value="Marcar todos" onClick="marcaTodosLovInterno();" style="display: none;">
	<input type="button" id="btnLovInternoDesmarcaTodos" value="Desmarcar todos" onClick="desmarcaTodosLovInterno();" style="display: none;">
	<input type="button" id="btnLovInternoOk" value="OK" onClick="popularRetornoLovInterno();" style="display: none;" class="btn-primary">
    
    <div id="resultadoBusca" >
    </div>
    
</g:form>
</div>