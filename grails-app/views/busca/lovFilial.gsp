<g:form>

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>

	
	<input type="hidden" name="multiLov" id="multiLov" value="${params.multi}"/>
	
    <label>Buscar por:</label>
    <g:textField name="dscBusca" maxlength="60"/>
    <input type="button" value="Buscar" onClick="buscarLovResultadoFilial();" style="width: 120px;">
    
    <br>
    
    <g:if test="${params.multi}">
	    <input type="button" id="btnLovMarcaTodos" value="Marcar todos" onClick="marcaTodosLov();" style="display: none;">
		<input type="button" id="btnLovDesmarcaTodos" value="Desmarcar todos" onClick="desmarcaTodosLov();" style="display: none;">
		<input type="button" id="btnLovOk" value="OK" onClick="popularRetornoLovMulti();" style="display: none;" class="btn-primary">
	</g:if>
	
    <div id="resultadoBusca" >
    </div>
    
</g:form>