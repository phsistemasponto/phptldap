
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="filial/filial.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Importar dados do funcionário</h3>
    </div>
    
    <form action="${request.contextPath}/importarFuncionarios/lerArquivo" class="form-horizontal" method="POST" enctype="multipart/form-data">
    
    	<div class="well">
    	
    		<div class="control-group">
				<label class="control-label">Filial:</label>				
				<div class="controls">
					<input type="text" id="filialId" name="filialId" maxlength="5" value="${params.filialId}" style="width:50px" onBlur="buscaIdFilial();"/>&nbsp;
					<i class="icon-search" onclick="buscarLovFilial();" style="cursor:pointer" title="Procurar Filial"></i>&nbsp;
					<input type="text" id="filialNome" name="filialNome" value="${params.filialNome}" readonly="true" style="width:50%"/>
				</div>
			</div>
			
			<input type="file" id="files" name="files" />
			
			<br><br>
				    
			<input type="submit" class="btn btn-medium btn-primary" value="Processar arquivo">
    	
		</div>
    
    </form>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
    <g:if test="${session.registrosArquivo != null}">
    
    
    <div style="height: 300px; overflow-y: scroll; border: 1px solid rgba(0, 0, 0, 0.05); padding: 10px;">
    
	    <table class="table table-striped table-bordered table-condensed">
	    	<thead>
	    		<tr>
	    			<th>Nome</th>
	    			<th>Matrícula</th>
	    			<th>PIS</th>
	    			<th>Cargo</th>
	    			<th>Setor</th>
	    			<th>Data nascimento</th>
	    			<th>Data admissão</th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    		<g:each in="${session.registrosArquivo}" status="i" var="registro">
	    			<tr>
	    				<td>${registro.nome}</td>
	    				<td>${registro.matricula}</td>
	    				<td>${registro.pis}</td>
	    				<td>${registro.nomeCargo}</td>
	    				<td>${registro.nomeSetor}</td>
	    				<td>${registro.nascimento}</td>
	    				<td>${registro.admissao}</td>
	    			</tr>
	    		</g:each>
	    	</tbody>
	    </table>
	    
	</div>
	
	<br><br>
	
	<input type="button" class="btn btn-medium btn-primary" value="Salvar" onclick="salvarRegistros()"/>
	
	</g:if>
	
	<div id="divLogImportacao" style="display: none;" title="Resultado importação">
		<input type="text" disabled="disabled" readonly="readonly" id="quantSucess" name="quantSucess" style="width: 50px;"/> funcionários importados com sucesso
		<br/><br/>
		<h1>Erros:</h1>
    	<table id="tableResultLog" class="table table-striped table-bordered table-condensed">
    		<thead>
    			<tr>
    				<th>Funcionário</th>
    				<th>Mensagem</th>
    			</tr>
    		</thead>
    		<tbody>
    		</tbody>
    	</table>
    </div>

    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
    <script>

    	var $j = jQuery.noConflict();
    
    	function salvarRegistros() {
    		$j('#carregando').show();
    		$j.ajax({
     		   async: false,
     		   type: "POST",
     		   url:context+"/importarFuncionarios/salvarRegistros",	
     		   data: {filialId: $j('#filialId').val()},
     		   dataType:"json",
     		   success:function(json){

     			  $j('#carregando').hide();
          		  $j('#quantSucess').val(json.qtRegistros);
         		  $j('#tableResultLog > tbody').empty();

         		  $j.each(json.erros, function(index, item) {
 					var append =  '<tr>';
 						append += '	<td>';
 						append += item.nome;
 						append += '	</td>';
 						append += '	<td>';
 						append += item.erro;
 						append += '	</td>';
 						append += '</tr>';
 					$j("#tableResultLog > tbody").append(append);
 				  });
 				  
 				  $j("#divLogImportacao").dialog({width: 800, height: 500, modal: true});
         		  
     		   },
     		   error:function(xhr){
     			  alert('Erro na operação');
     			  $j('#carregando').hide();	
     		   }
     		});	
        }
    	
    </script>
    
  </body>
</html>