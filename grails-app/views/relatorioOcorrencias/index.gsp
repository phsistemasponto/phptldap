
<%@page import="cadastros.Ocorrencias"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    <g:javascript src="reports/ocorrencias.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Relatório de ocorrências</h3>
    </div>
    
    <g:jasperForm controller="relatorioOcorrencias" action="generateReport" jasper="ocorrencias" class="form-horizontal" id="formPesquisa" target="_blank">
    
    	<div class="well">
    	
    		<div class="well" style="border: 1px solid #859CA4; background-color: #859CA4;">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					</div>
				</div>
			</div>
			
			<div class="tabbable"> 
			    <ul class="nav nav-tabs">
				    <li class="active"><a href="#tab1" data-toggle="tab">Opções básicas</a></li>
				    <li><a href="#tab2" data-toggle="tab">Opções de ocorrências</a></li>
			    </ul>
			    <div class="tab-content">
			    
			    	<!--
			    	  --
			    	  -- TAB OPCOES BASICAS
			    	  -- 
			    	 -->
				    <div class="tab-pane active" id="tab1">
				    
				    	<g:if test="${filtros != null && filtros.size() > 0}">
					    	<div class="control-group">
								<label class="control-label" style="font-weight: bold;">Filtro:</label>
								<div class="controls">
									<g:select id="filtroSalvo" name="filtroSalvo" noSelection="['':'..']"
										from="${filtros}" optionKey="id" required="" onchange="aplicaFiltro();"/>
								</div>
							</div>
						</g:if>
				    
				    	<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Data início:</label>
							<div class="controls">
								<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" />
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Data fim:</label>
							<div class="controls">
								<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" />
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Relatório:</label>
							<div class="controls">
								<g:select name="relatorio" id="relatorio" optionKey="id" optionValue="nome" 
										value="${params.relatorio}"
										style="width:200px;"
										from="${[ [id:'0', nome:'Proventos'],
												  [id:'1', nome:'Descontos'],
												  [id:'2', nome:'Proventos/Descontos'],
												  [id:'3', nome:'Ocorrências']  ]}"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Tipo de relatório:</label>
							<div class="controls">
								<g:select name="tipoRelatorio" id="tipoRelatorio" optionKey="id" optionValue="nome" 
										value="${params.tipoRelatorio}"
										style="width:100px;"
										from="${[ [id:'0', nome:'Analítico'],
												  [id:'1', nome:'Sintético']  ]}"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Tipo de agrupamento:</label>
							<div class="controls">
								<g:select name="agrupamento" id="agrupamento" optionKey="id" optionValue="nome" 
										value="${params.agrupamento}"
										style="width:150px;"
										from="${[ [id:'0', nome:'Filial'],
												  [id:'1', nome:'Departamento'],
												  [id:'2', nome:'Função'],
												  [id:'3', nome:'Funcionário']  ]}"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Ordenação:</label>
							<div class="controls">
								<g:select name="ordenacao" id="ordenacao" optionKey="id" optionValue="nome" 
										value="${params.ordenacao}"
										style="width:200px;"
										from="${[ [id:'1', nome:'Código'],
												  [id:'2', nome:'Descrição'] ]}"/>
							</div>
						</div>
						
				    </div>
				    
				    <!--
			    	  --
			    	  -- TAB OPCOES HORAS EXTRAS
			    	  -- 
			    	 -->
				    <div class="tab-pane" id="tab2">
				    	
				    	<table style="width: 100%;">
				    		<tr>
				    			<td width="50%">
				    				<span style="font-weight: bold; margin-left: 3px;">PROVENTOS</span><BR>
				    				<table class="table table-striped table-bordered table-condensed" id="horarioTable" style="margin-top: 5px;">
							    		<thead>
							    			<tr>
							    				<th>Seq</th>
							    				<th>Ocorrência</th>
							    				<th>&nbsp;</th>
							    			</tr>
							    		</thead>
							    		<tbody id="tbodyOcorProvento">
								    		<g:each in="${Ocorrencias.findAll('from Ocorrencias o where o.idTpOcor = \'P\'')}" var="ocorrencia">
								    			<tr id="rowTableProvento${ocorrencia.id}">
								    				<td>
								    					${ocorrencia.id}
								    					<input type="hidden" name="ocorrenciasId" id="ocorrenciasId" value="${ocorrencia.id}">
								    				</td>
								    				<td>${ocorrencia.dscOcor}</td>
								    				<td>
								    					<i class="icon-remove" onclick="excluirProvento(${ocorrencia.id});" style="cursor: pointer" title="Excluir registro"></i>
								    				</td>
								    			</tr>
								    		</g:each>
							    		</tbody>
							    	</table>
				    			</td>
				    			<td width="50%">
				    				<span style="font-weight: bold; margin-left: 3px; margin-bottom: 3px;">DESCONTOS</span><BR>
				    				<table class="table table-striped table-bordered table-condensed" id="horarioTable" style="margin-top: 5px;">
							    		<thead>
							    			<tr>
							    				<th>Seq</th>
							    				<th>Ocorrência</th>
							    				<th>&nbsp;</th>
							    			</tr>
							    		</thead>
							    		<tbody id="tbodyOcorDesconto">
								    		<g:each in="${Ocorrencias.findAll('from Ocorrencias o where o.idTpOcor = \'D\'')}" var="ocorrencia">
								    			<tr id="rowTableDebito${ocorrencia.id}">
								    				<td>
								    					${ocorrencia.id}
								    					<input type="hidden" name="ocorrenciasId" id="ocorrenciasId" value="${ocorrencia.id}">
								    				</td>
								    				<td>${ocorrencia.dscOcor}</td>
								    				<td>
								    					<i class="icon-remove" onclick="excluirDebito(${ocorrencia.id});" style="cursor: pointer" title="Excluir registro"></i>
								    				</td>
								    			</tr>
								    		</g:each>
							    		</tbody>
							    	</table>
				    			</td>
				    		</tr>
				    	</table>
				    	
				    	<input type="hidden" name="ocorrenciasId" id="ocorrenciasId" value="0">
				    </div>
				    
				</div>
				
			</div>
			
			<input type="hidden" id="urlSalvarFiltro" name="urlSalvarFiltro" value="/relatorioOcorrencias/salvarFiltro">
				    
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('PDF'); else return false;">Gerar em PDF</button>
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('XLS'); else return false;">Gerar em Excel</button>
			<button class="btn btn-medium btn-primary" onclick="showSalvarFiltro();" type="button">Salvar filtro</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    <g:render template="/relatorio/modalSalvarFiltro"/>

    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>