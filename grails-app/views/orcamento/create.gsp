<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
    <h2><g:message code="default.create.label" args="['Orcamento']" /></h2>
    <g:render template="form"/>
  </body>
</html>