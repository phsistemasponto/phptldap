

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${orcamentoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${orcamentoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${orcamentoInstance.id}">
		<form action="${request.contextPath}/orcamento/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${orcamentoInstance?.id}" />
			<g:hiddenField name="version" value="${orcamentoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/orcamento/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Valor:</label>
				<div class="controls">
					<g:textField name="valor" required="" value="${fieldValue(bean: orcamentoInstance, field: 'valor')}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Liberado:</label>
				<div class="controls">
					<g:checkBox name="liberado" value="${orcamentoInstance?.liberado}" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Filial:</label>
				<div class="controls">
					<g:set var="fs" bean="filialService"/>
					<g:select id="filial" name="filial.id" from="${fs.filiaisDoUsuario()}" optionKey="id" required="" value="${orcamentoInstance?.filial?.id}" class="many-to-one"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Periodo:</label>
				<div class="controls">
					<g:select id="periodo" name="periodo.id" from="${cadastros.Periodo.list()}" optionKey="id" required="" value="${orcamentoInstance?.periodo?.id}" class="many-to-one"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>