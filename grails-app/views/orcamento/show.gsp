
<%@ page import="cadastros.Orcamento" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'orcamento.label', default: 'Orcamento')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-orcamento" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-orcamento" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list orcamento">
			
				<g:if test="${orcamentoInstance?.valor}">
				<li class="fieldcontain">
					<span id="valor-label" class="property-label"><g:message code="orcamento.valor.label" default="Valor" /></span>
					
						<span class="property-value" aria-labelledby="valor-label"><g:fieldValue bean="${orcamentoInstance}" field="valor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${orcamentoInstance?.liberado}">
				<li class="fieldcontain">
					<span id="liberado-label" class="property-label"><g:message code="orcamento.liberado.label" default="Liberado" /></span>
					
						<span class="property-value" aria-labelledby="liberado-label"><g:formatBoolean boolean="${orcamentoInstance?.liberado}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${orcamentoInstance?.filial}">
				<li class="fieldcontain">
					<span id="filial-label" class="property-label"><g:message code="orcamento.filial.label" default="Filial" /></span>
					
						<span class="property-value" aria-labelledby="filial-label"><g:link controller="filial" action="show" id="${orcamentoInstance?.filial?.id}">${orcamentoInstance?.filial?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${orcamentoInstance?.periodo}">
				<li class="fieldcontain">
					<span id="periodo-label" class="property-label"><g:message code="orcamento.periodo.label" default="Periodo" /></span>
					
						<span class="property-value" aria-labelledby="periodo-label"><g:link controller="periodo" action="show" id="${orcamentoInstance?.periodo?.id}">${orcamentoInstance?.periodo?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${orcamentoInstance?.id}" />
					<g:link class="edit" action="edit" id="${orcamentoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
