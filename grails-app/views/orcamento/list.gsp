
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="jquery.maskMoney.js" />
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="orcamento/orcamento.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Orçamentos</h3>
    </div>
    
    <form action="${request.contextPath}/orcamento/list" method="POST" id="formPesquisa">
    	<div class="well">
    		<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Período:</label>
				<div class="controls">
					<g:select id="periodo" name="periodo" from="${periodos}" optionKey="id" value="${params.periodo}" 
						noSelection="['':'..']" style="width:145px;" onchange="mudaPeriodo()"/>
					<input type="text" disabled="disabled" id="dtInicio" style="width:70px;">
					<input type="text" disabled="disabled" id="dtFim" style="width:70px;">
					<g:each in="${periodos}" var="periodo">
						<input type="hidden" id="dtInicialPeriodo${periodo.id}" value="${periodo.dtIniPrFormatada}"/>
						<input type="hidden" id="dtFinalPeriodo${periodo.id}" value="${periodo.dtFimPrFormatada}"/>
					</g:each>
					<g:submitButton name="btnFiltro" class="btn btn-medium btn-primary" value="Listar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>
	
	<form action="${request.contextPath}/orcamento/salvaOrcamento" method="POST" id="formOrcamento">
		
		<g:if test="${orcamentoInstanceTotal > 0}">
	    	<g:submitButton name="btnConcluir" class="btn btn-medium btn-primary" value="Salvar" />
	    </g:if>
		
		<br><br>

	    <table class="table table-striped table-bordered table-condensed">
	      <thead>
	        <tr>
	          <th style="width: 50%">Filial</th>
	          <th style="width: 20%">Liberado</th>
	          <th style="width: 30%">Valor</th>
	        </tr>
	      </thead>
	
	      <tbody>
	      <g:each in="${orcamentoInstanceList}" status="i" var="orcamentoInstance">
	        <tr>
	          <td>${orcamentoInstance.filial}</td>
	          <td><g:formatBoolean boolean="${orcamentoInstance.liberado}" /></td>
	          <td>
	          	<input type="text" 
	          		id="valor_${orcamentoInstance.filial.id}_${periodoSelecionado.id}" 
	          		name="valor_${orcamentoInstance.filial.id}_${periodoSelecionado.id}" 
	          		value=${orcamentoInstance.valor} class="valor" style="width: 100px; margin-bottom: 0px !important;"/>
	          </td>
	        </tr>
	      </g:each>
	      </tbody>
	    </table>
	    <div class="paginate">
	      <g:paginate total="${orcamentoInstanceTotal}" params="${params}" />
	      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${orcamentoInstanceTotal}</span>
	    </div>
	</form>
  </body>
</html>