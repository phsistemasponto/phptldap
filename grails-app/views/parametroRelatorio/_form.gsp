
	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="parametroRelatorio/parametroRelatorio.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${parametroRelatorioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${parametroRelatorioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${parametroRelatorioInstance.id}">
		<form action="${request.contextPath}/parametroRelatorio/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${parametroRelatorioInstance?.id}" />
			<g:hiddenField name="version" value="${parametroRelatorioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/parametroRelatorio/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descricao:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="80" required="" value="${parametroRelatorioInstance?.descricao}"/>
				</div>
			</div>

			<fieldset>
				<legend>Detalhes</legend>
				
				<div class="control-group">
					<label class="control-label">Ocorrência:</label>
					<div class="controls">
						<g:textField name="ocorrenciaId" maxlength="5" style="width:50px" onBlur="buscaOcorrenciaId();"/>&nbsp;
					  	<i class="icon-search" style="cursor:pointer" title="Procurar Ocorrência" onclick="lovOcorrencia();" ></i>&nbsp;
						<g:textField name="ocorrenciaDescricao" readonly="true" style="width:450px"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Totais:</label>
					<div class="controls">
						<g:select name="totalizador" from="${cadastros.ParametroRelatorioTotais.list()}" optionKey="id" optionValue="descricao"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Descrição:</label>
					<div class="controls">
						<g:textField name="descricaoDetalhe" maxlength="80" style="width:350px" />
					</div>
				</div>
				
				<button type="button" class="btn btn-medium btn-primary" onclick="incluirDetalhe();">Incluir</button>
				
				<br/><br/>
				
				<table class="table table-striped table-bordered table-condensed" id="detalhesTable">
					<thead>
						<tr>
							<th>Totalizador</th>
							<th>Ocorrência</th>
							<th>Descrição</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<g:each in="${detalhes}" var="detalhe" status="i">
							<tr id="rowDetalhe-${i}">
								<td>
									<input name="idTotalizador" type="hidden" value="${detalhe.parametroRelatorioTotais.id}"/>
									<input name="dscTotalizador" type="text" readonly="" value="${detalhe.parametroRelatorioTotais.descricao}" style="width: 80%"/>
								</td>
								<td>
									<input name="idOcorParametroDetalhe" type="hidden" value="${detalhe.ocorrencias.id}"/>
									<input name="dscOcorParametroDetalhe" type="text" readonly="" value="${detalhe.ocorrencias.dscOcor}" style="width: 80%"/>
								</td>
								<td>
									<input name="descricaoParametroDetalhe" type="text" readonly="" value="${detalhe.descricao}" style="width: 80%"/>
								</td>
								<td>
									<i class="icon-remove" onclick="excluirDetalhe(${i});" style="cursor: pointer" title="Excluir registro"></i>
								</td>
							</tr>
						</g:each>
					</tbody>
				</table>
				
			</fieldset>
			
			<fieldset>
				<legend>Filiais</legend>
				
				<button type="button" class="btn btn-medium btn-primary" onclick="marcarTodos();">Marcar todos</button>
				<button type="button" class="btn btn-medium btn-primary" onclick="desmarcarTodos();">Desmarcar todos</button>
				
				<br/>
				
				<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Filial</th>
						</tr>
					</thead>
					<tbody>
						<g:set var="fs" bean="filialService"/>
						<g:each in="${fs.filiaisDoUsuario()}" var="f" status="i">
							<tr>
								<td>
									<g:if test="${parametroRelatorioInstance.id != null && cadastros.ParametroRelatorioFilial.findByParametroRelatorioAndFilial(parametroRelatorioInstance, f) != null}">
										<input type="checkbox" name="filial" id="filial" value="${f.id}" checked="checked"/>
									</g:if>
									<g:else>
										<input type="checkbox" name="filial" id="filial" value="${f.id}"/>
									</g:else>
								</td>
								<td>${f}</td>
							</tr>
						</g:each>
					</tbody>
				</table>
				
			</fieldset>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		
	</form>
	
	<g:render template="/busca/lovBuscaInterna"/>