
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Parâmetros de Relatórios</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Parâmetro de Relatório']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>Descrição</th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${parametroRelatorioInstanceList}" status="i" var="parametroRelatorioInstance">
        <tr>
		
          <td>${fieldValue(bean: parametroRelatorioInstance, field: "descricao")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${parametroRelatorioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/parametroRelatorio/delete', ${parametroRelatorioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${parametroRelatorioInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${parametroRelatorioInstanceTotal}</span>
    </div>
  </body>
</html>