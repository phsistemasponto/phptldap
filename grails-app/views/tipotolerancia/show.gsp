
<%@ page import="cadastros.Tipotolerancia" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipotolerancia.label', default: 'Tipotolerancia')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipotolerancia" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipotolerancia" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipotolerancia">
			
				<g:if test="${tipotoleranciaInstance?.dsctolerancia}">
				<li class="fieldcontain">
					<span id="dsctolerancia-label" class="property-label"><g:message code="tipotolerancia.dsctolerancia.label" default="Dsctolerancia" /></span>
					
						<span class="property-value" aria-labelledby="dsctolerancia-label"><g:fieldValue bean="${tipotoleranciaInstance}" field="dsctolerancia"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipotoleranciaInstance?.nrTolAntEnt}">
				<li class="fieldcontain">
					<span id="nrTolAntEnt-label" class="property-label"><g:message code="tipotolerancia.nrTolAntEnt.label" default="Nr Tol Ant Ent" /></span>
					
						<span class="property-value" aria-labelledby="nrTolAntEnt-label"><g:fieldValue bean="${tipotoleranciaInstance}" field="nrTolAntEnt"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipotoleranciaInstance?.nrTolAntSai}">
				<li class="fieldcontain">
					<span id="nrTolAntSai-label" class="property-label"><g:message code="tipotolerancia.nrTolAntSai.label" default="Nr Tol Ant Sai" /></span>
					
						<span class="property-value" aria-labelledby="nrTolAntSai-label"><g:fieldValue bean="${tipotoleranciaInstance}" field="nrTolAntSai"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipotoleranciaInstance?.nrTolAtrEnt}">
				<li class="fieldcontain">
					<span id="nrTolAtrEnt-label" class="property-label"><g:message code="tipotolerancia.nrTolAtrEnt.label" default="Nr Tol Atr Ent" /></span>
					
						<span class="property-value" aria-labelledby="nrTolAtrEnt-label"><g:fieldValue bean="${tipotoleranciaInstance}" field="nrTolAtrEnt"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipotoleranciaInstance?.nrTolAtrSai}">
				<li class="fieldcontain">
					<span id="nrTolAtrSai-label" class="property-label"><g:message code="tipotolerancia.nrTolAtrSai.label" default="Nr Tol Atr Sai" /></span>
					
						<span class="property-value" aria-labelledby="nrTolAtrSai-label"><g:fieldValue bean="${tipotoleranciaInstance}" field="nrTolAtrSai"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${tipotoleranciaInstance?.id}" />
					<g:link class="edit" action="edit" id="${tipotoleranciaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
