<g:javascript src="jquery.meiomask.js"/>
<g:javascript src="jquery.maskMoney.js" />
<g:javascript src="comum/lov.js"/>
<g:javascript src="tipotolerancia/tipotolerancia.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${tipotoleranciaInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${tipotoleranciaInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${tipotoleranciaInstance.id}">
		<form action="${request.contextPath}/tipotolerancia/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${tipotoleranciaInstance?.id}" />
			<g:hiddenField name="version" value="${tipotoleranciaInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/tipotolerancia/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dsctolerancia" maxlength="30" required="" value="${tipotoleranciaInstance?.dsctolerancia}"/>
				</div>
			</div>
		<hr />
		<h4>Tolerancia em Minutos</h4>
		<br/>

			<div class="control-group">
				<label class="control-label">Antecipação Entrada:</label>
				<div class="controls">
					<g:textField name="nrTolAntEnt" required="" value="${fieldValue(bean: tipotoleranciaInstance, field: 'nrTolAntEnt')}"style="width:50px" class="span2"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Antec. 1 Saida:</label>
				<div class="controls">
					<g:textField name="nrTolAntSai" required="" value="${fieldValue(bean: tipotoleranciaInstance, field: 'nrTolAntSai')}"style="width:50px" class="span2"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Atraso na Entrada:</label>
				<div class="controls">
					<g:textField name="nrTolAtrEnt" required="" value="${fieldValue(bean: tipotoleranciaInstance, field: 'nrTolAtrEnt')}"style="width:50px" class="span2"/>				
				</div>
				
			</div>

			<div class="control-group">
				<label class="control-label">Antec. 2 Saida:</label>
				<div class="controls">
					<g:textField name="nrTolAtrSai" required="" value="${fieldValue(bean: tipotoleranciaInstance, field: 'nrTolAtrSai')}"style="width:50px" class="span2"/>
				</div>
			</div>

		</fieldset>
		
		<fieldset>
			<legend>Filiais</legend>
			
			<button type="button" class="btn btn-medium btn-primary" onclick="marcarTodos();">Marcar todos</button>
			<button type="button" class="btn btn-medium btn-primary" onclick="desmarcarTodos();">Desmarcar todos</button>
			
			<br/>
			<br/>
			
			<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th>Filial</th>
					</tr>
				</thead>
				<tbody>
					<g:set var="fs" bean="filialService"/>
					<g:each in="${fs.filiaisDoUsuario()}" var="f" status="i">
						<tr>
							<td>
								<g:if test="${tipotoleranciaInstance.id != null && cadastros.TipotoleranciaFilial.findByTipotoleranciaAndFilial(tipotoleranciaInstance, f) != null}">
									<input type="checkbox" name="filial" id="filial" value="${f.id}" checked="checked"/>
								</g:if>
								<g:else>
									<input type="checkbox" name="filial" id="filial" value="${f.id}"/>
								</g:else>
							</td>
							<td>${f}</td>
						</tr>
					</g:each>
				</tbody>
			</table>
			
		</fieldset>
		
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>