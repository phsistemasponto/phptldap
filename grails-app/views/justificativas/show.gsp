
<%@ page import="cadastros.Justificativas" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'justificativas.label', default: 'Justificativas')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-justificativas" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-justificativas" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list justificativas">
			
				<g:if test="${justificativasInstance?.dscJust}">
				<li class="fieldcontain">
					<span id="dscJust-label" class="property-label"><g:message code="justificativas.dscJust.label" default="Dsc Just" /></span>
					
						<span class="property-value" aria-labelledby="dscJust-label"><g:fieldValue bean="${justificativasInstance}" field="dscJust"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.idLimpaDesc}">
				<li class="fieldcontain">
					<span id="idLimpaDesc-label" class="property-label"><g:message code="justificativas.idLimpaDesc.label" default="Id Limpa Desc" /></span>
					
						<span class="property-value" aria-labelledby="idLimpaDesc-label"><g:fieldValue bean="${justificativasInstance}" field="idLimpaDesc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.idDebitaBh}">
				<li class="fieldcontain">
					<span id="idDebitaBh-label" class="property-label"><g:message code="justificativas.idDebitaBh.label" default="Id Debita Bh" /></span>
					
						<span class="property-value" aria-labelledby="idDebitaBh-label"><g:fieldValue bean="${justificativasInstance}" field="idDebitaBh"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.idTpJust}">
				<li class="fieldcontain">
					<span id="idTpJust-label" class="property-label"><g:message code="justificativas.idTpJust.label" default="Id Tp Just" /></span>
					
						<span class="property-value" aria-labelledby="idTpJust-label"><g:fieldValue bean="${justificativasInstance}" field="idTpJust"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.seqOutSistem}">
				<li class="fieldcontain">
					<span id="seqOutSistem-label" class="property-label"><g:message code="justificativas.seqOutSistem.label" default="Seq Out Sistem" /></span>
					
						<span class="property-value" aria-labelledby="seqOutSistem-label"><g:fieldValue bean="${justificativasInstance}" field="seqOutSistem"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.seqTpBh}">
				<li class="fieldcontain">
					<span id="seqTpBh-label" class="property-label"><g:message code="justificativas.seqTpBh.label" default="Seq Tp Bh" /></span>
					
						<span class="property-value" aria-labelledby="seqTpBh-label"><g:fieldValue bean="${justificativasInstance}" field="seqTpBh"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.idEscala}">
				<li class="fieldcontain">
					<span id="idEscala-label" class="property-label"><g:message code="justificativas.idEscala.label" default="Id Escala" /></span>
					
						<span class="property-value" aria-labelledby="idEscala-label"><g:fieldValue bean="${justificativasInstance}" field="idEscala"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.idAdcionalnot}">
				<li class="fieldcontain">
					<span id="idAdcionalnot-label" class="property-label"><g:message code="justificativas.idAdcionalnot.label" default="Id Adcionalnot" /></span>
					
						<span class="property-value" aria-labelledby="idAdcionalnot-label"><g:fieldValue bean="${justificativasInstance}" field="idAdcionalnot"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.idNewMatricula}">
				<li class="fieldcontain">
					<span id="idNewMatricula-label" class="property-label"><g:message code="justificativas.idNewMatricula.label" default="Id New Matricula" /></span>
					
						<span class="property-value" aria-labelledby="idNewMatricula-label"><g:fieldValue bean="${justificativasInstance}" field="idNewMatricula"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.idCidsn}">
				<li class="fieldcontain">
					<span id="idCidsn-label" class="property-label"><g:message code="justificativas.idCidsn.label" default="Id Cidsn" /></span>
					
						<span class="property-value" aria-labelledby="idCidsn-label"><g:fieldValue bean="${justificativasInstance}" field="idCidsn"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.vrAumentoHr}">
				<li class="fieldcontain">
					<span id="vrAumentoHr-label" class="property-label"><g:message code="justificativas.vrAumentoHr.label" default="Vr Aumento Hr" /></span>
					
						<span class="property-value" aria-labelledby="vrAumentoHr-label"><g:fieldValue bean="${justificativasInstance}" field="vrAumentoHr"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${justificativasInstance?.vrDiminuiHr}">
				<li class="fieldcontain">
					<span id="vrDiminuiHr-label" class="property-label"><g:message code="justificativas.vrDiminuiHr.label" default="Vr Diminui Hr" /></span>
					
						<span class="property-value" aria-labelledby="vrDiminuiHr-label"><g:fieldValue bean="${justificativasInstance}" field="vrDiminuiHr"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${justificativasInstance?.id}" />
					<g:link class="edit" action="edit" id="${justificativasInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
