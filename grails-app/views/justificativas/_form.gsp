<g:javascript src="justificativa/justificativa.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${justificativasInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${justificativasInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${justificativasInstance.id}">
		<form action="${request.contextPath}/justificativas/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${justificativasInstance?.id}" />
			<g:hiddenField name="version" value="${justificativasInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/justificativas/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

	       <div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
				    <g:textField name="dscJust" maxlength="40" required="" value="${justificativasInstance?.dscJust}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Tipo Justificativas:</label>
				<div class="controls">
				   <select name="idTpJust">
					    <option value="AB">Abonos</option>
						<option value="AF">Afastamentos</option>					
					</select>
				</div>
			</div>
			
			
			<div class="row-fluid">	
		       <div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Limpa Desconto?</label>
						<div class="controls">							
							<g:checkBox name="idLimpaDesc" value="${justificativasInstance?.idLimpaDesc}" checked="${justificativasInstance?.idLimpaDesc == 'S'}" /></td>
						</div>
					</div>             
				</div>		
				
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Verificação SID?</label>
						<div class="controls">							
							<g:checkBox name="idCidsn" value="${justificativasInstance?.idCidsn}" checked="${justificativasInstance?.idCidsn == 'S'}" /></td>
						</div>
					</div>             
				</div>
			</div>		
				
		
			<div class="row-fluid">	
		       <div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Lançando Nova Matricula?</label>
						<div class="controls">							
							<g:checkBox name="idNewMatricula" value="${justificativasInstance?.idNewMatricula}" checked="${justificativasInstance?.idNewMatricula == 'S'}" /></td>
						</div>
					</div>             
				</div>		
				
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Debita BH?</label>
						<div class="controls">							
							<g:checkBox name="idDebitaBh" value="${justificativasInstance?.idDebitaBh}" checked="${justificativasInstance?.idDebitaBh == 'S'}" /></td>
						</div>
					</div>             
				</div>
			</div>		
			
         	<div class="row-fluid">	
		       <div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Paga Adcional Not?</label>
						<div class="controls">							
							<g:checkBox name="idAdcionalnot" value="${justificativasInstance?.idAdcionalnot}" checked="${justificativasInstance?.idAdcionalnot == 'S'}" />
						</div>
					</div>             
				</div>
				
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Justificar suspensão?</label>
						<div class="controls">							
							<g:checkBox name="idSuspensao" value="${justificativasInstance?.idSuspensao}" checked="${justificativasInstance?.idSuspensao == 'S'}" />
						</div>
					</div>             
				</div>
               
			</div>					
					
			<div class="control-group">
				<label class="control-label">Código outro Sistema da Jusitificativa:</label>
				<div class="controls">
							<g:textField name="seqOutSistem" maxlength="12" value="${justificativasInstance?.seqOutSistem}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Dias initerruptos:</label>
				<div class="controls">
				   <g:textField name="vrDiasValido"  value="${justificativasInstance?.vrDiasValido}"/>
				</div>
			</div>

		     
		       
			<div class="control-group">
				<label class="control-label">Horas para adicionar BH:</label>
				<div class="controls">
					<g:textField name="vrAumentoHr" value="${fieldValue(bean: justificativasInstance, field: 'vrAumentoHr')}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Horas para diminuir BH:</label>
				<div class="controls">
					<g:textField name="vrDiminuiHr" value="${fieldValue(bean: justificativasInstance, field: 'vrDiminuiHr')}"/>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>