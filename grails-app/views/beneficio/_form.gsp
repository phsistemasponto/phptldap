	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="jquery.maskMoney.js" />
	<g:javascript src="beneficio/beneficio.js"/>
	<g:javascript src="filial/filial.js"/>
	<g:javascript src="comum/lov.js"/>
	<g:javascript src="tipoDeBeneficio/tipoDeBeneficio.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${beneficioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${beneficioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${beneficioInstance.id}">
		<form action="${request.contextPath}/beneficio/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${beneficioInstance?.id}" />
			<g:hiddenField name="version" value="${beneficioInstance?.version}" />
			<g:hiddenField name="sequencia"  value="${sequencia}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/beneficio/save" class="well form-horizontal" method="POST">
			<g:hiddenField name="sequencia"  value="1" />
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Tipo De Beneficio:</label>
				<div class="controls">
					<g:textField name="tipoDeBeneficioId" maxlength="5" required="" value="${beneficioInstance?.tipoDeBeneficio?.id}" style="width:50px"
						onBlur="buscaIdTipoBeneficio('#tipoDeBeneficioId','#tipoDeBeneficioNome');"
					/>
					&nbsp;
				  	<i class="icon-search" onclick="buscarLovTipoBeneficio('#tipoDeBeneficioId','#tipoDeBeneficioNome');" style="cursor:pointer" title="Procurar Tipo de Beneficio"></i>
					&nbsp;
					<g:textField name="tipoDeBeneficioNome" maxlength="450" required="" value="${beneficioInstance?.tipoDeBeneficio?.descricao}" 
						readOnly="true" style="width:450px"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Descricao:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="80" required="" value="${beneficioInstance?.descricao}"/>
				</div>
			</div>
			
			<hr />
			<h4>Valores do benefício</h4>
			<br/>

			<div class="row-fluid">	
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label">Valor:</label>
						<div class="controls">			
							<g:textField name="valorInput" maxlength="20" style="width:100px" />
						</div>
					</div>
				</div>
				<div class="span5 bgcolor">
					<div class="control-group">
						<label class="control-label">Data início:</label>
						<div class="controls">			
							<g:textField name="dataInicioInput" maxlength="10" style="width:100px" />
							<a href="#" class="btn btn-medium" onClick="incluirValor();">Incluir</a>
						</div>
					</div>
				</div>
			</div>
			
			
			<div id="valorBeneficio">
				<table class="table table-striped table-bordered table-condensed"
					id="valorBeneficioTable">
					<thead>
						<tr>
								<th style="width: 30%;">Valor</th>
								<th style="width: 30%;">Data início</th>
								<th style="width: 30%;">Data fim</th>
								<th style="width: 10%;"></th>
						</tr>
					</thead>
					<tbody>
							<g:each in="${valores?}" status="i" var="valor">
								<tr id="rowTableValorBeneficio${i+1}" class="even">
									<td>
										<g:hiddenField name="seqval" value="${valor?.sequencia}"/>
										<g:textField name="idvben" style="width:30%" value="${valor?.valorFormatado}" readOnly="true"/>
									</td>
									<td>
										<g:textField name="dtInicio" style="width:30%" value="${valor?.dataInicioFormatada}" readOnly="true"/>
									</td>
									<td>
										<g:textField name="dtFim" style="width:30%" value="${valor?.dataFimFormatada}" readOnly="true"/>
									</td>
									<td>
										<g:if test="${i == quantidadeRegistros}">
											<i class="icon-remove" onclick="excluirValor(${i+1});" style="cursor: pointer" title="Excluir registro"></i>
										</g:if>
										<g:else>
										</g:else>
									</td>
								</tr>
							</g:each>
					</tbody>
				</table>
			</div>
			
		</fieldset>
		
		<fieldset>
			<legend>Filiais</legend>
			
			<button type="button" class="btn btn-medium btn-primary" onclick="marcarTodos();">Marcar todos</button>
			<button type="button" class="btn btn-medium btn-primary" onclick="desmarcarTodos();">Desmarcar todos</button>
			
			<br/>
			<br/>
			
			<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th>Filial</th>
					</tr>
				</thead>
				<tbody>
					<g:set var="fs" bean="filialService"/>
					<g:each in="${fs.filiaisDoUsuario()}" var="f" status="i">
						<tr>
							<td>
								<g:if test="${beneficioInstance.id != null && cadastros.BeneficioFilial.findByBeneficioAndFilial(beneficioInstance, f) != null}">
									<input type="checkbox" name="filial" id="filial" value="${f.id}" checked="checked"/>
								</g:if>
								<g:else>
									<input type="checkbox" name="filial" id="filial" value="${f.id}"/>
								</g:else>
							</td>
							<td>${f}</td>
						</tr>
					</g:each>
				</tbody>
			</table>
			
		</fieldset>
		
		
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		
		<g:render template="/busca/lovBuscaInterna"/>
		
	</form>