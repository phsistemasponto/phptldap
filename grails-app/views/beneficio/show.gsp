
<%@ page import="cadastros.Beneficio" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'beneficio.label', default: 'Beneficio')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-beneficio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-beneficio" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list beneficio">
			
				<g:if test="${beneficioInstance?.descricao}">
				<li class="fieldcontain">
					<span id="descricao-label" class="property-label"><g:message code="beneficio.descricao.label" default="Descricao" /></span>
					
						<span class="property-value" aria-labelledby="descricao-label"><g:fieldValue bean="${beneficioInstance}" field="descricao"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioInstance?.filial}">
				<li class="fieldcontain">
					<span id="filial-label" class="property-label"><g:message code="beneficio.filial.label" default="Filial" /></span>
					
						<span class="property-value" aria-labelledby="filial-label"><g:link controller="filial" action="show" id="${beneficioInstance?.filial?.id}">${beneficioInstance?.filial?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioInstance?.tipoDeBeneficio}">
				<li class="fieldcontain">
					<span id="tipoDeBeneficio-label" class="property-label"><g:message code="beneficio.tipoDeBeneficio.label" default="Tipo De Beneficio" /></span>
					
						<span class="property-value" aria-labelledby="tipoDeBeneficio-label"><g:link controller="tipoDeBeneficio" action="show" id="${beneficioInstance?.tipoDeBeneficio?.id}">${beneficioInstance?.tipoDeBeneficio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${beneficioInstance?.valores}">
				<li class="fieldcontain">
					<span id="valores-label" class="property-label"><g:message code="beneficio.valores.label" default="Valores" /></span>
					
						<g:each in="${beneficioInstance.valores}" var="v">
						<span class="property-value" aria-labelledby="valores-label"><g:link controller="valorBeneficio" action="show" id="${v.id}">${v?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${beneficioInstance?.id}" />
					<g:link class="edit" action="edit" id="${beneficioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
