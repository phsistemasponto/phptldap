
<html>
  <head>
    <meta name="layout" content="main">
    <g:javascript src="comum/lov.js"/>
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Beneficios</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Beneficio']" /></g:link>
    </div>
    
    <form action="${request.contextPath}/beneficio/list" method="POST" id="formPesquisa">
    	<div class="well">
  			<h3>Filtro</h3>
  			<div class="control-group">
				<label class="control-label">Tipo de filtro:</label>				
				<div class="controls">
					<g:select name="tipoFiltro" optionKey="id" optionValue="nome" 
							value="${params.tipoFiltro}"
							style="width:100px;"
							from="${[ [id:'0', nome:'...'], 
									  [id:'1', nome:'Descrição'],
								  	  [id:'2', nome:'Tipo de Benefício'] ]}"/>
					<g:textField name="filtro" maxlength="250" value="${params.filtro}" style="margin-left: 10px; width: 400px;"/>
					<input type="hidden" id="order" name="order"/>
					<input type="hidden" id="sortType" name="sortType"/>
					<input type="hidden" id="max" name="max" value="${params.max}"/>
					<input type="hidden" id="offset" name="offset" value="${params.offset}"/>
					<g:submitButton name="filtrar" class="btn btn-medium btn-primary" value="Filtrar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>
          	Descrição
          	<g:if test="${order != 'descricao'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('descricao', 'asc')"/></g:if>
          	<g:if test="${order == 'descricao' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('descricao', 'desc')"/></g:if>
          	<g:if test="${order == 'descricao' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('descricao', 'asc')"/></g:if>
          </th>
          <th>
          	Tipo De Beneficio
          	<g:if test="${order != 'tipoDeBeneficio'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('tipoDeBeneficio', 'asc')"/></g:if>
          	<g:if test="${order == 'tipoDeBeneficio' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('tipoDeBeneficio', 'desc')"/></g:if>
          	<g:if test="${order == 'tipoDeBeneficio' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('tipoDeBeneficio', 'asc')"/></g:if>
          </th>
		  <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${beneficioInstanceList}" status="i" var="beneficioInstance">
        <tr>
		  <td>${fieldValue(bean: beneficioInstance, field: "descricao")}</td>
		  <td>${fieldValue(bean: beneficioInstance, field: "tipoDeBeneficio")}</td>
		  <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${beneficioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/beneficio/delete', ${beneficioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${beneficioInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${beneficioInstanceTotal}</span>
    </div>
  </body>
</html>