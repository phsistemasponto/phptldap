
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h2>Alteração</h2>
    </div>
    
    <br/><br/><br/>
    
    <form action="${request.contextPath}/funcionarioLote/concluir" method="POST" id="formResumo">
    
    	<input type="hidden" id="campoAlteracao" name="campoAlteracao" value="${params.campoAlteracao}">
    	<input type="hidden" id="filialId" name="filialId" value="${params.filialId}">
    	<input type="hidden" id="filialNome" name="filialNome" value="${params.filialNome}">
    	<input type="hidden" id="filialDataInicio" name="filialDataInicio" value="${params.filialDataInicio}">
    	<input type="hidden" id="setorId" name="setorId" value="${params.setorId}">
    	<input type="hidden" id="setorDescricao" name="setorDescricao" value="${params.setorDescricao}">
    	<input type="hidden" id="setorDataInicio" name="setorDataInicio" value="${params.setorDataInicio}">
    	<input type="hidden" id="cargoId" name="cargoId" value="${params.cargoId}">
    	<input type="hidden" id="cargoDescricao" name="cargoDescricao" value="${params.cargoDescricao}">
    	<input type="hidden" id="cargoDataInicio" name="cargoDataInicio" value="${params.cargoDataInicio}">
    	<input type="hidden" id="horarioId" name="horarioId" value="${params.horarioId}">
    	<input type="hidden" id="horarioDescricao" name="horarioDescricao" value="${params.horarioDescricao}">
    	<input type="hidden" id="dtAdmComoDtIniHorario" name="dtAdmComoDtIniHorario" value="${params.dtAdmComoDtIniHorario != null ? 'S' : 'N'}">
    	<input type="hidden" id="horarioDataInicio" name="horarioDataInicio" value="${params.horarioDataInicio}">
    	<input type="hidden" id="horarioIndice" name="horarioIndice" value="${params.horarioIndice}">
    	<input type="hidden" id="obrigIntervalo" name="obrigIntervalo" value="${params.obrigIntervalo != null ? 'S' : 'N'}">
    	<input type="hidden" id="intervFlex" name="intervFlex" value="${params.intervFlex != null ? 'S' : 'N'}">
    	<input type="hidden" id="sensivelFeriado" name="sensivelFeriado" value="${params.sensivelFeriado != null ? 'S' : 'N'}">
    	<input type="hidden" id="rrt" name="rrt" value="${params.rrt != null ? 'S' : 'N'}">
    	<input type="hidden" id="calcAdicionalNoturno" name="calcAdicionalNoturno" value="${params.calcAdicionalNoturno != null ? 'S' : 'N'}">
    	<input type="hidden" id="dsrRemunerado" name="dsrRemunerado" value="${params.dsrRemunerado}">
    	<input type="hidden" id="inputSalario" name="inputSalario" value="${params.inputSalario}">
    	<input type="hidden" id="inputDataAdmissao" name="inputDataAdmissao" value="${params.inputDataAdmissao}">
    	<input type="hidden" id="inputDataRescisao" name="inputDataRescisao" value="${params.inputDataRescisao}">
    	<input type="hidden" id="tipoFuncionarioId" name="tipoFuncionarioId" value="${params.tipoFuncionarioId}">
    	<input type="hidden" id="tipoFuncionarioDescricao" name="tipoFuncionarioDescricao" value="${params.tipoFuncionarioDescricao}">
    	<input type="hidden" id="configHoraExtraId" name="configHoraExtraId" value="${params.configHoraExtraId}">
    	<input type="hidden" id="configuracaoHoraExtraDescricao" name="configuracaoHoraExtraDescricao" value="${params.configuracaoHoraExtraDescricao}">
    	<input type="hidden" id="dtAdmComoDtIniConfigExtra" name="dtAdmComoDtIniConfigExtra" value="${params.dtAdmComoDtIniConfigExtra != null ? 'S' : 'N'}">
    	<input type="hidden" id="configHoraExtraDataInicio" name="configHoraExtraDataInicio" value="${params.configHoraExtraDataInicio}">
    	<input type="hidden" id="beneficioId" name="beneficioId" value="${params.beneficioId}">
    	<input type="hidden" id="beneficioDescricao" name="beneficioDescricao" value="${params.beneficioDescricao}">
    	<input type="hidden" id="beneficioDataInicio" name="beneficioDataInicio" value="${params.beneficioDataInicio}">
    	<input type="hidden" id="beneficioQuantidade" name="beneficioQuantidade" value="${params.beneficioQuantidade}">
    	
    	
    	<div class="well">
    		<h4>Funcionários</h4>
    		<br>
    		<table style="width: 450px;">
				<g:each in="${funcionarios}" var="func">
					<tr style="border: 1px solid rgba(0, 0, 0, 0.05);">
						<td style="padding: 5px;">
							${func.nomFunc}
						</td>
					</tr>
				</g:each>
    		</table>
    		
    		<hr>    		
    		
    		<g:if test="${params.campoAlteracao.equals('filial')}">
    			<span style="font-weight: bold;">Filial: </span> ${params.filialId} - ${params.filialNome} <br>
    			<span style="font-weight: bold;">Data início: </span> ${params.filialDataInicio} <br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('departamento')}">
    			<span style="font-weight: bold;">Departamento: </span> ${params.setorId} - ${params.setorDescricao} <br>
    			<span style="font-weight: bold;">Data início: </span> ${params.setorDataInicio} <br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('cargo')}">
    			<span style="font-weight: bold;">Cargo: </span> ${params.cargoId} - ${params.cargoDescricao} <br>
    			<span style="font-weight: bold;">Data início: </span> ${params.cargoDataInicio} <br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('horario')}">
    			<span style="font-weight: bold;">Horário: </span> ${params.horarioId} - ${params.horarioDescricao}  <br> 
    			<g:if test="${params.dtAdmComoDtIniHorario != null}">
    				<span style="font-weight: bold;">Data início: </span> Usar data de admissão <br>
    			</g:if>
    			<g:else>
    				<span style="font-weight: bold;">Data início: </span> ${params.horarioDataInicio} <br>
    			</g:else>
    			<span style="font-weight: bold;">Índice: </span> ${params.horarioIndice} <br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('dadosFunc')}">
    			<span style="font-weight: bold;">Bate Intervalo para Refeição: </span> ${params.obrigIntervalo != null ? 'Sim' : 'Não'} <br>
    			<span style="font-weight: bold;">Intervalo Flexível: </span> ${params.intervFlex != null ? 'Sim' : 'Não'} <br>
    			<span style="font-weight: bold;">Sensível a Feriado: </span> ${params.sensivelFeriado != null ? 'Sim' : 'Não'} <br>
    			<span style="font-weight: bold;">Repouso Remunerado Trabalhado: </span> ${params.rrt != null ? 'Sim' : 'Não'} <br>
    			<span style="font-weight: bold;">Calcula adicional noturno: </span> ${params.calcAdicionalNoturno != null ? 'Sim' : 'Não'} <br>
    			<span style="font-weight: bold;">(DSR) Descanso Semanal Remunerado: </span> ${params.dsrRemunerado} <br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('salario')}">
    			<span style="font-weight: bold;">Salário: </span> ${params.inputSalario}<br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('dataCadastral')}">
    			<span style="font-weight: bold;">Data Admissão: </span> ${params.inputDataAdmissao}<br>
    			<span style="font-weight: bold;">Data Rescisão: </span> ${params.inputDataRescisao}<br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('tipoFuncionario')}">
    			<span style="font-weight: bold;">Tipo funcionário: </span> ${params.tipoFuncionarioId} - ${params.tipoFuncionarioDescricao} <br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('horasExtras')}">
    			<span style="font-weight: bold;">Horas extras: </span> ${params.configHoraExtraId} - ${params.configuracaoHoraExtraDescricao} <br>
    			<g:if test="${params.dtAdmComoDtIniConfigExtra != null}">
    				<span style="font-weight: bold;">Data início: </span> Usar data de admissão <br>
    			</g:if>
    			<g:else>
    				<span style="font-weight: bold;">Data início: </span> ${params.configHoraExtraDataInicio} <br>
    			</g:else>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('beneficio')}">
    			<span style="font-weight: bold;">Benefício: </span> ${params.beneficioId} - ${params.beneficioDescricao} <br>
    			<span style="font-weight: bold;">Data início: </span> ${params.beneficioDataInicio} <br>
    			<span style="font-weight: bold;">Quantidade: </span> ${params.beneficioQuantidade} <br>
    		</g:if>
    		
    		<g:if test="${params.campoAlteracao.equals('adicionalNoturno')}">
    			<span style="font-weight: bold;">Adicional noturno: </span> ${params._adcNot} <br>
    		</g:if>
    		
    		<hr>
    		
    		<g:submitButton name="btnConcluir" class="btn btn-medium btn-primary" value="Concluir" />
    		<g:link action="list" class="btn btn-medium"><g:message code="default.button.return.label" /></g:link>
		
		</div>
		
	</form>
           
    <hr/>
    
  </body>
</html>