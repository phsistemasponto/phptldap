
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="filial/filial.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="setor/setor.js"/>
	<g:javascript src="tipofunc/tipofunc.js"/>
	<g:javascript src="cargo/cargo.js"/>
	<g:javascript src="uniorg/uniorg.js"/>
	<g:javascript src="beneficio/beneficio.js"/>
	<g:javascript src="horario/horario.js"/>
	<g:javascript src="comum/lov.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h2>Campos</h2>
    </div>
    
    <br/><br/><br/>
    
    <form action="${request.contextPath}/funcionarioLote/resumoAlteracao" method="POST" id="formAlteracao" class="form-horizontal">
    
    	<div class="well">
    		<table>
    			<tr>
    				<td style="padding-right: 50px;">
    					<input type="radio" name="campoAlteracao" id="campoAlteracao" value="filial"> &nbsp; Filial <br/>
			 			<input type="radio" name="campoAlteracao" id="campoAlteracao" value="departamento"> &nbsp; Departamento <br/>
			 			<input type="radio" name="campoAlteracao" id="campoAlteracao" value="cargo"> &nbsp; Cargo <br/>
			 			<input type="radio" name="campoAlteracao" id="campoAlteracao" value="horario"> &nbsp; Horário <br/>
			 			<input type="radio" name="campoAlteracao" id="campoAlteracao" value="dadosFunc"> &nbsp; Dados do funcionário <br/>
			 			<input type="radio" name="campoAlteracao" id="campoAlteracao" value="salario"> &nbsp; Salário <br/>
    				</td>
    				<td>
    					<input type="radio" name="campoAlteracao" id="campoAlteracao" value="dataCadastral"> &nbsp; Data cadastral <br/>
			 			<input type="radio" name="campoAlteracao" id="campoAlteracao" value="tipoFuncionario"> &nbsp; Tipo funcionário <br/>
			 			<input type="radio" name="campoAlteracao" id="campoAlteracao" value="horasExtras"> &nbsp; Autorização de extras <br/>
			 			<input type="radio" name="campoAlteracao" id="campoAlteracao" value="beneficio"> &nbsp; Benefícios <br/>
    				</td>
    			</tr>
    		</table>
    		
    		<hr/>
    		
    		<div id="areasAjustes">
    		
    			<div id="ajuste_campo_filial">
    				<div class="control-group">
						<label class="control-label">Filial:</label>				
						<div class="controls">
							<g:textField name="filialId" maxlength="5" style="width:50px" onBlur="buscaIdFilial();"/>&nbsp;
						  	<i class="icon-search" onclick="buscarLovFilial();" style="cursor:pointer" title="Procurar Filial"></i>&nbsp;
							<g:textField name="filialNome" readonly="true" style="width:450px"/>&nbsp;
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_filial">
					<div class="control-group">
						<label class="control-label">Data início:</label>				
						<div class="controls">
							<g:textField name="filialDataInicio" style="width:80px" />
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_departamento">
    				<div class="control-group">
						<label class="control-label">Departamento:</label>				
						<div class="controls">
							<g:textField name="setorId" maxlength="5" style="width:50px" onBlur="buscaIdSetorComFilial();"/>&nbsp;
							<i class="icon-search" onclick="buscarLovSetor();" 
								style="cursor:pointer" title="Procurar Setor" id="setorBotaoBusca"></i>&nbsp;
							<g:textField name="setorDescricao" readonly="true" style="width:450px"/>&nbsp;
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Data início:</label>				
						<div class="controls">
							<g:textField name="setorDataInicio" style="width:80px" />
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_cargo">
    				<div class="control-group">
						<label class="control-label">Cargo:</label>				
						<div class="controls">
							<g:textField name="cargoId" maxlength="5" style="width:50px" onBlur="buscaIdCargo();"/>&nbsp;
							<i class="icon-search" onclick="buscarLovCargo();" style="cursor:pointer" title="Procurar Cargo"></i>&nbsp;
							<g:textField name="cargoDescricao" readonly="true" style="width:450px"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Data início:</label>				
						<div class="controls">
							<g:textField name="cargoDataInicio" style="width:80px" />
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_horario">
    				<div class="control-group">
						<label class="control-label">Horário:</label>				
						<div class="controls">
							<g:textField name="horarioId" maxlength="5" style="width:50px" onBlur="buscaIdHorario();"/>&nbsp;
							<i class="icon-search" onclick="buscarLovHorario();" style="cursor:pointer" title="Procurar Horário"></i>&nbsp;
							<g:textField name="horarioDescricao" maxlength="450" readOnly="true" style="width:450px"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Usa data de admissão na data início:</label>				
						<div class="controls">
							<input type="checkbox" id="dtAdmComoDtIniHorario" name="dtAdmComoDtIniHorario" value="S" onchange="alteraCheckHorario();">
						</div>
					</div>
					<div class="control-group" id="divDtIniHorario">
						<label class="control-label">Data início:</label>				
						<div class="controls">
							<g:textField name="horarioDataInicio" style="width:80px" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Índice:</label>				
						<div class="controls">
							<g:textField name="horarioIndice" style="width:30px" />
							<a id="viewIndice" class="btn btn-medium" 
										onClick="visualizaIndice('#horarioId', '#horarioDataInicio', '#horarioIndice');">Visualiza o índice</a>
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_dadosFunc">
    				<div class="control-group">
						<div class="row show-grid">
							<div class="span4" data-original-title="">
								<label class="checkbox inline">
							    	<input type="checkbox" id="obrigIntervalo" name="obrigIntervalo" value="S"> Bate Intervalo para Refeição
							    </label>
							</div>
							<div class="span4" data-original-title="">
								<label class="checkbox inline">
									<input type="checkbox" id="intervFlex" name="intervFlex" value="S"> Intervalo Flexível
							    </label>
							</div>
						</div>
						<div class="row show-grid">
							<div class="span4" data-original-title="">
							    <label class="checkbox inline">
							    	<input type="checkbox" id="sensivelFeriado" name="sensivelFeriado" value="S"> Sensível a Feriado
							    </label>
							</div>
							<div class="span4" data-original-title="">
							    <label class="checkbox inline">
							    	<input type="checkbox" id="rrt" name="rrt" value="S"> Repouso Remunerado Trabalhado
							    </label>
							</div>
						</div>
						<div class="row show-grid">
							<div class="span4" data-original-title="">
							    <label class="checkbox inline">
							    	<input type="checkbox" id="calcAdicionalNoturno" name="calcAdicionalNoturno" value="S"> Calcula Adicional Noturno
							    </label>
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">(DSR) Descanso Semanal Remunerado:</label>
						<div class="controls">
							<g:textField name="dsrRemunerado" style="width:50px"/>
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_salario">
    				<div class="control-group">
						<label class="control-label">Salário:</label>				
						<div class="controls">
							<g:textField name="inputSalario" style="width:100px"/>&nbsp;
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_dataCadastral">
    				<div class="control-group">
						<label class="control-label">Admissão:</label>		
						<div class="controls">
							<g:textField name="inputDataAdmissao" style="width:80px"/>&nbsp;
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Rescisão:</label>		
						<div class="controls">
							<g:textField name="inputDataRescisao" style="width:80px"/>&nbsp;
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_tipoFuncionario">
    				<div class="control-group">
						<label class="control-label">Tipo funcionário:</label>				
						<div class="controls">
							<g:textField name="tipoFuncionarioId" maxlength="5" style="width:50px" onBlur="buscaIdTipoFuncionario();"/>&nbsp;
							<i class="icon-search" onclick="buscarLovTipoFuncionario();" style="cursor:pointer" title="Procurar Setor" id="tipoFuncionarioBotaoBusca"></i>&nbsp;
							<g:textField name="tipoFuncionarioDescricao" readonly="true" style="width:450px"/>
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_horasExtras">
    				<div class="control-group">
						<label class="control-label">Configuração hora extra:</label>				
						<div class="controls">
							<g:textField name="configHoraExtraId" maxlength="5" style="width:50px" onBlur="buscaIdConfiguracaoHoraExtra();"/>&nbsp;
							<i class="icon-search" onclick="buscarLovConfiguracaoHoraExtra();" style="cursor:pointer" title="Procurar Configuração Hora Extra"></i>&nbsp;
							<g:textField name="configuracaoHoraExtraDescricao" maxlength="450" readOnly="true" style="width:450px"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Usa data de admissão na data início:</label>				
						<div class="controls">
							<input type="checkbox" id="dtAdmComoDtIniConfigExtra" name="dtAdmComoDtIniConfigExtra" value="S" onchange="alteraCheckConfigExtra();">
						</div>
					</div>
					<div class="control-group" id="divDtIniConfigExtra">
						<label class="control-label">Data início:</label>				
						<div class="controls">
							<g:textField name="configHoraExtraDataInicio" style="width:80px" />
						</div>
					</div>
    			</div>
    			
    			<div id="ajuste_beneficio">
    				<div class="control-group">
						<label class="control-label">Benefício:</label>				
						<div class="controls">
							<g:textField name="beneficioId" maxlength="5" style="width:50px" onBlur="buscaIdBeneficio();"/>&nbsp;
							<i class="icon-search" onclick="buscarLovBeneficio();" style="cursor:pointer" title="Procurar Benefício"></i>&nbsp;
							<g:textField name="beneficioDescricao" maxlength="450" readOnly="true" style="width:450px"/>&nbsp;
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Valor:</label>		
						<div class="controls">
							<g:textField name="beneficioValor" style="width:80px" readOnly="true"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Quantidade:</label>		
						<div class="controls">
							<g:textField name="beneficioQuantidade" style="width:80px" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Data início:</label>				
						<div class="controls">
							<g:textField name="beneficioDataInicio" style="width:80px" />
						</div>
					</div>
    			</div>
    			
    		</div>
    		
    		<hr>
		
			<button type="button" name="btnProximo" class="btn btn-medium btn-primary" onclick="submeteAlteracao()">Próximo</button>
			<g:link action="list" class="btn btn-medium"><g:message code="default.button.return.label" /></g:link>
		
		</div>
		
		<g:render template="/busca/lovBuscaInterna"/>
		<g:render template="/horario/lovIndiceHorario"/>
		
	</form>
           
    <hr/>
    
  </body>
</html>