
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Atualização de cadastros</h3>
    </div>
    
    <form action="${request.contextPath}/funcionarioLote/list" method="POST" id="formPesquisa">
    	<div class="well">
 			<div class="control-group">
				<label class="control-label">Filtro:</label>
				<div class="controls">
					<input type="hidden" name="filtroId" id="filtroId">
					<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
					<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
					<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
					<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
					<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
					<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
					<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
					<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
					
					<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
					<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					<g:submitButton name="btnFiltro" class="btn btn-medium btn-primary" value="OK" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
			
		</div>
	</form>
	
    <hr/>

	<form action="${request.contextPath}/funcionarioLote/selecionaTipoAlteracao" method="POST" id="formFuncionarios">
		
		<div style="float: right;">
			<button class="btn" type="button" onClick="marcarTodosFuncionarios();">Marcar Todos</button> 
			<button class="btn" type="button" onClick="desmarcarTodosFuncionarios();">Desmarcar Todos</button></th>
		</div>
		
		<br><br>
		
	    <table class="table table-striped table-bordered table-condensed">
	      <thead>
	        <tr>
	          <th style="width: 20px;">
	          </th>
	          <th style="width: 100px;">
	          	<span>Id</span>
	          </th>
	          <th style="width: 100px;">
	          	<span>Matrícula</span>
	          </th>
	          <th style="width: 100px;">
	          	<span>Crachá</span>
	          </th>
	          <th style="width: 775px;">
	          	<span>Nome</span>
	          </th>
	        </tr>
	      </thead>
	
	      <tbody>
	      <g:each in="${funcionarioInstanceList}" status="i" var="funcionarioInstance">
	        <tr>
	          <td style="width: 20px;">
	          	<input type="checkbox" name="funcionarioSelecionado" id="funcionarioSelecionado" value="${funcionarioInstance?.id}">
	          </td>
	          <td style="width: 100px;">${funcionarioInstance?.id}</td>
	          <td style="width: 100px;">${funcionarioInstance?.mtrFunc}</td>
	          <td style="width: 100px;">${funcionarioInstance?.crcFunc}</td>
	          <td style="width: 775px;">${funcionarioInstance?.nomFunc}</td>
	        </tr>
	      </g:each>
	      </tbody>
	    </table>
	    
	    <div class="paginate">
	      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${funcionarioInstanceTotal}</span>
	    </div>
	
	    <g:if test="${funcionarioInstanceTotal > 0}">
	    	<g:submitButton name="btnProximo" class="btn btn-medium btn-primary" value="Próximo" />
	    </g:if>
	    
    </form>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
  </body>
</html>