
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="gerarArquivoAfdt/gerarArquivoAfdt.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Geração do arquivo fonte dados - AFDT</h3>
    </div>
    
    <g:form class="well form-horizontal" controller="gerarArquivoAfdt" action="index">
    
    	<div class="well" style="border: 1px solid #859CA4; background-color: #859CA4;">
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Filtro:</label>
				<div class="controls">
					<input type="hidden" name="filtroId" id="filtroId">
					<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
					<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
					<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
					<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
					<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
					<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
					<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
					<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
					
					<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
					<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Data início:</label>
			<div class="controls">
				<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Data fim:</label>
			<div class="controls">
				<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" />
			</div>
		</div>
		
		<button class="btn btn-medium btn-primary" onclick="if (validateForm()) gerarArquivo(); else return false;">Executar</button>
    
    </g:form>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
    <div id="baixarArquivo" class="carregandoDiv">
    	<div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<g:form class="well" controller="gerarArquivoAfdt" action="baixarArquivo">
	    		<input type="hidden" id="baixarArquivoId" name="baixarArquivoId" value=""/>
	    		<g:submitButton name="btnBaixarArquivo" value="Baixar arquivo" class="btn btn-medium btn-primary"/>
	    		<button type="button" id="btnFechar" class="btn btn-medium btn-primary" onclick="hideBaixar();">Fechar</button> 
	    	</g:form>
    	</div>
    </div>
    
  </body>
</html>