
<%@ page import="cadastros.Situacaofunc" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'situacaofunc.label', default: 'Situacaofunc')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-situacaofunc" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-situacaofunc" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list situacaofunc">
			
				<g:if test="${situacaofuncInstance?.dscstfunc}">
				<li class="fieldcontain">
					<span id="dscstfunc-label" class="property-label"><g:message code="situacaofunc.dscstfunc.label" default="Dscstfunc" /></span>
					
						<span class="property-value" aria-labelledby="dscstfunc-label"><g:fieldValue bean="${situacaofuncInstance}" field="dscstfunc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${situacaofuncInstance?.idstfunc}">
				<li class="fieldcontain">
					<span id="idstfunc-label" class="property-label"><g:message code="situacaofunc.idstfunc.label" default="Idstfunc" /></span>
					
						<span class="property-value" aria-labelledby="idstfunc-label"><g:fieldValue bean="${situacaofuncInstance}" field="idstfunc"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${situacaofuncInstance?.id}" />
					<g:link class="edit" action="edit" id="${situacaofuncInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
