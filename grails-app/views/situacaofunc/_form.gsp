<g:javascript src="situacaofunc/situacaofunc.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${situacaofuncInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${situacaofuncInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${situacaofuncInstance.id}">
		<form action="${request.contextPath}/situacaofunc/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${situacaofuncInstance?.id}" />
			<g:hiddenField name="version" value="${situacaofuncInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/situacaofunc/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscstfunc" maxlength="30" required="" value="${situacaofuncInstance?.dscstfunc}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Tipo:</label>
				<div class="controls">
				<select name="idstfunc">
                   <option value="A">Ativo</option>
                    <option value="D">Demitido</option>
    				<option value="F">Afastado</option>					
				</select>					
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>