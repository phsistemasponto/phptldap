
<%@page import="cadastros.Ocorrencias"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    <g:javascript src="reports/absenteismo.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Relatório de Absenteísmo</h3>
    </div>
    
    <g:jasperForm controller="relatorioAbsenteismo" action="generateReport" jasper="absenteismo" class="form-horizontal" id="formPesquisa">
    
    	<div class="well">
    	
    		<div class="well" style="border: 1px solid #859CA4; background-color: #859CA4;">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					</div>
				</div>
			</div>
			
			<div class="tabbable"> 
			    <ul class="nav nav-tabs">
				    <li class="active"><a href="#tab1" data-toggle="tab">Opções básicas</a></li>
				    <li><a href="#tab2" data-toggle="tab">Justificativas</a></li>
			    </ul>
			    <div class="tab-content">
			    
			    	<!--
			    	  --
			    	  -- TAB OPCOES BASICAS
			    	  -- 
			    	 -->
				    <div class="tab-pane active" id="tab1">
				    
				    	
				    	<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Data início:</label>
							<div class="controls">
								<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" />
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Data fim:</label>
							<div class="controls">
								<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" />
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Tipo de relatório:</label>
							<div class="controls">
								<g:select name="tipoRelatorio" id="tipoRelatorio" optionKey="id" optionValue="nome" 
										value="${params.tipoRelatorio}"
										style="width:100px;"
										from="${[ [id:'0', nome:'Analítico'],
												  [id:'1', nome:'Sintético']  ]}"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Tipo de agrupamento:</label>
							<div class="controls">
								<g:select name="agrupamento" id="agrupamento" optionKey="id" optionValue="nome" 
										value="${params.agrupamento}"
										style="width:150px;"
										from="${[ [id:'0', nome:'Filial'],
												  [id:'1', nome:'Departamento'],
												  [id:'2', nome:'Função'],
												  [id:'3', nome:'Funcionário']  ]}"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Ordenação:</label>
							<div class="controls">
								<g:select name="ordenacao" id="ordenacao" optionKey="id" optionValue="nome" 
										value="${params.ordenacao}"
										style="width:200px;"
										from="${[ [id:'1', nome:'Código'],
												  [id:'2', nome:'Descrição'] ]}"/>
							</div>
						</div>
						
				    </div>
				    
				    <!--
			    	  --
			    	  -- TAB OPCOES JUSTIFICATIVAS
			    	  -- 
			    	 -->
				    <div class="tab-pane" id="tab2">
				    	
				    	<fieldset>
				    		<legend>Justificativas para falta:</legend>
				    		
				    		<div class="control-group">
								<label class="control-label" style="font-weight: bold;">Justificativa:</label>
								<div class="controls">
									<g:textField name="justFaltaId" style="width:40px" onchange="buscaIdJustificativaFalta();" />
									<i class="icon-search" style="cursor:pointer" title="Procurar Justificativa" id="justificativaBotaoBusca"
										onclick="buscaInternaJustificativaFalta();"></i>&nbsp;
									<g:textField name="justFaltaDesc" readonly="true" style="width:250px"/>
									<button type="button" class="btn btn-medium" onclick="incluirFalta();">Incluir</button>
								</div>
							</div>
							
							<table class="table table-striped table-bordered table-condensed" id="faltasTable">
								<thead>
									<tr>
										<th>Descrição</th>
										<th width="15px">&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									<g:each in="${justificativasFaltas}" status="i" var="justificativa">
										<tr>
											<td>${justificativa.justificativas.dscJust}</td>
											<td>
												<input type="hidden" id="justificativasFaltasId" name="justificativasFaltasId" value="${justificativa.justificativas.id}"/>
												<i class="icon-remove" style="cursor:pointer" title="Excluir registro" onclick="excluirRegistro(${i+1}, '#faltasTable');"></i>
											</td>
										</tr>
									</g:each>
								</tbody>
							</table>
				    		
				    		<input type="hidden" name="justificativasFaltasId" id="justificativasFaltasId" value="0">
				    	</fieldset>
				    	
				    	<br><br>
				    	
				    	<fieldset>
				    		<legend>Justificativas para suspensão:</legend>
				    		
				    		<div class="control-group">
								<label class="control-label" style="font-weight: bold;">Justificativa:</label>
								<div class="controls">
									<g:textField name="justSuspensaoId" style="width:40px" onchange="buscaIdJustificativaSuspensao();" />
									<i class="icon-search" style="cursor:pointer" title="Procurar Justificativa" id="justificativaBotaoBusca"
										onclick="buscaInternaJustificativaSuspensao();"></i>&nbsp;
									<g:textField name="justSuspensaoDesc" readonly="true" style="width:250px"/>
									<button type="button" class="btn btn-medium" onclick="incluirSuspensao();">Incluir</button>
								</div>
							</div>
							
							<table class="table table-striped table-bordered table-condensed" id="suspensaoTable">
								<thead>
									<tr>
										<th>Descrição</th>
										<th width="15px">&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									<g:each in="${justificativasSuspensao}" status="i" var="justificativa">
										<tr>
											<td>${justificativa.justificativas.dscJust}</td>
											<td>
												<input type="hidden" id="justificativasSuspensaoId" name="justificativasSuspensaoId" value="${justificativa.justificativas.id}"/>
												<i class="icon-remove" style="cursor:pointer" title="Excluir registro" onclick="excluirRegistro(${i+1}, '#suspensaoTable');"></i>
											</td>
										</tr>
									</g:each>
								</tbody>
							</table>
				    		
				    		<input type="hidden" name="justificativasSuspensaoId" id="justificativasSuspensaoId" value="0">
				    	</fieldset>
				    	
				    </div>
				    
				</div>
				
			</div>
				    
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('PDF'); else return false;">Gerar em PDF</button>
			<button class="btn btn-medium btn-primary" onclick="if (validateReport()) generateReport('XLS'); else return false;">Gerar em Excel</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

	<div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>