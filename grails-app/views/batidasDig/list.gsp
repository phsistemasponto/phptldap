<html>
  <head>
    <meta name="layout" content="main">
    
    <style type="text/css">
       .timeInput {
       		margin-bottom: 0px;
       		height: 16px;
       }
    </style>
    
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="espelhoPonto/espelhoPonto.js"/>
    <g:javascript src="batidasDig/batidasDig.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Cartão ponto</h3>
    </div>    
    <form action="${request.contextPath}/batidasDig/list" method="POST" id="formPesquisa" class="form-horizontal">
    	<div class="well">
    		<div class="control-group">
				<label class="control-label">Funcionário:</label>
				<div class="controls">
					<g:textField name="funcionarioId" maxlength="5" style="width:50px" onBlur="buscaIdFuncionario();" 
						value="${params.funcionarioId}"/>&nbsp; 
					<i class="icon-search" onclick="buscarLovFuncionario();" style="cursor: pointer" title="Procurar Funcionário" id="funcionarioBotaoBusca"></i> &nbsp;
					<g:textField name="funcionarioNome" maxlength="450" readOnly="true" style="width:450px" 
						value="${params.funcionarioNome}"/>
				</div>
			</div>
			<div class="control-group">
		    	<div class="span3">
					<div class="controls-horizontal">
						<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" placeholder="Data início:" required="required"/>
						&emsp;<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" placeholder="Data fim:" required="required"/>
					</div>
				</div>
				<div class="span1">
					<div class="controls-horizontal">
						<g:submitButton name="btnFiltro" class="btn btn-medium btn-info" value="Visualizar"/>
					</div>
				</div>
			</div>
			
		</div>
	</form>
           
    <hr/>
	<form action="${request.contextPath}/batidasDig/salvar" method="POST" id="formBatidas">
		
		<input type="hidden" name="funcionarioId" value="${params.funcionarioId}">
		<input type="hidden" name="dataInicio" value="${params.dataInicio}">
		<input type="hidden" name="dataFim" value="${params.dataFim}">
		
		<table class="table table-bordered table-condensed" style="width: 0%;">
	      <thead>
	        <tr>
	          <th class="span2">
	          	<span class="text-center">Dia</span>
	          </th>
	          <th class="span2">
	          	<span>Data</span>
	          </th>
	          <th class="span3">
	          	<span>Entrada 1</span>
	          </th>
	          <th class="span2">
	          	<span>Saída 1</span>
	          </th>
	          <th class="span3">
	          	<span>Entrada 2</span>
	          </th>
	          <th class="span3">
	          	<span>Saída 2</span>
	          </th>
	          <th class="span3">
	          	<span>Entrada 3</span>
	          </th>
	          <th class="span3">
	          	<span>Saída 3</span>
	          </th>
	        </tr>
	      </thead>
	
	      <tbody>
	      <g:each in="${batidas}" status="i" var="batida">
	        <tr>
	        	<td style="vertical-align: middle;">${batida.dia}</td>
	        	<td style="vertical-align: middle;">${batida.data}</td>
	        	<td>
	        		<input type="text" name="bat-${batida.idData}-E1" value="${batida.entrada1}" style="width:60px" class="timeInput">
	        	</td>
	        	<td>
	        		<input type="text" name="bat-${batida.idData}-S1" value="${batida.saida1}" style="width:60px" class="timeInput">
	        	</td>
	        	<td>
	        		<input type="text" name="bat-${batida.idData}-E2" value="${batida.entrada2}" style="width:60px" class="timeInput">
	        	</td>
	        	<td>
	        		<input type="text" name="bat-${batida.idData}-S2" value="${batida.saida2}" style="width:60px" class="timeInput">
	        	</td>
	        	<td>
	        		<input type="text" name="bat-${batida.idData}-E3" value="${batida.entrada3}" style="width:60px" class="timeInput">
	        	</td>
	        	<td>
	        		<input type="text" name="bat-${batida.idData}-S3" value="${batida.saida3}" style="width:60px" class="timeInput">
	        	</td>
	        </tr>
	      </g:each>
	      </tbody>
	    </table>
	    
	    <div class="span1">
			<div class="controls-horizontal">
				<g:submitButton name="btnFiltro" class="btn btn-medium btn-info" value="Salvar"/>
			</div>
		</div>
		
		<br><br>
	    
    </form>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
     
  </body>
</html>