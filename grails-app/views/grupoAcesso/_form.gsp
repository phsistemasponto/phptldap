

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${grupoAcessoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${grupoAcessoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${grupoAcessoInstance.id}">
		<form action="${request.contextPath}/grupoAcesso/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${grupoAcessoInstance?.id}" />
			<g:hiddenField name="version" value="${grupoAcessoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/grupoAcesso/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Perfil de Acesso:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="50" required="" value="${grupoAcessoInstance?.descricao}"/>
				</div>
			</div>

		</fieldset>
		
		<g:if test="${grupoAcessoInstance.id}">
			<hr>
			<h4>Acessos</h4>
			<br />
			<div>
				<table class="table table-striped table-bordered table-condensed"
					id="perfisTable">
					<thead>
						<tr>
							<th style="width: 40%;">Perfil</th>
							<th style="width: 10%;">Acesso </th>
							<th style="width: 50%;">&nbsp; <button class="btn" type="button" onClick="marcarTodos();">Marcar Todos</button> <button class="btn" type="button" onClick="desmarcarTodos();">Desmarcar Todos</button></th>
						</tr>
					</thead>
	
					<tbody>
						<g:each in="${perfis?}" status="i" var="item">
							<tr id="rowTablePerfil${i}" class="even">
								<td><g:hiddenField name="perfilId" value="${item?.perfil?.id}" />
									<g:hiddenField name="permissao" value="${item?.temPerfil}" />
									<g:textField name="descricaoPerfil" style="width:90%" value="${item?.perfil?.textoExibicao}" readOnly="true" />
								</td>
								<td><g:checkBox name="temPerfil${i}" value="${item?.temPerfil}" onChange="atualizaPermissao(${i});"/></td>
								<td/>
							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</g:if>
		
		
		
		<hr>
		
		
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>