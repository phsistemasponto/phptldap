
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Perfis de Acesso</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Perfil de Acesso']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Descrição</th>
		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${grupoAcessoInstanceList}" status="i" var="grupoAcessoInstance">
        <tr>
		
          <td>${fieldValue(bean: grupoAcessoInstance, field: "descricao")}</td>
		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${grupoAcessoInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/grupoAcesso/delete', ${grupoAcessoInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${grupoAcessoInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${grupoAcessoInstanceTotal}</span>
    </div>
  </body>
</html>