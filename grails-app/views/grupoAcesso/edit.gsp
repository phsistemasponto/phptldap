<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
  	<g:javascript src="grupoAcesso/grupoAcesso.js"/>
    <h2><g:message code="default.edit.label" args="['Perfil de Acesso']" /></h2>
    <g:render template="form"/>
  </body>
</html>