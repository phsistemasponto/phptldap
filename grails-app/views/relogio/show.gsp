
<%@ page import="cadastros.Relogio" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'relogio.label', default: 'Relogio')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-relogio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-relogio" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list relogio">
			
				<g:if test="${relogioInstance?.dscRel}">
				<li class="fieldcontain">
					<span id="dscRel-label" class="property-label"><g:message code="relogio.dscRel.label" default="Dsc Rel" /></span>
					
						<span class="property-value" aria-labelledby="dscRel-label"><g:fieldValue bean="${relogioInstance}" field="dscRel"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${relogioInstance?.fabricante}">
				<li class="fieldcontain">
					<span id="fabricante-label" class="property-label"><g:message code="relogio.fabricante.label" default="Fabricante" /></span>
					
						<span class="property-value" aria-labelledby="fabricante-label"><g:fieldValue bean="${relogioInstance}" field="fabricante"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${relogioInstance?.idOnline}">
				<li class="fieldcontain">
					<span id="idOnline-label" class="property-label"><g:message code="relogio.idOnline.label" default="Id Online" /></span>
					
						<span class="property-value" aria-labelledby="idOnline-label"><g:fieldValue bean="${relogioInstance}" field="idOnline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${relogioInstance?.idAfd}">
				<li class="fieldcontain">
					<span id="idAfd-label" class="property-label"><g:message code="relogio.idAfd.label" default="Id Afd" /></span>
					
						<span class="property-value" aria-labelledby="idAfd-label"><g:fieldValue bean="${relogioInstance}" field="idAfd"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${relogioInstance?.idAfdt}">
				<li class="fieldcontain">
					<span id="idAfdt-label" class="property-label"><g:message code="relogio.idAfdt.label" default="Id Afdt" /></span>
					
						<span class="property-value" aria-labelledby="idAfdt-label"><g:fieldValue bean="${relogioInstance}" field="idAfdt"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${relogioInstance?.idEntSai}">
				<li class="fieldcontain">
					<span id="idEntSai-label" class="property-label"><g:message code="relogio.idEntSai.label" default="Id Ent Sai" /></span>
					
						<span class="property-value" aria-labelledby="idEntSai-label"><g:fieldValue bean="${relogioInstance}" field="idEntSai"/></span>
					
				</li>
				</g:if>
			

			
				<g:if test="${relogioInstance?.filial}">
				<li class="fieldcontain">
					<span id="filial-label" class="property-label"><g:message code="relogio.filial.label" default="Filial" /></span>
					
						<span class="property-value" aria-labelledby="filial-label"><g:link controller="filial" action="show" id="${relogioInstance?.filial?.id}">${relogioInstance?.filial?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${relogioInstance?.id}" />
					<g:link class="edit" action="edit" id="${relogioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
