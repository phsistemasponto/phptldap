	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="relogio/relogio.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${relogioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${relogioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${relogioInstance.id}">
		<form action="${request.contextPath}/relogio/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${relogioInstance?.id}" />
			<g:hiddenField name="version" value="${relogioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/relogio/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>
		
			<div class="control-group">
				<label class="control-label">Filial:</label>
				<div class="controls">
					<g:set var="fs" bean="filialService"/>
					<g:select name="filialId" from="${fs.filiaisDoUsuario()}" optionKey="id" required="" value="${relogioInstance?.filial?.id}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscRel" maxlength="60" value="${relogioInstance?.dscRel}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Fabricante:</label>
				<div class="controls">
					<g:textField name="fabricante" maxlength="40" value="${relogioInstance?.fabricante}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Arquivo:</label>
				<div class="controls">
					<input type="file" id="files" name="files[]" />
				</div>
			</div>

			<div class="row-fluid">	
		       <div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Online?</label>
						<div class="controls">							
							<g:checkBox name="idOnline" value="${relogioInstance?.idOnline}" checked="${relogioInstance?.idOnline == 'S'}" /></td>							
						</div>
					</div>             
				</div>
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Importar do arquivo AFDT?</label>
						<div class="controls">							
							<g:checkBox name="idAfdt" value="${relogioInstance?.idAfdt}" checked="${relogioInstance?.idAfdt == 'S'}" /></td>														
						</div>
					</div>             
				</div>
			</div>		
				
			
			<div class="row-fluid">
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Importar do arquivo AFD?</label>
						<div class="controls">							
							<g:checkBox name="idAfd" value="${relogioInstance?.idAfd}" checked="${relogioInstance?.idAfd == 'S'}" /></td>							
						</div>
					</div>             
				</div>	
		       	<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Importar Verificação Entrada/Saida?</label>
						<div class="controls">							
							<g:checkBox name="idEntSai" value="${relogioInstance?.idEntSai}" checked="${relogioInstance?.idEntSai == 'S'}" /></td>			
						</div>
					</div>             
				</div>
			</div>
		
		
			<fieldset>
				<legend>Adicionar parâmetro</legend>
				
				<div class="control-group">
					<label class="control-label">Índice:</label>
					<div class="controls">
						<g:select name="indice" from="${cadastros.IndiceLayoutRelogio.list()}" optionKey="id"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Configuração:</label>
					<div class="controls">
						<div id="list" style="height: 50px; width:300px; padding: 7px; border: 1px solid #DDDDDD;" onmouseup="configuraPosicoes()">
						</div>
						<button type="button" onclick="$j('#listTodo').show();" >Mostrar todo</button>
						<div id="listTodo" style="height: 50px; width:300px; padding: 7px; border: 1px solid #DDDDDD; display: none;">
						</div>
						<input type="hidden" name="conteudoArquivo" id="conteudoArquivo" value="${relogioInstance?.conteudoArquivo}">
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Inicio param:</label>
					<div class="controls">
						<g:textField name="inicioParam" maxlength="5"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Fim param:</label>
					<div class="controls">
						<g:textField name="fimParam" maxlength="5"/>
						<a id="incParametro" class="btn btn-medium" onClick="incluirParametro();">Incluir</a>
					</div>
				</div>
				
				<div id="parametros">
					<table class="table table-striped table-bordered table-condensed" id="parametroTable">
						<thead>
							<tr>
								<th style="width: 30%;">Indice</th>
								<th style="width: 30%;">Início param</th>
								<th style="width: 30%;">Fim param</th>
								<th style="width: 10%;"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type="hidden" id="sequencialTemp" name="sequencialTemp" value="0"/></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						
							<g:each in="${parametrosRelogio}" status="i" var="parametro">
								<tr id="rowTableParametro${i+1}" class="even">
									<td> 
										<input type="text" id="nomeIndice" name="nomeIndice" value="${parametro.indice.descricao}" readonly style="width:30%"/>
										<input type="hidden" id="idIndice" name="idIndice" value="${parametro.indice.id}"/>
										<input type="hidden" id="sequencialTemp" name="sequencialTemp" value="${i+1}"/>
									</td>
									<td> 
										<input type="text" id="posIni" name="posIni" value="${parametro.posicaoInicial}" readOnly style="width:30%"/> 
									</td>
									<td> 
										<input type="text" id="posFim" name="posFim" value="${parametro.posicaoFinal}" readOnly style="width:30%"/> 
									</td>
									<td> 
										<i class="icon-remove" onclick="excluirParametro(${i+1});" style="cursor:pointer" title="Excluir registro"></i> 
									</td>
								</tr>
							</g:each>
						
						</tbody>			
						
						
					</table>
				</div>
				
			</fieldset>

		</fieldset>
		
		<hr>
			<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
			<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		</hr>
		
	</form>