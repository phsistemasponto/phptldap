<!DOCTYPE html>
<html lang="pt-BR">
  <head>
 
    <title>PHPONTO</title>

    <meta charset="utf-8">   
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-ico" />

    <link rel="stylesheet" href="${resource(dir:'css',file:'bootstrap.css')}" />
    <style type="text/css">
      html, body {
        background-color: #00577f;
        background-image: url(../images/fundo_login.png);
        background-repeat: no-repeat;
        background-position: center;
        background-position: top;
      }
      body {
        
        height: 100%;
        
      }
      .container {
        width: 300px;
        
      }
      .container > .content {
        background-color: #fff;
        
        padding: 20px;
        margin: 60px -20px;
        -webkit-border-radius: 10px 10px 10px 10px;
           -moz-border-radius: 10px 10px 10px 10px;
                border-radius: 10px 10px 10px 10px;
        -webkit-box-shadow: 2px 1px 19px -1px rgba(0,0,0,0.7);
		-moz-box-shadow: 2px 1px 19px -1px rgba(0,0,0,0.7);
		box-shadow: 2px 1px 19px -1px rgba(0,0,0,0.7);
      }
      .login-form {
        margin-left: 65px;
      }
      .logo_login{
      	text-align: center;
      	margin: 20px 0 20px 0;

      }
      .logo_login span{
      	font-size: 25px;
      	font-weight:bold;
      	color: #0898DA;
      	margin: 20px 0 20px 0;
      	display: block;
      }
      
      .button_login{
        position: relative;
      	text-align: right;
      	background-color: 000;
      }
      .button_login a{
      	position: absolute;
      	left: 40px;
      }
      .button_login input{
      	position: absolute;
      	right: 40px;
      }
      legend {
        margin-right: -50px;
        font-weight: bold;
        color: #404040;
      } 
    </style>
  </head>

  <body>
  <script type="text/javascript" src="${resource(dir:'js',file:'jquery-1.8.0.min.js')}"></script>
  <g:javascript src="login/login.js"/>
  <div class="container" style="width: 980px;">
  	
  </div>
    <div class="container">
      <div class="content">

        <g:if test='${flash.message}'>
        <div class='alert alert-info' style="text-align: center;">${flash.message}</div>
        
        </g:if>
        <div class="logo_login">
        	<img src="${resource(dir:'images',file:'logo_login.png')}" width="55px"/><br/>
        	<span><img src="${resource(dir:'images',file:'phpontowebtexto_login.png')}" /></span></span>
        	<hr />
        </div>
        <div class="logo_login">         
          <div class="login-foRrm">
            <form action='${postUrl}?spring-security-redirect=/redirectLogin/index' method='POST' id='loginForm' autocomplete='off'>
              <fieldset>
                <div class="clearfix">
                  <input type='text' name='j_username' id='username' required='' placeholder='Usuário' />
                </div>
                <div class="clearfix">
                  <input type='password' name='j_password' id='password' required='' placeholder='Senha' /> <br/>
                </div>
                <div class="button_login">
	                <a class="home" href="${createLink(uri: '/reiniciarSenha/reiniciarSenha')}">Redefinir senha</a>
	                <input type='submit' id="submit" class="btn  btn-info" value='Acessar'/>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div> <!-- /container -->
  </body>
</html>