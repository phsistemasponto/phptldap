<%@page import="cadastros.Periodo"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="fechamento/fechamentoBancoHoras.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Fechamento do Banco de Horas</h3>
    </div>
    
    <form action="${request.contextPath}/fechamentoBancoHoras/list" method="POST" id="formPesquisa">
    	<div class="well">
    	
    		<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Ano:</label>
				<div class="controls">
					<g:select id="ano" name="ano" value="${params.ano}" onchange="mudaAno()" style="width:145px;" noSelection="['':'..']"
						from="${Periodo.executeQuery('select distinct year(p.dtIniPr) as ano from Periodo p order by 1 asc')}" />
				</div>
			</div>
	    
	    	<div class="control-group">
				<label class="control-label" style="font-weight: bold;">PerÃ­odo:</label>
				<div class="controls">
					<select id="periodo" name="periodo" onchange="mudaPeriodo()" style="width: 145px;">
						<option value="">..</option>
					</select>
					<input type="text" disabled="disabled" id="dataInicio" style="width:70px;">
					<input type="text" disabled="disabled" id="dataFim" style="width:70px;">
				</div>
			</div>
    	
    		<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Tipo de fechamento:</label>
				<div class="controls">
					<input type="radio" name="tipoFechamento" id="tipoFechamento" value="filial" onchange="controlaFiltroPadrao()"> &nbsp; Filial
					<input type="radio" name="tipoFechamento" id="tipoFechamento" value="funcionario" style="margin-left: 10px;" onchange="controlaFiltroPadrao()"> &nbsp; FuncionÃ¡rio
				</div>
			</div>
			
			<div id="divFiltroPadrao" style="display: none;">
	 			<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
						
					</div>
				</div>
			</div>
			
			<br>
			
			<g:submitButton name="btnFiltro" class="btn btn-medium btn-primary" value="Listar" />
			
			<input type="hidden" id="order" name="order"/>
			<input type="hidden" id="sortType" name="sortType"/>
					
		</div>
	</form>
           
    <hr/>
    
    <!-- FORM PARA FUNCIONARIOS -->
	<g:if test="${params.tipoFechamento == 'filial'}">
		<form action="${request.contextPath}/fechamentoBancoHoras/concluirFilial" method="POST" id="formFiliais">
		
			<input type="hidden" name="periodo" id="periodo" value="${params.periodo}"/>
			<input type="hidden" name="tipoFechamento" id="tipoFechamento" value="${params.tipoFechamento}"/>
		
			<g:if test="${filialInstanceTotal > 0}">
		    	<button type="submit" class="btn btn-medium btn-primary">Processar</button>
		    	<button type="button" class="btn btn-medium btn-primary" onclick="desfazerFechamentoFilial();">Desfazer fechamento</button>
		    </g:if>
			
			<div style="float: right;">
				<button class="btn" type="button" onClick="marcarTodosFiliais();">Marcar Todos</button> 
				<button class="btn" type="button" onClick="desmarcarTodosFiliais();">Desmarcar Todos</button>
			</div>
			
			<br><br>
			
		    <table class="table table-striped table-bordered table-condensed header-fixed">
		      <thead>
		        <tr>
		          <th style="width: 20px;">
		          </th>
		          <th style="width: 800px;">
		          	<span>Nome</span>
		          </th>
		          <th style="width: 275px;">
		          	<span>Status</span>
		          </th>
		        </tr>
		      </thead>
		
		      <tbody>
		      <g:each in="${filialInstanceList}" status="i" var="filialInstance">
		        <tr>
		          <td style="width: 20px;">
		          	<input type="checkbox" name="filialSelecionada" id="filialSelecionada" value="${filialInstance?.id}">
		          </td>
		          <td style="width: 800px;">${filialInstance}</td>
		          <td style="width: 275px;" id="columnFilial-${filialInstance?.id}">
		          	<g:if test="${filialInstance?.statusFechamento(periodoSelecionado) == 'Aberto'}">
		          		<span class="label label-success">Aberto</span>
		          	</g:if>
		          	<g:else>
		          		<span class="label label-important">Fechado</span>
		          	</g:else>
		          </td>
		        </tr>
		      </g:each>
		      </tbody>
		    </table>
		    
		    <div class="paginate">
		      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${filialInstanceTotal}</span>
		    </div>
		    
	    </form>
	</g:if>

	<!-- FORM PARA FUNCIONARIOS -->
	<g:if test="${params.tipoFechamento == 'funcionario'}">
		<form action="${request.contextPath}/fechamentoBancoHoras/concluirFuncionario" method="POST" id="formFuncionarios">
		
			<input type="hidden" name="periodo" id="periodo" value="${params.periodo}"/>
			<input type="hidden" name="tipoFechamento" id="tipoFechamento" value="${params.tipoFechamento}"/>
		
			<g:if test="${funcionarioInstanceTotal > 0}">
		    	<button type="submit" class="btn btn-medium btn-primary">Processar</button>
		    	<button type="button" class="btn btn-medium btn-primary" onclick="desfazerFechamentoFuncionario();">Desfazer fechamento</button>
		    </g:if>
			
			<div style="float: right;">
				<button class="btn" type="button" onClick="marcarTodosFuncionarios();">Marcar Todos</button> 
				<button class="btn" type="button" onClick="desmarcarTodosFuncionarios();">Desmarcar Todos</button>
			</div>
			
			<br><br>
			
		    <table class="table table-striped table-bordered table-condensed header-fixed">
		      <thead>
		        <tr>
		          <th style="width: 20px;">
		          </th>
		          <th style="width: 100px;">
		          	<span>MatrÃ­cula</span>
		          	<g:if test="${order != 'mtrFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('mtrFunc', 'asc')"/></g:if>
		          	<g:if test="${order == 'mtrFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('mtrFunc', 'desc')"/></g:if>
		          	<g:if test="${order == 'mtrFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('mtrFunc', 'asc')"/></g:if>
		          </th>
		          <th style="width: 100px;">
		          	<span>CrachÃ¡</span>
		          	<g:if test="${order != 'crcFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('crcFunc', 'asc')"/></g:if>
		          	<g:if test="${order == 'crcFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('crcFunc', 'desc')"/></g:if>
		          	<g:if test="${order == 'crcFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('crcFunc', 'asc')"/></g:if>
		          </th>
		          <th style="width: 600px;">
		          	<span>Nome</span>
		          	<g:if test="${order != 'nomFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('nomFunc', 'asc')"/></g:if>
		          	<g:if test="${order == 'nomFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('nomFunc', 'desc')"/></g:if>
		          	<g:if test="${order == 'nomFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('nomFunc', 'asc')"/></g:if>
		          </th>
		          <th style="width: 275px;">
		          	<span>Status</span>
		          </th>
		        </tr>
		      </thead>
		
		      <tbody>
		      <g:each in="${funcionarioInstanceList}" status="i" var="funcionarioInstance">
		        <tr>
		          <td style="width: 20px;">
		          	<input type="checkbox" name="funcionarioSelecionado" id="funcionarioSelecionado" value="${funcionarioInstance?.id}">
		          </td>
		          <td style="width: 100px;">${funcionarioInstance?.mtrFunc}</td>
		          <td style="width: 100px;">${funcionarioInstance?.crcFunc}</td>
		          <td style="width: 600px;">${funcionarioInstance?.nomFunc}</td>
		          <td style="width: 275px;" id="columnFuncionario-${funcionarioInstance?.id}">
		          	<g:if test="${funcionarioInstance?.statusFechamento(periodoSelecionado) == 'Aberto'}">
		          		<span class="label label-success">Aberto</span>
		          	</g:if>
		          	<g:else>
		          		<span class="label label-important">Fechado</span>
		          	</g:else>
		          </td>
		        </tr>
		      </g:each>
		      </tbody>
		    </table>
		    
		    <div class="paginate">
		      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${funcionarioInstanceTotal}</span>
		    </div>
		    
	    </form>
	</g:if>
	
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
    <script type="text/javascript">
    	var tipoFechamento = '${params.tipoFechamento}';
    	var selector = null;
    	if (tipoFechamento != null && tipoFechamento != '') {
    		selector = 'input[name="tipoFechamento"][value="' + tipoFechamento + '"]';
        } else {
        	selector = 'input[name="tipoFechamento"][value="filial"]';
        }
    	
    	$j(selector).attr("checked","true");
    	controlaFiltroPadrao();
    </script>
    
    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>