<div id="divLovHorasExtras" style="display: none;" title="Horas Extras">


	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
	
	<br>
	
	<table id="tableHorasExtras" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Data</th>
				<th>Dia</th>
				<th>Ocorrência</th>
				<th>Quantidade hora</th>
				<th>Valor</th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${dadosHorasExtras}" status="i" var="he">
				<tr>
					<td>${he.data}</td>
					<td>${he.dia}</td>
					<td>${he.ocorrencia}</td>
					<td>${he.quantidadeHora}</td>
					<td>${he.valor}</td>
				</tr>
			</g:each>
		</tbody>
	</table>
	
	<br>
	
	<div style="float: right;">
		<b>Extras 1:</b> <input type="text" value="${dadosTotalizadoresHorasExtras.totalExtras1}" disabled="true" readonly="readonly" style="width: 100px;"> <br/>
		<b>Extras 2:</b> <input type="text" value="${dadosTotalizadoresHorasExtras.totalExtras2}" disabled="true" readonly="readonly" style="width: 100px;"> <br/>
	</div>
	
</div>