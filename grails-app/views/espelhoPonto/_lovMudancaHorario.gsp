<div id="divLovMudancaHorario" style="display: none;" title="Mudança de horário">

<g:form class="form-horizontal" action="mudancaHorario" method="POST">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
	
	<br>
	
	<div class="row" style="margin-bottom: 10px;">
		<div class="span6">
			<label style="font-weight: bold;">Horário:</label>
			<g:textField name="mudancaHorarioId" style="width:40px" required="true" 
				onchange="buscaId('cadastros.Horario', 'id', 'dscHorario', 'mudancaHorarioId', 'horarioDescricao', false);"/>
			<button style="cursor:pointer" title="Procurar Justificativa" id="justificativaBotaoBusca"
				onclick="showBuscaInterna('mudancaHorarioId', 'horarioDescricao', 'cadastros.Horario', 'id', 'dscHorario', false);"><i class="icon-search" ></i></button>&nbsp;
			<g:textField name="horarioDescricao" readonly="true" style="width: 70%;"/>
		</div>
	</div>
	<div class="row" style="margin-bottom: 10px;">
		<div class="span3">
			<label style="font-weight: bold;">Data início:</label>
			<g:textField name="dataInicioHorario" style="width: 70%;"/>
		</div>
		<div class="span3">
			<label style="font-weight: bold;">Índice:</label>
			<g:textField name="indiceHorario" class="span3" />
		</div>
	</div>
	
	<div class="row" style="margin-bottom: 10px;">
		<div class="span3">
			<g:submitButton class="btn btn-medium btn-info" name="OK" />
			<a id="viewIndice" class="btn btn-medium" onClick="visualizaIndice('#mudancaHorarioId', '#dataInicioHorario', '#indiceHorario');">Visualiza o índice</a>
		</div>
	</div>
	
	<g:each in="${params.keySet()}" status="i" var="p">
   		<g:if test="${p.startsWith('formPesquisa_')}">
   			<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
   		</g:if>
     </g:each>
	
</g:form>

</div>