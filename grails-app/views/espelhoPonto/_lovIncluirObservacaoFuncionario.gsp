<div id="divLovIncluirObservacaoFuncionario" style="display: none;" title="Incluir observação">

<g:form class="form-horizontal" action="incluirObservacaoFuncionario" method="POST">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${funcionario.id}"/>
	<input type="hidden" id="dataInicio" name="dataInicio" value="${params.dataInicio}"/>
	<input type="hidden" id="dataFim" name="dataFim" value="${params.dataFim}"/>
	
	<br>
	
	<div class="control-group">
		<label class="control-label" style="font-weight: bold;">Data:</label>
		<div class="controls">
			<g:textField name="dataIncluirObservacao" style="width:80px" readonly="readonly"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" style="font-weight: bold;">Observação:</label>
		<div class="controls">
			<g:textArea name="observacaoFuncionario" style="width:410px;height:200px;" required="true"/>
		</div>
	</div>
	
	<g:submitButton id="btnIncluirObservacao" class="btn btn-medium btn-primary" name="OK" />
	
</g:form>

</div>