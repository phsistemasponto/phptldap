<div id="divLovCorrigirBatidas" style="display: none;" title="Corrigir Batidas">

<g:form class="form-horizontal" action="corrigirBatidas" method="POST">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	
	
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
	
	<br>
	
	<table id="tableCorrigirBatidas" class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th>Data</th>
				<th>Dia</th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${dadosCorrigirBatidas}" status="i" var="c">
				<tr id="rowDia-${c.identificadorDia}">
					<td style="vertical-align: middle; width:30px; border-right: 1px solid #DDDDDD; text-align: center;">
						${c.data}
					</td>
					<td style="vertical-align: middle; width:30px; border-right: 1px solid #DDDDDD; text-align: center;">
						${c.dia}
					</td>
					<td style="vertical-align: middle; width:110px; border-right: 1px solid #DDDDDD; text-align: center;">
						<input type="button" class="btn btn-medium btn-primary btnAlterarStatusBatidas" value="Alterar status"/>
					</td>
					<td style="vertical-align: middle; width:50px; border-right: 1px solid #DDDDDD; text-align: center;">
						<input type="button" class="btn btn-medium btn-primary btnIncluirBatidas" value="Incluir"/>
					</td>
					<td>
						<table style="width:100%;margin-bottom: 0px;" class="table table-bordered table-condensed">
							<g:each in="${c.listaBatidas}" status="j" var="st">
								<tr id="rowStatus-${c.identificadorDia}">
									<td style="width:100px;">
										<input type="hidden" name="statusBatida-${st.identificador}" id="statusBatida-${st.identificador}" value="${st.status}">
										<span id="descStatusBatida-${st.identificador}">${st.descStatus}</span>
									</td>
									<td style="width:60px;">
										${st.horas}
									</td>
									<td style="width:70px;">
										<input type="checkbox" name="excluirBatida-${st.identificador}" id="excluirBatida-${st.identificador}" class="checkExcluirBatidas"> Excluir
									</td>
									<td>
										<div id="divMotivo-${st.identificador}" style="display: none;">
											<b>Motivo:</b> 
											<g:textField name="idMotivoExcluirBatida-${st.identificador}" style="width:40px" disabled="disabled" required="true" 
												onchange="buscaIdMotivoCorrigirBatidas('idMotivoExcluirBatida-${st.identificador}', 'descMotivoExcluirBatida-${st.identificador}');" />
											<i class="icon-search" style="cursor: pointer" id="filtroBusca" 
												onclick="showModalMotivoCorrigirBatidas('idMotivoExcluirBatida-${st.identificador}', 'descMotivoExcluirBatida-${st.identificador}');"></i>&nbsp;
											<g:textField name="descMotivoExcluirBatida-${st.identificador}" readonly="true" style="width:200px"/>
										</div>
									</td>
								</tr>
							</g:each>
						</table>
					</td>
				</tr>
				
			</g:each>
		</tbody>
	</table>
	
	<g:submitButton class="btn btn-medium btn-info" name="Aplicar" />
	
	<g:each in="${params.keySet()}" status="i" var="p">
   		<g:if test="${p.startsWith('formPesquisa_')}">
   			<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
   		</g:if>
     </g:each>
	
</g:form>
</div>

<div id="confirmBox" style="display: none;" title="Confirmação">
    <div class="message"><b>Aplicar em todos os dias?</b></div>
    <br>
    <input type="button" class="btn btn-medium btn-primary yes" value="Sim"/>
    <input type="button" class="btn btn-medium btn-primary no" value="Não, somente no atual"/>
</div>