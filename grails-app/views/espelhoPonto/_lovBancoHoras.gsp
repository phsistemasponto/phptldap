<div id="divLovBancoHoras" style="display: none;" title="Banco de Horas">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
	
	<br>
	
	<table id="tableBancoHoras" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Data</th>
				<th>Dia</th>
				<th>Ocorrência</th>
				<th>Quantidade hora</th>
				<th>Tipo lançamento</th>
				<th>Tipo ocorrência</th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${dadosBancoHoras}" status="i" var="bh">
				<tr>
					<td>${bh.data}</td>
					<td>${bh.dia}</td>
					<td>${bh.ocorrencia}</td>
					<td>${bh.quantidadeHora}</td>
					<td>${bh.lancamento}</td>
					<td>${bh.tipoOcorrencia}</td>
				</tr>
			</g:each>
		</tbody>
	</table>
	
</div>