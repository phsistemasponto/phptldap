<html>
  <head>
    <meta name="layout" content="main">
    <style>
    .titulo{
	    	background-color: #05577f !important;
	    	padding: 5px 20px !important;
	    	-webkit-border-radius: 5px 5px 0 0!important;
			-moz-border-radius: 5px 5px 0 0 !important;
			border-radius: 5px 5px 0 0 !important;
	}
	.titulo h3{
		color:#FFFFFF !important;
	}
    .text-center{
    	text-align: center !important;
    }
    </style>
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="espelhoPonto/espelhoPonto.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Correção de batidas</h3>
    </div>    
    <form action="${request.contextPath}/espelhoPonto/list" method="POST" id="formPesquisa" class="form-horizontal">
    	<div class="well">
	    	<div class="row-fluid" style="margin-bottom: 15px;">
				<div class="span3">
					<div class="controls-horizontal">
						<g:if test="${params.somenteComObservacao}">
						<input type="checkbox" name="somenteComObservacao" id="somenteComObservacao" checked="checked">&nbsp;
						</g:if>
						<g:else>
							<input type="checkbox" name="somenteComObservacao" id="somenteComObservacao">&nbsp;
						</g:else>
						
						<b>Só funcionários com observação</b>
					</div>
				</div>
				<div class="span6">
					<div class="controls-horizontal">
						
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						<input type="hidden" id="order" name="order"/>
						<input type="hidden" id="sortType" name="sortType"/>
						<g:textField name="filtroDesc" readonly="true" onclick="showFiltroPadrao();" value="${params.filtroDesc}" placeholder="Clique na lupa para mais filtros" class="search-query span3" />
						<button class="btn" type="button" onclick="showFiltroPadrao();"><i class="icon-search" id="filtroBusca"></i></button>

					</div>
				</div>
			</div>
			<div class="row-fluid">
    		<div class="span5">
				<div class="controls-horizontal">
					<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" placeholder="Data início:" required="required"/>
					&emsp;<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" placeholder="Data fim:" required="required"/>
				</div>
			</div>
			<div class="span2">
				<div class="controls-horizontal">
					<g:submitButton name="btnFiltro" class="btn btn-medium btn-info" value="Listar funcionários"/>
				</div>
			</div>
		</div>
		</div>
	</form>
           
    <hr/>
	<form action="${request.contextPath}/espelhoPonto/exibir" method="POST" id="formFuncionarios">
		
		<table class="table table-bordered table-condensed header-fluid">
	      <thead>
	        <tr>
	          <th class="span2">
	          	<span class="text-center">Matrícula</span>
	          	<g:if test="${order != 'mtrFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('mtrFunc', 'asc')"/></g:if>
	          	<g:if test="${order == 'mtrFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('mtrFunc', 'desc')"/></g:if>
	          	<g:if test="${order == 'mtrFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('mtrFunc', 'asc')"/></g:if>
	          </th>
	          <th class="span2">
	          	<span>Crachá</span>
	          	<g:if test="${order != 'crcFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('crcFunc', 'asc')"/></g:if>
	          	<g:if test="${order == 'crcFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('crcFunc', 'desc')"/></g:if>
	          	<g:if test="${order == 'crcFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('crcFunc', 'asc')"/></g:if>
	          </th>
	          <th class="span3">
	          	<span>Nome</span>
	          	<g:if test="${order != 'nomFunc'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('nomFunc', 'asc')"/></g:if>
	          	<g:if test="${order == 'nomFunc' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('nomFunc', 'desc')"/></g:if>
	          	<g:if test="${order == 'nomFunc' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('nomFunc', 'asc')"/></g:if>
	          </th>
	          <th class="span2">
	          	<span>Cargo</span>
	          	<g:if test="${order != 'cargo.dscCargo'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('cargo.dscCargo', 'asc')"/></g:if>
	          	<g:if test="${order == 'cargo.dscCargo' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('cargo.dscCargo', 'desc')"/></g:if>
	          	<g:if test="${order == 'cargo.dscCargo' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('cargo.dscCargo', 'asc')"/></g:if>
	          </th>
	          <th class="span3">
	          	<span>Filial</span>
	          	<g:if test="${order != 'filial.dscFantFil'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('filial.dscFantFil', 'asc')"/></g:if>
	          	<g:if test="${order == 'filial.dscFantFil' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('filial.dscFantFil', 'desc')"/></g:if>
	          	<g:if test="${order == 'filial.dscFantFil' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('filial.dscFantFil', 'asc')"/></g:if>
	          </th>
	        </tr>
	      </thead>
	
	      <tbody>
	      <g:each in="${funcionarioInstanceList}" status="i" var="funcionarioInstance">
	        <g:if test="${funcionarioInstance?.situacaoFunc.idstfunc == 'A'}">
	        	<tr id="linhaFuncionario-${funcionarioInstance.id}" class="linhaFuncionario">
			</g:if>
			<g:elseif test="${funcionarioInstance?.situacaoFunc.idstfunc == 'F'}">
				<tr data-toggle="tooltip" data-placement="top" title="AFASTADO" id="linhaFuncionario-${funcionarioInstance.id}" class="linhaFuncionario tip" style="background-color:#FCF8E3">
			</g:elseif>
			<g:elseif test="${funcionarioInstance?.situacaoFunc.idstfunc == 'D'}">
				<tr data-toggle="tooltip" data-placement="top" title="DEMITIDO" id="linhaFuncionario-${funcionarioInstance.id}" class="linhaFuncionario tip" style="background-color:#FBBDBD">
			</g:elseif>
	        
	        
	          <td style="width: 90px;">${funcionarioInstance?.mtrFunc}</td>
	          <td style="width: 90px;">${funcionarioInstance?.crcFunc}</td>
	          <td style="width: 370px;">${funcionarioInstance?.nomFunc}</td>
	          <td style="width: 230px;">${funcionarioInstance?.cargo?.dscCargo}</td>
	          <td style="width: 300px;">${funcionarioInstance?.filial?.dscFantFil}</td>
	          
	        </tr>
	      </g:each>
	      </tbody>
	    </table>
	    
	    <div class="paginate">
	      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${funcionarioInstanceTotal}</span>
	    </div>
	    
	    <br/>
	    
	    <input type="hidden" name="funcionarioId" id="funcionarioId">
	    <input type="hidden" name="dtInicio" id="dtInicio">
	    <input type="hidden" name="dtFim" id="dtFim">
	    
	    <input type="hidden" name="dataInicio" id="dataInicio" value="${params.dataInicio}">
	    <input type="hidden" name="dataFim" id="dataFim" value="${params.dataFim}">
	    
    </form>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
     <script>
     var $j = jQuery.noConflict();
     $j(document).ready(function() {
    	 $j(".tip").tooltip();
     });
     </script>
  </body>
</html>