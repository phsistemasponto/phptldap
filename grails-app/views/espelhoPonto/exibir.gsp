<html>
  <head>
    <meta name="layout" content="main">
    <link rel="stylesheet" href="${resource(dir:'css',file:'bootstrap.css')}" />
	<link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
	<link rel="stylesheet" href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
	<link rel="stylesheet" href="${resource(dir:'css',file:'jquery-ui-1.8.23.custom.css')}" />
	<style>
		body{
			background-position: 0 120px !important;
		}
	</style>
  
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
  	<g:javascript src="comum/lov.js"/>
    <g:javascript src="classificacao/classificacao.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="horario/horario.js"/>
    <g:javascript src="espelhoPonto/espelhoPonto.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Espelho do ponto</h3>
    </div>
    <div class="well">
    
    	<input type="hidden" name="periodoFechado" id="periodoFechado" value="${periodoFechado}">
    	
    	<table style="font-size:10px;">
    		<tr>
    			<td rowspan="6" style="padding-right: 30px;">
    				<div style="height: 150px; width: 150px; border: 1px solid black;" id="divFoto">
						<img src="${createLink(controller:'funcionario', action:'image', id: funcionario.id)}"/>
					</div>
    			</td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>Período:</h4></td>
    			<td style="font-size:14px;"><i>${params.dtInicio} - ${params.dtFim}</i></td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>Funcionario:</h4></td>
    			<td style="font-size:14px;"><i>${funcionario.crcFunc} - ${funcionario.nomFunc}</i></td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>PIS:</h4></td>
    			<td style="font-size:14px;"><i>${funcionario.pisFunc}</i></td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>Filial:</h4></td>
    			<td style="font-size:14px;"><i>${funcionario.filial.dscFantFil}</i></td>
    		</tr>
    		<tr>
    			<td style="text-align: right; padding-right: 10px;"><h4>Cargo:</h4></td>
    			<td style="font-size:14px;"><i>${funcionario.cargo.dscCargo}</i></td>
    		</tr>
    	</table>    	
    </div>
    <div class="container">
    	<div class="row" style="margin-bottom:10px;">
	    	<button class="btn btn-medium btn-info span3" type="button" onclick="showLovLancamentosDiarios();">Lanc diários</button>
			<button class="btn btn-medium btn-info span3" type="button" onclick="showLovMudancaHorario();">Mudança horário</button>
			<button class="btn btn-medium btn-info span3" type="button" onclick="redirectToCadastroPrincipal();">Cadastro principal</button>
			<button class="btn btn-medium btn-info span3" type="button" onclick="showLovAfastamentos();">Afastamentos</button>
		</div>
		<div class="row">
			<button class="btn btn-medium btn-info span3" type="button" onclick="showLovHorasExtras();">Horas extras</button>
			<button class="btn btn-medium btn-info span3" type="button" onclick="showLovBancoHoras();">Banco de horas</button>
			<button class="btn btn-medium btn-info span3" type="button" onclick="showLovCorrigirBatidas();">Corrigir as batidas</button>
			<button class="btn btn-medium btn-info span3" type="button" onclick="processar();">Processar</button>
    	</div>
    </div>
    <br/>
    		<table id="tabelaPrincipal" class="table table-bordered table-condensed header-fluid">
				    	<thead>
				    		<tr>
					    		<th>Data</th>
					    		<th>Dia</th>
					    		<th>Horario</th>
					    		<th>Entra1</th>
					    		<th>Saida1</th>
					    		<th>Entra2</th>
					    		<th>Saida2</th>
					    		<th>Entra3</th>
					    		<th>Saida3</th>
					    		<th>Entra4</th>
					    		<th>Saida4</th>
					    		<th>Observações</th>
					    		<th style="text-align:center;" class="span1">Visualizar</th>
					    		<th style="text-align:center;" class="span1">Abono</th>
					    		<th style="text-align:center;" class="span1"><i class="icon-comment"></i></th>
					    	</tr>
				    	</thead>
				    	<tbody>
				    		<g:each in="${dadosPrincipais}" status="i" var="dado">
					        	<tr id="linhaDado-${dado.identificador}" class="linhaDado">
						          <td>${dado?.dataBatida}</td>
						          <td>${dado?.diaBatida}</td>
						          <td>${dado?.horario}</td>
						          <td>${dado?.entrada1}</td>
						          <td>${dado?.saida1}</td>
						          <td>${dado?.entrada2}</td>
						          <td>${dado?.saida2}</td>
						          <td>${dado?.entrada3}</td>
						          <td>${dado?.saida3}</td>
						          <td>${dado?.entrada4}</td>
						          <td>${dado?.saida4}</td>
						          <td>${dado?.observacao}</td>
						          <td style="text-align:center;">
							          <div class="btn-group" style="text-align:center;">
							          	<button class="btn dropdown-toggle" data-toggle="dropdown" href="#" style="height: 25px;margin-left: 10px;">
							          		<i class="icon-eye-open"></i>
							          	</button>
							          	<ul class="dropdown-menu" style="position: absolute;left:-175px;">
							          		<li>
												<div style="margin: 5px; border-radius:5px;min-height: 110px;">
													<table id="tabelaOcorrencias" class="table table-bordered table-condensed">
												    	<thead>
												    		<tr style="color: #000;">
													    		<th style="padding: 5px; border: 1px solid #ccc;">Ocorrências</th>
													    		<th style="padding: 5px; border: 1px solid #ccc;">Horas</th>
													    	</tr>
												    	</thead>
												    	<tbody>
												    	</tbody>
												    </table>
												    
												    <table id="tabelaBatidas" class="table table-bordered table-condensed">
												    	<thead>
												    		<tr style="color: #000;">
													    		<th style="padding: 4px; border: 1px solid #ccc;" colspan="2">Batida realizada pelo funcionário</th>
													    	</tr>
												    		<tr style="color: #000;">
													    		<th style="padding: 5px; border: 1px solid #ccc;">Status</th>
													    		<th style="padding: 5px; border: 1px solid #ccc;">Batida</th>
													    		<th style="padding: 5px; border: 1px solid #ccc;"></th>
													    	</tr>
												    	</thead>
												    	<tbody>
												    	</tbody>
								    				</table>
							    				</div>							          		
							          		</li>
							          	</ul>
							          </div>
						          </td>
						          <td style="text-align:center;">
						          	<g:if test="${dado.possuiAbono}">
						          		<button id="imgAbono" src="${resource(dir:'images',file:'abono1_vermelho.png')}" onclick="showLovAbono('');" alt="Desabonar" title="Desabonar">
							          			<i class="icon-ok"></i>
							          	</button>
						          	</g:if>
						          	<g:else>
						          		<button id="imgAbono" src="${resource(dir:'images',file:'abono1_vermelho.png')}" onclick="showLovAbono('${dado?.dataBatida}');" alt="Abonar" title="Abonar">
							          			<i id="imgAbono" class="icon-edit"></i>
							          	</button>
						          	</g:else>
						          </td>
						          <td style="text-align:center;">
						          	<g:if test="${dado.observacaoFuncionario.length() > 0}">
						          		<g:if test="${dado.statusObservacaoFuncionario == 'A'}">
							          		<img id="imgObsFuncionario" src="${resource(dir:'images',file:'msg_unread1.jpg')}" class="ph btn" 
							          			onclick="showLovVisualizarObservacao(this, '${dado?.dataBatida}', '${dado.observacaoFuncionario}')" title="Observação"/>
						          		</g:if>
						          		<g:elseif test="${dado.statusObservacaoFuncionario == 'F'}">
							          		<img id="imgObsFuncionario" src="${resource(dir:'images',file:'msg_read1.png')}" class="ph btn" style="width:13px;"
							          			onclick="showLovVisualizarObservacao(this, '${dado?.dataBatida}', '${dado.observacaoFuncionario}')" title="Observação"/>
						          		</g:elseif>
						          	</g:if>
						          </td>
					        	</tr>
					      	</g:each>
				    	</tbody>
				    </table>
    
    <br/><br/><br/>
    
    <button class="btn btn-large" onclick="$j('#formPesquisa').submit();">Retornar</button>
    
    <form action="${request.contextPath}/espelhoPonto/list" method="POST" id="formPesquisa">
    	<g:each in="${params.keySet()}" status="i" var="p">
    		<g:if test="${p.startsWith('formPesquisa_')}">
    			<input type="hidden" name="${p.replace('formPesquisa_', '')}" id="${p.replace('formPesquisa_', '')}" value="${params[p]}"/>
    		</g:if>
      	</g:each>
      	<input type="hidden" id="retornar" name="retornar" value="true"/>
    </form>
    
    <form action="${request.contextPath}/espelhoPonto/exibir" method="POST" id="formResubmit">
    	<g:each in="${params.keySet()}" status="i" var="p">
    		<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
      	</g:each>
      	<input type="hidden" id="retornar" name="retornar" value="true"/>
    </form>
    
    <form action="${request.contextPath}/espelhoPonto/chamarProcedure" method="POST" id="formProcessar">
    	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
    	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
    	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${funcionario.id}"/>
    </form>
    
    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
    <g:render template="/espelhoPonto/lovAbono"/>
    <g:render template="/espelhoPonto/lovCorrigirBatidas"/>
    <g:render template="/espelhoPonto/lovLancamentosDiarios"/>
    <g:render template="/espelhoPonto/lovMudancaHorario"/>
    <g:render template="/espelhoPonto/lovCadastroPrincipal"/>
    <g:render template="/espelhoPonto/lovAfastamentos"/>
    <g:render template="/espelhoPonto/lovHorasExtras"/>
    <g:render template="/espelhoPonto/lovBancoHoras"/>
    <g:render template="/espelhoPonto/lovIncluirObservacaoFuncionario"/>
    <g:render template="/horario/lovIndiceHorario"/>
    
    <g:render template="/busca/lovBuscaInterna"/>
    
  </body>
</html>