<div id="divLovAbono" style="display: none;" title="Lançamentos de abonos">

<g:form class="form-horizontal" action="abonar" method="POST">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
	
	<br>
	
	
	<div id="divPanelAbono" style="border: 1px solid #DDDDDD; width: 1050px; height: 100%; padding: 10px;">
	
		<div class="row-fluid">
		    <div class="span4 bgcolor">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Data início:</label>
					<div class="controls">
						<g:textField name="dataInicioAbono" style="width:80px" onchange="buscaMovimentoDia();"/>
					</div>
				</div>
			</div>
			
			<div class="span3 bgcolor">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Carga horária:</label>
					<div class="controls">
						<g:textField name="cargaHorariaInicioAbono" style="width:120px"/>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Ocorrência:</label>
			<div class="controls">
				<g:textField name="ocorrenciaInicioAbono" readonly="true" style="width:300px"/>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Quantidade dias:</label>
			<div class="controls">
				<g:textField name="quantidadeDiasAbono" style="width:40px" value="1"/>
			</div>
		</div>
		
		<div class="row-fluid">
		    
		    <div class="span6 bgcolor">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Justificativa:</label>
					<div class="controls">
						<g:textField name="justificativaAbonoId" style="width:40px" required="true" 
							onchange="buscaIdJustificativaEspelhoPonto('#justificativaAbonoId', '#justificativaAbonoDescricao', 'AB');"/>
						<i class="icon-search" style="cursor:pointer" title="Procurar Justificativa" id="justificativaBotaoBusca"
							onclick="buscarLovJustificativaEspelhoPonto('AB');"></i>&nbsp;
						<g:textField name="justificativaAbonoDescricao" readonly="true" style="width:250px"/>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="control-group" id="divCidAbono" style="display:none;">
			<label class="control-label" style="font-weight: bold;">CID:</label>
			<div class="controls">
				<g:textField name="cidId" style="width:40px" required="true" onchange="buscaIdCidAbono();" disabled="disabled"/>
				<i class="icon-search" style="cursor:pointer" title="Procurar Cid" id="cidBotaoBusca" onclick="showModalCidAbono();"></i>&nbsp;
				<g:textField name="cidDescricao" readonly="true" style="width:250px"/>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Observação:</label>
			<div class="controls">
				<g:textArea name="observacaoAbono" style="width: 480px; height: 60px; resize: none;"/>
			</div>
		</div>
		
		<g:submitButton class="btn btn-medium btn-primary" name="Abonar" />
		
	</div>
	
		<g:each in="${params.keySet()}" status="i" var="p">
   		<g:if test="${p.startsWith('formPesquisa_')}">
   			<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
   		</g:if>
     </g:each>
	
</g:form>
	
	
	<table id="tableAbonos" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Data início</th>
				<th>Data fim</th>
				<th>Qtdade dias</th>
				<th>Ocorrência</th>
				<th>Justificativa</th>
				<th>Observação</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${dadosAbono}" status="i" var="ab">
				<tr>
					<td>${ab.dtInicio}</td>
					<td>${ab.dtFim}</td>
					<td>${ab.qtdadeDias}</td>
					<td>${ab.ocorrencia}</td>
					<td>${ab.justificativa}</td>
					<td>${ab.observacao}</td>
					<td>
						<g:form class="form-horizontal" action="desabonar" method="POST">
							<input type="hidden" id="idDesabono" name="idDesabono" value="${ab.id}"/>
							<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
							<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
							<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
							<g:submitButton class="btn btn-medium btn-primary" name="Desabonar" />
						</g:form>
					</td>			
				</tr>
			</g:each>
		</tbody>
	</table>

</div>