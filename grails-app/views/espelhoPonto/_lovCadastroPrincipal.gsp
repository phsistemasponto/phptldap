<div id="divLovCorrigirBatidas" style="display: none;" title="Corrigir Batidas">

<g:form class="form-horizontal formCadastroPrincipal" action="cadastroPrincipal" method="POST">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	
	<input type="hidden" id="id" name="id" value="${params.funcionarioId}"/>
	<input type="hidden" id="returnFluxo" name="returnFluxo" value="/espelhoPonto"/>
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
	
	<g:each in="${params.keySet()}" status="i" var="p">
   		<g:if test="${p.startsWith('formPesquisa_')}">
   			<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
   		</g:if>
     </g:each>
	
</g:form>
</div>