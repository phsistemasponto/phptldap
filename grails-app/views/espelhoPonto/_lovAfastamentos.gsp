<div id="divLovAfastamentos" style="display: none;" title="Afastamentos">

<g:form class="form-horizontal" action="afastamentos" method="POST">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
	
	<br>
	<div class="row" style="margin-bottom: 10px;">
		<div class="span6">
			<label style="font-weight: bold;">Justificativa:</label>
			<g:textField name="justificativaAfastamentoId" style="width:40px" required="true" 
				onchange="buscaIdJustificativaEspelhoPonto('#justificativaAfastamentoId', '#justificativaAfastamentoDescricao', 'AF');"/>
			<button style="cursor:pointer" title="Procurar Justificativa" id="justificativaBotaoBusca"
				onclick="buscarLovJustificativaEspelhoPonto('AF');"><i class="icon-search" ></i></button>&nbsp;
			<g:textField name="justificativaAfastamentoDescricao" readonly="true" style="width: 70%;"/>
		</div>
	</div>
		
	<div class="row">
		<div class="span3">
			<label style="font-weight: bold;">Data início:</label>
			<g:textField name="dataInicioAfastamento" style="width: 70%;" />
		</div>
		<div class="span3">
			<label style="font-weight: bold;">Data fim:</label>
			<g:textField name="dataFimAfastamento" style="width: 70%;" />
		</div>
	</div>
	
	<br><br>
	
	<table id="tableAfastamentos" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Funcionario</th>
				<th>Justificativa</th>
				<th>Data início</th>
				<th>Data fim</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${dadosAfastamentos}" status="i" var="af">
				<tr>
					<td>${af.funcionario}</td>
					<td>${af.justificativa}</td>
					<td>${af.dataInicio}</td>
					<td>${af.dataFim}</td>
					<td style="width: 50px; text-align: center">
			            <i class="icon-remove" onclick="excluirAfastamento('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/espelhoPonto/deleteAfastamentos', ${af.id})" 
			            	style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
			        </td>
				</tr>
			</g:each>
		</tbody>
	</table>
	
	<g:submitButton class="btn btn-medium btn-info" name="OK" />
	
	<g:each in="${params.keySet()}" status="i" var="p">
   		<g:if test="${p.startsWith('formPesquisa_')}">
   			<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
   		</g:if>
     </g:each>
	
</g:form>

</div>