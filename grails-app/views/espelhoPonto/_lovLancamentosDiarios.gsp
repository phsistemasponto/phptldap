<div id="divLovLancamentosDiarios" style="display: none;" title="Lançamentos diários">

<g:form class="form-horizontal" action="lancamentosDiarios" method="POST">

	<script type="text/javascript">
	$j(document).keypress(
		function(e){
			if (e.keyCode==10 || e.keyCode == 13) {
				e.preventDefault();
			}
		}
	)
	</script>
	
	<input type="hidden" id="funcionarioId" name="funcionarioId" value="${params.funcionarioId}"/>
	<input type="hidden" id="dtInicio" name="dtInicio" value="${params.dtInicio}"/>
	<input type="hidden" id="dtFim" name="dtFim" value="${params.dtFim}"/>
	
	<br>
	
	<table>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Período de:</label>
					<div class="controls">
						<g:textField name="lancamentoInicio" value="${params.dtInicio}" style="width:80px;" readonly="true"/> à
						<g:textField name="lancamentoFim" value="${params.dtFim}" style="width:80px;" readonly="true" />
					</div>
				</div>
			</td>
			<td style="padding-left: 30px;" rowspan="3">
				<div style="border: 1px solid #DDDDDD; width: 200px; padding: 10px;">
					<table>
						<g:each in="${cadastros.Classificacao.findAll(sort:"id")}" status="i" var="c">
							<tr>
								<td>
									<canvas id="canvasClassificacao-${c.id}" width="20" height="20"></canvas>
								</td>
								<td valign="top" style="padding-left: 15px;">
									${c.dscClass} 
									<input type="hidden" id="corClassificacao-${c.id}" value="${c.corExibicao}"/>
								</td>
							</tr>
						</g:each>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Classificação:</label>
					<div class="controls">
						<g:textField name="classificacaoId" style="width:40px" onchange="buscaIdClassificacaoLancamentosDiarios();"/>
						<i class="icon-search" style="cursor:pointer" title="Procurar Classificação" id="classificacaoLancamentoBotaoBusca"
							onclick="showModalClassificacaoLancamentosDiarios();"></i>&nbsp;
						<g:textField name="classificacaoDescricao" readonly="true" style="width:250px"/>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="control-group" id="divItemHorario" style="display: none;">
					<label class="control-label" style="font-weight: bold;">Item horário:</label>
					<div class="controls">
						<g:textField name="itemHorarioId" style="width:40px"
							onchange="buscaIdPerfilhorarioLancamentosDiarios();"/>
						<i class="icon-search" style="cursor:pointer" title="Procurar Item Horário" id="itemHorarioBotaoBusca"
							onclick="showModalItemHorarioLancamentosDiarios();"></i>&nbsp;
						<g:textField name="itemHorarioDescricao" readonly="true" style="width:250px" disabled="true"/>
					</div>
				</div>
			</td>
		</tr>
	</table>
	
	<br>
	
	<g:submitButton class="btn btn-medium btn-primary" name="Aplicar" />
	
	<br><br>
	
	<table id="tableLancamentosDiarios" class="table table-striped table-bordered table-condensed" style="margin-right: 10px;">
		<tr>
			<td>Dom</td>
			<td>Seg</td>
			<td>Ter</td>
			<td>Qua</td>
			<td>Qui</td>
			<td>Sex</td>
			<td>Sab</td>
		</tr>
		<g:if test="${dadosLancamentosDiarios[0].dia.equals('Seg')}">
			<tr><td>&nbsp;</td>
		</g:if>
		<g:elseif test="${dadosLancamentosDiarios[0].dia.equals('Ter')}">
			<tr><td>&nbsp;</td><td>&nbsp;</td>
		</g:elseif>
		<g:elseif test="${dadosLancamentosDiarios[0].dia.equals('Qua')}">
			<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</g:elseif>
		<g:elseif test="${dadosLancamentosDiarios[0].dia.equals('Qui')}">
			<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</g:elseif>
		<g:elseif test="${dadosLancamentosDiarios[0].dia.equals('Sex')}">
			<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</g:elseif>
		<g:elseif test="${dadosLancamentosDiarios[0].dia.equals('Sáb')}">
			<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</g:elseif>
		<g:each in="${dadosLancamentosDiarios}" status="i" var="dld">
			<g:if test="${dld.dia.equals('Dom')}">
				<tr>
			</g:if>
			<td class="celulaLancamentoDiario" style="text-align: center; line-height: 25px; vertical-align: middle;">
				${dld.data}
				<canvas id="canvasCelulaLancamento-${dld.identificadorDia}" width="20" height="20" style="margin-left: 5px; margin-top: 5px;"></canvas>
				<input type="hidden" id="classificacaoCelulaLancamento-${dld.identificadorDia}" 
					name="classificacaoCelulaLancamento-${dld.identificadorDia}" value="${dld.classificacao}">
				<input type="hidden" id="itemHorarioCelulaLancamento-${dld.identificadorDia}" 
					name="itemHorarioCelulaLancamento-${dld.identificadorDia}" value="${dld.itemHorario}">
			</td>
			<g:if test="${dld.dia.equals('Sáb')}">
				</tr>
			</g:if>
		</g:each>
	</table>
	
	<g:each in="${params.keySet()}" status="i" var="p">
   		<g:if test="${p.startsWith('formPesquisa_')}">
   			<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
   		</g:if>
     </g:each>
	
</g:form>

</div>