
<%@ page import="cadastros.Cargo" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cargo.label', default: 'Cargo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-cargo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-cargo" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list cargo">
			
				<g:if test="${cargoInstance?.dscCargo}">
				<li class="fieldcontain">
					<span id="dscCargo-label" class="property-label"><g:message code="cargo.dscCargo.label" default="Descrição" /></span>
					
						<span class="property-value" aria-labelledby="dscCargo-label"><g:fieldValue bean="${cargoInstance}" field="dscCargo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargoInstance?.idfolgaincrementada}">
				<li class="fieldcontain">
					<span id="idfolgaincrementada-label" class="property-label"><g:message code="cargo.idfolgaincrementada.label" default="Trabalhando (Domingo/Fériado) tem direito mais uma folga" /></span>
					
						<span class="property-value" aria-labelledby="idfolgaincrementada-label"><g:fieldValue bean="${cargoInstance}" field="idfolgaincrementada"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargoInstance?.idpghrext}">
				<li class="fieldcontain">
					<span id="idpghrext-label" class="property-label"><g:message code="cargo.idpghrext.label" default="paga hora extra" /></span>
					
						<span class="property-value" aria-labelledby="idpghrext-label"><g:fieldValue bean="${cargoInstance}" field="idpghrext"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${cargoInstance?.id}" />
					<g:link class="edit" action="edit" id="${cargoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
