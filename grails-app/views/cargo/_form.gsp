<g:javascript src="cargo/cargo.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${cargoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${cargoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${cargoInstance.id}">
		<form action="${request.contextPath}/cargo/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${cargoInstance?.id}" />
			<g:hiddenField name="version" value="${cargoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/cargo/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscCargo" maxlength="80" required="" value="${cargoInstance?.dscCargo}"/>
				</div>
			</div>
			
			
			<div class="control-group">
				<label class="control-label">CBO:</label>
				<div class="controls">
					<g:textField name="cbo" maxlength="12" required="" value="${cargoInstance?.cbo}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Trabalhando (Domingo/Feriado)tem direito mais uma folga :</label>
				<div class="controls">
				    <g:checkBox name="idfolgaincrementada" value="${cargoInstance?.idfolgaincrementada}" checked="${cargoInstance?.idfolgaincrementada == 'S'}" /></td>					
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Paga hora extras:</label>
				<div class="controls">
					<g:checkBox name="idpghrext" value="${cargoInstance?.idpghrext}" checked="${cargoInstance?.idpghrext == 'S'}" /></td>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>