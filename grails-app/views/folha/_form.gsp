	<g:javascript src="folha/folha.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${folhaInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${folhaInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${folhaInstance.id}">
		<form action="${request.contextPath}/folha/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${folhaInstance?.id}" />
			<g:hiddenField name="version" value="${folhaInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/folha/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição:</label>
				<div class="controls">
					<g:textField name="dscFol" maxlength="60" value="${folhaInstance?.dscFol}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Nome do Arquivo:</label>
				<div class="controls">
					<g:textField name="nomArq" maxlength="60" required="" value="${folhaInstance?.nomArq}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Email:</label>
				<div class="controls">
					<g:field type="email" name="email" value="${folhaInstance?.email}"/>
				</div>
			</div>	
			
			<hr />
			
			<h4>Configuração dos eventos da folha</h4>	
			<br/>
				
			<div class="control-group">
				<label class="control-label">Ocorrencias:</label>
				<div class="controls">
					<g:select id="ocorrencias" name="ocorrencias"
						from="${cadastros.Ocorrencias.list()}" optionKey="id"
						class="many-to-one" style="width:300px;" noSelection="['':'---']" />
				</div>
			</div>
			
			
			<div class="row-fluid">	
		       <div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Exportar em Dias:</label>
						<div class="controls">							
							<g:checkBox name="idExportaDias" value=""/>
						</div>
					</div>             
				</div>
				
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Exportar em Horas?</label>
						<div class="controls">							
							<g:checkBox name="idExportaHoras" value=""/>
						</div>
					</div>
				</div>
			</div>		
			
			<div class="row-fluid">	
		       <div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Exportar em Hexadecimal:</label>
						<div class="controls">							
							<g:checkBox name="idExportaHexa" value=""/>
						</div>
					</div>             
				</div>		
				
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Soma o Evento?</label>
						<div class="controls">
							<g:checkBox name="idSomaEvento" value=""/>
						</div>
					</div>             
				</div>
			</div>
			
			<div class="row-fluid">	
		       <div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Evento da Folha:</label>
						<div class="controls">		
						  <g:textField name="evtFolha" maxlength="60" style="width:80px" />				
						</div>
					</div>             
				</div>		
				
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Tipo de Evento</label>
						<div class="controls">
						   <select id="idTpEvtFolha" name="idTpEvtFolha">
					          <option value="1">Fechamento Normal</option>
						      <option value="2">Fechamento Demissional</option>					
					       </select>
						</div>
					</div>             
				</div>
				
				<div class="span3 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street"></label>
						<div class="controls">
			                <a id="incEventoFolha" class="btn btn-medium" onClick="incluirEventoFolha();">Incluir</a>
						</div>
					</div>             
				</div>
			</div> 
			
			<div id="configuracaoRel">
				<table class="table table-striped table-bordered table-condensed" id="folhaTable">
					<thead>
						<tr>
							<th style="width: 15%;">Ocorrências</th>
							<th style="width: 10%;">Exportar Dias</th>
							<th style="width: 10%;">Exportar Horas</th>							
							<th style="width: 10%;">Exportar Hexadecimal</th>
							<th style="width: 10%;">Somatório</th>
							<th style="width: 10%;">Evento da folha</th>
							<th style="width: 10%;">Tipo de Evento</th>
							<th style="width: 5%;"></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="hidden" id="sequencialTemp" name="sequencialTemp" value="0"/></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						
						<g:each in="${eventosFolha}" status="i" var="evento">
							<tr id="rowTableFolha${i+1}" class="even">
							
								<td> 
									<input type="text" id="nomeOcorrencia" name="nomeOcorrencia" value="${evento.ocorrencias}" readonly style="width:80%" />
									<input type="hidden" id="idOcorrencia" name="idOcorrencia" value="${evento.ocorrencias.id}"/>
									<input type="hidden" id="sequencialTemp" name="sequencialTemp" value="${i+1}"/>
								</td>
								<td>
									<input type="text" id="exportaDia" name="exportaDia" value="${evento.expDiasFormatada}" readOnly style="width:80%"/> 
								</td>
								<td>
									<input type="text" id="exportaHora" name="exportaHora" value="${evento.expHorasFormatada}" readOnly style="width:80%"/> 
								</td>
								<td> 
									<input type="text" id="exportaHexa" name="exportaHexa" value="${evento.expHorasHexFormatada}" readOnly style="width:80%"/> 
								</td>
								<td> 
									<input type="text" id="somaEvento" name="somaEvento" value="${evento.sumEventoFormatada}" readOnly style="width:80%"/> 
								</td>
								<td> 
									<input type="text" id="eventoFolha" name="eventoFolha" value="${evento.dscEvento}" readOnly style="width:80%"/> 
								</td>
								<td> 
									<input type="text" id="nomeTipoEvento" name="nomeTipoEvento" value="${evento.tipoEvento}" readonly style="width:80%" />
									<input type="hidden" id="idTipoEvento" name="idTipoEvento" value="${evento.tipoEvento}"/>
								</td>
								<td> 
									<i class="icon-remove" onclick="excluirEventoFolha(${i+1})" style="cursor:pointer" title="Excluir registro"></i> 
								</td>
							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
			
			<hr>
			
			<h4>Filiais</h4>
			<br />
			<div>
				<g:if test="${folhaInstance.id}">
					<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
						<thead>
							<tr>
								<th style="width: 50%;">Filial</th>
								<th style="width: 10%;">Existe </th>
								<th style="width: 40%;">
									<button class="btn" type="button" onClick="marcarTodos();">Marcar Todos</button>
									<button class="btn" type="button" onClick="desmarcarTodos();">Desmarcar Todos</button>
								</th>
							</tr>
						</thead>
	
						<tbody>
							<g:each in="${filiais?}" status="i" var="item">
								<tr id="rowTableFilial${i}" class="even">
									<td>
										<g:hiddenField name="filialId" value="${item?.filial?.id}" />
										<g:hiddenField name="temFilialPeriodo" value="${item?.temFilial}" />
										<g:if test="${item?.temFilial=='true' }">
											<g:hiddenField name="statusPeriodo" value="${item?.statusPeriodo}" />
										</g:if>
										<g:else>
											<g:hiddenField name="statusPeriodo" value="X" />
										</g:else>
										<g:textField name="descricaoFilial" style="width:90%" value='${item?.filial?.dscFil+" - "+item?.filial?.unicodFilial}' readOnly="true" />
									</td>
									
									<td>
										<g:if test="${item?.temFilial=='true' }">
											<input type='checkbox' name="temFilial${i}" value='true' checked="checked" onChange="atualizaFilialBotao(${i});"/>
										</g:if>
										<g:else>
											<input type='checkbox' name="temFilial${i}" value='false' onChange="atualizaFilialBotao(${i});"/>
										</g:else>
									</td>
									<td>
									</td>
								</tr>
							</g:each>
						</tbody>
					</table>
				</g:if>
				<g:else>
					<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
						<thead>
							<tr>
								<th style="width: 50%;">Filial</th>
								<th style="width: 10%;">Existe </th>
								<th style="width: 40%;">
									<button class="btn" type="button" onClick="marcarTodos();">Marcar Todos</button>
									<button class="btn" type="button" onClick="desmarcarTodos();">Desmarcar Todos</button>
								</th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${filiais?}" status="i" var="item">
								<tr id="rowTableFilial${i}" class="even">
									<td>
										<g:hiddenField name="filialId" value="${item?.filial?.id}" />
										<g:hiddenField name="temFilialPeriodo" value="${item?.temFilial}" />
										<g:textField name="descricaoFilial" style="width:90%" value='${item?.filial?.dscFil+" - "+item?.filial?.unicodFilial}' readOnly="true" />
									</td>
									<td>
										<g:if test="${item?.temFilial=='true' }">
											<input type='checkbox' name="temFilial${i}" value='true' checked="checked" onChange="atualizaFilialBotao(${i});"/>
										</g:if>
										<g:else>
											<input type='checkbox' name="temFilial${i}" value='false' onChange="atualizaFilialBotao(${i});"/>
										</g:else>
									</td>
									<td> 
									</td>
								</tr>
							</g:each>
						</tbody>
					</table>
				</g:else>
			</div>
			<hr>

		</fieldset>
		<hr>
		
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" 
			onclick="if (validateFolha()) return true; else return false;"/>
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
		
	</form>