
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Configuração da Folha Pagamento:</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Folha']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		
          <th>Descrição</th>
		
          		
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${folhaInstanceList}" status="i" var="folhaInstance">
        <tr>
		
          <td>${fieldValue(bean: folhaInstance, field: "dscFol")}</td>		
          		
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${folhaInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/folha/delete', ${folhaInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${folhaInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${folhaInstanceTotal}</span>
    </div>
  </body>
</html>