
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h2>Horário Simulado</h2>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Horário Simulado']" /></g:link>
    </div>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
		  <g:if test="${administrador}">
		  	<th>Usuário</th>
		  </g:if>
          <th>Data simulado</th>
          <th>Carga Horaria</th>
		  <th>Observação</th>
		  <th>Flag</th>
		  <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${horarioSimuladoInstanceList}" status="i" var="horarioSimuladoInstance">
        <tr>
          <g:if test="${administrador}">
		  	<td>${horarioSimuladoInstance.usuarioSimulado.nome}</td>
		  </g:if>
		  <td>
		  	<g:formatDate date="${horarioSimuladoInstance.dtSimulado}" format="dd/MM/yyyy"/>
		  </td>
		  <td>
		  	${horarioSimuladoInstance.cargaTotalStr}
		  </td>
          <td>
          	${horarioSimuladoInstance.observacao}
          </td>
          <td>
          	<g:if test="${horarioSimuladoInstance.flag == '1'}">
		  		<input type="checkbox" disabled="disabled"/>
		  	</g:if>
		  	<g:if test="${horarioSimuladoInstance.flag == '2'}">
		  		<input type="checkbox" checked="checked" disabled="disabled"/>
		  	</g:if>
          </td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${horarioSimuladoInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <!-- 
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/horarioSimulado/delete', ${horarioSimuladoInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
             -->
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${horarioSimuladoInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${horarioSimuladoInstanceTotal}</span>
    </div>
  </body>
</html>