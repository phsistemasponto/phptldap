
	<g:javascript src="horarioSimulado/horarioSimulado.js"/>
	<g:javascript src="comum/lov.js"/>
	<g:javascript src="jquery.meiomask.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${horarioSimuladoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${horarioSimuladoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${horarioSimuladoInstance.id}">
		<form action="${request.contextPath}/horarioSimulado/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${horarioSimuladoInstance?.id}" />
			<g:hiddenField name="version" value="${horarioSimuladoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/horarioSimulado/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>
		
			<div class="control-group">
				<label class="control-label">Classificação:</label>
				<div class="controls">			
					<g:select name="classificacao" from="${cadastros.Classificacao.list(sort: 'dscClass')}" optionKey="id" optionValue="dscClass" style="width:200px" onChange="habilitarHorario();"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">1a Entrada:</label>
				<div class="controls">
					<g:textField name="hora1" maxlength="5" value="" style="width:50px" onchange="atualizaCargaHoraria();"/>
					&nbsp;
					1a Saída:
					<g:textField name="hora2" maxlength="5" value="" style="width:50px" onchange="atualizaCargaHoraria();"/>
					&nbsp;
					2a Entrada:
					<g:textField name="hora3" maxlength="5" value="" style="width:50px" onchange="atualizaCargaHoraria();"/>
					&nbsp;
					2a Saída:
					<g:textField name="hora4" maxlength="5" value="" style="width:50px" onchange="atualizaCargaHoraria();"/>
					&nbsp;
					Carga Horária:
					<g:textField name="carga" maxlength="5" value="" style="width:50px" readonly="true"/>
					<a class="btn btn-medium" onClick="incluirItemHorario();">Incluir</a>
				</div>	
			</div>
			
			<div id="itemhorario">
				<table class="table table-striped table-bordered table-condensed"
					id="itemHorarioTable">
					<thead>
						<tr>
							<th style="width: 15%;">Dia</th>
							<th style="width: 25%;">Classificação</th>
							<th style="width: 12%;">Hora 1</th>
							<th style="width: 12%;">Hora 2</th>
							<th style="width: 12%;">Hora 3</th>
							<th style="width: 12%;">Hora 4</th>
							<th style="width: 12%;">Quant. Horas</th>
						</tr>
					</thead>
					<tbody>
						<g:each in="${itemHorarios?}" status="i" var="item">
							<tr id="rowTableItemHorario${i+1}" class="even">
								<td>
									<g:hiddenField name="nrDiaSemana-${item?.sequencia}" value="${item?.nrDiaSemana}" />
									<g:hiddenField name="sequencia" value="${item?.sequencia}" />
									<g:textField name="diaSemana" style="width:90%" value="${item?.diaSemana}" readOnly="true"/>
								</td>
								<td>
									<g:hiddenField name="idClassificacao-${item?.nrDiaSemana}" value="${item?.classificacao?.id}" />
									<g:textField name="dscClassificacao" style="width:90%" value="${item?.classificacao?.dscClass}" readOnly="true"/>
								</td>
								<td>
									<g:textField name="itemHora1-${item?.nrDiaSemana}" style="width:90%" value="${item?.hora1Str}" readOnly="true" />	
								</td>
								<td>
									<g:textField name="itemHora2-${item?.nrDiaSemana}" style="width:90%" value="${item?.hora2Str}" readOnly="true" />	
								</td>
								<td>
									<g:textField name="itemHora3-${item?.nrDiaSemana}" style="width:90%" value="${item?.hora3Str}" readOnly="true" />	
								</td>
								<td>
									<g:textField name="itemHora4-${item?.nrDiaSemana}" style="width:90%" value="${item?.hora4Str}" readOnly="true" />	
								</td>
								<td>
									<g:textField name="itemCarga-${item?.nrDiaSemana}" style="width:90%" value="${item?.cargaHorariaStr}" readOnly="true" />	
								</td>
							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
			
			<div class="control-group">
				<label class="control-label">Carga Horária Total:</label>
				<div class="controls">
					<g:textField name="cargaHorariaTotal" style="width:50px" value="${horarioSimuladoInstance?.cargaTotalStr}" readOnly="true"/>
				</div>
			</div>
		
			<div class="control-group">
				<label class="control-label">Observacao:</label>
				<div class="controls">
					<g:textField name="observacao" value="${horarioSimuladoInstance?.observacao}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Escala:</label>
				<div class="controls">
					<g:checkBox name="escala" value="${horarioSimuladoInstance?.escala}" checked="${horarioSimuladoInstance?.escala == 'S'}" />
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:if test="${administrador}">
			<button type="button" class="btn btn-large btn-primary" onclick="rejeitarHorario();">Rejeitar</button>
			<button type="button" class="btn btn-large btn-primary" onclick="gerarHorario();">Gerar horário</button>
		</g:if>
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>
	
	<div id="divConfirmGerarHorario" style="display: none;" title="Gerar Hor&aacute;rio">
	
		<form action="${request.contextPath}/horarioSimulado/gerarHorario" class="well" method="POST" id="formConfirmarGerar">
			
			<g:hiddenField name="id" value="${horarioSimuladoInstance?.id}" />
			
			<div class="control-group">
				<label class="control-label">Nome do horário:</label>
				<div class="controls">
					<g:textField name="nomeHorario" />
				</div>
			</div>
			
			<button type="button" class="btn btn-large btn-primary" onclick="confirmaGerarHorario();">Gerar horário</button>
		</form>
	
	</div>