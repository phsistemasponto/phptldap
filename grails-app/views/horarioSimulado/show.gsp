
<%@ page import="cadastros.HorarioSimulado" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'horarioSimulado.label', default: 'HorarioSimulado')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-horarioSimulado" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-horarioSimulado" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list horarioSimulado">
			
				<g:if test="${horarioSimuladoInstance?.usuarioSimulado}">
				<li class="fieldcontain">
					<span id="usuarioSimulado-label" class="property-label"><g:message code="horarioSimulado.usuarioSimulado.label" default="Usuario Simulado" /></span>
					
						<span class="property-value" aria-labelledby="usuarioSimulado-label"><g:link controller="usuario" action="show" id="${horarioSimuladoInstance?.usuarioSimulado?.id}">${horarioSimuladoInstance?.usuarioSimulado?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${horarioSimuladoInstance?.dtSimulado}">
				<li class="fieldcontain">
					<span id="dtSimulado-label" class="property-label"><g:message code="horarioSimulado.dtSimulado.label" default="Dt Simulado" /></span>
					
						<span class="property-value" aria-labelledby="dtSimulado-label"><g:formatDate date="${horarioSimuladoInstance?.dtSimulado}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${horarioSimuladoInstance?.flag}">
				<li class="fieldcontain">
					<span id="flag-label" class="property-label"><g:message code="horarioSimulado.flag.label" default="Flag" /></span>
					
						<span class="property-value" aria-labelledby="flag-label"><g:fieldValue bean="${horarioSimuladoInstance}" field="flag"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${horarioSimuladoInstance?.cargaHoraria}">
				<li class="fieldcontain">
					<span id="cargaHoraria-label" class="property-label"><g:message code="horarioSimulado.cargaHoraria.label" default="Carga Horaria" /></span>
					
						<span class="property-value" aria-labelledby="cargaHoraria-label"><g:fieldValue bean="${horarioSimuladoInstance}" field="cargaHoraria"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${horarioSimuladoInstance?.dtCriado}">
				<li class="fieldcontain">
					<span id="dtCriado-label" class="property-label"><g:message code="horarioSimulado.dtCriado.label" default="Dt Criado" /></span>
					
						<span class="property-value" aria-labelledby="dtCriado-label"><g:formatDate date="${horarioSimuladoInstance?.dtCriado}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${horarioSimuladoInstance?.historicoHorario}">
				<li class="fieldcontain">
					<span id="historicoHorario-label" class="property-label"><g:message code="horarioSimulado.historicoHorario.label" default="Historico Horario" /></span>
					
						<span class="property-value" aria-labelledby="historicoHorario-label"><g:fieldValue bean="${horarioSimuladoInstance}" field="historicoHorario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${horarioSimuladoInstance?.itensHorario}">
				<li class="fieldcontain">
					<span id="itensHorario-label" class="property-label"><g:message code="horarioSimulado.itensHorario.label" default="Itens Horario" /></span>
					
						<g:each in="${horarioSimuladoInstance.itensHorario}" var="i">
						<span class="property-value" aria-labelledby="itensHorario-label"><g:link controller="itemHorarioSimulado" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${horarioSimuladoInstance?.observacao}">
				<li class="fieldcontain">
					<span id="observacao-label" class="property-label"><g:message code="horarioSimulado.observacao.label" default="Observacao" /></span>
					
						<span class="property-value" aria-labelledby="observacao-label"><g:fieldValue bean="${horarioSimuladoInstance}" field="observacao"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${horarioSimuladoInstance?.usuarioCriado}">
				<li class="fieldcontain">
					<span id="usuarioCriado-label" class="property-label"><g:message code="horarioSimulado.usuarioCriado.label" default="Usuario Criado" /></span>
					
						<span class="property-value" aria-labelledby="usuarioCriado-label"><g:link controller="usuario" action="show" id="${horarioSimuladoInstance?.usuarioCriado?.id}">${horarioSimuladoInstance?.usuarioCriado?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${horarioSimuladoInstance?.id}" />
					<g:link class="edit" action="edit" id="${horarioSimuladoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
