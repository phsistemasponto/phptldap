	<g:javascript src="arquivoFolha/arquivoFolha.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${arquivoFolhaInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${arquivoFolhaInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${arquivoFolhaInstance.id}">
		<form action="${request.contextPath}/arquivoFolha/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${arquivoFolhaInstance?.id}" />
			<g:hiddenField name="version" value="${arquivoFolhaInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/arquivoFolha/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descrição Arquivo:</label>
				<div class="controls">
					<g:textField name="dscArquivo" maxlength="80" required="" value="${arquivoFolhaInstance?.dscArquivo}"/>
				</div>
			</div>
			
			<fieldset>
				<legend>Layout</legend>
				
				<div class="control-group">
					<label class="control-label">Campo:</label>
					<div class="controls">
						<g:select name="campo" from="${cadastros.ArquivoFolhaCampo.list()}" optionKey="id" optionValue="dscCampo" 
							required="" />
					</div>
				</div>
				
				<div class="row-fluid">	
					<div class="span3 bgcolor">
						<div class="control-group">
							<label class="control-label">Layout linha:</label>
							<div class="controls">
								<g:select name="layoutLinha" optionKey="id" optionValue="nome"
									style="width:100px;"
									from="${[ [id:'', nome:'...'],
											  [id:'1', nome:'Título'],
											  [id:'2', nome:'Detalhe'],
											  [id:'3', nome:'Sumário']  ]}"/>
							</div>
						</div>
					</div>
					
					<div class="span2 bgcolor">	
						<div class="control-group">
							<label class="control-label">Layout início:</label>
							<div class="controls">
								<g:textField name="layoutInicio" maxlength="4" style="width:50px;"/>
							</div>
						</div>
					</div>
						
					<div class="span2 bgcolor">
						<div class="control-group">
							<label class="control-label">Layout fim:</label>
							<div class="controls">
								<g:textField name="layoutfim" maxlength="4" style="width:50px;"/>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row-fluid">	
					<div class="span3 bgcolor">	
						<div class="control-group">
							<label class="control-label">Layout campo fixo:</label>
							<div class="controls">
								<g:textField name="layoutcampofixo" maxlength="20" style="width:50px;"/>
							</div>
						</div>
					</div>
					
					<div class="span2 bgcolor">	
						<div class="control-group">
							<label class="control-label">Layout preenchimento:</label>
							<div class="controls">
								<g:textField name="layoutpreenchimento" maxlength="20" style="width:50px;"/>
							</div>
						</div>
					</div>
						
					<div class="span3 bgcolor">
						<div class="control-group">
							<label class="control-label">Layout direção:</label>
							<div class="controls">
								<g:textField name="layoutdirecao" maxlength="4" style="width:50px;"/>
								
							</div>
						</div>
					</div>
					
					<div class="span bgcolor">
						<button type="button" class="btn btn-small" onclick="incluirLayout()">Incluir</button>
					</div>
					
				</div>
				
				<div id="layoutsDiv">
					<table class="table table-striped table-bordered table-condensed" id="layoutsTable">
						<thead>
							<tr>
								<th style="width: 30%;">Campo</th>
								<th style="width: 10%;">Linha</th>
								<th style="width: 10%;">Início</th>
								<th style="width: 10%;">Fim</th>
								<th style="width: 10%;">Campo fixo</th>
								<th style="width: 10%;">Preenchimento</th>
								<th style="width: 10%;">Direção</th>
								<th style="width: 10%;">&nbsp;</th>
							</tr>
						</thead>
  						<tbody>
							<g:each in="${arquivoFolhaInstance.layouts}" var="layout" status="i">
								<tr id="rowTableLayout-${i}">
									<td>${layout.arquivofolhacampo.dscCampo}</td>
									<td>${layout.layoutLinha}</td>
									<td>${layout.layoutInicio}</td>
									<td>${layout.layoutfim}</td>
									<td>${layout.layoutcampofixo}</td>
									<td>${layout.layoutpreenchimento}</td>
									<td>${layout.layoutdirecao}</td>
									<td>
										<input type="hidden" id="cmpArquivofolhacampo" name="cmpArquivofolhacampo" value="${layout.arquivofolhacampo.id}"/>
										<input type="hidden" id="cmpLayoutLinha" name="cmpLayoutLinha" value="${layout.layoutLinha}"/>
										<input type="hidden" id="cmpLayoutInicio" name="cmpLayoutInicio" value="${layout.layoutInicio}"/>
										<input type="hidden" id="cmpLayoutfim" name="cmpLayoutfim" value="${layout.layoutfim}"/>
										<input type="hidden" id="cmpLayoutcampofixo" name="cmpLayoutcampofixo" value="${layout.layoutcampofixo}"/>
										<input type="hidden" id="cmpLayoutpreenchimento" name="cmpLayoutpreenchimento" value="${layout.layoutpreenchimento}"/>
										<input type="hidden" id="cmpLayoutdirecao" name="cmpLayoutdirecao" value="${layout.layoutdirecao}"/>
										<i class="icon-remove" onclick="excluirLayout(${i});" style="cursor: pointer" title="Excluir registro"></i>
									</td>
								</tr>
							</g:each>
						</tbody>
					</table>
				</div>
				
			</fieldset>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>