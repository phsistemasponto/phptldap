<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    
    <g:if test="${request.message}">
    <div class="alert alert-info" role="status">${request.message}</div>
    </g:if>

    <g:if test="${request.error}">
    <div class="alert alert-error" role="status">${request.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Geração de benefícios</h3>
    </div>
    
    <g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="gerarBeneficios/gerarBeneficios.js"/>
        
    <g:form class="well form-horizontal" controller="gerarBeneficios" action="index" >
    	
    	
   		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Tipo de benefício:</label>
			<div class="controls">
				<g:select name="tipoBeneficio" from="${cadastros.TipoDeBeneficio.list()}" optionKey="id" value="${params.tipoBeneficio}" 
					noSelection="['':'..']" style="width:145px;" onchange="controlaTipoDeBeneficio()"/>
			</div>
		</div>
		
		<div id="divOpcaoFiltro" style="display: none;">
		
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Opção de filtro:</label>
				<div class="controls">
					<input type="radio" name="opcaoFiltro" id="opcaoFiltro" value="filial" onchange="controlaOpcaoFiltro();" 
						${params.opcaoFiltro == 'filial' ? 'checked=checked' : '' } > &nbsp; Filial
					<input type="radio" name="opcaoFiltro" id="opcaoFiltro" value="funcionario" onchange="controlaOpcaoFiltro()" 
						${params.opcaoFiltro == 'funcionario' ? 'checked=checked' : '' } > &nbsp; Funcionário
				</div>
			</div>
			
			<div id="divFiltroPadrao" style="display: none;">
	 			<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Filtro:</label>
					<div class="controls">
						<input type="hidden" name="filtroId" id="filtroId">
						<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
						<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
						<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
						<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
						<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
						<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
						<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
						<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
						
						<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
						<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
						
					</div>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Período desconto:</label>
				<div class="controls">
					<g:textField name="dataInicioDesconto" value="${params.dataInicioDesconto}" style="width:80px" required="" />
					até
					<g:textField name="dataFimDesconto" value="${params.dataFimDesconto}" style="width:80px" required="" />
				</div>
			</div>
		
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Período de compra:</label>
				<div class="controls">
					<g:textField name="dataInicioCompra" value="${params.dataInicioCompra}" style="width:80px" required=""/>
					até
					<g:textField name="dataFimCompra" value="${params.dataFimCompra}" style="width:80px" required=""/>
				</div>
			</div>
			
			<br/>
			<g:submitButton name="btnFiltro" value="Filtrar" class="btn btn-medium btn-primary" />
			<br/><br/>
			
    	</div>
    	
    </g:form>
    
    <br/><br/>
    
    <g:if test="${result != null && (result.typeResult == 'filial' || result.typeResult == 'funcionario')}">
    
		<g:if test="${result.typeResult == 'filial'}">
    	
    		<form action="${request.contextPath}/gerarBeneficios/processarFiliais" method="POST" id="formFiliais">
    		
    			<input type="hidden" id="dtIniDes" name="dtIniDes" value="${params.dataInicioDesconto}">
    			<input type="hidden" id="dtFimDes" name="dtFimDes" value="${params.dataFimDesconto}">
    			<input type="hidden" id="dtIniCom" name="dtIniCom" value="${params.dataInicioCompra}">
    			<input type="hidden" id="dtFimCom" name="dtFimCom" value="${params.dataFimCompra}">
    			<input type="hidden" id="tpBenefi" name="tpBenefi" value="${params.tipoBeneficio}">
    	
	    		<button type="button" class="btn btn-medium" onclick="marcarTodasFiliais();">Marcar todas</button>
				<button type="button" class="btn btn-medium" onclick="desmarcarTodasFiliais();">Desmarcar todas</button>
				<button type="submit" class="btn btn-medium btn-primary">Processar</button>
				
				<br/><br/>
		    	
		    	<table class="table table-striped table-bordered table-condensed header-fixed">
		    		<thead>
		    			<tr>
		    				<th style="width: 20px;"></th>
		    				<th style="width: 1000px;">Filial</th>
		    				<th style="width: 50px;">&nbsp;</th>
		    			</tr>
		    		</thead>
		    		<tbody>
		    			<g:each in="${result.data}" var="filial">
		    				<tr>
		    					<td style="width: 20px;">
		    						<input type="checkbox" name="filial" id="filial" value="${filial.id}"/>
		    					</td>
		    					<td style="width: 1000px;">${filial}</td>
		    					<td style="width: 50px;">&nbsp;</td>
		    				</tr>
		    			</g:each>
		    		</tbody>
		    	</table>
	    	
	    	</form>
	    	
    	</g:if>
    	<g:else>
    		
    		<form action="${request.contextPath}/gerarBeneficios/processarFuncionarios" method="POST" id="formFuncionarios">
    		
    			<input type="hidden" id="dtIniDes" name="dtIniDes" value="${params.dataInicioDesconto}">
    			<input type="hidden" id="dtFimDes" name="dtFimDes" value="${params.dataFimDesconto}">
    			<input type="hidden" id="dtIniCom" name="dtIniCom" value="${params.dataInicioCompra}">
    			<input type="hidden" id="dtFimCom" name="dtFimCom" value="${params.dataFimCompra}">
    			<input type="hidden" id="tpBenefi" name="tpBenefi" value="${params.tipoBeneficio}">
    	
	    		<button type="button" class="btn btn-medium" onclick="marcarTodosFuncionarios();">Marcar todos</button>
				<button type="button" class="btn btn-medium" onclick="desmarcarTodosFuncionarios();">Desmarcar todos</button>
				<button type="submit" class="btn btn-medium btn-primary">Processar</button>
				
				<br/><br/>
	    	
	    		<table class="table table-striped table-bordered table-condensed header-fixed">
		    		<thead>
		    			<tr>
		    				<th style="width: 20px;"></th>
		    				<th style="width: 1000px;">Funcionario</th>
		    				<th style="width: 50px;">&nbsp;</th>
		    			</tr>
		    		</thead>
		    		<tbody>
		    			<g:each in="${result.data}" var="funcionario">
		    				<tr>
		    					<td style="width: 20px;">
		    						<input type="checkbox" name="funcionario" id="funcionario" value="${funcionario.id}"/>
		    					</td>
		    					<td style="width: 1000px;">${funcionario}</td>
		    					<td style="width: 50px;">&nbsp;</td>
		    				</tr>
		    			</g:each>
		    		</tbody>
		    	</table>
	    	
	    	</form>
	    	
    	</g:else>
	    
    </g:if>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>