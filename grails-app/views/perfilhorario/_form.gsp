<g:javascript src="perfilhorario/perfilhorario.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${perfilhorarioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${perfilhorarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${perfilhorarioInstance.id}">
		<form action="${request.contextPath}/perfilhorario/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${perfilhorarioInstance?.id}" />
			<g:hiddenField name="version" value="${perfilhorarioInstance?.version}" />	
			<g:hiddenField name="sequencia"  value="${sequencia}" />
			
			<div class="row-fluid">	
				<div class="control-group">
					<label class="control-label">Descrição:</label>
					<div class="controls">
						<g:textField name="dscperfil" maxlength="80" required="" value="${perfilhorarioInstance?.dscperfil}" class="span4" readonly="true"/>
					</div>
				</div>
			</div>		
			
			
			<div class="row-fluid">		
				<div class="span2 bgcolor">
					<div class="control-group">
						<label class="control-label">Vira ?</label>
						<div class="controls">
						   <g:textField name="viraString" maxlength="80" required="" value="${perfilhorarioInstance?.viraString}" class="span1" readonly="true"/>
						</div>
					</div>
				</div>
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label">Carga Horária:</label>
						<div class="controls">
							<g:textField name="cargahor"  required="" value="${perfilhorarioInstance?.cargaHorariaHoras}"style="width:50px" class="span2" readonly="true"/>
						</div>
					</div>
				</div>



			</div>				 
	</g:if>
	<g:else>
		<form action="${request.contextPath}/perfilhorario/save" class="well form-horizontal" method="POST">
			<g:hiddenField name="sequencia"  value="1" /> 
	</g:else>	
		<fieldset>

				<div class="control-group">			
					<label class="control-label">Tipo Tolerância:</label>
					<div class="controls">
						<g:select id="tipotolerancia" name="tipotolerancia.id" from="${cadastros.Tipotolerancia.list()}" optionKey="id" required="" value="${perfilhorarioInstance?.tipotolerancia?.id}" class="many-to-one" noSelection="['null': '']" class="span4"/>
					</div>
				</div>
		
		</fieldset>				
		
		
		<hr />
		<h4>Horário das Batidas</h4>
		<br/>
		<div class="row-fluid">	
			<div class="span3 bgcolor">
				<div class="control-group">
					<label class="control-label">Cadastro das batidas:</label>
					<div class="controls">			
						<g:textField name="hrbatInput" maxlength="5" style="width:60px" />
					</div>
				</div>
			</div>
			<div class="span4 bgcolor">
				<div class="control-group">
					<label class="control-label">Status da batida:</label>
					<div class="controls">
						<g:select name="idbatInput" from="['Entrada','Saida']" keys="['E','S']"style="width:80px"/>
						<a href="#" class="btn btn-medium" onClick="incluirBatida();">Incluir</a>
					</div>	
				</div>
			</div>
		</div>
		<div id="bathorario">
			<table class="table table-striped table-bordered table-condensed"
				id="bathorarioTable">
				<thead>
					<tr>
							<th style="width: 20%;">Sequência</th>
							<th style="width: 20%;">Status Batida</th>
							<th style="width: 20%;">Horário</th>
							<th style="width: 10%;"></th>
					</tr>
				</thead>
				<tbody>
						<g:each in="${bathorarios?}" status="i" var="item">
							<tr id="rowTableBathorario${i+1}" class="even">
								<td><g:textField name="seqbathorario" style="width:90%"
										value="${item?.seqbathorario}" readOnly="true"/></td>
								<td><g:textField name="idbat" style="width:90%"
										value="${item?.idbatFormatado}" readOnly="true" /></td>
								<td><g:textField name="hrbat" style="width:90%"
										value="${item?.horaFormatada}" readOnly="true"/></td>
								<td>
									<g:if test="${i == quantidadeRegistros}">
										<i class="icon-remove" onclick="excluirBatida(${i+1});" style="cursor: pointer" title="Excluir registro"></i>
									</g:if>
									<g:else>
									</g:else>
								</td>
							</tr>
						</g:each>
				
				</tbody>
			</table>
		</div>			
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>