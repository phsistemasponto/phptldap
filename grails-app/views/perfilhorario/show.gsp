
<%@ page import="cadastros.Perfilhorario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'perfilhorario.label', default: 'Perfilhorario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-perfilhorario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-perfilhorario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list perfilhorario">
			
				<g:if test="${perfilhorarioInstance?.dscperfil}">
				<li class="fieldcontain">
					<span id="dscperfil-label" class="property-label"><g:message code="perfilhorario.dscperfil.label" default="Dscperfil" /></span>
					
						<span class="property-value" aria-labelledby="dscperfil-label"><g:fieldValue bean="${perfilhorarioInstance}" field="dscperfil"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${perfilhorarioInstance?.idvira}">
				<li class="fieldcontain">
					<span id="idvira-label" class="property-label"><g:message code="perfilhorario.idvira.label" default="Idvira" /></span>
					
						<span class="property-value" aria-labelledby="idvira-label"><g:fieldValue bean="${perfilhorarioInstance}" field="idvira"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${perfilhorarioInstance?.cargahor}">
				<li class="fieldcontain">
					<span id="cargahor-label" class="property-label"><g:message code="perfilhorario.cargahor.label" default="Cargahor" /></span>
					
						<span class="property-value" aria-labelledby="cargahor-label"><g:fieldValue bean="${perfilhorarioInstance}" field="cargahor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${perfilhorarioInstance?.tipotolerancia}">
				<li class="fieldcontain">
					<span id="tipotolerancia-label" class="property-label"><g:message code="perfilhorario.tipotolerancia.label" default="Tipotolerancia" /></span>
					
						<span class="property-value" aria-labelledby="tipotolerancia-label"><g:link controller="tipotolerancia" action="show" id="${perfilhorarioInstance?.tipotolerancia?.id}">${perfilhorarioInstance?.tipotolerancia?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${perfilhorarioInstance?.id}" />
					<g:link class="edit" action="edit" id="${perfilhorarioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
