<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
    <h2><g:message code="default.edit.label" args="['Perfil/Horário']" /></h2>
	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="perfilhorario/perfilhorario.js"/>       
    <g:render template="form"/>
  </body>
</html>