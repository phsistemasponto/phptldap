
<html>
  <head>
    <meta name="layout" content="main">
    <g:javascript src="comum/lov.js"/>
  </head>

  <body>
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Cadastro dos Horários das Batidas</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Perfil/Horário']" /></g:link>
    </div>
    
    <form action="${request.contextPath}/perfilhorario/list" method="POST" id="formPesquisa">
    	<div class="well">
  			<h3>Filtro</h3>
  			<div class="control-group">
				<label class="control-label">Tipo de filtro:</label>				
				<div class="controls">
					<g:select name="tipoFiltro" optionKey="id" optionValue="nome" 
							value="${params.tipoFiltro}"
							style="width:100px;"
							from="${[ [id:'0', nome:'...'],
									  [id:'1', nome:'Código'], 
									  [id:'2', nome:'Descrição'] ]}"/>
					<g:textField name="filtro" maxlength="250" value="${params.filtro}" style="margin-left: 10px; width: 400px;"/>
					<input type="hidden" id="order" name="order"/>
					<input type="hidden" id="sortType" name="sortType"/>
					<g:submitButton name="filtrar" class="btn btn-medium btn-primary" value="Filtrar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
	</form>

    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>
          	Descrição
          	<g:if test="${order != 'dscperfil'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('dscperfil', 'asc')"/></g:if>
          	<g:if test="${order == 'dscperfil' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('dscperfil', 'desc')"/></g:if>
          	<g:if test="${order == 'dscperfil' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('dscperfil', 'asc')"/></g:if>
          </th>
          <th>
          	Vira?
          	<g:if test="${order != 'idvira'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('idvira', 'asc')"/></g:if>
          	<g:if test="${order == 'idvira' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('idvira', 'desc')"/></g:if>
          	<g:if test="${order == 'idvira' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('idvira', 'asc')"/></g:if>
          </th>
          <th>
          	Carga Horária
          	<g:if test="${order != 'cargahor'}"><img src="${resource(dir:'images',file:'sort.png')}" onclick="order('cargahor', 'asc')"/></g:if>
          	<g:if test="${order == 'cargahor' && sortType == 'asc'}"><img src="${resource(dir:'images',file:'sort-asc.png')}" onclick="order('cargahor', 'desc')"/></g:if>
          	<g:if test="${order == 'cargahor' && sortType == 'desc'}"><img src="${resource(dir:'images',file:'sort-desc.png')}" onclick="order('cargahor', 'asc')"/></g:if>
          </th>
          <th>&nbsp;</th>
        </tr>
      </thead>

      <tbody>
      <g:each in="${perfilhorarioInstanceList}" status="i" var="perfilhorarioInstance">
        <tr>
          <td>${fieldValue(bean: perfilhorarioInstance, field: "dscperfil")}</td>
          <td>${fieldValue(bean: perfilhorarioInstance, field: "viraString")}</td>
          <td>${fieldValue(bean: perfilhorarioInstance, field: "cargaHorariaHoras")}</td>
          <td style="width: 50px; text-align: center">
            <g:link action="edit" id="${perfilhorarioInstance.id}" title="${message(code:'default.button.edit.label')}"><i class="icon-edit"></i></g:link>&nbsp;
            <i class="icon-remove" onclick="excluirRegistro('${message(code:'default.delete.confirm.message')}', '${request.contextPath}/perfilhorario/delete', ${perfilhorarioInstance.id})" style="cursor:pointer" title="${message(code:'default.button.delete.label')}"></i>
          </td>
        </tr>
      </g:each>
      </tbody>
    </table>

    <div class="paginate">
      <g:paginate total="${perfilhorarioInstanceTotal}" />
      <span class="label label-info" style="float: right; position: relative; top:-3px;">Total de registros: ${perfilhorarioInstanceTotal}</span>
    </div>
  </body>
</html>