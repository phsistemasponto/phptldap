
<%@page import="cadastros.Horario"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="reports/reports.js"/>
    <g:javascript src="reports/horarios.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Relatório de horários</h3>
    </div>    
    <g:jasperForm controller="relatorioHorarios" action="generateReport" jasper="horarios" class="form-horizontal" id="formPesquisa">
    
    	<div class="well">
    	
    		<button type="button" class="btn btn-medium btn-primary" onclick="marcarTodosHorarios();">Marcar todos</button>
    		<button type="button" class="btn btn-medium btn-primary" onclick="desmarcarTodosHorarios();">Desmarcar todos</button>
    		
    		<br><br>
    	
    		<table class="table table-striped table-bordered table-condensed" id="horarioTable">
	    		<tbody>
	    			<g:each in="${Horario.findAll('from Horario j order by dscHorario')}" var="hor" status="i">
	    				<g:if test="${i%4==0}">
		    			<tr>
		    			</g:if>
		    				<td width="1%">
		    					<input type="checkbox" name="horariosId" id="horariosId" value="${hor.id}" checked="checked"/>
		    				</td>
		    				<td>${hor.dscHorario}</td>
		    			<g:if test="${i%4==3}">
		    			</tr>
		    			</g:if>
	    			</g:each>
	    		</tbody>
	    	</table>
				    
			<button class="btn btn-medium btn-primary" onclick="generateReport('PDF');">Gerar em PDF</button>
			<button class="btn btn-medium btn-primary" onclick="generateReport('XLS');">Gerar em Excel</button>
    	
		</div>
    
    </g:jasperForm>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>

    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>