<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
    <div class="titulo">
      <h3>Administração</h3>
      <g:link action="create" class="btn"><g:message code="default.create.label" args="['Perfil de Acesso']" /></g:link>
    </div>
	<g:javascript src="jquery.meiomask.js"/>
	<g:javascript src="administracao/administracao.js"/>
    <g:render template="form"/>
  </body>
</html>