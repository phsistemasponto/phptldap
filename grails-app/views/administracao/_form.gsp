

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${administracaoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${administracaoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${administracaoInstance.id}">
		<form action="${request.contextPath}/administracao/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${administracaoInstance?.id}" />
			<g:hiddenField name="version" value="${administracaoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/administracao/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>
			<div class="row-fluid">		
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Tipo do Caracter:</label>
						<div class="controls">
							<g:select name="tipoCaracter" from="${['Letras e Números','Letras','Números']}" keys="${['T','L','N']}" required="" value="${administracaoInstance?.tipoCaracter}" valueMessagePrefix="administracao.tipoCaracter"/>
						</div>
					</div>             
				</div>
				
				<div class="span6 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Sincronismo:</label>
						<div class="controls">
							<g:select name="sincronismo" from="${['Sim','Não']}" keys="${['S','N']}" required="" value="${administracaoInstance?.sincronismo}" valueMessagePrefix="administracao.sincronismo"/>
						</div>
					</div>             
				</div>
				
			</div>



			<div class="row-fluid">
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Histórico de Login:</label>
						<div class="controls">
							<g:select name="historicoLogin" from="${['Sim','Não']}" keys="${['S','N']}"  required="" value="${administracaoInstance?.historicoLogin}" valueMessagePrefix="administracao.historicoLogin"/>
						</div>
					</div>             
				</div>
				
				
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Histórico de Transação:</label>
						<div class="controls">
							<g:select name="historicoTrans" from="${['Sim','Não']}" keys="${['S','N']}"  required="" value="${administracaoInstance?.historicoTrans}" valueMessagePrefix="administracao.historicoTrans"/>
						</div>
					</div>             
				</div>
								
			</div>



			<div class="row-fluid">		
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label">Dias Aviso:</label>
						<div class="controls">
							<g:textField name="diasAviso" required="" value="${fieldValue(bean: administracaoInstance, field: 'diasAviso')}" maxLength="1"/>
						</div>
					</div>
				</div>
			
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label">Intervalo Repetição:</label>
						<div class="controls">
							<g:textField name="intervaloRepeticao" required="" value="${fieldValue(bean: administracaoInstance, field: 'intervaloRepeticao')}" maxLength="1"/>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="row-fluid">		
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label">Periodicidade:</label>
						<div class="controls">
							<g:textField name="periodicidade" required="" value="${fieldValue(bean: administracaoInstance, field: 'periodicidade')}" maxLength="1"/>
						</div>
					</div>
				</div>

				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label">Período Inativo:</label>
						<div class="controls">
							<g:textField name="periodoInativo" required="" value="${fieldValue(bean: administracaoInstance, field: 'periodoInativo')}" maxLength="1"/>
						</div>
					</div>
				</div>
			</div>
				
			<div class="row-fluid">		
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label">Quant.Máx.Caracteres Repetidos:</label>
						<div class="controls">
							<g:textField name="qtdMaxCaracRep" required="" value="${fieldValue(bean: administracaoInstance, field: 'qtdMaxCaracRep')}" maxLength="1"/>
						</div>
					</div>
				</div>
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label">Quant. Senhas Repetidas:</label>
						<div class="controls">
							<g:textField name="qtdSenhasRepetidas" required="" value="${fieldValue(bean: administracaoInstance, field: 'qtdSenhasRepetidas')}" maxLength="1"/>
						</div>
					</div>
				</div>
			</div>

			<div class="row-fluid">		
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label">Quantidade Caracteres:</label>
						<div class="controls">
							<g:textField name="quantidadeCaracteres" required="" value="${fieldValue(bean: administracaoInstance, field: 'quantidadeCaracteres')}" maxLength="1"/>
						</div>
					</div>
				</div>

				<div class="span4 bgcolor">
				
					<div class="control-group">
						<label class="control-label">Tentativas de Login:</label>
						<div class="controls">
							<g:textField name="tentativasLogin" required="" value="${fieldValue(bean: administracaoInstance, field: 'tentativasLogin')}" maxLength="1"/>
						</div>
					</div>
				</div>

			</div>
		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<a class="btn btn-large" href="${request.contextPath}/">Retornar</a>
	</form>