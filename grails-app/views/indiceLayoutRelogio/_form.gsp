

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${indiceLayoutRelogioInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${indiceLayoutRelogioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${indiceLayoutRelogioInstance.id}">
		<form action="${request.contextPath}/indiceLayoutRelogio/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${indiceLayoutRelogioInstance?.id}" />
			<g:hiddenField name="version" value="${indiceLayoutRelogioInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/indiceLayoutRelogio/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Descricao:</label>
				<div class="controls">
					<g:textField name="descricao" maxlength="60" required="" value="${indiceLayoutRelogioInstance?.descricao}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Parametro:</label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${indiceLayoutRelogioInstance?.parametro?}" var="p">
    <li><g:link controller="paramRelogio" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="paramRelogio" action="create" params="['indiceLayoutRelogio.id': indiceLayoutRelogioInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'paramRelogio.label', default: 'ParamRelogio')])}</g:link>
</li>
</ul>

				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>