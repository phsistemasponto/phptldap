<%@page import="cadastros.Periodo"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="fechamento/fechamento.js"/>
    <g:javascript src="criticasExtras/criticasExtras.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Críticas extras</h3>
    </div>
    <form action="${request.contextPath}/criticasExtras/list" method="POST" id="formPesquisa">
    	<div class="well">
    		<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Filial:</label>
				<div class="controls">
					<g:select id="filial" name="filial" from="${filiais}" optionKey="id" value="${params.filial}" 
						noSelection="['':'..']" style="width:145px;" onchange="mudaFilial()"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Ano:</label>
				<div class="controls">
					<g:select id="ano" name="ano" onchange="mudaAno()" style="width:145px;" noSelection="['':'..']"
						from="${Periodo.executeQuery('select distinct year(p.dtIniPr) as ano from Periodo p order by 1 asc')}" />
				</div>
			</div>
    		<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Período:</label>
				<div class="controls">
					<select id="periodo" name="periodo" onchange="mudaPeriodo()" style="width: 145px;">
						<option value="">..</option>
					</select>
					<input type="text" disabled="disabled" id="dataInicio" style="width:70px;">
					<input type="text" disabled="disabled" id="dataFim" style="width:70px;">
				</div>
			</div>
 			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Filtro:</label>
				<div class="controls">
					<input type="hidden" name="filtroId" id="filtroId">
					<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
					<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
					<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
					<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
					<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
					<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
					<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
					<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
					
					<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
					<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					<g:submitButton name="btnFiltro" class="btn btn-medium btn-primary" value="Listar" style="margin-left: 10px; margin-top: -10px;"/>
				</div>
			</div>
		</div>
			
		<input type="hidden" id="order" name="order"/>
		<input type="hidden" id="sortType" name="sortType"/>
		
	</form>
           
    <hr/>

	<form action="${request.contextPath}/criticasExtras/concluir" method="POST" id="formFuncionarios">
		
		<table class="table table-striped table-bordered table-condensed header-fixed">
	      <thead>
	        <tr>
	          <th style="width: 70px;"><span>Sequência</span></th>
	          <th style="width: 300px;"><span>Nome do funcionário</span></th>
	          <th style="width: 100px;"><span>Banco anterior</span></th>
	          <th style="width: 100px;"><span>Compensações</span></th>
	          <th style="width: 70px;"><span>1 Extras</span></th>
	          <th style="width: 70px;"><span>2 Extras</span></th>
	          <th style="width: 70px;"><span>Pré crítica</span></th>
	          <th style="width: 70px;"><span>Valor real</span></th>
	          <th style="width: 100px;"><span>Valor receber</span></th>
	          <th style="width: 90px;"><span>Situação</span></th>
	        </tr>
	      </thead>
	
	      <tbody>
	      <g:each in="${criticasList}" status="i" var="c">
	        <tr class="situacao${c.situacao}">
	          <td style="width: 70px;">${c.sequencia}</td>
	          <td style="width: 300px;">${c.nome}</td>
	          <td style="width: 100px;">${c.bancoAnterior}</td>
	          <td style="width: 100px;">${c.compensacoes}</td>
	          <td style="width: 70px;">${c.extras1}</td>
	          <td style="width: 70px;">${c.extras2}</td>
	          <td style="width: 70px;">${c.preCritica}</td>
	          <td style="width: 70px;">${c.valorReal}</td>
	          <td style="width: 100px;">${c.valorReceber}</td>
	          <td style="width: 90px;">
	          	<g:if test="${c.situacao == 0}">
	          		<input type="checkbox" name="situacao" id="situacao" class="situacao${c.situacao}">
	          	</g:if>
	          	<g:else>
	          		<input type="checkbox" name="situacao" id="situacao" class="situacao${c.situacao}" checked="checked">
	          	</g:else>
	          </td>
	        </tr>
	      </g:each>
	      </tbody>
	      <tfoot>
	      	<tr>
	      		<td style="width: 70px;"></td>
	      		<td style="width: 766px;">Total: ${criticasTotal}</td>
		      	<td style="width: 70px;">${somaValorReal}</td>
		      	<td style="width: 100px;">${somaValorReceber}</td>
		      	<td style="width: 105px;">&nbsp;</td>
		    </tr>
	      </tfoot>
	    </table>
	    
	    
	    <div class="row-fluid" style="margin-left: 660px;">
    		<div class="span2 bgcolor">
	    		<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Orçamento filial:</label>
					<div class="controls">
						<g:textField name="orcamentoFilial" value="${orcamentoFilial}" style="width:180px" readonly="true" />
					</div>
				</div>
			</div>
			<div class="span2 bgcolor">
				<div class="control-group">
					<label class="control-label" style="font-weight: bold;">Diferença:</label>
					<div class="controls">
						<g:textField name="diferenca" value="${diferenca}" style="width:180px" readonly="true" />
					</div>
				</div>
			</div>
		</div>
	    
    </form>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
    <script>
    	$j('#filtroFilialId').parent().parent().remove();
    	$j("tr.situacao1").css({'background-color': 'orange'});
    	$j("input.situacao1").attr("checked","true");
	</script>
    
  </body>
</html>