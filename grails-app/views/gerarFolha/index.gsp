<%@page import="cadastros.Periodo"%>
<%@page import="cadastros.Folha"%>
<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
    
    <g:if test="${request.message}">
    <div class="alert alert-info" role="status">${request.message}</div>
    </g:if>

    <g:if test="${request.error}">
    <div class="alert alert-error" role="status">${request.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Geração de folha</h3>
    </div>
    
    <g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="gerarFolha/gerarFolha.js"/>
       
    <g:form class="well form-horizontal" controller="gerarFolha" action="index" >
    
    	<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Ano:</label>
			<div class="controls">
				<g:select id="ano" name="ano" onchange="mudaAno()" value="${params.ano}" style="width:145px;" noSelection="['':'..']"
					from="${Periodo.executeQuery('select distinct year(p.dtIniPr) as ano from Periodo p order by 1 asc')}" />
			</div>
		</div>
   		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Período:</label>
			<div class="controls">
				<select id="periodo" name="periodo" onchange="mudaPeriodo()" style="width: 145px;">
					<option value="">..</option>
				</select>
				<input type="text" disabled="disabled" id="dataInicio" style="width:70px;">
				<input type="text" disabled="disabled" id="dataFim" style="width:70px;">
			</div>
		</div>
    	
    	<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Opção de filtro:</label>
			<div class="controls">
				<input type="radio" name="opcaoFiltro" id="opcaoFiltro" value="filial" onchange="controlaOpcaoFiltro();" 
					${params.opcaoFiltro == 'filial' || params.opcaoFiltro == null ? 'checked=checked' : '' } > &nbsp; Filial
				<input type="radio" name="opcaoFiltro" id="opcaoFiltro" value="funcionario" onchange="controlaOpcaoFiltro()" 
					${params.opcaoFiltro == 'funcionario' ? 'checked=checked' : '' } > &nbsp; Funcionário
			</div>
		</div>
		
		<div id="divFiltroPadrao" style="display: none;">
 			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Filtro:</label>
				<div class="controls">
					<input type="hidden" name="filtroId" id="filtroId">
					<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
					<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
					<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
					<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
					<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
					<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
					<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
					<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
					<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
					<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Folha a exportar:</label>
			<div class="controls">
				<g:select id="opcaoFolha" name="opcaoFolha" style="width:145px;" noSelection="['':'..']" from="${Folha.findAll()}" optionKey="id"/>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" style="font-weight: bold;">Ordem:</label>
			<div class="controls">
				<input type="radio" name="opcaoOrdem" id="opcaoOrdem" value="F" ${params.opcaoOrdem == 'M' ? 'checked=checked' : '' }>&nbsp; Matrícula
				<input type="radio" name="opcaoOrdem" id="opcaoOrdem" value="E" ${params.opcaoOrdem == 'E' ? 'checked=checked' : '' }>&nbsp; Evento
			</div>
		</div>
		
		<br/>
		<g:submitButton name="btnFiltro" value="Filtrar" class="btn btn-medium btn-primary" />
		<br/><br/>
			
    	
    </g:form>
    
    <br/><br/>
    
    <g:if test="${result != null && (result.typeResult == 'filial' || result.typeResult == 'funcionario')}">
    
		<g:if test="${result.typeResult == 'filial'}">
    	
    		<form action="${request.contextPath}/gerarFolha/processarFiliais" method="POST" id="formFiliais">
    		
    			<input type="hidden" id="cmpPeriodo" name="cmpPeriodo" value="${params.periodo}">
    			<input type="hidden" id="cmpOpcaoFolha" name="cmpOpcaoFolha" value="${params.opcaoFolha}">
    			<input type="hidden" id="cmpOpcaoOrdem" name="cmpOpcaoOrdem" value="${params.opcaoOrdem}">
    	
	    		<button type="button" class="btn btn-medium" onclick="marcarTodasFiliais();">Marcar todas</button>
				<button type="button" class="btn btn-medium" onclick="desmarcarTodasFiliais();">Desmarcar todas</button>
				<button type="submit" class="btn btn-medium btn-primary">Processar</button>
				
				<br/><br/>
		    	
		    	<table class="table table-striped table-bordered table-condensed header-fixed">
		    		<thead>
		    			<tr>
		    				<th style="width: 20px;"></th>
		    				<th style="width: 700px;">Filial</th>
		    				<th style="width: 300px;">Status</th>
		    				<th style="width: 50px;">&nbsp;</th>
		    			</tr>
		    		</thead>
		    		<tbody>
		    			<g:each in="${result.data}" var="i">
		    				<tr>
		    					<td style="width: 20px;">
		    						<input type="checkbox" name="filial" id="filial" value="${i.filial.id}"/>
		    					</td>
		    					<td style="width: 700px;">${i.filial}</td>
		    					<td style="width: 300px;">
		    						<g:if test="${i.status == 'A'}">
						          		<span class="label label-success">Aberto</span>
						          	</g:if>
						          	<g:else>
						          		<span class="label label-important">Fechado</span>
						          	</g:else>
		    					</td>
		    					<td style="width: 50px;">&nbsp;</td>
		    				</tr>
		    			</g:each>
		    		</tbody>
		    	</table>
	    	
	    	</form>
	    	
    	</g:if>
    	<g:else>
    		
    		<form action="${request.contextPath}/gerarBeneficios/processarFuncionarios" method="POST" id="formFuncionarios">
    		
    			<input type="hidden" id="cmpPeriodo" name="cmpPeriodo" value="${params.periodo}">
    			<input type="hidden" id="cmpOpcaoFolha" name="cmpOpcaoFolha" value="${params.opcaoFolha}">
    			<input type="hidden" id="cmpOpcaoOrdem" name="cmpOpcaoOrdem" value="${params.opcaoOrdem}">
    	
	    		<button type="button" class="btn btn-medium" onclick="marcarTodosFuncionarios();">Marcar todos</button>
				<button type="button" class="btn btn-medium" onclick="desmarcarTodosFuncionarios();">Desmarcar todos</button>
				<button type="submit" class="btn btn-medium btn-primary">Processar</button>
				
				<br/><br/>
	    	
	    		<table class="table table-striped table-bordered table-condensed header-fixed">
		    		<thead>
		    			<tr>
		    				<th style="width: 20px;"></th>
		    				<th style="width: 1000px;">Funcionario</th>
		    				<th style="width: 50px;">&nbsp;</th>
		    			</tr>
		    		</thead>
		    		<tbody>
		    			<g:each in="${result.data}" var="funcionario">
		    				<tr>
		    					<td style="width: 20px;">
		    						<input type="checkbox" name="funcionario" id="funcionario" value="${funcionario.id}"/>
		    					</td>
		    					<td style="width: 1000px;">${funcionario}</td>
		    					<td style="width: 50px;">&nbsp;</td>
		    				</tr>
		    			</g:each>
		    		</tbody>
		    	</table>
	    	
	    	</form>
	    	
    	</g:else>
	    
    </g:if>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
    <div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
    <div id="baixarArquivo" class="carregandoDiv">
    	<div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<g:form class="well form-horizontal" controller="gerarFolha" action="baixarArquivo">
	    		<input type="hidden" id="baixarArquivoId" name="baixarArquivoId" value=""/>
	    		<g:submitButton name="btnBaixarArquivo" value="Baixar arquivo" class="btn btn-medium btn-primary"/>
	    		<button type="button" id="btnFechar" class="btn btn-medium btn-primary" onclick="hideBaixar();">Fechar</button> 
	    	</g:form>
    	</div>
    </div>
    
  </body>
</html>