
<html>
  <head>
  	<meta name="layout" content="main">
  </head>

  <body>
  
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="usuario/usuario.js"/>
  	
    <g:if test="${request.message}">
    <div class="alert alert-info" role="status">${request.message}</div>
    </g:if>

    <g:if test="${request.error}">
    <div class="alert alert-error" role="status">${request.error}</div>
    </g:if>

    <div class="titulo">
      <h2>Reiniciar senha</h2>
    </div>
    
    <br/><br/><br/>
    
    <form action="${request.contextPath}/reiniciarSenha/reiniciarSenha" method="POST" id="formReiniciarSenha">
    
    	<div class="control-group">
			<label class="control-label">Login:</label>
			<div class="controls">
				<g:textField name="login" maxlength="80" required="" value="${params.login}"/>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Data de nascimento:</label>
			<div class="controls">
				<g:textField name="dtNascimento" maxlength="80" required="" value="${params.dtNascimento}"/>
			</div>
		</div>
		
		<button type="button" class="btn btn-medium btn-primary" onclick="reiniciarSenha();">Reiniciar</button>
		
	</form>
	
  </body>
</html>