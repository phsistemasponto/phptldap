<g:javascript src="empresa/empresa.js"/>

	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${empresaInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${empresaInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${empresaInstance.id}">
		<form action="${request.contextPath}/empresa/update" class="well form-horizontal" method="POST" enctype="multipart/form-data">
			<g:hiddenField name="id" value="${empresaInstance?.id}" />
			<g:hiddenField name="version" value="${empresaInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/empresa/save" class="well form-horizontal" method="POST" enctype="multipart/form-data">
	</g:else>	
		<fieldset>
             <div class="row-fluid">		
				<div class="span8 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Descrição:</label>
						<div class="controls">
							<g:textField name="dscEmp" maxlength="80" required="" value="${empresaInstance?.dscEmp}"/>
						</div>
					</div>             
				</div>			
				
			</div>
			
			
             <div class="row-fluid">
                <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Nome Smtp:</label>
						<div class="controls">
							<g:textField name="nomeSmtp" maxlength="80" value="${empresaInstance?.nomeSmtp}"/>
						</div>
					</div>             
				</div>
             
             
             
			    <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Smtp Servidor:</label>
						<div class="controls">
							<g:textField name="smtpServidor" maxlength="80" value="${empresaInstance?.smtpServidor}"/>
						</div>
					</div>             
				</div>		
											
								
			</div>


             <div class="row-fluid">
               <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Email Smtp:</label>
						<div class="controls">
							<g:textField name="emailSmtp" maxlength="80" value="${empresaInstance?.emailSmtp}"/>
						</div>
					</div>             
				</div>		
				
 
                <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Smtp Usuario:</label>
						<div class="controls">
							<g:textField name="smtpUsuario" maxlength="80" value="${empresaInstance?.smtpUsuario}"/>
						</div>
					</div>             
				</div>		
		  	</div>		
				
				
			<div class="row-fluid">
               <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Smtp Senha:</label>
						<div class="controls">
							<g:textField name="smtpSenha" maxlength="80" value="${empresaInstance?.smtpSenha}"/>
						</div>
					</div>             
				</div>
			 
		  	</div>					
		  	
			<div class="row-fluid">	
		       <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Ao Processar retirar os abonos?:</label>
						<div class="controls">							
							<g:checkBox name="chekAbonos" value="${empresaInstance?.chekAbonos}" checked="${empresaInstance?.chekAbonos == 'S'}" /></td>
						</div>
					</div>             
				</div>		
				
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Mostra documentação do funcionário?</label>
						<div class="controls">							
							<g:checkBox name="idDoc" value="${empresaInstance?.idDoc}" checked="${empresaInstance?.idDoc == 'S'}" /></td>
						</div>
					</div>             
				</div>		
				
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Mostra salário do funcionário?</label>
						<div class="controls">							
							<g:checkBox name="idSal" value="${empresaInstance?.idSal}" checked="${empresaInstance?.idSal == 'S'}" /></td>
						</div>
					</div>             
				</div>
				
		    </div>		
				
			<div class="row-fluid">
               <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Carga Inter Jor:</label>
						<div class="controls">
							<g:textField name="cargaInterJor"  required="" value="${empresaInstance?.cargaInterJorHoras}"style="width:50px" class="span2"/>
						</div>
					</div>             
				</div>		
				
 
                <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Carga Limt Mensal:</label>
						<div class="controls">							
							<g:textField name="cargaLimtMensal"  required="" value="${empresaInstance?.cargaLimtMensalHoras}"style="width:50px" class="span2"/>
						</div>
					</div>             
				</div>		
		  	</div>		

 			<div class="row-fluid">
               <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Carga Limt Semanal:</label>
						<div class="controls">							
							<g:textField name="cargaLimtSemanal"  required="" value="${empresaInstance?.cargaLimtSemanalHoras}"style="width:50px" class="span2"/>
						</div>
					</div>             
			</div>		
				
 
            <div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" for="street">Carga Limt Semanal Not:</label>
						<div class="controls">							
							<g:textField name="cargaLimtSemanalNot"  required="" value="${empresaInstance?.cargaLimtSemanalNotHoras}"style="width:50px" class="span2"/>
						</div>
					</div>             
				</div>		
		  	</div>		
		
			
			<div class="control-group">
				<label class="control-label">Smtp Porta:</label>
				<div class="controls">
					<g:textField name="smtpPorta" required="" value="${fieldValue(bean: empresaInstance, field: 'smtpPorta')}"/>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Logomarca:</label>
				<div class="controls" id="controlsFoto">
					<input type="hidden" id="mudouFoto" name="mudouFoto" value="false"/>
					<div style="height: 150px; width: 150px; border: 1px solid black;" id="divFoto">
						<g:if test="${possuiFoto}">
							<img src="${createLink(controller:'empresa', action:'image', id: empresaInstance.id)}"/>
						</g:if>
					</div>
					<g:if test="${!possuiFoto}">
						<input type="file" id="files" name="files" />
					</g:if>
					<g:else>
						<button id="removFoto" type="button" onclick="removerFoto();">Remover foto</button>
					</g:else>
				</div>
			</div>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>
	
	<script>

		function removerFoto() {
			$j('#divFoto img').remove();
			$j('#removFoto').remove();

			$j('#controlsFoto').append('<input type=\"file\" id=\"files\" name=\"files\" />');

			$j('#mudouFoto').val('true');
		}

	</script>
	
	