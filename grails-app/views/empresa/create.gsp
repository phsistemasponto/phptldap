
<html>
  <head>
    <meta name="layout" content="main">
  </head>
  <body>
     <g:javascript src="jquery.meiomask.js"/>
     <g:javascript src="empresa/empresa.js"/>
    <h2><g:message code="default.create.label" args="['Empresa']" /></h2>
    <g:render template="form"/>
  </body>
</html>