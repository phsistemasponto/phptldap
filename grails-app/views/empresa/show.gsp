
<%@ page import="cadastros.Empresa" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'empresa.label', default: 'Empresa')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-empresa" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-empresa" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list empresa">
			
				<g:if test="${empresaInstance?.dscEmp}">
				<li class="fieldcontain">
					<span id="dscEmp-label" class="property-label"><g:message code="empresa.dscEmp.label" default="Descrição" /></span>
					
						<span class="property-value" aria-labelledby="dscEmp-label"><g:fieldValue bean="${empresaInstance}" field="dscEmp"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.smtpServidor}">
				<li class="fieldcontain">
					<span id="smtpServidor-label" class="property-label"><g:message code="empresa.smtpServidor.label" default="Smtp Servidor" /></span>
					
						<span class="property-value" aria-labelledby="smtpServidor-label"><g:fieldValue bean="${empresaInstance}" field="smtpServidor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.emailSmtp}">
				<li class="fieldcontain">
					<span id="emailSmtp-label" class="property-label"><g:message code="empresa.emailSmtp.label" default="Email Smtp" /></span>
					
						<span class="property-value" aria-labelledby="emailSmtp-label"><g:fieldValue bean="${empresaInstance}" field="emailSmtp"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.nomeSmtp}">
				<li class="fieldcontain">
					<span id="nomeSmtp-label" class="property-label"><g:message code="empresa.nomeSmtp.label" default="Nome Smtp" /></span>
					
						<span class="property-value" aria-labelledby="nomeSmtp-label"><g:fieldValue bean="${empresaInstance}" field="nomeSmtp"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.smtpUsuario}">
				<li class="fieldcontain">
					<span id="smtpUsuario-label" class="property-label"><g:message code="empresa.smtpUsuario.label" default="Smtp Usuario" /></span>
					
						<span class="property-value" aria-labelledby="smtpUsuario-label"><g:fieldValue bean="${empresaInstance}" field="smtpUsuario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.smtpSenha}">
				<li class="fieldcontain">
					<span id="smtpSenha-label" class="property-label"><g:message code="empresa.smtpSenha.label" default="Smtp Senha" /></span>
					
						<span class="property-value" aria-labelledby="smtpSenha-label"><g:fieldValue bean="${empresaInstance}" field="smtpSenha"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.chekAbonos}">
				<li class="fieldcontain">
					<span id="chekAbonos-label" class="property-label"><g:message code="empresa.chekAbonos.label" default="Ao processar retirar os abonos" /></span>
					
						<span class="property-value" aria-labelledby="chekAbonos-label"><g:fieldValue bean="${empresaInstance}" field="chekAbonos"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.idTpPesq}">
				<li class="fieldcontain">
					<span id="idTpPesq-label" class="property-label"><g:message code="empresa.idTpPesq.label" default="Consultar pelo" /></span>
					
						<span class="property-value" aria-labelledby="idTpPesq-label"><g:fieldValue bean="${empresaInstance}" field="idTpPesq"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.idCriaSeq}">
				<li class="fieldcontain">
					<span id="idCriaSeq-label" class="property-label"><g:message code="empresa.idCriaSeq.label" default="Cria sequencia do funcionário" /></span>
					
						<span class="property-value" aria-labelledby="idCriaSeq-label"><g:fieldValue bean="${empresaInstance}" field="idCriaSeq"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.idDoc}">
				<li class="fieldcontain">
					<span id="idDoc-label" class="property-label"><g:message code="empresa.idDoc.label" default="Mostra endereço do funcionário" /></span>
					
						<span class="property-value" aria-labelledby="idDoc-label"><g:fieldValue bean="${empresaInstance}" field="idDoc"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.cargaInterJor}">
				<li class="fieldcontain">
					<span id="cargaInterJor-label" class="property-label"><g:message code="empresa.cargaInterJor.label" default="Carga Inter Jor" /></span>
					
						<span class="property-value" aria-labelledby="cargaInterJor-label"><g:fieldValue bean="${empresaInstance}" field="cargaInterJor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.cargaLimtMensal}">
				<li class="fieldcontain">
					<span id="cargaLimtMensal-label" class="property-label"><g:message code="empresa.cargaLimtMensal.label" default="Carga Limt Mensal" /></span>
					
						<span class="property-value" aria-labelledby="cargaLimtMensal-label"><g:fieldValue bean="${empresaInstance}" field="cargaLimtMensal"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.cargaLimtSemanal}">
				<li class="fieldcontain">
					<span id="cargaLimtSemanal-label" class="property-label"><g:message code="empresa.cargaLimtSemanal.label" default="Carga Limt Semanal" /></span>
					
						<span class="property-value" aria-labelledby="cargaLimtSemanal-label"><g:fieldValue bean="${empresaInstance}" field="cargaLimtSemanal"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.cargaLimtSemanalNot}">
				<li class="fieldcontain">
					<span id="cargaLimtSemanalNot-label" class="property-label"><g:message code="empresa.cargaLimtSemanalNot.label" default="Carga Limt Semanal Not" /></span>
					
						<span class="property-value" aria-labelledby="cargaLimtSemanalNot-label"><g:fieldValue bean="${empresaInstance}" field="cargaLimtSemanalNot"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empresaInstance?.smtpPorta}">
				<li class="fieldcontain">
					<span id="smtpPorta-label" class="property-label"><g:message code="empresa.smtpPorta.label" default="Smtp Porta" /></span>
					
						<span class="property-value" aria-labelledby="smtpPorta-label"><g:fieldValue bean="${empresaInstance}" field="smtpPorta"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${empresaInstance?.id}" />
					<g:link class="edit" action="edit" id="${empresaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
