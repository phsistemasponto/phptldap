<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="classificacao/classificacao.js"/>
    <g:javascript src="funcionario/funcionario.js"/>
    <g:javascript src="funcionarioLote/funcionarioLote.js"/>
    <g:javascript src="lancamentosDiarios/lancamentosDiarios.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Lançamentos diários</h3>
    </div>
        
    <form action="${request.contextPath}/lancamentosDiarios/list" method="POST" id="formPesquisa" class="form-horizontal">
    	<div class="well">
	    	<div class="row-fluid">
	    		<div class="span4 bgcolor">
		    		<div class="control-group">
						<label class="control-label" style="font-weight: bold;">Data início:</label>
						<div class="controls">
							<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" />
						</div>
					</div>
				</div>
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" style="font-weight: bold;">Data fim:</label>
						<div class="controls">
							<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" />
						</div>
					</div>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" style="font-weight: bold;">Filtro:</label>
				<div class="controls">
					<input type="hidden" name="filtroId" id="filtroId">
					<input type="hidden" name="filtroPadrao_FilialId" id="filtroPadrao_FilialId" value="${params.filtroPadrao_FilialId}">
					<input type="hidden" name="filtroPadrao_CargoId" id="filtroPadrao_CargoId" value="${params.filtroPadrao_CargoId}">
					<input type="hidden" name="filtroPadrao_DepartamentoId" id="filtroPadrao_DepartamentoId" value="${params.filtroPadrao_DepartamentoId}">
					<input type="hidden" name="filtroPadrao_FuncionarioId" id="filtroPadrao_FuncionarioId" value="${params.filtroPadrao_FuncionarioId}">
					<input type="hidden" name="filtroPadrao_TipoFuncionarioId" id="filtroPadrao_TipoFuncionarioId" value="${params.filtroPadrao_TipoFuncionarioId}">
					<input type="hidden" name="filtroPadrao_HorarioId" id="filtroPadrao_HorarioId" value="${params.filtroPadrao_HorarioId}">
					<input type="hidden" name="filtroPadrao_SubUniOrgId" id="filtroPadrao_SubUniOrgId" value="${params.filtroPadrao_SubUniOrgId}">
					<input type="hidden" name="filtroPadrao_NaoListaDemitidos" id="filtroPadrao_NaoListaDemitidos" value="${params.filtroPadrao_NaoListaDemitidos}">
					
					<g:textField name="filtroDesc" readonly="true" style="width:300px" value="${params.filtroDesc}"/>
					<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showFiltroPadrao();"></i>&nbsp;
					<g:submitButton name="btnFiltro" class="btn btn-medium btn-primary" value="Listar" style="margin-left: 10px;"/>
				</div>
			</div>
			
			<input type="hidden" id="order" name="order"/>
			<input type="hidden" id="sortType" name="sortType"/>
					
		</div>
	</form>
           
    <hr/>

	<form action="${request.contextPath}/lancamentosDiarios/lancamentosDiarios" method="POST" id="formFuncionarios">
	
		<g:if test="${funcionarioInstanceList}">
		
		<div style="max-width:1170px; overflow-x: scroll; white-space: nowrap;">
		
			
			<table id="tableLancamentosDiarios" class="table table-striped table-bordered table-condensed" style="margin-right: 10px; overflow: scroll;">
				<thead>
					<tr>
						<th>Matrícula</th>
						<th style="width:200px;">Funcionário</th>
						<g:each in="${funcionarioInstanceList.values().toArray()[0]}" status="i" var="dld">
							<th style="text-align: center;" class="celulaLancamentoDiarioGeral" id="celulaLancamentoDiarioGeral-${dld.identificadorDia}">
								${dld.dia}<br>${dld.data}
								<input type="hidden" id="hiddenLancamentoDiarioGeral-${dld.identificadorDia}" 
									name="hiddenLancamentoDiarioGeral-${dld.identificadorDia}" value="">
							</th>
						</g:each>
					</tr>
				</thead>
				<tbody>
					<g:each in="${funcionarioInstanceList.keySet()}" status="i" var="funcionario">
						<tr>
							<td>${funcionario.mtrFunc}</td>
							<td nowrap="nowrap">${funcionario.nomFunc}</td>
							<g:each in="${funcionarioInstanceList.get(funcionario)}" status="j" var="dld">
								<td class="celulaLancamentoDiario" id="celulaLancamentoDiario-${funcionario.id}-${dld.identificadorDia}">
									<canvas id="canvasCelulaLancamento-${funcionario.id}-${dld.identificadorDia}" width="20" height="20"></canvas>
									<input type="hidden" id="classificacaoCelulaLancamento-${funcionario.id}-${dld.identificadorDia}" 
										name="classificacaoCelulaLancamento-${funcionario.id}-${dld.identificadorDia}" value="${dld.classificacao}">
									<input type="hidden" id="itemHorarioCelulaLancamento-${funcionario.id}-${dld.identificadorDia}" 
										name="itemHorarioCelulaLancamento-${funcionario.id}-${dld.identificadorDia}" value="${dld.itemHorario}">
								</td>
							</g:each>
						</tr>
					</g:each>
				</tbody>
			</table>
		
			<table>
				<tr>
					<td>
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Classificação:</label>
							<div class="controls">
								<g:textField name="classificacaoId" style="width:40px" onchange="buscaIdClassificacaoLancamentosDiarios();"/>
								<i class="icon-search" style="cursor:pointer" title="Procurar Classificação" id="classificacaoLancamentoBotaoBusca"
									onclick="showModalClassificacaoLancamentosDiarios();"></i>&nbsp;
								<g:textField name="classificacaoDescricao" readonly="true" style="width:250px"/>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="font-weight: bold;">Item horário:</label>
							<div class="controls">
								<g:textField name="itemHorarioId" style="width:40px" required="true" 
									onchange="buscaIdPerfilhorarioLancamentosDiarios();" disabled="true"/>
								<i class="icon-search" style="cursor:pointer" title="Procurar Item Horário" id="itemHorarioBotaoBusca"
									onclick="showModalPerfilhorarioLancamentosDiarios();"></i>&nbsp;
								<g:textField name="itemHorarioDescricao" readonly="true" style="width:250px" disabled="true"/>
							</div>
						</div>
						
						<g:submitButton name="Aplicar" class="btn btn-medium btn-primary"/>
						
					</td>
					<td style="padding-left: 50px;">
						<div style="border: 1px solid #DDDDDD; width: 200px; padding: 10px;">
							<table>
								<g:each in="${cadastros.Classificacao.findAll(sort:"id")}" status="i" var="c">
									<tr>
										<td>
											<canvas id="canvasClassificacao-${c.id}" width="20" height="20"></canvas>
										</td>
										<td valign="top" style="padding-left: 15px;">
											${c.dscClass} 
											<input type="hidden" id="corClassificacao-${c.id}" value="${c.corExibicao}"/>
										</td>
									</tr>
								</g:each>
							</table>
						</div>
					</td>
				</tr>
			</table>
			
			<br><br>
			
		</div>
		
		</g:if>
		
		<g:hiddenField name="dataInicio" value="${params.dataInicio}"/>
		<g:hiddenField name="dataFim" value="${params.dataFim}"/>
		
		<g:each in="${params.keySet()}" status="i" var="p">
	   		<g:if test="${p.startsWith('formPesquisa_')}">
	   			<input type="hidden" name="${p}" id="${p}" value="${params[p]}"/>
	   		</g:if>
	     </g:each>
	    
    </form>
    
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
    
  </body>
</html>