
	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="parametroPeriodo/parametroPeriodo.js"/>
    
	<g:if test="${flash.message}">
		<div class="alert alert-info">${flash.message}</div>
	</g:if>

	<g:hasErrors bean="${parametroPeriodoInstance}">
		<div class="alert alert-error">
			<ul class="errors" role="alert">
				<g:eachError bean="${parametroPeriodoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
		</div>
	</g:hasErrors>

	<g:if test="${parametroPeriodoInstance.id}">
		<form action="${request.contextPath}/parametroPeriodo/update" class="well form-horizontal" method="POST">
			<g:hiddenField name="id" value="${parametroPeriodoInstance?.id}" />
			<g:hiddenField name="version" value="${parametroPeriodoInstance?.version}" />
	</g:if>
	<g:else>
		<form action="${request.contextPath}/parametroPeriodo/save" class="well form-horizontal" method="POST">
	</g:else>	
		<fieldset>

			<div class="control-group">
				<label class="control-label">Padrao Nome:</label>
				<div class="controls">
					<g:textField name="padraoNome" required="" value="${parametroPeriodoInstance?.padraoNome}"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Dia Início:</label>
				<div class="controls">
					<g:textField name="diaInicio" required="" value="${fieldValue(bean: parametroPeriodoInstance, field: 'diaInicio')}" style="width: 45px;"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Dia Fim (Último do mês):</label>
				<div class="controls">
					<g:checkBox name="diaFimUltimoMes" value="${parametroPeriodoInstance?.diaFimUltimoMes}" onchange="changeUltimoDiaMes();"/>
				</div>
			</div>

			<div class="control-group" id="divDiaFim">
				<label class="control-label">Dia Fim:</label>
				<div class="controls">
					<g:textField name="diaFim" value="${fieldValue(bean: parametroPeriodoInstance, field: 'diaFim')}" style="width: 45px;"/>
				</div>
			</div>
			
			<fieldset>
				<legend>Filiais</legend>
				
				<button type="button" class="btn btn-medium btn-primary" onclick="marcarTodos();">Marcar todos</button>
				<button type="button" class="btn btn-medium btn-primary" onclick="desmarcarTodos();">Desmarcar todos</button>
				
				<br/><br/>
				
				<table class="table table-striped table-bordered table-condensed" id="filiaisTable">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Filial</th>
						</tr>
					</thead>
					<tbody>
						<g:set var="fs" bean="filialService"/>
						<g:each in="${fs.filiaisDoUsuario()}" var="f" status="i">
							<tr>
								<td>
									<g:if test="${parametroPeriodoInstance.id != null && cadastros.ParametroPeriodoFilial.findByParametroPeriodoAndFilial(parametroPeriodoInstance, f) != null}">
										<input type="checkbox" name="filial" id="filial" value="${f.id}" checked="checked"/>
									</g:if>
									<g:else>
										<input type="checkbox" name="filial" id="filial" value="${f.id}"/>
									</g:else>
								</td>
								<td>${f}</td>
							</tr>
						</g:each>
					</tbody>
				</table>
				
			</fieldset>

		</fieldset>
		<hr>
		<g:submitButton name="salvar" class="btn btn-large btn-primary" value="${message(code: 'default.button.save.label')}" />
		<g:if test="${parametroPeriodoInstance.id}">
			<button type="button" class="btn btn-large btn-primary" onclick="criarPeriodo();">Criar período</button>
		</g:if>
		<g:link action="list" class="btn btn-large"><g:message code="default.button.return.label" /></g:link>
	</form>