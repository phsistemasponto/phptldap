
<%@ page import="cadastros.ParametroPeriodo" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'parametroPeriodo.label', default: 'ParametroPeriodo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-parametroPeriodo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-parametroPeriodo" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list parametroPeriodo">
			
				<g:if test="${parametroPeriodoInstance?.padraoNome}">
				<li class="fieldcontain">
					<span id="padraoNome-label" class="property-label"><g:message code="parametroPeriodo.padraoNome.label" default="Padrao Nome" /></span>
					
						<span class="property-value" aria-labelledby="padraoNome-label"><g:fieldValue bean="${parametroPeriodoInstance}" field="padraoNome"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroPeriodoInstance?.diaInicio}">
				<li class="fieldcontain">
					<span id="diaInicio-label" class="property-label"><g:message code="parametroPeriodo.diaInicio.label" default="Dia Inicio" /></span>
					
						<span class="property-value" aria-labelledby="diaInicio-label"><g:fieldValue bean="${parametroPeriodoInstance}" field="diaInicio"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroPeriodoInstance?.diaFimUltimoMes}">
				<li class="fieldcontain">
					<span id="diaFimUltimoMes-label" class="property-label"><g:message code="parametroPeriodo.diaFimUltimoMes.label" default="Dia Fim Ultimo Mes" /></span>
					
						<span class="property-value" aria-labelledby="diaFimUltimoMes-label"><g:formatBoolean boolean="${parametroPeriodoInstance?.diaFimUltimoMes}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroPeriodoInstance?.diaFim}">
				<li class="fieldcontain">
					<span id="diaFim-label" class="property-label"><g:message code="parametroPeriodo.diaFim.label" default="Dia Fim" /></span>
					
						<span class="property-value" aria-labelledby="diaFim-label"><g:fieldValue bean="${parametroPeriodoInstance}" field="diaFim"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parametroPeriodoInstance?.filiais}">
				<li class="fieldcontain">
					<span id="filiais-label" class="property-label"><g:message code="parametroPeriodo.filiais.label" default="Filiais" /></span>
					
						<g:each in="${parametroPeriodoInstance.filiais}" var="f">
						<span class="property-value" aria-labelledby="filiais-label"><g:link controller="parametroPeriodoFilial" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${parametroPeriodoInstance?.id}" />
					<g:link class="edit" action="edit" id="${parametroPeriodoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
