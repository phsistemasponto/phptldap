<html>
  <head>
    <meta name="layout" content="main">
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="escala/escala.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h3>Escala</h3>
    </div>
  
    <form action="${request.contextPath}/escala/lancarEscala" method="POST" id="formEscala" class="form-horizontal">
    
    	<div class="well">
    	
	    	<div class="row-fluid">
	
	            <div class="span5">
				<div class="controls-horizontal">
					<g:textField name="dataInicio" value="${params.dataInicio}" style="width:80px" placeholder="Data início:" required="required" onchange="buscaDataEscala()"/>
					&emsp;<g:textField name="dataFim" value="${params.dataFim}" style="width:80px" placeholder="Data fim:" required="required"/>
				</div>
			    </div>
	
   		
				<input type="hidden" name="escalaDataId" id="escalaDataId" value="${params.escalaDataId}"/>
				
				
				<div class="span4 bgcolor">
					<div class="control-group">
						<label class="control-label" style="font-weight: bold;">Descrição:</label>
						<div class="controls">
							<g:textField name="descricao" value="${params.descricao}" style="width:250px" />
						</div>
					</div>
				</div>
				<div class="span2 bgcolor">
					<div class="control-group">
						<label class="control-label" style="font-weight: bold;">Status:</label>
						<div class="controls">
							<g:textField name="status" value="${params.status}" style="width:80px" readonly="readonly"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<hr/>
		
		<div class="well">
			<div class="row-fluid">
	    		<div class="control-group">
					<label class="control-label"><b>Item Horário:</b></label>
					<div class="controls">
						<g:textField name="itemHorarioId" maxlength="5" style="width:50px" 
							onBlur="buscaId('cadastros.Perfilhorario', 'id', 'dscperfil', 'itemHorarioId', 'itemHorarioDescricao', false);"/>
						&nbsp; 
						<i class="icon-search" onclick="showBuscaInterna('itemHorarioId', 'itemHorarioDescricao', 'cadastros.Perfilhorario', 'id', 'dscperfil', false);"
							style="cursor: pointer" title="Procurar Item Horario" id="itemHorarioBotaoBusca"></i> 
						&nbsp;
						<g:textField name="itemHorarioDescricao" maxlength="450" readOnly="true" style="width:450px" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><b>Letra Legenda:</b></label>
					<div class="controls">
						<g:textField name="letraLegenda" maxlength="5" style="width:50px"/>
						<button type="button" class="btn btn-medium btn-primary" style="margin-left: 30px;" onclick="incluirItemHorario()">Incluir</button>
					</div>
				</div>
			</div>
			
			<br><br>
			
			<table class="table table-striped table-bordered table-condensed" id="itemHorarioTable">
				<thead>
					<tr>
						<th>Horário</th>
						<th>Letra</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${escalasHorarios}" status="i" var="escala">
						<tr id="rowEscalaHorario-${i}">
							<td>
								<input type="hidden" name="escId" id="escIdHorario" value="${escala.id}"/>
								<input type="hidden" name="escIdHorario" id="escIdHorario" value="${escala.perfilhorario.id}"/>
								<input type="text" name="escDescHorario" id="escDescHorario" value="${escala.perfilhorario.dscperfil}" readonly="readonly"/>
							</td>
							<td>
								<input type="text" name="escLetra" id="escLetra" value="${escala.idLetra}" readonly="readonly"/>
							</td>
							<td>
								<i class="icon-remove" onclick="excluirItemHorario(${i});" style="cursor: pointer" title="Excluir registro"></i>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
		
		<br><br>
		
		<button type="button" class="btn btn-medium btn-primary" style="margin-left: 30px;" onclick="avancaLancarEscalas()">Proximo</button>
		
	</form>
           
    <g:render template="/busca/lovPadrao"/>
    <g:render template="/busca/lovBuscaInterna"/>
    
  </body>
</html>