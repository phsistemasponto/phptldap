<html>
  <head>
    <meta name="layout" content="main">
    <style>
    .titulo{
	    	background-color: #05577f !important;
	    	padding: 5px 20px !important;
	    	-webkit-border-radius: 5px 5px 0 0!important;
			-moz-border-radius: 5px 5px 0 0 !important;
			border-radius: 5px 5px 0 0 !important;
	}
	.titulo h3{
		color:#FFFFFF !important;
	}
    .text-center{
    	text-align: center !important;
    }
    </style>
  </head>

  <body>
  	
  	<g:javascript src="jquery.meiomask.js"/>
    <g:javascript src="comum/lov.js"/>
    <g:javascript src="escala/escala.js"/>
    
    <g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    

    <g:if test="${flash.error}">
    <div class="alert alert-error" role="status">${flash.error}</div>
    </g:if>

    <div class="titulo">
      <h2>Lançar escalas</h2>
    </div>
    
    <br/><br/><br/>
    
    <form action="${request.contextPath}/escala/aplicarLancamentoEscala" method="POST" id="formLancamentoEscala" class="form-horizontal">
    
	    <button type="submit" class="btn btn-medium btn-primary" id="btnAplicar">Aplicar</button>
	    <button type="button" class="btn btn-medium btn-primary" id="btnEncerrar" style="margin-left: 15px;" onclick="encerrarPeriodo()">Encerrar Período</button>
	    <button type="button" class="btn btn-medium btn-primary" id="btnRelatorio" style="margin-left: 15px;" onclick="generateReport('PDF')">Relatório</button>
	    <button type="button" class="btn btn-medium btn-primary" id="btnEnviarEmail" style="margin-left: 15px;" onclick="enviarEmail()">Enviar e-mail</button>

		   
	    <input type="hidden" name="dtInicio" id="dtInicio" value="${params.dataInicio}">
	    <input type="hidden" name="dtFim" id="dtFim" value="${params.dataFim}">
	    <input type="hidden" name="statusEscala" id="statusEscala" value="${statusEscala}">
	    
	    <br/><br/>
	    
	    <div class="well">
	    	
	    	<div class="row-fluid">
			    <div class="control-group">
					<label class="control-label"><b>Horários:</b></label>
					<div class="controls">
						<g:select id="perfilHorario" name="perfilHorario" from="${horarios}" optionKey="id" style="width:220px;" />
							
						<span style="margin-left: 30px;">
							<input type="checkbox" name="marcaTodos" id="marcaTodos">&nbsp;
							<b>Marcar para todos</b>
						</span>
					</div>
				</div>
			</div>
			
			<br/><br/>
			
			<div style="width: 1100px; overflow-x: scroll; border: 1px solid rgba(0, 0, 0, 0.05); padding: 10px;">
				
				<table class="table table-striped table-bordered table-condensed" id="lancamentosTable" style="width: 100%; border-collapse:separate;">
					<thead>
						<tr>
							<th style="width: 50px;">Código</th>
							<th style="width: 200px; white-space:nowrap;">Funcionário</th>
							<th style="width: 80px; white-space:nowrap;">Grupo</th>
							<g:each in="${diasLancamentos}" status="i" var="dld">
								<th style="width: 50px; text-align: center;">${dld.dia}<br>${dld.data}</th>
							</g:each>
						</tr>
					</thead>
					<tbody>
						<g:each in="${dadosLancamentos}" status="j" var="lanc">
							<tr>
								<td style="width: 50px;">${lanc.funcionarioCracha}</td>
								<td style="width: 200px; white-space:nowrap;">${lanc.funcionarioNome}</td>
								<td style="width: 80px; white-space:nowrap;">${lanc.grupo}</td>
								<g:each in="${lanc.lancamentos}" status="l" var="lancDia">
									<td style="width: 50px; text-align: center; white-space: nowrap;" class="celulaDadosLancamentos">
										<span>${lancDia.siglaEscala}</span>
										<input type="hidden" 
											name="hiddenLancDia-${lanc.funcionarioId}-${lancDia.data}"
											id="hiddenLancDia-${lanc.funcionarioId}-${lancDia.data}"
											value="${lancDia.siglaEscala}">
									</td>
								</g:each>
							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
			
		</div>
		
	</form>
	
	<form action="${request.contextPath}/escala/encerrarPeriodo" method="POST" id="formEncerrarPeriodo" class="form-horizontal">
		<input type="hidden" name="dtInicio" id="dtInicio" value="${params.dataInicio}">
		<input type="hidden" name="dtFim" id="dtFim" value="${params.dataFim}">
	</form>
	
	<form action="${request.contextPath}/escala/generateReport" method="POST" id="formReportEscala" class="form-horizontal">
		<input type="hidden" name="_format" id="_format" value="PDF">
		<input type="hidden" name="dtInicio" id="dtInicio" value="${params.dataInicio}">
	    <input type="hidden" name="dtFim" id="dtFim" value="${params.dataFim}">
	    <input type="hidden" name="statusEscala" id="statusEscala" value="${statusEscala}">
	    <input type="hidden" name="escalaDataId" id="escalaDataId" value="${params.escalaDataId}"/>
	</form>
	
	<div id="carregando" class="carregandoDiv">
	    <div id="carregandoDiv" style="position:absolute; left:50%; top:50%; margin-left: -110px;">
	    	<img src="${resource(dir:'images',file:'ajax-loader.gif')}" class="ph"/>
	    </div>
    </div>
    
  </body>
</html>