package cadastros

import java.math.*
import java.text.*

class HorarioService {

	boolean transactional = true
	
	Horario salvarHorario(params) {
						
		Horario horarioInstance = new Horario()
		horarioInstance.dscHorario= params.dscHorario
		Date dataOcorrencia
		
		try {
			horarioInstance.dtIniVig= new SimpleDateFormat("dd/MM/yyyy").parse(params.dtIniVig)
		} catch (Exception e) {
		  	println (e)
		}	
		
		if (!horarioInstance.save()){
			horarioInstance.errors.each {
				println(it)
			}
			return horarioInstance
		} else {
		}
		 		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.seqhorario.getClass()) }
		def arraySeqHorario = isArray ? params.seqhorario : [params.seqhorario]
		def arrayClassificacao = isArray ? params.idClassificacao : [params.idClassificacao]
		def arrayPerfilHorario = isArray ? params.idPerfilhorario : [params.idPerfilhorario]
		def arrayDataOcorrencia = isArray ? params.dataOcorrencia : [params.dataOcorrencia]
		def arrayNrDiaSemana = isArray ? params.nrDiaSemana : [params.nrDiaSemana]
		def arrayCargaHoraria = isArray ? params.cargaHoraria : [params.cargaHoraria]
			
		for (x in 0..arraySeqHorario.size()-1){
			Classificacao classificacao = Classificacao.get(Long.parseLong(arrayClassificacao[x]))

			Perfilhorario perfilhorario
			try {
				perfilhorario = Perfilhorario.get(Long.parseLong(arrayPerfilHorario[x]))
			} catch (Exception e) {
				println('Horario nulo!')
			}
			try {
				dataOcorrencia = new SimpleDateFormat("dd/MM/yyyy").parse(arrayDataOcorrencia[x])
			} catch (Exception e) {
	  			println (e)
			}	
			
			horarioInstance.addToItensHorario(
				new Itemhorario(
					classificacao: classificacao,
					perfilhorario: perfilhorario,
					seqitemhorario: arraySeqHorario[x],
					nrdiasemana: arrayNrDiaSemana[x],
					dtinivig: dataOcorrencia,
					qtdechefetiva: arrayCargaHoraria[x],
					idvira:'S'
					
				)
			)
		}

		horarioInstance.save(flush: true)
		horarioInstance.itensHorario.each { i ->
			i.save(flush: true) 
			i.errors.each { e->
				println(e)
			}
		}
		
		horarioInstance 
	}
	
	Horario atualizarHorario(params){
		
		Horario horario = Horario.get(Integer.parseInt(params.id))
		
		def itenshorarios = Itemhorario.findAll ('from Itemhorario where horario=:horario',[horario: horario])
		itenshorarios.each() {
			Itemhorario itemhorario = Itemhorario.get(it.id)
			horario.removeFromItensHorario(itemhorario)
			itemhorario.delete()
		}
				
		horario.dscHorario= params.dscHorario
		Date dataOcorrencia
		

		
		try {
			horario.dtIniVig= new SimpleDateFormat("dd/MM/yyyy").parse(params.dtIniVig)
		} catch (Exception e) {
		  println (e)
		}	
		
		if (!horario.save()){
			horario.errors.each {
				println(it)
			}
			return horario
		} else {
		}
		
		if (params?.seqhorario?.size()>1){
			
			for (x in 0..params.seqhorario.size()-1){
				Classificacao classificacao = Classificacao.get(Long.parseLong(params.idClassificacao[x]))
				Perfilhorario perfilhorario 
				
				try {
					perfilhorario = Perfilhorario.get(Long.parseLong(params.idPerfilhorario[x]))
				} catch (Exception e) {
					println('Hor�rio nulo!')
				}
				
				
				
				try {
					dataOcorrencia = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataOcorrencia[x])
				} catch (Exception e) {
		  			println (e)
				}	

				
				
				horario.addToItensHorario(
					new Itemhorario(
						classificacao: classificacao,
						perfilhorario: perfilhorario,
						seqitemhorario: params.seqhorario[x],
						nrdiasemana: params.nrDiaSemana[x],
						dtinivig: dataOcorrencia,
						qtdechefetiva: params.cargaHoraria[x],
						idvira:'S'
					)
				)
			}


		} else if (params?.seqhorario?.size()==1){
				Classificacao classificacao = Classificacao.get(Long.parseLong(params.idClassificacao))
				
				Perfilhorario perfilhorario
				
				try {
					perfilhorario = Perfilhorario.get(Long.parseLong(params.idPerfilhorario))
				} catch (Exception e) {
					println('Hor�rio nulo!')
				}

				
				
				try {
					dataOcorrencia = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataOcorrencia)
				} catch (Exception e) {
		  			println (e)
				}	
				horario.addToItensHorario(
					new Itemhorario(
						classificacao: classificacao,
						perfilhorario: perfilhorario,
						seqitemhorario: params.seqhorario,
						nrdiasemana: params.nrDiaSemana,
						dtinivig: dataOcorrencia,
						qtdechefetiva: params.cargaHoraria,
						idvira:'S'
					)
				)
		}
		
		horario.save()
		horario
				
	}
	
	Horario excluirHorario(idHorario){
		def horario = Horario.get(Long.parseLong(idHorario))
		Itemhorario itemhorario
		def itenshorarios = Itemhorario.findAll ('from Itemhorario where horario=:horario',[horario: horario])
		itenshorarios.each() {
			itemhorario = Itemhorario.get(it.id)
			horario.removeFromItensHorario(itemhorario)
			itemhorario.delete()
		}
		
		horario.save()
		horario.delete()
		horario
	}
}


