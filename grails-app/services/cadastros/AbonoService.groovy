


package cadastros

import java.math.*
import java.text.*

import org.springframework.jdbc.core.JdbcTemplate

class AbonoService {

	def dataSource
	boolean transactional = true
	
	def processaAbono(
			seqFilial,
			seqFunc,
			seqUsuario,
			seqJustificativa,
			dtIni,
			dtFim,
			quantidadeDias,
			quantidadeHoras,
			tipoAbono,
			observacao,
			cidId) {
			
		def retorno
		def seq_pr
		def statusFechamento
		def idLimpaDescontos
		def idDebitaBh
		def idAdcionalnot
		def idNewMatricula
		def eventoAdc
		def statusAdc
		def hora1
		def hora2
		def horaAdc
		def eventoDsrp
		def eventoDsrd
		def dtInicio
		def faltas
		def nrHrfaltas
		def vrdsr
		def eventoFtotal
		def cont
		def aumentoHoras
		def tipoExtra
		def varSeqOcor
		def horas
		def saldoExtras
		def horaExtras1
		def dtinicio
		def dtfim
		def comp
		def idTipo
		def reg
		def ocorrencia
		def ocorrenciaPrimeiroDia
		def quantidadeHorasDias
		
		def horasAbono = 0
		def dtBat = dtIni
		def funcionario = Funcionario.get(seqFunc.toLong())
		def filial = Filial.get(seqFilial.toLong())
		def justificativa = Justificativas.get(seqJustificativa.toLong())
		
		retorno = 9
		comp = 0
		
		while (dtBat <= dtFim) {
			
			def consulta = "from Movimentos m "
					.concat("where m.ocorrencia.idTpOcor = 'D' ")
					.concat("  and m.ocorrencia.dscOcor not like 'DSR%' ")
					.concat("  and m.nrQtdeHr > 0 ")
					.concat("  and m.dtBat = :data ")
					.concat("  and m.funcionario = :funcionario ")
					.concat("order by m.dtBat ")
					
			Movimentos movimento = Movimentos.find(consulta, [funcionario: funcionario, data: dtBat])
			ocorrencia = movimento?.ocorrencia
			if (ocorrenciaPrimeiroDia == null) {
				ocorrenciaPrimeiroDia = movimento?.ocorrencia
			}
			
			if (observacao.toString().trim().equals("99999")) {
				idTipo = 2
			} else {
				idTipo = 1
			}
			
			saldoExtras = quantidadeHoras
			quantidadeHorasDias = (movimento == null ? 0 : movimento.nrQtdeHr)
			
			// verifica se o periodo ja esta fechado , ai o sistema n�o deixa mais abonar
			def periodo = null
			def periodoFilial = null
			
			try {
				periodo = Periodo.find ("from Periodo p where :data between p.dtIniPr and p.dtFimPr ", [data: dtBat])
				periodoFilial = PeriodoFilial.findByFilialAndPeriodo(filial, periodo)
				
				seq_pr = periodoFilial.id
				
			} catch (Exception e) {
				retorno = 0
				return retorno
			}
			
			reg = seqFunc
			statusFechamento = StatusFechamento.findByFuncionarioAndPeriodo(funcionario, periodo)
			if (statusFechamento != null) {
				retorno = 1
				return retorno
			}
			
			// pega os parametros das justificativas
			idLimpaDescontos = justificativa.idLimpaDesc
			idDebitaBh = justificativa.idDebitaBh
			idAdcionalnot = justificativa.idAdcionalnot
			idNewMatricula = justificativa.idNewMatricula
			aumentoHoras = justificativa.vrAumentoHr
			
			if (tipoAbono.equals("0") || tipoAbono.equals("1")) {
			
				horaExtras1 = 0
				if (idDebitaBh.equals("S")) {
					def queryMovimentos  = "select sum(nrQtdeHr) from Movimentos m "
						queryMovimentos += "where m.ocorrencia.idTpOcor = 'P' "
						queryMovimentos += "and m.ocorrencia.tipohoraextras is not null "
						queryMovimentos += "and m.ocorrencia.indiceExt = 1 "
						queryMovimentos += "and m.dtBat between :dtInicio and :dtFim "
						queryMovimentos += "and m.funcionario = :funcionario "
					horaExtras1 = Movimentos.executeQuery(queryMovimentos, [dtInicio: dtIni, dtFim: dtFim, funcionario: funcionario])[0]
					
					if (horaExtras1 == null) {
						horaExtras1 = 0
					}
					
					comp = 0
				}	
			}
			
			vrdsr = funcionario.dsrRemunerado
			
			def dados = Dados.findAllByFuncionarioAndDtBat(funcionario, dtBat)
			dados.each { d ->
				if (d.justificativa1 == null) {
					d.justificativa1 = justificativa
					d.ocorrencia1 = null
				} else if (d.justificativa2 == null) {
					d.justificativa2 = justificativa
					d.ocorrencia2 = null
				} else if (d.justificativa3 == null) {
					d.justificativa3 = justificativa
					d.ocorrencia3 = null
				}  else if (d.justificativa4 == null) {
					d.justificativa4 = justificativa
					d.ocorrencia4 = null
				}  else if (d.justificativa5 == null) {
					d.justificativa5 = justificativa
					d.ocorrencia5 = null
				}  else if (d.justificativa6 == null) {
					d.justificativa6 = justificativa
					d.ocorrencia6 = null
				}
				d.nrQtdeChEfetivada = d.nrQtdeChPrevista
				d.save()
			}
			
			if (idLimpaDescontos.equals("N")) {
				movimento?.idSituacaoAbono = "B"
			} else if (idLimpaDescontos.equals("S")) {
				if (tipoAbono.equals("0")) {
					movimento?.idSituacaoAbono = "A"
					movimento?.nrQtdeHr = 0
				} else {
					movimento?.idSituacaoAbono = "N"
					movimento?.nrQtdeHr = (movimento?.nrQtdeHr - quantidadeHoras)
				}
			}
			
			if (idDebitaBh.equals("S")) {
				if (ocorrencia != null) {
					OcorrenciasBh obh = new OcorrenciasBh()
					obh.funcionario = funcionario
					obh.dtBat = dtBat
					obh.ocorrencias = ocorrencia
					obh.nrQtdeHr = quantidadeHoras
					obh.idTpLanc = "A"
					obh.idTpOcorr = "D"
					obh.observacao = observacao
					obh.save()
				}
			}
			
			// ao abonar se o funcionario tiver adcional noturno verificar;
			
			def paramSistemaFilial = ParamSistemaFilial.findByFilial(filial)
			def paramSistema = paramSistemaFilial.paramSistema
			eventoAdc = paramSistema.evAdicionalNoturno
			statusAdc = paramSistema.adicionalNoturnoNormal
			eventoDsrp = paramSistema.evDsr
			eventoDsrd = paramSistema.evDsrd
			eventoFtotal = paramSistema.evFaltaTotal
			
			cont = 0
			
			if (idAdcionalnot.equals("S")) {
				
				def consultaAdicional  = "select b.seq_horario,c.hrbat "
					consultaAdicional += "from dados b "
					consultaAdicional += "left join bathorario c on b.id_perfil_horario = c.perfilhorario_id "
					consultaAdicional += "where b.seq_func = " + seqFunc
					consultaAdicional += "and b.dt_bat = '" + new SimpleDateFormat("yyyy-MM-dd").format(dtBat) + "' "
					consultaAdicional += "and c.seqbathorario in (select min(c.seqbathorario) "
					consultaAdicional += "						from dados b "
					consultaAdicional += "						left join bathorario c on b.id_perfil_horario = c.perfilhorario_id "
					consultaAdicional += "						where b.seq_func = " + seqFunc
					consultaAdicional += "						  and b.dt_bat = '" + new SimpleDateFormat("yyyy-MM-dd").format(dtBat) + "' ) "
					consultaAdicional += "union "
					consultaAdicional += "select b.seq_horario,c.hrbat "
					consultaAdicional += "from dados b "
					consultaAdicional += "left join bathorario c on b.id_perfil_horario = c.perfilhorario_id "
					consultaAdicional += "where b.seq_func = " + seqFunc
					consultaAdicional += "and b.dt_bat = '" + new SimpleDateFormat("yyyy-MM-dd").format(dtBat) + "' "
					consultaAdicional += "and c.seqbathorario in (select max(c.seqbathorario) "
					consultaAdicional += "						from dados b "
					consultaAdicional += "						left join bathorario c on b.id_perfil_horario = c.perfilhorario_id "
					consultaAdicional += "						where b.seq_func = " + seqFunc
					consultaAdicional += "						  and b.dt_bat = '" + new SimpleDateFormat("yyyy-MM-dd").format(dtBat) + "' ) "
					
				consultaAdicional.each {
					if (hora1 != null) {
						hora1 = it.hrbat
					} else {
						hora2 = it.hrbat
					}
				}

				def template = new JdbcTemplate(dataSource)
				horaAdc = template.queryForObject("select calcula_adcionalnot(${hora1},0,${hora2},1320,300,${statusAdc})", Integer.class)
				
				if (horaAdc > 0) {
					Ocorrencias ocorrenciaAdc = Ocorrencias.get(eventoAdc.toLong())
					Movimentos.findAllByFuncionarioAndOcorrenciaAndDtBat(funcionario, ocorrenciaAdc, dtBat) {
						it.delete()
					}
					
					Movimentos m = new Movimentos()
					m.funcionario = funcionario
					m.dtBat = dtBat
					m.ocorrencia = ocorrenciaAdc
					m.nrQtdeHr = horaAdc
					m.idSituacaoAbono = "S"
					m.save()
				}
				
			}
			
			Abonos.findAll("from Abonos where funcionario = :funcionario and :data between dtInicio and dtFim", 
				[funcionario: funcionario, data: dtBat]).each {
				
				it.delete()
			}
			
			horasAbono = horasAbono + quantidadeHorasDias
			dtBat++
			
		}
		
		Abonos abono = new Abonos()
		abono.funcionario = funcionario
		abono.dtInicio = dtIni
		abono.dtFim = dtFim
		abono.qtdadeDias = quantidadeDias
		abono.ocorrencias = ocorrenciaPrimeiroDia
		abono.idSituacaoAbono = idTipo
		abono.nrQtdeHrJustif = horasAbono
		abono.justificativas = justificativa
		abono.dtJustificativa = new Date()
		abono.ocorrenciasOld = ocorrencia
		abono.cidId = cidId
		abono.obsJustificativa = observacao
		abono.save()
		
		return retorno
		
	}
	
	
}


