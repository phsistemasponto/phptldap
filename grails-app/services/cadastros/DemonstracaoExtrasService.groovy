package cadastros

import java.math.*
import java.text.*

import funcoes.Calculos
import groovy.sql.Sql


class DemonstracaoExtrasService {

	def dataSource
	boolean transactional = true
	
	def gerarDemonstracaoExtras(whereExpression, filial, periodoFilial, usuario) {
		
		def results = []
		
		Sql consulta = new Sql(dataSource)
		
		def dtInicial = periodoFilial.periodo.dtIniPr
		def dtFinal = periodoFilial.periodo.dtFimPr
		def pagaAdm = filial.idPgHrextAdm
		
		String consultaFuncionario = "select id from funcionario f where (1=1) and " + whereExpression
		consultaFuncionario = consultaFuncionario.replace("''", "'")
		println "### " + consultaFuncionario 
		def funcionarios = consulta.rows(consultaFuncionario)
		funcionarios.each { f ->
			
			Funcionario func = Funcionario.get(f.id)
			
			// verificar se funcionario ja ta na demonstracao das extras
			DemonstracaoExtras de = DemonstracaoExtras.findByFuncionarioAndPeriodo(func, periodoFilial.periodo)
			
			if (de != null) {
				
				results.add([seqFunc: f.id, 
							 nomeFunc: f.nomFunc, 
							 bancoAnterior: de.bancoAnt,
							 compensacao: de.comp,
							 extras1: de.extra1,
							 extras2: de.extra2,
							 preCritica: de.preCritica,
							 valorReal: de.getVrRealExibicao(),
							 valorReceber: de.getVrReceberExibicao() ])
				
			} else {
				
				def critica = "F"
				def valorReceberReal = 0
				def valorReceberTotal = 0
				def valorReceberExtras1 = 0
				def valorReceberExtras2 = 0
				
				def valorExtras1 = 0
				def valorExtras2 = 0
				def valorExtras1Total = 0
				def valorExtras2Total = 0
				
				def valorSaldoExtras1 = 0
				def valorSaldoExtras2 = 0
				
				def pagaExtras = false;
				def existeExtras = false;
				
				def consultaExtra1 = """
					   select f.id, f.vr_sal, o.id as ocid, o.formula_ocor, sum(m.nr_qtde_hr) as soma, 
							  retornavalor(o.formula_ocor, cast(f.vr_sal as numeric), sum(m.nr_qtde_hr)) as valor
					   from movimentos m
					   inner join funcionario f on m.seq_func = f.id
					   inner join ocorrencias o on m.seq_ocor = o.id and o.id_tp_ocor = 'P' and o.tipohoraextras_id is not null
					   where m.dt_bat between ${dtInicial} and ${dtFinal}
					   	 and f.id = ${func.id}
						 and m.seq_ocor <> 32
						 and o.indice_ext = 1
					   group by f.id, f.vr_sal, o.id, o.formula_ocor
					   """
				def resultExtras1 = consulta.rows(consultaExtra1)
				resultExtras1.each { r->
					valorReceberTotal += r.valor 
					valorExtras1Total += r.soma
				}
				
				def consultaExtra2 = """
					   select f.id, f.vr_sal, o.id, o.formula_ocor, sum(m.nr_qtde_hr), 
							  retornavalor(o.formula_ocor, cast(f.vr_sal as numeric), sum(m.nr_qtde_hr))
					   from movimentos m
					   inner join funcionario f on m.seq_func = f.id
					   inner join ocorrencias o on m.seq_ocor = o.id and o.id_tp_ocor = 'P' and o.tipohoraextras_id is not null
					   where m.dt_bat between ${dtInicial} and ${dtFinal}
					   	 and f.id = ${func.id}
						 and m.seq_ocor <> 32
						 and o.indice_ext = 2
					   group by f.id, f.vr_sal, o.id, o.formula_ocor
					   """
				 def resultExtras2 = consulta.rows(consultaExtra2)
				 resultExtras2.each { r->
					 valorReceberTotal += r[5]
					 valorExtras2Total += r[4]
				 }
				 
				 
				 
				 def consultaSaldoBancoAnterior = """
			 					 select sum(s.vr_sld_atual) as soma
								 from saldo_bh s
								 inner join periodo_filial pf on s.seq_pr = pf.id
								 inner join periodo p on pf.periodo_id = p.id
								 where s.seq_func = ${func.id} and p.dt_ini_pr <
									 (select pi.dt_ini_pr from periodo_filial pfi
									 inner join periodo pi on pfi.periodo_id = pi.id
									 where pfi.id = ${periodoFilial.id})
			 					 			"""
				def valorBancoAnterior = consulta.rows(consultaSaldoBancoAnterior)[0].soma
				if (valorBancoAnterior == null) {
					valorBancoAnterior = 0
				}
				
				def consultaCompensacao = """
							   SELECT sum(CASE o.id_tp_ocorr WHEN 'C' THEN o.NR_QTDE_HR ELSE 0 END)
									- sum(CASE o.id_tp_ocorr WHEN 'D' THEN o.NR_QTDE_HR ELSE 0 END) AS NR_QTDE_HR
							   FROM ocorrencias_bh o
							   WHERE o.seq_func = ${func.id}
								   AND o.dt_bat BETWEEN ${dtInicial} and ${dtFinal}
									  """
				
				def aComp = 0
				def comp = consulta.rows(consultaCompensacao)[0].nr_qtde_hr
				if (comp == null) {
					comp = 0
				}
				
				if (comp == null) {
					comp = 0
				} else if (comp != 0) {
					aComp = comp
				}
				
				if (aComp < 1) {
					comp = comp * (-1)
					aComp = aComp * (-1)
				}
				
				if (valorBancoAnterior < 0) {
					comp = comp + (valorBancoAnterior * (-1))
				}
				
				if(valorBancoAnterior > comp) {
					pagaExtras = true
				}
				
				
				def consultaExtrasFuncionarioTipo1 = """
					select b.seq_func, a.vr_sal, b.seq_ocor, d.formula_ocor, d.dsc_ocor, d.indice_ext, sum(nr_qtde_hr) as total,
						   retornavalor(d.formula_ocor, cast(a.vr_sal as numeric), sum(b.nr_qtde_hr)) as valorTotal
					from movimentos b
					inner join funcionario a on  b.seq_func = a.id
					inner join ocorrencias d on (b.seq_ocor = d.id) and (d.id_tp_ocor = 'P') and (d.tipohoraextras_id is not null)
					where b.dt_bat between ${dtInicial} and ${dtFinal} 
					  and b.seq_ocor <> 32
					  and b.seq_func = ${func.id}
					  and d.indice_ext = 1
					  group by b.seq_func, a.vr_sal, b.seq_ocor, d.formula_ocor, d.dsc_ocor, d.indice_ext
					  order by b.seq_Func, d.indice_ext, b.seq_ocor
													"""
				def resultExtrasFuncionarioTipo1 = consulta.rows(consultaExtrasFuncionarioTipo1)
				resultExtrasFuncionarioTipo1.each { r->
					
					if (!pagaExtras) {
						
						valorExtras1 += r[6]
						valorReceberReal += r[7]
						
						if (pagaExtras) {
							valorSaldoExtras1 += r[6]
							valorReceberExtras1 += r[7]
						} else if (comp > r[6]) {
							valorSaldoExtras1 = 0
							valorReceberExtras1 = 0
							comp = comp - r[6]
							critica = 1
						} else if ((comp > 0) && (comp < r[6])) {
							valorSaldoExtras1 += r[6] - comp
							valorReceberExtras1 += Calculos.retornarValor(r[3], r[1], r[6]-comp, 0)
							comp = 0
							critica = 1
						} else {
							valorSaldoExtras1 += r[6]
							valorReceberExtras1 += r[7]
						}
						
						existeExtras = true
						
					} else {
					
						valorExtras1 += r[6]
						valorReceberReal += r[7]
						
						if (valorBancoAnterior > comp) {
							valorReceberExtras1 += r[7]
							comp = 0
							critica = 1
						} else {
							valorSaldoExtras1 = 0
							critica = 1
							existeExtras = true
						}
					
					}
					 
				}
				
				
				def consultaExtrasFuncionarioTipo2 = """
					select b.seq_func, a.vr_sal, b.seq_ocor, d.formula_ocor, d.dsc_ocor, d.indice_ext, sum(nr_qtde_hr) as total,
						   retornavalor(d.formula_ocor, cast(a.vr_sal as numeric), sum(b.nr_qtde_hr)) as valorTotal
					from movimentos b
					inner join funcionario a on  b.seq_func = a.id
					inner join ocorrencias d on (b.seq_ocor = d.id) and (d.id_tp_ocor = 'P') and (d.tipohoraextras_id is not null)
					where b.dt_bat between ${dtInicial} and ${dtFinal} 
					  and b.seq_ocor <> 32
					  and b.seq_func = ${func.id}
					  and d.indice_ext = 2
					  group by b.seq_func, a.vr_sal, b.seq_ocor, d.formula_ocor, d.dsc_ocor, d.indice_ext
					  order by b.seq_Func, d.indice_ext, b.seq_ocor
													"""
				def resultExtrasFuncionarioTipo2 = consulta.rows(consultaExtrasFuncionarioTipo2)
				resultExtrasFuncionarioTipo2.each { r->
					
					valorSaldoExtras2 += r[6]
					valorReceberExtras2 += r[7]
					valorReceberReal += r[7]
					existeExtras = true
					
				}
				
			}
								  
		}
		
		
		
	}
	
	
}


