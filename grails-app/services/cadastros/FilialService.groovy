package cadastros

import acesso.Usuario
import acesso.UsuarioFilial

class FilialService {
	
	def springSecurityService
	
	def filiaisDoUsuario() {
		Usuario usuario = springSecurityService.currentUser
		List<UsuarioFilial> userFil = usuario.getUsuarioFiliais()
		List<Filial> filiais = new ArrayList<Filial>();
		userFil.each { uf ->
			filiais.add(uf.filial)
		}
		filiais
	}
	
}






