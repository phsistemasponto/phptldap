package cadastros



class PeriodoService {
	def springSecurityService
	
	boolean transactional = true
	
	def incluirPeriodoFilial(params, periodoParametro){
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialId.getClass()) }
		def filiaisArray = isArray ? params.filialId : [params.filialId]
		
		PeriodoFilial periodoFilial
		Filial filial
		filiaisArray.eachWithIndex { it, i ->
			
			def temPeriodo = isArray ? params.temFilialPeriodo[i] : params.temFilialPeriodo
			
			if (temPeriodo=='true') {
				filial = Filial.get(it)
				periodoFilial = new PeriodoFilial( periodo: periodoParametro, filial: filial, status: 'A', version: 0)
				if (!periodoFilial.save()){
					println('erros!')
					periodoFilial.errors.each { item ->
						println(item)
					}
				} else {
					println('salvo!!')
				}
			}
		}
		
	}
	
	
	def AtualizarPeriodoFilial(params, periodoParametro){
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialId.getClass()) }
		def filiaisArray = isArray ? params.filialId : [params.filialId]
		
		def periodoFilial
		Filial filial
		filiaisArray.eachWithIndex { it, i ->
			
			filial = Filial.get(it)
			periodoFilial = PeriodoFilial.findByFilialAndPeriodo(filial, periodoParametro)

			def temPeriodo = isArray ? params.temFilialPeriodo[i] : params.temFilialPeriodo
			
			if (temPeriodo=='true') {
				
				if (periodoFilial){
					periodoFilial.status=params.statusPeriodo[i]
				} else {
					periodoFilial = new PeriodoFilial( periodo: periodoParametro, filial: filial, status: 'A')
				}
				
				if (!periodoFilial.save()){
					periodoFilial.errors.each { item ->
						println(item)
					}
				}

			} else {
				if (periodoFilial){
					periodoFilial.delete()
				}
			}
		}
		
	}

	
}






