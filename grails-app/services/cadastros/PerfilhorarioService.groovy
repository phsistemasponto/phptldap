package cadastros

import cadastros.Perfilhorario;
import cadastros.Tipotolerancia;
import cadastros.Bathorario
import funcoes.Horarios

class PerfilhorarioService {

	boolean transactional = true
	Perfilhorario salvarPerfilhorario(params) {
		
		
		Long somaCargaHoraria = 0
		Boolean primeiraVerificacao = true
		int quantidadeBatidas = 0
		
		Long hora1 = 0
		Long hora2 = 0
		String vira = 'N'

		// verificacao de virada
		Long horaAnterior = 0
		Long horaAtual = 0
		if (params.seqbathorario.size()>1){
			horaAtual = Horarios.calculaMinutos(params.hrbat[0])
		} else {
			horaAtual = params.hrbat
		}
		

		

		String dscPerfilString = ''
		if (params.seqbathorario.size()>0){
			if (params.seqbathorario.size()>1){
				for (x in 0..params.seqbathorario.size()-1){
					dscPerfilString=dscPerfilString+params.hrbat[x]+' '
					
					if (horaAtual<horaAnterior){
						vira='S'
					} 
					
					if (!primeiraVerificacao){
						horaAnterior = horaAtual
						horaAtual = Horarios.calculaMinutos(params.hrbat[x])
					} else {
						primeiraVerificacao=false
					}
					
					
					if (quantidadeBatidas == 0) {
						hora1 = Horarios.calculaMinutos(params.hrbat[x])
						quantidadeBatidas = 1
					} else if (quantidadeBatidas == 1) {
						hora2 = Horarios.calculaMinutos(params.hrbat[x])
						quantidadeBatidas = 2
						quantidadeBatidas = 0
						if (hora2<hora1){
							somaCargaHoraria = somaCargaHoraria+(hora2+1440)-hora1
							vira = 'S'
						} else {
							somaCargaHoraria = somaCargaHoraria+(hora2-hora1)
						}
					}
		
					
				}
			} else {
				dscPerfilString=dscPerfilString+params.hrbat
			}
		}
		
		if (horaAtual<horaAnterior){
			vira='S'
		}

				
		Perfilhorario perfilhorario = new Perfilhorario()
		perfilhorario.tipotolerancia = Tipotolerancia.get(Integer.parseInt(params.tipotolerancia.id))
		perfilhorario.dscperfil = dscPerfilString
		perfilhorario.cargahor = somaCargaHoraria  
		perfilhorario.idvira = vira 
		
		if (!perfilhorario.save()){
			perfilhorario.errors.each {
				println(it)
			}
			return perfilhorario
		} else {
		}
		
		
		if (params.seqbathorario.size()>1){
			
			for (x in 0..params.seqbathorario.size()-1){

				perfilhorario.addToBathorarios(
					new Bathorario(
						seqbathorario: params.seqbathorario[x],
						hrbat: Horarios.calculaMinutos(params.hrbat[x]),
						perfilhorario: perfilhorario,
						idbat: (params.idbat[x]=='Entrada')?'E':'S'
					)
				)
			}


		} else if (params.seqbathorario.size()==1){
			perfilhorario.addToBathorarios(
				new Bathorario(
					seqbathorario: params.seqbathorario,
					hrbat: Horarios.calculaMinutos(params.hrbat),
					perfilhorario: perfilhorario,
					idbat: (params.idbat=='Entrada')?'E':'S'
				)
			)
		}
		
		perfilhorario.save()
		perfilhorario
		
	}
	
	Perfilhorario atualizarPerfilhorario(params){
		
		Perfilhorario perfilhorario = Perfilhorario.get(Integer.parseInt(params.id))
		
		def bathorarios = Bathorario.findAll ('from Bathorario where perfilhorario=:perfilhorario',[perfilhorario: perfilhorario])
		bathorarios.each() {
			Bathorario bathorario = Bathorario.get(it.id)
			perfilhorario.removeFromBathorarios(bathorario)
			bathorario.delete()
		}
		

		
		
		Long somaCargaHoraria = 0
		Boolean primeiraVerificacao = true
		int quantidadeBatidas = 0
		
		Long hora1 = 0
		Long hora2 = 0
		String vira = 'N'

//		verificacao de virada
		Long horaAnterior = 0
		Long horaAtual = 0
		if (params.seqbathorario.size()>1){
			horaAtual = Horarios.calculaMinutos(params.hrbat[0])
		} else {
			horaAtual = params.hrbat
		}
		

		

		String dscPerfilString = ''
		if (params.seqbathorario.size()>0){
			if (params.seqbathorario.size()>1){
				for (x in 0..params.seqbathorario.size()-1){
					dscPerfilString=dscPerfilString+params.hrbat[x]+' '
					
					if (horaAtual<horaAnterior){
						vira='S'
					} 
					
					if (!primeiraVerificacao){
						horaAnterior = horaAtual
						horaAtual = Horarios.calculaMinutos(params.hrbat[x])
					} else {
						primeiraVerificacao=false
					}
					
					
					if (quantidadeBatidas == 0) {
						hora1 = Horarios.calculaMinutos(params.hrbat[x])
						quantidadeBatidas = 1
					} else if (quantidadeBatidas == 1) {
						hora2 = Horarios.calculaMinutos(params.hrbat[x])
						quantidadeBatidas = 2
						quantidadeBatidas = 0
						if (hora2<hora1){
							somaCargaHoraria = somaCargaHoraria+(hora2+1440)-hora1
							vira = 'S'
						} else {
							somaCargaHoraria = somaCargaHoraria+(hora2-hora1)
						}
					}
		
					
				}
			} else {
				dscPerfilString=dscPerfilString+params.hrbat
			}
		}
		
		if (horaAtual<horaAnterior){
			vira='S'
		}

				
		perfilhorario.tipotolerancia = Tipotolerancia.get(Integer.parseInt(params.tipotolerancia.id))
		perfilhorario.dscperfil = dscPerfilString
		perfilhorario.cargahor = somaCargaHoraria  
		perfilhorario.idvira = vira 
		
		if (!perfilhorario.save()){
			perfilhorario.errors.each {
				println(it)
			}
			return perfilhorario
		} else {
		}
		
		
		if (params.seqbathorario.size()>1){
			
			for (x in 0..params.seqbathorario.size()-1){

				perfilhorario.addToBathorarios(
					new Bathorario(
						seqbathorario: params.seqbathorario[x],
						hrbat: Horarios.calculaMinutos(params.hrbat[x]),
						perfilhorario: perfilhorario,
						idbat: (params.idbat[x]=='Entrada')?'E':'S'
					)
				)
			}


		} else if (params.seqbathorario.size()==1){
			perfilhorario.addToBathorarios(
				new Bathorario(
					seqbathorario: params.seqbathorario,
					hrbat: Horarios.calculaMinutos(params.hrbat),
					perfilhorario: perfilhorario,
					idbat: (params.idbat=='Entrada')?'E':'S'
				)
			)
		}
		
		perfilhorario.save()
		perfilhorario
				
		
	}
}


