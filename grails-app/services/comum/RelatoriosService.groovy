package comum
import java.sql.Blob

import org.springframework.context.ApplicationContext
import org.springframework.transaction.annotation.Transactional

import acesso.*
import cadastros.*

@Transactional
class RelatoriosService {
	
	
	@Transactional
	def montaLogomarca(Usuario usuario, ApplicationContext appContext) {
		
		InputStream logomarca
		List<Filial> filiais = usuario.getFiliais()
		Empresa empresa = filiais[0].empresa
		Set<EmpresaFoto> foto = empresa.foto
		if (foto != null && !foto.isEmpty()) {
			EmpresaFoto empresaFoto
			foto.each { empresaFoto = it }
			Blob conteudo = empresaFoto.conteudo
			byte[] image = conteudo.getBytes(1l, conteudo.length().toInteger())
			logomarca = new ByteArrayInputStream(image)
		} else {
			def baseFolder = appContext.getResource("/").getFile().toString()
			def imagesPath = baseFolder + File.separator + "images" + File.separator + "ph.gif"
			File file = new File(imagesPath)
			logomarca = new FileInputStream(file)
		}
		
		logomarca
	}
				
}