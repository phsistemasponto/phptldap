package comum
import java.text.DecimalFormat

import acesso.*
import cadastros.*

class BuscaService {
	
	def springSecurityService
	def filtroPadraoService
	
	boolean transactional = true
	
	/*
	Caso o par�metro "buscaFilial=true", ser� feito a consulta utilizando o filtro por filial. 
	Caso o valor do id da filial seja maior que zero, ser� utilizado o id informado para filtragem, caso contr�rio,
	  todos os id's de filiais que o usu�rio tiver acesso
	*/
	def resultadoBuscaLov(Map parametros, Usuario usuario) {
			
		String classe = parametros.get("classeBusca") 
		String stringBusca = parametros.get("stringBusca")
		String campoId = parametros.get("campoId")
		String campoDescricao = parametros.get("campoDescricao")
		
		boolean filtraFiliaisUsuario = true
		if (parametros.get("filtraFiliaisUsuario")) {
			filtraFiliaisUsuario = new Boolean(parametros.get("filtraFiliaisUsuario")) 
		}
		
		boolean filtraFilialEspecifica = false
		if (parametros.get("filtraFilialEspecifica")) {
			filtraFilialEspecifica = new Boolean(parametros.get("filtraFilialEspecifica"))
		}
		
		String tipoJustificativaEspelhoPonto = parametros.get("tipoJustificativaEspelhoPonto") 
		String filialEspecifica = parametros.get("filialEspecifica")
		
		def classeInstanciada = new GroovyClassLoader().loadClass(classe)
        try {
			
			def stringSql = "from ${classe} c where upper(${campoDescricao}) like upper('${stringBusca}%') "
			def resultado

			if (classe.equals("cadastros.Filial")) {
				if (filtraFiliaisUsuario) {
					def filiais = usuario.getFiliais()
					stringSql += " and c in (:filiais) "
					resultado = classeInstanciada.findAll(stringSql, [filiais: filiais])
				} else {
					resultado = classeInstanciada.findAll(stringSql)
				}
				
			} else if (classe.equals("cadastros.Funcionario")) {
				def filiais = usuario.getFiliais()
				stringSql += " and c.filial in (:filiais) "
				
				def filtroSetoresDoUsuario = filtroPadraoService.montaFiltroSetoresDoUsuario()
				if (filtroSetoresDoUsuario) {
					stringSql = stringSql + " and c.setor.id in (" + filtroSetoresDoUsuario + ") "
				}
				
				def filtroGruposFuncionarios = filtroPadraoService.montaFiltroGruposFuncionariosDoUsuario()
				if (filtroGruposFuncionarios) {
					stringSql = stringSql + " and c.id in (" + filtroGruposFuncionarios + ") "
				}
				
				resultado = classeInstanciada.findAll(stringSql, [filiais: filiais])
			} else if (classe.equals("cadastros.Setor")) {
			
				def filiais = usuario.getFiliais()
				stringSql += " and c.filial in (:filiais) "
				
				if (filtraFilialEspecifica) {
					stringSql = stringSql + " and c.filial.id = " + filialEspecifica + " "
				}
				
				resultado = classeInstanciada.findAll(stringSql, [filiais: filiais])
			} else if (classe.equals("cadastros.Justificativas") 
					&& tipoJustificativaEspelhoPonto != null 
					&& !tipoJustificativaEspelhoPonto.trim().isEmpty()) {
					
				stringSql += " and c.idTpJust = :tipoJustificativaEspelhoPonto "
				resultado = classeInstanciada.findAll(stringSql, 
					[tipoJustificativaEspelhoPonto: tipoJustificativaEspelhoPonto, max: 100])
				
			} else {
	 			resultado = classeInstanciada.findAll(stringSql, [max: 100])
			}
             
            def resultadoMap = []
            resultado.each() {
                resultadoMap.add([id: it[campoId], descricao: it[campoDescricao]])
            }
            return resultadoMap
        } catch(Exception e){
            println(e)
			return []
        }
		
	}
	
	def buscaId(String classe, String campoId, String campoDescricao, String stringBusca, Usuario usuario) {
		def classeInstanciada = new GroovyClassLoader().loadClass(classe)
		def resultado
		try {
			def stringSql = "from ${classe} c where ${campoId} = ${stringBusca}"
			
			if (classe.equals("cadastros.Filial")) {
				def filiais = usuario.getFiliais()
				stringSql += " and c in (:filiais) "
				resultado = classeInstanciada.findAll(stringSql, [filiais: filiais])
			} else if (classe.equals("cadastros.Funcionario")) {
				def filiais = usuario.getFiliais()
				stringSql += " and c.filial in (:filiais) "
				resultado = classeInstanciada.findAll(stringSql, [filiais: filiais])
			} else {
				 resultado = classeInstanciada.findAll(stringSql, [max: 100])
			}
			
			return (resultado) ? [id: resultado[campoId], descricao: resultado[campoDescricao]] : []
		} catch(Exception e){
			println(e)
			return []
		}
	}
	
	def buscaIdPerfilHorario(String codigo) {
		try {
			def resultado = Perfilhorario.get(codigo.toLong())
			def	resultadoMap=[id: resultado.id, descricao: resultado.dscperfil, cargahor: resultado.cargahor, cargaHorariaHoras: resultado.cargaHorariaHoras]
			return resultadoMap
		} catch(Exception e){
			println(e)
			return null
		}
	}
	def resultadoBuscaLovPerfilHorario(String stringBusca) {
        try {
			

			def stringSql = "  "
			def resultado
	 		resultado = Perfilhorario.findAll("from Perfilhorario c where dscperfil like '${stringBusca}%'")	
             
            def resultadoMap = []
            resultado.each() {
                resultadoMap.add([id: it.id, descricao: it.dscperfil, cargahor: it.cargahor, cargaHorariaHoras: it.cargaHorariaHoras])
            }
            return resultadoMap
        } catch(Exception e){
            println(e)
			return []
        }
		
	}
	
	
// Filial	
	
	def buscaIdFilial(Long filialId){
		

		def resultado = Filial.get(filialId)
		
		return (resultado) ? [id: resultado.id, nome: resultado.dscFil] : []
	}
	
	
	def resultadoBuscaLovFilial(String stringBusca, String empresaId) {
		try {
		
			Empresa empresa = Empresa.get( empresaId.toLong() )
			def stringSql = "  "
			def resultado
			 resultado = Filial.findAll("from Filial f where f.dscFil like '${stringBusca}%' and f.empresa=:empresa",[empresa: empresa])
			 
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, nome: it.dscFil])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

// Fim filial
	
// Ocorrencia
	
	def buscaIdOcorrencia(Long ocorrenciaId){
		def resultado = Ocorrencias.get(ocorrenciaId)
		return (resultado) ? [id: resultado.id, nome: resultado.dscOcor] : []
	}
	
	
	def resultadoBuscaLovOcorrencia(String stringBusca) {
		try {
		
			def resultado = Ocorrencias.findAll("from Ocorrencias o where o.dscOcor like '${stringBusca}%'")
			 
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, nome: it.dscOcor])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

// Fim Ocorrencia
	
// Tipo hora extra
	
	def buscaIdTipoHora(Long tipoHoraId, String suprimir){
		def suprimirLong = new ArrayList()
		def remove = suprimir.split(",")
		remove.each {
			suprimirLong.add(it.toLong())
		}
		def resultado = Tipohoraextras.find(
			'from Tipohoraextras where id=:id and id not in (:suprimir)', [id: tipoHoraId, suprimir: suprimirLong])
		return (resultado) ? [id: resultado.id, nome: resultado.dscTpHorExt] : []
	}
	
	
	def resultadoBuscaLovTipoHora(String stringBusca, String suprimir) {
		try {
			
			def suprimirLong = new ArrayList()
			def remove = suprimir.split(",")
			remove.each {
				suprimirLong.add(it.toLong())
			}
		
			def resultado = Tipohoraextras.findAll("from Tipohoraextras t where t.dscTpHorExt like '${stringBusca}%' and t.id not in (:suprimir)",
				[suprimir: suprimirLong])
			 
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, nome: it.dscTpHorExt])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

// Fim Tipo hora extra
	
// Classificacao
	
	def buscaIdClassificacao(Long classificacaoId, String suprimir){
		def suprimirLong = new ArrayList()
		def remove = suprimir.split(",")
		remove.each {
			suprimirLong.add(it.toLong())
		}
		def resultado = Classificacao.find(
			'from Classificacao where id=:id and id not in (:suprimir)', [id: classificacaoId, suprimir: suprimirLong])
		return (resultado) ? [id: resultado.id, nome: resultado.dscClass] : []
	}
	
	
	def resultadoBuscaLovClassificacao(String stringBusca, String suprimir) {
		try {
			
			def suprimirLong = new ArrayList()
			def remove = suprimir.split(",")
			remove.each {
				suprimirLong.add(it.toLong())
			}
		
			def resultado = Classificacao.findAll("from Classificacao c where c.dscClass like '${stringBusca}%' and c.id not in (:suprimir)", 
				[suprimir: suprimirLong])
			 
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, nome: it.dscClass])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

// Fim Classificacao
	
// Setor
	
	def buscaIdSetor(Long setorId){
		def resultado = Setor.get(setorId)
		return (resultado) ? [id: resultado.id, nome: resultado.dscSetor] : []
	}
	
	def buscaIdSetorComFilial(Long setorId, Long filialId) {
		def resultado = Setor.find("from Setor s where s.id = ${setorId} and s.filial.id = ${filialId} ")    
		return (resultado) ? [id: resultado.id, descricao: resultado.dscSetor] : [] 
	}
	
	
	def resultadoBuscaLovSetor(String stringBusca, String filialId) {
		try {
		
			def stringSql = "  "
			def resultado
			resultado = Setor.findAll("from Setor s where s.dscSetor like '${stringBusca}%'")
			 
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, nome: it.dscSetor])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

	
// Fim setor
	
// Beneficio
	
	def buscaIdBeneficio(Long beneficioId){
		def resultado = Beneficio.get(beneficioId)
		return (resultado) ? [id: resultado.id, nome: resultado.descricao, valor: new DecimalFormat("0.00").format(resultado.valorAtivo.valor)] : []
	}
	
	
	def resultadoBuscaLovBeneficio(String stringBusca) {
		try {
		
			def resultado = Beneficio.findAll("from Beneficio o where o.descricao like '${stringBusca}%'")
			 
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, nome: it.descricao, valor: new DecimalFormat("0.00").format(it.valorAtivo.valor) ])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

// Fim Beneficio
	
	
// Tipo de beneficio
	
	def buscaIdTipoBeneficio(Long tipoBeneficioId){
		def resultado = TipoDeBeneficio.get(tipoBeneficioId)
		return (resultado) ? [id: resultado.id, nome: resultado.descricao] : []
	}
	
	
	def resultadoBuscaLovTipoBeneficio(String stringBusca) {
		try {
		
			def resultado = TipoDeBeneficio.findAll("from TipoDeBeneficio o where o.descricao like '${stringBusca}%'")
			 
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, nome: it.descricao])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

// Fim Tipo de beneficio
	
	
// Funcionario
	
	def buscaMatriculaFuncionario(String matricula){
		

		def resultado = Funcionario.findByMrtFunc(matricula)
		
		return (resultado) ? [id: resultado.id, matricula: resultado.mrtFunc, nome: resultado.nomFunc] : []
	}
	
	
	def resultadoBuscaLovFuncionario(String stringBusca, Usuario usuario) {
		try {
		
			def stringSql = "from Funcionario f where f.nomFunc like '${stringBusca}%' "
			
			def filiais = usuario.getFiliais()
			stringSql += " and c.filial in (:filiais) "
			
			def resultado
			resultado = Funcionario.findAll(stringSql)
			
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, matricula: it.mrtFunc, nome: it.nomFunc])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

	
// fim Funcionario
	
// FiltroUsuario
	
	def buscaIdFiltroUsuario(Long filtroUsuarioId, Usuario usuario){
		

		def resultado = FiltroUsuario.get(filtroUsuarioId)
		
		if (!resultado.usuario.id.equals(usuario.id)) {
			resultado = null
		}
		
		return (resultado) ? [id: resultado.id, 
								nome: resultado.nomeFiltro,
								filtroFilial: resultado.filtroFilial,
								filtroCargo: resultado.filtroCargo,
								filtroDepartamento: resultado.filtroDepartamento,
								filtroFuncionario: resultado.filtroFuncionario,
								filtroTipoFuncionario: resultado.filtroTipoFuncionario,
								filtroHorario: resultado.filtroHorario,
								filtroSubUniorg: resultado.filtroSubUniorg] : []
	}
	
	
	def resultadoBuscaLovFiltroUsuario(String stringBusca, Usuario usuario) {
		try {
		
			def stringSql = "  "
			def resultado
			resultado = FiltroUsuario.findAll("from FiltroUsuario f where f.nomeFiltro like '${stringBusca}%' and f.usuario=:usuario",[usuario: usuario])
			 
			def resultadoMap = []
			resultado.each() {
				resultadoMap.add([id: it.id, 
								nome: it.nomeFiltro,
								filtroFilial: it.filtroFilial,
								filtroCargo: it.filtroCargo,
								filtroDepartamento: it.filtroDepartamento,
								filtroFuncionario: it.filtroFuncionario,
								filtroTipoFuncionario: it.filtroTipoFuncionario,
								filtroHorario: it.filtroHorario,
								filtroSubUniorg: it.filtroSubUniorg])
			}
			return resultadoMap
		} catch(Exception e){
			println(e)
			return []
		}
		
	}

	
// Fim FiltroUsuario
	
				
}
