package comum
import acesso.*
import cadastros.*

class DetectDataBaseService {
	
	def dataSource
	
	boolean isPostgreSql() {
		String driver = dataSource.targetDataSource.driverClassName
		return driver.contains("postgresql")
	}
	
	boolean isOracle() {
		String driver = dataSource.targetDataSource.driverClassName
		return driver.contains("oracle")
	}
				
}
