package comum
import acesso.*
import cadastros.*

class FiltroPadraoService {
	
	def springSecurityService 
	
	def montaFiltroPadrao(Map parametros) {
		def consulta = "from Funcionario f where 1 = 1 "
		
		def filtroFiliaisDoUsuario = montaFiltroFiliaisDoUsuario()
		if (filtroFiliaisDoUsuario) {
			consulta = consulta + " and f.filial.id in (" + filtroFiliaisDoUsuario + ") "
		}
		
		def filtroSetoresDoUsuario = montaFiltroSetoresDoUsuario()
		if (filtroSetoresDoUsuario) {
			consulta = consulta + " and f.setor.id in (" + filtroSetoresDoUsuario + ") "
		}
		
		def filtroGruposFuncionarios = montaFiltroGruposFuncionariosDoUsuario()
		if (filtroGruposFuncionarios) {
			consulta = consulta + " and f.id in (" + filtroGruposFuncionarios + ") "
		}
		
		parametros.entrySet().findAll {
			it.key.startsWith("filtroPadrao_")
		}.each { 
			if (it.value != null && !it.value.trim().equals("")) {
			
				switch (it.key) {
					case "filtroPadrao_FilialId":
						consulta = consulta + " and f.filial.id in (" + it.value + ") "
						break
					case "filtroPadrao_CargoId":
						consulta = consulta + " and f.cargo.id in (" + it.value + ") "
						break
					case "filtroPadrao_DepartamentoId":
						consulta = consulta + " and f.setor.id in (" + it.value + ") "
						break
					case "filtroPadrao_FuncionarioId":
						consulta = consulta + " and f.id in (" + it.value + ") "
						break
					case "filtroPadrao_TipoFuncionarioId":
						consulta = consulta + " and f.tipoFunc.id in (" + it.value + ") "
						break
					case "filtroPadrao_HorarioId":
						consulta = consulta + " and f.horario.id in (" + it.value + ") "
						break
					case "filtroPadrao_SubUniOrgId":
						consulta = consulta + " and f.uniorg.id in (" + it.value + ") "
						break
					case "filtroPadrao_NaoListaDemitidos":
						def naoListaDemitidos = it.value.toBoolean()
						if (naoListaDemitidos) {
							consulta = consulta + " and f.dtResc is null "
						}
				}
				
			}
		}
		
		return consulta
	}
	
	def montaFiltroPadraoSqlNativo(Map parametros) {
		def consulta = " "
		
		def filtroFiliaisDoUsuario = montaFiltroFiliaisDoUsuario()
		if (filtroFiliaisDoUsuario) {
			consulta = consulta + " and f.filial_id in (" + filtroFiliaisDoUsuario + ") "
		}
		
		def filtroSetoresDoUsuario = montaFiltroSetoresDoUsuario()
		if (filtroSetoresDoUsuario) {
			consulta = consulta + " and f.setor_id in  (" + filtroSetoresDoUsuario + ") "
		}
		
		def filtroGruposFuncionarios = montaFiltroGruposFuncionariosDoUsuario()
		if (filtroGruposFuncionarios) {
			consulta = consulta + " and f.id in (" + filtroGruposFuncionarios + ") "
		}
		
		parametros.entrySet().findAll {
			it.key.startsWith("filtroPadrao_")
		}.each {
			if (it.value != null && !it.value.trim().equals("")) {
			
				switch (it.key) {
					case "filtroPadrao_FilialId":
						consulta = consulta + " and f.filial_id in (" + it.value + ") "
						break
					case "filtroPadrao_CargoId":
						consulta = consulta + " and f.cargo_id in (" + it.value + ") "
						break
					case "filtroPadrao_DepartamentoId":
						consulta = consulta + " and f.setor_id in  (" + it.value + ") "
						break
					case "filtroPadrao_FuncionarioId":
						consulta = consulta + " and f.id in (" + it.value + ") "
						break
					case "filtroPadrao_TipoFuncionarioId":
						consulta = consulta + " and f.tipo_func_id in (" + it.value + ") "
						break
					case "filtroPadrao_HorarioId":
						consulta = consulta + " and f.horario_id in (" + it.value + ") "
						break
					case "filtroPadrao_SubUniOrgId":
						consulta = consulta + " and f.uniorg_id in (" + it.value + ") "
						break
					case "filtroPadrao_NaoListaDemitidos":
						def naoListaDemitidos = it.value.toBoolean()
						if (naoListaDemitidos) {
							consulta = consulta + " and f.dt_resc is null "
						}
				}
			}
		}
		
		return consulta
	}
	
	def montaFiltroFiliaisDoUsuario() {
		Usuario user = springSecurityService.currentUser
		String filtro = " "
		
		if (user.filiais) {
			user.filiais.each {
				filtro += it.id + ","
			}
		}
		
		filtro.substring(0, filtro.length()-1).trim()
	}
	
	def montaFiltroSetoresDoUsuario() {
		Usuario user = springSecurityService.currentUser
		String filtro = " "
		
		if (user.getUsuarioSetores()) {
			user.getUsuarioSetores().each {
				filtro += it.setor.id + ","
			}
		}
		
		filtro.substring(0, filtro.length()-1).trim()
	}
	
	def montaFiltroGruposFuncionariosDoUsuario() {
		Usuario user = springSecurityService.currentUser
		String filtro = " "
		
		def grupos = UsuarioGrupoFuncionario.findAllByUsuario(user).collect { it.grupoFuncionario }
		grupos.each { g->
			def funcionarios = GrupoFuncionario.findAllByGrupo(g).collect { gf -> gf.funcionario }
			funcionarios.each {
				filtro += it.id + ","
			}
		}
		
		filtro.substring(0, filtro.length()-1).trim()
	}
				
}