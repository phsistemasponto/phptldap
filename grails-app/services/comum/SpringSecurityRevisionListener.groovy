package comum;

import grails.util.GrailsWebUtil

import org.hibernate.envers.RevisionListener

import acesso.UserRevisionEntity


class SpringSecurityRevisionListener implements RevisionListener {
	
	def springSecurityService
	
	public void newRevision(Object entity) {
		UserRevisionEntity revisionEntity = (UserRevisionEntity) entity
		def application = GrailsWebUtil.currentApplication()
		if (application) {
			springSecurityService = application.getMainContext().getBean("springSecurityService")
			def usuario = springSecurityService.getCurrentUser()
			revisionEntity.usuario = usuario
			revisionEntity.data = new Date()
		}
	}
}