package acesso


class UsuarioService {
	
	def springSecurityService
	
	boolean transactional = true
	
	def atualizarAcessos(params, usuarioParametro){
		def gruposUsuario = UsuarioGrupo.where { usuario == usuarioParametro}
		gruposUsuario.deleteAll()
		
		def acessos = UsuarioPerfil.where { usuario == usuarioParametro}
		acessos.deleteAll()
		Perfil perfil
		GrupoAcesso grupoAcesso
		UsuarioPerfil usuarioPerfil
		UsuarioGrupo usuarioGrupo
		def perfisGrupo
		params.perfilId.eachWithIndex { it, i ->
			println('Indice: '+i+'   valor: '+params.permissao[i])
			if (params.permissao[i]=='true') {
				grupoAcesso = GrupoAcesso.get(it)
				usuarioGrupo = new UsuarioGrupo( usuario: usuarioParametro, grupo: grupoAcesso)
				usuarioGrupo.save()
				
				perfisGrupo = GrupoPerfil.where { grupo == grupoAcesso }
				perfisGrupo.each { perfilGrupo ->
					usuarioPerfil = new UsuarioPerfil(usuario: usuarioParametro, perfil: perfilGrupo.perfil)
					usuarioPerfil.save()
				}
				
				
			}
		}
		
	}
	
}



