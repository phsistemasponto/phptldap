package acesso

import org.springframework.transaction.annotation.Transactional



class GrupoAcessoService {
	def springSecurityService
	
	boolean transactional = true
	
	def atualizarAcessos(params, grupoParametro){
		def acessos = GrupoPerfil.where { grupo == grupoParametro}
		acessos.deleteAll()
		Perfil perfil
		GrupoPerfil grupoPerfil
		params.perfilId.eachWithIndex { it, i ->
			
			if (params.permissao[i]=='true') {
				perfil = Perfil.get(it)
				grupoPerfil = new GrupoPerfil( grupo: grupoParametro, perfil: perfil)
				grupoPerfil.save()	
			}
		}
		
	}
	
	Set<Perfil> retornaPerfis(Usuario usuario) {
		def perfis = new HashSet<Perfil>()
		def consulta = Perfil.executeQuery("select gp.perfil from UsuarioGrupo ug, GrupoPerfil gp \
								   where ug.grupo = gp.grupo and ug.usuario = :usuario ) ", [usuario: usuario])
		perfis.addAll(consulta)
		perfis
	}
	
	@Transactional
	GrupoAcesso retornaGrupoAcesso(Usuario usuario) {
		def consulta = GrupoAcesso.executeQuery("select ug.grupo from UsuarioGrupo ug where ug.usuario = :usuario ", [usuario: usuario])
		return consulta.get(0)
	}
	
}



