package acesso

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional

import cadastros.Filial
import cadastros.Funcionario
import cadastros.Grupo
import cadastros.Setor


@Transactional
class UsuarioController {
	
	def usuarioService
	def springSecurityService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def usuarioList = new ArrayList<Usuario>()
		int usuarioCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			if (tipoFiltro > 0) {
				order = params.order ? params.order : "id"
				sortType = params.sortType ? params.sortType : "desc"
				def orderBy = "order by " + order + " " + sortType
				switch (tipoFiltro) {
					case 1:
						usuarioList.addAll(
							Usuario.findAll(
								"from Usuario c where c.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong(), max: params.max, offset: params.offset]))
						usuarioCount = Usuario.findAll(
								"from Usuario c where c.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong()]).size()
						break;
					case 2:
						usuarioList.addAll(
							Usuario.findAll(
								"from Usuario c where upper(c.username) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						usuarioCount = Usuario.findAll(
								"from Usuario c where upper(c.username) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					case 3:
						usuarioList.addAll(
							Usuario.findAll(
								"from Usuario c where upper(c.nome) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						usuarioCount = Usuario.findAll(
								"from Usuario c where upper(c.nome) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					default:
						break;
				}
			} else {
				request.message = "Selecione um tipo de filtro"
			}
		}
		
		[usuarioInstanceList: usuarioList, usuarioInstanceTotal: usuarioCount, order: order, sortType: sortType]
    }
	
	

    def delete() {
		def usuarioInstance = Usuario.get(params.id)
		
		UsuarioGrupo.removeAll(usuarioInstance)
		UsuarioFuncionario.removeAll(usuarioInstance)
		UsuarioFilial.removeAll(usuarioInstance)
		UsuarioSetor.removeAll(usuarioInstance)
		UsuarioGrupoFuncionario.removeAll(usuarioInstance)
		
        if (!usuarioInstance) {
            request.error = message(code: 'default.not.found.message', args: ['Usuario', usuarioInstance.id])
        } else {
            try {
                usuarioInstance.delete(flush: true)
                request.message = message(code: 'default.deleted.message', args: ['Usuario'])
            } catch (DataIntegrityViolationException e) {
                request.error = message(code: 'default.not.deleted.message', args: ['Usuario'])
            }
        }

        redirect(action: "list")
    }

    def create() {
		def perfis = new ArrayList<Perfil>()
		def filiais = new ArrayList<Filial>()
		def setores = new ArrayList<Setor>()
		def funcionarios = new ArrayList<Funcionario>()
		def grupos = new ArrayList<Grupo>()
		
		def tamanhoFiliais = (filiais)?filiais.size():0
		def tamanhoSetores = (setores)?setores.size():0
		def tamanhoFuncionarios = (funcionarios)?funcionarios.size():0
		def tamanhoGrupos = (grupos)?grupos.size():0
		
		[usuarioInstance: new Usuario(),
			perfis: perfis, filiais: filiais, setores: setores, funcionarios: funcionarios, grupos: grupos,
			sequenciaFilial: tamanhoFiliais, sequenciaSetores: tamanhoSetores, sequenciaFuncionarios: tamanhoFuncionarios, sequenciaGrupo: grupos]
    }

    def edit() {
        def usuarioInstance = Usuario.get(params.id)
        if (!usuarioInstance) {
            request.error = message(code: 'default.not.found.message', args: ['Usuario', usuarioInstance.id])
            redirect(action: "list")
            return
        }
		def perfis = usuarioInstance.getPerfisUsuario()
		def filiais = usuarioInstance.getUsuarioFiliais()
		def setores = usuarioInstance.getUsuarioSetores()
		def funcionarios = usuarioInstance.getFuncionarios()
		def grupos = usuarioInstance.getUsuarioGrupos()
		
		def tamanhoFiliais = (filiais)?filiais.size():0
		def tamanhoSetores = (setores)?setores.size():0
		def tamanhoFuncionarios = (funcionarios)?funcionarios.size():0
		def tamanhoGrupos = (grupos)?grupos.size():0
		
        [usuarioInstance: usuarioInstance, 
			perfis: perfis, filiais: filiais, setores: setores, funcionarios: funcionarios, grupos: grupos,
			sequenciaFilial: tamanhoFiliais, sequenciaSetores: tamanhoSetores, sequenciaFuncionarios: tamanhoFuncionarios, sequenciaGrupo: grupos]
    }

    def save() {
		
		def usuarioInstance = new Usuario(params)
		
		if (params.funcionarioId) {
			usuarioInstance.funcionario = Funcionario.findByCrcFunc(params.funcionarioId.toLong())
		}
		
		if (validaTipoCaracteres(usuarioInstance)
			&& validaQuantidadeCaracteres(usuarioInstance)
			&& validaQuantidadeMaximaCaracteres(usuarioInstance)) {
			
		
	        if (!usuarioInstance.save()) {
	            render(view: "create", model: [usuarioInstance: usuarioInstance])
	            return
	        }
			
			configuraUsuarioGrupo(usuarioInstance, params)
			configuraUsuarioFuncionario(usuarioInstance, params)
			configuraUsuarioFilial(usuarioInstance, params)
			configuraUsuarioSetor(usuarioInstance, params)
			configuraUsuarioGrupoFuncionario(usuarioInstance, params)
			
			HistoricoSenha historico = new HistoricoSenha()
			historico.login = usuarioInstance.username
			historico.data = new Date()
			historico.password = params.password
			historico.save(flush: true)
	
			request.message = message(code: 'default.created.message', args: ['Usuario'])
			
		} else {
			render(view: "create", model: [usuarioInstance: usuarioInstance])
			return
		}
			
        redirect(action: "create")
    }

    def update() {
        def usuarioInstance = Usuario.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (usuarioInstance.version > version) {
                request.error = message(code: 'default.optimistic.locking.failure', args: ['Usuario'])
                render(view: "edit", model: [usuarioInstance: usuarioInstance])
                return
            }
        }

        usuarioInstance.properties = params
		
		if (params.funcionarioId) {
			usuarioInstance.funcionario = Funcionario.findByCrcFunc(params.funcionarioId.toLong())
		}

        if (!usuarioInstance.save()) {
            render(view: "edit", model: [usuarioInstance: usuarioInstance])
            return
        }
		
		configuraUsuarioGrupo(usuarioInstance, params)
		configuraUsuarioFuncionario(usuarioInstance, params)
		configuraUsuarioFilial(usuarioInstance, params)
		configuraUsuarioSetor(usuarioInstance, params)
		configuraUsuarioGrupoFuncionario(usuarioInstance, params)
		
		request.message = message(code: 'default.updated.message', args: ['Usuario'])
        redirect(action: "list")
    }
	
	@Transactional
	def alterarSenha() {
		
		def login = params.login
		def senha = params.senha
		
		if (login && senha) {
			Usuario user = Usuario.findByUsername(login.trim())
			
			if (user) {
				
				user.password = senha
				
				if (validaTipoCaracteres(user)
					&& validaQuantidadeCaracteres(user)
					&& validaQuantidadeMaximaCaracteres(user)
					&& validaIntervaloRepeticao(user)) {
					
					user.accountLocked = false
					user.accountExpired = false
					user.passwordExpired = false
					user.enabled = true
					user.save(flush: true)
					
					HistoricoSenha historico = new HistoricoSenha()
					historico.login = login
					historico.data = new Date()
					historico.password = senha
					historico.save(flush: true)
					
					request.message = "Usu&aacute;rio atualizado com sucesso"
					
				}
				
			} else {
				request.error = "Usu&aacute;rio n&atilde;o existe"
			}
			
		}
	}
	
	@Transactional
	def alterarSenhaUsuario() {
		
		def senha = params.senha
		
		if (senha) {
			
			Usuario user = springSecurityService.currentUser
			
			if (user) {
				
				user.password = senha
				
				if (validaTipoCaracteres(user)
					&& validaQuantidadeCaracteres(user)
					&& validaQuantidadeMaximaCaracteres(user)
					&& validaIntervaloRepeticao(user)) {
					
					user.accountLocked = false
					user.accountExpired = false
					user.passwordExpired = false
					user.enabled = true
					user.save(flush: true)
					
					HistoricoSenha historico = new HistoricoSenha()
					historico.login = user.username
					historico.data = new Date()
					historico.password = senha
					historico.save(flush: true)
					
					request.message = "Usu&aacute;rio atualizado com sucesso"
					
				}
				
			} else {
				request.error = "Usu&aacute;rio n&atilde;o existe"
			}
			
		}
	}

	private configuraUsuarioGrupo(Usuario usuarioInstance, Map params) {
		UsuarioGrupo.removeAll(usuarioInstance)
		
		UsuarioGrupo ug = new UsuarioGrupo()
		ug.usuario = usuarioInstance
		ug.grupo = GrupoAcesso.get(params.acesso.toLong())
		ug.save()
	}
	
	private configuraUsuarioFuncionario(Usuario usuarioInstance, Map params) {
		UsuarioFuncionario.removeAll(usuarioInstance)
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.funcionarioIdTable.getClass()) }
		def array = isArray ? params.funcionarioIdTable : [params.funcionarioIdTable]

		if (array != null) {
			array.each {
				if (it != null) {
					UsuarioFuncionario uf = new UsuarioFuncionario()
					uf.usuario = usuarioInstance
					uf.funcionario = Funcionario.get(it.toLong())
					uf.save()
				}
			}
		}
	}
	
	private configuraUsuarioFilial(Usuario usuarioInstance, Map params) {
		UsuarioFilial.removeAll(usuarioInstance)
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialIdTable.getClass()) }
		def array = isArray ? params.filialIdTable : [params.filialIdTable]

		if (array != null) {
			array.each {
				if (it != null) {
					UsuarioFilial uf = new UsuarioFilial()
					uf.usuario = usuarioInstance
					uf.filial = Filial.get(it.toLong())
					uf.save()
				}
			}
		}
	}
	
	private configuraUsuarioSetor(Usuario usuarioInstance, Map params) {
		UsuarioSetor.removeAll(usuarioInstance)
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.setorIdTable.getClass()) }
		def array = isArray ? params.setorIdTable : [params.setorIdTable]

		if (array != null) {
			array.each {
				if (it != null) {
					UsuarioSetor uf = new UsuarioSetor()
					uf.usuario = usuarioInstance
					uf.setor = Setor.get(it.toLong())
					uf.save()
				}
			}
		}
	}
	
	private configuraUsuarioGrupoFuncionario(Usuario usuarioInstance, Map params) {
		UsuarioGrupoFuncionario.removeAll(usuarioInstance)
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.grupoIdTable.getClass()) }
		def array = isArray ? params.grupoIdTable : [params.grupoIdTable]

		if (array != null) {
			array.each {
				if (it != null) {
					UsuarioGrupoFuncionario uf = new UsuarioGrupoFuncionario()
					uf.usuario = usuarioInstance
					uf.grupoFuncionario = Grupo.get(it.toLong())
					uf.save()
				}
			}
		}
	}
	
	def validaQuantidadeCaracteres(Usuario instance) {
		Administracao config = Administracao.get(1)
		String senha = instance.password
		
		def quantidadeMinima = config.quantidadeCaracteres
		def retorno = true
		
		if (senha.length() < quantidadeMinima) {
			request.error = "Quantidade m&iacute;nimina de caracteres para a senha deve ser " + quantidadeMinima
			retorno = false 
		}
		
		retorno
	}
	
	def validaTipoCaracteres(Usuario instance) {
		Administracao config = Administracao.get(1)
		String senha = instance.password
		
		def tipoCaracter = config.tipoCaracter
		def expressao = /^[a-zA-Z]+$/
		def regra = ""
		def retorno = true
		
		if (config.senhaDeveTerSomenteLetras()) {
			expressao = /^[a-zA-Z]+$/
			regra = "somente letras"
		} else if (config.senhaDeveTerSomenteNumeros()) {
			expressao = /^[0-9]+$/
			regra = "somente n&uacute;meros"
		} else if (config.senhaDeveTerLetrasENumeros()) {
			expressao = /^[a-zA-Z0-9]+$/
			regra = "deve ter letras e n&uacute;meros"
		}  
		
		if (!senha.matches(expressao)) {
			request.error = "Senha n&atilde;o obedece &agrave; regra: " + regra
			retorno = false
		}
		
		retorno
	}
	
	def validaQuantidadeMaximaCaracteres(Usuario instance) {
		Administracao config = Administracao.get(1)
		String senha = instance.password

		def quantidadeMaximaCaracteres = config.qtdMaxCaracRep
		def retorno = true
		Character c

		for (int i=0; i<senha.length(); i++) {
			c = senha.charAt(i)
			def ocorrencias = senha.count(c.toString())
			if (ocorrencias > quantidadeMaximaCaracteres) {
				retorno = false
				break
			}
		}
		
		if (!retorno) {
			request.error = "A senha n&atilde;o pode ter mais que " + quantidadeMaximaCaracteres + " caracteres repetidos"
		}

		retorno
	}
	
	def validaIntervaloRepeticao(Usuario instance) {
		Administracao config = Administracao.get(1)
		Long intervalo = config.intervaloRepeticao.toInteger()

		def retorno = true
		
		def senha = springSecurityService.encodePassword(instance.password)
		def consultaHistorico = "from HistoricoSenha h where h.login = :login order by h.data desc"
		def historico = HistoricoSenha.findAll (consultaHistorico, [login: instance.username, max: intervalo, offset: 0])
		historico.each {
			if (it.password.equals(senha)) {
				retorno = false
			}
		}
		
		if (!retorno) {
			request.error = "Esta senha j&aacute; foi usada recentemente"
		}

		retorno
	}
	
}