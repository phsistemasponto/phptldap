package acesso

import org.springframework.dao.DataIntegrityViolationException

class MapeamentoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [mapeamentoInstanceList: Mapeamento.list(params), mapeamentoInstanceTotal: Mapeamento.count()]
    }

    def delete() {
        def mapeamentoInstance = Mapeamento.get(params.id)
        if (!mapeamentoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Mapeamento', mapeamentoInstance.id])
        } else {
            try {
                mapeamentoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Mapeamento'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Mapeamento'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [mapeamentoInstance: new Mapeamento()]
    }

    def edit() {
        def mapeamentoInstance = Mapeamento.get(params.id)
        if (!mapeamentoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Mapeamento', mapeamentoInstance.id])
            redirect(action: "list")
            return
        }

        [mapeamentoInstance: mapeamentoInstance]
    }

    def save() {
        def mapeamentoInstance = new Mapeamento(params)
        if (!mapeamentoInstance.save(flush: true)) {
            render(view: "create", model: [mapeamentoInstance: mapeamentoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Mapeamento'])
        redirect(action: "create")
    }

    def update() {
        def mapeamentoInstance = Mapeamento.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (mapeamentoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Mapeamento'])
                render(view: "edit", model: [mapeamentoInstance: mapeamentoInstance])
                return
            }
        }

        mapeamentoInstance.properties = params

        if (!mapeamentoInstance.save(flush: true)) {
            render(view: "edit", model: [mapeamentoInstance: mapeamentoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Mapeamento'])
        redirect(action: "list")
    }
}