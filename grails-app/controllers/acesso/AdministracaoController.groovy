package acesso

import org.springframework.dao.DataIntegrityViolationException

class AdministracaoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "create", params: params)
    }



    def create() {
		def totalRegistros = Administracao.list()
		if (totalRegistros){
			render(view: "edit", model: [administracaoInstance: (Administracao)totalRegistros[0]])
		} else {
			[administracaoInstance: new Administracao()]
		}
    }

    def edit() {
        def administracaoInstance = Administracao.get(params.id)
        if (!administracaoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Administracao', administracaoInstance.id])
            redirect (action: "create")
            return
        }

        [administracaoInstance: administracaoInstance]
    }

    def save() {
        def administracaoInstance = new Administracao(params)
        if (!administracaoInstance.save(flush: true)) {
            render(view: "create", model: [administracaoInstance: administracaoInstance])
            return
        }

		flash.message = 'Registro criado'
        render(view: "edit", model: [administracaoInstance: administracaoInstance])
    }

    def update() {
        def administracaoInstance = Administracao.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (administracaoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Administracao'])
                render(view: "edit", model: [administracaoInstance: administracaoInstance])
                return
            }
        }

        administracaoInstance.properties = params

        if (!administracaoInstance.save(flush: true)) {
            render(view: "edit", model: [administracaoInstance: administracaoInstance])
            return
        }

		//flash.message = message(code: 'default.updated.message', args: ['Administracao'])
		flash.message = 'Registro atualizado'
        render(view: "edit", model: [administracaoInstance: administracaoInstance])
    }
}