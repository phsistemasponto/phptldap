package acesso

import grails.plugin.springsecurity.annotation.Secured

import java.text.SimpleDateFormat

import org.springframework.transaction.annotation.Transactional

import cadastros.Funcionario


@Transactional
class ReiniciarSenhaController {
	
	def usuarioService
	def springSecurityService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    @Transactional
	def alterarSenha() {
		
		def login = params.login
		def senha = params.senha
		
		if (login && senha) {
			Usuario user = Usuario.findByUsername(login.trim())
			
			if (user) {
				
				user.password = senha
				
				if (validaTipoCaracteres(user)
					&& validaQuantidadeCaracteres(user)
					&& validaQuantidadeMaximaCaracteres(user)
					&& validaIntervaloRepeticao(user)) {
					
					user.accountLocked = false
					user.accountExpired = false
					user.passwordExpired = false
					user.enabled = true
					user.save(flush: true)
					
					HistoricoSenha historico = new HistoricoSenha()
					historico.login = login
					historico.data = new Date()
					historico.password = senha
					historico.save(flush: true)
					
					request.message = "Usu&aacute;rio atualizado com sucesso"
					
				}
				
			} else {
				request.error = "Usu&aacute;rio n&atilde;o existe"
			}
			
		}
	}
	
	@Transactional
	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
	def reiniciarSenha() {
		
		def login = params.login
		def dtNascimento = params.dtNascimento
		
		if (login && dtNascimento) {
			Usuario user = Usuario.findByUsername(login)
			
			if (user) {
				
				Funcionario func = user.funcionario
				if (func != null) {
					Date dtNasc = func.dtNscFunc
					Date dtNascForm = new SimpleDateFormat("dd/MM/yyyy").parse(dtNascimento)
					if (dtNasc.getTime().equals(dtNascForm.getTime())) {
						redirect(action: "alterarSenha", params: params)
					} else {
						request.error = "Data de nascimento informada n&atilde;o corresponde com a do cadastro"
					}
				} else {
					request.error = "Usu&aacute;rio n&atilde;o possui funcion&aacute;rio vinculado"
				}
				
			} else {
				request.error = "Usu&aacute;rio n&atilde;o existe"
			}
			
		}
	}
	
	def validaQuantidadeCaracteres(Usuario instance) {
		Administracao config = Administracao.get(1)
		String senha = instance.password
		
		def quantidadeMinima = config.quantidadeCaracteres
		def retorno = true
		
		if (senha.length() < quantidadeMinima) {
			request.error = "Quantidade m&iacute;nimina de caracteres para a senha deve ser " + quantidadeMinima
			retorno = false
		}
		
		retorno
	}
	
	def validaTipoCaracteres(Usuario instance) {
		Administracao config = Administracao.get(1)
		String senha = instance.password
		
		def tipoCaracter = config.tipoCaracter
		def expressao = /^[a-zA-Z]+$/
		def regra = ""
		def retorno = true
		
		if (config.senhaDeveTerSomenteLetras()) {
			expressao = /^[a-zA-Z]+$/
			regra = "somente letras"
		} else if (config.senhaDeveTerSomenteNumeros()) {
			expressao = /^[0-9]+$/
			regra = "somente n&uacute;meros"
		} else if (config.senhaDeveTerLetrasENumeros()) {
			expressao = /^[a-zA-Z0-9]+$/
			regra = "deve ter letras e n&uacute;meros"
		}
		
		if (!senha.matches(expressao)) {
			request.error = "Senha n&atilde;o obedece &agrave; regra: " + regra
			retorno = false
		}
		
		retorno
	}
	
	def validaQuantidadeMaximaCaracteres(Usuario instance) {
		Administracao config = Administracao.get(1)
		String senha = instance.password

		def quantidadeMaximaCaracteres = config.qtdMaxCaracRep
		def retorno = true
		Character c

		for (int i=0; i<senha.length(); i++) {
			c = senha.charAt(i)
			def ocorrencias = senha.count(c.toString())
			if (ocorrencias > quantidadeMaximaCaracteres) {
				retorno = false
				break
			}
		}
		
		if (!retorno) {
			request.error = "A senha n&atilde;o pode ter mais que " + quantidadeMaximaCaracteres + " caracteres repetidos"
		}

		retorno
	}
	
	def validaIntervaloRepeticao(Usuario instance) {
		Administracao config = Administracao.get(1)
		Long intervalo = config.intervaloRepeticao.toInteger()

		def retorno = true
		
		def senha = springSecurityService.encodePassword(instance.password)
		def consultaHistorico = "from HistoricoSenha h where h.login = :login order by h.data desc"
		def historico = HistoricoSenha.findAll (consultaHistorico, [login: instance.username, max: intervalo, offset: 0])
		historico.each {
			if (it.password.equals(senha)) {
				retorno = false
			}
		}
		
		if (!retorno) {
			request.error = "Esta senha j&aacute; foi usada recentemente"
		}

		retorno
	}
	
}