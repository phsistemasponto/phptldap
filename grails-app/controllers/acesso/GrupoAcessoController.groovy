package acesso

import org.springframework.dao.DataIntegrityViolationException

class GrupoAcessoController {
	
	def grupoAcessoService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [grupoAcessoInstanceList: GrupoAcesso.list(params), grupoAcessoInstanceTotal: GrupoAcesso.count()]
    }

    def delete() {
        def grupoAcessoInstance = GrupoAcesso.get(params.id)
        if (!grupoAcessoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Perfil de Acesso', grupoAcessoInstance.id])
        } else {
            try {
                grupoAcessoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Perfil de Acesso'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Perfil de Acesso'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [grupoAcessoInstance: new GrupoAcesso()]
    }

    def edit() {
        def grupoAcessoInstance = GrupoAcesso.get(params.id)
        if (!grupoAcessoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Perfil de Acesso', grupoAcessoInstance.id])
            redirect(action: "list")
            return
        }
		def perfis = grupoAcessoInstance.getPerfisGrupo()

        [grupoAcessoInstance: grupoAcessoInstance, perfis: perfis]
    }

    def save() {
        def grupoAcessoInstance = new GrupoAcesso(params)
        if (!grupoAcessoInstance.save(flush: true)) {
            render(view: "create", model: [grupoAcessoInstance: grupoAcessoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Perfil de Acesso'])
        redirect(action: "create")
    }

    def update() {
        def grupoAcessoInstance = GrupoAcesso.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (grupoAcessoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Perfil de Acesso'])
                render(view: "edit", model: [grupoAcessoInstance: grupoAcessoInstance])
                return
            }
        }

        grupoAcessoInstance.properties = params

        if (!grupoAcessoInstance.save(flush: true)) {
            render(view: "edit", model: [grupoAcessoInstance: grupoAcessoInstance])
            return
        }

		grupoAcessoService.atualizarAcessos(params, grupoAcessoInstance)

		flash.message = message(code: 'default.updated.message', args: ['Perfil de Acesso'])
        redirect(action: "list")
    }
}