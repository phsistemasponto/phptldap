package acesso

import org.springframework.dao.DataIntegrityViolationException

class PerfilController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [perfilInstanceList: Perfil.list(params), perfilInstanceTotal: Perfil.count()]
    }

    def delete() {
        def perfilInstance = Perfil.get(params.id)
        if (!perfilInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Perfil', perfilInstance.id])
        } else {
            try {
                perfilInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Perfil'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Perfil'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [perfilInstance: new Perfil()]
    }

    def edit() {
        def perfilInstance = Perfil.get(params.id)
        if (!perfilInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Perfil', perfilInstance.id])
            redirect(action: "list")
            return
        }

        [perfilInstance: perfilInstance]
    }

    def save() {
        def perfilInstance = new Perfil(params)
        if (!perfilInstance.save(flush: true)) {
            render(view: "create", model: [perfilInstance: perfilInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Perfil'])
        redirect(action: "create")
    }

    def update() {
        def perfilInstance = Perfil.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (perfilInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Perfil'])
                render(view: "edit", model: [perfilInstance: perfilInstance])
                return
            }
        }

        perfilInstance.properties = params

        if (!perfilInstance.save(flush: true)) {
            render(view: "edit", model: [perfilInstance: perfilInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Perfil'])
        redirect(action: "list")
    }
}