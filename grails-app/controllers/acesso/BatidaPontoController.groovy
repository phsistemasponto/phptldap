package acesso

import java.text.SimpleDateFormat

import org.springframework.transaction.annotation.Transactional
import grails.plugin.springsecurity.annotation.Secured

import cadastros.Batidas
import cadastros.Funcionario
import funcoes.Horarios


@Transactional
class BatidaPontoController {
	
	def usuarioService
	def springSecurityService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	@Transactional
	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
	def index() {
		
	}
	
	@Transactional
	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
	def baterPonto() {
		
		def cpf = params.cpf
		def pis = params.pis
		def funcionario = null
		
		if (cpf != null && !cpf.trim().isEmpty()) {
			funcionario = Funcionario.findByDocCpf(cpf)
		} else {
			funcionario = Funcionario.findByPisFunc(pis)
		}
		
		if (funcionario == null) {
			request.error = "Funcion&aacute;rio n&atilde;o encontrado"
			return
		} else {
		
			def data = new SimpleDateFormat("dd/MM/yyyy").format(new Date())
			def horas = new SimpleDateFormat("HH:mm").format(new Date())
			def horasInt = Horarios.calculaMinutos(horas) 
			
			Batidas b = new Batidas()
			b.funcionario = funcionario
			b.dtBat = new Date()
			b.hrBat = horasInt
			b.save(flush: true)
			
			[funcionarioNome: funcionario.nomFunc, horarioBatida: data.concat(" - ").concat(horas)]
		}
		
	}
	
}