package acesso

import java.sql.Blob

import javax.sql.rowset.serial.SerialBlob

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile


@Transactional
class MenuController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		[menuInstanceList: Menu.list(params), menuInstanceTotal: Menu.count()]
	}
	
	@Transactional
	def image() {
		def menu = Menu.get( params.id )
		Set<MenuIcone> icone = menu.icone
		if (icone != null && !icone.isEmpty()) {
			MenuIcone menuIcone
			icone.each {
				menuIcone = it
			}
			Blob conteudo = menuIcone.conteudo
			byte[] image = conteudo.getBytes(1l, conteudo.length().toInteger())
			response.outputStream << image
		}
	}

	def delete() {
		def menuInstance = Menu.get(params.id)
		if (!menuInstance) {
			flash.error = message(code: 'default.not.found.message', args: ['Menu', menuInstance.id])
		} else {
			try {
				menuInstance.delete(flush: true)
				flash.message = message(code: 'default.deleted.message', args: ['Menu'])
			} catch (DataIntegrityViolationException e) {
				flash.error = message(code: 'default.not.deleted.message', args: ['Menu'])
			}
		}

		redirect(action: "list")
	}

	def create() {
		[menuInstance: new Menu(), possuiIcone: false]
	}

	@Transactional
	def edit() {
		def menuInstance = Menu.get(params.id)
		
		if (!menuInstance) {
			flash.error = message(code: 'default.not.found.message', args: ['Menu', menuInstance.id])
			redirect(action: "list")
			return
		}
		
		def possuiIcone = !MenuIcone.findAll ('from MenuIcone where menu=:menu',[menu: menuInstance]).isEmpty()

		[menuInstance: menuInstance, possuiIcone: possuiIcone]
	}

	@Transactional
	def save() {
		def menuInstance = new Menu(params)
		if (!menuInstance.save(flush: true)) {
			render(view: "create", model: [menuInstance: menuInstance])
			return
		}
		
		configuraIcone(menuInstance, params)

		flash.message = message(code: 'default.created.message', args: ['Menu'])
		redirect(action: "create")
	}

	@Transactional
	def update() {
		def menuInstance = Menu.get(params.id)
		if (params.version) {
			def version = params.version.toLong()
			if (menuInstance.version > version) {
				flash.error = message(code: 'default.optimistic.locking.failure', args: ['Menu'])
				render(view: "edit", model: [menuInstance: menuInstance])
				return
			}
		}

		menuInstance.properties = params

		if (!menuInstance.save(flush: true)) {
			render(view: "edit", model: [menuInstance: menuInstance])
			return
		}
		
		configuraIcone(menuInstance, params)

		flash.message = message(code: 'default.updated.message', args: ['Menu'])
		redirect(action: "list")
	}
	
	private configuraIcone(Menu menuInstance, Map params) {
		
		if (params.mudouIcone.toBoolean()) {
			if (menuInstance.id != null) {
				def iconeMenu = MenuIcone.findAll ('from MenuIcone where menu=:menu',[menu: menuInstance])
				iconeMenu.each {
					MenuIcone menu = MenuIcone.get(it.id)
					menuInstance.icone = null
					menu.delete()
				}
			}
						
			def icone = params.files
	
			def inputStream
	
			if (icone instanceof CommonsMultipartFile) {
				inputStream = icone.getInputStream()
			}
	
			if (inputStream != null) {
				def bytes = inputStream.bytes
	
				MenuIcone mi = new MenuIcone()
				mi.menu = menuInstance
				mi.conteudo = new SerialBlob(bytes)
	
				menuInstance.icone = new HashSet<MenuIcone>()
				menuInstance.icone.add(mi)
			}
		}
	}
}