package acesso

import grails.plugin.springsecurity.annotation.Secured

import java.text.SimpleDateFormat

import org.springframework.transaction.annotation.Transactional


@Transactional
class RedirectLoginController {
	
	def usuarioService
	def springSecurityService
	
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	@Transactional
	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
	def index() {
		
		Usuario usuario = springSecurityService.currentUser
		println('teste!!!')
		println('Usuario: '+usuario)
		def sessionId = session.getId()
		Usuario user = Usuario.get(usuario.id)
		GrupoAcesso grupo = user.getAcesso()
		
		Administracao config = Administracao.get(1)
		def historicoLogin = config.historicoLogin
		
		if (historicoLogin.equals("S")) {
			HistoricoLogin historico = new HistoricoLogin()
			historico.login = user.username
			historico.data = new Date()
			historico.sessionId = sessionId
			historico.sucesso = true
			historico.save(flush: true)
			
			println historico.errors
		}
		
		verificaDiasAvisoSenha()
		populaInfoLogin()
		
		// direciona para espelho de ponto se for funcionario
		if (grupo.descricao.equals("FUNCIONARIO")) {
			redirect(uri: "/espelhoPontoFuncionario/")
		} else {
			redirect(uri: "/")
		}
	}
	
	def verificaDiasAvisoSenha() {
		
		Administracao config = Administracao.get(1)
		def diasAviso = config.diasAviso.toInteger()
		def periodicidade = config.periodicidade.toInteger()
		
		Usuario usuario = springSecurityService.currentUser
		
		def consultaHistorico = "from HistoricoSenha h where h.login = :login order by h.data desc"
		def historico = HistoricoSenha.findAll (consultaHistorico, [login: usuario.username, max: 1, offset: 0])
		def ultimoHistorico = historico != null && !historico.isEmpty() ? historico[0] : null
		
		def hoje = new Date()
		def dataUltimoSenha = ultimoHistorico != null ? ultimoHistorico.data : new Date()
		def validade = dataUltimoSenha + periodicidade
		
		//session.putAt("ultimoLogin", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(dataUltimoSenha))
		
		if (diasAviso > 0 && periodicidade > 0 && ((hoje+diasAviso) > validade)) {
			flash.message = "Sua senha ir&aacute; expirar em " + new SimpleDateFormat("dd/MM/yyyy").format(validade)
		}
	}
	
	def populaInfoLogin() {
		
		Usuario usuario = springSecurityService.currentUser
		
		def consultaHistorico = "from HistoricoLogin h where h.login = :login order by h.data desc"
		def historico = HistoricoLogin.findAll (consultaHistorico, [login: usuario.username, max: 2, offset: 0])
		def ultimoHistorico = historico != null && !historico.isEmpty() && historico.size() >= 2? historico[1] : null
		def infoUltimoLogin = ultimoHistorico != null ? 
			new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(ultimoHistorico.data) : ""
		
		session.putAt("ultimoLogin", infoUltimoLogin)
	}
	
}