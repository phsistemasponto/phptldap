package comum

import grails.converters.JSON
import groovy.sql.GroovyRowResult
import groovy.sql.Sql

import org.apache.commons.codec.binary.Base64
import org.apache.poi.hssf.usermodel.HSSFCell
import org.apache.poi.hssf.usermodel.HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Font

import acesso.Usuario
import cadastros.HistoricoConsulta


class GeracaoConsultasController {
	
	def dataSource
	def springSecurityService
	
	def index() {
		Usuario usuario = springSecurityService.currentUser
		[consultas: HistoricoConsulta.findAllByUsuario(usuario)]
	}
	
	def executeSql() {
		
		def sql = params.inputSql
		if (sql) {
			
			try {
				
				Usuario usuario = springSecurityService.currentUser
				HistoricoConsulta hc = new HistoricoConsulta()
				hc.usuario = usuario
				hc.consulta = sql
				hc.save(flush: true)
				
				if (sql.trim().startsWith("select")) {
					Sql sqlObj = new Sql(dataSource)
					def result = sqlObj.rows(sql)
					if (result) {
						def columNames = ((GroovyRowResult) result[0]).keySet()
						def json = [result: result, columNames: columNames]
						render json as JSON
					} else {
						def json = [messageErro: "Sem resultado"]
						render json as JSON
					}
				} else {
					Sql sqlObj = new Sql(dataSource)
					def qtRegistros = sqlObj.executeUpdate(sql)
					def json = [qtRegistros: qtRegistros]
					render json as JSON
				}
			} catch(Exception e) {
				def json = [messageErro: e.getLocalizedMessage()]
				render json as JSON
			}
			
		} else {
			def json = [messageErro: "Deve ser informado uma consulta"]
			render json as JSON
		}
		
	}
	
	def exportExcel() {
		
		session.setAttribute("reportData", null)
		session.setAttribute("reportFormat", null)
		
		def sql = params.inputSql
		def result = []
		def columNames = []
		
		if (sql) {
			
			if (sql.trim().startsWith("select")) {
				
				Sql sqlObj = new Sql(dataSource)
				result = sqlObj.rows(sql)
				if (result) {
					columNames = ((GroovyRowResult) result[0]).keySet()
					def json = [result: result, columNames: columNames]
					render json as JSON
				}
			} else {
			
				def json = [messageErro: "Somente select's podem ser exportados"]
				render json as JSON
				
			}
			
		}
		
		if (!result.empty && !columNames.empty) {
			
			HSSFWorkbook workbook = new HSSFWorkbook()
			HSSFSheet firstSheet = workbook.createSheet("Resultado");
			
			Font headerFont = workbook.createFont();
			headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			CellStyle headerStyle = workbook.createCellStyle();
			headerStyle.setFont(headerFont);
			
			HSSFRow rowHeader = firstSheet.createRow(0);
			columNames.eachWithIndex { cn, idx ->
				HSSFCell celHeader = rowHeader.createCell(idx)
				celHeader.setCellStyle(headerStyle)
				celHeader.setCellValue(cn)
			}
			
			result.eachWithIndex { r, idx ->
				HSSFRow row = firstSheet.createRow(idx+1)
				r.eachWithIndex { c, idx2 -> 
					row.createCell(idx2).setCellValue(c.getValue());
				}
			}
			
			columNames.eachWithIndex { cn, idx ->
				firstSheet.autoSizeColumn(idx)
			}
			
			ByteArrayOutputStream saida = new ByteArrayOutputStream()
			workbook.write(saida)
			
			def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
			def reportData = [reportData: binaryBase64, format: "XLS"]
			
			session.setAttribute("reportData", binaryBase64)
			session.setAttribute("reportFormat", "XLS")
			
			render reportData as JSON
			
		} else {
			def json = [messageErro: "Sem resultado"]
			render json as JSON
		}
		
		
		
	}
		
}