package comum

import javax.servlet.ServletOutputStream

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat


class ReportPrintController {
	
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def printReport() {
		
		def reportData = ""
		def format = ""
		
		if (params.reportData != null) {
			reportData = params.reportData.toString()
			format = params.format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		} else {
			reportData = session.getAttribute("reportData")
			format = session.getAttribute("reportFormat").equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		}
		
		def contentType = format.mimeTyp
		byte[] saida = Base64.decodeBase64(reportData)
		
		//get the output stream from the common property 'response'
		ServletOutputStream servletOutputStream = response.outputStream
 
		//set the byte content on the response. This determines what is shown in your browser window.
		
		response.setContentType(contentType)
		response.setContentLength(saida.size())
	 
		response.setHeader('Content-disposition', 'inline; filename='.concat("relatorio.").concat(format.extension))
		response.setHeader('Expires', '0');
		response.setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
		servletOutputStream << saida
		
		
	}
		
}