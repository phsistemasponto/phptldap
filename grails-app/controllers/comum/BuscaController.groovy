package comum
import grails.converters.JSON
import cadastros.Funcionario


class BuscaController {
	def buscaService
	def filtroPadraoService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def lovBusca = {
		render(view:"lovBusca", model: [campoIdRetorno: params.campoIdRetorno, campoDescricaoRetorno: params.descricaoRetorno, classeBusca: params.classeBusca, campoId: params.campoId, campoDescricao: params.campoDescricao, campoCodigo: params.campoCodigo])
    }

	def lovResultado = {
		
		def usuario = session['usuario']
		
		def resultado = buscaService.resultadoBuscaLov(params, usuario)
		if (resultado) {
			render resultado as JSON
		} else {
			[]
		}
	}
	
	def buscaId = {
		/*
			classeBusca = A classe, com package, que onde se ir� procurar
			campoId = o atributo da classe que ser� o id da busca solicitada
			campoDescricao = o campo de descricao de retorno a ser buscado
			campoCodigo = o campo de codigo de retorno. caso seja o mesmo do id, informar igual. 
						  Ex.: no cfop, o campo c�digo � significativo para busca, 
			              mas deve ser utilizado o campo id para salvar ou atualizar.
			empresa = se utilizar� a empresa que est� na sess�o para a busca
		*/
		def usuario = session['usuario']
		
		def resultado = buscaService.buscaId(params.classeBusca, params.campoId, params.campoDescricao, params.stringBusca, usuario)
		render resultado as JSON		
	}
	
	def buscaFiltroPadrao = {
		def funcs = new ArrayList<Funcionario>()
		def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)
		def orderby = " order by f.id "
		filtroPadrao += orderby
		funcs.addAll(Funcionario.findAll(filtroPadrao))
		
		
		def resultadoMap = []
		funcs.each {  
			resultadoMap.add([id: it.id, matricula: it.mtrFunc, nome: it.nomFunc])
		}
		
		render resultadoMap as JSON
	}
	
	def lovBuscaPerfilHorario = {
		render(view:"lovBuscaPerfilHorario")
	}

	def lovResultadoPerfilHorario = {
		
		def resultado = buscaService.resultadoBuscaLovPerfilHorario( params.stringBusca)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdPerfilHorario = {	
		def resultado = buscaService.buscaIdPerfilHorario(params.codigo)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
		
	}
	
	def _lovPadrao = {
	}
	
	def lovJustificativaEspelhoPonto = {
	}

	
// Filial	
	
		
	def lovFilial = {
	}
	
	def lovResultadoFilial= {

		def resultado = buscaService.resultadoBuscaLovFilial( params.stringBusca, (session['empresaId']=='null')?session['empresaId']:'1'  )
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdFilial = {
		def resultado = buscaService.buscaIdFilial(params.codigo.toLong())
		if (!resultado){
			resultado = []
		}
		
		render resultado as JSON
		
	}

// Fim filial
	
// Ocorrencia
	
		
	def lovOcorrencia = {
	}
	
	def lovResultadoOcorrencia= {

		def resultado = buscaService.resultadoBuscaLovOcorrencia(params.stringBusca)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdOcorrencia = {
		def resultado = buscaService.buscaIdOcorrencia(params.codigo.toLong())
		if (!resultado){
			resultado = []
		}
		
		render resultado as JSON
		
	}

// Fim Ocorrencia
	
// TipoHora
	
		
	def lovTipoHora = {
	}
	
	def lovResultadoTipoHora= {

		def resultado = buscaService.resultadoBuscaLovTipoHora(params.stringBusca, params.suprimir)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdTipoHora = {
		def resultado = buscaService.buscaIdTipoHora(params.codigo.toLong(), params.suprimir)
		if (!resultado){
			resultado = []
		}
		
		render resultado as JSON
		
	}

// Fim TipoHora
	
// Classificacao
	
		
	def lovClassificacao = {
	}
	
	def lovResultadoClassificacao= {

		def resultado = buscaService.resultadoBuscaLovClassificacao(params.stringBusca, params.suprimir)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdClassificacao = {
		def resultado = buscaService.buscaIdClassificacao(params.codigo.toLong(), params.suprimir)
		if (!resultado){
			resultado = []
		}
		
		render resultado as JSON
		
	}

// Fim Classificacao

// Beneficio
	
		
	def lovBeneficio = {
	}
	
	def lovResultadoBeneficio = {

		def resultado = buscaService.resultadoBuscaLovBeneficio(params.stringBusca)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdBeneficio = {
		def resultado = buscaService.buscaIdBeneficio(params.codigo.toLong())
		if (!resultado){
			resultado = []
		}
		
		render resultado as JSON
		
	}

// Fim Beneficio

// TipoBeneficio
	
		
	def lovTipoBeneficio = {
	}
	
	def lovResultadoTipoBeneficio = {

		def resultado = buscaService.resultadoBuscaLovTipoBeneficio(params.stringBusca)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdTipoBeneficio = {
		def resultado = buscaService.buscaIdTipoBeneficio(params.codigo.toLong())
		if (!resultado){
			resultado = []
		}
		
		render resultado as JSON
		
	}

// Fim Tipo de Beneficio
	
	
// Setor
	
	def lovSetor = {
	}
	
	def lovResultadoSetor = {

		def resultado = buscaService.resultadoBuscaLovSetor( params.stringBusca, params.filial )
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdSetor = {
		def resultado = buscaService.buscaIdSetor(params.codigo.toLong())
		if (!resultado){
			resultado = []
		}
		
		render resultado as JSON
		
	}
	
	def buscaIdSetorComFilial = {
		def resultado = buscaService.buscaIdSetorComFilial(params.stringBusca.toLong(), params.filial.toLong()) 
		if (!resultado) { 
			resultado = []
		}
		
		render resultado as JSON
		
	}

	
// Fim setor
	
// Funcionario
	def lovFuncionario = {
	}
	
	def lovResultadoFuncionario = {

		def usuario = session['usuario']
		def resultado = buscaService.resultadoBuscaLovFuncionario(params.stringBusca, usuario)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaMatriculaFuncionario = {
		def resultado = buscaService.buscaMatriculaFuncionario(params.codigo)
		if (!resultado){
			resultado = []
		}
		
		render resultado as JSON
		
	}

// fim funcionario
	
// FiltroUsuario
	def lovFiltroUsuario = {
	}
	
	def lovResultadoFiltroUsuario = {

		def usuario = session['usuario']
		
		def resultado = buscaService.resultadoBuscaLovFiltroUsuario(params.stringBusca, usuario)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}
	
	def buscaIdFiltroUsuario = {
		def usuario = session['usuario']
		
		def resultado = buscaService.buscaIdFiltroUsuario(params.codigo.toLong(), usuario)
		if (!resultado){
			resultado = []
		}
		render resultado as JSON
	}

// fim FiltroUsuario
		
}