package relatorios

import grails.converters.JSON
import groovy.sql.Sql
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import net.sf.jasperreports.engine.design.JRDesignBand
import net.sf.jasperreports.engine.design.JRDesignExpression
import net.sf.jasperreports.engine.design.JRDesignSection
import net.sf.jasperreports.engine.design.JRDesignStaticText
import net.sf.jasperreports.engine.design.JRDesignTextField
import net.sf.jasperreports.engine.design.JasperDesign
import net.sf.jasperreports.engine.export.JRXlsExporterParameter
import net.sf.jasperreports.engine.type.HorizontalAlignEnum
import net.sf.jasperreports.engine.xml.JRXmlLoader

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.SessionFactory
import org.springframework.context.ApplicationContext
import org.springframework.transaction.annotation.Transactional

import acesso.Usuario

@Transactional
class RelatorioFuncionariosDinamicoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	SessionFactory sessionFactory
	
	def index() {
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)
		
		def ctxPath = grailsAttributes.applicationContext.getResource("/").getFile().toString()
		def pathReport = ctxPath + File.separator +  "reports" + File.separator + reportName
		JasperDesign jd = JRXmlLoader.load(pathReport)
		
		montaRelatorioDinamico(jd);
		
		JRDataSource ds = new JRBeanCollectionDataSource(results);
		JasperReport jasperReport = JasperCompileManager.compileReport(jd);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		ByteArrayOutputStream saida = new ByteArrayOutputStream();
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}

	private montaRelatorioDinamico(JasperDesign jd) {
		
		JRDesignSection detailSection = jd.detailSection
		JRDesignBand cabecalhoBand = new JRDesignBand()
		JRDesignBand detailBand = detailSection.bandsList.get(0)

		cabecalhoBand.setHeight(20)
		jd.setColumnHeader(cabecalhoBand)
		detailBand.setHeight(20)
		
		boolean exibeMatricula = params.matricula
		boolean exibeCracha = params.cracha
		boolean exibeNome = params.nome
		boolean exibeFilial = params.filial
		boolean exibeCargo = params.cargo
		boolean exibeSetor = params.setor
		boolean exibeHorario = params.horario
		boolean exibeHoraExtra = params.horaExtra
		boolean exibeNascimento = params.nascimento
		boolean exibeAdmissao = params.admissao
		boolean exibeRescisao = params.rescisao
		
		int positionReport = 0
		
		if (exibeMatricula) { 
			int tamanho = 40
			montaCampo(cabecalhoBand, "Matricula", detailBand, '$F{mtr_func}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeCracha) {
			int tamanho = 40
			montaCampo(cabecalhoBand, "Cracha", detailBand, '$F{crc_func}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeNome) {
			int tamanho = 200
			montaCampo(cabecalhoBand, "Nome", detailBand, '$F{nom_func}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeFilial) {
			int tamanho = 200
			montaCampo(cabecalhoBand, "Filial", detailBand, '$F{dsc_fil}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeCargo) {
			int tamanho = 80
			montaCampo(cabecalhoBand, "Cargo", detailBand, '$F{dsc_cargo}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeSetor) {
			int tamanho = 80
			montaCampo(cabecalhoBand, "Setor", detailBand, '$F{dsc_setor}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeHorario) {
			int tamanho = 80
			montaCampo(cabecalhoBand, "Horario", detailBand, '$F{dsc_horario}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeHoraExtra) {
			int tamanho = 80
			montaCampo(cabecalhoBand, "Hora extra", detailBand, '$F{horaextra}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeNascimento) {
			int tamanho = 60
			montaCampo(cabecalhoBand, "Nascimento", detailBand, '$F{dt_nsc_func}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeAdmissao) {
			int tamanho = 60
			montaCampo(cabecalhoBand, "Admissao", detailBand, '$F{dt_adm}', positionReport, tamanho)
			positionReport += tamanho
		}
		
		if (exibeRescisao) {
			int tamanho = 60
			montaCampo(cabecalhoBand, "Rescisao", detailBand, '$F{dt_resc}', positionReport, tamanho)
			positionReport += tamanho
		}
		
	}
	
	private void montaCampo(JRDesignBand cabecalhoBand, String textoCabecalho, JRDesignBand detailBand, String campo, int x, int width) {
		JRDesignStaticText staticText = new JRDesignStaticText();
		staticText.setX(x);
		staticText.setY(0);
		staticText.setWidth(width);
		staticText.setHeight(20);
		staticText.setBold(true);
		staticText.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
		staticText.setText(textoCabecalho);
		cabecalhoBand.addElement(staticText)
		
		JRDesignTextField textField = new JRDesignTextField()
		textField.setX(x);
		textField.setY(0);
		textField.setWidth(width);
		textField.setHeight(20);
		textField.setBlankWhenNull(true);
		
		JRDesignExpression expression = new JRDesignExpression()
		expression.setValueClass(java.lang.String.class);
		expression.setText(campo);
		
		textField.setExpression(expression);
		detailBand.addElement(textField)
	}
	
	def generateSql(params) {
		
		def sql = ""
		
		sql = sql + "select f.mtr_func, f.crc_func, f.nom_func "
		sql = sql + "		,fil.dsc_fil, c.dsc_cargo, s.dsc_setor "
		sql = sql + "		,h.dsc_horario, che.descricao as horaextra "
		sql = sql + "		,to_char(f.dt_nsc_func, 'dd/MM/yyyy') AS dt_nsc_func "
		sql = sql + "		,to_char(f.dt_adm, 'dd/MM/yyyy') AS dt_adm "
		sql = sql + "		,to_char(f.dt_resc, 'dd/MM/yyyy') AS dt_resc "
		sql = sql + "from funcionario f "
		sql = sql + "inner join filial fil on f.filial_id = fil.id "
		sql = sql + "inner join cargo c on f.cargo_id = c.id "
		sql = sql + "inner join setor s on f.setor_id = s.id "
		sql = sql + "left join horario h on f.horario_id = h.id "
		sql = sql + "left join configuracao_hora_extra che on f.configuracao_hora_extra_id = che.id "
		sql = sql + "WHERE (1=1) "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		
		sql += "ORDER BY " + (params.ordenacao.toInteger() == 1 ? "f.mtr_func" : "f.nom_func")
		
		sql 
	}
	
	def generateReportName() {
		def reportName = "relatorioFuncionariosDinamico"
		
		if (params.orientacao != null && params.orientacao.equals("1")) {
			reportName = reportName.concat("Paisagem")
		}
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jrxml")
		
		reportName
	}
	
	@Transactional
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		String baseFolder = appContext.getResource("/").getFile().toString()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext),
			SUBREPORT_DIR: baseFolder.concat(File.separator).concat("reports").concat(File.separator),
			REPORT_CONNECTION: sessionFactory.currentSession.connection()
		]
		parameters
	}
	
}