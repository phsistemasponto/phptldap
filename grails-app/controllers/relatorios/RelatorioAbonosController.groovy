package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario
import cadastros.Justificativas
import cadastros.ParamRelatorioAbono
import cadastros.ParamRelatorioAbonoJustificativa

class RelatorioAbonosController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
		Usuario usuario = session['usuario']
		def filtros = ParamRelatorioAbono.findAllByUsuario(usuario)
		
		[filtros: filtros]
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		def dtInicioParam = formatBD.format(dtInicio)
		def dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
		
		def tipoRelatorio = params.tipoRelatorio
		def agrupamento = params.agrupamento
		
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			selectClause = selectClause + " f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome,  "
			selectClause = selectClause + " s.dsc_setor as setor, j.dsc_just as justificativa, a.nr_qtde_hr_justif as horas, "
			selectClause = selectClause + " to_char(a.dt_justificativa, 'dd/MM/yyyy') as data_justificativa, to_char(a.dt_inicio, 'dd/MM/yyyy') as data_batida, a.obs_justificativa as observacao "
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause =  selectClause + " fil.id as seq, fil.dsc_fil as descricao, j.dsc_just as justificativa, sum(a.nr_qtde_hr_justif) as horas "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil, j.dsc_just "
					break
				case "1":
					selectClause =  selectClause + " s.id as seq,s.dsc_setor as descricao, j.dsc_just as justificativa, sum(a.nr_qtde_hr_justif) as horas "
					groupByClause = groupByClause + " s.id, s.dsc_setor, j.dsc_just "
					break
				case "2":
					selectClause =  selectClause + " c.id as seq , c.dsc_cargo as descricao, j.dsc_just as justificativa, sum(a.nr_qtde_hr_justif) as horas "
					groupByClause = groupByClause + " c.id , c.dsc_cargo, j.dsc_just "
					break
				case "3":
					selectClause =  selectClause + " f.crc_func as seq, f.nom_func as descricao, j.dsc_just as justificativa, sum(a.nr_qtde_hr_justif) as horas "
					groupByClause = groupByClause + " f.crc_func, f.nom_func, j.dsc_just "
					break
			}
		}
		
		
		sql = selectClause
		sql = sql + " from abonos a "
		sql = sql + " join ocorrencias o on a.seq_ocor = o.id "
		sql = sql + " join justificativas j on a.seq_justif = j.id "
		sql = sql + " join funcionario f on a.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		
		sql = sql + " where (a.dt_inicio between '${dtInicioParam}' and '${dtFimParam}' or a.dt_fim between '${dtInicioParam}' and '${dtFimParam}') "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and j.id in ( " + params.justificativaId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		if (tipoRelatorio.equals("0")) {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
			sql = sql + " order by " + paramOrdenacao + ", data_batida "
		} else {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
			sql = sql + " order by " + paramOrdenacao 
		}
		
		sql  
	}
	
	def generateReportName() {
		def reportName = ""
		
		reportName = params.tipoRelatorio.equals("0") ? "relatorioAbonosAnalitico" : "relatorioAbonosSintetico"
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def generateParameters() {
		
		def agrupamento = ""
		switch (params.agrupamento) {
			case "0":
				agrupamento = "Filial"
				break
			case "1":
				agrupamento = "Departamento"
				break
			case "2":
				agrupamento = "Cargo"
				break
			case "3":
				agrupamento = "Funcion\u00E1rio"
				break
		}
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			AGRUPAMENTO: agrupamento,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
	def salvarFiltro() {
		
		def nomeFiltro = params.nomeFiltro
		def justificativas = params.justificativas.split(",")
		Usuario usuario = session['usuario']
		
		ParamRelatorioAbono parametro = new ParamRelatorioAbono()
		parametro.usuario = usuario
		parametro.descricao = nomeFiltro
		parametro.save(true)
		
		justificativas.each {
			Justificativas j = Justificativas.get(it.toLong())
			ParamRelatorioAbonoJustificativa justificativa = new ParamRelatorioAbonoJustificativa()
			justificativa.justificativa = j
			justificativa.param = parametro
			justificativa.save(true)
		}
		
		return render(text: [success:true] as JSON, contentType:'text/json')
	}
	
	def aplicarFiltro() {
		
		ParamRelatorioAbono paramRelat = ParamRelatorioAbono.get(params.paramRelat) 
		Set<ParamRelatorioAbonoJustificativa> justificativas = paramRelat.justificativas
		
		def result = []
		justificativas.each {
			result.add([id: it.justificativa.id,
				descricao: it.justificativa.dscJust])
		}
		
		render result as JSON
	}
	
}