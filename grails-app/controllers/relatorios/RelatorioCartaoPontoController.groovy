package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.SessionFactory
import org.springframework.context.ApplicationContext
import org.springframework.transaction.annotation.Transactional

import acesso.Usuario


@Transactional
class RelatorioCartaoPontoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	static transactional = true
	SessionFactory sessionFactory
	
	def index() {
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)
		
		def bytes = jasperService.generateReport(reportDef).toByteArray()
		
		def binaryBase64 = new String(Base64.encodeBase64(bytes))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		def dtInicioParam = formatBD.format(dtInicio)
		def dtFimParam = formatBD.format(dtFim)
		
		def sqlPrincipal = """
			select f.id as id_funcionario,
				  f.mtr_func as matricula,
				  f.crc_func as cracha_funcionario,
				  f.nom_func as nome_funcionario,
				  emp.dsc_emp as empresa,
				  fil.cnpj_fil as cnpj_empresa,
				  fil.atv_fil as atividade_empresa,
				  fil.dsc_log as endereco_empresa,
				  s.dsc_setor as setor,
				  c.dsc_cargo as cargo,
				  f.pis_func as pis,
				  to_char(f.dt_adm, 'dd/MM/yyyy') as data_admissao,
				  to_char(d.dt_bat, 'dd/MM/yyyy') as data_batida,
				  to_char(d.dt_bat, 'Dy') as data_batida2,
				  horario.dscperfil as horario,
				  d.hr_bat1 as horario_bat1,
				  coalesce(minu_to_hora(d.hr_bat1), '') as horario_bat1_str,
				  d.hr_bat2 as horario_bat2,
				  coalesce(minu_to_hora(d.hr_bat2), '') as horario_bat2_str,
				  d.hr_bat3 as horario_bat3,
				  coalesce(minu_to_hora(d.hr_bat3), '') as horario_bat3_str,
				  d.hr_bat4 as horario_bat4,
				  coalesce(minu_to_hora(d.hr_bat4), '') as horario_bat4_str,
				  d.hr_bat5 as horario_bat5,
				  coalesce(minu_to_hora(d.hr_bat5), '') as horario_bat5_str,
				  d.hr_bat6 as horario_bat6,
				  coalesce(minu_to_hora(d.hr_bat6), '') as horario_bat6_str,
				  coalesce(o1.dsc_ocor, o2.dsc_ocor, o3.dsc_ocor, o4.dsc_ocor) as ocorrencia,
				  ocbh.nr_qtde_hr AS saldo_bh, 
				  coalesce(minu_to_hora(cast(ocbh.nr_qtde_hr AS INTEGER)), '') AS saldo_bh_str, 
				  movext.proventos AS extras, 
				  coalesce(minu_to_hora(cast(movext.proventos AS INTEGER)), '') AS extras_str, 
				  coalesce(minu_to_hora(cast(movext.descontos AS INTEGER)), '') AS descontos, 
				  coalesce(minu_to_hora(cast(movadc.proventos_adc AS INTEGER)), '') AS adicional,  
				  juab.dsc_just as abono
			from dados d
			inner join funcionario f on d.seq_func = f.id
			inner join filial fil on f.filial_id = fil.id
			inner join empresa emp on fil.empresa_id = emp.id
			inner join setor s on f.setor_id = s.id
			inner join cargo c on f.cargo_id = c.id
			left join perfilhorario horario on d.id_perfil_horario = horario.id
			left join ocorrencias o1 on d.seq_ocor1 = o1.id
			left join ocorrencias o2 on d.seq_ocor2 = o2.id
			left join ocorrencias o3 on d.seq_ocor3 = o3.id
			left join ocorrencias o4 on d.seq_ocor4 = o4.id
			left join ocorrencias_bh ocbh on d.dt_bat = ocbh.dt_bat and d.seq_func = ocbh.seq_func
			LEFT JOIN v_ocorrencias movext ON movext.dt_bat = d.dt_bat AND movext.seq_func = f.id
			LEFT JOIN v_ocorrencias_adc movadc ON movadc.dt_bat = d.dt_bat AND movadc.seq_func = f.id
			left join abonos ab on ((d.dt_bat between ab.dt_inicio and ab.dt_fim) and ab.seq_func = f.id)
			left join justificativas juab on ab.seq_justif = juab.id
		"""
		
		def where = " where d.dt_bat between '${dtInicioParam}' and '${dtFimParam}' "
		where = where + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		
		def orderBy = " ORDER BY " + (params.ordenacao.toInteger() == 1 ? "f.mtr_func" : "f.nom_func")
		
		sqlPrincipal.concat(where).concat(orderBy)
	}
	
	@Transactional
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		String baseFolder = appContext.getResource("/").getFile().toString()
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		
		def parameters = [
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext),
			DT_INICIO: new SimpleDateFormat("yyyy-MM-dd").format(dtInicio),
			DT_FIM: new SimpleDateFormat("yyyy-MM-dd").format(dtFim),
			DATA_INICIO: new SimpleDateFormat("yyyy-MM-dd").format(dtInicio),
			DATA_FIM: new SimpleDateFormat("yyyy-MM-dd").format(dtFim),
			SUBREPORT_DIR: baseFolder.concat(File.separator).concat("reports").concat(File.separator),
			REPORT_CONNECTION: sessionFactory.currentSession.connection()
		]
		parameters
	}
	
	def generateReportName() {
		def reportName = ""
		if (params.relatorio.equals("0")) {
			reportName = "cartaoPonto"
		} else if (params.relatorio.equals("1")) {
			reportName = "listagemBatidas"
		}
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
}