package relatorios

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario
import funcoes.Horarios
import grails.converters.JSON
import groovy.sql.Sql

class RelatorioAdministrativoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		def dtInicioParam = formatBD.format(dtInicio)
		def dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
		
		if (params.relatorio.equals("0")) {
			sql = generateSqlFuncionariosPresentes(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("1")) {
			sql = generateSqlHorasTrabalhadas(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("2")) {
			sql = generateSqlLevantamentoFaltas(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("3")) {
			sql = generateSqlLevantamentoOcorrencias(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("4")) {
			sql = generateSqlFuncionariosSairamDepoisHorario(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("5")) {
			sql = generateSqlFuncionariosEntraramMaisCedo(dtInicioParam, dtFimParam)
		} 
		
		sql
	}
	
	def generateSqlFuncionariosPresentes(dtInicioParam, dtFimParam) {
		def sql =  " select fil.id as seq_filial " 
			sql += " 	   ,f.id as seq_func "
			sql += " 	   ,f.crc_func as cracha_func "
			sql += " 	   ,fil.dsc_fil as dsc_filial "
			sql += " 	   ,s.dsc_setor as dsc_setor "
			sql += " 	   ,c.dsc_cargo as dsc_cargo "
			sql += " 	   ,f.nom_func as nome_funcionario "
			sql += " 	   ,d.nr_qtde_ch_prevista as qtd_prevista "
			sql += " 	   ,d.nr_qtde_ch_efetivada as qtd_efetivada "
			sql += " 	   ,bat.hrbat as hora_batida "
			sql += " 	   ,to_char(d.dt_bat, 'dd/MM/yyyy') AS data_batida "
			sql += " from dados d "
			sql += " inner join funcionario f on d.seq_func = f.id "
			sql += " inner join filial fil on f.filial_id = fil.id "
			sql += " inner join cargo c on f.cargo_id = c.id "
			sql += " inner join setor s on f.setor_id = s.id "
			sql += " left join ( "
			sql += " select b.seq_func as func, b.dt_bat as dtbat, min(b.hr_bat) as hrbat from batidas b "
			sql += " where b.id_bat = 'E' " 
			sql += " and b.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += " group by b.seq_func, b.dt_bat "
			sql += " ) bat on bat.func = f.id and bat.dtbat = d.dt_bat "
			sql += " where bat.hrbat > 0 "
		if (params.verificacaoPresenca.equals("0")) {
			sql += " and d.id_tp_dia <> 'TR' "
		} else if (params.verificacaoPresenca.equals("1")) {
			sql += " and d.id_tp_dia = 'TR' "
		}
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " order by fil.dsc_fil, s.dsc_setor, f.id, d.dt_bat "
			
		sql
	}
	
	def generateSqlHorasTrabalhadas(dtInicioParam, dtFimParam) {
		
		def sql =  " select fil.id as seq_filial "
			sql += " 		,f.id as seq_func "
			sql += " 		,f.crc_func as cracha_func "
			sql += " 		,fil.dsc_fil as dsc_filial "
			sql += " 		,s.dsc_setor as dsc_setor "
			sql += " 		,c.dsc_cargo as dsc_cargo "
			sql += " 		,f.nom_func as nome_funcionario "
			sql += " 		,sum(d.nr_qtde_ch_efetivada) as qtd_efetivada "
			sql += " from dados d "
			sql += " inner join funcionario f on d.seq_func = f.id "
			sql += " inner join filial fil on f.filial_id = fil.id "
			sql += " inner join cargo c on f.cargo_id = c.id "
			sql += " inner join setor s on f.setor_id = s.id "
			sql += " where (1=1) "
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " group by fil.id "
			sql += " 		  ,f.id "
			sql += " 		  ,f.crc_func "
			sql += " 		  ,fil.dsc_fil "
			sql += " 		  ,s.dsc_setor "
			sql += " 		  ,c.dsc_cargo "
			sql += " 		  ,f.nom_func "
			sql += " order by fil.dsc_fil, s.dsc_setor, f.id "
			
		sql
	}
	
	def generateSqlLevantamentoFaltas(dtInicioParam, dtFimParam) {
		
		def sql =  " select f.id as id_func "
			sql += " 		,f.crc_func as crc_func "
			sql += " 		,fil.dsc_fil as filial "
			sql += " 		,s.dsc_setor as setor "
			sql += " 		,f.nom_func as nome "
			sql += " 		,o.dsc_ocor as ocorrencia "
			sql += " 		,to_char(m.dt_bat, 'dd/MM/yyyy') as data_batida "
			sql += " 		,m.nr_qtde_hr as horas "
			sql += " 		,j1.dsc_just as justificativa1 "
			sql += " 		,j2.dsc_just as justificativa2 "
			sql += " 		,to_char(ab.dt_justificativa, 'dd/MM/yyyy') as just_abono "
			sql += " 		,ab.obs_justificativa as obs_abono "
			sql += " 		,CASE m.nr_qtde_hr WHEN 0 THEN 1 else 0 end as tipo_falta "
			sql += " from movimentos m "
			sql += " inner join funcionario f on m.seq_func = f.id "
			sql += " inner join filial fil on fil.id = f.filial_id "
			sql += " inner join param_sistema_filial psf on psf.filial_id = fil.id "
			sql += " inner join param_sistema ps on psf.param_sistema_id = ps.id and m.seq_ocor = ps.ev_falta_total "
			sql += " inner join dados d on d.seq_func = m.seq_func and d.dt_bat = m.dt_bat "
			sql += " inner join setor s on f.setor_id = s.id "
			sql += " inner join ocorrencias o on m.seq_ocor = o.id "
			sql += " left join justificativas j1 on d.seq_justif1 = j1.id "
			sql += " left join justificativas j2 on d.seq_justif2 = j2.id "
			sql += " left join abonos ab on (ab.seq_func = f.id and (d.dt_bat between ab.dt_inicio and ab.dt_fim) and ab.seq_ocor = m.seq_ocor) "
			sql += " where (1=1) "
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " order by tipo_falta, fil.dsc_fil, s.dsc_setor, f.nom_func, d.dt_bat, m.nr_qtde_hr "
			
		sql
	}
	
	def generateSqlLevantamentoOcorrencias(dtInicioParam, dtFimParam) {
		
		def sql =  " select v.filial_id, v.dsc_setor, v.descricao, to_char(v.dt_bat, 'dd/MM/yyyy') as dt_bat, v.tipo, v.quantidade "
			sql += " from ( "
			sql += " 	( "
			sql += " 		select f.filial_id, s.dsc_setor, o.dsc_ocor as descricao, m.dt_bat, '1' as tipo, sum(m.nr_qtde_hr) as quantidade "
			sql += " 		from movimentos m "
			sql += " 		inner join funcionario f on m.seq_func = f.id "
			sql += " 		inner join setor s on f.setor_id = s.id "
			sql += " 		inner join ocorrencias o on m.seq_ocor = o.id "
			sql += " 	 	where (1=1) "
			sql += " 		and m.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 	 	group by f.filial_id, s.dsc_setor, o.dsc_ocor, m.dt_bat, 1 "
			sql += "  	) union ( "
			sql += " 	 	select f.filial_id, s.dsc_setor, j.dsc_just, a.dt_inicio as dt_bat, '2' as tipo, sum(a.nr_qtde_hr_justif) as quantidade "
			sql += " 		from abonos a "
			sql += " 		inner join funcionario f on a.seq_func = f.id "
			sql += " 		inner join setor s on f.setor_id = s.id "
			sql += " 		inner join justificativas j on a.seq_justif = j.id "
			sql += " 		where (1=1) "
			sql += " 		and (a.dt_inicio between '" + dtInicioParam + "' and '" + dtFimParam + "' or a.dt_fim between '" + dtInicioParam + "' and '" + dtFimParam + "') " 
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 		group by f.filial_id, s.dsc_setor, j.dsc_just, a.dt_inicio, 2 "
			sql += " 	) union ( "
			sql += " 	 	select f.filial_id, s.dsc_setor, c.dsc_class, i.dt_bat, '3'  as tipo, count(c.dsc_class) as quantidade "
			sql += " 		from itens_diario i "
			sql += " 		inner join funcionario f on i.seq_func = f.id "
			sql += " 		inner join classificacao c on i.seq_class = c.id "
			sql += " 		inner join setor s on f.setor_id = s.id "
			sql += " 		where (1=1) "
			sql += " 		and i.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 		group by f.filial_id, s.dsc_setor, c.dsc_class, i.dt_bat, 3 "
			sql += " 	) union ( "
			sql += " 		select f.filial_id, s.dsc_setor, j.dsc_just, a.data_inicio as dt_bat, '4' as tipo, count(a.funcionario_id) as quantidade "
			sql += " 		from afastamentos a "
			sql += " 		inner join funcionario f on a.funcionario_id = f.id "
			sql += " 		inner join setor s on f.setor_id = s.id "
			sql += " 		inner join justificativas j on a.justificativas_id = j.id "
			sql += " 		where (1=1) "
			sql += " 		and a.data_inicio between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 		group by f.filial_id, s.dsc_setor, j.dsc_just, a.data_inicio, 4 "
			sql += " 	) union ( "
			sql += " 		select f.filial_id, s.dsc_setor, 'HORAS_TRABALHADAS' as horas_trab, d.dt_bat, '5' as tipo, sum(d.nr_qtde_ch_efetivada) "
			sql += " 		from dados d "
			sql += " 		inner join funcionario f on d.seq_func = f.id "
			sql += " 		inner join setor s on f.setor_id = s.id "
			sql += " 		where (1=1) "
			sql += " 		and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 		group by f.filial_id, s.dsc_setor, d.dt_bat, 5 "
			sql += " 	) "
			sql += " ) v order by v.dsc_setor, v.filial_id, v.tipo "
			
		sql 
	}
	
	def generateSqlFuncionariosSairamDepoisHorario(dtInicioParam, dtFimParam) {
		
		def sql =  "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += "  "
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " order by  "
			
		sql
	}
	
	def generateSqlFuncionariosEntraramMaisCedo(dtInicioParam, dtFimParam) {
		
		def diferenca = Horarios.calculaMinutos(params.diferencaHoras)
		
		def sql =  " select distinct "
			sql += " 	  f.id as id_func "
			sql += " 	 ,f.mtr_func "
			sql += " 	 ,f.crc_func "
			sql += " 	 ,f.nom_func "
			sql += " 	 ,s.dsc_setor "
			sql += " 	 ,h.dsc_horario "
			sql += " 	 ,d.dt_bat "
			sql += " 	 ,d.hr_bat1 "
			sql += " 	 ,bat.hrbat "
			sql += " 	 ,bat.hrbat - d.hr_bat1 as diferenca "
			sql += " 	 ,d.id_tp_dia "
			sql += " 	 ,d.id_perfil_horario "
			sql += " 	 ,d.seq_horario "
			sql += " 	 ,c.dsc_cargo "
			sql += " 	 ,fil.dsc_fil "
			sql += " 	 ,CASE d.id_tp_dia "
			sql += " 	  WHEN 'FE' THEN 'FERIADO' "
			sql += " 	  WHEN 'DS' THEN 'DSR' "
			sql += " 	  WHEN 'CO' THEN 'COMPENSADO' "
			sql += " 	  WHEN 'FG' THEN 'FOLGA' "
			sql += " 	  ELSE 'TRABALHO' END AS dsc_class "
			sql += " from dados d "
			sql += " inner join funcionario f on d.seq_func = f.id "
			sql += " inner join filial fil on f.filial_id = fil.id "
			sql += " inner join setor s on f.setor_id = s.id "
			sql += " inner join cargo c on f.cargo_id = c.id "
			sql += " left join horario h on d.seq_horario = h.id "
			sql += " left join itemhorario ih on ih.horario_id = h.id "
			sql += " left join perfilhorario ph on ih.perfilhorario_id = ph.id "
			sql += " left join bathorario bat on bat.perfilhorario_id = ph.id and bat.seqbathorario = 1 "
			sql += " where (1=1) "
			sql += "   and d.hr_bat1 > 0 "
			sql += "   and d.id_tp_dia = 'TR' "
			sql += "   and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "   and (bat.hrbat - d.hr_bat1) > "
			sql += diferenca
			sql += " order by fil.dsc_fil,s.dsc_setor,f.nom_func,d.dt_bat "
			
		sql
	}
	
	def generateReportName() {
		def reportName = ""
		
		if (params.relatorio.equals("0")) {
			reportName = "relatorioFuncionariosPresentes"
		} else if (params.relatorio.equals("1")) {
			reportName = "relatorioHorasTrabalhadas"
		} else if (params.relatorio.equals("2")) {
			reportName = "relatorioLevantamentoFaltas"
		} else if (params.relatorio.equals("3")) {
			reportName = "relatorioGeralOcorrencias"
		} else if (params.relatorio.equals("4")) {
			reportName = "relatorioFuncionariosSairamDepoisHorario"
		} else if (params.relatorio.equals("5")) {
			reportName = "relatorioFuncionariosEntraramMaisCedo"
		}
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
}