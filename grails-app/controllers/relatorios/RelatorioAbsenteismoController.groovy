package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario
import cadastros.Justificativas
import cadastros.JustificativasAbsenteismoFaltas
import cadastros.JustificativasAbsenteismoSuspensao


class RelatorioAbsenteismoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
		[justificativasFaltas: JustificativasAbsenteismoFaltas.findAll(), justificativasSuspensao: JustificativasAbsenteismoSuspensao.findAll()]
	}
	
	def generateReport() {
		
		removerJustificativasFaltas()
		removerJustificativasSuspensao()
		salvarJustificativasFaltas()
		salvarJustificativasSuspensao()
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = null
		def dtFim = null
		def dtInicioParam = null
		def dtFimParam = null
		
		dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		dtInicioParam = formatBD.format(dtInicio)
		dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
		
		def tipoRelatorio = params.tipoRelatorio
		def agrupamento = params.agrupamento
		
		if (tipoRelatorio.equals("0")) {
			sql = generateSqlAbsenteismoAnalitico(dtInicioParam, dtFimParam)
		} else {
			sql = generateSqlAbsenteismoSintetico(dtInicioParam, dtFimParam)
		}
		
		sql 
	}
	
	def generateSqlAbsenteismoAnalitico(dtInicioParam, dtFimParam) {
		
		def sql =  "SELECT seq_fil "
			sql += "	  ,seq_Func "
			sql += "	  ,unicod_filial "
			sql += "	  ,nom_func "
			sql += "	  ,dsc_cargo "
			sql += "	  ,flt_justificadas "
			sql += "	  ,suspensao "
			sql += "	  ,flt_injustificadas "
			sql += "	  ,id_cid "
			sql += "	  ,dsc_cid "
			sql += "	  ,categoria "
			sql += "FROM ( "
			sql += "	SELECT f.filial_id as seq_fil "
			sql += "		  ,f.id as seq_func "
			sql += "		  ,c.unicod_filial "
			sql += "		  ,f.nom_func "
			sql += "		  ,d.dsc_cargo "
			sql += "		  ,y.flt_justificadas "
			sql += "		  ,v.suspensao "
			sql += "		  ,k.flt_injustificadas "
			sql += "		  ,'' AS id_cid "
			sql += "		  ,'' AS dsc_cid "
			sql += "		  ,'' AS categoria "
			sql += "	FROM funcionario f "
			sql += "	INNER JOIN filial c ON f.filial_id = c.id "
			sql += "	INNER JOIN cargo d ON f.cargo_id = d.id "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT a.seq_func, count(a.seq_ocor) AS flt_justificadas "
			sql += "		FROM v_falta_total_abonada a "
			sql += "		WHERE (1=1) "
			sql += "  		AND (a.dt_inicio between '" + dtInicioParam + "' and '" + dtFimParam + "' or a.dt_fim BETWEEN '" + dtInicioParam + "' and '" + dtFimParam + "' ) " 
			sql += "  		AND a.seq_justif IN  ( " + montaParametrosJustificativasFaltas() + " ) "
			sql += "		GROUP BY a.seq_func "
			sql += "	) y ON f.id = y.seq_func "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT a.seq_func, Count(a.nr_qtde_hr) AS suspensao "
			sql += "		FROM movimentos a "
			sql += "		INNER JOIN abonos b ON (a.seq_func = b.seq_func AND (a.dt_bat between b.dt_inicio and b.dt_fim) AND b.cid_id IS NULL) "
			sql += "		WHERE (1=1) "
			sql += "  		AND a.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += "  		AND b.seq_justif IN  ( " + montaParametrosJustificativasSuspensao() + " ) "
			sql += "		AND a.nr_qtde_hr > 0 "
			sql += "		GROUP BY a.seq_func "
			sql += "	) v ON f.id = v.seq_func "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT a.seq_func, Count(a.nr_qtde_hr) AS flt_injustificadas "
			sql += "		FROM movimentos a "
			sql += "		INNER JOIN funcionario f on a.seq_func = f.id "
			sql += "		WHERE (1=1) "
			sql += "  		AND a.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += "		AND a.seq_ocor = 1 "
			sql += "		AND a.nr_qtde_hr > 0 "
			sql += "		GROUP BY a.seq_func "
			sql += "	) k ON f.id = k.seq_func "
			sql += "	WHERE (1=1) "
			sql += 		filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "	UNION "
			sql += "	SELECT f.filial_id "
			sql += "		  ,a.seq_Func "
			sql += "		  ,c.unicod_filial "
			sql += "		  ,f.nom_func "
			sql += "		  ,d.dsc_cargo "
			sql += "		  ,1 as flt_justificadas "
			sql += "		  ,0 as suspensao "
			sql += "		  ,0 as flt_injustificadas "
			sql += "		  ,a.cid_id "
			sql += "		  ,l.dsc_cid || '   ' || just.dsc_just as justificativa "
			sql += "		  ,'' categoria "
			sql += "	FROM abonos a "
			sql += "	INNER JOIN funcionario f ON a.seq_func = f.id "
			sql += "	INNER JOIN filial c ON f.filial_id = c.id "
			sql += "	INNER JOIN cargo d ON f.cargo_id = d.id "
			sql += "	INNER JOIN param_sistema_filial psf on f.filial_id = psf.id "
			sql += "	INNER JOIN param_sistema ps ON psf.param_sistema_id = ps.id "
			if (detectDataBaseService.isPostgreSql()) {
				sql += "	LEFT JOIN cid l ON trim(Upper(cast(a.cid_id as varchar))) = Trim(Upper(cast(l.id as varchar))) "
			} else {
				sql += "	LEFT JOIN cid l ON a.cid_id = l.id "
			}
			sql += "	LEFT JOIN justificativas just ON a.seq_justif = just.id AND a.seq_justif <> 7 "
			sql += "	WHERE (1=1) "
			sql += "  	AND (a.dt_inicio between '" + dtInicioParam + "' and '" + dtFimParam + "' or a.dt_fim BETWEEN '" + dtInicioParam + "' and '" + dtFimParam + "' ) "
			sql += 		filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "	AND a.cid_id IS NOT NULL "
			sql += "	AND (a.seq_ocor = ps.ev_falta_total) "
			sql += ") "
			sql += "WHERE ((suspensao > 0) OR (flt_injustificadas > 0) OR (flt_justificadas > 0)) "
			
		sql += " order by " + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlAbsenteismoSintetico(dtInicioParam, dtFimParam) {
		
		def selectClause = ""
		def groupClause = ""
		
		def agrupamento = params.agrupamento
		
		switch (agrupamento) {
			case "0":
				selectClause = "SELECT dsc_fil as agrupamento, sum(flt_justificadas) as just, sum(suspensao) as susp, sum(flt_injustificadas) as injust, id_cid, dsc_cid, categoria "
				groupClause = "GROUP BY dsc_fil, id_cid, dsc_cid, categoria "
				break
			case "1":
				selectClause = "SELECT dsc_setor as agrupamento, sum(flt_justificadas) as just, sum(suspensao) as susp, sum(flt_injustificadas) as injust, id_cid, dsc_cid, categoria "
				groupClause = "GROUP BY dsc_setor, id_cid, dsc_cid, categoria "
				break
			case "2":
				selectClause = "SELECT dsc_cargo as agrupamento, sum(flt_justificadas) as just, sum(suspensao) as susp, sum(flt_injustificadas) as injust, id_cid, dsc_cid, categoria "
				groupClause = "GROUP BY dsc_cargo, id_cid, dsc_cid, categoria "
				break
			case "3":
				selectClause = "SELECT nom_func as agrupamento, sum(flt_justificadas) as just, sum(suspensao) as susp, sum(flt_injustificadas) as injust, id_cid, dsc_cid, categoria "
				groupClause = "GROUP BY nom_func, id_cid, dsc_cid, categoria "
				break
		}
		
		def sql = selectClause
			sql += "FROM ( "
			sql += "	SELECT f.filial_id as seq_fil "
			sql += "		  ,f.id as seq_func "
			sql += "		  ,c.unicod_filial "
			sql += "		  ,f.nom_func "
			sql += "		  ,c.dsc_fil "
			sql += "		  ,d.dsc_cargo "
			sql += "		  ,s.dsc_setor "
			sql += "		  ,y.flt_justificadas "
			sql += "		  ,v.suspensao "
			sql += "		  ,k.flt_injustificadas "
			sql += "		  ,'' AS id_cid "
			sql += "		  ,'' AS dsc_cid "
			sql += "		  ,'' AS categoria "
			sql += "	FROM funcionario f "
			sql += "	INNER JOIN filial c ON f.filial_id = c.id "
			sql += "	INNER JOIN cargo d ON f.cargo_id = d.id "
			sql += "	INNER JOIN setor s on f.setor_id = s.id "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT a.seq_func, count(a.seq_ocor) AS flt_justificadas "
			sql += "		FROM v_falta_total_abonada a "
			sql += "		WHERE (1=1) "
			sql += "  		AND a.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += "  		AND a.seq_justif IN  ( " + montaParametrosJustificativasFaltas() + " ) "
			sql += "		GROUP BY a.seq_func "
			sql += "	) y ON f.id = y.seq_func "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT a.seq_func, Count(a.nr_qtde_hr) AS suspensao "
			sql += "		FROM movimentos a "
			sql += "		INNER JOIN abonos b ON (a.seq_func = b.seq_func AND (a.dt_bat between b.dt_inicio and b.dt_fim) AND b.cid_id IS NULL) "
			sql += "		WHERE (1=1) "
			sql += "  		AND a.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += "  		AND b.seq_justif IN  ( " + montaParametrosJustificativasSuspensao() + " ) "
			sql += "		AND a.nr_qtde_hr > 0 "
			sql += "		GROUP BY a.seq_func "
			sql += "	) v ON f.id = v.seq_func "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT a.seq_func, Count(a.nr_qtde_hr) AS flt_injustificadas "
			sql += "		FROM movimentos a "
			sql += "		INNER JOIN funcionario f on a.seq_func = f.id "
			sql += "		WHERE (1=1) "
			sql += "  		AND a.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += "		AND a.seq_ocor = 1 "
			sql += "		AND a.nr_qtde_hr > 0 "
			sql += "		GROUP BY a.seq_func "
			sql += "	) k ON f.id = k.seq_func "
			sql += "	WHERE (1=1) "
			sql += 		filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "	UNION "
			sql += "	SELECT f.filial_id "
			sql += "		  ,a.seq_Func "
			sql += "		  ,c.unicod_filial "
			sql += "		  ,f.nom_func "
			sql += "		  ,c.dsc_fil "
			sql += "		  ,d.dsc_cargo "
			sql += "		  ,s.dsc_setor "
			sql += "		  ,1 as flt_justificadas "
			sql += "		  ,0 as suspensao "
			sql += "		  ,0 as flt_injustificadas "
			sql += "		  ,a.cid_id "
			sql += "		  ,l.dsc_cid || '   ' || just.dsc_just as justificativa "
			sql += "		  ,'' categoria "
			sql += "	FROM abonos a "
			sql += "	INNER JOIN funcionario f ON a.seq_func = f.id "
			sql += "	INNER JOIN filial c ON f.filial_id = c.id "
			sql += "	INNER JOIN cargo d ON f.cargo_id = d.id "
			sql += "	INNER JOIN setor s on f.setor_id = s.id "
			sql += "	INNER JOIN param_sistema_filial psf on f.filial_id = psf.id "
			sql += "	INNER JOIN param_sistema ps ON psf.param_sistema_id = ps.id "
			if (detectDataBaseService.isPostgreSql()) {
				sql += "	LEFT JOIN cid l ON trim(Upper(cast(a.cid_id as varchar))) = Trim(Upper(cast(l.id as varchar))) "
			} else {
				sql += "	LEFT JOIN cid l ON a.cid_id = l.id "
			}
			sql += "	LEFT JOIN justificativas just ON a.seq_justif = just.id AND a.seq_justif <> 7 "
			sql += "	WHERE (1=1) "
			sql += "  	AND a.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += 		filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "	AND a.cid_id IS NOT NULL "
			sql += "	AND (a.seq_ocor = ps.ev_falta_total) "
			sql += ") as sub "
			sql += "WHERE ((suspensao > 0) OR (flt_injustificadas > 0) OR (flt_justificadas > 0)) "
			sql += groupClause
			sql += "ORDER BY agrupamento "
			
		sql
	}
	
	def generateParameters() {
		
		def agrupamento = ""
		switch (params.agrupamento) {
			case "0":
				agrupamento = "Filial"
				break
			case "1":
				agrupamento = "Departamento"
				break
			case "2":
				agrupamento = "Cargo"
				break
			case "3":
				agrupamento = "Funcion\u00E1rio"
				break
		}
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			AGRUPAMENTO: agrupamento,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
	def generateReportName() {
		def reportName = ""
		
		if (params.tipoRelatorio.equals("0")) {
			reportName = "relatorioAbsenteismoAnalitico"
		} else {
			reportName = "relatorioAbsenteismoSintetico"
		}
			
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def montaParametrosJustificativasFaltas() {
		def justificativas = [0]
		if (params.justificativasFaltasId) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.justificativasFaltasId.getClass()) }
			justificativas = isArray ? params.justificativasFaltasId : [params.justificativasFaltasId]
		}
		
		return justificativas.toString().replace('[', ' ').replace(']', ' ')
	}
	
	def montaParametrosJustificativasSuspensao() {
		def justificativas = [0]
		if (params.justificativasSuspensaoId) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.justificativasSuspensaoId.getClass()) }
			justificativas = isArray ? params.justificativasSuspensaoId : [params.justificativasSuspensaoId]
		}
		
		return justificativas.toString().replace('[', ' ').replace(']', ' ')
	}
	
	def removerJustificativasFaltas() {
		JustificativasAbsenteismoFaltas.findAll().each {
			it.delete(flush: true)
		}
	}
	
	def removerJustificativasSuspensao() {
		JustificativasAbsenteismoSuspensao.findAll().each {
			it.delete(flush: true)
		}
	}
	
	def salvarJustificativasFaltas() {
		if (params.justificativasFaltasId) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.justificativasFaltasId.getClass()) }
			def justificativas = isArray ? params.justificativasFaltasId : [params.justificativasFaltasId]
			justificativas.each {
				Justificativas justificativa = Justificativas.get(it.toLong())
				if (justificativa != null) {
					JustificativasAbsenteismoFaltas ja = new JustificativasAbsenteismoFaltas()
					ja.justificativas = justificativa
					ja.save(flush: true)
				}
			}
		}
	}
	
	def salvarJustificativasSuspensao() {
		if (params.justificativasSuspensaoId) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.justificativasSuspensaoId.getClass()) }
			def justificativas = isArray ? params.justificativasSuspensaoId : [params.justificativasSuspensaoId]
			justificativas.each {
				Justificativas justificativa = Justificativas.get(it.toLong())
				if (justificativa != null) {
					JustificativasAbsenteismoSuspensao ja = new JustificativasAbsenteismoSuspensao()
					ja.justificativas = justificativa
					ja.save(flush: true)
				}
			}
		}
	}
	
}