package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.SessionFactory
import org.springframework.context.ApplicationContext
import org.springframework.transaction.annotation.Transactional

import acesso.Usuario

@Transactional
class RelatorioFuncionariosController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	SessionFactory sessionFactory
	
	def index() {
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		def sql = ""
				
		sql = sql + "SELECT f.id "
		sql = sql + "	   ,f.crc_func AS cracha "
		sql = sql + "	   ,f.mtr_func AS matricula "
		sql = sql + "	   ,f.nom_func AS nome "
		sql = sql + "	   ,d.dsc_fil || '-' || d.unicod_filial AS filial "
		sql = sql + "	   ,d.atv_fil "
		sql = sql + "	   ,d.dsc_log "
		sql = sql + "	   ,d.cnpj_fil "
		sql = sql + "	   ,b.dsc_setor AS setor "
		sql = sql + "	   ,c.dsc_cargo AS cargo "
		sql = sql + "	   ,u.dsc_uniorg "
		sql = sql + "	   ,h.dsc_horario AS horario "
		sql = sql + "	   ,hr.descricao AS extras "
		sql = sql + "	   ,f.pis_func "
		sql = sql + "	   ,to_char(f.dt_adm, 'dd/MM/yyyy') AS dt_adm "
		sql = sql + "	   ,to_char(f.dt_resc, 'dd/MM/yyyy') AS dt_resc "
		sql = sql + "FROM funcionario f "
		sql = sql + "INNER JOIN filial d ON f.filial_id = d.id "
		sql = sql + "INNER JOIN setor b ON f.setor_id = b.id "
		sql = sql + "INNER JOIN cargo c ON f.cargo_id = c.id "
		sql = sql + "LEFT JOIN uniorg u ON f.uniorg_id = u.id "
		sql = sql + "LEFT JOIN horario h ON f.horario_id = h.id "
		sql = sql + "LEFT JOIN ( "
		sql = sql + "	SELECT funcionario_id, y.descricao "
		sql = sql + "	FROM funcionario_hora_extra k "
		sql = sql + "	INNER JOIN configuracao_hora_extra y ON config_hora_extra_id = y.id "
		sql = sql + "	WHERE k.dt_ini_vig IN ( "
		sql = sql + "		SELECT max(dt_ini_vig) "
		sql = sql + "		FROM funcionario_hora_extra "
		sql = sql + "		WHERE funcionario_id = k.funcionario_id "
		sql = sql + "	) "
		sql = sql + ") hr ON f.id = hr.funcionario_id "
		sql = sql + "WHERE (1=1) "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		
		def relatorio = params.relatorio
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = null
		def dtFim = null
		def dtInicioParam = null
		def dtFimParam = null
		
		if (params.relatorio.equals("2") || params.relatorio.equals("3")) {
			dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
			dtInicioParam = formatBD.format(dtInicio)
			dtFimParam = formatBD.format(dtFim)
		}
		
		if (relatorio.equals("2")) {
			sql += "AND f.dt_adm between '" + dtInicioParam + "' and '" + dtFimParam + "' "
		} else if (relatorio.equals("3")) {
			sql += "AND f.dt_resc between '" + dtInicioParam + "' and '" + dtFimParam + "' "
		} else if (relatorio.equals("4")) {
			sql += "AND h.id is null "
		} else if (relatorio.equals("5")) {
			sql += "AND hr.descricao is null "
		}
		
		sql += "ORDER BY " + (params.ordenacao.toInteger() == 1 ? "f.id" : "f.nom_func")
		
		sql 
	}
	
	def generateReportName() {
		def reportName = ""
		
		switch (params.relatorio) {
			case "0":
			case "2":
			case "3":
			case "4":
			case "5":
				reportName = "relatorioFuncionarios"
				break
			case "1":
				reportName = "relatorioQuadroHorarios"
				break
		}
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	@Transactional
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		String baseFolder = appContext.getResource("/").getFile().toString()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext),
			SUBREPORT_DIR: baseFolder.concat(File.separator).concat("reports").concat(File.separator),
			REPORT_CONNECTION: sessionFactory.currentSession.connection()
		]
		parameters
	}
	
}