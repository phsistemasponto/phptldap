package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.SessionFactory
import org.springframework.context.ApplicationContext
import org.springframework.transaction.annotation.Transactional

import acesso.Usuario
import cadastros.Ocorrencias
import cadastros.ParamRelatorioOcorrencia
import cadastros.ParamRelatorioOcorrenciaOcorrencia

@Transactional
class RelatorioOcorrenciasController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	SessionFactory sessionFactory
	
	def index() {
		
		Usuario usuario = session['usuario']
		def filtros = ParamRelatorioOcorrencia.findAllByUsuario(usuario)
		
		[filtros: filtros]
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		def dtInicioParam = formatBD.format(dtInicio)
		def dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
				
		def tipoRelatorio = params.tipoRelatorio
		def agrupamento = params.agrupamento
		
		def tipoOcor = "'P', 'D'"
		if (params.relatorio.equals("0")) {
			tipoOcor = "'P'"
		} else if (params.relatorio.equals("1")) {
			tipoOcor = "'D'"
		}
		
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0") || params.relatorio.equals("3")) {
			selectClause = selectClause + " f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome,  "
			selectClause = selectClause + " h.dsc_horario as horario, o.dsc_ocor as ocorrencia, m.nr_qtde_hr as horas, to_char(m.dt_bat, 'dd/MM/yyyy') as data_batida, "
			selectClause = selectClause + " c.dsc_cargo as cargo, s.dsc_setor as setor, o.id_tp_ocor as tipo_ocorrencia, "
			selectClause = selectClause + " fil.dsc_fil as filial, u.dsc_uniorg as uniorg  "
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause =  selectClause + " fil.id as seq, fil.dsc_fil as descricao, o.dsc_ocor as ocorrencia, o.id_tp_ocor as tipo_ocorrencia, sum(m.nr_qtde_hr) as horas "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil, o.dsc_ocor, o.id_tp_ocor "
					break
				case "1":
					selectClause =  selectClause + " s.id as seq, s.dsc_setor as descricao, o.dsc_ocor as ocorrencia, o.id_tp_ocor as tipo_ocorrencia, sum(m.nr_qtde_hr) as horas "
					groupByClause = groupByClause + " s.id, s.dsc_setor, o.dsc_ocor, o.id_tp_ocor "
					break
				case "2":
					selectClause =  selectClause + " c.id as seq, c.dsc_cargo as descricao, o.dsc_ocor as ocorrencia, o.id_tp_ocor as tipo_ocorrencia, sum(m.nr_qtde_hr) as horas "
					groupByClause = groupByClause + " c.id , c.dsc_cargo, o.dsc_ocor, o.id_tp_ocor "
					break
				case "3":
					selectClause =  selectClause + " f.crc_func as seq, f.nom_func as descricao, o.dsc_ocor as ocorrencia, o.id_tp_ocor as tipo_ocorrencia, sum(m.nr_qtde_hr) as horas "
					groupByClause = groupByClause + " f.crc_func, f.nom_func, o.dsc_ocor, o.id_tp_ocor "
					break
			}
		}
		
		sql = sql.concat(selectClause)
		sql = sql + " from movimentos m "
		sql = sql + " inner join ocorrencias o on m.seq_ocor = o.id "
		sql = sql + " inner join dados d on d.seq_func = m.seq_func and d.dt_bat = m.dt_bat "
		sql = sql + " inner join horario h on d.seq_horario = h.id "
		sql = sql + " inner join funcionario f on m.seq_func = f.id "
		sql = sql + " inner join filial fil on f.filial_id = fil.id "
		sql = sql + " inner join setor s on f.setor_id = s.id "
		sql = sql + " inner join cargo c on f.cargo_id = c.id "
		sql = sql + " left join uniorg u on f.uniorg_id = u.id "
		
		sql = sql + " where m.dt_bat between '${dtInicioParam}' and '${dtFimParam}' "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		sql = sql + " and o.id_tp_ocor in ( " + tipoOcor + " ) "
		
		sql = sql + groupByClause
		
		if (tipoRelatorio.equals("0") || params.relatorio.equals("3")) {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
			sql = sql + " order by " + paramOrdenacao + ", data_batida "
		} else {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		}
		
		sql 
	}
	
	def generateReportName() {
		def reportName = ""
		if (params.relatorio.equals("0")) {
			reportName = params.tipoRelatorio.equals("0") ? "relatorioProventosAnalitico" : "relatorioProventosSintetico"
		} else if (params.relatorio.equals("1")) {
			reportName = params.tipoRelatorio.equals("0") ? "relatorioDescontosAnalitico" : "relatorioDescontosSintetico"
		} else if (params.relatorio.equals("2")) {
			reportName = params.tipoRelatorio.equals("0") ? "relatorioProventosDescontosAnalitico" : "relatorioProventosDescontosSintetico"
		} else if (params.relatorio.equals("3")) {
			reportName = "relatorioOcorrencias"
		}
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	@Transactional
	def generateParameters() {
		
		def agrupamento = ""
		switch (params.agrupamento) {
			case "0":
				agrupamento = "Filial"
				break
			case "1":
				agrupamento = "Departamento"
				break
			case "2":
				agrupamento = "Cargo"
				break
			case "3":
				agrupamento = "Funcion\u00E1rio"
				break
		}
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		String baseFolder = appContext.getResource("/").getFile().toString()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			AGRUPAMENTO: agrupamento,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext),
			SUBREPORT_DIR: baseFolder.concat(File.separator).concat("reports").concat(File.separator),
			REPORT_CONNECTION: sessionFactory.currentSession.connection()
		]
		parameters
	}
	
	def salvarFiltro() {
		
		def nomeFiltro = params.nomeFiltro
		def ocorrencias = params.ocorrencias.split(",")
		Usuario usuario = session['usuario']
		
		ParamRelatorioOcorrencia parametro = new ParamRelatorioOcorrencia()
		parametro.usuario = usuario
		parametro.descricao = nomeFiltro
		parametro.save(true)
		
		ocorrencias.each {
			Ocorrencias o = Ocorrencias.get(it.toLong())
			ParamRelatorioOcorrenciaOcorrencia ocorrencia = new ParamRelatorioOcorrenciaOcorrencia()
			ocorrencia.ocorrencia = o
			ocorrencia.param = parametro
			ocorrencia.save(true)
		}
		
		return render(text: [success:true] as JSON, contentType:'text/json')
	}
	
	def aplicarFiltro() {
		
		ParamRelatorioOcorrencia paramRelat = ParamRelatorioOcorrencia.get(params.paramRelat)
		Set<ParamRelatorioOcorrenciaOcorrencia> ocorrencias = paramRelat.ocorrencias
		
		def result = []
		ocorrencias.each {
			result.add([id: it.ocorrencia.id,
				descricao: it.ocorrencia.dscOcor,
				formula: it.ocorrencia.formulaOcor,
				tipo: it.ocorrencia.idTpOcor])
		}
		
		render result as JSON
	}
	
}