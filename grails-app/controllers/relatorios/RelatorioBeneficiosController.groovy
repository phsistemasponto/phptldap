package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario



class RelatorioBeneficiosController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray())).trim()
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = null
		def dtFim = null
		def dtInicioParam = null
		def dtFimParam = null
		
		dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		dtInicioParam = formatBD.format(dtInicio)
		dtFimParam = formatBD.format(dtFim)
		
		def tipoBeneficio = params.tipoBeneficio
		def beneficios = montaParametrosBeneficios()
		def naturezaBeneficios = montaParametrosNaturezaBeneficios()
		
		def sql = ""
		
		if (naturezaBeneficios.equals("1")) {
			sql = generateSqlBeneficiosNormais(dtInicioParam, dtFimParam, tipoBeneficio, beneficios)
		} else if (naturezaBeneficios.equals("2")) {
			sql = generateSqlBeneficiosExtras(dtInicioParam, dtFimParam, tipoBeneficio, beneficios)
		} else if (naturezaBeneficios.equals("1, 2")) {
			sql = generateSqlBeneficiosMisto(dtInicioParam, dtFimParam, tipoBeneficio, beneficios)
		}
		
		sql 
	}
	
	def generateSqlBeneficiosNormais(dtInicioParam, dtFimParam, tipoBeneficio, beneficios) {
		def sql =  "SELECT 1 AS tipo "
			sql += "	,f.mtr_func AS matricula "
			sql += "	,f.nom_func AS nome "
			sql += "	,c.descricao AS beneficios "
			sql += "	,to_char(a.dt_fim, 'dd/MM/yyyy') AS data "
			sql += "	,a.dias_uteis AS dias "
			sql += "	,a.quantidade "
			sql += "FROM beneficio_movimentos a "
			sql += "INNER JOIN funcionario f ON a.funcionario_id = f.id "
			sql += "INNER JOIN beneficio c ON a.beneficio_id = c.id "
			sql += "WHERE (1=1) "
			sql += "	AND c.tipo_de_beneficio_id = " + tipoBeneficio + " "
			sql += "	AND a.beneficio_id IN (" + beneficios + ") "
			sql += "	AND ((a.dt_inicio BETWEEN '" + dtInicioParam + "' and '" + dtFimParam + "') "
			sql += "		OR (a.dt_fim BETWEEN '" + dtInicioParam + "' and '" + dtFimParam + "' )) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			
		sql += " order by " + (params.ordenacao.toInteger() == 1 ? " matricula " : " nome ")
			
		sql
	}
	
	def generateSqlBeneficiosExtras(dtInicioParam, dtFimParam, tipoBeneficio, beneficios) {

		def sql =  "SELECT 2 AS tipo "
			sql += "	,f.mtr_func AS matricula "
			sql += "	,f.nom_func AS nome "
			sql += "	,c.descricao AS beneficios "
			sql += "	,to_char(a.dt_beneficio, 'dd/MM/yyyy') AS data "
			sql += "	,1 AS dias "
			sql += "	,a.quantidade "
			sql += "FROM funcionario_beneficio_extra a "
			sql += "INNER JOIN funcionario f ON a.funcionario_id = f.id "
			sql += "INNER JOIN beneficio c ON a.beneficio_id = c.id "
			sql += "WHERE (1=1) "
			sql += "	AND c.tipo_de_beneficio_id = " + tipoBeneficio + " "
			sql += "	AND a.beneficio_id IN (" + beneficios + ") "
			sql += "	AND a.dt_beneficio BETWEEN '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			
		sql += " order by " + (params.ordenacao.toInteger() == 1 ? " matricula " : " nome ")
		
		sql
	
	}
	
	def generateSqlBeneficiosMisto(dtInicioParam, dtFimParam, tipoBeneficio, beneficios) {
		
		def sqlNormal = generateSqlBeneficiosNormais(dtInicioParam, dtFimParam, tipoBeneficio, beneficios)
		def sqlExtra = generateSqlBeneficiosExtras(dtInicioParam, dtFimParam, tipoBeneficio, beneficios)
		
		def sqlMisto  = sqlNormal
			sqlMisto += " UNION "
			sqlMisto += sqlExtra
			
		sqlMisto
	}
	
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
	def generateReportName() {
		def reportName = "relatorioBeneficios"
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def montaParametrosBeneficios() {
		def beneficios = [0]
		if (params.beneficiosId) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.beneficiosId.getClass()) }
			beneficios = isArray ? params.beneficiosId : [params.beneficiosId]
		}
		
		return beneficios.toString().replace('[', ' ').replace(']', ' ').trim()
	}
	
	def montaParametrosNaturezaBeneficios() {
		def beneficios = [0]
		if (params.naturezaBeneficio) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.naturezaBeneficio.getClass()) }
			beneficios = isArray ? params.naturezaBeneficio : [params.naturezaBeneficio]
		}
		
		return beneficios.toString().replace('[', ' ').replace(']', ' ').trim()
	}
	
	
	
}