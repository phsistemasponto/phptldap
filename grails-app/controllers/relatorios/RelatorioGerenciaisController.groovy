package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario

class RelatorioGerenciaisController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null 
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
//		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
//		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
//		
//		def dtInicioParam = formatBD.format(dtInicio)
//		def dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
		
		if (params.relatorio.equals("0")) {
			if (params.pesquisa.equals("0")) {
				sql = generateSqlBancoHoras()
			} else if (params.pesquisa.equals("1")) {
				sql = generateSqlBancoHorasMoeda()
			} else if (params.pesquisa.equals("2")) {
				
			} else if (params.pesquisa.equals("3")) {
				sql = generateSqlQuantidadeBH(">")
			} else if (params.pesquisa.equals("4")) {
				sql = generateSqlQuantidadeBH("<")
			}
		
		}
		
		sql
	}
	
	def generateSqlBancoHoras() {
	
		def sql =  "SELECT filial_id "  
			sql += "	,filial "
			sql += "	,seq_pr "
			sql += "	,dsc_pr "
			sql += "	,dt_ini_pr "
			sql += "	,saldo_atual "
			sql += "FROM ( "
			sql += "	SELECT f.filial_id "
			sql += "		,fil.unicod_filial AS filial "
			sql += "		,sbh.seq_pr "
			sql += "		,p.dsc_pr "
			sql += "		,to_char(p.dt_ini_pr, 'dd/MM/yyyy') as dt_ini_pr "
			sql += "		,sum(sbh.vr_sld_atual) AS saldo_atual "
			sql += "	FROM periodo p "
			sql += "	INNER JOIN periodo_filial pf on pf.periodo_id = p.id "
			sql += "	INNER JOIN funcionario f on pf.filial_id = f.filial_id "
			sql += "	INNER JOIN filial fil on f.filial_id = fil.id "
			sql += "	INNER JOIN saldo_bh sbh on sbh.seq_func = f.id and sbh.seq_pr = pf.id and sbh.vr_sld_atual >=0 "
			sql += "	WHERE (1 = 1) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "  	  AND p.id in ( " + montaParametrosPeriodos() + " ) "
			sql += "	GROUP BY f.filial_id, fil.unicod_filial, sbh.seq_pr, p.dsc_pr, p.dt_ini_pr "
			sql += "	UNION "
			sql += "	SELECT f.filial_id "
			sql += "		,fil.unicod_filial AS filial "
			sql += "		,999999 AS seq_pr "
			sql += "		,'TOTAL' AS dsc_pr "
			sql += "		,'01/01/2040' "
			sql += "		,sum(sbh.vr_sld_atual) AS saldo_atual "
			sql += "	FROM periodo p "
			sql += "	INNER JOIN periodo_filial pf on pf.periodo_id = p.id "
			sql += "	INNER JOIN funcionario f on pf.filial_id = f.filial_id "
			sql += "	INNER JOIN filial fil on f.filial_id = fil.id "
			sql += "	INNER JOIN saldo_bh sbh on sbh.seq_func = f.id and sbh.seq_pr = pf.id and sbh.vr_sld_atual >=0 "
			sql += "	WHERE (1 = 1) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "	GROUP BY f.filial_id, fil.unicod_filial "
			sql += "	) as sub "
			sql += "ORDER BY filial,dt_ini_pr "
		
		sql
	}
	
	def generateSqlBancoHorasMoeda() {
		
		def sql =  "SELECT filial_id "
			sql += "	,filial "
			sql += "	,seq_func "
			sql += "	,periodo_id "
			sql += "	,dsc_pr "
			sql += "	,dt_ini_pr "
			sql += "	,vr_sal "
			sql += "	,formula_ocor "
			sql += "	,saldo_atual "
			sql += "	,valor "
			sql += "FROM ( "
			sql += "	SELECT f.filial_id "
			sql += "		,fil.unicod_filial AS filial "
			sql += "		,sbh.seq_func "
			sql += "		,pf.periodo_id "
			sql += "		,p.dsc_pr "
			sql += "		,to_char(p.dt_ini_pr, 'dd/MM/yyyy') as dt_ini_pr "
			sql += "		,f.vr_sal "
			sql += "		,vk.formula_ocor "
			sql += "		,sum(sbh.vr_sld_atual) AS saldo_atual "
			sql += "		,retornavalor(vk.formula_ocor, cast(f.vr_sal as numeric), cast((cast(sum(sbh.vr_sld_atual) as float)/60) as numeric)) AS valor "
			sql += "	FROM periodo p "
			sql += "	INNER JOIN periodo_filial pf on pf.periodo_id = p.id "
			sql += "	INNER JOIN filial fil on pf.filial_id = fil.id "
			sql += "	INNER JOIN funcionario f on f.filial_id = fil.id "
			sql += "	INNER JOIN saldo_bh sbh on sbh.seq_func = f.id and sbh.seq_pr = pf.id and sbh.vr_sld_atual >= 0 "
			sql += "	INNER JOIN v_funcionario_formulaextra vk ON sbh.seq_func = vk.seq_Func "
			sql += "	WHERE (1 = 1) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "  	  AND p.id in ( " + montaParametrosPeriodos() + " ) "
			sql += "	GROUP BY f.filial_id "
			sql += "		,fil.unicod_filial "
			sql += "		,sbh.seq_func "
			sql += "		,pf.periodo_id "
			sql += "		,p.dsc_pr "
			sql += "		,p.dt_ini_pr "
			sql += "		,f.vr_sal "
			sql += "		,vk.formula_ocor "
			sql += "	) as sub "
			sql += "WHERE saldo_atual > 0 "
			sql += "ORDER BY filial,dt_ini_pr "
		
		sql

	}
	
	def generateSqlQuantidadeBH(sinal) {
	
		def sql =  "SELECT f.filial_id "
			sql += "	,fil.unicod_filial "
			sql += "	,vk.seq_func "
			sql += "	,f.nom_func "
			sql += "	,c.dsc_cargo "
			sql += "	,s.apl_setor "
			sql += "	,f.vr_sal "
			sql += "	,vk.formula_ocor "
			sql += "	,sub.total AS saldo_atual "
			sql += "	,trunc(sub.total / 480) AS dias "
			sql += "	,retornavalor(vk.formula_ocor, cast(f.vr_sal as numeric), cast((cast(sum(sub.total) as float)/60) as numeric)) AS valor "
			sql += "	,((retornavalor(vk.formula_ocor, cast(f.vr_sal AS NUMERIC), cast((cast(sum(sub.total) AS FLOAT) / 60) AS NUMERIC))/f.vr_sal)*100) AS perc "
			sql += "FROM funcionario f "
			sql += "INNER JOIN filial fil on f.filial_id = fil.id "
			sql += "INNER JOIN cargo c on f.cargo_id = c.id "
			sql += "INNER JOIN setor s on f.setor_id = s.id "
			sql += "INNER JOIN v_funcionario_formulaextra vk ON vk.seq_func = f.id "
			sql += "LEFT JOIN ( "
			sql += "	SELECT s.seq_func, sum(s.vr_sld_atual) AS total "
			sql += "	FROM saldo_bh s "
			sql += "	GROUP BY s.seq_func "
			sql += "	) sub ON sub.seq_func = f.id "
			sql += "WHERE (1 = 1) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "	AND sub.total " + sinal +  " 0 "
			sql += "GROUP BY f.filial_id "
			sql += "	,fil.unicod_filial "
			sql += "	,vk.seq_func "
			sql += "	,f.nom_func "
			sql += "	,c.dsc_cargo "
			sql += "	,s.apl_setor "
			sql += "	,f.vr_sal "
			sql += "	,vk.formula_ocor "
			sql += "	,sub.total "
			sql += " ORDER BY sub.total " + (sinal.equals(">") ? " desc " : " asc ") 
		
		sql
		
	}
	
	def montaParametrosPeriodos() {
		def periodos = [0]
		if (params.periodosId) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.periodosId.getClass()) }
			periodos = isArray ? params.periodosId : [params.periodosId]
		}
		
		return periodos.toString().replace('[', ' ').replace(']', ' ')
	}
	
	def generateReportName() {
		def reportName = ""
		
		if (params.relatorio.equals("0")) {
			if (params.pesquisa.equals("0")) {
				reportName = "relatorioGerencialBancoHorasExcel"
			} else if (params.pesquisa.equals("1")) {
				reportName = "relatorioGerencialBancoHorasMoedaExcel"
			} else if (params.pesquisa.equals("2")) {
				
			} else if (params.pesquisa.equals("3")) {
				reportName = "relatorioGerencialQuantidadeBH"
			} else if (params.pesquisa.equals("4")) {
				reportName = "relatorioGerencialQuantidadeBH"
			}
		}
		
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	
	
	def generateParameters() {
		
		def agrupamento = ""
		switch (params.agrupamento) {
			case "0":
				agrupamento = "Filial"
				break
			case "1":
				agrupamento = "Departamento"
				break
			case "2":
				agrupamento = "Cargo"
				break
			case "3":
				agrupamento = "Funcion\u00E1rio"
				break
		}
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			AGRUPAMENTO: agrupamento,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
}