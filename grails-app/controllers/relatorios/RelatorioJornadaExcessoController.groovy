package relatorios

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario
import funcoes.Horarios
import grails.converters.JSON
import groovy.sql.Sql

class RelatorioJornadaExcessoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		def dtInicioParam = formatBD.format(dtInicio)
		def dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
		
		if (params.relatorio.equals("0")) {
			sql = generateSqlJornadaExcesso(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("1")) {
			sql = generateSqlJornadaSemFolga(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("2")) {
			sql = generateSqlExtrasAntesJornada(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("3")) {
			sql = generateSqlValorExtrasMaiorSalario(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("4")) {
			sql = generateSqlExtrasDepoisJornada(dtInicioParam, dtFimParam)
		} 
		
		sql
	}
	
	def generateSqlJornadaExcesso(dtInicioParam, dtFimParam) {
		def cargaHoraria = Horarios.calculaMinutos(params.cargaHoraria)
		
		def sql =  "  select f.filial_id "
			sql += "		,seq_func "
			sql += "		,f.nom_func "
			sql += "		,to_char(dt_bat, 'dd/MM/yyyy') as dt_bat "
			sql += "		,dsc_setor "
			sql += "		,total "
			sql += "		,hr_bat1 "
			sql += "		,hr_bat2 "
			sql += "		,dsc_cargo "
			sql += "		,dsc_Fil "
			sql += "		,dsc_uniorg "
			sql += "		,unicod_filial "
			sql += "		,to_char(f.dt_adm, 'dd/MM/yyyy') as dt_adm "
			sql += "   from ( "
			sql += "	   select f.filial_id "
			sql += "			,d.seq_func "
			sql += "			,f.nom_func "
			sql += "			,d.dt_bat "
			sql += "			,s.dsc_setor "
			sql += "			,CASE "
			sql += "			   WHEN (d.hr_bat2 > d.hr_bat1) THEN ((d.hr_bat2 - d.hr_bat1) - m.hr_inter) "
			sql += "			   WHEN (d.hr_bat2 < d.hr_bat1) THEN ((d.hr_bat2 + 1440 - d.hr_bat1) - m.hr_inter) "
			sql += "			   ELSE 0 "
			sql += "			END as total "
			sql += "			,d.hr_bat1 "
			sql += "			,d.hr_bat2 "
			sql += "			,c.dsc_cargo "
			sql += "			,fil.dsc_fil || '' || fil.unicod_filial as dsc_fil "
			sql += "			,u.dsc_uniorg "
			sql += "			,fil.unicod_filial "
			sql += "			,f.dt_adm "
			sql += "	   from dados d "
			sql += "	   inner join funcionario f on d.seq_func = f.id "
			sql += "	   inner join setor s on f.setor_id = s.id "
			sql += "	   inner join cargo c on f.cargo_id = c.id "
			sql += "	   inner join filial fil on f.filial_id = fil.id "
			sql += "	   inner join uniorg u on f.uniorg_id = u.id "
			sql += "	   left join ( "
			sql += "		   select perfilhorario_id, hr_inter, count(1) "
			sql += "			 from v_hr_inter "
			sql += "		  group by perfilhorario_id, hr_inter "
			sql += "	   ) m on d.id_perfil_horario = m.perfilhorario_id) as cons "
			sql += "   inner join funcionario f on cons.seq_func = f.id "
			sql += "   where (1=1) "
			sql += "   and hr_bat1 > 0  and hr_bat2 > 0 "
			sql += "   and total > " + cargaHoraria
			sql += "   and dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " order by 1,dsc_setor, "
			
		sql = sql + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlJornadaSemFolga(dtInicioParam, dtFimParam) {
		def sql =  " select f.filial_id "
			sql += " 	,d.seq_func "
			sql += " 	,f.nom_func "
			sql += " 	,c.dsc_cargo "
			sql += " 	,s.dsc_setor "
			sql += " 	,d.hr_bat1 "
			sql += " 	,d.hr_bat2 "
			sql += " 	,fil.dsc_fil || ' ' || fil.unicod_filial as dsc_fil "
			sql += " 	,to_char(d.dt_bat, 'dd/MM/yyyy') as dt_bat "
			sql += " from dados d "
			sql += " inner join funcionario f on d.seq_func = f.id "
			sql += " inner join cargo c on f.cargo_id = c.id "
			sql += " inner join setor s on f.setor_id = s.id "
			sql += " inner join filial fil on f.filial_id = fil.id "
			sql += " inner join uniorg u on f.uniorg_id = u.id "
			sql += " where (1=1) "
			sql += "   and d.id_tp_dia = 'DS' "
			sql += " 	and d.hr_bat1 > 0 and d.hr_bat2 > 0 "
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " order by 1,s.dsc_setor, "
			
		sql = sql + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlExtrasAntesJornada(dtInicioParam, dtFimParam) {
		
		def tolerancia = Horarios.calculaMinutos(params.tolerancia) 
		
		def sql =  " select filial_id "
			sql += " 	,seq_func "
			sql += " 	,nom_func "
			sql += " 	,dsc_fil "
			sql += " 	,dsc_cargo "
			sql += " 	,to_char(dt_bat, 'dd/MM/yyyy') as dt_bat "
			sql += " 	,hr_bat1 "
			sql += " 	,hr_bat2 "
			sql += " 	,entrada "
			sql += " 	,total "
			sql += " 	,dsc_uniorg "
			sql += " 	,vr_sal "
			sql += " 	,formula_ocor "
			sql += " 	,retornavalor(cast(formula_ocor as text), cast(vr_sal as numeric), cast((total/60) as numeric)) as valor "
			sql += " from ( "
			sql += " 	select f.filial_id "
			sql += " 		,d.seq_func "
			sql += " 		,f.nom_func "
			sql += " 		,fil.dsc_fil ||' '|| fil.unicod_filial as dsc_fil "
			sql += " 		,c.dsc_cargo "
			sql += " 		,d.dt_bat "
			sql += " 		,d.hr_bat1 "
			sql += " 		,d.hr_bat2 "
			sql += " 		,s.entrada "
			sql += " 		,s.entrada - d.hr_bat1 as total "
			sql += " 		,u.dsc_uniorg "
			sql += " 		,f.vr_sal "
			sql += " 		,fhe.formula_ocor "
			sql += " 	from dados d "
			sql += " 	inner join funcionario f on d.seq_func = f.id "
			sql += " 	inner join cargo c on f.cargo_id = c.id "
			sql += " 	inner join filial fil on f.filial_id = fil.id "
			sql += " 	inner join uniorg u on f.uniorg_id = u.id "
			sql += " 	inner join v_funcionario_formulaextra fhe on fhe.seq_func = f.id "
			sql += " 	left join ( "
			sql += " 		select f.filial_id as seq_fil "
			sql += " 			,d.seq_func "
			sql += " 			,d.dt_bat "
			sql += " 			,ih.horario_id "
			sql += " 			,bh.hrbat as entrada "
			sql += " 		from dados d "
			sql += " 		inner join funcionario f on d.seq_func = f.id "
			sql += " 		left join perfilhorario ph on d.id_perfil_horario = ph.id "
			sql += " 		left join itemhorario ih on ih.perfilhorario_id = ph.id "
			sql += " 		inner join bathorario bh on d.id_perfil_horario = bh.perfilhorario_id "
			sql += " 		where bh.seqbathorario = 1 "
			sql += " 		order by bh.seqbathorario "
			sql += " 	) s on d.seq_func = s.seq_func and d.dt_bat = s.dt_bat "
			sql += " 	where (1=1) "
			sql += " and d.hr_bat1 > 0 "
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " and (entrada - d.hr_bat1) > " + tolerancia + ") as func "
			
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlValorExtrasMaiorSalario(dtInicioParam, dtFimParam) {
		
		Sql bd = new Sql(dataSource)
		bd.execute("delete from rel_extras")
		
		def insert = """ insert into rel_extras 
						 select m.seq_func, sum(m.nr_qtde_hr) as total 
						 from movimentos m
						 inner join funcionario f on m.seq_func = f.id
						 inner join ocorrencias o on m.seq_ocor = o.id
						 where (1=1)
						 and o.indice_ext > 0
						 group by m.seq_func 
						 having sum(m.nr_qtde_hr) > 3600 
						 order by m.seq_func; """
		bd.executeInsert(insert)
		
		
		def sql =  " select f.filial_id "
			sql += " 	,m.seq_func "
			sql += " 	,f.nom_func "
			sql += " 	,m.seq_ocor "
			sql += " 	,f.vr_sal "
			sql += " 	,c.dsc_cargo "
			sql += " 	,fil.dsc_fil || ' ' || fil.unicod_filial as dsc_fil "
			sql += " 	,o.formula_ocor "
			sql += " 	,sum(m.nr_qtde_hr) as total "
			sql += " 	,retornavalor(cast(o.formula_ocor as text), cast(f.vr_sal as numeric), cast((sum(m.nr_qtde_hr)/60) as numeric)) as valor "
			sql += " from movimentos m "
			sql += " inner join funcionario f on m.seq_func = f.id "
			sql += " inner join cargo c on f.cargo_id = c.id "
			sql += " inner join filial fil on f.filial_id = fil.id "
			sql += " inner join ocorrencias o on m.seq_ocor = o.id "
			sql += " inner join uniorg u on f.uniorg_id = u.id "
			sql += " where (1=1) "
			sql += "   and o.indice_ext > 0 "
			sql += "   and m.seq_func in ( "
			sql += " 	select a.seq_func "
			sql += " 	from rel_extras a "
			sql += " 	inner join funcionario f on a.seq_func = f.id "
			sql += " 	where  (1=1) "
			sql += "   ) "
			sql += "   and m.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " group by f.filial_id "
			sql += " 	,m.seq_func "
			sql += " 	,f.nom_func "
			sql += " 	,m.seq_ocor "
			sql += " 	,f.vr_sal "
			sql += " 	,c.dsc_cargo "
			sql += " 	,fil.dsc_fil || ' ' || fil.unicod_filial "
			sql += " ,o.formula_ocor "
			sql += " order by m.seq_func "
			
		sql = sql + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlExtrasDepoisJornada(dtInicioParam, dtFimParam) {
		
		def tolerancia = Horarios.calculaMinutos(params.tolerancia)
		
		def sql =  " select f.filial_id "
			sql += " 	,d.seq_func "
			sql += " 	,f.nom_func "
			sql += " 	,fil.dsc_fil || ' ' || fil.unicod_filial as dsc_Fil "
			sql += " 	,c.dsc_cargo "
			sql += " 	,to_char(d.dt_bat, 'dd/MM/yyyy') as dt_bat "
			sql += " 	,d.hr_bat1 "
			sql += " 	,d.hr_bat2 "
			sql += " 	,s.saida "
			sql += " 	,d.hr_bat2 - s.saida as total "
			sql += " 	,u.dsc_uniorg "
			sql += " 	,f.vr_sal "
			sql += " 	,vfe.formula_ocor "
			sql += " 	,retornavalor(cast(vfe.formula_ocor as text), cast(f.vr_sal as numeric), cast(((d.hr_bat2 - s.saida)/60) as numeric)) as valor "
			sql += " from dados d "
			sql += " inner join funcionario f on d.seq_func = f.id "
			sql += " inner join cargo c on f.cargo_id = c.id "
			sql += " inner join filial fil on f.filial_id = fil.id "
			sql += " inner join uniorg u on f.uniorg_id = u.id "
			sql += " inner join v_funcionario_formulaextra vfe on vfe.seq_func = f.id "
			sql += " left join ( "
			sql += " 	select di.seq_func, di.dt_bat, bat.hrbat as saida "
			sql += " 	from dados di "
			sql += " 	left join perfilhorario ph on di.id_perfil_horario = ph.id "
			sql += " 	inner join v_batida_saida bat on di.id_perfil_horario = bat.perfilhorario_id "
			sql += " 	order by bat.seqbathorario "
			sql += " ) s on d.seq_func= s.seq_func and d.dt_bat = s.dt_bat "
			sql += " where (1=1) "
			sql += "   and  d.hr_bat2 > 0 "
			sql += "   and (d.hr_bat2 - saida ) > " + tolerancia
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " order by 1, "
			
		sql = sql + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateReportName() {
		def reportName = ""
		
		if (params.relatorio.equals("0")) {
			reportName = "relatorioJornadaExcesso"
		} else if (params.relatorio.equals("1")) {
			reportName = "relatorioJornadaSemFolga"
		} else if (params.relatorio.equals("2")) {
			reportName = "relatorioExtrasAntesJornada"
		} else if (params.relatorio.equals("3")) {
			reportName = "relatorioValorExtrasMaiorSalario"
		} else if (params.relatorio.equals("4")) {
			reportName = "relatorioExtrasDepoisJornada"
		} 
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
}