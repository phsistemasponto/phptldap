package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario
import cadastros.Ocorrencias
import cadastros.ParamRelatorioHorasExtras
import cadastros.ParamRelatorioHorasExtrasOcorrencia

class RelatorioExtrasController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
		Usuario usuario = session['usuario'] 
		def filtros = ParamRelatorioHorasExtras.findAllByUsuario(usuario)
		
		[filtros: filtros]
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		def dtInicioParam = formatBD.format(dtInicio)
		def dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
		
		def tipoRelatorio = params.tipoRelatorio
		def agrupamento = params.agrupamento
		
		if (params.relatorio.equals("0")) {
			sql = generateSqlExtras(tipoRelatorio, agrupamento, dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("1")) {
			sql = generateSqlExtrasComExcedente(tipoRelatorio, agrupamento, dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("2")) {
			sql = generateSqlExtrasComValorPagar(tipoRelatorio, agrupamento, dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("3")) {
			sql = generateSqlBancoHoras(tipoRelatorio, agrupamento, dtInicioParam, dtFimParam)
		} 
		
		sql 
	}
	
	def generateSqlExtras(tipoRelatorio, agrupamento, dtInicioParam, dtFimParam) {
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			selectClause = selectClause + " f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome,  "
			selectClause = selectClause + " s.dsc_setor as setor, o.dsc_ocor as ocorrencia, to_char(m.dt_bat, 'dd/MM/yyyy') as data_batida, m.NR_QTDE_HR as horas "
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause =  selectClause + " fil.id as seq, fil.dsc_fil as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil, o.dsc_ocor "
					break
				case "1":
					selectClause =  selectClause + " s.id as seq,s.dsc_setor as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " s.id, s.dsc_setor, o.dsc_ocor "
					break
				case "2":
					selectClause =  selectClause + " c.id as seq , c.dsc_cargo as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " c.id , c.dsc_cargo, o.dsc_ocor "
					break
				case "3":
					selectClause =  selectClause + " f.crc_func as seq, f.nom_func as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " f.crc_func, f.nom_func, o.dsc_ocor "
					break
			}
			
			selectClause =  selectClause + " coalesce(minu_to_hora( cast(sum(m.NR_QTDE_HR) as integer) ), '') as horas_str "
		}
		
		
		def sql = selectClause
		sql = sql + " from movimentos m "
		sql = sql + " join ocorrencias o on m.seq_ocor = o.id "
		sql = sql + " join funcionario f on m.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		
		sql = sql + " where m.dt_bat between '${dtInicioParam}' and '${dtFimParam}' "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		if (tipoRelatorio.equals("0")) {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
			sql = sql + " order by " + paramOrdenacao + ", data_batida "
		} else {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		}
		
		sql
	}
	
	def generateSqlExtrasComValorPagar(tipoRelatorio, agrupamento, dtInicioParam, dtFimParam) {
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			selectClause = selectClause + " f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome,  "
			selectClause = selectClause + " s.dsc_setor as setor, o.dsc_ocor as ocorrencia, o.formula_ocor as formula, to_char(m.dt_bat, 'dd/MM/yyyy') as data_batida, "
			selectClause = selectClause + " f.vr_sal as salario, m.NR_QTDE_HR as horas, "
			selectClause = selectClause + " CASE WHEN o.formula_ocor is null or o.formula_ocor = '' THEN 0 "
			selectClause = selectClause + "      ELSE retornavalor(o.formula_ocor, cast(f.vr_sal as numeric),  "
			if (detectDataBaseService.isPostgreSql()) {
				selectClause = selectClause + "      cast((m.NR_QTDE_HR::float/60) as numeric) "
			} else {
				selectClause = selectClause + "      cast((cast(m.NR_QTDE_HR as float)/60) as numeric) "
			}
			selectClause = selectClause + "      ) END as valor_pagar "
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause = selectClause + " fil.id as seq, fil.dsc_fil as descricao, f.id AS funcionario_id, f.vr_sal AS funcionario_salario, "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil, o.formula_ocor, o.dsc_ocor, f.id, f.vr_sal "
					break
				case "1":
					selectClause = selectClause + " s.id as seq,s.dsc_setor as descricao, f.id AS funcionario_id, f.vr_sal AS funcionario_salario, "
					groupByClause = groupByClause + " s.id, s.dsc_setor, o.formula_ocor, o.dsc_ocor, f.id, f.vr_sal "
					break
				case "2":
					selectClause = selectClause + " c.id as seq , c.dsc_cargo as descricao, f.id AS funcionario_id, f.vr_sal AS funcionario_salario, "
					groupByClause = groupByClause + " c.id , c.dsc_cargo, o.formula_ocor, o.dsc_ocor, f.id, f.vr_sal "
					break
				case "3":
					selectClause = selectClause + " f.crc_func as seq, f.nom_func as descricao, f.id AS funcionario_id, f.vr_sal AS funcionario_salario, "
					groupByClause = groupByClause + " f.crc_func, f.nom_func, o.formula_ocor, o.dsc_ocor, f.id, f.vr_sal "
					break
			}
			
			selectClause = selectClause + " o.formula_ocor as formula, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas,  "
			selectClause = selectClause + " coalesce(minu_to_hora( cast(sum(m.NR_QTDE_HR) as integer) ), '') as horas_str, "
			selectClause = selectClause + " CASE WHEN o.formula_ocor is null or o.formula_ocor = '' THEN 0 "
			selectClause = selectClause + "      ELSE retornavalor(o.formula_ocor, cast(f.vr_sal as numeric),  "
			if (detectDataBaseService.isPostgreSql()) {
				selectClause = selectClause + "      cast(sum((m.NR_QTDE_HR::float/60)) as numeric) "
			} else {
			selectClause = selectClause + "      cast(sum((cast(m.NR_QTDE_HR as float)/60)) as numeric) "
			}
			selectClause = selectClause + "      ) END as valor_pagar "
		}
		
		def sql = selectClause
		sql = sql + " from movimentos m "
		sql = sql + " join ocorrencias o on m.seq_ocor = o.id "
		sql = sql + " join funcionario f on m.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		sql = sql + " where m.dt_bat between '${dtInicioParam}' and '${dtFimParam}' "
		
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		if (tipoRelatorio.equals("0")) {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
			sql = sql + " order by " + paramOrdenacao + ", data_batida "
		} else {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		}
		
		
		sql
	}
	
	def generateSqlExtrasComExcedente(tipoRelatorio, agrupamento, dtInicioParam, dtFimParam) {
		def selectClause = " select " 
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			selectClause = selectClause + " f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome,  "
			selectClause = selectClause + " s.dsc_setor as setor, o.dsc_ocor as ocorrencia, to_char(m.dt_bat, 'dd/MM/yyyy') as data_batida, m.NR_QTDE_HR as horas, obh.nr_qtde_hr as horas_exc "
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause = selectClause + " fil.id as seq, fil.dsc_fil as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil, o.dsc_ocor "
					break
				case "1":
					selectClause = selectClause + " s.id as seq,s.dsc_setor as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " s.id, s.dsc_setor, o.dsc_ocor "
					break
				case "2":
					selectClause = selectClause + " c.id as seq , c.dsc_cargo as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " c.id , c.dsc_cargo, o.dsc_ocor "
					break
				case "3":
					selectClause = selectClause + " f.crc_func as seq, f.nom_func as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " f.crc_func, f.nom_func, o.dsc_ocor "
					break
			}
			
			selectClause = selectClause + " coalesce(minu_to_hora( cast(sum(m.NR_QTDE_HR) as integer) ), '') as horas_str, "
			selectClause = selectClause + " coalesce(sum(obh.nr_qtde_hr), 0) as horas_exec, "
			selectClause = selectClause + " coalesce(minu_to_hora( cast(sum(obh.nr_qtde_hr) as integer) ), '') as horas_exec_str "
		}
		
		def sql = selectClause
		sql = sql + " from movimentos m "
		sql = sql + " join ocorrencias o on m.seq_ocor = o.id "
		sql = sql + " join funcionario f on m.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		sql = sql + " left join ocorrencias_bh obh on obh.seq_func = f.id "
		sql = sql + " 							  and obh.dt_bat = m.dt_bat "
		sql = sql + " 							  and obh.seq_ocor = o.id "
		sql = sql + " 							  and obh.id_tp_ocorr = 'C' "
		sql = sql + " 							  and obh.id_tp_lanc = 'A' "
		
		sql = sql + " where m.dt_bat between '${dtInicioParam}' and '${dtFimParam}' "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		if (tipoRelatorio.equals("0")) {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
			sql = sql + " order by " + paramOrdenacao + ", data_batida "
		} else {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		}
		
		sql
	}
	
	def generateSqlBancoHoras(tipoRelatorio, agrupamento, dtInicioParam, dtFimParam) {
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			selectClause = selectClause + " f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome, f.vr_sal as salario, "
			selectClause = selectClause + " s.dsc_setor as setor, o.dsc_ocor as ocorrencia, o.formula_ocor as formula, "
			selectClause = selectClause + " to_char(m.dt_bat, 'dd/MM/yyyy') as data_batida, m.NR_QTDE_HR as horas "
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause = selectClause + " fil.id as seq, fil.dsc_fil as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil, o.dsc_ocor "
					break
				case "1":
					selectClause = selectClause + " s.id as seq,s.dsc_setor as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " s.id, s.dsc_setor, o.dsc_ocor "
					break
				case "2":
					selectClause = selectClause + " c.id as seq , c.dsc_cargo as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " c.id , c.dsc_cargo, o.dsc_ocor "
					break
				case "3":
					selectClause = selectClause + " f.crc_func as seq, f.nom_func as descricao, o.dsc_ocor as ocorrencia, sum(m.NR_QTDE_HR) as horas, "
					groupByClause = groupByClause + " f.crc_func, f.nom_func, o.dsc_ocor "
					break
			}
			
			selectClause = selectClause + " coalesce(minu_to_hora( cast(sum(m.NR_QTDE_HR) as integer) ), '') as horas_str "
		}
		
		def sql = selectClause
		sql = sql + " from ocorrencias_bh m "
		sql = sql + " join ocorrencias o on m.seq_ocor = o.id "
		sql = sql + " join funcionario f on m.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		
		sql = sql + " where m.dt_bat between '${dtInicioParam}' and '${dtFimParam}' "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		if (tipoRelatorio.equals("0")) {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
			sql = sql + " order by " + paramOrdenacao + ", data_batida "
		} else {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		}
		
		sql
	}
	
	def generateReportName() {
		def reportName = ""
		if (params.relatorio.equals("0")) {
			reportName = params.tipoRelatorio.equals("0") ? "relatorioExtrasAnalitico" : "relatorioExtrasSintetico"
		} else if (params.relatorio.equals("1")) {
			reportName = params.tipoRelatorio.equals("0") ? "relatorioExtrasComExcedenteAnalitico" : "relatorioExtrasComExcedenteSintetico"
		} else if (params.relatorio.equals("2")) {
			reportName = params.tipoRelatorio.equals("0") ? "relatorioExtrasComValorPagarAnalitico" : "relatorioExtrasComValorPagarSintetico"
		} else if (params.relatorio.equals("3")) {
			reportName = params.tipoRelatorio.equals("0") ? "relatorioBancoHorasAnalitico" : "relatorioBancoHorasSintetico"
		}
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def generateParameters() {
		
		def agrupamento = ""
		switch (params.agrupamento) {
			case "0":
				agrupamento = "Filial"
				break
			case "1":
				agrupamento = "Departamento"
				break
			case "2":
				agrupamento = "Cargo"
				break
			case "3":
				agrupamento = "Funcion\u00E1rio"
				break
		}
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			AGRUPAMENTO: agrupamento,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
	def salvarFiltro() {
		
		def nomeFiltro = params.nomeFiltro
		def ocorrencias = params.ocorrencias.split(",")
		Usuario usuario = session['usuario']
		
		ParamRelatorioHorasExtras parametro = new ParamRelatorioHorasExtras()
		parametro.usuario = usuario
		parametro.descricao = nomeFiltro
		parametro.save(true)
		
		ocorrencias.each {
			Ocorrencias o = Ocorrencias.get(it.toLong())
			ParamRelatorioHorasExtrasOcorrencia ocorrencia = new ParamRelatorioHorasExtrasOcorrencia()
			ocorrencia.ocorrencia = o
			ocorrencia.param = parametro
			ocorrencia.save(true)
		}
		
		return render(text: [success:true] as JSON, contentType:'text/json')
	}
	
	def aplicarFiltro() {
		
		ParamRelatorioHorasExtras paramRelat = ParamRelatorioHorasExtras.get(params.paramRelat) 
		Set<ParamRelatorioHorasExtrasOcorrencia> ocorrencias = paramRelat.ocorrencias
		
		def result = []
		ocorrencias.each {
			result.add([id: it.ocorrencia.id, 
				descricao: it.ocorrencia.dscOcor, 
				formula: it.ocorrencia.formulaOcor])
		}
		
		render result as JSON
	}
	
}