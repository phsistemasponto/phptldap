package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.SessionFactory
import org.springframework.context.ApplicationContext
import org.springframework.transaction.annotation.Transactional

import acesso.Usuario
import cadastros.Ocorrencias
import cadastros.ParamRelatorioFechamento
import cadastros.ParamRelatorioFechamentoOcorrencia;
import cadastros.ParamRelatorioHorasExtras
import cadastros.ParamRelatorioHorasExtrasOcorrencia
import cadastros.Periodo


@Transactional
class RelatorioFechamentoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	SessionFactory sessionFactory
	
	def index() {
		Usuario usuario = session['usuario']
		def filtros = ParamRelatorioFechamento.findAllByUsuario(usuario)
		
		[filtros: filtros]
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def periodo = Periodo.get(params.periodo)
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").format(periodo.dtIniPr)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").format(periodo.dtFimPr)
		
		def dtInicioParam = formatBD.format(periodo.dtIniPr)
		def dtFimParam = formatBD.format(periodo.dtFimPr)
		
		def sql = ""
		
		def tipoRelatorio = params.tipoRelatorio
		def agrupamento = params.agrupamento
		def format = params._format
		
		if (params.relatorio.equals("0")) {
			sql = format.equals("PDF") ? 
				generateSqlFechamentoDoMesPDF(tipoRelatorio, agrupamento, periodo.id) : 
				generateSqlFechamentoDoMesExcel(tipoRelatorio, agrupamento, periodo.id)
		} else if (params.relatorio.equals("1")) {
			sql = format.equals("PDF") ?
				generateSqlFechamentoMensalPDF(tipoRelatorio, agrupamento, periodo.id) :
				generateSqlFechamentoMensalExcel(tipoRelatorio, agrupamento, periodo.id)
		}
		
		sql 
	}
	
	def generateSqlFechamentoDoMesPDF(tipoRelatorio, agrupamento, periodoId) {
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			if (detectDataBaseService.isPostgreSql()) {
				selectClause = selectClause + " distinct f.mtr_func::int8 as seq, f.nom_func as descricao  "
			} else {
				selectClause = selectClause + " distinct cast(f.mtr_func as integer) as seq, f.nom_func as descricao  "
			}
			
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause =  selectClause + " distinct fil.id as seq, fil.dsc_fil as descricao "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil "
					break
				case "1":
					selectClause =  selectClause + " distinct s.id as seq, s.dsc_setor as descricao "
					groupByClause = groupByClause + " s.id, s.dsc_setor "
					break
				case "2":
					selectClause =  selectClause + " distinct c.id as seq, c.dsc_cargo as descricao "
					groupByClause = groupByClause + " c.id, c.dsc_cargo "
					break
				case "3":
					selectClause =  selectClause + " distinct f.crc_func as seq, f.nom_func as descricao "
					groupByClause = groupByClause + " f.crc_func, f.nom_func "
					break
			}
		}
		
		
		def sql = selectClause
		sql = sql + " from fechamento fec "
		sql = sql + " join periodo p on fec.seq_pr = p.id "
		sql = sql + " join funcionario f on fec.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		sql = sql + " join ocorrencias o on fec.seq_ocor = o.id "
		
		sql = sql + " where p.id = ${periodoId} "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
		
		sql
	}
	
	def generateSqlFechamentoDoMesExcel(tipoRelatorio, agrupamento, periodoId) {
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			selectClause = selectClause + " f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome,  "
			selectClause = selectClause + " s.dsc_setor as setor, o.dsc_ocor as ocorrencia, fec.nr_qt_horas as quant_horas, fec.nr_qt as quant_dias "
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause =  selectClause + " fil.id as seq, fil.dsc_fil as descricao, o.dsc_ocor as ocorrencia,  "
					selectClause =  selectClause + " sum(fec.nr_qt_horas) as quant_horas, sum(fec.nr_qt) as quant_dias "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil, o.dsc_ocor "
					break
				case "1":
					selectClause =  selectClause + " s.id as seq,s.dsc_setor as descricao, o.dsc_ocor as ocorrencia,  "
					selectClause =  selectClause + " sum(fec.nr_qt_horas) as quant_horas, sum(fec.nr_qt) as quant_dias "
					groupByClause = groupByClause + " s.id, s.dsc_setor, o.dsc_ocor "
					break
				case "2":
					selectClause =  selectClause + " c.id as seq , c.dsc_cargo as descricao, o.dsc_ocor as ocorrencia,  "
					selectClause =  selectClause + " sum(fec.nr_qt_horas) as quant_horas, sum(fec.nr_qt) as quant_dias "
					groupByClause = groupByClause + " c.id , c.dsc_cargo, o.dsc_ocor "
					break
				case "3":
					selectClause =  selectClause + " f.crc_func as seq, f.nom_func as descricao, o.dsc_ocor as ocorrencia, "
					selectClause =  selectClause + " sum(fec.nr_qt_horas) as quant_horas, sum(fec.nr_qt) as quant_dias "
					groupByClause = groupByClause + " f.crc_func, f.nom_func, o.dsc_ocor "
					break
			}
		}
		
		
		def sql = selectClause
		sql = sql + " from fechamento fec "
		sql = sql + " join periodo p on fec.seq_pr = p.id "
		sql = sql + " join funcionario f on fec.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		sql = sql + " join ocorrencias o on fec.seq_ocor = o.id "
		
		sql = sql + " where p.id = ${periodoId} "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		if (tipoRelatorio.equals("0") || params.relatorio.equals("3")) {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		} else {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		}
		
		sql
	}
	
	def generateSqlFechamentoMensalPDF(tipoRelatorio, agrupamento, periodoId) {
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			if (detectDataBaseService.isPostgreSql()) {
				selectClause = selectClause + " distinct f.mtr_func::int8 as seq, f.nom_func as descricao  "
			} else {
				selectClause = selectClause + " distinct cast(f.mtr_func as integer) as seq, f.nom_func as descricao  "
			}
			
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause =  selectClause + " distinct fil.id as seq, fil.dsc_fil as descricao "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil "
					break
				case "1":
					selectClause =  selectClause + " distinct s.id as seq, s.dsc_setor as descricao "
					groupByClause = groupByClause + " s.id, s.dsc_setor "
					break
				case "2":
					selectClause =  selectClause + " distinct c.id as seq, c.dsc_cargo as descricao "
					groupByClause = groupByClause + " c.id, c.dsc_cargo "
					break
				case "3":
					selectClause =  selectClause + " distinct f.crc_func as seq, f.nom_func as descricao "
					groupByClause = groupByClause + " f.crc_func, f.nom_func "
					break
			}
		}
		
		
		def sql = selectClause
		sql = sql + " from movimentos m "
		sql = sql + " join periodo p on m.dt_bat between p.dt_ini_pr and p.dt_fim_pr  "
		sql = sql + " join funcionario f on m.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		sql = sql + " join ocorrencias o on m.seq_ocor = o.id "
		
		sql = sql + " where p.id = ${periodoId} "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
		
		sql
	}
	
	def generateSqlFechamentoMensalExcel(tipoRelatorio, agrupamento, periodoId) {
		def selectClause = " select "
		def groupByClause = " group by "
		
		if (tipoRelatorio.equals("0")) {
			selectClause = selectClause + " f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome,  "
			selectClause = selectClause + " s.dsc_setor as setor, o.dsc_ocor as ocorrencia, to_char(m.dt_bat, 'dd/MM/yyyy') AS data_batida, m.nr_qtde_hr as quant_horas "
			groupByClause = " "
		} else {
			switch (agrupamento) {
				case "0":
					selectClause =  selectClause + " fil.id as seq, fil.dsc_fil as descricao, o.dsc_ocor as ocorrencia,  "
					selectClause =  selectClause + " sum(m.nr_qtde_hr) as quant_horas "
					groupByClause = groupByClause + " fil.id, fil.dsc_fil, o.dsc_ocor "
					break
				case "1":
					selectClause =  selectClause + " s.id as seq,s.dsc_setor as descricao, o.dsc_ocor as ocorrencia,  "
					selectClause =  selectClause + " sum(m.nr_qtde_hr) as quant_horas "
					groupByClause = groupByClause + " s.id, s.dsc_setor, o.dsc_ocor "
					break
				case "2":
					selectClause =  selectClause + " c.id as seq , c.dsc_cargo as descricao, o.dsc_ocor as ocorrencia,  "
					selectClause =  selectClause + " sum(m.nr_qtde_hr) as quant_horas "
					groupByClause = groupByClause + " c.id , c.dsc_cargo, o.dsc_ocor "
					break
				case "3":
					selectClause =  selectClause + " f.crc_func as seq, f.nom_func as descricao, o.dsc_ocor as ocorrencia, "
					selectClause =  selectClause + " sum(m.nr_qtde_hr) as quant_horas "
					groupByClause = groupByClause + " f.crc_func, f.nom_func, o.dsc_ocor "
					break
			}
		}
		
		
		def sql = selectClause
		sql = sql + " from movimentos m "
		sql = sql + " join periodo p on m.dt_bat between p.dt_ini_pr and p.dt_fim_pr  "
		sql = sql + " join funcionario f on m.seq_func = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id "
		sql = sql + " join ocorrencias o on m.seq_ocor = o.id "
		
		sql = sql + " where p.id = ${periodoId} "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + groupByClause
		
		
		if (tipoRelatorio.equals("0") || params.relatorio.equals("3")) {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		} else {
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
			sql = sql + " order by " + paramOrdenacao + ", ocorrencia "
		}
		
		sql
	}
	
	def generateReportName() {
		def reportName = ""
		if (params.relatorio.equals("0")) {
			if (params._format.equals("PDF")) {
				reportName = "relatorioFechamentoDoMes"
			} else {
				reportName = params.tipoRelatorio.equals("0") ? "relatorioFechamentoDoMesAnalitico" : "relatorioFechamentoDoMesSintetico"
			}
			
		} else if (params.relatorio.equals("1")) {
		
			if (params._format.equals("PDF")) {
				reportName = "relatorioFechamentoMensal"
			} else {
				reportName = params.tipoRelatorio.equals("0") ? "relatorioFechamentoMensalAnalitico" : "relatorioFechamentoMensalSintetico"
			}
		} 
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	@Transactional
	def generateParameters() {
		
		def filtroPadrao = filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		def clausePeriodo = " p.id = " + params.periodo.toLong()
		def clauseOcorrencias = " and o.id in ( " + params.ocorrenciasId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		def agrupamento = ""
		def clauseAgrupamento = ""
		def tipoAgrupamento = ""
		
		if (params.tipoRelatorio.equals("0")) {
			if (detectDataBaseService.isPostgreSql()) {
				clauseAgrupamento = " and f.mtr_func::int8 = "
			} else {
				clauseAgrupamento = " and f.mtr_func = "
			}
			tipoAgrupamento = "FUNCIONARIO"
		} else {
			switch (params.agrupamento) {
				case "0":
					agrupamento = "Filial"
					clauseAgrupamento = " and f.filial_id = "
					tipoAgrupamento = "FILIAL"
					break
				case "1":
					agrupamento = "Departamento"
					clauseAgrupamento = " and f.setor_id = "
					tipoAgrupamento = "SETOR"
					break
				case "2":
					agrupamento = "Cargo"
					clauseAgrupamento = " and f.cargo_id = "
					tipoAgrupamento = "CARGO"
					break
				case "3":
					agrupamento = "Funcion\u00E1rio"
					if (detectDataBaseService.isPostgreSql()) {
						clauseAgrupamento = " and f.mtr_func::int8 = "
					} else {
						clauseAgrupamento = " and f.mtr_func = "
					}
					
					tipoAgrupamento = "FUNCIONARIO"
					break
				default:
					agrupamento = "Funcion\u00E1rio"
					if (detectDataBaseService.isPostgreSql()) {
						clauseAgrupamento = " and f.mtr_func::int8 = "
					} else {
					clauseAgrupamento = " and f.mtr_func = "
					}
					
					tipoAgrupamento = "FUNCIONARIO"
					break
			}
		}
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		String baseFolder = appContext.getResource("/").getFile().toString()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			AGRUPAMENTO: agrupamento,
			TIPO_AGRUPAMENTO: tipoAgrupamento,
			CLAUSE_PERIODO: clausePeriodo,
			CLAUSE_AGRUPAMENTO: clauseAgrupamento,
			CLAUSE_OCORRENCIAS: clauseOcorrencias,
			FILTRO_PADRAO: filtroPadrao,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext),
			SUBREPORT_DIR: baseFolder.concat(File.separator).concat("reports").concat(File.separator),
			REPORT_CONNECTION: sessionFactory.currentSession.connection()
		]
		parameters
	}
	
	def salvarFiltro() {
		
		def nomeFiltro = params.nomeFiltro
		def ocorrencias = params.ocorrencias.split(",")
		Usuario usuario = session['usuario']
		
		ParamRelatorioFechamento parametro = new ParamRelatorioFechamento()
		parametro.usuario = usuario
		parametro.descricao = nomeFiltro
		parametro.save(true)
		
		ocorrencias.each {
			Ocorrencias o = Ocorrencias.get(it.toLong())
			ParamRelatorioFechamentoOcorrencia ocorrencia = new ParamRelatorioFechamentoOcorrencia()
			ocorrencia.ocorrencia = o
			ocorrencia.param = parametro
			ocorrencia.save(true)
		}
		
		return render(text: [success:true] as JSON, contentType:'text/json')
	}
	
	def aplicarFiltro() {
		
		ParamRelatorioFechamento paramRelat = ParamRelatorioFechamento.get(params.paramRelat)
		Set<ParamRelatorioFechamentoOcorrencia> ocorrencias = paramRelat.ocorrencias
		
		def result = []
		ocorrencias.each {
			result.add([id: it.ocorrencia.id,
				descricao: it.ocorrencia.dscOcor,
				formula: it.ocorrencia.formulaOcor])
		}
		
		render result as JSON
	}
	
}