package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario

class RelatorioInformativoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		def dtInicioParam = formatBD.format(dtInicio)
		def dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
		
		if (params.relatorio.equals("0")) {
			sql = generateSqlConferenciaFolha(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("1")) {
			sql = generateSqlFechamentoFolha(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("2")) {
			sql = generateSqlConferenciaFuncionariosQueTiveram(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("3")) {
			sql = generateSqlConferenciaFuncionariosQueNaoTiveram(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("4")) {
			sql = generateSqlConferenciaExcessoJornada(dtInicioParam, dtFimParam)
		} else if (params.relatorio.equals("5")) {
			if (params.agrupamento.equals("0")) {
				sql = generateSqlProdutividadeAnalitico(dtInicioParam, dtFimParam)
			} else {
				sql = generateSqlProdutividadeSintetico(dtInicioParam, dtFimParam)
			}
			
		} 
		
		sql
	}
	
	def generateSqlConferenciaFolha(dtInicioParam, dtFimParam) {
		
		def sql =  " SELECT seq_fil " 
			sql += " 	,seq_func "
			sql += " 	,nom_func "
			sql += " 	,extras1 "
			sql += " 	,extras2 "
			sql += " 	,credito "
			sql += " 	,debito "
			sql += " 	,saldo "
			sql += " 	,Saldobh "
			sql += " FROM ( "
			sql += " 	SELECT f.filial_id as seq_fil "
			sql += " 		,f.id as seq_func "
			sql += " 		,f.nom_func "
			sql += " 		,y.extras1 "
			sql += " 		,z.extras2 "
			sql += " 		,w.credito "
			sql += " 		,p.debito "
			if (detectDataBaseService.isPostgreSql()) {
				sql += " 		,(coalesce(w.credito, 0) - coalesce(p.debito, 0)) AS saldo "
			} else {
				sql += " 		,(nvl(w.credito, 0) - nvl(p.debito, 0)) AS saldo "
			}
			sql += " 		,g.saldobh "
			sql += " 	FROM funcionario f "
			sql += " 	INNER JOIN filial fil on f.filial_id = fil.id "
			sql += " 	LEFT JOIN ( "
			sql += " 		SELECT m.seq_func ,sum(m.nr_qtde_hr) AS extras1 "
			sql += " 		FROM movimentos m "
			sql += " 		INNER JOIN funcionario f on m.seq_func = f.id "
			sql += " 		INNER JOIN ocorrencias o on m.seq_ocor = o.id "
			sql += " 			AND o.id_tp_ocor = 'P' "
			sql += " 			AND o.indice_ext = 1 "
			sql += " 		WHERE (1 = 1) "
			sql += " 		  and m.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += 			  filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 		GROUP BY m.seq_func "
			sql += "		) y ON f.id = y.seq_func "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT m.seq_func ,sum(m.nr_qtde_hr) AS extras2 "
			sql += "		FROM movimentos m "
			sql += "		INNER JOIN funcionario f on m.seq_func = f.id "
			sql += "		INNER JOIN ocorrencias o on m.seq_ocor = o.id "
			sql += "			AND o.id_tp_ocor = 'P' "
			sql += "			AND o.indice_ext = 2 "
			sql += "		WHERE (1 = 1) "
			sql += " 		  and m.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += 			  filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "		GROUP BY m.seq_func "
			sql += "		) z ON f.id = z.seq_func "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT o.seq_func, sum(o.nr_qtde_hr) AS credito "
			sql += "		FROM ocorrencias_bh o "
			sql += "		INNER JOIN funcionario f on o.seq_func = f.id AND o.id_tp_ocorr = 'C' "
			sql += "		WHERE (1 = 1) "
			sql += " 		  and o.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += 			  filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "		GROUP BY o.seq_Func "
			sql += "		) W ON f.id = w.seq_func "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT o.seq_func, sum(o.nr_qtde_hr) AS debito "
			sql += "		FROM ocorrencias_bh o "
			sql += "		INNER JOIN funcionario f on o.seq_func = f.id AND o.id_tp_ocorr = 'C' "
			sql += "		WHERE (1 = 1) "
			sql += " 		  and o.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += 			  filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "		GROUP BY o.seq_Func "
			sql += "		) p ON f.id = p.seq_func "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT s.seq_func, sum(s.vr_sld_atual) AS saldobh "
			sql += "		FROM saldo_bh s "
			sql += "		INNER JOIN funcionario f on s.seq_func = f.id "
			sql += "		WHERE (1 = 1) "
			sql += 			  filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "		GROUP BY s.seq_func "
			sql += "		) g ON f.id = g.seq_func "
			sql += "	WHERE (1 = 1) "
			sql += 			  filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " ) a "
			
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlFechamentoFolha(dtInicioParam, dtFimParam) {
		def sql =  "  "
			sql += "  "
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " order by "
			
		sql
	}
	
	def generateSqlConferenciaFuncionariosQueTiveram(dtInicioParam, dtFimParam) {
		
		def sql =  " SELECT f.filial_id as seq_fil "
			sql += " 	,f.id as seq_func "
			sql += " 	,f.nom_func "
			sql += " 	,c.dsc_cargo "
			sql += " 	,s.dsc_setor "
			sql += " FROM funcionario f "
			sql += " INNER JOIN cargo c ON f.cargo_id = c.id "
			sql += " INNER JOIN setor s ON f.setor_id = s.id "
			sql += " WHERE (1 = 1) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 	AND f.id IN (SELECT fh.funcionario_id FROM funcionario_horario fh) "
			sql += " 	AND f.id IN ( "
			sql += " 		SELECT d.seq_func "
			sql += " 		FROM dados d "
			sql += " 		INNER JOIN funcionario f on d.seq_func = f.id "
			sql += " 		WHERE (1 = 1) "
			sql += 				filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 			AND d.id_tp_dia = '" + params.classificacao + "' "
			sql += " 			AND d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += " 			AND (d.hr_bat1 = 0 or d.hr_bat1 is null) "
			sql += " 		) "
			sql += " ORDER BY s.dsc_setor, "
			
		sql = sql + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlConferenciaFuncionariosQueNaoTiveram(dtInicioParam, dtFimParam) {
		def sql =  " SELECT f.filial_id as seq_fil "
			sql += " 	,f.id as seq_func "
			sql += " 	,f.nom_func "
			sql += " 	,c.dsc_cargo "
			sql += " 	,s.dsc_setor "
			sql += " FROM funcionario f "
			sql += " INNER JOIN cargo c ON f.cargo_id = c.id "
			sql += " INNER JOIN setor s ON f.setor_id = s.id "
			sql += " WHERE (1 = 1) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 	AND f.id IN (SELECT fh.funcionario_id FROM funcionario_horario fh) "
			sql += " 	AND f.id NOT IN ( "
			sql += " 		SELECT d.seq_func "
			sql += " 		FROM dados d "
			sql += " 		INNER JOIN funcionario f on d.seq_func = f.id "
			sql += " 		WHERE (1 = 1) "
			sql += 				filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 			AND d.id_tp_dia = '" + params.classificacao + "' "
			sql += " 			AND d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += " 			AND (d.hr_bat1 = 0 or d.hr_bat1 is null) "
			sql += " 		) "
			
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlConferenciaExcessoJornada(dtInicioParam, dtFimParam) {
		def sql =  "  "
			sql += "  "
			sql += " and d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " order by "
			
		sql
	}
	
	def generateSqlProdutividadeAnalitico(dtInicioParam, dtFimParam) {
		
		def sql =  " 	SELECT "
			sql += " 		to_char(d.dt_bat, 'dd/MM/yyyy') AS Data "
			sql += " 	   ,d.seq_func AS ID "
			sql += " 	   ,fil.unicod_filial AS unidade "
			sql += " 	   ,f.nom_func AS Nome "
			sql += " 	   ,c.dsc_cargo AS cargo "
			sql += " 	   ,u.DSC_UNIORG AS uniorg "
			sql += " 	   ,d.nr_qtde_ch_prevista AS carga_horaria_diaria "
			sql += " 	   ,d.nr_qtde_ch_efetivada AS carga_horaria_realizada "
			sql += " 	   ,CASE "
			sql += " 		   WHEN d.nr_qtde_ch_efetivada > d.nr_qtde_ch_prevista "
			sql += " 			   THEN (d.nr_qtde_ch_efetivada - d.nr_qtde_ch_prevista) "
			sql += " 		   ELSE 0 "
			sql += " 		   END AS Proventos "
			sql += " 	   ,CASE "
			sql += " 		   WHEN d.nr_qtde_ch_efetivada < nr_qtde_ch_prevista "
			sql += " 			   THEN d.nr_qtde_ch_prevista - d.nr_qtde_ch_efetivada "
			sql += " 		   ELSE 0 "
			sql += " 		   END AS descontos "
			sql += " 	   ,CASE "
			sql += " 		   WHEN d.nr_qtde_ch_efetivada - nr_qtde_ch_prevista > 120 "
			sql += " 			   THEN (d.nr_qtde_ch_efetivada - nr_qtde_ch_prevista) - 120 "
			sql += " 		   ELSE 0 "
			sql += " 		   END AS excesso "
			if (detectDataBaseService.isPostgreSql()) {
				sql += " 		,coalesce(v.saldo_bh, 0) AS saldo_bh "
			} else {
				sql += " 		,nvl(v.saldo_bh, 0) AS saldo_bh "
			}
			sql += "    FROM dados d "
			sql += "    INNER JOIN funcionario f on d.seq_func = f.id "
			sql += "    INNER JOIN filial fil on f.filial_id = fil.id "
			sql += "    INNER JOIN cargo c on f.cargo_id = c.id "
			sql += "    LEFT JOIN uniorg u on f.uniorg_id = u.id "
			sql += "    LEFT JOIN v_ocorrencias vo ON vo.seq_func = f.id AND vo.dt_bat = d.dt_bat "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT k.seq_func, sum(k.total) AS saldo_bh "
			sql += "		FROM v_saldo_bh_rel_ant k "
			sql += "		INNER JOIN funcionario f ON k.seq_func = f.id "
			sql += "			AND k.dt_ini_pr < '" + dtInicioParam + "' " 
			sql += "		GROUP BY k.seq_func "
			sql += "	) v ON d.seq_func = v.seq_func "
			sql += "    WHERE (1 = 1) "
			sql += 			  filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 	  AND d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " ID " : " NOME ")
			
		sql
	}
	
	def generateSqlProdutividadeSintetico(dtInicioParam, dtFimParam) {
		
		def sql =  " select id, unidade, nome, cargo, uniorg, saldo_bh, sum(carga_horaria_diaria) as carga_horaria_diaria,  "
			sql += " sum(carga_horaria_realizada)as carga_horaria_realizada,  "
			sql += " sum(proventos)as proventos,  "
			sql += " sum(descontos)as descontos,  "
			sql += " sum(excesso)as excesso  "
			sql += " from (  "
			sql += " 	SELECT "
			sql += " 		to_char(d.dt_bat, 'dd/MM/yyyy') AS Data "
			sql += " 	   ,d.seq_func AS ID "
			sql += " 	   ,fil.unicod_filial AS unidade "
			sql += " 	   ,f.nom_func AS Nome "
			sql += " 	   ,c.dsc_cargo AS cargo "
			sql += " 	   ,u.DSC_UNIORG AS uniorg "
			sql += " 	   ,d.nr_qtde_ch_prevista AS carga_horaria_diaria "
			sql += " 	   ,d.nr_qtde_ch_efetivada AS carga_horaria_realizada "
			sql += " 	   ,CASE "
			sql += " 		   WHEN d.nr_qtde_ch_efetivada > d.nr_qtde_ch_prevista "
			sql += " 			   THEN (d.nr_qtde_ch_efetivada - d.nr_qtde_ch_prevista) "
			sql += " 		   ELSE 0 "
			sql += " 		   END AS Proventos "
			sql += " 	   ,CASE "
			sql += " 		   WHEN d.nr_qtde_ch_efetivada < nr_qtde_ch_prevista "
			sql += " 			   THEN d.nr_qtde_ch_prevista - d.nr_qtde_ch_efetivada "
			sql += " 		   ELSE 0 "
			sql += " 		   END AS descontos "
			sql += " 	   ,CASE "
			sql += " 		   WHEN d.nr_qtde_ch_efetivada - nr_qtde_ch_prevista > 120 "
			sql += " 			   THEN (d.nr_qtde_ch_efetivada - nr_qtde_ch_prevista) - 120 "
			sql += " 		   ELSE 0 "
			sql += " 		   END AS excesso "
			if (detectDataBaseService.isPostgreSql()) {
				sql += " 		,coalesce(v.saldo_bh, 0) AS saldo_bh "
			} else {
				sql += " 		,nvl(v.saldo_bh, 0) AS saldo_bh "
			}
			sql += "    FROM dados d "
			sql += "    INNER JOIN funcionario f on d.seq_func = f.id "
			sql += "    INNER JOIN filial fil on f.filial_id = fil.id "
			sql += "    INNER JOIN cargo c on f.cargo_id = c.id "
			sql += "    LEFT JOIN uniorg u on f.uniorg_id = u.id "
			sql += "    LEFT JOIN v_ocorrencias vo ON vo.seq_func = f.id AND vo.dt_bat = d.dt_bat "
			sql += "	LEFT JOIN ( "
			sql += "		SELECT k.seq_func, sum(k.total) AS saldo_bh "
			sql += "		FROM v_saldo_bh_rel_ant k "
			sql += "		INNER JOIN funcionario f ON k.seq_func = f.id "
			sql += "			AND k.dt_ini_pr < '" + dtInicioParam + "' "
			sql += "		GROUP BY k.seq_func "
			sql += "	) v ON d.seq_func = v.seq_func "
			sql += "    WHERE (1 = 1) "
			sql += 			  filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += " 	  AND d.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += " ) a GROUP BY id,unidade,nome,cargo,uniorg,saldo_bh "
			
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " ID " : " NOME ")
			
		sql
	}
	
	def generateRel1(func, dtInicioParam, dtFimParam) {
		
	}
	
	def generateReportName() {
		def reportName = ""
		
		if (params.relatorio.equals("0")) {
			reportName = "relatorioConferenciaFolha"
		} else if (params.relatorio.equals("1")) {
			reportName = "relatorioFechamentoFolha"
		} else if (params.relatorio.equals("2")) {
			reportName = "relatorioConferenciaFuncionariosQueTiveram"
		} else if (params.relatorio.equals("3")) {
			reportName = "relatorioConferenciaFuncionariosQueNaoTiveram"
		} else if (params.relatorio.equals("4")) {
			reportName = "relatorioConferenciaExcessoJornada"
		} else if (params.relatorio.equals("5")) {
			if (params.agrupamento.equals("0")) {
				reportName = "relatorioProdutividade"
			} else {
				reportName = "relatorioProdutividadeSintetico"
			}
			
		}
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
}