package relatorios

import grails.converters.JSON
import groovy.sql.Sql
import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.SessionFactory
import org.springframework.context.ApplicationContext
import org.springframework.transaction.annotation.Transactional

import acesso.Usuario

@Transactional
class RelatorioCadastraisController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def relatoriosService
	def jasperService
	def dataSource
	SessionFactory sessionFactory
	
	def index() {
		
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		def sql = ""
				
		def relatorio = params.relatorio
		
		if (relatorio.equals("0")) {
			sql = "select id as seq, dsc_fil as dsc, unicod_filial, cnpj_fil, dsc_log, atv_fil from filial order by " + params.ordenacao
		} else if (relatorio.equals("1")) {
			sql = "select id as seq, dsc_setor as dsc from setor order by " + params.ordenacao
		} else if (relatorio.equals("2")) {
			sql = "select id as seq, dsc_cargo as dsc from cargo order by " + params.ordenacao
		} else if (relatorio.equals("3")) {
			sql = "select id as seq, dsc_just as dsc, id_tp_just as tpjust from justificativas order by " + params.ordenacao
		}		
		
		sql 
	}
	
	def generateReportName() {
		def reportName = ""
		if (params.relatorio.equals("0")) {
			reportName = "relatorioFiliais"
		} else if (params.relatorio.equals("1")) {
			reportName = "relatorioCadastrais"
		} else if (params.relatorio.equals("2")) {
			reportName = "relatorioCadastrais"
		} else if (params.relatorio.equals("3")) {
			reportName = "relatorioJustificativas"
		}  
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	@Transactional
	def generateParameters() {
		
		def titulo = ""
		switch (params.relatorio) {
			case "0":
				titulo = "Relatório de Filiais"
				break
			case "1":
				titulo = "Relatório de Setores"
				break
			case "2":
				titulo = "Relatório de Cargos"
				break
			case "3":
				titulo = "Relatório de Justificativas"
				break
		}
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		String baseFolder = appContext.getResource("/").getFile().toString()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			TITULO: titulo,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext),
			SUBREPORT_DIR: baseFolder.concat(File.separator).concat("reports").concat(File.separator),
			REPORT_CONNECTION: sessionFactory.currentSession.connection()
		]
		parameters
	}
	
}