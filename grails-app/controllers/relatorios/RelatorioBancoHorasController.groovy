package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario
import cadastros.Ocorrencias
import cadastros.ParamRelatorioBancoHoras
import cadastros.ParamRelatorioBancoHorasOcorrencia


class RelatorioBancoHorasController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
		Usuario usuario = session['usuario']
		
		def mesesPadrao = 6
		params.meses = mesesPadrao
		
		def filiais
		def periodos
		
		filiais = usuario.getFiliais() 
		
		if (filiais.size() == 1) {
			def sqlPeriodos = """
							 select p.id as id, p.apel_pr as \"apelPr\" 
							 from periodo p 
							 inner join periodo_filial pf on pf.periodo_id = p.id
							 inner join parametro_periodo_filial ppf on ppf.filial_id = pf.filial_id
							 inner join parametro_periodo pp on ppf.parametro_periodo_id = pp.id and pp.dia_inicio = date_part('day', p.dt_ini_pr)
							 where (1=1) 
                          """
			sqlPeriodos += " and pf.filial_id in (" + filtroPadraoService.montaFiltroFiliaisDoUsuario() + ")"
			sqlPeriodos += " order by p.dt_ini_pr desc "
			sqlPeriodos += " limit " + mesesPadrao
			
			Sql sql = new Sql(dataSource)
			periodos = sql.rows(sqlPeriodos)
		}
		
		def filtros = ParamRelatorioBancoHoras.findAllByUsuario(usuario)
		
		[filiais: filiais, periodos: periodos, filtros: filtros]
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray())).trim()
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = null
		def dtFim = null
		def dtInicioParam = null
		def dtFimParam = null
		
		if (params.relatorio.equals("0")) {
			dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
			dtInicioParam = formatBD.format(dtInicio)
			dtFimParam = formatBD.format(dtFim)
		}
		
		def sql = ""
		
		def tipoRelatorio = params.tipoRelatorio
		def agrupamento = params.agrupamento
		
		if (params.relatorio.equals("0")) {
			if (tipoRelatorio.equals("0")) {
				sql = generateSqlBancoHorasNormalAnalitico(dtInicioParam, dtFimParam)
			} else {
				sql = generateSqlBancoHorasNormalSintetico(agrupamento, dtInicioParam, dtFimParam)
			}
		} else if (params.relatorio.equals("1")) {
			sql = generateSqlPlanilhaSaldoBancoHoras()
		} else if (params.relatorio.equals("2")) {
			sql = generateSqlPagamentoBancoHoras()
		} else if (params.relatorio.equals("3")) {
			sql = generateSqlMovimentacaoFechamentoBancoHoras()
		} else if (params.relatorio.equals("4")) {
			sql = generateSqlSaldoBancoSemFazerFechamento()
		} else if (params.relatorio.equals("5")) {
			sql = generateSqlComparacaoDeBancoDeHoras()
		}  
		
		sql 
	}
	
	def generateSqlBancoHorasNormalAnalitico(dtInicioParam, dtFimParam) {
		
		def sql =  "SELECT a.seq_func " 
			sql += "	  ,f.mtr_func "
			sql += "	  ,f.nom_func "
			sql += "	  ,f.crc_func "
			sql += "	  ,f.setor_id AS seq_setor "
			sql += "	  ,k.dsc_setor "
			sql += "	  ,o.dsc_ocor "
			sql += "	  ,to_char(a.dt_bat, 'dd/MM/yyyy') as dt_bat "
			sql += "	  ,to_char(a.dt_bat, 'Dy') as dt_bat2 "
			sql += "	  ,a.NR_QTDE_HR "
			sql += "	  ,a.id_tp_lanc "
			sql += "	  ,a.id_tp_ocorr "
			sql += "	  ,a.observacao "
			sql += "FROM ocorrencias_bh a "
			sql += "INNER JOIN ocorrencias o ON a.SEQ_OCOR = o.id "
			sql += "INNER JOIN funcionario f ON f.id = a.seq_func "
			sql += "INNER JOIN filial e ON f.filial_id = e.id "
			sql += "INNER JOIN cargo c ON c.id = f.cargo_id "
			sql += "INNER JOIN setor k ON k.id = f.setor_id "
			sql += "WHERE (1 = 1) "
			sql += "  AND a.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
			sql += "  AND a.id_tp_ocorr in ( " + montaParametrosTipoOcorrencias() + " ) "
			sql += "  AND o.id in ( " + montaParametrosOcorrencias() + " ) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			
		sql
	}
	
	def generateSqlBancoHorasNormalSintetico(agrupamento, dtInicioParam, dtFimParam) {
		
		def sql =  "SELECT "
		
		def selectClause = ""
		def groupClause = ""
		
		if (agrupamento.equals("0")) {
			selectClause = " f.filial_id as sequencia, e.dsc_fil as descricao "
			groupClause = " f.filial_id, e.dsc_fil "
		} else if (agrupamento.equals("1")) {
			selectClause = " f.setor_id as sequencia, k.dsc_setor as descricao  "
			groupClause = " f.setor_id, k.dsc_setor  "
		} else if (agrupamento.equals("2")) {
			selectClause = " f.cargo_id as sequencia, c.dsc_cargo as descricao  "
			groupClause = " f.cargo_id, c.dsc_cargo "
		} else if (agrupamento.equals("3")) {
			selectClause = " a.seq_func as sequencia, f.nom_func as descricao  "
			groupClause = " a.seq_func, f.nom_func "
		}
		
		sql += selectClause
		
		sql += "	,sum(CASE a.id_tp_ocorr WHEN 'C' THEN a.NR_QTDE_HR ELSE 0 END) "
		sql += "	- sum(CASE a.id_tp_ocorr WHEN 'D' THEN a.NR_QTDE_HR ELSE 0 END) AS NR_QTDE_HR "
		sql += "FROM ocorrencias_bh a "
		sql += "INNER JOIN ocorrencias o ON a.SEQ_OCOR = o.id "
		sql += "INNER JOIN funcionario f ON f.id = a.seq_Func "
		sql += "INNER JOIN filial e ON f.filial_id = e.id "
		sql += "INNER JOIN cargo c ON c.id = f.cargo_id "
		sql += "INNER JOIN setor k ON k.id = f.setor_id "
		sql += "WHERE (1=1) "
		sql += "  AND a.dt_bat between '" + dtInicioParam + "' and '" + dtFimParam + "' "
		sql += "  AND a.id_tp_ocorr in ( " + montaParametrosTipoOcorrencias() + " ) "
		sql += "  AND o.id in ( " + montaParametrosOcorrencias() + " ) "
		sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql += "GROUP BY "
		sql += groupClause
		
		sql += " order by " + (params.ordenacao.toInteger() == 1 ? " sequencia " : " descricao ")
			
		sql
		
	}
	
	def generateSqlPlanilhaSaldoBancoHoras() {
		
		def sql = "SELECT f.filial_id AS seq_fil "
			sql += "	 ,f.id AS seq_func "
			sql += "	 ,f.nom_func "
			sql += "	 ,a.id AS seq_pr "
			sql += "	 ,a.apel_pr "
			sql += "	 ,k.dsc_setor "
			sql += "	 ,c.dsc_cargo "
			sql += "	 ,f.vr_sal "
			sql += "	 ,s.saldo_atual "
			sql += "	 ,s.saldo_anterior "
			sql += "	 ,s.credito "
			sql += "	 ,s.debito "
			sql += "	 ,s.pagamento "
			sql += "	 ,a.dt_ini_pr "
			sql += "	 ,vk.formula_ocor "
			sql += "	 ,CASE WHEN s.saldo_atual is null "
			sql += "	 THEN 0 "
			if (detectDataBaseService.isPostgreSql()) {
				sql += "	 ELSE retornavalor(vk.formula_ocor, cast(f.vr_sal as numeric), cast((s.saldo_atual::float/60) as numeric)) END as valor_pagar "
			} else {
				sql += "	 ELSE retornavalor(vk.formula_ocor, cast(f.vr_sal as numeric), cast((cast(s.saldo_atual as float)/60) as numeric)) END as valor_pagar "
			}
			
			sql += "	 ,s.saldo_mes_1 "
			sql += "FROM periodo a "
			sql += "INNER JOIN periodo_filial b ON a.id = periodo_id "
			sql += "INNER JOIN funcionario f ON b.filial_id = f.filial_id "
			sql += "INNER JOIN filial e ON b.filial_id = e.id "
			sql += "INNER JOIN cargo c ON c.id = f.cargo_id "
			sql += "INNER JOIN v_funcionario_formulaextra vk ON f.id = vk.seq_Func "
			sql += "INNER JOIN setor k ON k.id = f.setor_id "
			sql += "LEFT JOIN ( "
			sql += "	SELECT e.seq_func "
			sql += "		  ,e.seq_pr "
			sql += "		  ,e.vr_sld_atual AS saldo_atual "
			sql += "		  ,e.VR_SLD_ANT AS saldo_anterior "
			sql += "		  ,e.VR_CRED AS credito "
			sql += "		  ,e.VR_DEB AS debito "
			sql += "		  ,e.VR_PAGO AS pagamento "
			sql += "		  ,e.vr_sld_mes AS saldo_mes_1 "
			sql += "	FROM saldo_Bh e "
			sql += ") s ON f.id = s.seq_func AND a.id = s.seq_pr "
			sql += "WHERE (1=1) "
			sql += "  AND a.id in ( " + montaParametrosPeriodos() + " ) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "UNION "
			sql += "SELECT e.id AS seq_fil "
			sql += "	  ,f.id AS seq_func "
			sql += "	  ,f.nom_func "
			sql += "	  ,0 AS seq_pr "
			sql += "	  ,'ANT ' AS dsc_pr "
			sql += "	  ,k.dsc_setor "
			sql += "	  ,c.dsc_cargo "
			sql += "	  ,f.vr_sal "
			sql += "	  ,t.total AS saldo_atual "
			sql += "	  ,0 AS saldo_anterior "
			sql += "	  ,0 AS credito "
			sql += "	  ,0 AS debito "
			sql += "	  ,0 AS pagamento "
			sql += "	  ,cast('01/01/1999' AS DATE) AS dt_ini_pr "
			sql += "	  ,vk.formula_ocor "
			sql += "	  ,CASE WHEN t.total is null "
			sql += "	  THEN 0 "
			if (detectDataBaseService.isPostgreSql()) {
				sql += "	  ELSE retornavalor(vk.formula_ocor, cast(f.vr_sal as numeric), cast((t.total::float/60) as numeric)) END as valor_pagar "
			} else {
				sql += "	  ELSE retornavalor(vk.formula_ocor, cast(f.vr_sal as numeric), cast((cast(t.total as float)/60) as numeric)) END as valor_pagar "
			}
			sql += "	  ,0 AS saldo_mes_1 "
			sql += "FROM funcionario f "
			sql += "INNER JOIN filial e ON f.filial_id = e.id "
			sql += "INNER JOIN cargo c ON c.id = f.cargo_id "
			sql += "INNER JOIN setor k ON k.id = f.setor_id "
			sql += "INNER JOIN v_funcionario_formulaextra vk ON f.id = vk.seq_Func "
			sql += "LEFT JOIN ( "
			sql += "	SELECT z.seq_func "
			sql += "		  ,sum(z.vr_sld_atual) AS total "
			sql += "	FROM saldo_bh z "
			sql += "	INNER JOIN periodo w ON z.seq_pr = w.id "
			sql += "	WHERE (1=1) "
			sql += "      AND w.id not in ( " + montaParametrosPeriodos() + " ) "
			sql += "	GROUP BY z.seq_func "
			sql += ") t ON f.id = t.seq_func "
			sql += "WHERE (1=1) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "UNION "
			sql += "SELECT f.filial_id AS seq_fil "
			sql += "	  ,f.id AS seq_func "
			sql += "	  ,f.nom_func "
			sql += "	  ,0 AS seq_pr "
			sql += "	  ,'TOTAL ' AS dsc_pr "
			sql += "	  ,k.dsc_setor "
			sql += "	  ,c.dsc_cargo "
			sql += "	  ,f.vr_sal "
			sql += "	  ,w.total AS saldo_atual "
			sql += "	  ,0 AS saldo_anterior "
			sql += "	  ,0 AS credito "
			sql += "	  ,0 AS debito "
			sql += "	  ,0 AS pagamento "
			sql += "	  ,cast('01/01/2040' AS DATE) AS dt_ini_pr "
			sql += "	  ,vk.formula_ocor "
			sql += "	  ,CASE WHEN w.total is null "
			sql += "	  THEN 0 "
			if (detectDataBaseService.isPostgreSql()) {
				sql += "	  ELSE retornavalor(vk.formula_ocor, cast(f.vr_sal as numeric), cast((w.total::float/60) as numeric)) END as valor_pagar "
			} else {
				sql += "	  ELSE retornavalor(vk.formula_ocor, cast(f.vr_sal as numeric), cast((cast(w.total as float)/60) as numeric)) END as valor_pagar "
			}
			
			sql += "	  ,0 AS saldo_mes_1 "
			sql += "FROM funcionario f "
			sql += "INNER JOIN filial e ON f.filial_id = e.id "
			sql += "INNER JOIN cargo c ON c.id = f.cargo_id "
			sql += "INNER JOIN setor k ON k.id = f.setor_id "
			sql += "INNER JOIN v_funcionario_formulaextra vk ON f.id = vk.seq_Func "
			sql += "LEFT JOIN ( "
			sql += "	SELECT z.seq_func "
			sql += "		  ,sum(z.vr_sld_atual) AS total "
			sql += "	FROM saldo_bh z "
			sql += "	GROUP BY z.seq_func "
			sql += ") w ON f.id = w.seq_func "
			sql += "WHERE (1=1) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			
			def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
			sql = sql + " order by " + paramOrdenacao + ", dt_ini_pr "
		
			sql
		
	}
	
	def generateSqlPagamentoBancoHoras() {
		
		def sql =  "SELECT v.seq_func AS seq "
			sql += "	  ,f.nom_func AS descricao "
			sql += "	  ,a.apel_pr "
			sql += "	  ,v.vr_hr "
			sql += "	  ,v.qt_hr "
			sql += "FROM pg_saldo_bh v "
			sql += "INNER JOIN saldo_bh e ON v.seq_func = e.seq_func AND v.SEQ_PR = e.SEQ_PR "
			sql += "INNER JOIN funcionario f ON f.id = v.SEQ_FUNC "
			sql += "INNER JOIN setor g ON g.id = f.setor_id "
			sql += "INNER JOIN cargo h ON h.id = f.cargo_id "
			sql += "INNER JOIN periodo_filial k ON k.id = v.seq_pr "
			sql += "INNER JOIN periodo a ON a.id = k.periodo_id "
			sql += "WHERE (1=1) "
			sql += "  AND a.id in ( " + montaParametrosPeriodos() + " ) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			
		def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq " : " descricao ")
		sql = sql + " order by " + paramOrdenacao + ", a.dt_ini_pr "
			
		sql
	}
	
	def generateSqlMovimentacaoFechamentoBancoHoras() {
		
		def sql =  "SELECT a.seq_func "
			sql += "	  ,f.nom_func "
			sql += "	  ,a.SEQ_PR_PROC "
			sql += "	  ,d.DSC_PR "
			sql += "	  ,o1.DSC_PR AS periodo1 "
			sql += "	  ,a.NR_QTDE_HR "
			sql += "	  ,a.ID_TP_LANC "
			sql += "	  ,a.OBSERVACAO "
			sql += "FROM ocorrencias_sld_bh a "
			sql += "INNER JOIN periodo_filial pfa ON a.seq_pr_proc = pfa.id "
			sql += "INNER JOIN periodo_filial pfB ON a.seq_pr = pfb.id "
			sql += "INNER JOIN periodo d ON pfa.periodo_id = d.id "
			sql += "INNER JOIN periodo o1 ON pfb.periodo_id = o1.id "
			sql += "INNER JOIN funcionario f ON f.id = a.SEQ_FUNC "
			sql += "INNER JOIN setor g ON g.id = f.setor_id "
			sql += "INNER JOIN cargo h ON h.id = f.cargo_id "
			sql += "WHERE (1=1) "
			sql += "  AND d.id in ( " + montaParametrosPeriodos() + " ) "
			sql += "  AND o1.id in ( " + montaParametrosPeriodos() + " ) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			
		def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
		sql = sql + " order by " + paramOrdenacao + ", d.dt_ini_pr, o1.dt_ini_pr "
			
		sql
	}
	
	def generateSqlSaldoBancoSemFazerFechamento() {
		
		def sql =  "SELECT c.seq_func "
			sql += "	  ,f.nom_func "
			sql += "	  ,d.id AS seq_pr "
			sql += "	  ,d.dt_ini_pr "
			sql += "	  ,d.apel_pr "
			sql += "	  ,sum(CASE c.id_tp_ocorr WHEN 'C' THEN c.NR_QTDE_HR ELSE 0 END) AS credito "
			sql += "	  ,sum(CASE c.id_tp_ocorr WHEN 'D' THEN c.NR_QTDE_HR ELSE 0 END) AS debito "
			sql += "	  ,(sum(CASE c.id_tp_ocorr WHEN 'C' THEN c.NR_QTDE_HR ELSE 0 END) "
			sql += "		- sum(CASE c.id_tp_ocorr WHEN 'D' THEN c.NR_QTDE_HR ELSE 0 END)) AS total "
			sql += "FROM ocorrencias_bh c "
			sql += "INNER JOIN funcionario f ON c.seq_func = f.id "
			sql += "INNER JOIN periodo_filial g ON f.filial_id = g.filial_id "
			sql += "INNER JOIN periodo d ON g.periodo_id = d.id AND c.dt_bat BETWEEN d.dt_ini_pr AND d.dt_fim_pr "
			sql += "WHERE (1=1) "
			sql += "  AND d.id in ( " + montaParametrosPeriodos() + " ) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "GROUP BY c.seq_func, f.nom_func, d.id, d.dt_ini_pr, d.apel_pr "
			
		def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
		sql = sql + " order by " + paramOrdenacao + ", d.dt_ini_pr "
			
		sql
	}
	
	def generateSqlComparacaoDeBancoDeHoras() {
		
		def sql =  "SELECT f.filial_id "
			sql += "	  ,o.seq_func "
			sql += "	  ,f.nom_func "
			sql += "	  ,p.id as seq_pr "
			sql += "	  ,p.dt_ini_pr "
			sql += "	  ,p.apel_pr "
			sql += "	  ,p.dsc_pr "
			sql += "	  ,c.dsc_cargo "
			sql += "	  ,sum(CASE o.id_tp_ocorr WHEN 'C' THEN o.nr_qtde_hr ELSE 0 END) "
			sql += "		- sum(CASE o.id_tp_ocorr WHEN 'D' THEN o.nr_qtde_hr ELSE 0 END) AS saldo_atual "
			sql += "FROM ocorrencias_bh o "
			sql += "INNER JOIN funcionario f on o.seq_func = f.id "
			sql += "INNER JOIN periodo_filial pf on pf.filial_id = f.filial_id "
			sql += "INNER JOIN periodo p on pf.periodo_id = p.id and o.dt_bat between p.dt_ini_pr and p.dt_fim_pr "
			sql += "INNER JOIN cargo c on f.cargo_id = c.id "
			sql += "WHERE (1=1) "
			sql += "  AND p.id in ( " + montaParametrosPeriodos() + " ) "
			sql += filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			sql += "GROUP BY f.filial_id,o.seq_func,f.nom_func,p.id,p.dt_ini_pr,p.dsc_pr,p.apel_pr,c.dsc_cargo "
			
		def paramOrdenacao = (params.ordenacao.toInteger() == 1 ? " seq_func " : " nom_func ")
		sql = sql + " order by f.filial_id, " + paramOrdenacao + ", p.dt_ini_pr "
			
		sql
	}
	
	def generateParameters() {
		
		def agrupamento = ""
		switch (params.agrupamento) {
			case "0":
				agrupamento = "Filial"
				break
			case "1":
				agrupamento = "Departamento"
				break
			case "2":
				agrupamento = "Cargo"
				break
			case "3":
				agrupamento = "Funcion\u00E1rio"
				break
		}
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			AGRUPAMENTO: agrupamento,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
	def generateReportName() {
		def reportName = ""
		
		if (params.relatorio.equals("0")) {
			if (params.tipoRelatorio.equals("0")) {
				reportName = "relatorioBancoHorasNormalAnalitico"
			} else {
				reportName = "relatorioBancoHorasNormalSintetico"
			}
			
		} else if (params.relatorio.equals("1")) {
			if (params.checkValor) {
				reportName = "relatorioPlanilhaSaldoBancoHorasComValorAnalitico"
			} else {
				reportName = "relatorioPlanilhaSaldoBancoHorasAnalitico"
			}
		} else if (params.relatorio.equals("2")) {
			reportName = "relatorioPagamentoBancoHorasAnalitico"
		} else if (params.relatorio.equals("3")) {
			reportName = "relatorioMovimentacaoFechamentoBancoHorasAnalitico"
		} else if (params.relatorio.equals("4")) {
			reportName = "relatorioSaldoBancoHorasSemFazerFechamentoAnalitico"
		} else if (params.relatorio.equals("5")) {
			reportName = "relatorioComparacaoSaldoBancoHorasAnalitico"
		}
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def montaParametrosPeriodos() {
		def periodos = [0]
		if (params.periodosId) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.periodosId.getClass()) }
			periodos = isArray ? params.periodosId : [params.periodosId]
		}
		
		return periodos.toString().replace('[', ' ').replace(']', ' ')
	}
	
	def montaParametrosTipoOcorrencias() {
		def tipoOcorrencias = [0]
		if (params.tipoOcorrencia) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.tipoOcorrencia.getClass()) }
			tipoOcorrencias = isArray ? params.tipoOcorrencia : [params.tipoOcorrencia]
		}
		
		return tipoOcorrencias.toString()
			.replace('[', '\'')
			.replace(']', '\'')
			.replace(', ', '\',\'')
	}
	
	def montaParametrosOcorrencias() {
		def ocorrencias = [0]
		if (params.ocorrenciasId) {
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.ocorrenciasId.getClass()) }
			ocorrencias = isArray ? params.ocorrenciasId : [params.ocorrenciasId]
		}
		
		return ocorrencias.toString().replace('[', ' ').replace(']', ' ')
	}
	
	def salvarFiltro() {
		
		def nomeFiltro = params.nomeFiltro
		def ocorrencias = params.ocorrencias.split(",")
		Usuario usuario = session['usuario']
		
		ParamRelatorioBancoHoras parametro = new ParamRelatorioBancoHoras()
		parametro.usuario = usuario
		parametro.descricao = nomeFiltro
		parametro.save(true)
		
		ocorrencias.each {
			Ocorrencias o = Ocorrencias.get(it.toLong())
			ParamRelatorioBancoHorasOcorrencia ocorrencia = new ParamRelatorioBancoHorasOcorrencia()
			ocorrencia.ocorrencia = o
			ocorrencia.param = parametro
			ocorrencia.save(true)
		}
		
		return render(text: [success:true] as JSON, contentType:'text/json')
	}
	
	def aplicarFiltro() {
		
		ParamRelatorioBancoHoras paramRelat = ParamRelatorioBancoHoras.get(params.paramRelat)
		Set<ParamRelatorioBancoHorasOcorrencia> ocorrencias = paramRelat.ocorrencias
		
		def result = []
		ocorrencias.each {
			result.add([id: it.ocorrencia.id,
				descricao: it.ocorrencia.dscOcor,
				formula: it.ocorrencia.formulaOcor])
		}
		
		render result as JSON
	}
	
}