package relatorios

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext

import acesso.Usuario
import cadastros.Justificativas
import cadastros.ParamRelatorioAfastamento
import cadastros.ParamRelatorioAfastamentoJustificativa

class RelatorioAfastamentosController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filtroPadraoService
	def detectDataBaseService
	def relatoriosService
	def jasperService
	def dataSource
	
	def index() {
		
		Usuario usuario = session['usuario']
		def filtros = ParamRelatorioAfastamento.findAllByUsuario(usuario)
		
		[filtros: filtros]
	}
	
	def generateReport() {
		
		String query = generateSql(params)
		println "### Consulta do relatorio: " + query
		Sql sql = new Sql(dataSource)
		def results = []
		
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
			flash.message = "Sem registros"
			redirect(action: "index")
		}
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = params._format.equals("PDF") ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = generateReportName()
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();		
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: params._format]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", params._format)
		
		render reportData as JSON
	}
	
	def generateSql(params) {
		
		SimpleDateFormat formatBD = null
		if (detectDataBaseService.isPostgreSql()) {
			formatBD = new SimpleDateFormat("yyyy-MM-dd")
		} else {
			formatBD = new SimpleDateFormat("dd/MM/yyyy")
		}
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		def dtInicioParam = formatBD.format(dtInicio)
		def dtFimParam = formatBD.format(dtFim)
		
		def sql = ""
		
		def tipoRelatorio = params.tipoRelatorio
		def agrupamento = params.agrupamento
		
		def selectClause = " select f.id as seq, f.mtr_func as matricula, f.crc_func as cracha, f.nom_func as nome,  "
		selectClause = selectClause + " s.dsc_setor as setor, j.dsc_just as justificativa, to_char(a.data_inicio, 'dd/MM/yyyy') AS inicio, to_char(a.data_fim, 'dd/MM/yyyy') AS fim, (a.data_fim-a.data_inicio) as dias "
		
		sql = selectClause
		sql = sql + " from afastamentos a "
		sql = sql + " join justificativas j on a.justificativas_id = j.id "
		sql = sql + " join funcionario f on a.funcionario_id = f.id "
		sql = sql + " join filial fil on f.filial_id = fil.id "
		sql = sql + " join cargo c on f.cargo_id = c.id "
		sql = sql + " join setor s on f.setor_id = s.id " 
		
		sql = sql + " where (a.data_inicio between '${dtInicioParam}' and '${dtFimParam}' or a.data_fim between '${dtInicioParam}' and '${dtFimParam}') "
		sql = sql + filtroPadraoService.montaFiltroPadraoSqlNativo(params)
		sql = sql + " and j.id in ( " + params.justificativaId.toString().replace('[', ' ').replace(']', ' ') + " ) "
		
		sql = sql + " order by " + (params.ordenacao.toInteger() == 1 ? " seq " : " nome ")
		
		sql  
	}
	
	def generateReportName() {
		def reportName = ""
		
		reportName = "relatorioAfastamentos"
		
		reportName = reportName.concat(params._format.equals("PDF") ? "" : "Excel")
		reportName = reportName.concat(".jasper")
		
		reportName
	}
	
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		
		def parameters = [
			DT_INICIO: params.dataInicio,
			DT_FIM: params.dataFim,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext)
		]
		parameters
	}
	
	def salvarFiltro() {
		
		def nomeFiltro = params.nomeFiltro
		def justificativas = params.justificativas.split(",")
		Usuario usuario = session['usuario']
		
		ParamRelatorioAfastamento parametro = new ParamRelatorioAfastamento()
		parametro.usuario = usuario
		parametro.descricao = nomeFiltro
		parametro.save(true)
		
		justificativas.each {
			Justificativas j = Justificativas.get(it.toLong())
			ParamRelatorioAfastamentoJustificativa justificativa = new ParamRelatorioAfastamentoJustificativa()
			justificativa.justificativa = j
			justificativa.param = parametro
			justificativa.save(true)
		}
		
		return render(text: [success:true] as JSON, contentType:'text/json')
	}
	
	def aplicarFiltro() {
		
		ParamRelatorioAfastamento paramRelat = ParamRelatorioAfastamento.get(params.paramRelat)
		Set<ParamRelatorioAfastamentoJustificativa> justificativas = paramRelat.justificativas
		
		def result = []
		justificativas.each {
			result.add([id: it.justificativa.id,
				descricao: it.justificativa.dscJust])
		}
		
		render result as JSON
	}
	
}