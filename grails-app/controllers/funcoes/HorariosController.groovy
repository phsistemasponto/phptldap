package funcoes

import grails.converters.JSON
import groovy.sql.Sql

class HorariosController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def dataSource

    def retornaCargaHorariaHoras() {
		def cargaHoraria = params.qtdMinutos
		cargaHoraria = (cargaHoraria)?cargaHoraria.toLong():0
		def retornoMap = [cargaHorariaHoras: Horarios.retornaHora(cargaHoraria)]
		render retornoMap as JSON
    }
	
	def retornaQuantidadeHoras() {
		
		def sql =  " select b.qthoras as qt "
			sql += " from horario a "
			sql += " left join (select horario_id , sum(qtdechefetiva)as qthoras "
			sql += " 	   from itemhorario "
			sql += " 	   group by horario_id) b on a.id = b.horario_id "
			sql += " where a.id = " + params.idHora
			
		Sql sqlObj = new Sql(dataSource)
		def results = []
		
		try {
			results = sqlObj.rows(sql)
		} catch(Exception e) {
			e.printStackTrace()
		}
			
		def hora = results[0].qt
		def horaStr = Horarios.retornaHora(hora.toLong())
		
		def retornoMap = [total: horaStr]
		render retornoMap as JSON
	}

}
