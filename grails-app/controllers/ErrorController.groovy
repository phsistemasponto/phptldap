import grails.util.GrailsUtil

import org.codehaus.groovy.grails.commons.GrailsApplication

class ErrorController {

	def index() {
		if ( GrailsApplication.ENV_PRODUCTION == GrailsUtil.environment ) {
			// Production: show a nice error message
			render(view:'errorProduction')
		} else {
			// Not it production? show an ugly, developer-focused error message
			render(view:'errorDevelopment')
		}
	}
}