package cadastros

import java.math.*
import java.text.*

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.transaction.annotation.Transactional


@Transactional
class GerarBeneficiosController {
	
	def filtroPadraoService
	def detectDataBaseService
	def dataSource
	
	def index() {
		def result = []
		
		if (params.btnFiltro) {
			
			def typeResult = params.opcaoFiltro
			def data = []
			
			if (typeResult.equals("filial")) {
				data = filiaisComTipoDeBeneficio()
			} else if (typeResult.equals("funcionario")) {
			
				def filiais = filiaisComTipoDeBeneficio()
				def filiaisId = filiais.collect { it.id }
				
				def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)
				if (filiaisId) {
					filtroPadrao += " and f.filial.id in ( " + filiaisId.toString().replace("[", "").replace("]", "") + ") "
				} else {
					filtroPadrao += " and f.filial.id in (0) "
				}
				def orderby = " order by f.id "
				filtroPadrao += orderby
				
				data = Funcionario.findAll(filtroPadrao)
			} 
			
			result = [typeResult: typeResult, data: data]
		}
		
		[result: result]
	}
	
	def filiaisComTipoDeBeneficio() {
		Set<Filial> filiais = new HashSet<Filial>()
		
		if (params.tipoBeneficio) {
			
			TipoDeBeneficio tipoBeneficio = TipoDeBeneficio.get(params.tipoBeneficio.toLong())
			BeneficioParam.findAllByTipoDeBeneficio(tipoBeneficio).each { bp ->
				BeneficioParamFilial.findAllByBeneficioParam(bp).each { bpf ->
					filiais.add(bpf.filial)
				}
			}
			
		}
		
		filiais
	}
	
	def processarFiliais() {
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def arrayFilial = isArray ? params.filial : [params.filial]
		
		def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)
		filtroPadrao += " and f.filial.id in ( " + arrayFilial.toString().replace("[", "").replace("]", "") + ") "
		
		def funcionarios = Funcionario.findAll(filtroPadrao)
		
		processamento(funcionarios) 
		
		render(status: response.SC_OK)
	}
	
	def processarFuncionarios() {
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.funcionario.getClass()) }
		def arrayFuncionario = isArray ? params.funcionario : [params.funcionario]
		def funcionarios = Funcionario.findAll("from Funcionario f where f.id in ("+ arrayFuncionario.toString().replace("[", "").replace("]", "") + ")")
		
		processamento(funcionarios)
		
		render(status: response.SC_OK)
	}
	
	@Transactional
	def processamento(funcionarios) {
		
		def template = new JdbcTemplate(dataSource)
		TipoDeBeneficio tpBeneficio = TipoDeBeneficio.get(params.tpBenefi.toLong())
		
		def dtIniDesconto = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtIniDes)
		def dtFimDesconto = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFimDes)
		def dtIniCompra = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtIniCom)
		def dtFimCompra = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFimCom)
		
		funcionarios.each { f ->
			
			def sqlFuncBeneficio  = "from FuncionarioBeneficio fb where fb.funcionario = :func and fb.beneficio.tipoDeBeneficio = :tpBeneficio "
				sqlFuncBeneficio += "and (:dtIniDes between fb.dtIniVig and fb.dtFimVig or :dtFimDes between fb.dtIniVig and fb.dtFimVig or fb.dtFimVig is null) "
				
			def funcionarioBeneficio = FuncionarioBeneficio.findAll(sqlFuncBeneficio, 
					[func: f, tpBeneficio: tpBeneficio, dtIniDes: dtIniDesconto, dtFimDes: dtFimDesconto])
			
			if (funcionarioBeneficio != null) {
				
				funcionarioBeneficio.each { fb ->
					
					// descontos
					
					def beneficiosFiliais = BeneficioParamFilial.findAllByFilial(f.filial) 
					BeneficioParamFilial bp = beneficiosFiliais.find { it.beneficioParam.tipoDeBeneficio.equals(tpBeneficio) }
					
					def ocorrencias = bp.beneficioParam.ocorrencias
					def justificativas = bp.beneficioParam.justificativasAbono
					
					ocorrencias.each { o->
						
						def consulta = "from Movimentos m where m.funcionario = :func and m.dtBat between :dtIniDes and :dtFimDes and m.ocorrencia = :ocor and m.idSituacaoAbono is null"
						def movimentos = Movimentos.findAll(consulta, [func: f, dtIniDes: dtIniDesconto, dtFimDes: dtFimDesconto, ocor: o.ocorrencias])
						
						if (movimentos) {
							
							def sqlBeneficios = "from BeneficioFuncOcor where funcionario = :f and ocorrencias = :o and (dtInicio between :dtIniDes and :dtFimDes or dtFim between :dtIniDes and :dtFimDes)"
							BeneficioFuncOcor.findAll(sqlBeneficios, [f: f, o: o.ocorrencias, dtIniDes: dtIniDesconto, dtFimDes: dtFimDesconto]).each {
								it.delete(flush: true)
							}
							
							BeneficioFuncOcor bfo = new BeneficioFuncOcor()
							bfo.funcionario = f
							bfo.ocorrencias = o.ocorrencias
							bfo.beneficio = fb.beneficio
							bfo.dtInicio = dtIniDesconto
							bfo.dtFim = dtFimDesconto
							bfo.dtProc = new Date()
							bfo.quantidade = movimentos.size()
							bfo.save(flush: true)
							
						}
						
						justificativas.each { j ->
							
							def consultaAbono  = "from Abonos a where a.funcionario = :func and (a.dtInicio between :dtIniDes and :dtFimDes or a.dtFim between :dtIniDes and :dtFimDes) "
								consultaAbono += "and a.justificativas = :just and a.ocorrencias = :ocor"
							def abonos = Abonos.findAll(consultaAbono, [func: f, dtIniDes: dtIniDesconto, dtFimDes: dtFimDesconto, just: j.justificativas, ocor: o.ocorrencias])
							
							if (abonos) {
								
								def quantidade = 0
								abonos.each {  a ->
									quantidade += a.qtdadeDias
								}
								
								def sqlBeneficios = "from BeneficioFuncFltsAbonadas where funcionario = :f and ocorrencias = :o and justificativas = :j and (dtInicio between :dtIniDes and :dtFimDes or dtFim between :dtIniDes and :dtFimDes)"
								BeneficioFuncFltsAbonadas.findAll(sqlBeneficios, [f: f, o: o.ocorrencias, j: j.justificativas, dtIniDes: dtIniDesconto, dtFimDes: dtFimDesconto]).each {
									it.delete(flush: true)
								}
								
								BeneficioFuncFltsAbonadas bffa = new BeneficioFuncFltsAbonadas()
								bffa.funcionario = f
								bffa.ocorrencias = o.ocorrencias
								bffa.justificativas = j.justificativas
								bffa.beneficio = fb.beneficio
								bffa.dtInicio = dtIniDesconto
								bffa.dtFim = dtFimDesconto
								bffa.dtProc = new Date()
								bffa.quantidade = quantidade
								bffa.save(flush: true)
							}
							
							
						}
						
						
					}
					
					def tipoIncremento = bp.beneficioParam.idBenIncremento
					if (tipoIncremento.equals("M")) {
						
						def temDireito = true
						ocorrencias.each { o ->
							
							def sqlOcor = "from BeneficioFuncOcor where funcionario = :f and dtInicio = :dIni and dtFim = :dFim and ocorrencias = :o "
							def benFuncOcor = BeneficioFuncOcor.find(sqlOcor, [f: f, o: o.ocorrencias, dIni: dtIniDesconto, dFim: dtFimDesconto])
							def quantidadeFaltas = benFuncOcor != null ? benFuncOcor.quantidade : 0
							if (o.quantidade < quantidadeFaltas) {
								temDireito = false
							}
							
							if (temDireito) {
								
								justificativas.each { j->
									def sqlJust = "from BeneficioFuncFltsAbonadas where funcionario = :f and dtInicio = :dIni and dtFim = :dFim and ocorrencias = :o and justificativas = :j"
									def benJust = BeneficioFuncFltsAbonadas.find(sqlJust, [f: f, o: o.ocorrencias, j: j.justificativas, dIni: dtIniDesconto, dFim: dtFimDesconto])
									def quantidadeAbonadas = benJust != null ? benFuncOcor.quantidade : 0
									if (j.quantidade < quantidadeFaltas) {
										temDireito = false
									}
								}
								
							}
							
							BeneficioMovimentos bm = new BeneficioMovimentos()
							bm.funcionario = f
							bm.beneficio = fb.beneficio
							bm.dtInicio = dtIniCompra
							bm.dtFim = dtFimCompra
							bm.diasUteis = 0
							
							if (temDireito) {
								bm.quantidade = 1
							} else {
								bm.quantidade = 0
							}
							
							bm.save(flush: true)
						}
						
					} else {
					
						// calculo normal
						Date dataTemp = new java.util.Date(dtIniDesconto.getTime())
						def diasUteis = 0
						
						while (dataTemp.before(dtFimDesconto) || dataTemp.equals(dtFimDesconto)) {
							
							def afastamentos = Afastamentos.findAll(
								"from Afastamentos a where a.funcionario = :func and :dtBat between a.dataInicio and a.dataFim ",
								[func: f, dtBat: dataTemp])
							
							if (afastamentos == null || afastamentos.isEmpty()) {
								
								def tpDia = ""
								if (detectDataBaseService.isPostgreSql()) {
									tpDia = template.queryForObject("select pr_getdiasema(${f.id}::integer, '${dataTemp}'::date);", String.class)
								} else if (detectDataBaseService.isOracle()) {
									tpDia = template.queryForObject("select pr_getdiasema(${f.id}::integer, '${dataTemp}'::date);", String.class)
								}
								
								if (tpDia != null && tpDia.equals("TR")) {
									diasUteis++
								}
								
							}
							
							dataTemp = dataTemp.plus(1)
						}
						
						def sqlOcor = "from BeneficioFuncOcor where funcionario = :f and dtInicio = :dIni and dtFim = :dFim "
						def benFuncOcor = BeneficioFuncOcor.find(sqlOcor, [f: f, dIni: dtIniDesconto, dFim: dtFimDesconto])
						def quantidadeFaltas = benFuncOcor != null ? benFuncOcor.quantidade : 0
						def total = diasUteis - quantidadeFaltas
						def total2 = total * fb.quantidade
						
						BeneficioMovimentos bm = new BeneficioMovimentos()
						bm.funcionario = f
						bm.beneficio = fb.beneficio
						bm.dtInicio = dtIniCompra
						bm.dtFim = dtFimCompra
						bm.diasUteis = diasUteis
						bm.quantidade = total2
						bm.save(flush: true)
					
					}
					
				}
				
				
								
			} else {
				
				FuncionarioSemBeneficio fsb = new FuncionarioSemBeneficio()
				fsb.funcionario = f
				fsb.tipoBeneficio = tpBeneficio
				fsb.dtProcessamento = new Date()
				fsb.dtInicioDesconto = dtIniDesconto
				fsb.dtFimDesconto = dtFimDesconto
				fsb.save(flush: true)
				
			}
			
		}
			
	}
	
}