package cadastros

import org.springframework.dao.DataIntegrityViolationException




class AdcNoturnoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [adcNoturnoInstanceList: AdcNoturno.list(params), adcNoturnoInstanceTotal: AdcNoturno.count()]
    }

    def delete() {
        def adcNoturnoInstance = AdcNoturno.get(params.id)
        if (!adcNoturnoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['AdcNoturno', adcNoturnoInstance.id])
        } else {
            try {
                adcNoturnoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['AdcNoturno'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['AdcNoturno'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [adcNoturnoInstance: new AdcNoturno()]
    }

    def edit() {
        def adcNoturnoInstance = AdcNoturno.get(params.id)
        if (!adcNoturnoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['AdcNoturno', adcNoturnoInstance.id])
            redirect(action: "list")
            return
        }

        [adcNoturnoInstance: adcNoturnoInstance]
    }

    def save() {
        def adcNoturnoInstance = new AdcNoturno(params)
        if (!adcNoturnoInstance.save(flush: true)) {
            render(view: "create", model: [adcNoturnoInstance: adcNoturnoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['AdcNoturno'])
        redirect(action: "create")
    }

    def update() {
        def adcNoturnoInstance = AdcNoturno.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (adcNoturnoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['AdcNoturno'])
                render(view: "edit", model: [adcNoturnoInstance: adcNoturnoInstance])
                return
            }
        }

        adcNoturnoInstance.properties = params

        if (!adcNoturnoInstance.save(flush: true)) {
            render(view: "edit", model: [adcNoturnoInstance: adcNoturnoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['AdcNoturno'])
        redirect(action: "list")
    }
}