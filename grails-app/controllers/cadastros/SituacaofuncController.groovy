package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;


@Transactional
class SituacaofuncController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [situacaofuncInstanceList: Situacaofunc.list(params), situacaofuncInstanceTotal: Situacaofunc.count()]
    }

    def delete() {
        def situacaofuncInstance = Situacaofunc.get(params.id)
        if (!situacaofuncInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Situacaofunc', situacaofuncInstance.id])
        } else {
            try {
                situacaofuncInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Situacaofunc'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Situacaofunc'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [situacaofuncInstance: new Situacaofunc()]
    }

    def edit() {
        def situacaofuncInstance = Situacaofunc.get(params.id)
        if (!situacaofuncInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Situacaofunc', situacaofuncInstance.id])
            redirect(action: "list")
            return
        }

        [situacaofuncInstance: situacaofuncInstance]
    }

    def save() {
        def situacaofuncInstance = new Situacaofunc(params)
        if (!situacaofuncInstance.save(flush: true)) {
            render(view: "create", model: [situacaofuncInstance: situacaofuncInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Situacaofunc'])
        redirect(action: "create")
    }

    def update() {
        def situacaofuncInstance = Situacaofunc.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (situacaofuncInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Situacaofunc'])
                render(view: "edit", model: [situacaofuncInstance: situacaofuncInstance])
                return
            }
        }

        situacaofuncInstance.properties = params

        if (!situacaofuncInstance.save(flush: true)) {
            render(view: "edit", model: [situacaofuncInstance: situacaofuncInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Situacaofunc'])
        redirect(action: "list")
    }
}