package cadastros

import org.springframework.dao.DataIntegrityViolationException


class ArquivoFolhaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [arquivoFolhaInstanceList: ArquivoFolha.list(params), arquivoFolhaInstanceTotal: ArquivoFolha.count()]
    }

    def delete() {
        def arquivoFolhaInstance = ArquivoFolha.get(params.id)
        if (!arquivoFolhaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ArquivoFolha', arquivoFolhaInstance.id])
        } else {
            try {
                arquivoFolhaInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['ArquivoFolha'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['ArquivoFolha'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [arquivoFolhaInstance: new ArquivoFolha()]
    }

    def edit() {
        def arquivoFolhaInstance = ArquivoFolha.get(params.id)
        if (!arquivoFolhaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ArquivoFolha', arquivoFolhaInstance.id])
            redirect(action: "list")
            return
        }

        [arquivoFolhaInstance: arquivoFolhaInstance]
    }

    def save() {
        def arquivoFolhaInstance = new ArquivoFolha(params)
		
		if (!arquivoFolhaInstance.save(flush: true)) {
            render(view: "create", model: [arquivoFolhaInstance: arquivoFolhaInstance])
            return
        }
		
		configuraFolhaLayout(arquivoFolhaInstance)

		flash.message = message(code: 'default.created.message', args: ['ArquivoFolha'])
        redirect(action: "create")
    }

    def update() {
        def arquivoFolhaInstance = ArquivoFolha.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (arquivoFolhaInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['ArquivoFolha'])
                render(view: "edit", model: [arquivoFolhaInstance: arquivoFolhaInstance])
                return
            }
        }

        arquivoFolhaInstance.properties = params

        if (!arquivoFolhaInstance.save(flush: true)) {
            render(view: "edit", model: [arquivoFolhaInstance: arquivoFolhaInstance])
            return
        }
		
		
		configuraFolhaLayout(arquivoFolhaInstance)

		flash.message = message(code: 'default.updated.message', args: ['ArquivoFolha'])
        redirect(action: "list")
    }
	
	
	def configuraFolhaLayout(ArquivoFolha arquivoFolhaInstance) {
		
		if (arquivoFolhaInstance.id != null) {
			def layouts = ArquivoFolhaLayout.findAllByArquivofolha(arquivoFolhaInstance)
			layouts.each { l ->
				l.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.cmpArquivofolhacampo.getClass()) }
		
		def arrayFolhaCampo = isArray ? params.cmpArquivofolhacampo : [params.cmpArquivofolhacampo]
		def arrayLayoutLinha = isArray ? params.cmpLayoutLinha : [params.cmpLayoutLinha]
		def arrayLayoutInicio = isArray ? params.cmpLayoutInicio : [params.cmpLayoutInicio]
		def arrayLayoutFim = isArray ? params.cmpLayoutfim : [params.cmpLayoutfim]
		def arrayLayoutCampoFixo = isArray ? params.cmpLayoutcampofixo : [params.cmpLayoutcampofixo]
		def arrayLayoutPreenchimento = isArray ? params.cmpLayoutpreenchimento : [params.cmpLayoutpreenchimento]
		def arrayLayoutDirecao = isArray ? params.cmpLayoutdirecao : [params.cmpLayoutdirecao]
		
		if (arrayFolhaCampo != null) {
			
			arrayFolhaCampo.eachWithIndex { j, i ->
				if (j != null) {
					
					ArquivoFolhaLayout layout = new ArquivoFolhaLayout()
					
					layout.arquivofolha = arquivoFolhaInstance
					layout.arquivofolhacampo = ArquivoFolhaCampo.get(j.toLong())
					layout.layoutLinha = arrayLayoutLinha[i].toInteger()
					layout.layoutInicio = arrayLayoutInicio[i].toInteger()
					layout.layoutfim = arrayLayoutFim[i].toInteger()
					layout.layoutcampofixo = arrayLayoutCampoFixo[i]
					layout.layoutpreenchimento = arrayLayoutPreenchimento[i]
					layout.layoutdirecao = arrayLayoutDirecao[i].toInteger()
					layout.save()
					
					if (arquivoFolhaInstance.layouts == null) {
						arquivoFolhaInstance.layouts = new ArrayList<ArquivoFolhaLayout>()
					}
					
					arquivoFolhaInstance.layouts.add(layout)
				}
			}
		}
		
	}
	
}