package cadastros

import java.math.*
import java.text.*

import org.apache.commons.collections.map.HashedMap
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.transaction.annotation.Transactional


@Transactional
class LancamentosDiariosController {

	def filtroPadraoService
	def abonoService
	def springSecurityService
	def dataSource
	def detectDataBaseService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		
		def funcs = new LinkedHashMap<Funcionario, Object[]>()

		if (params.dataInicio && params.dataFim) {
			
			def dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			def dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)

			def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)
			filtroPadrao += " order by f.id "
			def funcionariosList = Funcionario.findAll(filtroPadrao)
			
			funcionariosList.each { funcionario ->
				
				def dadosLancamentosFuncionario = []
				funcs.put(funcionario, dadosLancamentosFuncionario)
				
				Date dataTemp = new java.util.Date(dataInicio.time)
				while (dataTemp.before(dataFim) || dataTemp.equals(dataFim)) {
					ItensDiario item = ItensDiario.findByFuncionarioAndDtBat(funcionario, dataTemp)
					dadosLancamentosFuncionario.add(
							[dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(dataTemp),
								data: new SimpleDateFormat("dd").format(dataTemp),
								identificadorDia: new SimpleDateFormat("ddMMyyyy").format(dataTemp),
								classificacao: item?.classificacao?.id,
								itemHorario: item?.perfilHorario?.id])
					dataTemp = dataTemp.plus(1)
				}
			}
			
		}

		[funcionarioInstanceList: funcs]
	}

	def lancamentosDiarios() {

		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		def funcionarios = new ArrayList<Funcionario>()
		
		params.each { p ->
			if (p.key.toString().startsWith("classificacaoCelulaLancamento-")) {
				def campos = p.key.toString().split("-")
				
				def funcionarioStr = campos[1]
				def funcionario = Funcionario.get(funcionarioStr.toLong())
				if (!funcionarios.contains(funcionario)) {
					funcionarios.add(funcionario)
				}
			}
		}
		
		funcionarios.each { funcionario ->
			ItensDiario.executeUpdate("delete from ItensDiario where funcionario = :funcionario and dtBat between :dtInicio and :dtFim",
				[funcionario: funcionario, dtInicio: dataInicio, dtFim: dataFim])
		}

		params.each { p ->
			if (p.key.toString().startsWith("classificacaoCelulaLancamento-")) {
				def campos = p.key.toString().split("-")
				
				def funcionarioStr = campos[1]
				def funcionario = Funcionario.get(funcionarioStr.toLong())
				if (!funcionarios.contains(funcionario)) {
					funcionarios.add(funcionario)
				}
				
				def dataStr = campos[2]
				def dataObj = new SimpleDateFormat("ddMMyyyy").parse(dataStr)
				
				def classificao = p.value.toString()

				if (classificao) {
					
					ItensDiario item = new ItensDiario()
					item.funcionario = funcionario
					item.dtBat = dataObj
					item.classificacao = Classificacao.get(classificao.toLong())
					if (item.classificacao.idTpDia.equals("TR")) {
						def itemHorario = params["itemHorarioCelulaLancamento-"+dataStr]
						item.perfilHorario = itemHorario ? Perfilhorario.get(itemHorario.toLong()) : null
					}
					item.qtdeChEfetiva = 0

					item.save(flush: true)
				}
			}
		}

		funcionarios.each { funcionario ->
//			chamaProcedureProcessar(funcionario, dataInicio, dataFim)
		}
		
		def funcs = new HashedMap<Funcionario, Object[]>()
		funcionarios.each { funcionario ->
			
			def dadosLancamentosFuncionario = []
			funcs.put(funcionario, dadosLancamentosFuncionario)
			
			Date dataTemp = new java.util.Date(dataInicio.time)
			while (dataTemp.before(dataFim) || dataTemp.equals(dataFim)) {
				ItensDiario item = ItensDiario.findByFuncionarioAndDtBat(funcionario, dataTemp)
				dadosLancamentosFuncionario.add(
						[dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(dataTemp),
							data: new SimpleDateFormat("dd").format(dataTemp),
							identificadorDia: new SimpleDateFormat("ddMMyyyy").format(dataTemp),
							classificacao: item?.classificacao?.id,
							itemHorario: item?.perfilHorario?.id])
				dataTemp = dataTemp.plus(1)
			}
		}
		
		flash.message = "Altera&ccedil;&atilde;o realizada com sucesso"
		
		redirect(action: "list", 
			params: [dataInicio: params.dataInicio, dataFim: params.dataFim], 
			model: [funcionarioInstanceList: funcs])
	}
	
	def chamaProcedureProcessar(funcionario, dataInicio, dataFim) {
		
		def seqEmpresa = funcionario.filial.empresa.id
		def seqFilial = funcionario.filial.id
		def seqFuncionario = funcionario.id

		def template = new JdbcTemplate(dataSource)

		if (detectDataBaseService.isPostgreSql()) {
			def valor = template.queryForObject("select pr_processar(${seqEmpresa}::integer, ${seqFilial}::integer, ${seqFuncionario}::integer, '${dataInicio}'::date, '${dataFim}'::date);", Integer.class)
		} else if (detectDataBaseService.isOracle()) {
			// TODO chamar
		}

	}
	

}