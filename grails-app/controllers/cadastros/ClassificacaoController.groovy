package cadastros

import grails.converters.JSON

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional


@Transactional
class ClassificacaoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [classificacaoInstanceList: Classificacao.list(params), classificacaoInstanceTotal: Classificacao.count()]
    }

    def delete() {
        def classificacaoInstance = Classificacao.get(params.id)
        if (!classificacaoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Classificacao', classificacaoInstance.id])
        } else {
            try {
                classificacaoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Classificacao'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Classificacao'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [classificacaoInstance: new Classificacao()]
    }

    def edit() {
        def classificacaoInstance = Classificacao.get(params.id)
        if (!classificacaoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Classificacao', classificacaoInstance.id])
            redirect(action: "list")
            return
        }

        [classificacaoInstance: classificacaoInstance]
    }

    def save() {
        def classificacaoInstance = new Classificacao(params)
        if (!classificacaoInstance.save(flush: true)) {
            render(view: "create", model: [classificacaoInstance: classificacaoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Classificacao'])
        redirect(action: "create")
    }

    def update() {
        def classificacaoInstance = Classificacao.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (classificacaoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Classificacao'])
                render(view: "edit", model: [classificacaoInstance: classificacaoInstance])
                return
            }
        }

        classificacaoInstance.properties = params

        if (!classificacaoInstance.save(flush: true)) {
            render(view: "edit", model: [classificacaoInstance: classificacaoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Classificacao'])
        redirect(action: "list")
    }
	
	def buscaTipoClassificacao() {
		def classificacao = Classificacao.get(params.id)
		def resultadoMap = []
		if (params.id) {
			resultadoMap = [id: classificacao.id, tipoClassificacao: classificacao.idTpDia]
		}
		render resultadoMap as JSON
	}
}