package cadastros

import org.springframework.dao.DataIntegrityViolationException

class FeriadosNacionaisController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [feriadosNacionaisInstanceList: FeriadosNacionais.list(params), feriadosNacionaisInstanceTotal: FeriadosNacionais.count()]
    }

    def delete() {
        def feriadosNacionaisInstance = FeriadosNacionais.get(params.id)
        if (!feriadosNacionaisInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FeriadosNacionais', feriadosNacionaisInstance.id])
        } else {
            try {
                feriadosNacionaisInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['FeriadosNacionais'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['FeriadosNacionais'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [feriadosNacionaisInstance: new FeriadosNacionais()]
    }

    def edit() {
        def feriadosNacionaisInstance = FeriadosNacionais.get(params.id)
        if (!feriadosNacionaisInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FeriadosNacionais', feriadosNacionaisInstance.id])
            redirect(action: "list")
            return
        }

        [feriadosNacionaisInstance: feriadosNacionaisInstance]
    }

    def save() {
        def feriadosNacionaisInstance = new FeriadosNacionais(params)
        if (!feriadosNacionaisInstance.save(flush: true)) {
            render(view: "create", model: [feriadosNacionaisInstance: feriadosNacionaisInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['FeriadosNacionais'])
        redirect(action: "create")
    }

    def update() {
        def feriadosNacionaisInstance = FeriadosNacionais.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (feriadosNacionaisInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['FeriadosNacionais'])
                render(view: "edit", model: [feriadosNacionaisInstance: feriadosNacionaisInstance])
                return
            }
        }

        feriadosNacionaisInstance.properties = params

        if (!feriadosNacionaisInstance.save(flush: true)) {
            render(view: "edit", model: [feriadosNacionaisInstance: feriadosNacionaisInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['FeriadosNacionais'])
        redirect(action: "list")
    }
}