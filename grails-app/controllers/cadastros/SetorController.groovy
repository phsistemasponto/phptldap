package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional


@Transactional
class SetorController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def setorList = new ArrayList<Setor>()
		int setorCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			
			order = params.order ? params.order : "id"
			sortType = params.sortType ? params.sortType : "desc"
			def orderBy = "order by " + order + " " + sortType
			
			if (tipoFiltro > 0) {
				
				switch (tipoFiltro) {
					case 1:
						setorList.addAll(
							Setor.findAll(
								"from Setor s where s.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong(), max: params.max, offset: params.offset]))
						setorCount = Setor.findAll(
								"from Setor s where s.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong()]).size()
						break;
					case 2:
						setorList.addAll(
							Setor.findAll(
								"from Setor s where upper(s.dscSetor) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						setorCount = Setor.findAll(
								"from Setor s where upper(s.dscSetor) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					default:
						break;
				}
			} else {
				setorList.addAll(
					Setor.findAll(
						"from Setor s " + orderBy,
						[max: params.max, offset: params.offset]))
				setorCount = Setor.findAll(
						"from Setor s " + orderBy).size()
			}
		}
		
		[setorInstanceList: setorList, setorInstanceTotal: setorCount, order: order, sortType: sortType]
    }

    def delete() {
        def setorInstance = Setor.get(params.id)
        if (!setorInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Setor', setorInstance.id])
        } else {
            try {
                setorInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Setor'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Setor'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [setorInstance: new Setor()]
    }

    def edit() {
        def setorInstance = Setor.get(params.id)
        if (!setorInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Setor', setorInstance.id])
            redirect(action: "list")
            return
        }

        [setorInstance: setorInstance]
    }

    def save() {
        def setorInstance = new Setor(params)
        if (!setorInstance.save(flush: true)) {
            render(view: "create", model: [setorInstance: setorInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Setor'])
        redirect(action: "create")
    }

    def update() {
        def setorInstance = Setor.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (setorInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Setor'])
                render(view: "edit", model: [setorInstance: setorInstance])
                return
            }
        }

        setorInstance.properties = params

        if (!setorInstance.save(flush: true)) {
            render(view: "edit", model: [setorInstance: setorInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Setor'])
        redirect(action: "list")
    }
}