package cadastros

import java.math.*
import java.text.*

import org.springframework.jdbc.core.JdbcTemplate

import funcoes.Horarios
import grails.converters.JSON
import groovy.sql.Sql


class CriticasExtrasController {
	
	def filtroPadraoService
	def detectDataBaseService
	def demonstracaoExtrasService
	def dataSource
	
	def index() {
		redirect(action: "list", params: params)
	}
	
	def list() {
		
		def order = params.order ? params.order : "f.id"
		def sortType = params.sortType ? params.sortType : "desc"
		
		def usuario = session['usuario']
		def filiais = usuario.getFiliais()
		def filialSelecionada = (params.filial) ? Filial.get(params.filial.toLong()) : null
		
		def periodos = new HashSet()
		def periodoSelecionado = null
		
		if (filialSelecionada != null) {
			PeriodoFilial.findAll("from PeriodoFilial where filial = :filial ", [filial: filialSelecionada]).each {
				if (!it.status.equals("E")) {
					periodos.add(it.periodo)
				}
			}
		}
		
		NumberFormat formatter = new DecimalFormat("0.00") 
		def somaValorReal = 0.0
		def somaValorReceber = 0.0
		def orcamentoFilial = 0.0
		def diferenca = 0.0
		
		def funcs = new ArrayList<Funcionario>()
		def resultadoMap = []
		
		if (params.periodo) {
			
			periodoSelecionado = Periodo.get(params.periodo.toLong())
			def periodoFilial = PeriodoFilial.findByPeriodoAndFilial(periodoSelecionado, filialSelecionada)
			
			String consultaDemonstracao = "select d.*, f.nom_func  from demonstracao_extras d "
			consultaDemonstracao += " inner join funcionario f on d.seq_func = f.id "
			consultaDemonstracao += " where (1=1) " 
			
			String filtroPadrao = filtroPadraoService.montaFiltroPadraoSqlNativo(params)
			consultaDemonstracao += filtroPadrao
			
			if (detectDataBaseService.isPostgreSql()) {
				def dtIniPeriodo = new SimpleDateFormat("yyyy-MM-dd").format(periodoSelecionado.dtIniPr)
				def dtFimPeriodo = new SimpleDateFormat("yyyy-MM-dd").format(periodoSelecionado.dtFimPr)
				def clausePeriodo = " and ( " 
					.concat(" (f.dt_resc is null and f.dt_adm <= \"" + dtFimPeriodo + "\") ") 
					.concat(" or (f.dt_resc is not null and (f.dt_adm >= \"" + dtIniPeriodo + "\" or f.dt_resc <= \"" + dtFimPeriodo + "\") ) )")
				consultaDemonstracao += clausePeriodo
			
			}
			else if (detectDataBaseService.isOracle()) {
				def dtIniPeriodo = new SimpleDateFormat("dd/MM/yyyy").format(periodoSelecionado.dtIniPr)
                def dtFimPeriodo = new SimpleDateFormat("dd/MM/yyyy").format(periodoSelecionado.dtFimPr)
                def clausePeriodo = " and ( "
                .concat(" (f.dt_resc is null and f.dt_adm <= \"" +dtFimPeriodo +"\") ")
                .concat(" or (f.dt_resc is not null and (f.dt_adm>= \"" + dtIniPeriodo + "\" or f.dt_resc <= \"" + dtFimPeriodo + "\") ) )")                    
				consultaDemonstracao += clausePeriodo
			}
			
			def clauseFilial = " and f.filial_id = " + filialSelecionada.id
			consultaDemonstracao += clauseFilial
			
			def orderBy = " order by " + order + " " + sortType
			consultaDemonstracao += orderBy
			
			String whereExpression = consultaDemonstracao.substring(consultaDemonstracao.indexOf("where")).substring(5)
			whereExpression = whereExpression.replace("\"", "''")
			
			if (detectDataBaseService.isPostgreSql()) {
				def template = new JdbcTemplate(dataSource)
				def consulta = "select prc_demostracao_ext('${whereExpression}', ${filialSelecionada.id}::integer, ${periodoFilial.id}::integer, ${usuario.id}::integer);"
				println "###" + consulta
				def valor = template.queryForObject(consulta, Integer.class)
			} else if (detectDataBaseService.isOracle()) {
			    Sql sql = new Sql(dataSource)
				def chamada = "call prc_demostracao_ext('${whereExpression}', ${filialSelecionada.id}, ${periodoFilial.id}, ${usuario.id})"
				println(chamada)
			    sql.call(chamada)
			}
			
			demonstracaoExtrasService.gerarDemonstracaoExtras(whereExpression, filialSelecionada, periodoFilial, usuario)
			
			Sql sql = new Sql(dataSource)
			consultaDemonstracao = consultaDemonstracao.replace("\"", "'")
			println consultaDemonstracao
			def demonstracoes = sql.rows(consultaDemonstracao)
			
			if (demonstracoes) {
				demonstracoes.each { d ->
					
//					ID=3469, 
//					VERSION=0, 
//					BANCO_ANT=null, 
//					COD_USU=2, 
//					COMP=0, 
//					DT_CREATION=2015-05-05 22:25:40.0, 
//					EXTRA1=0, 
//					EXTRA2=0, 
//					SEQ_FUNC=45704, 
//					ID_FECHADO=V, 
//					ID_PAGO=0, 
//					SEQ_PR=2496, 
//					PRE_CRITICA=0, 
//					SLD_EXTRA1=0, 
//					SLD_EXTRA2=0, 
//					STATUS=2, 
//					VR_RECEBER=0, 
//					VR_TOTAL=0
					
					somaValorReal = somaValorReal + d.SLD_EXTRA1 + d.SLD_EXTRA2
					somaValorReceber = somaValorReceber + d.VR_RECEBER
					
					long bancAnt = d.BANCO_ANT != null ? d.BANCO_ANT : 0
					long comp = d.COMP != null ? d.COMP : 0
					long extra1 = d.EXTRA1 != null ? d.EXTRA1 : 0
					long extra2 = d.EXTRA2 != null ? d.EXTRA2 : 0
					long preCritica = d.PRE_CRITICA != null ? d.PRE_CRITICA : 0
					
					resultadoMap.add(
						[sequencia: d.SEQ_FUNC, 
							nome: d.NOM_FUNC,  
							bancoAnterior: Horarios.retornaHora(bancAnt),
							compensacoes: Horarios.retornaHora(comp),
							extras1: Horarios.retornaHora(extra1),
							extras2: Horarios.retornaHora(extra2),
							preCritica: Horarios.retornaHora(preCritica),
							valorReal: d.VR_TOTAL,
							valorReceber: d.VR_RECEBER,
							situacao: d.ID_PAGO])
				}
			} else {
				flash.message = "N&atilde;o existe registro"
			}
			
			Orcamento o = Orcamento.findByFilialAndPeriodo(filialSelecionada, periodoSelecionado)
			
			orcamentoFilial = o != null ? o.valor : 0
			diferenca =  orcamentoFilial - somaValorReceber
		}
		
		
		[criticasList: resultadoMap, 
		 criticasTotal: resultadoMap.size(), 
		 filiais: filiais,
		 filialSelecionada: filialSelecionada,
		 periodos: periodos,
		 periodoSelecionado: periodoSelecionado,
		 somaValorReal: formatter.format(somaValorReal),
		 somaValorReceber: formatter.format(somaValorReceber),
		 orcamentoFilial: formatter.format(orcamentoFilial), 
		 diferenca: formatter.format(diferenca),
		 order: order, 
		 sortType: sortType]
	}
	
	def buscaPeriodos() {
		
		def filial = (params.filial) ? Filial.get(params.filial.toLong()) : null
		def periodos = new HashSet()
		
		if (filial != null) {
			PeriodoFilial.findAll("from PeriodoFilial where filial = :filial ", [filial: filial]).each {
				periodos.add(it.periodo)
			}
		}
		
		if (filial) {
			render periodos as JSON
		} else {
			[]
		}
	}
	
	def buscaDatasPeriodos() {
		
		def periodo = (params.periodo) ? Periodo.get(params.periodo.toLong()) : null
		def dataInicio = periodo.dtIniPrFormatada
		def dataFim = periodo.dtFimPrFormatada
		def map = [dataInicio: dataInicio, dataFim: dataFim]
		
		if (periodo) {
			render map as JSON
		} else {
			[]
		}
	}
	
	def concluir() {
		redirect(action: "list", params: params)
	}
	
}