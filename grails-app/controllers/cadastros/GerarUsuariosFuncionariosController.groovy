package cadastros

import java.math.*
import java.text.*

import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

import acesso.GrupoAcesso
import acesso.Usuario
import acesso.UsuarioGrupo


class GerarUsuariosFuncionariosController {
	
	def filtroPadraoService
	def detectDataBaseService
	def springSecurityService
	def dataSource
	
	def index() {
		def sqlFuncionariosPendentes = "from Funcionario f where f.id not in (select distinct u.funcionario.id from Usuario u where u.funcionario != null) and (f.emlFunc is not null or trim(f.emlFunc) <> '')"
		def sqlFuncionariosPendentesSemEmail = "from Funcionario f where f.id not in (select distinct u.funcionario.id from Usuario u where u.funcionario != null) and (f.emlFunc is null or trim(f.emlFunc) = '')"
		def funcionariosPendentes = Funcionario.findAll(sqlFuncionariosPendentes)
		def funcionariosSemEmailPendentes = Funcionario.findAll(sqlFuncionariosPendentesSemEmail)
		
		[funcionariosPendentes: funcionariosPendentes, funcionariosSemEmailPendentes: funcionariosSemEmailPendentes]
	}
	
	def atualizarEmail() {
		
		def atualizados = 0
		
		params.each { p ->
			if (p.key.toString().startsWith("emailFunc-")) {
				def campos = p.key.toString().split("-")
				def idFunc = campos[1]
				def email = p.value.toString()

				if (email) {
					Funcionario f = Funcionario.get(idFunc.toLong())
					f.emlFunc = email
					f.save(flush: true)
					
					if (f.errors != null && f.errors.errorCount > 0) { 
						f.errors.each { item ->
							flash.error = item.toString()
						}
					} else {
						atualizados++
					}
				}
			}
		}
		
		if (atualizados > 0) { 
			flash.message = "Numero de funcionarios atualizados: " + atualizados
		}
		
		def sqlFuncionariosPendentes = "from Funcionario f where f.id not in (select distinct u.funcionario.id from Usuario u where u.funcionario != null) and (f.emlFunc is not null or trim(f.emlFunc) <> '')"
		def sqlFuncionariosPendentesSemEmail = "from Funcionario f where f.id not in (select distinct u.funcionario.id from Usuario u where u.funcionario != null) and (f.emlFunc is null or trim(f.emlFunc) = '')"
		def funcionariosPendentes = Funcionario.findAll(sqlFuncionariosPendentes)
		def funcionariosSemEmailPendentes = Funcionario.findAll(sqlFuncionariosPendentesSemEmail)
		
		redirect(action: "index", params: params, 
			model: [funcionariosPendentes: funcionariosPendentes, funcionariosSemEmailPendentes: funcionariosSemEmailPendentes])
	}
	
	def gerarUsuarios() {
		
		def sqlFuncionariosPendentes = "from Funcionario f where f.id not in (select distinct u.funcionario.id from Usuario u where u.funcionario != null) and (f.emlFunc is not null or trim(f.emlFunc) <> '')"
		def funcs = Funcionario.findAll(sqlFuncionariosPendentes)
		def grupoFuncionario = GrupoAcesso.findByDescricao("FUNCIONARIO")
		
		def criados = 0
		
		funcs.each { f ->
			
			Empresa empresa = f.filial.empresa
			def login = gerarLogin(f)
			def senha = gerarSenha()
			
			Usuario u = new Usuario()
			u.nome = f.nomFunc
			u.email = f.emlFunc
			u.username = login
			u.password = senha
			u.enabled = true
			u.accountExpired = false
			u.accountLocked = false
			u.passwordExpired = false
			u.funcionario = f
			u.save(flush: true)
			
			UsuarioGrupo ug = new UsuarioGrupo()
			ug.usuario = u
			ug.grupo = grupoFuncionario
			ug.save(flush: true)
			
			Properties properties = System.getProperties()
			properties.setProperty("mail.smtp.host", empresa.smtpServidor)
			properties.setProperty("mail.smtp.port", empresa.smtpPorta.toString())
			properties.setProperty("mail.smtp.user", empresa.smtpUsuario)
			properties.setProperty("mail.smtp.password", empresa.smtpSenha);
			
			Session session = Session.getDefaultInstance(properties)
			
			try {
				
				def text = "Criado usuario ${u.username} no sistema de ponto eletr�nico. Senha: ${senha} ."
				println text
				
				MimeMessage message = new MimeMessage(session)
				message.setFrom(new InternetAddress(empresa.emailSmtp))
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(f.emlFunc))
				message.setSubject("Usu�rio gerado no sistema de ponto eletr�nico");
				message.setText(text);

				Transport.send(message)
			} catch( MessagingException mex ) {
				mex.printStackTrace()
			}
			
			if (u.errors != null && u.errors.errorCount > 0) {
				u.errors.each { item ->
					flash.error = item.toString()
				}
			} else {
				criados++
			}
			
		}
		
		if (criados > 0) {
			flash.message = "Numero de usuarios criados: " + criados
		}
		
		def sqlFuncionariosPendentesSemEmail = "from Funcionario f where f.id not in (select distinct u.funcionario.id from Usuario u where u.funcionario != null) and (f.emlFunc is null or trim(f.emlFunc) = '')"
		def funcionariosPendentes = Funcionario.findAll(sqlFuncionariosPendentes)
		def funcionariosSemEmailPendentes = Funcionario.findAll(sqlFuncionariosPendentesSemEmail)
		
		redirect(action: "index", params: params,
			model: [funcionariosPendentes: funcionariosPendentes, funcionariosSemEmailPendentes: funcionariosSemEmailPendentes])
	}
	
	def gerarLogin(funcionario) {
		def login = ""
		
		if (params.opcaoLogin.equals("0")) {
			login = funcionario.emlFunc
		} else if (params.opcaoLogin.equals("1")) {
			def nomeParts = funcionario.nomFunc.split(" ")
			login = nomeParts[0].concat(".").concat(nomeParts[1].substring(0, 1))
		} else {
			login = funcionario.emlFunc
		}
		
		login
	}
	
	def gerarSenha() {
		def senha = ""
		for (int i = 0; i < 10; i++) {
			def randomNumber = (Math.random() * 10).toInteger()
			senha += randomNumber.toString()
		}
		senha
	}
	
}