package cadastros

import java.text.SimpleDateFormat

import org.springframework.dao.DataIntegrityViolationException

class ParametroPeriodoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [parametroPeriodoInstanceList: ParametroPeriodo.list(params), parametroPeriodoInstanceTotal: ParametroPeriodo.count()]
    }

    def delete() {
        def parametroPeriodoInstance = ParametroPeriodo.get(params.id)
        if (!parametroPeriodoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ParametroPeriodo', parametroPeriodoInstance.id])
        } else {
            try {
                parametroPeriodoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['ParametroPeriodo'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['ParametroPeriodo'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [parametroPeriodoInstance: new ParametroPeriodo()]
    }

    def edit() {
        def parametroPeriodoInstance = ParametroPeriodo.get(params.id)
        if (!parametroPeriodoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ParametroPeriodo', parametroPeriodoInstance.id])
            redirect(action: "list")
            return
        }

        [parametroPeriodoInstance: parametroPeriodoInstance]
    }

    def save() {
        def parametroPeriodoInstance = new ParametroPeriodo(params)
        if (!parametroPeriodoInstance.save(flush: true)) {
            render(view: "create", model: [parametroPeriodoInstance: parametroPeriodoInstance])
            return
        }
		
		configuraFiliais(parametroPeriodoInstance)

		flash.message = message(code: 'default.created.message', args: ['ParametroPeriodo'])
        redirect(action: "create")
    }

    def update() {
        def parametroPeriodoInstance = ParametroPeriodo.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (parametroPeriodoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['ParametroPeriodo'])
                render(view: "edit", model: [parametroPeriodoInstance: parametroPeriodoInstance])
                return
            }
        }

        parametroPeriodoInstance.properties = params

        if (!parametroPeriodoInstance.save(flush: true)) {
            render(view: "edit", model: [parametroPeriodoInstance: parametroPeriodoInstance])
            return
        }
		
		configuraFiliais(parametroPeriodoInstance)

		flash.message = message(code: 'default.updated.message', args: ['ParametroPeriodo'])
        redirect(action: "list")
    }
	
	def configuraFiliais(ParametroPeriodo parametroPeriodoInstance) {
		
		if (parametroPeriodoInstance.id != null) {
			ParametroPeriodoFilial.findAll('from ParametroPeriodoFilial where parametroPeriodo = :p', [p: parametroPeriodoInstance]).each { pr ->
				pr.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def filiaisArray = isArray ? params.filial : [params.filial]
		
		filiaisArray.eachWithIndex { f, i ->
			if (f != null) {
				ParametroPeriodoFilial prf = new ParametroPeriodoFilial()
				prf.filial = Filial.get(filiaisArray[i].toLong())
				prf.parametroPeriodo = parametroPeriodoInstance
				
				if (parametroPeriodoInstance.filiais == null) {
					parametroPeriodoInstance.filiais = new ArrayList<ParametroPeriodoFilial>()
				}
				
				parametroPeriodoInstance.filiais.add(prf)
			}
		}
	}
	
	def criarPeriodo() {
		ParametroPeriodo parametroPeriodoInstance = ParametroPeriodo.get(params.id)
		Set<ParametroPeriodoFilial> parametrosFiliais = parametroPeriodoInstance.filiais  
		
		parametrosFiliais.each {
			Calendar calendar = new GregorianCalendar()
			calendar.set(Calendar.DAY_OF_MONTH, parametroPeriodoInstance.diaInicio)
			
			Periodo periodoJaSalvo = Periodo.find("from Periodo where :data between dtIniPr and dtFimPr", [data: calendar.getTime()])
			
			if (periodoJaSalvo != null) {
				
				PeriodoFilial periodoFilialJaSalvo = PeriodoFilial.findByFilialAndPeriodo(it.filial, periodoJaSalvo)
				if (!periodoFilialJaSalvo) {
					
					PeriodoFilial pf = new PeriodoFilial()
					pf.periodo = periodoJaSalvo
					pf.filial = it.filial
					pf.save(flush: true)
					
				}
				
			} else {
				
				Periodo novoPeriodo = new Periodo()
				novoPeriodo.dscPr = new SimpleDateFormat(parametroPeriodoInstance.padraoNome).format(calendar.getTime()).toUpperCase()
				novoPeriodo.apelPr = new SimpleDateFormat(parametroPeriodoInstance.padraoNome).format(calendar.getTime()).toUpperCase()
				novoPeriodo.idStsPr = "N"
				
				calendar.set(Calendar.DAY_OF_MONTH, parametroPeriodoInstance.diaInicio)
				novoPeriodo.setDtIniPr(calendar.getTime())
				
				if (parametroPeriodoInstance.diaFimUltimoMes) {
					calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
				} else {
					calendar.set(Calendar.DAY_OF_MONTH, parametroPeriodoInstance.diaFim)
				}
				novoPeriodo.setDtFimPr(calendar.getTime())
				
				novoPeriodo.save(flush: true)
				
				PeriodoFilial pf = new PeriodoFilial()
				pf.periodo = novoPeriodo
				pf.filial = it.filial
				pf.status = "A"
				pf.save(flush: true)
			
			}
			
		}
		
		flash.message = "Rotina invocada"
		redirect(action: "list")
	}
}