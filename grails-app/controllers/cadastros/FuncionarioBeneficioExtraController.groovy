package cadastros

import java.text.DecimalFormat
import java.text.SimpleDateFormat

import org.springframework.dao.DataIntegrityViolationException

class FuncionarioBeneficioExtraController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [funcionarioBeneficioExtraInstanceList: FuncionarioBeneficioExtra.list(params), funcionarioBeneficioExtraInstanceTotal: FuncionarioBeneficioExtra.count()]
    }

    def delete() {
        def funcionarioBeneficioExtraInstance = FuncionarioBeneficioExtra.get(params.id)
        if (!funcionarioBeneficioExtraInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FuncionarioBeneficioExtra', funcionarioBeneficioExtraInstance.id])
        } else {
            try {
                funcionarioBeneficioExtraInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['FuncionarioBeneficioExtra'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['FuncionarioBeneficioExtra'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [funcionarioBeneficioExtraInstance: new FuncionarioBeneficioExtra()]
    }

    def edit() {
        def funcionarioBeneficioExtraInstance = FuncionarioBeneficioExtra.get(params.id)
        if (!funcionarioBeneficioExtraInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FuncionarioBeneficioExtra', funcionarioBeneficioExtraInstance.id])
            redirect(action: "list")
            return
        }
		
		params.put("funcionarioId", funcionarioBeneficioExtraInstance.funcionario.crcFunc)
		params.put("funcionarioNome", funcionarioBeneficioExtraInstance.funcionario.nomFunc)
		
		params.put("beneficioId", funcionarioBeneficioExtraInstance.beneficio.id)
		params.put("beneficioDescricao", funcionarioBeneficioExtraInstance.beneficio.id)
		params.put("beneficioValor", new DecimalFormat("0.00").format(funcionarioBeneficioExtraInstance.beneficio.valorAtivo.valor))
		
		params.put("beneficioQuantidade", funcionarioBeneficioExtraInstance.quantidade)
		params.put("dataBeneficio", new SimpleDateFormat("dd/MM/yyyy").format(funcionarioBeneficioExtraInstance.dtBeneficio))

        [funcionarioBeneficioExtraInstance: funcionarioBeneficioExtraInstance]
    }

    def save() {
        def funcionarioBeneficioExtraInstance = new FuncionarioBeneficioExtra(params)
		
		Funcionario funcionario = Funcionario.findByCrcFunc(params.funcionarioId.toLong())
		Beneficio beneficio = Beneficio.get(params.beneficioId.toLong())
		
		funcionarioBeneficioExtraInstance.funcionario = funcionario
		funcionarioBeneficioExtraInstance.beneficio = beneficio
		funcionarioBeneficioExtraInstance.dtTransacao = new Date()
		funcionarioBeneficioExtraInstance.quantidade = params.beneficioQuantidade.toLong()
		funcionarioBeneficioExtraInstance.dtBeneficio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataBeneficio)
		
        if (!funcionarioBeneficioExtraInstance.save(flush: true)) {
            render(view: "create", model: [funcionarioBeneficioExtraInstance: funcionarioBeneficioExtraInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['FuncionarioBeneficioExtra'])
        redirect(action: "create")
    }

    def update() {
        def funcionarioBeneficioExtraInstance = FuncionarioBeneficioExtra.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (funcionarioBeneficioExtraInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['FuncionarioBeneficioExtra'])
                render(view: "edit", model: [funcionarioBeneficioExtraInstance: funcionarioBeneficioExtraInstance])
                return
            }
        }

        funcionarioBeneficioExtraInstance.properties = params
		
		Funcionario funcionario = Funcionario.findByCrcFunc(params.funcionarioId.toLong())
		Beneficio beneficio = Beneficio.get(params.beneficioId.toLong())
		
		funcionarioBeneficioExtraInstance.funcionario = funcionario
		funcionarioBeneficioExtraInstance.beneficio = beneficio
		funcionarioBeneficioExtraInstance.dtTransacao = new Date()
		funcionarioBeneficioExtraInstance.quantidade = params.beneficioQuantidade.toLong()
		funcionarioBeneficioExtraInstance.dtBeneficio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataBeneficio)

        if (!funcionarioBeneficioExtraInstance.save(flush: true)) {
            render(view: "edit", model: [funcionarioBeneficioExtraInstance: funcionarioBeneficioExtraInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['FuncionarioBeneficioExtra'])
        redirect(action: "list")
    }
}