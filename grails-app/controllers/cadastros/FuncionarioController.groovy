package cadastros

import java.math.*
import java.sql.Blob
import java.text.*

import javax.sql.rowset.serial.SerialBlob

import org.apache.commons.codec.binary.Base64
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile

import funcoes.Horarios


@Transactional
class FuncionarioController {
	
	def springSecurityService
	def filtroPadraoService
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def funcionarioList = new ArrayList<Funcionario>()
		int funcionarioCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			
			def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)
			
			order = params.order ? params.order : "id"
			sortType = params.sortType ? params.sortType : "desc"
			def orderBy = "order by " + order + " " + sortType
			
			if (tipoFiltro > 0) {
				
				switch (tipoFiltro) {
					case 1:
						funcionarioList.addAll(
							Funcionario.findAll(
								filtroPadrao + " and f.mtrFunc = :matricula " + orderBy,
								[matricula: params.filtro, max: params.max, offset: params.offset]))
						funcionarioCount = Funcionario.findAll(
								filtroPadrao + " and f.mtrFunc = :matricula " + orderBy,
								[matricula: params.filtro]).size()
						break;
					case 2:
						funcionarioList.addAll(
							Funcionario.findAll(
								filtroPadrao + " and upper(f.nomFunc) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						funcionarioCount = Funcionario.findAll(
								filtroPadrao + " and upper(f.nomFunc) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					case 3:
						funcionarioList.addAll(
							Funcionario.findAll(
								filtroPadrao + " and f.crcFunc = :cracha " + orderBy,
								[cracha: params.filtro.toLong(), max: params.max, offset: params.offset]))
						funcionarioCount = Funcionario.findAll(
								filtroPadrao + " and f.crcFunc = :cracha " + orderBy,
								[cracha: params.filtro.toLong()]).size()
						break;
					case 4:
						funcionarioList.addAll(
							Funcionario.findAll(
								filtroPadrao + " and f.pisFunc = :pis " + orderBy,
								[filiais: filiais, pis: params.filtro, max: params.max, offset: params.offset]))
						funcionarioCount = Funcionario.findAll(
								filtroPadrao + " and f.pisFunc = :pis " + orderBy,
								[filiais: filiais, pis: params.filtro]).size()
						break;
					default:
						break;
				}
			} else {
				
				funcionarioList.addAll(
					Funcionario.findAll(
						filtroPadrao + orderBy,
						[max: params.max, offset: params.offset]))
				funcionarioCount = Funcionario.findAll(
						filtroPadrao + orderBy).size()
			}
		}
		
        [funcionarioInstanceList: funcionarioList, funcionarioInstanceTotal: funcionarioCount, order: order, sortType: sortType]
    }
	
	@Transactional
	def image() {
		def funcionario = Funcionario.get( params.id )
		Set<FuncionarioFoto> foto = funcionario.foto
		if (foto != null && !foto.isEmpty()) {
			FuncionarioFoto funcionarioFoto
			foto.each {
				funcionarioFoto = it
			}
			Blob conteudo = funcionarioFoto.conteudo
			byte[] image = conteudo.getBytes(1l, conteudo.length().toInteger())
			response.outputStream << image
		} else {
			def baseFolder = grailsAttributes.getApplicationContext().getResource("/").getFile().toString()
			def imagesPath = baseFolder + '\\images\\user.jpg'
			File file = new File(imagesPath)
			FileInputStream fis = new FileInputStream(file)
			
			byte[] image = new byte[fis.available()]
			fis.read(image)
			
			response.outputStream << image
		}
	}

    def delete() {
        def funcionarioInstance = Funcionario.get(params.id)
        if (!funcionarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Funcionario', funcionarioInstance.id])
        } else {
            try {
                funcionarioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Funcionario'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Funcionario'])
            }
        }

        redirect(action: "list")
    }

    def create() {
		def exibeDocumento = false
		def exibeSalario = false
		def usuario = session['usuario']
		exibeDocumento = usuario.possuiFilialQueVisualizaDocumento()
		exibeSalario = usuario.possuiFilialQueVisualizaSalario()
		
		def funcionarioInstance = new Funcionario()
		funcionarioInstance.setDsrRemunerado(Horarios.calculaMinutos("07:20"))
		
        [funcionarioInstance: funcionarioInstance, 
			exibeDocumento: exibeDocumento, 
			exibeSalario: exibeSalario, 
			possuiFoto: false]
    }

	@Transactional
    def edit() {
        def funcionarioInstance = Funcionario.get(params.id)
        if (!funcionarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Funcion�rio', funcionarioInstance.id])
            redirect(action: "list")
            return
        }
		
		List<FuncionarioHorario> horarios = FuncionarioHorario.findAll(
			'from FuncionarioHorario where funcionario=:funcionario order by sequencia', [funcionario: funcionarioInstance])
		List<FuncionarioHoraExtra> horasExtras = FuncionarioHoraExtra.findAll(
			'from FuncionarioHoraExtra where funcionario=:funcionario order by sequencia', [funcionario: funcionarioInstance])
		List<FuncionarioBeneficio> beneficios = FuncionarioBeneficio.findAll(
			'from FuncionarioBeneficio where funcionario=:funcionario order by sequencia', [funcionario: funcionarioInstance])
		
		def exibeDocumento = false
		def exibeSalario = false
		def usuario = session['usuario']
		exibeDocumento = usuario.possuiFilialQueVisualizaDocumento()
		exibeSalario = usuario.possuiFilialQueVisualizaSalario()
		
		def possuiFoto = !FuncionarioFoto.findAll ('from FuncionarioFoto where funcionario=:funcionario',[funcionario: funcionarioInstance]).isEmpty()
		
        [funcionarioInstance: funcionarioInstance, 
			horarios: horarios, 
			horasExtras: horasExtras, 
			beneficios: beneficios, 
			exibeDocumento: exibeDocumento,
			exibeSalario: exibeSalario,
			possuiFoto: possuiFoto]
    }

	@Transactional
    def save() {
        def funcionarioInstance = new Funcionario()
			
		configuraInstance(funcionarioInstance, params)
		
        if (!funcionarioInstance.save(flush: true)) {
            render(view: "create", model: [funcionarioInstance: funcionarioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Funcionario', params.nomFunc])
		redirect(action: "create")
    }

	private configuraInstance(Funcionario funcionarioInstance, Map params) {
		
		/* campos data */
		if (params.dataNscFunc){
			funcionarioInstance.dtNscFunc = new Date().parse("dd/MM/yyyy", params.dataNscFunc)
		}

		if (params.dataAdm){
			funcionarioInstance.dtAdm = new Date().parse("dd/MM/yyyy", params.dataAdm)
		}

		if (params.dataResc){
			funcionarioInstance.dtResc = new Date().parse("dd/MM/yyyy", params.dataResc)
		}

		if (params.docRgDataEmi){
			funcionarioInstance.docRgDtEmi = new Date().parse("dd/MM/yyyy", params.docRgDataEmi)
		}

		if (params.docCrtPrfDataEmi){
			funcionarioInstance.docCrtPrfEmi = new Date().parse("dd/MM/yyyy", params.docCrtPrfDataEmi)
		}

		/* campos chck */
		if (params.adcNot) {
			funcionarioInstance.idAdcNot = 'S'
		} else {
			funcionarioInstance.idAdcNot = 'N'
		}

		if (params.obrigIntervalo) {
			funcionarioInstance.idObrigIntervalo = 'S'
		} else {
			funcionarioInstance.idObrigIntervalo = 'N'
		}

		if (params.intervFlex) {
			funcionarioInstance.idIntervFlex = 'S'
		} else {
			funcionarioInstance.idIntervFlex = 'N'
		}

		if (params.sensivelFeriado) {
			funcionarioInstance.idSensivelFeriado = 'S'
		} else {
			funcionarioInstance.idSensivelFeriado = 'N'
		}

		if (params.rrt) {
			funcionarioInstance.idRrt = 'S'
		} else {
			funcionarioInstance.idRrt = 'N'
		}

		Filial filial = Filial.get(params.filialId.toLong())
		Cargo cargo = Cargo.get(params.cargoId.toLong())
		Setor setor = Setor.get(params.setorId)
		Tipofunc tipoFunc = Tipofunc.get(params.tipoFuncionarioId)
		Horario horario = Horario.get(params.horarioId)
		Uniorg uniorg = (params.subUniorgId) ? Uniorg.get(params.subUniorgId) : null
		
		funcionarioInstance.situacaoFunc = (funcionarioInstance.dtResc!=null) ? Situacaofunc.findByIdstfunc("D") : Situacaofunc.findByIdstfunc("A")
		funcionarioInstance.filial = filial
		funcionarioInstance.cargo = cargo
		funcionarioInstance.setor = setor
		funcionarioInstance.tipoFunc = tipoFunc
		funcionarioInstance.uniorg = uniorg
		
		funcionarioInstance.nomFunc = params.nomFunc
		funcionarioInstance.mtrFunc = params.mtrFunc
		funcionarioInstance.emlFunc = params.emlFunc
		funcionarioInstance.pisFunc = params.pisFunc
		
		if (params.crcFunc!=''){
			funcionarioInstance.crcFunc = Long.valueOf(params.crcFunc)
		}
		
		def exibeSalario = false
		def usuario = session['usuario']
		exibeSalario = usuario.possuiFilialQueVisualizaSalario()	
				
		if (params.vrSal!='')
		  if (exibeSalario) {
		     funcionarioInstance.vrSal = Double.valueOf(params.vrSal.toString().replace('.','').replace(',','.'))
		   }else {
			funcionarioInstance.vrSal = funcionarioInstance.vrSal}
		
		funcionarioInstance.dsrRemunerado = params.dsrRemunerado ? Horarios.calculaMinutos(params.dsrRemunerado) : 0
		funcionarioInstance.docRgNr = params.docRgNr
		funcionarioInstance.docRgEmi = params.docRgEmi
		funcionarioInstance.docRgEmiUf = params.docRgEmiUf
		funcionarioInstance.docCrtPrfUf = params.docCrtPrfUf
		funcionarioInstance.baiLog = params.baiLog
		funcionarioInstance.cepLog = params.cepLog
		funcionarioInstance.cmpLog = params.cmpLog
		funcionarioInstance.docCpf = params.docCpf
		funcionarioInstance.docRegPrf = params.docRegPrf
		funcionarioInstance.dscLog = params.dscLog
		funcionarioInstance.fonCelFun = params.fonCelFun
		funcionarioInstance.fonComFun = params.fonComFun
		funcionarioInstance.fonResFun = params.fonResFun
		funcionarioInstance.nrLog = params.nrLog
		funcionarioInstance.docCrtPrf = params.docCrtPrf
		funcionarioInstance.docCrtPrfSer = params.docCrtPrfSer
		
		configuraHorarios(funcionarioInstance, params)
		configuraHorasExtras(funcionarioInstance, params)
		configuraBeneficios(funcionarioInstance, params)
		configuraFoto(funcionarioInstance, params)
		configuraFilial(funcionarioInstance, filial)
		configuraSetor(funcionarioInstance, setor)
		configuraCargo(funcionarioInstance, cargo)
	}

	private configuraHorarios(Funcionario funcionarioInstance, Map params) {
		
		if (funcionarioInstance.id != null) {
			def horarios = FuncionarioHorario.findAll ('from FuncionarioHorario where funcionario=:funcionario',[funcionario: funcionarioInstance])
			horarios.each() {
				FuncionarioHorario func = FuncionarioHorario.get(it.id)
				funcionarioInstance.horarios.remove(func)
				func.delete()
			}
		}
		
		funcionarioInstance.horarios = new HashSet<FuncionarioHorario>()
		
		for (x in 0..params.sequencialTempHorario.size()-2) {
			def seq = params.sequencialTempHorario[x].toInteger()
			def sequencial = seq+1

			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idHorario.getClass()) }
		
			def idHorario = isArray ? params.idHorario[x] : params.idHorario
			def dtInicio = isArray ? params.dtInicioHorario[x] : params.dtInicioHorario
			def dtFim = isArray ? params.dtFimHorario[x] : params.dtFimHorario
			def indice = isArray ? params.idcHorario[x] : params.idcHorario
			
			if (idHorario != null && !idHorario.isEmpty()) {
			
				FuncionarioHorario fh = new FuncionarioHorario()
				fh.funcionario = funcionarioInstance
				fh.horario = Horario.get(idHorario.toLong())
				fh.sequencia = sequencial.toLong()
				fh.dtIniVig = new SimpleDateFormat("dd/MM/yyyy").parse(dtInicio)
				fh.dtFimVig = dtFim != "" ? new SimpleDateFormat("dd/MM/yyyy").parse(dtInicio) : null
				fh.nrIndHorario = indice.toInteger()
				
				funcionarioInstance.horarios.add(fh)
				
				if (fh.dtFimVig == null) {
					// configurando horario ativo
					funcionarioInstance.horario = Horario.get(idHorario.toLong())
				}
			}
		}
	}
	
	private configuraHorasExtras(Funcionario funcionarioInstance, Map params) {
		
		if (funcionarioInstance.id != null) {
			def horasExtras = FuncionarioHoraExtra.findAll ('from FuncionarioHoraExtra where funcionario=:funcionario',[funcionario: funcionarioInstance])
			horasExtras.each() {
				FuncionarioHoraExtra func = FuncionarioHoraExtra.get(it.id)
				funcionarioInstance.horasExtras.remove(func)
				func.delete()
			}
		}
		
		funcionarioInstance.horasExtras = new HashSet<FuncionarioHoraExtra>()
		
		for (x in 0..params.sequencialTempConfiguracaoHoraExtra.size()-2) {
			def seq = params.sequencialTempConfiguracaoHoraExtra[x].toInteger()
			def sequencial = seq+1

			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idConfiguracaoHoraExtra.getClass()) }
		
			def idHoraExtra = isArray ? params.idConfiguracaoHoraExtra[x] : params.idConfiguracaoHoraExtra
			def dtInicio = isArray ? params.dtInicioConfigHoraExtra[x] : params.dtInicioConfigHoraExtra
			def dtFim = isArray ? params.dtFimConfigHoraExtra[x] : params.dtFimConfigHoraExtra
			
			if (idHoraExtra != null && !idHoraExtra.isEmpty()) {
			
				FuncionarioHoraExtra fh = new FuncionarioHoraExtra()
				fh.funcionario = funcionarioInstance
				fh.configHoraExtra = ConfiguracaoHoraExtra.get(idHoraExtra.toLong())
				fh.sequencia = sequencial.toLong()
				fh.dtIniVig = new SimpleDateFormat("dd/MM/yyyy").parse(dtInicio)
				fh.dtFimVig = dtFim != "" ? new SimpleDateFormat("dd/MM/yyyy").parse(dtInicio) : null
				
				funcionarioInstance.horasExtras.add(fh)
				
				if (fh.dtFimVig == null) {
					// configurando horario ativo
					funcionarioInstance.configuracaoHoraExtra = ConfiguracaoHoraExtra.get(idHoraExtra.toLong())
				}
			}
		}
	}
	
	private configuraBeneficios(Funcionario funcionarioInstance, Map params) {
		
		if (funcionarioInstance.id != null) {
			def beneficios = FuncionarioBeneficio.findAll ('from FuncionarioBeneficio where funcionario=:funcionario',[funcionario: funcionarioInstance])
			beneficios.each() {
				FuncionarioBeneficio func = FuncionarioBeneficio.get(it.id)
				funcionarioInstance.beneficios.remove(func)
				func.delete()
			}
		}
		
		funcionarioInstance.beneficios = new HashSet<FuncionarioBeneficio>()
		
		for (x in 0..params.sequencialTempBeneficio.size()-2) {
			def seq = params.sequencialTempBeneficio[x].toInteger()
			def sequencial = seq+1

			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idBeneficio.getClass()) }
		
			def idBeneficio = isArray ? params.idBeneficio[x] : params.idBeneficio
			def quantidade = isArray ? params.qtBeneficio[x] : params.qtBeneficio
			def dtInicio = isArray ? params.dtInicioBeneficio[x] : params.dtInicioBeneficio
			def dtFim = isArray ? params["dtFimBeneficio-"+sequencial] : params.dtFimBeneficio
			
			println "###" + params
			
			if (idBeneficio != null && !idBeneficio.isEmpty()) {
			
				FuncionarioBeneficio fh = new FuncionarioBeneficio()
				fh.funcionario = funcionarioInstance
				fh.beneficio = Beneficio.get(idBeneficio.toLong())
				fh.quantidade = quantidade.toLong()
				fh.sequencia = sequencial.toLong()
				fh.dtIniVig = new SimpleDateFormat("dd/MM/yyyy").parse(dtInicio)
				fh.dtFimVig = dtFim != null && dtFim != "" ? new SimpleDateFormat("dd/MM/yyyy").parse(dtFim) : null
				
				funcionarioInstance.beneficios.add(fh)
			}
		}
	}
	
	private configuraFoto(Funcionario funcionarioInstance, Map params) {
		
		if (params.mudouFoto.toBoolean()) {
			if (funcionarioInstance.id != null) {
				def fotoFuncionario = FuncionarioFoto.findAll ('from FuncionarioFoto where funcionario=:funcionario',[funcionario: funcionarioInstance])
				fotoFuncionario.each {
					FuncionarioFoto func = FuncionarioFoto.get(it.id)
					funcionarioInstance.foto = null
					func.delete()
				}
			}
						
			if (params.canvasContent) {
			
				def canvasContent = params.canvasContent
				canvasContent = canvasContent.replace("data:image/png;base64,", "")
				canvasContent = canvasContent.replace(" ", "+")

				def bytes = Base64.decodeBase64(canvasContent)
				
				FuncionarioFoto ff = new FuncionarioFoto()
				ff.funcionario = funcionarioInstance
				ff.conteudo = new SerialBlob(bytes)
	
				funcionarioInstance.foto = new HashSet<FuncionarioFoto>()
				funcionarioInstance.foto.add(ff)
			
			} else {
	
				def foto = params.files
		
				def inputStream
		
				if (foto instanceof CommonsMultipartFile) {
					inputStream = foto.getInputStream()
				}
		
				if (inputStream != null) {
					byte[] bytes = inputStream.bytes
		
					if (bytes.size() > 0) {
						FuncionarioFoto ff = new FuncionarioFoto()
						ff.funcionario = funcionarioInstance
						ff.conteudo = new SerialBlob(bytes)
			
						funcionarioInstance.foto = new HashSet<FuncionarioFoto>()
						funcionarioInstance.foto.add(ff)
					}
				}
			}
		}
	}
	
	private configuraFilial(Funcionario funcionarioInstance, Filial filial) {
		if (funcionarioInstance.id == null) {
			FuncionarioFilial ff = new FuncionarioFilial()
			ff.funcionario = funcionarioInstance
			ff.filial = filial
			ff.dtIniVig = new Date()
			
			funcionarioInstance.filiais = new HashSet<FuncionarioFilial>()
			funcionarioInstance.filiais.add(ff)
		} else {
			FuncionarioFilial ultimoAtivo
			funcionarioInstance.filiais.each {
				if (it.dtFimVig == null) {
					ultimoAtivo = it
				}
			}
			
			if (ultimoAtivo == null || !ultimoAtivo.filial.id.equals(filial.id)) {
				
				FuncionarioFilial ff = new FuncionarioFilial()
				ff.funcionario = funcionarioInstance
				ff.filial = filial
				ff.dtIniVig = new Date()
				
				funcionarioInstance.filiais.add(ff)
				
				if (ultimoAtivo != null) {
					ultimoAtivo.dtFimVig = new Date()
				}
			}
		}
	}
	
	private configuraSetor(Funcionario funcionarioInstance, Setor setor) {
		if (funcionarioInstance.id == null) {
			FuncionarioSetor ff = new FuncionarioSetor()
			ff.funcionario = funcionarioInstance
			ff.setor = setor
			ff.dtIniVig = new Date()
			
			funcionarioInstance.setores = new HashSet<FuncionarioSetor>()
			funcionarioInstance.setores.add(ff)
		} else {
			FuncionarioSetor ultimoAtivo
			funcionarioInstance.setores.each {
				if (it.dtFimVig == null) {
					ultimoAtivo = it
				}
			}
			if (ultimoAtivo == null || !ultimoAtivo.setor.id.equals(setor.id)) {
				
				FuncionarioSetor ff = new FuncionarioSetor()
				ff.funcionario = funcionarioInstance
				ff.setor = setor
				ff.dtIniVig = new Date()
				
				funcionarioInstance.setores.add(ff)
				
				if (ultimoAtivo != null) {
					ultimoAtivo.dtFimVig = new Date()
				}
			}
		}
	}
	
	private configuraCargo(Funcionario funcionarioInstance, Cargo cargo) {
		if (funcionarioInstance.id == null) {
			FuncionarioCargo ff = new FuncionarioCargo()
			ff.funcionario = funcionarioInstance
			ff.cargo = cargo
			ff.dtIniVig = new Date()
			
			funcionarioInstance.cargos = new HashSet<FuncionarioCargo>()
			funcionarioInstance.cargos.add(ff)
		} else {
			FuncionarioCargo ultimoAtivo
			funcionarioInstance.cargos.each {
				if (it.dtFimVig == null) {
					ultimoAtivo = it
				}
			}
			if (ultimoAtivo == null || !ultimoAtivo.cargo.id.equals(cargo.id)) {
				
				FuncionarioCargo ff = new FuncionarioCargo()
				ff.funcionario = funcionarioInstance
				ff.cargo = cargo
				ff.dtIniVig = new Date()
				
				funcionarioInstance.cargos.add(ff)
				
				if (ultimoAtivo != null) {
					ultimoAtivo.dtFimVig = new Date()
				}
			}
		}
	}

	@Transactional
    def update() {
        def funcionarioInstance = Funcionario.get(params.id)

        funcionarioInstance.properties = params
		
		configuraInstance(funcionarioInstance, params)

        if (!funcionarioInstance.save(flush: true)) {
            render(view: "edit", model: [funcionarioInstance: funcionarioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Funcionario'])
        redirect(action: "list")
    }

	
	
}