package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;


@Transactional
class TipohoraextrasController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [tipohoraextrasInstanceList: Tipohoraextras.list(params), tipohoraextrasInstanceTotal: Tipohoraextras.count()]
    }

    def delete() {
        def tipohoraextrasInstance = Tipohoraextras.get(params.id)
        if (!tipohoraextrasInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Tipohoraextras', tipohoraextrasInstance.id])
        } else {
            try {
                tipohoraextrasInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Tipohoraextras'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Tipohoraextras'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [tipohoraextrasInstance: new Tipohoraextras()]
    }

    def edit() {
        def tipohoraextrasInstance = Tipohoraextras.get(params.id)
        if (!tipohoraextrasInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Tipohoraextras', tipohoraextrasInstance.id])
            redirect(action: "list")
            return
        }

        [tipohoraextrasInstance: tipohoraextrasInstance]
    }

    def save() {
        def tipohoraextrasInstance = new Tipohoraextras(params)
        if (!tipohoraextrasInstance.save(flush: true)) {
            render(view: "create", model: [tipohoraextrasInstance: tipohoraextrasInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Tipohoraextras'])
        redirect(action: "create")
    }

    def update() {
        def tipohoraextrasInstance = Tipohoraextras.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (tipohoraextrasInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Tipohoraextras'])
                render(view: "edit", model: [tipohoraextrasInstance: tipohoraextrasInstance])
                return
            }
        }

        tipohoraextrasInstance.properties = params

        if (!tipohoraextrasInstance.save(flush: true)) {
            render(view: "edit", model: [tipohoraextrasInstance: tipohoraextrasInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Tipohoraextras'])
        redirect(action: "list")
    }
}