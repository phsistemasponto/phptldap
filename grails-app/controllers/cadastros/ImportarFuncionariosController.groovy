package cadastros

import grails.converters.JSON

import java.math.*
import java.text.*

import org.apache.poi.hssf.usermodel.HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile


@Transactional
class ImportarFuncionariosController {
	
	def filtroPadraoService
	def dataSource
	def springSecurityService
	
	def index() {	
		session.removeAttribute("registrosArquivo")
	}
	
	def lerArquivo() {
		
		def registrosArquivo = []
		
		println params.filialId
		println params.filialNome
		
		def planilha = params.files
		
		def inputStream

		if (planilha instanceof CommonsMultipartFile) {
			inputStream = planilha.getInputStream()
		}

		if (inputStream != null) {
			
			HSSFWorkbook workbook = new HSSFWorkbook(inputStream)
			HSSFSheet worksheet = workbook.getSheetAt(0)
			Iterator<HSSFRow> rowIterator = worksheet.iterator();
			
			// pula primeira linha
			rowIterator.next();
			
			while(rowIterator.hasNext()) {
				HSSFRow row = rowIterator.next();
				
				def matricula = row.getCell(0) != null ? row.getCell(0).getStringCellValue().toUpperCase() : ""
				def nome = row.getCell(1) != null ? row.getCell(1).getStringCellValue().toUpperCase() : ""
				def pis = row.getCell(2) != null ? row.getCell(2).getStringCellValue().toUpperCase() : ""
				def nomeCargo = row.getCell(3) != null ? row.getCell(3).getStringCellValue().toUpperCase() : ""
				def nomeSetor = row.getCell(4) != null ? row.getCell(4).getStringCellValue().toUpperCase() : ""
				def nascimento = row.getCell(5) != null ? row.getCell(5).getStringCellValue().toUpperCase() : ""
				def admissao = row.getCell(6) != null ? row.getCell(6).getStringCellValue().toUpperCase() : ""
				
				registrosArquivo.add([
						matricula: matricula,
						nome: nome,
						pis: pis,
						nomeCargo: nomeCargo,
						nomeSetor: nomeSetor,
						nascimento: nascimento,
						admissao: admissao,
					])
			}
			
		}
		
		session.putAt("registrosArquivo", registrosArquivo)

		render(view: "index", model: [registros: registrosArquivo])
		
	}
	
	def salvarRegistros() {
		
		def quantidadeSalvos = 0
		def erros = []
		
		Filial filial = Filial.get(params.filialId.toLong())
		Situacaofunc situacao = Situacaofunc.get(1l)
		Tipofunc tipo = Tipofunc.get(1l)
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
		
		List<String> cargos = new ArrayList<String>()
		session.registrosArquivo.each { r ->
			def nomeCargo = r.nomeCargo.trim().toUpperCase()
			if (!cargos.contains(nomeCargo)) {
				cargos.add(nomeCargo)
			}
		}
		
		Map<String, Long> cargosIds = new HashMap<String, Long>()
		cargos.each { c->
			Cargo cargo = Cargo.findByDscCargo(c)
			if (cargo == null) {
				cargo = new Cargo()
				cargo.cbo = null
				cargo.dscCargo = c
				cargo.idfolgaincrementada = "N"
				cargo.idpghrext = "N"
				cargo.save(flush: true)
				
				if (cargo.errors.errorCount == 0) {
					println "Salvo cargo " + cargo
				} else {
					println "Erro salvar cargo " + cargo.errors
				}
			}
			cargosIds.put(c, cargo.id)
		}
		
		List<String> setores = new ArrayList<String>()
		session.registrosArquivo.each { r ->
			def nomeSetor = r.nomeSetor.trim().toUpperCase()
			if (!setores.contains(nomeSetor)) {
				setores.add(nomeSetor)
			}
		}
		
		Map<String, Long> setoresIds = new HashMap<String, Long>()
		setores.each { s->
			Setor setor = Setor.findByDscSetor(s)
			if (setor == null) {
				setor = new Setor()
				setor.aplSetor = s
				setor.dscSetor = s
				setor.filial = filial
				setor.save(flush: true)
				
				if (setor.errors.errorCount == 0) {
					println "Salvo setor " + setor
				} else {
					println "Erro salvar setor " + setor.errors
				}
			}
			setoresIds.put(s, setor.id)
		}
		
		session.registrosArquivo.each { r ->
			
			def matricula = r.matricula
			def nome = r.nome.trim().toUpperCase()
			def pis = r.pis
			def nomeCargo = r.nomeCargo.trim().toUpperCase()
			def nomeSetor = r.nomeSetor.trim().toUpperCase()
			def nascimento = r.nascimento
			def admissao = r.admissao
			
			Cargo cargo = Cargo.findByDscCargo(nomeCargo)
			Setor setor = Setor.findByDscSetor(nomeSetor)
			
			Funcionario func = Funcionario.findByMtrFunc(matricula)
			
			if (func == null) {
				
				boolean validado = true
				
				func = new Funcionario()
				func.nomFunc = nome
				func.pisFunc = pis
				func.filial = filial
				func.cargo = cargo
				func.setor = setor
				
				if (matricula) {
					func.mtrFunc = matricula
				} else {
					validado = false
					erros.add([nome: nome, erro: "Matr&iacute;cula em branco"])
				}
				
				if (nascimento) {
					func.dtNscFunc = sdf.parse(nascimento)
				} else {
					validado = false
					erros.add([nome: nome, erro: "Data de nascimento em branco"])
				}
				
				if (admissao) {
					func.dtAdm = sdf.parse(admissao)
				} else {
					validado = false
					erros.add([nome: nome, erro: "Data de admiss&atilde;o em branco"])
				}
				
				func.situacaoFunc = situacao
				func.tipoFunc = tipo
				
				func.crcFunc = matricula.toLong()
				func.idObrigIntervalo = "N"
				func.idSensivelFeriado = "S"
				func.idAdcNot = "1"
				func.idIntervFlex = "1"
				
				func.save(flush: true)
				
				if (func.errors.errorCount == 0) {
					println "Salvo funcionario " + func
					quantidadeSalvos++
				} else {
					println "Erro salvar funcionario " + func.errors
				}
				
			}
			
		}
		
		println "Quantidade salvos: " + quantidadeSalvos
		
		def json = [qtRegistros: quantidadeSalvos, erros: erros]
		render json as JSON
	}
	
}