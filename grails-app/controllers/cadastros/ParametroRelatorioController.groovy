package cadastros

import org.springframework.dao.DataIntegrityViolationException


class ParametroRelatorioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
		[parametroRelatorioInstanceList: ParametroRelatorio.list(params), parametroRelatorioInstanceTotal: ParametroRelatorio.count()]
    }

    def delete() {
        def parametroRelatorioInstance = ParametroRelatorio.get(params.id)
		
		ParametroRelatorioDetalhe.findAll('from ParametroRelatorioDetalhe where parametroRelatorio = :p', [p: parametroRelatorioInstance]).each { pr ->
			pr.delete(flush: true)
		}
		
		ParametroRelatorioFilial.findAll('from ParametroRelatorioFilial where parametroRelatorio = :p', [p: parametroRelatorioInstance]).each { pr ->
			pr.delete(flush: true)
		}
		
		
        if (!parametroRelatorioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ParametroRelatorio', parametroRelatorioInstance.id])
        } else {
            try {
                parametroRelatorioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['ParametroRelatorio'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['ParametroRelatorio'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [parametroRelatorioInstance: new ParametroRelatorio(), detalhes: new ArrayList<ParametroRelatorioDetalhe>()]
    }

    def edit() {
        def parametroRelatorioInstance = ParametroRelatorio.get(params.id)
        if (!parametroRelatorioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ParametroRelatorio', parametroRelatorioInstance.id])
            redirect(action: "list")
            return
        }
		
		def detalhes = ParametroRelatorioDetalhe.findAll("from ParametroRelatorioDetalhe where parametroRelatorio = :p order by id asc", 
			[p: parametroRelatorioInstance])

        [parametroRelatorioInstance: parametroRelatorioInstance, detalhes: detalhes]
    }

    def save() {
        def parametroRelatorioInstance = new ParametroRelatorio(params)
		
		configuraDetalhes(parametroRelatorioInstance)
		configuraFiliais(parametroRelatorioInstance)
		
        if (!parametroRelatorioInstance.save(flush: true)) {
            render(view: "create", model: [parametroRelatorioInstance: parametroRelatorioInstance])
            return
        }
		
		flash.message = message(code: 'default.created.message', args: ['ParametroRelatorio'])
        redirect(action: "create")
    }

    def update() {
        def parametroRelatorioInstance = ParametroRelatorio.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (parametroRelatorioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['ParametroRelatorio'])
                render(view: "edit", model: [parametroRelatorioInstance: parametroRelatorioInstance])
                return
            }
        }

        parametroRelatorioInstance.properties = params
		
		configuraDetalhes(parametroRelatorioInstance)
		configuraFiliais(parametroRelatorioInstance)

        if (!parametroRelatorioInstance.save(flush: true)) {
            render(view: "edit", model: [parametroRelatorioInstance: parametroRelatorioInstance])
            return
        }
		
		flash.message = message(code: 'default.updated.message', args: ['ParametroRelatorio'])
        redirect(action: "list")
    }
	
	def configuraDetalhes(ParametroRelatorio parametroRelatorioInstance) {
		if (parametroRelatorioInstance.id != null) {
			ParametroRelatorioDetalhe.findAll('from ParametroRelatorioDetalhe where parametroRelatorio = :p', [p: parametroRelatorioInstance]).each { pr ->
				pr.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idOcorParametroDetalhe.getClass()) }
		def ocorrenciasArray = isArray ? params.idOcorParametroDetalhe : [params.idOcorParametroDetalhe]
		def totalizadoresArray = isArray ? params.idTotalizador : [params.idTotalizador]
		def descricoesArray = isArray ? params.descricaoParametroDetalhe : [params.descricaoParametroDetalhe]
		
		ocorrenciasArray.eachWithIndex { oid, i ->
			ParametroRelatorioDetalhe prd = new ParametroRelatorioDetalhe()
			prd.ocorrencias = Ocorrencias.get(ocorrenciasArray[i].toLong())
			prd.parametroRelatorioTotais = ParametroRelatorioTotais.get(totalizadoresArray[i].toLong())
			prd.descricao = descricoesArray[i]
			prd.parametroRelatorio = parametroRelatorioInstance
			
			if (parametroRelatorioInstance.detalhes == null) {
				parametroRelatorioInstance.detalhes = new ArrayList<ParametroRelatorioDetalhe>()
			}
			
			parametroRelatorioInstance.detalhes.add(prd)
		}
		
	}
	
	def configuraFiliais(ParametroRelatorio parametroRelatorioInstance) {
		
		println params.filial
		
		if (parametroRelatorioInstance.id != null) {
			ParametroRelatorioFilial.findAll('from ParametroRelatorioFilial where parametroRelatorio = :p', [p: parametroRelatorioInstance]).each { pr ->
				pr.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def filiaisArray = isArray ? params.filial : [params.filial]
		
		filiaisArray.eachWithIndex { f, i ->
			if (f != null) {
				ParametroRelatorioFilial prf = new ParametroRelatorioFilial()
				prf.filial = Filial.get(filiaisArray[i].toLong())
				prf.parametroRelatorio = parametroRelatorioInstance
				
				if (parametroRelatorioInstance.filiais == null) {
					parametroRelatorioInstance.filiais = new ArrayList<ParametroRelatorioFilial>()
				}
				
				parametroRelatorioInstance.filiais.add(prf)
			}
		}
	}
	
}