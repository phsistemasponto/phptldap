package cadastros

import org.springframework.dao.DataIntegrityViolationException

class ParametroCriticaExtrasController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [parametroCriticaExtrasInstanceList: ParametroCriticaExtras.list(params), parametroCriticaExtrasInstanceTotal: ParametroCriticaExtras.count()]
    }

    def delete() {
        def parametroCriticaExtrasInstance = ParametroCriticaExtras.get(params.id)
        if (!parametroCriticaExtrasInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ParametroCriticaExtras', parametroCriticaExtrasInstance.id])
        } else {
            try {
                parametroCriticaExtrasInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['ParametroCriticaExtras'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['ParametroCriticaExtras'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [parametroCriticaExtrasInstance: new ParametroCriticaExtras()]
    }

    def edit() {
        def parametroCriticaExtrasInstance = ParametroCriticaExtras.get(params.id)
        if (!parametroCriticaExtrasInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ParametroCriticaExtras', parametroCriticaExtrasInstance.id])
            redirect(action: "list")
            return
        }

        [parametroCriticaExtrasInstance: parametroCriticaExtrasInstance]
    }

    def save() {
        def parametroCriticaExtrasInstance = new ParametroCriticaExtras(params)
		
		configuraChecks(parametroCriticaExtrasInstance)
		
        if (!parametroCriticaExtrasInstance.save(flush: true)) {
            render(view: "create", model: [parametroCriticaExtrasInstance: parametroCriticaExtrasInstance])
            return
        }

		configuraFiliais(parametroCriticaExtrasInstance)
		
		flash.message = message(code: 'default.created.message', args: ['ParametroCriticaExtras'])
        redirect(action: "create")
    }

    def update() {
        def parametroCriticaExtrasInstance = ParametroCriticaExtras.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (parametroCriticaExtrasInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['ParametroCriticaExtras'])
                render(view: "edit", model: [parametroCriticaExtrasInstance: parametroCriticaExtrasInstance])
                return
            }
        }

        parametroCriticaExtrasInstance.properties = params
		
		configuraChecks(parametroCriticaExtrasInstance)

        if (!parametroCriticaExtrasInstance.save(flush: true)) {
            render(view: "edit", model: [parametroCriticaExtrasInstance: parametroCriticaExtrasInstance])
            return
        }
		
		configuraFiliais(parametroCriticaExtrasInstance)

		flash.message = message(code: 'default.updated.message', args: ['ParametroCriticaExtras'])
        redirect(action: "list")
    }
	
	def configuraChecks(ParametroCriticaExtras parametroCriticaExtrasInstance) {
		
		if (params.admExtra50){
			parametroCriticaExtrasInstance.admExtra50='S'
		} else {
			parametroCriticaExtrasInstance.admExtra50='N'
		}
		
		if (params.admExtra100){
			parametroCriticaExtrasInstance.admExtra100='S'
		} else {
			parametroCriticaExtrasInstance.admExtra100='N'
		}
		
		if (params.naoAdmExtra50){
			parametroCriticaExtrasInstance.naoAdmExtra50='S'
		} else {
			parametroCriticaExtrasInstance.naoAdmExtra50='N'
		}
		
		if (params.naoAdmExtra100){
			parametroCriticaExtrasInstance.naoAdmExtra100='S'
		} else {
			parametroCriticaExtrasInstance.naoAdmExtra100='N'
		}
		
		 
	}
	
	def configuraFiliais(ParametroCriticaExtras parametroCriticaExtrasInstance) {
		
		if (parametroCriticaExtrasInstance.id != null) {
			ParametroCriticaExtrasFilial.findAll('from ParametroCriticaExtrasFilial where parametroCriticaExtras = :p', [p: parametroCriticaExtrasInstance]).each { pr ->
				pr.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def filiaisArray = isArray ? params.filial : [params.filial]
		
		filiaisArray.eachWithIndex { f, i ->
			if (f != null) { 
				Filial filial = Filial.get(filiaisArray[i].toLong())
				ParametroCriticaExtrasFilial prf = ParametroCriticaExtrasFilial.findByFilial(filial)
				if (prf == null) {
					
					prf = new ParametroCriticaExtrasFilial()
					prf.filial = filial
					prf.parametroCriticaExtras = parametroCriticaExtrasInstance
					
					if (parametroCriticaExtrasInstance.filiais == null) {
						parametroCriticaExtrasInstance.filiais = new ArrayList<ParametroCriticaExtrasFilial>()
					}
					
					parametroCriticaExtrasInstance.filiais.add(prf)
					
				} else {
				
					flash.message = "A filial " + filial + " j&aacute; foi vinculada em outro par&acirc;metro "
				
				}
			}
		}
	}
}