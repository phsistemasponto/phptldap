package cadastros

import org.springframework.dao.DataIntegrityViolationException


import funcoes.Horarios



class ConfiguracaoHoraExtraController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [configuracaoHoraExtraInstanceList: ConfiguracaoHoraExtra.list(params), configuracaoHoraExtraInstanceTotal: ConfiguracaoHoraExtra.count()]
    }

    def delete() {
        def configuracaoHoraExtraInstance = ConfiguracaoHoraExtra.get(params.id)
        if (!configuracaoHoraExtraInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ConfiguracaoHoraExtra', configuracaoHoraExtraInstance.id])
        } else {
            try {
                configuracaoHoraExtraInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['ConfiguracaoHoraExtra'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['ConfiguracaoHoraExtra'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [configuracaoHoraExtraInstance: new ConfiguracaoHoraExtra()]
    }

    def edit() {
        def configuracaoHoraExtraInstance = ConfiguracaoHoraExtra.get(params.id)
        if (!configuracaoHoraExtraInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ConfiguracaoHoraExtra', configuracaoHoraExtraInstance.id])
            redirect(action: "list")
            return
        }

		List<ItensConfiguracaoHoraExtra> itensConfiguracaoHora = 
			ItensConfiguracaoHoraExtra.findAll('from ItensConfiguracaoHoraExtra where configuracao=:configuracao order by sequencial', 
				[configuracao: configuracaoHoraExtraInstance])
        [configuracaoHoraExtraInstance: configuracaoHoraExtraInstance, itensConfiguracaoHora: itensConfiguracaoHora]
    }

    def save() {
        def configuracaoHoraExtraInstance = new ConfiguracaoHoraExtra(params)
		
		configuraInstance(configuracaoHoraExtraInstance, params)
		
        if (!configuracaoHoraExtraInstance.save(flush: true)) {
            render(view: "create", model: [configuracaoHoraExtraInstance: configuracaoHoraExtraInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['ConfiguracaoHoraExtra'])
        redirect(action: "list")
    }

    def update() {
        def configuracaoHoraExtraInstance = ConfiguracaoHoraExtra.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (configuracaoHoraExtraInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['ConfiguracaoHoraExtra'])
                render(view: "edit", model: [configuracaoHoraExtraInstance: configuracaoHoraExtraInstance])
                return
            }
        }

        configuracaoHoraExtraInstance.properties = params
		
		def itens = ItensConfiguracaoHoraExtra.findAll('from ItensConfiguracaoHoraExtra where configuracao=:configuracao',[configuracao: configuracaoHoraExtraInstance])
		itens.each() {
			def percentuais = PercentualItensConfiguracaoHoraExtra.findAll('from PercentualItensConfiguracaoHoraExtra where itemConfig=:itemConfig', [itemConfig: it])
			percentuais.each() { p ->
				p.delete()
				it.percentuais.remove(p)
			}
			it.delete()
			configuracaoHoraExtraInstance.itens.remove(it)
		}
		
		configuraInstance(configuracaoHoraExtraInstance, params)

        if (!configuracaoHoraExtraInstance.save(flush: true)) {
            render(view: "edit", model: [configuracaoHoraExtraInstance: configuracaoHoraExtraInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['ConfiguracaoHoraExtra'])
        redirect(action: "list")
    }
	
	private Map configuraInstance(ConfiguracaoHoraExtra configuracaoHoraExtraInstance, Map params) {
		if (params.horaExtraAntesExpediente){
			configuracaoHoraExtraInstance.horaExtraAntesExpediente = 'S'
		} else {
			configuracaoHoraExtraInstance.horaExtraAntesExpediente = 'N'
		}

		if (params.horaExtraIntervaloRefeicao){
			configuracaoHoraExtraInstance.horaExtraIntervaloRefeicao = 'S'
		} else {
			configuracaoHoraExtraInstance.horaExtraIntervaloRefeicao = 'N'
		}

		for (x in 0..params.sequencialItem.size()-2){
			def seq = params.sequencialItem[x].toInteger()
			def seqItem = seq+1

			def isArrayItem = [Collection, Object[]].any { it.isAssignableFrom(params.classificacaoIdItem.getClass()) }

			def classificacaoIdItem = isArrayItem ? params.classificacaoIdItem[x] : params.classificacaoIdItem
			def toleranciaItem = isArrayItem ? params.toleranciaItem[x] : params.toleranciaItem
			def limitePorDiaItem = isArrayItem ? params.limitePorDiaItem[x] : params.limitePorDiaItem
			def diaEspecificoSemanaItem = isArrayItem ? params.diaEspecificoSemanaItemValue[x] : params.diaEspecificoSemanaItemValue
			def tipoConfiguracaoItem = isArrayItem ? params.tipoConfiguracaoItem[x] : params.tipoConfiguracaoItem
			def qtdadeHorasDescontarItem = isArrayItem ? params.qtdadeHorasDescontarItem[x] : params.qtdadeHorasDescontarItem
			def limiteHorasDescontoItem = isArrayItem ? params.limiteHorasDescontoItem[x] : params.limiteHorasDescontoItem
			def limiteQuantidadeEfetuadaDescontoItem = isArrayItem ? params.limiteQuantidadeEfetuadaDescontoItem[x] : params.limiteQuantidadeEfetuadaDescontoItem

			def item = new ItensConfiguracaoHoraExtra(
					configuracao: configuracaoHoraExtraInstance,
					classificacao: Classificacao.get(classificacaoIdItem),
					sequencial: seqItem.toLong(),
					tolerancia: toleranciaItem ? Horarios.calculaMinutos(toleranciaItem) : 0,
					limitePorDia: limitePorDiaItem ? Horarios.calculaMinutos(limitePorDiaItem) : 0,
					diaEspecificoSemana: diaEspecificoSemanaItem,
					tipoConfiguracao: tipoConfiguracaoItem,
					qtdadeHorasDescontar: qtdadeHorasDescontarItem ? Horarios.calculaMinutos(qtdadeHorasDescontarItem) : 0,
					limiteHorasDesconto: limiteHorasDescontoItem ? Horarios.calculaMinutos(limiteHorasDescontoItem) : 0,
					limiteQuantidadeEfetuadaDesconto: limiteQuantidadeEfetuadaDescontoItem ? 
						Horarios.calculaMinutos(limiteQuantidadeEfetuadaDescontoItem) : 0
					)

			item.sequencial = seqItem.toLong()

			if (configuracaoHoraExtraInstance.itens == null) {
				configuracaoHoraExtraInstance.itens = new ArrayList()
			} 
			configuracaoHoraExtraInstance.itens.add(item)

			def seqPercentual = 1
			def paramTipoHora = 'idTipoHe' + seqPercentual + 'item' + seqItem
			while (params.get(paramTipoHora) != null) {
				def idTipoHe = params.get(paramTipoHora).toLong()
				def horaIni = params.get(('horaIni' + seqPercentual + 'item' + seqItem))
				def horaFim = params.get(('horaFim' + seqPercentual + 'item' + seqItem))

				def percen = new PercentualItensConfiguracaoHoraExtra(
						itemConfig: item,
						tipoHora: Tipohoraextras.get(idTipoHe),
						sequencial: seqPercentual,
						horaInicio: Horarios.calculaMinutos(horaIni),
						horaFim: Horarios.calculaMinutos(horaFim)
						)
				
				if (item.percentuais == null) {
					item.percentuais = new ArrayList()
				}

				item.percentuais.add(percen)

				seqPercentual = seqPercentual + 1
				paramTipoHora = 'idTipoHe' + seqPercentual + 'item' + seqItem
			}

		}
		return params
	}
	
}