package cadastros

import java.math.*
import java.text.*

import org.springframework.transaction.annotation.Transactional

import acesso.Usuario
import funcoes.Horarios
import grails.converters.JSON


@Transactional
class EspelhoPontoFuncionarioController {
	
	def filtroPadraoService
	def dataSource
	def springSecurityService
	
	def index() {
		redirect(action: "list", params: params)
	}
	
	def list() {
		
		def usuario = springSecurityService.currentUser
		Usuario user = Usuario.get(usuario.id)
		Funcionario funcionario = user.funcionario
		
		if (funcionario == null) {
			flash.message = "Esse usu&aacute;rio n&atilde;o possui funcion&aacute;rio configurado"
			return
		}
		
		def dadosPrincipais = []
		
		if (params.dataInicio && params.dataFim) {
			
			Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
			
			def sql = "from Dados d where d.funcionario = :funcionario and d.dtBat between :dataInicio and :dataFim order by d.dtBat"
			def dados = Dados.findAll(sql, [funcionario: funcionario, dataInicio: dataInicio, dataFim: dataFim])
			
			dadosPrincipais = populaDadosPrincipais(dados)
			
		}
		
		[funcionario: funcionario, dadosPrincipais: dadosPrincipais]
	}
	
	def populaDadosPrincipais(dados) {
		def dadosPrincipais = []
		
		dados.each { d->
			
			ObservacaoFuncionario obs = ObservacaoFuncionario.findByFuncionarioAndDtBat(d.funcionario, d.dtBat)
			
			dadosPrincipais.add([
				identificador: d.id,
				dataBatida: new SimpleDateFormat("dd/MM/yyyy").format(d.dtBat),
				diaBatida: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(d.dtBat),
				horario: d.perfilhorario?.dscperfil,
				entrada1: d.hrBat1 ? Horarios.retornaHora(d.hrBat1) : "",
				saida1: d.hrBat2 ? Horarios.retornaHora(d.hrBat2) : "",
				entrada2: d.hrBat3 ? Horarios.retornaHora(d.hrBat3) : "",
				saida2: d.hrBat4 ? Horarios.retornaHora(d.hrBat4) : "",
				entrada3: d.hrBat5 ? Horarios.retornaHora(d.hrBat5) : "",
				saida3: d.hrBat6 ? Horarios.retornaHora(d.hrBat6) : "",
				entrada4: d.hrBat7 ? Horarios.retornaHora(d.hrBat7) : "",
				saida4: d.hrBat8 ? Horarios.retornaHora(d.hrBat8) : "",
				observacao: descricaoTipoDia(d),
				observacaoFuncionario: obs ? obs.observacao : "",
				statusObservacaoFuncionario: obs ? obs.status : "A"
			])
		}
		
		return dadosPrincipais
	}
	
	def verificaObservacao() {
		
		def resultadoMap = []
		
		def usuario = session['usuario']
		Usuario user = Usuario.get(usuario.id)
		Funcionario funcionario = user.funcionario
		Date dataObservacao = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataObservacao)
		
		ObservacaoFuncionario obs = ObservacaoFuncionario.findByFuncionarioAndDtBat(funcionario, dataObservacao)
		
		if (obs != null) {
			resultadoMap.add([id: obs.id,
				observacao: obs.observacao])
		} 
		
		if (resultadoMap) {
			render resultadoMap as JSON
		} else {
			[]
		}
	}
	
	def incluirObservacaoFuncionario() {
		
		Funcionario funcionario = Funcionario.get(params.funcionarioId.toLong())
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		Date dataObservacao = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataIncluirObservacao)
		String observacao = params.observacaoFuncionario
		
		ObservacaoFuncionario obs = ObservacaoFuncionario.findByFuncionarioAndDtBat(funcionario, dataObservacao)
		if (obs == null) {
			obs = new ObservacaoFuncionario()
			obs.funcionario = funcionario
			obs.dtBat = dataObservacao
		}
		obs.observacao = observacao
		obs.status = 'A'
		obs.save(flush: true)
		
		def sql = "from Dados d where d.funcionario = :funcionario and d.dtBat between :dataInicio and :dataFim order by d.dtBat"
		def dados = Dados.findAll(sql, [funcionario: funcionario, dataInicio: dataInicio, dataFim: dataFim])
		
		def dadosPrincipais = populaDadosPrincipais(dados)
		
		redirect(action: "list", params: params, model: [funcionario: funcionario, dadosPrincipais: dadosPrincipais])
	}
	
	String descricaoTipoDia(Dados d) {
		String retorno = ""
		
		if (d.justificativa1 != null) {
			retorno = d.justificativa1.dscJust
		} else if (d.justificativa2 != null) {
			retorno = d.justificativa2.dscJust
		} else if (d.justificativa3 != null) {
			retorno = d.justificativa3.dscJust
		}
		
		if (retorno.isEmpty()) {
			if (d.idTpDia.equals("FE")) {
				retorno = "FERIADO"
			} else if (d.idTpDia.equals("CO")) {
				retorno = "COMPENSADO"
			} else if (d.idTpDia.equals("DS")) {
				retorno = "DSR"
			} else if (d.idTpDia.equals("FG")) {
				retorno = "FOLGA"
			}
		}
		
		
		if (retorno.isEmpty()) {
			def movimentos = Movimentos.findAllByFuncionarioAndDtBat(d.funcionario, d.dtBat)
			if (movimentos != null && !movimentos.isEmpty()) {
				Movimentos m = movimentos.get(0)
				if (!m.ocorrencia.idTpOcor.equals("P")) {
					retorno = movimentos.get(0).ocorrencia.dscOcor
				}
			}
		}
		
		return retorno
	}
	
}