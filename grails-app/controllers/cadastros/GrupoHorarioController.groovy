package cadastros

import org.springframework.dao.DataIntegrityViolationException

import funcoes.Horarios


class GrupoHorarioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [grupoHorarioInstanceList: GrupoHorario.list(params), grupoHorarioInstanceTotal: GrupoHorario.count()]
    }

    def delete() {
        def grupoHorarioInstance = GrupoHorario.get(params.id)
        if (!grupoHorarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['GrupoHorario', grupoHorarioInstance.id])
        } else {
            try {
                grupoHorarioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['GrupoHorario'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['GrupoHorario'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [grupoHorarioInstance: new GrupoHorario(), horarios: new ArrayList<String>()]
    }

    def edit() {
        def grupoHorarioInstance = GrupoHorario.get(params.id)
        if (!grupoHorarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['GrupoHorario', grupoHorarioInstance.id])
            redirect(action: "list")
            return
        }
		
		def horarios = new ArrayList<String>()
		grupoHorarioInstance.itens.each { i ->
			horarios.add(Horarios.retornaHora(i.horario))
		}
		
		horarios.sort()

        [grupoHorarioInstance: grupoHorarioInstance, horarios: horarios]
    }

    def save() {
		
		def grupoHorarioInstance = new GrupoHorario(params)
        if (!grupoHorarioInstance.save(flush: true)) {
            render(view: "create", model: [grupoHorarioInstance: grupoHorarioInstance])
            return
        }
		
		def isArrayHorarios = [Collection, Object[]].any { it.isAssignableFrom(params.horarios.getClass()) }
		def horariosArray = isArrayHorarios ? params.horarios : [params.horarios]
		horariosArray.each { h ->
			GrupoItensHorario gih = new GrupoItensHorario()
			gih.grupoHorario = grupoHorarioInstance
			gih.horario = Horarios.calculaMinutos(h)
			gih.save(flush: true)
		}

		flash.message = message(code: 'default.created.message', args: ['GrupoHorario'])
        redirect(action: "create")
    }

    def update() {
        def grupoHorarioInstance = GrupoHorario.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (grupoHorarioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['GrupoHorario'])
                render(view: "edit", model: [grupoHorarioInstance: grupoHorarioInstance])
                return
            }
        }
		
		grupoHorarioInstance.properties = params

        if (!grupoHorarioInstance.save(flush: true)) {
            render(view: "edit", model: [grupoHorarioInstance: grupoHorarioInstance])
            return
        }
		
		GrupoItensHorario.removeAll(grupoHorarioInstance)
		
		def isArrayHorarios = [Collection, Object[]].any { it.isAssignableFrom(params.horarios.getClass()) }
		def horariosArray = isArrayHorarios ? params.horarios : [params.horarios]
		horariosArray.each { h ->
			GrupoItensHorario gih = new GrupoItensHorario()
			gih.grupoHorario = grupoHorarioInstance
			gih.horario = Horarios.calculaMinutos(h)
			gih.save()
		}

		flash.message = message(code: 'default.updated.message', args: ['GrupoHorario'])
        redirect(action: "list")
    }
}