package cadastros

import java.math.*
import java.text.*

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional


@Transactional
class HorarioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def horarioService
    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def horarioList = new ArrayList<Horario>()
		int horarioCount = 0 
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			if (tipoFiltro > 0) {
				order = params.order ? params.order : "id"
				sortType = params.sortType ? params.sortType : "desc"
				def orderBy = "order by " + order + " " + sortType
				switch (tipoFiltro) {
					case 1:
						horarioList.addAll(
							Horario.findAll(
								"from Horario h where h.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong(), max: params.max, offset: params.offset]))
						horarioCount = Horario.findAll(
								"from Horario h where h.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong()]).size()
						break;
					case 2:
						horarioList.addAll(
							Horario.findAll(
								"from Horario p where upper(p.dscHorario) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						horarioCount = Horario.findAll(
								"from Horario p where upper(p.dscHorario) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					default:
						break;
				}
			} else {
				flash.message = "Selecione um tipo de filtro"
			}
		}
		
        [horarioInstanceList: horarioList, horarioInstanceTotal: horarioCount, order: order, sortType: sortType]
    }

    def delete() {
        def horarioInstance 
        try {
            horarioInstance = horarioService.excluirHorario(params.id)
	        if (horarioInstance.hasErrors()) {
	            redirect(action: "list")
	            return
	        }
		} catch (DataIntegrityViolationException e) {
            flash.error = message(code: 'default.not.deleted.message', args: ['Horário'])
        }

        redirect(action: "list")
    }

    def create() {
        [horarioInstance: new Horario(), quantidadeRegistros: 0, sequencia: 0]
    }

    def edit() {
        def horarioInstance = Horario.get(params.id)
        if (!horarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Horario', horarioInstance.id])
            redirect(action: "list")
            return
        }

		List<Itemhorario> itensHorario = Itemhorario.findAll('from Itemhorario i where i.horario=:horario order by seqitemhorario',[horario: horarioInstance])
		
        [horarioInstance: horarioInstance, sequencia: (horarioInstance?.itensHorario)?horarioInstance.itensHorario.size():0, itemHorarios: itensHorario, quantidadeRegistros: (horarioInstance?.itensHorario)?horarioInstance.itensHorario.size()-1:-1 ]
    }

    def save() {
		
		println params
		
		def horarioInstance = horarioService.salvarHorario(params)
        if (horarioInstance.hasErrors()) {
            render(view: "create", model: [horarioInstance: horarioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Horário'])
        redirect(action: "create")
    }

    def update() {
		def horarioInstance = horarioService.atualizarHorario(params)
		if (horarioInstance.hasErrors()) {
			render(view: "edit", model: [horarioInstance: horarioInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: ['Horário'])
		redirect(action: "list")

    }
}