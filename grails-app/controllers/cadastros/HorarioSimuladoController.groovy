package cadastros

import org.springframework.dao.DataIntegrityViolationException

import acesso.GrupoAcesso
import acesso.Usuario
import funcoes.Horarios


class HorarioSimuladoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
		
		Usuario usuario = session['usuario']
		GrupoAcesso grupo = usuario.getAcesso()
		boolean administrador = false
		List<HorarioSimulado> horarios = new ArrayList<HorarioSimulado>()
		Integer countHorarios = 0
		
		if (grupo.descricao.equals("ADMINISTRADOR")) {
			administrador = true
			horarios.addAll(HorarioSimulado.findAll("from HorarioSimulado c where c.flag <> '3' ", 
				[max: params.max]))
			countHorarios = horarios.size() 
		} else {
			administrador = false
			horarios.addAll(HorarioSimulado.findAll("from HorarioSimulado c where c.usuarioSimulado = :usuario and c.flag <> '3' ", 
				[usuario: usuario, max: params.max]))
			countHorarios = horarios.size()
		}
		
        [horarioSimuladoInstanceList: horarios, 
			horarioSimuladoInstanceTotal: countHorarios,
			administrador: administrador]	
    }

    def delete() {
        def horarioSimuladoInstance = HorarioSimulado.get(params.id)
        if (!horarioSimuladoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['HorarioSimulado', horarioSimuladoInstance.id])
        } else {
            try {
                horarioSimuladoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['HorarioSimulado'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['HorarioSimulado'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [horarioSimuladoInstance: new HorarioSimulado()]
    }

    def edit() {
        def horarioSimuladoInstance = HorarioSimulado.get(params.id)
        if (!horarioSimuladoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['HorarioSimulado', horarioSimuladoInstance.id])
            redirect(action: "list")
            return
        }
		
		Usuario usuario = session['usuario']
		GrupoAcesso grupo = usuario.getAcesso()
		boolean administrador = false
		
		if (grupo.descricao.equals("ADMINISTRADOR")) {
			administrador = true
		}

        [horarioSimuladoInstance: horarioSimuladoInstance, 
			itemHorarios: horarioSimuladoInstance.itensHorario.sort{it.sequencia},
			administrador: administrador ]
    }

    def save() {
		
		def horarioSimuladoInstance = new HorarioSimulado(params)
		
		horarioSimuladoInstance.usuarioSimulado = session['usuario']
		horarioSimuladoInstance.dtSimulado = new Date()
		horarioSimuladoInstance.flag = "1"
		
		if (params.escala){
			horarioSimuladoInstance.escala ='S'
		} else {
			horarioSimuladoInstance.escala ='N'
		}
		
		horarioSimuladoInstance.cargaHoraria = Horarios.calculaMinutos(params.cargaHorariaTotal ? params.cargaHorariaTotal : "00:00")
		
		configuraItens(horarioSimuladoInstance)
		
		HorarioSimulado horarioSalvo = HorarioSimulado.findByHistoricoHorario(horarioSimuladoInstance.historicoHorario)
		if (horarioSalvo != null) {
			flash.message = "Hor&aacute;rio j&aacute; existe"
			render(view: "edit", model: [horarioSimuladoInstance: horarioSimuladoInstance])
			return
		}
		
        if (!horarioSimuladoInstance.save(flush: true)) {
            render(view: "create", model: [horarioSimuladoInstance: horarioSimuladoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['HorarioSimulado'])
        redirect(action: "create")
    }

    def update() {
        def horarioSimuladoInstance = HorarioSimulado.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (horarioSimuladoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['HorarioSimulado'])
                render(view: "edit", model: [horarioSimuladoInstance: horarioSimuladoInstance])
                return
            }
        }

        horarioSimuladoInstance.properties = params
		
		ItemHorarioSimulado.removeAll(horarioSimuladoInstance)
		
		configuraItens(horarioSimuladoInstance)
		
		HorarioSimulado horarioSalvo = HorarioSimulado.findByHistoricoHorario(horarioSimuladoInstance.historicoHorario)
		if (horarioSalvo != null) {
			flash.message = "Hor&aacute;rio j&aacute; existe"
			render(view: "edit", model: [horarioSimuladoInstance: horarioSimuladoInstance])
			return
		}

        if (!horarioSimuladoInstance.save(flush: true)) {
            render(view: "edit", model: [horarioSimuladoInstance: horarioSimuladoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['HorarioSimulado'])
        redirect(action: "list")
    }
	
	def configuraItens(horarioSimuladoInstance) {
		
		params.sequencia.each { p->
			
			ItemHorarioSimulado itemHorario = new ItemHorarioSimulado()
			
			def diaSemanaStr = params["nrDiaSemana-"+p]
			def hora1Str = params["itemHora1-"+p]
			def hora2Str = params["itemHora2-"+p]
			def hora3Str = params["itemHora3-"+p]
			def hora4Str = params["itemHora4-"+p]
			def cargaStr = params["itemCarga-"+p]
			def classificacaoStr = params["idClassificacao-"+p]
			
			def hora1 = hora1Str ? Horarios.calculaMinutos(hora1Str) : null
			def hora2 = hora2Str ? Horarios.calculaMinutos(hora2Str) : null
			def hora3 = hora3Str ? Horarios.calculaMinutos(hora3Str) : null
			def hora4 = hora4Str ? Horarios.calculaMinutos(hora4Str) : null
			def carga = cargaStr ? Horarios.calculaMinutos(cargaStr) : null
			def classificacao = Classificacao.get(classificacaoStr.toLong())
			
			
			itemHorario.horarioSimulado = horarioSimuladoInstance
			itemHorario.classificacao = classificacao
			itemHorario.nrDiaSemana = diaSemanaStr.toInteger()
			itemHorario.sequencia = p.toInteger()
			itemHorario.hora1 = hora1
			itemHorario.hora2 = hora2
			itemHorario.hora3 = hora3
			itemHorario.hora4 = hora4
			itemHorario.cargaHoraria = carga
			
			if (horarioSimuladoInstance.itensHorario == null) {
				horarioSimuladoInstance.itensHorario = new ArrayList<ItemHorarioSimulado>()
			}
			
			horarioSimuladoInstance.itensHorario.add(itemHorario)
		}
		
		def itensOrdenados = horarioSimuladoInstance.itensHorario.sort{
			it.sequencia
		}
		
		def historicoHorario = ""
		itensOrdenados.each { it
			if (it.sequencia <= 7) {
				def linhaDia = it.nrDiaSemana + "_" + it.classificacao.id + "_" + it.hora1 + "_" + it.hora2 + "_" + it.hora3 + "_" + it.hora4 + "#"
				historicoHorario += linhaDia
			}
		}
		
		horarioSimuladoInstance.historicoHorario = historicoHorario
		
	}
	
	def rejeitar() {
		
		HorarioSimulado horario = HorarioSimulado.get(params.id.toLong())
		horario.flag = "3"
		horario.save(flush: true)
		
		flash.message = "Hor&aacute;rio rejeitado com sucesso"
		redirect(action: "list")
		
	}
	
	def gerarHorario() {
	
		println "ID: " + params.id
		println "Nome horario: " + params.nomeHorario
		
		Usuario usuario = session['usuario']
		
		HorarioSimulado horario = HorarioSimulado.get(params.id.toLong())
		def itens = horario.itensHorario
		
		itens.each { i ->
			
			if (i.cargaHoraria != null) {
			
				def hora1 = i.hora1 ? Horarios.retornaHora(i.hora1) : ""
				def hora2 = i.hora2 ? Horarios.retornaHora(i.hora2) : ""
				def hora3 = i.hora3 ? Horarios.retornaHora(i.hora3) : ""
				def hora4 = i.hora4 ? Horarios.retornaHora(i.hora4) : ""
				def vira = (i.hora2 < i.hora1 || i.hora3 < i.hora2 || i.hora4 < i.hora3) 
				
				def dscPerfil = (hora1 + " " + hora2 + " " + hora3 + " " + hora4).trim()
				// colocado porque todos os perfil horario estao salvos com espaco no final, verificar
				dscPerfil += " "
				
				Perfilhorario perfil = Perfilhorario.findByDscperfil(dscPerfil)
				if (perfil == null) {
					
					perfil = new Perfilhorario()
					perfil.tipotolerancia = Tipotolerancia.get(1l)
					perfil.dscperfil = dscPerfil
					perfil.cargahor = i.cargaHoraria
					perfil.idvira = vira ? "S" : "N"
					perfil.save(flush: true)
					
					Bathorario bat1 = new Bathorario()
					bat1.perfilhorario = perfil
					bat1.seqbathorario = 1
					bat1.idbat = "E"
					bat1.hrbat = i.hora1
					bat1.save(flush: true)
					
					Bathorario bat2 = new Bathorario()
					bat2.perfilhorario = perfil
					bat2.seqbathorario = 2
					bat2.idbat = "S"
					bat2.hrbat = i.hora2
					bat2.save(flush: true)
					
					if (i.hora3 != null) {
						Bathorario bat3 = new Bathorario()
						bat3.perfilhorario = perfil
						bat3.seqbathorario = 3
						bat3.idbat = "E"
						bat3.hrbat = i.hora3
						bat3.save(flush: true)
					}
					
					if (i.hora4 != null) {
						Bathorario bat4 = new Bathorario()
						bat4.perfilhorario = perfil
						bat4.seqbathorario = 4
						bat4.idbat = "S"
						bat4.hrbat = i.hora4
						bat4.save(flush: true)
					}
				}
				
				i.perfilhorario = perfil
				i.save(flush: true)
			}
			
		}
		
		Date primeiraSegunda = horario.dtSimulado
		while (!primeiraSegunda.getCalendarDate().getDayOfWeek().equals(Calendar.MONDAY)) {
			primeiraSegunda = primeiraSegunda.minus(1)
		}
		
		Horario h = new Horario()
		h.dscHorario = params.nomeHorario
		h.dtIniVig = primeiraSegunda
		h.save(flush: true)
		
		itens.sort{it.sequencia}.each { i ->
			
			println "Criando item horario dia semana " + i.nrDiaSemana
			
			Itemhorario ih = new Itemhorario()
			
			ih.horario = h
			ih.classificacao = i.classificacao
			ih.perfilhorario = i.perfilhorario
			ih.seqitemhorario = i.sequencia
			ih.nrdiasemana = i.nrDiaSemana
			ih.dtinivig = primeiraSegunda
			ih.qtdechefetiva = i.cargaHoraria
			ih.idvira = i.perfilhorario ? i.perfilhorario.idvira : "N"
			
			primeiraSegunda = primeiraSegunda.plus(1)
			
			ih.save(flush: true)
		}
		
		horario.usuarioCriado = usuario
		horario.dtCriado = new Date()
		horario.flag = "2"
		horario.save(flush: true)
		
		flash.message = "Hor&aacute;rio criado com sucesso"
		redirect(action: "list")
	}
	
	
}