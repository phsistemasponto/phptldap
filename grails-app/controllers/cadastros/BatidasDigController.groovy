package cadastros

import java.text.SimpleDateFormat

import funcoes.Horarios




class BatidasDigController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		
		def batidas = []
		
		if (params.funcionarioId && params.dataInicio && params.dataFim) {
			
			Funcionario funcionario = Funcionario.findByCrcFunc(params.funcionarioId.toLong()) 
			Date dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			Date dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
			
			Date dataTemp = dtInicio
			while (dataTemp.before(dtFim) || dataTemp.equals(dtFim)) {
				
				BatidasDig batida = BatidasDig.findByFuncionarioAndData(funcionario, dataTemp)
				
				String entrada1Str = batida != null && batida.entrada1 != null ? 
					Horarios.retornaHora(batida.entrada1.toLong()) : ""
				String saida1Str = batida != null && batida.saida1 != null ?
					Horarios.retornaHora(batida.saida1.toLong()) : ""
				
				String entrada2Str = batida != null && batida.entrada2 != null ?
					Horarios.retornaHora(batida.entrada2.toLong()) : ""
				String saida2Str = batida != null && batida.saida2 != null ?
					Horarios.retornaHora(batida.saida2.toLong()) : ""
				
				String entrada3Str = batida != null && batida.entrada3 != null ?
					Horarios.retornaHora(batida.entrada3.toLong()) : ""
				String saida3Str = batida != null && batida.saida3 != null ?
					Horarios.retornaHora(batida.saida3.toLong()) : ""
				
				batidas.add([
						dia: new SimpleDateFormat("EEE").format(dataTemp),
						data: new SimpleDateFormat("dd/MM/yyyy").format(dataTemp),
						idData: new SimpleDateFormat("ddMMyyyy").format(dataTemp),
						entrada1: entrada1Str,
						saida1: saida1Str,
						entrada2: entrada2Str,
						saida2: saida2Str,
						entrada3: entrada3Str,
						saida3: saida3Str
					])
				
				dataTemp = dataTemp.plus(1)
			}
			
			[batidas: batidas]
		}
		
    }
	
	def salvar() {
		
		Funcionario funcionario = Funcionario.findByCrcFunc(params.funcionarioId.toLong())
		Date dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		Date dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		
		HashMap<Date, HashMap<String, Long>> parametrosMap = new HashMap<Date, HashMap<String, Long>>();
		
		params.each { p ->
			if (p.key.toString().startsWith("bat-")) {
				def partes = p.key.toString().split("-")
				Date dateParam = new SimpleDateFormat("ddMMyyyy").parse(partes[1])
				String tipoBatida = partes[2] 
				Long hora = p.value ? Horarios.calculaMinutos(p.value) : null
				
				if (hora != null) {
					if (parametrosMap.containsKey(dateParam)) {
						parametrosMap.get(dateParam).put(tipoBatida, hora)
					} else {
						parametrosMap.put(dateParam, new HashMap<String, Long>());
						parametrosMap.get(dateParam).put(tipoBatida, hora)
					}
				}
			}
		}
		
		parametrosMap.keySet().each { pm ->
			
			HashMap<String, Long> batidas = parametrosMap.get(pm)
			
			BatidasDig batida = BatidasDig.findByFuncionarioAndData(funcionario, pm)
			if (batida == null) {
				batida = new BatidasDig()
				batida.funcionario = funcionario
				batida.data = pm
			}
			
			batida.entrada1 = batidas.get("E1") 
			batida.saida1 = batidas.get("S1") 
			batida.entrada2 = batidas.get("E2") 
			batida.saida2 = batidas.get("S2") 
			batida.entrada3 = batidas.get("E3") 
			batida.saida3 = batidas.get("S3")
			
			batida.save(flush: true)
		}
		
		flash.message = "Cart&atilde;o atualizado com sucesso"
		redirect(action: "list", params: params)
		
	}
	
}