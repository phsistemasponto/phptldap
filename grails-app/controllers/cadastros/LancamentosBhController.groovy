package cadastros

import java.text.SimpleDateFormat

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional

import funcoes.Horarios


@Transactional
class LancamentosBhController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def lancamentosList = new ArrayList<LancamentosBh>()
		int lancamentosCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			if (tipoFiltro > 0) {
				order = params.order ? params.order : "id"
				sortType = params.sortType ? params.sortType : "desc"
				def orderBy = "order by " + order + " " + sortType
				switch (tipoFiltro) {
					case 1:
						lancamentosList.addAll(
							LancamentosBh.findAll(
								"from LancamentosBh c where c.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong(), max: params.max, offset: params.offset]))
						lancamentosCount = LancamentosBh.findAll(
								"from LancamentosBh c where c.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong()]).size()
						break;
					case 2:
						lancamentosList.addAll(
							LancamentosBh.findAll(
								"from LancamentosBh c where c.funcionario.crcFunc = :codigo " + orderBy,
								[codigo: params.filtro.toLong(), max: params.max, offset: params.offset]))
						lancamentosCount = LancamentosBh.findAll(
								"from LancamentosBh c where c.funcionario.crcFunc = :codigo " + orderBy,
								[codigo: params.filtro.toLong()]).size()
						break;
					default:
						break;
				}
			} else {
				flash.message = "Selecione um tipo de filtro"
			}
		}
		
		[lancamentosBhInstanceList: lancamentosList, lancamentosBhInstanceTotal: lancamentosCount, order: order, sortType: sortType]
    }

    def delete() {
        def lancamentosBhInstance = LancamentosBh.get(params.id)
		
		if (validaPeriodoFechado(lancamentosBhInstance)) {
			flash.message = "Impossível deletar: periodo fechado"
			redirect(action: "list")
		}
		
        if (!lancamentosBhInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['LancamentosBh', lancamentosBhInstance.id])
        } else {
            try {
                lancamentosBhInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['LancamentosBh'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['LancamentosBh'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [lancamentosBhInstance: new LancamentosBh()]
    }

    def edit() {
        def lancamentosBhInstance = LancamentosBh.get(params.id)
        if (!lancamentosBhInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['LancamentosBh', lancamentosBhInstance.id])
            redirect(action: "list")
            return
        }

        [lancamentosBhInstance: lancamentosBhInstance]
    }

    def save() {
        def lancamentosBhInstance = new LancamentosBh(params)
		
		lancamentosBhInstance.usuario = session['usuario']
		lancamentosBhInstance.dtLanc = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtLancamento)
		lancamentosBhInstance.funcionario = Funcionario.get(params.funcionarioId.toLong())
		lancamentosBhInstance.ocorrencias = Ocorrencias.get(params.ocorrenciaId.toLong())
		lancamentosBhInstance.nrQtdeHr = Horarios.calculaMinutos(params.horas)
		
		if (validaPeriodoFechado(lancamentosBhInstance)) {
			flash.message = "Periodo fechado"
			render(view: "create", model: [lancamentosBhInstance: lancamentosBhInstance])
            return
		}
		
        if (!lancamentosBhInstance.save(flush: true)) {
            render(view: "create", model: [lancamentosBhInstance: lancamentosBhInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['LancamentosBh'])
        redirect(action: "create")
    }

    def update() {
        def lancamentosBhInstance = LancamentosBh.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (lancamentosBhInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['LancamentosBh'])
                render(view: "edit", model: [lancamentosBhInstance: lancamentosBhInstance])
                return
            }
        }

        lancamentosBhInstance.properties = params
		
		lancamentosBhInstance.usuario = session['usuario']
		lancamentosBhInstance.dtLanc = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtLancamento)
		lancamentosBhInstance.funcionario = Funcionario.get(params.funcionarioId.toLong())
		lancamentosBhInstance.ocorrencias = Ocorrencias.get(params.ocorrenciaId.toLong())
		lancamentosBhInstance.nrQtdeHr = Horarios.calculaMinutos(params.horas)
		
		if (validaPeriodoFechado(lancamentosBhInstance)) {
			flash.message = "Periodo fechado"
			render(view: "edit", model: [lancamentosBhInstance: lancamentosBhInstance])
            return
		}

        if (!lancamentosBhInstance.save(flush: true)) {
            render(view: "edit", model: [lancamentosBhInstance: lancamentosBhInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['LancamentosBh'])
        redirect(action: "list")
    }
	
	def validaPeriodoFechado(lancamento) {
		Funcionario funcionario = lancamento.funcionario
		Filial filial = funcionario.filial
		Periodo periodo = Periodo.find("from Periodo where :dataLancamento between dtIniPr and dtFimPr ", [dataLancamento: lancamento.dtLanc])
		PeriodoFilial pf = PeriodoFilial.findByFilialAndPeriodo(filial, periodo)
		String status = pf != null ? pf.status : "A"
		def fechado = (status.equals("F") || status.equals("E"))
		return fechado
	}
}