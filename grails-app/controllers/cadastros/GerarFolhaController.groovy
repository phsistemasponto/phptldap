package cadastros

import groovy.sql.Sql

import java.math.*
import java.text.*

import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import javax.servlet.ServletOutputStream

import acesso.Usuario
import acesso.UsuarioFilial


class GerarFolhaController {
	
	def filtroPadraoService
	def detectDataBaseService
	def springSecurityService
	def dataSource
	
	def arquivo = ""
	
	def lsHoras = ""
	def ldHoras = ""
	def liHoras
	def restHr = 0
	def restNm = 0
	def liDias = 0
	
	def liTotalEv = 0
	def liTotalHr = 0
	def lsDir = ""
	def liTotalLin = 0
	
	def index() {
		def result = []
		
		if (params.btnFiltro) {
			
			def typeResult = params.opcaoFiltro
			def data = []
			
			if (typeResult.equals("filial")) {
				
				Periodo periodo = Periodo.get(params.periodo.toLong())
				Usuario usuario = springSecurityService.currentUser
				List<UsuarioFilial> uf = usuario.getUsuarioFiliais()
				uf.each {
					PeriodoFilial pf = PeriodoFilial.findByPeriodoAndFilial(periodo, it.filial)
					data.add([filial: it.filial, status: pf.status])
				}
				
			} else if (typeResult.equals("funcionario")) {
			
				def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)
				def orderby = " order by f.id "
				filtroPadrao += orderby
				
				data = Funcionario.findAll(filtroPadrao)
			} 
			
			result = [typeResult: typeResult, data: data]
		}
		
		[result: result]
	}
	
	def processarFiliais() {
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def arrayFilial = isArray ? params.filial : [params.filial]
		
		def retorno = processamento(null, arrayFilial)
		
		if (retorno instanceof Integer) {
			render(status: retorno)
		} else {
			render(status: response.SC_OK, text: retorno.idFolha)
		}
	}
	
	def processarFuncionarios() {
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.funcionario.getClass()) }
		def arrayFuncionario = isArray ? params.funcionario : [params.funcionario]
		def funcionarios = Funcionario.findAll("from Funcionario f where f.id in ("+ arrayFuncionario.toString().replace("[", "").replace("]", "") + ")")
		
		def retorno = processamento(funcionarios, null)
		
		if (retorno instanceof Integer) {
			render(status: retorno)
		} else {
			render(status: response.SC_OK, text: retorno.idFolha)
		}
		
	}
	
	def baixarArquivo() {
		GeracaoFolha gf = GeracaoFolha.get(params.baixarArquivoId)
		String arquivo = gf.arquivo
		
		//get the output stream from the common property 'response'
		ServletOutputStream servletOutputStream = response.outputStream
 
		//set the byte content on the response. This determines what is shown in your browser window.
		response.setContentType("text/plain")
		response.setContentLength(arquivo.length())
	 
		response.setHeader('Content-disposition', 'attachment; filename='.concat(gf.folha.nomArq).concat(".txt"))
		response.setHeader('Expires', '0');
		response.setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
		servletOutputStream << arquivo.getBytes()
		servletOutputStream.flush()
	}
	
	def processamento(funcionarios, arrayFilial) {
		
		def opcaoFolha = params.cmpOpcaoFolha
		def opcaoPeriodo = params.cmpPeriodo
		def folha = Folha.get(opcaoFolha.toLong())
		
		arquivo = ""
		
		if (opcaoFolha.equals("1")) {
			
			if (detectDataBaseService.isPostgreSql()) {
				return response.SC_FORBIDDEN
			}
			
			// FEDEX
			
			def query = """
				select trim(TO_CHAR(1,'00')) 
				    || trim(TO_CHAR(fec.seq_func,'0000000'))
				    || TO_CHAR(add_months(p.dt_ini_pr,1),'MMYYYY') 
				    || ef.dsc_evento 
				    || trim(CASE WHEN ef.exp_horas = 'S' THEN lpad(Minu_to_hora1(fec.nr_qt_horas),20,'0') || 'N' 
						 WHEN ef.exp_dias = 'S' THEN to_char(fec.nr_qt,'00000000000000000') || '000N' 
						 ELSE lpad(Minu_to_hora1(fec.nr_qt_horas),20,'0') || 'N' END) as result
				from fechamento fec
				inner join funcionario f on fec.seq_func = f.id
				inner join folha_filial ff on ff.filial_id = f.filial_id
				inner join folha fol on ff.folha_id = fol.id
				inner join eventos_folha ef on ef.folha_id = fol.id and ef.ocorrencias_id = fec.seq_ocor and ef.tipo_evento = 1
				inner join periodo_filial pf on fec.seq_pr = pf.id
				inner join periodo p on pf.periodo_id = p.id
				WHERE (1=1)
						"""
			
			if (arrayFilial != null) {
				query += " AND f.filial_id in ("+ arrayFilial.toString().replace("[", "").replace("]", "") + ")"
			} else {
				query += " AND fec.seq_func in ("+ funcionarios.toString().replace("[", "").replace("]", "") + ")"
			}  
			query += " AND p.id = " + opcaoPeriodo
			
			Sql sql = new Sql(dataSource)
			def results = sql.rows(query)
			
			results.each {
				arquivo = arquivo.concat(it.result)
				arquivo = arquivo.concat("\r\n")
			}
			
			
		} else if (opcaoFolha.equals("2")) {
		
			// FORTES
				
			geraArquivoHeader(funcionarios, arrayFilial)
			geraArquivoDetalhe(funcionarios, arrayFilial, opcaoPeriodo, opcaoFolha)
			geraArquivoFooter()
		
		}
		
		GeracaoFolha gf = new GeracaoFolha()
		gf.folha = folha
		gf.dtGeracao = new Date()
		gf.arquivo = arquivo.trim()
		gf.save(flush: true)
		
		if (folha.getEmail() != null && !folha.getEmail().trim().isEmpty()) {
			
			boolean enviouEmail = false
			
			Set<Filial> filiais = new HashSet<Filial>()
			funcionarios.each {
				if (!filiais.contains(it.filial)) {
					filiais.add(it.filial)
				}
			}
			
			filiais.each {
				Empresa empresa = it.empresa
				
				Properties properties = System.getProperties()
				properties.setProperty("mail.smtp.host", empresa.smtpServidor)
				properties.setProperty("mail.smtp.port", empresa.smtpPorta.toString())
				properties.setProperty("mail.smtp.user", empresa.smtpUsuario)
				properties.setProperty("mail.smtp.password", empresa.smtpSenha);
				
				Session session = Session.getDefaultInstance(properties)
				
				try {
					MimeMessage message = new MimeMessage(session)
					message.setFrom(new InternetAddress(empresa.emailSmtp))
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(folha.getEmail()))
					message.setSubject("Arquivo gerado folha: " + folha.getNomArq());
					message.setText(arquivo);
	
					Transport.send(message)
					enviouEmail = true
				} catch( MessagingException mex ) {
					mex.printStackTrace()
					enviouEmail = false 
				}
			}
			
			return enviouEmail ? response.SC_OK : [idFolha: gf.id]
			
		} else {
		
			return [idFolha: gf.id]
			
		}
		
	}
	
	// Geracao arquivos fortes
	
	def geraArquivoHeader(funcionarios, arrayFilial) {
		
		def query = """
					select '01XXXXXXXXACPONTOXXXXX'||trim(to_char(fil.unicod_filial::integer,'0000                                                  ')) as reader
					from funcionario f
					inner join filial fil on f.filial_id = fil.id
					where  (1=1)
				  """

		if (arrayFilial != null) {
			query += " AND fil.id in ("+ arrayFilial.toString().replace("[", "").replace("]", "") + ") "
		} else {
			query += " AND f.id in ( " + funcionariosIds(funcionarios) + ") "
		}
		
		if (detectDataBaseService.isPostgreSql()) {
			query += " limit 1 "
		} else {
			query += " rownum = 1 "
		}
		
		Sql sql = new Sql(dataSource)
		def results = sql.rows(query)
		results.each {
			arquivo += it.reader
		}
		
		arquivo += "\r\n"
		
	}
	
	def geraArquivoDetalhe(funcionarios, arrayFilial, periodo, folha) {
		
		lsHoras = ""
		ldHoras = ""
		lsDir = ""
		
		restHr = 0
		restNm = 0
		liDias = 0
		liHoras = 0
		liTotalEv = 0
		liTotalHr = 0
		liTotalLin = 0
		
		def query = """
					select fil.empresa_id
						 ,fil.id
						 ,f.id
						 ,f.mtr_func
						 ,fec.nr_qt_horas
						 ,fec.nr_qt
						 ,fec.seq_ocor 
						 ,e.id
						 ,e.dsc_evento
						 ,e.exp_dias
						 ,e.exp_horas
						 ,e.exp_horas_hex 
					from fechamento fec
					inner join funcionario f on fec.seq_func = f.id
					inner join filial fil on f.filial_id = fil.id
					inner join periodo_filial pf on pf.filial_id = fil.id
					inner join eventos_folha e on e.ocorrencias_id = fec.seq_ocor
					where  (1=1)
					"""

		if (arrayFilial != null) {
			query += " AND fil.id in ("+ arrayFilial.toString().replace("[", "").replace("]", "") + ") "
		} else {
			query += " and f.id in (" + funcionariosIds(funcionarios) + ") "
		}
		
		query += " and e.folha_id = " + folha
		query += " and pf.periodo_id = " + periodo
		query += " order by e.id,f.id "
		
		Sql sql = new Sql(dataSource)
		def results = sql.rows(query)
		results.each {
			
			liTotalLin++
			
			def expHoras = it.exp_horas
			def expHorasHex = it.exp_horas_hex
			if (expHoras.equals("S")) {
				liHoras = liHoras +  it.nr_qt_horas // strtofloat(MHora_ponto(D_IModulo.rs.FieldByName('nr_qt_horas').asinteger));
				lsHoras = it.nr_qt_horas // MHora_ponto(D_IModulo.rs.FieldByName('nr_qt_horas').asinteger);
			} else if (expHorasHex.equals("S")) {
				liHoras = liHoras +  it.nr_qt_horas // strtofloat(MHora_ponto_hex(D_IModulo.rs.FieldByName('nr_qt_horas').asinteger))
				lsHoras = it.nr_qt_horas //MHora_ponto_hex(D_IModulo.rs.FieldByName('nr_qt_horas').asinteger)
			} else {
				liHoras = liHoras + it.nr_qt
				lsHoras = it.nr_qt
			}
			
			def matricula = strZero(it.mtr_func, 6)
			def arqHoras = strZero(lsHoras, 10)
			def arqFim = strZero("", 16)
			
			arquivo += "1"  + matricula + it.dsc_evento + arqHoras + arqFim 
			arquivo += "\r\n"
			
			if (liDias > 0) {
				liHoras = liHoras + (liDias * 60)
				liTotalHr = liHoras
			} else {
				liTotalHr = liHoras
			}
			
		}
		
	}
	
	def geraArquivoFooter() {
		arquivo += "Z"  + strZero(liTotalLin, 5) + strZero(liTotalHr, 15) + strZero("", 15)
		arquivo += "\r\n"
	}
	
	def funcionariosIds(funcionarios) {
		def arrayFuncionario = funcionarios.collect { it.id }
		arrayFuncionario.toString().replace("[", "").replace("]")
	}
	
	def strZero(text, qt) {
		if (text.toString().length() == 6) {
			return text
		} else {
			String retorno = ""
			for (int i = 0; i <= qt; i++) {
				retorno += "0"
			}
			retorno += text
			return retorno.substring(retorno.length()-qt)
		}
	}
	
}