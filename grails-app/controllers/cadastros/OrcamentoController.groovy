package cadastros

import org.springframework.dao.DataIntegrityViolationException

class OrcamentoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def usuario = session['usuario']
		def filiais = usuario.getFiliais()
		
		def periodos = new HashSet()
		def periodoSelecionado = null
		def resultadoMap = []
		
		PeriodoFilial.findAll("from PeriodoFilial where filial in (:filiais)", [filiais: filiais]).each {
			periodos.add(it.periodo)
		}
		
		if (params.periodo) {
			periodoSelecionado = Periodo.get(params.periodo.toLong())
			
			Filial filial
			Periodo periodo
			
			filiais.each { f ->
				Orcamento orcamento = Orcamento.findByFilialAndPeriodo(f, periodoSelecionado)
				String valor = orcamento != null ? orcamento.valorFormatado : "0,00" 
				Boolean liberado = orcamento != null ? orcamento.liberado : false
				resultadoMap.add([filial: f, periodo: periodoSelecionado, liberado: liberado, valor: valor])
			}
			
		}
		
		[orcamentoInstanceList: resultadoMap.subList(params.offset, Math.min((params.offset+params.max), resultadoMap.size())), 
			orcamentoInstanceTotal: resultadoMap.size(), periodos: periodos, periodoSelecionado: periodoSelecionado]
    }
	
	def salvaOrcamento() {
		params.each { p ->
			if (p.key.toString().startsWith("valor_")) {
				def campos = p.key.toString().split("_")
				def filial = Filial.get(campos[1].toLong())
				def periodo = Periodo.get(campos[2].toLong())
				Orcamento orcamento = Orcamento.findByFilialAndPeriodo(filial, periodo)
				if (orcamento != null) {
					orcamento.valor = Double.valueOf(p.value.toString().replace('.','').replace(',','.'))
					orcamento.save(flush: true)
				} else {
					Orcamento newOrcamento = new Orcamento()
					newOrcamento.filial = filial
					newOrcamento.periodo = periodo
					newOrcamento.liberado = false
					newOrcamento.valor = Double.valueOf(p.value.toString().replace('.','').replace(',','.'))
					newOrcamento.save(flush: true)
				}
			}
		}
		flash.message = "Opera&ccedil;&atilde;o conclu&iacute;da com sucesso"
		redirect(action: "list", params: params)
	}

    def delete() {
        def orcamentoInstance = Orcamento.get(params.id)
        if (!orcamentoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Orcamento', orcamentoInstance.id])
        } else {
            try {
                orcamentoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Orcamento'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Orcamento'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [orcamentoInstance: new Orcamento()]
    }

    def edit() {
        def orcamentoInstance = Orcamento.get(params.id)
        if (!orcamentoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Orcamento', orcamentoInstance.id])
            redirect(action: "list")
            return
        }

        [orcamentoInstance: orcamentoInstance]
    }

    def save() {
        def orcamentoInstance = new Orcamento(params)
        if (!orcamentoInstance.save(flush: true)) {
            render(view: "create", model: [orcamentoInstance: orcamentoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Orcamento'])
        redirect(action: "create")
    }

    def update() {
        def orcamentoInstance = Orcamento.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (orcamentoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Orcamento'])
                render(view: "edit", model: [orcamentoInstance: orcamentoInstance])
                return
            }
        }

        orcamentoInstance.properties = params

        if (!orcamentoInstance.save(flush: true)) {
            render(view: "edit", model: [orcamentoInstance: orcamentoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Orcamento'])
        redirect(action: "list")
    }
}