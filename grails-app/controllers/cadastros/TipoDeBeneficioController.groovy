package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;

@Transactional
class TipoDeBeneficioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [tipoDeBeneficioInstanceList: TipoDeBeneficio.list(params), tipoDeBeneficioInstanceTotal: TipoDeBeneficio.count()]
    }

    def delete() {
        def tipoDeBeneficioInstance = TipoDeBeneficio.get(params.id)
        if (!tipoDeBeneficioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['TipoDeBeneficio', tipoDeBeneficioInstance.id])
        } else {
            try {
                tipoDeBeneficioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['TipoDeBeneficio'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['TipoDeBeneficio'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [tipoDeBeneficioInstance: new TipoDeBeneficio()]
    }

    def edit() {
        def tipoDeBeneficioInstance = TipoDeBeneficio.get(params.id)
        if (!tipoDeBeneficioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['TipoDeBeneficio', tipoDeBeneficioInstance.id])
            redirect(action: "list")
            return
        }

        [tipoDeBeneficioInstance: tipoDeBeneficioInstance]
    }

    def save() {
        def tipoDeBeneficioInstance = new TipoDeBeneficio(params)
        if (!tipoDeBeneficioInstance.save(flush: true)) {
            render(view: "create", model: [tipoDeBeneficioInstance: tipoDeBeneficioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['TipoDeBeneficio'])
        redirect(action: "create")
    }

    def update() {
        def tipoDeBeneficioInstance = TipoDeBeneficio.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (tipoDeBeneficioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['TipoDeBeneficio'])
                render(view: "edit", model: [tipoDeBeneficioInstance: tipoDeBeneficioInstance])
                return
            }
        }

        tipoDeBeneficioInstance.properties = params

        if (!tipoDeBeneficioInstance.save(flush: true)) {
            render(view: "edit", model: [tipoDeBeneficioInstance: tipoDeBeneficioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['TipoDeBeneficio'])
        redirect(action: "list")
    }
}