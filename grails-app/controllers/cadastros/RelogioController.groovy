package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional


@Transactional
class RelogioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [relogioInstanceList: Relogio.list(params), relogioInstanceTotal: Relogio.count()]
    }

    def delete() {
        def relogioInstance = Relogio.get(params.id)
        if (!relogioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Relogio', relogioInstance.id])
        } else {
            try {
                relogioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Relogio'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Relogio'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [relogioInstance: new Relogio()]
    }

    def edit() {
        def relogioInstance = Relogio.get(params.id)
		
        if (!relogioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Relogio', relogioInstance.id])
            redirect(action: "list")
            return
        }
		
		List<ParamRelogio> parametros = ParamRelogio.findAll('from ParamRelogio where relogio=:relogio order by sequencial',[relogio: relogioInstance])

        [relogioInstance: relogioInstance, parametrosRelogio: parametros]
    }

	private configuraInstance(Relogio relogioInstance) {
		if (params.idOnline){
			relogioInstance.idOnline='S'
		} else {
			relogioInstance.idOnline='N'
		}

		if (params.idAfd){
			relogioInstance.idAfd='S'
		} else {
			relogioInstance.idAfd='N'
		}

		if (params.idEntSai){
			relogioInstance.idEntSai='S'
		} else {
			relogioInstance.idEntSai='N'
		}

		if (params.idAfdt){
			relogioInstance.idAfdt='S'
		} else {
			relogioInstance.idAfdt='N'
		}
		
		relogioInstance.filial = Filial.get(params.filialId)
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idIndice.getClass()) }
		def indicesArray = isArray ? params.idIndice : [params.idIndice]
		def posIniArray = isArray ? params.posIni : [params.posIni]
		def posFimArray = isArray ? params.posFim : [params.posFim]
		
		if (indicesArray.size() > 0) {
			
			indicesArray.eachWithIndex { ind, i ->
				
				if (ind != null) {
					
					def parametroRelogio = new ParamRelogio(
						relogio: relogioInstance,
						indice: IndiceLayoutRelogio.get(ind.toLong()),
						sequencial: i,
						posicaoInicial: posIniArray[i].toInteger(),
						posicaoFinal: posFimArray[i].toInteger(),
					)
					
					if (relogioInstance.parametros == null) {
						relogioInstance.parametros = new ArrayList()
					}
					
					relogioInstance.parametros.add(parametroRelogio)
				}
				
			}
		}
		
	}

    def save() {
        def relogioInstance = new Relogio(params)
		
		configuraInstance(relogioInstance)
		
        if (!relogioInstance.save(flush: true)) {
            render(view: "create", model: [relogioInstance: relogioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Relogio'])
        redirect(action: "list")
    }

    def update() {
        def relogioInstance = Relogio.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (relogioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Relogio'])
                render(view: "edit", model: [relogioInstance: relogioInstance])
                return
            }
        }
		
		def parametros = ParamRelogio.findAll('from ParamRelogio where relogio=:relogio',[relogio: relogioInstance])
		parametros.each() {
			it.delete()
			relogioInstance.parametros.remove(it)
		}

        relogioInstance.properties = params
		
		configuraInstance(relogioInstance)

        if (!relogioInstance.save(flush: true)) {
            render(view: "edit", model: [relogioInstance: relogioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Relogio'])
        redirect(action: "list")
    }
	
}