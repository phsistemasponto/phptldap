package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional

@Transactional
class PerfilhorarioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def perfilhorarioService
	
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def perfilList = new ArrayList<Perfilhorario>()
		int perfilCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			if (tipoFiltro > 0) {
				order = params.order ? params.order : "id"
				sortType = params.sortType ? params.sortType : "desc"
				def orderBy = "order by " + order + " " + sortType
				switch (tipoFiltro) {
					case 1:
						perfilList.addAll(
							Perfilhorario.findAll(
								"from Perfilhorario p where p.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong(), max: params.max, offset: params.offset]))
						perfilCount = Perfilhorario.findAll(
								"from Perfilhorario p where p.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong()]).size()
						break;
					case 2:
						perfilList.addAll(
							Perfilhorario.findAll(
								"from Perfilhorario p where upper(p.dscperfil) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						perfilCount = Perfilhorario.findAll(
								"from Perfilhorario p where upper(p.dscperfil) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					default:
						break;
				}
			} else {
				flash.message = "Selecione um tipo de filtro"
			}
		}
		
		[perfilhorarioInstanceList: perfilList, perfilhorarioInstanceTotal: perfilCount, order: order, sortType: sortType]
    }

    def delete() {
        def perfilhorarioInstance = Perfilhorario.get(params.id)
        if (!perfilhorarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Perfilhorario', perfilhorarioInstance.id])
        } else {
            try {
                perfilhorarioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Perfilhorario'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Perfilhorario'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [perfilhorarioInstance: new Perfilhorario()]
    }

    def edit() {
        def perfilhorarioInstance = Perfilhorario.get(params.id)
        if (!perfilhorarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Perfilhorario', perfilhorarioInstance.id])
            redirect(action: "list")
            return
        }
		
		List<Bathorario> bathorarios = Bathorario.findAll('from Bathorario where perfilhorario=:perfilhorario order by seqbathorario',[perfilhorario: perfilhorarioInstance])

		
        [perfilhorarioInstance: perfilhorarioInstance,quantidadeRegistros: perfilhorarioInstance.bathorarios.size()-1, sequencia: (perfilhorarioInstance.bathorarios)?perfilhorarioInstance.bathorarios.size()+1:0, bathorarios: bathorarios]
    }

    def save() {

        def perfilhorarioInstance = perfilhorarioService.salvarPerfilhorario(params)
        if (perfilhorarioInstance.hasErrors()) {
            render(view: "create", model: [perfilhorarioInstance: perfilhorarioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Perfilhorario'])
        redirect(action: "create")
    }

    def update() {
		
		def perfilhorarioInstance = perfilhorarioService.atualizarPerfilhorario(params)
		if (perfilhorarioInstance.hasErrors()) {
			render(view: "edit", model: [perfilhorarioInstance: perfilhorarioInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: ['Perfil/Horário'])
        redirect(action: "list")
    }
}