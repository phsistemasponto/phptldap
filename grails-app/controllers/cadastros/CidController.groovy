package cadastros

import org.springframework.dao.DataIntegrityViolationException

class CidController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [cidInstanceList: Cid.list(params), cidInstanceTotal: Cid.count()]
    }

    def delete() {
        def cidInstance = Cid.get(params.id)
        if (!cidInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Cid', cidInstance.id])
        } else {
            try {
                cidInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Cid'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Cid'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [cidInstance: new Cid()]
    }

    def edit() {
        def cidInstance = Cid.get(params.id)
        if (!cidInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Cid', cidInstance.id])
            redirect(action: "list")
            return
        }

        [cidInstance: cidInstance]
    }

    def save() {
        def cidInstance = new Cid(params)
        if (!cidInstance.save(flush: true)) {
            render(view: "create", model: [cidInstance: cidInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Cid'])
        redirect(action: "create")
    }

    def update() {
        def cidInstance = Cid.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (cidInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Cid'])
                render(view: "edit", model: [cidInstance: cidInstance])
                return
            }
        }

        cidInstance.properties = params

        if (!cidInstance.save(flush: true)) {
            render(view: "edit", model: [cidInstance: cidInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Cid'])
        redirect(action: "list")
    }
}