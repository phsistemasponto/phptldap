package cadastros

import org.springframework.dao.DataIntegrityViolationException

class GrupoController {

	def springSecurityService
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [grupoInstanceList: Grupo.list(params), grupoInstanceTotal: Grupo.count()]
    }

    def delete() {
        def grupoInstance = Grupo.get(params.id)
        if (!grupoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Grupo', grupoInstance.id])
        } else {
            try {
                grupoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Grupo'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Grupo'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [grupoInstance: new Grupo()]
    }

    def edit() {
        def grupoInstance = Grupo.get(params.id)
        if (!grupoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Grupo', grupoInstance.id])
            redirect(action: "list")
            return
        }

        [grupoInstance: grupoInstance, funcionarios: GrupoFuncionario.findAll('from GrupoFuncionario where grupo=:grupo',[grupo: grupoInstance])]
    }

    def save() {
        def grupoInstance = new Grupo(params)
		grupoInstance.usuario = springSecurityService.currentUser
        if (!grupoInstance.save()) {
            render(view: "create", model: [grupoInstance: grupoInstance])
            return
        }
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.funcionarioIdTable.getClass()) }
		def funcionariosArray = isArray ? params.funcionarioIdTable : [params.funcionarioIdTable]
		funcionariosArray.each {
			Funcionario f = Funcionario.get(it.toLong())
			
			GrupoFuncionario gf = new GrupoFuncionario()
			gf.funcionario = f
			gf.grupo = grupoInstance
			
			gf.save()
		}

		flash.message = message(code: 'default.created.message', args: ['Grupo'])
        redirect(action: "create")
    }

    def update() {
        def grupoInstance = Grupo.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (grupoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Grupo'])
                render(view: "edit", model: [grupoInstance: grupoInstance])
                return
            }
        }

        grupoInstance.properties = params
		grupoInstance.usuario = springSecurityService.currentUser

        if (!grupoInstance.save()) {
            render(view: "edit", model: [grupoInstance: grupoInstance])
            return
        }
		
		def filhos = GrupoFuncionario.findAll('from GrupoFuncionario where grupo=:grupo',[grupo: grupoInstance])
		filhos.each() {
			it.delete()
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.funcionarioIdTable.getClass()) }
		def funcionariosArray = isArray ? params.funcionarioIdTable : [params.funcionarioIdTable]
		funcionariosArray.each {
			Funcionario f = Funcionario.get(it.toLong())
			
			GrupoFuncionario gf = new GrupoFuncionario()
			gf.funcionario = f
			gf.grupo = grupoInstance
			
			gf.save()
		}
		

		flash.message = message(code: 'default.updated.message', args: ['Grupo'])
        redirect(action: "list")
    }
}