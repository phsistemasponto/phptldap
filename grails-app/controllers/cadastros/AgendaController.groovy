package cadastros

import org.springframework.dao.DataIntegrityViolationException

class AgendaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [agendaInstanceList: Agenda.list(params), agendaInstanceTotal: Agenda.count()]
    }

    def delete() {
        def agendaInstance = Agenda.get(params.id)
        if (!agendaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Agenda', agendaInstance.id])
        } else {
            try {
                agendaInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Agenda'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Agenda'])
            }
        }

        redirect(action: "list")
    }

    def create() {
		HashMap<Integer, Long> mapHorarios = new HashMap<Integer, Long>() 
        [agendaInstance: new Agenda(), mapHorarios: mapHorarios]
    }

    def edit() {
        def agendaInstance = Agenda.get(params.id)
        if (!agendaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Agenda', agendaInstance.id])
            redirect(action: "list")
            return
        }
		
		HashMap<Integer, Long> mapHorarios = new HashMap<Integer, Long>()
		agendaInstance.horarios.each { a ->
			mapHorarios.put(a.diaSemana, a.horario)
		}

        [agendaInstance: agendaInstance, mapHorarios: mapHorarios]
    }

    def save() {
		
		def agendaInstance = new Agenda(params)
        if (!agendaInstance.save(flush: true)) {
            render(view: "create", model: [agendaInstance: agendaInstance])
            return
        }
		
		configuraHorario(agendaInstance)
		configuraFilial(agendaInstance)

		flash.message = message(code: 'default.created.message', args: ['Agenda'])
        redirect(action: "create")
    }

    def update() {
        def agendaInstance = Agenda.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (agendaInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Agenda'])
                render(view: "edit", model: [agendaInstance: agendaInstance])
                return
            }
        }

        agendaInstance.properties = params

        if (!agendaInstance.save(flush: true)) {
            render(view: "edit", model: [agendaInstance: agendaInstance])
            return
        }
		
		configuraHorario(agendaInstance)
		configuraFilial(agendaInstance)

		flash.message = message(code: 'default.updated.message', args: ['Agenda'])
        redirect(action: "list")
    }
	
	def configuraHorario(Agenda agendaInstance) {
		
		if (agendaInstance != null && agendaInstance.id != null) {
			AgendaHorario.removeAll(agendaInstance)
		}
		
		params.each { p ->
			if (p.key.toString().startsWith("grupoHorario-")) {
				def partes = p.key.toString().split("-")
				def diaSemana = partes[1].toInteger()
				def grupoId = p.value
				if (grupoId) {
					AgendaHorario ah = new AgendaHorario()
					ah.agenda = agendaInstance
					ah.horario = GrupoHorario.get(grupoId.toLong())
					ah.diaSemana = diaSemana
					ah.save()
				}
			}
		}
	}
	
	def configuraFilial(Agenda agendaInstance) {
	
		if (agendaInstance != null && agendaInstance.id != null) {
			AgendaFilial.removeAll(agendaInstance)
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def filiaisArray = isArray ? params.filial : [params.filial]
		
		filiaisArray.eachWithIndex { f, i ->
			if (f != null) {
				AgendaFilial af = new AgendaFilial()
				af.filial = Filial.get(filiaisArray[i].toLong())
				af.agenda = agendaInstance
				af.save()
			}
		}
		
	}
	
}