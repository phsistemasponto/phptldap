package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional


@Transactional
class CargoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def cargoList = new ArrayList<Cargo>()
		int cargoCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			
			order = params.order ? params.order : "id"
			sortType = params.sortType ? params.sortType : "desc"
			def orderBy = "order by " + order + " " + sortType
			
			if (tipoFiltro > 0) {
				
				switch (tipoFiltro) {
					case 1:
						cargoList.addAll(
							Cargo.findAll(
								"from Cargo c where c.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong(), max: params.max, offset: params.offset]))
						cargoCount = Cargo.findAll(
								"from Cargo c where c.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong()]).size()
						break;
					case 2:
						cargoList.addAll(
							Cargo.findAll(
								"from Cargo c where upper(c.dscCargo) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						cargoCount = Cargo.findAll(
								"from Cargo c where upper(c.dscCargo) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					default:
						break;
				}
			} else {
				cargoList.addAll(
					Cargo.findAll(
						"from Cargo c " + orderBy,
						[max: params.max, offset: params.offset]))
				cargoCount = Cargo.findAll(
						"from Cargo c " + orderBy).size()
			}
		}
		
		[cargoInstanceList: cargoList, cargoInstanceTotal: cargoCount, order: order, sortType: sortType]
    }

    def delete() {
        def cargoInstance = Cargo.get(params.id)
        if (!cargoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Cargo', cargoInstance.id])
        } else {
            try {
                cargoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Cargo'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Cargo'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [cargoInstance: new Cargo()]
    }

    def edit() {
        def cargoInstance = Cargo.get(params.id)
        if (!cargoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Cargo', cargoInstance.id])
            redirect(action: "list")
            return
        }

        [cargoInstance: cargoInstance]
    }

    def save() {
        def cargoInstance = new Cargo(params)

		cargoInstance.dscCargo= params.dscCargo.toUpperCase()
		
		if (params.idfolgaincrementada){
			cargoInstance.idfolgaincrementada='S'
		 } else {
			cargoInstance.idfolgaincrementada='N'
		 }
		 
		 if (params.idpghrext){
			 cargoInstance.idpghrext='S'
		  } else {
			 cargoInstance.idpghrext='N'
		  }
	
        if (!cargoInstance.save(flush: true)) {
            render(view: "create", model: [cargoInstance: cargoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Cargo'])
        redirect(action: "create")
    }

    def update() {
        def cargoInstance = Cargo.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (cargoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Cargo'])
                render(view: "edit", model: [cargoInstance: cargoInstance])
                return
            }
        }

		
		
        cargoInstance.properties = params
		if (params.idfolgaincrementada){
			cargoInstance.idfolgaincrementada='S'
		 } else {
			cargoInstance.idfolgaincrementada='N'
		 }
		 
		 if (params.idpghrext){
			 cargoInstance.idpghrext='S'
		  } else {
			 cargoInstance.idpghrext='N'
		  }
	

        if (!cargoInstance.save(flush: true)) {
            render(view: "edit", model: [cargoInstance: cargoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Cargo'])
        redirect(action: "list")
    }
}