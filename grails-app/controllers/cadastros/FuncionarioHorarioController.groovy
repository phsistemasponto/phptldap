package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;


@Transactional
class FuncionarioHorarioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def delete() {
        def funcionarioHorarioInstance = FuncionarioHorario.get(params.id)
        if (!funcionarioHorarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FuncionarioHorario', funcionarioHorarioInstance.id])
        } else {
            try {
                funcionarioHorarioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['FuncionarioHorario'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['FuncionarioHorario'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [funcionarioHorarioInstance: new FuncionarioHorario()]
    }

    def edit() {
        def funcionarioHorarioInstance = FuncionarioHorario.get(params.id)
        if (!funcionarioHorarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FuncionarioHorario', funcionarioHorarioInstance.id])
            redirect(action: "list")
            return
        }

        [funcionarioHorarioInstance: funcionarioHorarioInstance]
    }

    def save() {
        def funcionarioHorarioInstance = new FuncionarioHorario(params)
        if (!funcionarioHorarioInstance.save(flush: true)) {
            render(view: "create", model: [funcionarioHorarioInstance: funcionarioHorarioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['FuncionarioHorario'])
        redirect(action: "create")
    }

    def update() {
        def funcionarioHorarioInstance = FuncionarioHorario.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (funcionarioHorarioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['FuncionarioHorario'])
                render(view: "edit", model: [funcionarioHorarioInstance: funcionarioHorarioInstance])
                return
            }
        }

        funcionarioHorarioInstance.properties = params

        if (!funcionarioHorarioInstance.save(flush: true)) {
            render(view: "edit", model: [funcionarioHorarioInstance: funcionarioHorarioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['FuncionarioHorario'])
        redirect(action: "list")
    }
}