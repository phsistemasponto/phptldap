package cadastros

import java.sql.Blob

import javax.sql.rowset.serial.SerialBlob

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile

import funcoes.Horarios


@Transactional
class EmpresaController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		[empresaInstanceList: Empresa.list(params), empresaInstanceTotal: Empresa.count()]
	}

	def delete() {
		def empresaInstance = Empresa.get(params.id)
		if (!empresaInstance) {
			flash.error = message(code: 'default.not.found.message', args: [
				'Empresa',
				empresaInstance.id
			])
		} else {
			try {
				empresaInstance.delete(flush: true)
				flash.message = message(code: 'default.deleted.message', args: ['Empresa'])
			} catch (DataIntegrityViolationException e) {
				flash.error = message(code: 'default.not.deleted.message', args: ['Empresa'])
			}
		}

		redirect(action: "list")
	}

	def create() {
		[empresaInstance: new Empresa(), possuiFoto: false]
	}

	def edit() {
		def empresaInstance = Empresa.get(params.id)
		if (!empresaInstance) {
			flash.error = message(code: 'default.not.found.message', args: [
				'Empresa',
				empresaInstance.id
			])
			redirect(action: "list")
			return
		}

		def possuiFoto = !EmpresaFoto.findAll ('from EmpresaFoto where empresa=:empresa',[empresa: empresaInstance]).isEmpty()

		[empresaInstance: empresaInstance, possuiFoto: possuiFoto]
	}

	@Transactional
	def save() {
		def empresaInstance = new Empresa(params)
		if (params.idDoc){
			empresaInstance.idDoc='S'
		} else {
			empresaInstance.idDoc='N'
		}
		
		if (params.idSal){
			empresaInstance.idSal='S'
		} else {
			empresaInstance.idSal='N'
		}

		if (params.chekAbonos){
			empresaInstance.chekAbonos='S'
		} else {
			empresaInstance.chekAbonos='N'
		}
		
		empresaInstance.cargaInterJor = Horarios.calculaMinutos(params.cargaInterJor)
		empresaInstance.cargaLimtSemanalNot = Horarios.calculaMinutos(params.cargaLimtSemanalNot)
		empresaInstance.cargaLimtMensal = Horarios.calculaMinutos(params.cargaLimtMensal)
		empresaInstance.cargaLimtSemanal = Horarios.calculaMinutos(params.cargaLimtSemanal)
		
		configuraFoto(empresaInstance, params)

		if (!empresaInstance.save(flush: true)) {
			render(view: "create", model: [empresaInstance: empresaInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: ['Empresa'])
		redirect(action: "create")
	}

	@Transactional
	def update() {
		def empresaInstance = Empresa.get(params.id)
		if (params.version) {
			def version = params.version.toLong()
			if (empresaInstance.version > version) {
				flash.error = message(code: 'default.optimistic.locking.failure', args: ['Empresa'])
				render(view: "edit", model: [empresaInstance: empresaInstance])
				return
			}
		}

		empresaInstance.properties = params
		if (params.idDoc){
			empresaInstance.idDoc='S'
		} else {
			empresaInstance.idDoc='N'
		}
		
		if (params.idSal){
			empresaInstance.idSal='S'
		} else {
			empresaInstance.idSal='N'
		}

		if (params.chekAbonos){
			empresaInstance.chekAbonos='S'
		} else {
			empresaInstance.chekAbonos='N'
		}
		
		empresaInstance.cargaInterJor = Horarios.calculaMinutos(params.cargaInterJor)
		empresaInstance.cargaLimtSemanalNot = Horarios.calculaMinutos(params.cargaLimtSemanalNot)
		empresaInstance.cargaLimtMensal = Horarios.calculaMinutos(params.cargaLimtMensal)
		empresaInstance.cargaLimtSemanal = Horarios.calculaMinutos(params.cargaLimtSemanal)

		configuraFoto(empresaInstance, params)

		if (!empresaInstance.save(flush: true)) {
			render(view: "edit", model: [empresaInstance: empresaInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: ['Empresa'])
		redirect(action: "list")
	}
	
	@Transactional
	private configuraFoto(Empresa empresaInstance, Map params) {
		
		if (params.mudouFoto.toBoolean()) {
			
			if (empresaInstance.id != null) {
				def fotoEmpresa = EmpresaFoto.findAll ('from EmpresaFoto where empresa=:empresa',[empresa: empresaInstance])
				fotoEmpresa.each {
					EmpresaFoto empresa = EmpresaFoto.get(it.id)
					empresaInstance.foto = null
					empresa.delete()
				}
			}
						
			def foto = params.files
	
			def inputStream
	
			if (foto instanceof CommonsMultipartFile) {
				inputStream = foto.getInputStream()
			}
	
			if (inputStream != null) {
				byte[] bytes = inputStream.bytes
	
				if (bytes.size() > 0) {
					EmpresaFoto ff = new EmpresaFoto()
					ff.empresa = empresaInstance
					ff.conteudo = new SerialBlob(bytes)
		
					empresaInstance.foto = new HashSet<EmpresaFoto>()
					empresaInstance.foto.add(ff)
				}
			}
		}
	}

	@Transactional
	def image() {
		def empresa = Empresa.get( params.id )
		Set<EmpresaFoto> foto = empresa.foto
		if (foto != null && !foto.isEmpty()) {
			EmpresaFoto empresaFoto
			foto.each { empresaFoto = it }
			Blob conteudo = empresaFoto.conteudo
			byte[] image = conteudo.getBytes(1l, conteudo.length().toInteger())
			response.outputStream << image
		} else {
			def baseFolder = grailsAttributes.getApplicationContext().getResource("/").getFile().toString()
			def imagesPath = baseFolder + '\\images\\ph.gif'
			File file = new File(imagesPath)
			FileInputStream fis = new FileInputStream(file)

			byte[] image = new byte[fis.available()]
			fis.read(image)

			response.outputStream << image
		}
	}
}