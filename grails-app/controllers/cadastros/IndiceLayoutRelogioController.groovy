package cadastros

import org.springframework.dao.DataIntegrityViolationException




class IndiceLayoutRelogioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [indiceLayoutRelogioInstanceList: IndiceLayoutRelogio.list(params), indiceLayoutRelogioInstanceTotal: IndiceLayoutRelogio.count()]
    }

    def delete() {
        def indiceLayoutRelogioInstance = IndiceLayoutRelogio.get(params.id)
        if (!indiceLayoutRelogioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['IndiceLayoutRelogio', indiceLayoutRelogioInstance.id])
        } else {
            try {
                indiceLayoutRelogioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['IndiceLayoutRelogio'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['IndiceLayoutRelogio'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [indiceLayoutRelogioInstance: new IndiceLayoutRelogio()]
    }

    def edit() {
        def indiceLayoutRelogioInstance = IndiceLayoutRelogio.get(params.id)
        if (!indiceLayoutRelogioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['IndiceLayoutRelogio', indiceLayoutRelogioInstance.id])
            redirect(action: "list")
            return
        }

        [indiceLayoutRelogioInstance: indiceLayoutRelogioInstance]
    }

    def save() {
        def indiceLayoutRelogioInstance = new IndiceLayoutRelogio(params)
        if (!indiceLayoutRelogioInstance.save(flush: true)) {
            render(view: "create", model: [indiceLayoutRelogioInstance: indiceLayoutRelogioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['IndiceLayoutRelogio'])
        redirect(action: "create")
    }

    def update() {
        def indiceLayoutRelogioInstance = IndiceLayoutRelogio.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (indiceLayoutRelogioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['IndiceLayoutRelogio'])
                render(view: "edit", model: [indiceLayoutRelogioInstance: indiceLayoutRelogioInstance])
                return
            }
        }

        indiceLayoutRelogioInstance.properties = params

        if (!indiceLayoutRelogioInstance.save(flush: true)) {
            render(view: "edit", model: [indiceLayoutRelogioInstance: indiceLayoutRelogioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['IndiceLayoutRelogio'])
        redirect(action: "list")
    }
}