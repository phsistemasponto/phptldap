package cadastros

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional

@Transactional
class ItemhorarioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def dataSource

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [itemhorarioInstanceList: Itemhorario.list(params), itemhorarioInstanceTotal: Itemhorario.count()]
    }

    def delete() {
        def itemhorarioInstance = Itemhorario.get(params.id)
        if (!itemhorarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Itemhorario', itemhorarioInstance.id])
        } else {
            try {
                itemhorarioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Itemhorario'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Itemhorario'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [itemhorarioInstance: new Itemhorario()]
    }

    def edit() {
        def itemhorarioInstance = Itemhorario.get(params.id)
        if (!itemhorarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Itemhorario', itemhorarioInstance.id])
            redirect(action: "list")
            return
        }

        [itemhorarioInstance: itemhorarioInstance]
    }

    def save() {
        def itemhorarioInstance = new Itemhorario(params)
        if (!itemhorarioInstance.save(flush: true)) {
            render(view: "create", model: [itemhorarioInstance: itemhorarioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Itemhorario'])
        redirect(action: "create")
    }

    def update() {
        def itemhorarioInstance = Itemhorario.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (itemhorarioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Itemhorario'])
                render(view: "edit", model: [itemhorarioInstance: itemhorarioInstance])
                return
            }
        }

        itemhorarioInstance.properties = params

        if (!itemhorarioInstance.save(flush: true)) {
            render(view: "edit", model: [itemhorarioInstance: itemhorarioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Itemhorario'])
        redirect(action: "list")
    }
	
	def listaHorario() {
		
		Integer horario = params.horarioId.toInteger() 
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicioHorario)
		Date dataFim = dataInicio.plus(30) 
		Integer indice = params.indiceHorario

		def sqlHorarios = """
					select a.perfilhorario_id
					      ,b.dscperfil
					      ,a.qtdechefetiva
					      ,c.dsc_class
					      ,c.id_tp_dia
					      ,a.dtinivig
					      ,a.horario_id
					      ,a.seqitemhorario
					from itemhorario a
					left join perfilhorario b on a.perfilhorario_id = b.id
					left join classificacao c on a.classificacao_id = c.id
					where a.horario_id = ${horario}
					order by a.seqitemhorario
				  """

		Sql sql = new Sql(dataSource)
		def result = sql.rows(sqlHorarios)
		
		def r = []
		
//		result.each { row ->
//			def data = new SimpleDateFormat("dd/MM/yyyy").format(row.dtinivig)
//			def dia = new SimpleDateFormat("EEE").format(row.dtinivig)
//			def horarioDsc = row.dscperfil ? row.dscperfil : row.dsc_class
//			def idx = row.seqitemhorario
//			r.add([data: data, dia: dia, horario: horarioDsc, indice: idx])
//		}
		
		Integer countDias = 0
		Integer countIndice = 0
		Integer ultimoIndice = result[result.size()-1].seqitemhorario
		Date dtTemp = dataInicio
		while (dtTemp.before(dataFim) || dtTemp.equals(dataFim)) {
			
			countDias++
			countIndice++
			
			def data = new SimpleDateFormat("dd/MM/yyyy").format(dtTemp)
			def dia = new SimpleDateFormat("EEE").format(dtTemp)
			def horarioDsc = formataDscHorario(result, dtTemp)
			def idx = countIndice
			r.add([data: data, dia: dia, horario: horarioDsc, indice: idx])
			
			if (ultimoIndice == countIndice) {
				countIndice = 0
			}
			
			dtTemp = dtTemp.plus(1)
		}

		render r as JSON 
		
	}
	
	def formataDscHorario(arrayHorarios, data) {
		String retorno = ""
		Calendar calendarDateIteration = new GregorianCalendar();
		calendarDateIteration.setTime(data)
		arrayHorarios.each { t ->
			Calendar calendarDateArray = new GregorianCalendar();
			calendarDateArray.setTime(t.dtinivig)
			Integer diaSemanaArray = calendarDateArray.get(Calendar.DAY_OF_WEEK)
			Integer diaSemanaIteration = calendarDateIteration.get(Calendar.DAY_OF_WEEK)
			if (diaSemanaArray == diaSemanaIteration) {
				retorno = t.dscperfil ? t.dscperfil : t.dsc_class
			}
		}
		retorno
	}
}