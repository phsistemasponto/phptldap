package cadastros

import org.springframework.dao.DataIntegrityViolationException


class FolhaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [folhaInstanceList: Folha.list(params), folhaInstanceTotal: Folha.count()] 
    }

    def delete() {
        def folhaInstance = Folha.get(params.id)
        if (!folhaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Folha', folhaInstance.id])
        } else {
            try {
				
				def FolhaFilialList = FolhaFilial.findAllWhere(folha: folhaInstance)
				FolhaFilialList.each {
					it.delete()
				}
				
                folhaInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Folha'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Folha'])
            }
        }
		
        redirect(action: "list")
    }

    def create() {
		Folha folhaInstance = new Folha();
        [folhaInstance: folhaInstance, filiais: folhaInstance.getFolhaFiliais()]
    }

    def edit() {
        def folhaInstance = Folha.get(params.id)
        
		if (!folhaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Folha', folhaInstance.id])
            redirect(action: "list")
            return
        }
		
		List<EventosFolha> eventos = EventosFolha.findAll('from EventosFolha where folha=:folha order by sequencial',[folha: folhaInstance])

        [folhaInstance: folhaInstance, eventosFolha: eventos, filiais: folhaInstance.getFolhaFiliais()]
    }
	
	private configuraInstance(Folha folhaInstance) {
		
		for (x in 0..params.sequencialTemp.size()-2) {
			def seq = params.sequencialTemp[x].toInteger()
			def isArrayItem = [Collection, Object[]].any { it.isAssignableFrom(params.idOcorrencia.getClass()) }
			
			def idOcorrencia = isArrayItem ? params.idOcorrencia[x] : params.idOcorrencia
			def exportaDia = isArrayItem ? params.exportaDia[x] : params.exportaDia
			def exportaHora = isArrayItem ? params.exportaHora[x] : params.exportaHora
			def exportaHexa = isArrayItem ? params.exportaHexa[x] : params.exportaHexa
			def somaEvento = isArrayItem ? params.somaEvento[x] : params.somaEvento
			def eventoFolha = isArrayItem ? params.eventoFolha[x] : params.eventoFolha
			def idTipoEvento = isArrayItem ? params.idTipoEvento[x] : params.idTipoEvento
			
			def evtTmp = new EventosFolha()
			
			evtTmp.folha = folhaInstance
			evtTmp.ocorrencias = Ocorrencias.get(idOcorrencia.toLong())
			evtTmp.sequencial = seq.toInteger()
			evtTmp.dscEvento = eventoFolha
			evtTmp.tipoEvento = idTipoEvento
			evtTmp.expHoras = exportaHora.substring(0, 1)
			evtTmp.expDias = exportaDia.substring(0, 1)
			evtTmp.sumEvento = somaEvento.substring(0, 1)
			evtTmp.expHorasHex = exportaHexa.substring(0, 1)
			evtTmp.tipoEvento = idTipoEvento
			
			if (folhaInstance.eventos == null) {
				folhaInstance.eventos = new ArrayList()
			}
			
			folhaInstance.eventos.add(evtTmp)
			
		}
		
	}
	
	def incluirFolhaFilial(params,folhaInstance) {
		
		def isArrayFilial = [Collection, Object[]].any { it.isAssignableFrom(params.filialId.getClass()) }
		def filialArray = isArrayFilial ? params.filialId : [params.filialId]
		
		filialArray.eachWithIndex { it, i ->
			
			if (params.temFilialPeriodo[i] == "true" || params.temFilialPeriodo[i] == "t") {
				
				Filial filial = Filial.get(it)
				FolhaFilial folhaFilial = new FolhaFilial( filial: filial, folha: folhaInstance)
				
				if (!folhaFilial.save()){
					println('erros!')
					folhaFilial.errors.each { item ->
						println(item)
					}
				} else {
					println('salvo!!')
				}
			}
		}
		
	}

    def save() {
        def folhaInstance = new Folha(params)
		
		configuraInstance(folhaInstance)
		
        if (!folhaInstance.save(flush: true)) {
            render(view: "create", model: [folhaInstance: folhaInstance])
            return
        }
		
		incluirFolhaFilial(params,folhaInstance)

		flash.message = message(code: 'default.created.message', args: ['Folha'])
        redirect(action: "list")
    }

    def update() {
        def folhaInstance = Folha.get(params.id)
		
		if (params.version) {
            def version = params.version.toLong()
            if (folhaInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Folha'])
                render(view: "edit", model: [folhaInstance: folhaInstance])
                return
            }
        }
		
		folhaInstance.eventos.clear()
		def eventos = EventosFolha.findAll('from EventosFolha where folha=:folha',[folha: folhaInstance])
		eventos.each() {
			it.folha = null
			it.delete(flush: true)
		}

		folhaInstance.properties = params
		
		configuraInstance(folhaInstance)

        if (!folhaInstance.save(flush: true)) {
            render(view: "edit", model: [folhaInstance: folhaInstance])
            return
        }
		
		FolhaFilial folhaFilial = new FolhaFilial()
		folhaFilial.atualizarFolhaFilial(params, folhaInstance.id)

		flash.message = message(code: 'default.updated.message', args: ['Folha'])
        redirect(action: "list")
    }
}