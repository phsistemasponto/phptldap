package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;

import grails.converters.JSON 
import java.util.Date

@Transactional
class FuncionarioHoraExtraController {
	def springSecurityService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def listaPorFuncionario() {
		Funcionario funcionario = Funcionario.get(params.funcionarioId) 
		def lista = FuncionarioHoraExtra.findAll('from FuncionarioHoraExtra fhe where fhe.funcionario=:func',[func: funcionario])
		def retorno =[]
		def item = []
		if (lista){
			lista.each() {
				item = [id: "$it.configHoraExtra.id", descricao: "$it.configHoraExtra.dscConfigHe", dtIniVig: "$it.dataInicioFormatada" , dtFimVig: "$it.dataFimFormatada" ]
				retorno.add(item)
			}  
		} 
		render retorno as JSON
	}

    def delete() {
		def user = springSecurityService.currentUser
		Empresa empresa=Empresa.get(user.usuarioBanco.listEmpresas.toLong())
		Filial filial = Filial.findByEmpresaAndSeqFil(empresa,params.filialId)
		Funcionario funcionario = Funcionario.get(params.funcionarioId)
		ConfiguracaoHoraExtra configHoraExtra = ConfiguracaoHoraExtra.get(params.configHoraExtra)
		Date dtIniVig
		
		try {
			if (params.dtIniVig){
				dtIniVig = new Date().parse("dd/MM/yyyy", params.dtIniVig)
			}
		} catch (Exception e) {
		}
		def funcionarioHoraExtraInstance = FuncionarioHoraExtra.find('from FuncionarioHoraExtra f where f.filial=:filial and f.funcionario=:funcionario and f.configHoraExtra=:configHoraExtra and f.dtIniVig=:data',[filial: filial, funcionario: funcionario, configHoraExtra: configHoraExtra, data: dtIniVig ])

        if (!funcionarioHoraExtraInstance) {
            render message(code: 'default.not.found.message', args: ['FuncionarioHoraExtra', funcionarioHoraExtraInstance.id])
        } else {
            try {
                funcionarioHoraExtraInstance.delete(flush: true)
                render message(code: 'default.deleted.message', args: ['FuncionarioHoraExtra'])
            } catch (DataIntegrityViolationException e) {
                render message(code: 'default.not.deleted.message', args: ['FuncionarioHoraExtra'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [funcionarioHoraExtraInstance: new FuncionarioHoraExtra(), funcionarioId: params.funcionarioId, filialId: params.filialId, acao: 'create']
    }

    def edit() {
		def user = springSecurityService.currentUser
		Empresa empresa=Empresa.get(user.usuarioBanco.listEmpresas.toLong())				
		Filial filial = Filial.findByEmpresaAndSeqFil(empresa,params.filialId)
		Funcionario funcionario = Funcionario.get(params.funcionarioId)
		ConfiguracaoHoraExtra configHoraExtra = ConfiguracaoHoraExtra.get(params.configHoraExtra)
		Date dtIniVig
		
		try {
			if (params.dataInicio){
				dtIniVig = new Date().parse("dd/MM/yyyy", params.dataInicio)
			}
		} catch (Exception e) {
		}
        def funcionarioHoraExtraInstance = FuncionarioHoraExtra.find('from FuncionarioHoraExtra f where f.filial=:filial and f.funcionario=:funcionario and f.configHoraExtra=:configHoraExtra and f.dtIniVig=:data',[filial: filial, funcionario: funcionario, configHoraExtra: configHoraExtra, data: dtIniVig ])
        if (!funcionarioHoraExtraInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FuncionarioHoraExtra', funcionarioHoraExtraInstance.id])
            return
        }
        [funcionarioHoraExtraInstance: funcionarioHoraExtraInstance, acao: "edit", funcionarioId: params.funcionarioId, filialId: params.filialId]
    }

    def save() {
        def funcionarioHoraExtraInstance = new FuncionarioHoraExtra()
		try {
			if (params.dtIniVig){
				funcionarioHoraExtraInstance.dtIniVig = new Date().parse("dd/MM/yyyy", params.dtIniVig)
			}
		} catch (Exception e) {
		}
		try {
			if (params.dtFimVig){
				funcionarioHoraExtraInstance.dtFimVig = new Date().parse("dd/MM/yyyy", params.dtFimVig)
			}
		} catch (Exception e) {
		}


		def user = springSecurityService.currentUser
		Empresa empresa=Empresa.get(user.usuarioBanco.listEmpresas.toLong())				
		Filial filial = Filial.findByEmpresaAndSeqFil(empresa,params.filialId)
		Funcionario funcionario = Funcionario.get(params.funcionarioId) 
		ConfiguracaoHoraExtra configHoraExtra = ConfiguracaoHoraExtra.get(params.configHoraExtra)
		funcionarioHoraExtraInstance.filial = filial
		funcionarioHoraExtraInstance.funcionario = funcionario
		funcionarioHoraExtraInstance.configHoraExtra = configHoraExtra

        if (!funcionarioHoraExtraInstance.save(flush: true)) {
            render funcionarioHoraExtraInstance.errors
        } else {
			render message(code: 'default.created.message', args: ['FuncionarioHoraExtra'])
		}


    }

	// TODO - Corrigir a alteração de algumas linhas //
	
    def update() {

		def user = springSecurityService.currentUser
		Empresa empresa=Empresa.get(user.usuarioBanco.listEmpresas.toLong())				
		Filial filial = Filial.findByEmpresaAndSeqFil(empresa,params.filialId)
		Funcionario funcionario = Funcionario.get(params.funcionarioId) 
		ConfiguracaoHoraExtra configHoraExtra = ConfiguracaoHoraExtra.get(params.configHoraExtra)
		Date dtIniVig
		Date dtFimVig
		try {
			if (params.dtIniVig){
				dtIniVig = new Date().parse("dd/MM/yyyy", params.dtIniVig)
			}
		} catch (Exception e) {
		}	
		try {
			if (params.dtFimVig){
				dtFimVig = new Date().parse("dd/MM/yyyy", params.dtFimVig)
			}
		} catch (Exception e) {
		}	
		
		
        def funcionarioHoraExtraInstance = FuncionarioHoraExtra.find('from FuncionarioHoraExtra f where f.filial=:filial and f.funcionario=:funcionario and f.configHoraExtra=:configHoraExtra and f.dtIniVig=:data',[filial: filial, funcionario: funcionario, configHoraExtra: configHoraExtra, data: dtIniVig ])
		if (dtFimVig!=null) {
			funcionarioHoraExtraInstance.dtFimVig=dtFimVig
		}
		

        if (!funcionarioHoraExtraInstance.save(flush: true)) {
            render funcionarioHoraExtraInstance.errors
        } else {
			render message(code: 'default.updated.message', args: ['FuncionarioHoraExtra'])
		}
    }
}