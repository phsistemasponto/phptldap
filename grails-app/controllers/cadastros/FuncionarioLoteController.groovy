package cadastros

import java.math.*
import java.text.*

import org.springframework.transaction.annotation.Transactional

import funcoes.Horarios


@Transactional
class FuncionarioLoteController {
	
	def filtroPadraoService
	
	def index() {
		redirect(action: "list", params: params)
	}
	
	def list() {
		def funcs = new ArrayList<Funcionario>()
		
		if (params.btnFiltro) {
			def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)
			def orderby = " order by f.id "
			filtroPadrao += orderby
			funcs.addAll(Funcionario.findAll(filtroPadrao))
		}
		
		session.setAttribute("funcionariosAlteracaoLote", null)
		
		[funcionarioInstanceList: funcs, funcionarioInstanceTotal: funcs.size()]
	}
	
	def selecionaTipoAlteracao() {
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.funcionarioSelecionado.getClass()) }
		def arrayFunc = isArray ? params.funcionarioSelecionado : [params.funcionarioSelecionado]
		session.setAttribute("funcionariosAlteracaoLote", arrayFunc) 
		
	}
	
	def resumoAlteracao() {
		def funcIds = session.getAttribute("funcionariosAlteracaoLote").toString()
		def query = "from Funcionario where id in (" + funcIds.replace("[", "").replace("]", "") + ")"
		def funcionarios = Funcionario.findAll(query)
		[funcionarios: funcionarios]
	}
	
	def concluir() {
		def funcIds = session.getAttribute("funcionariosAlteracaoLote").toString()
		def query = "from Funcionario where id in (" + funcIds.replace("[", "").replace("]", "") + ")"
		def funcionarios = Funcionario.findAll(query)
		
		if (params.campoAlteracao.equals("filial")) {
			alteraFilial(funcionarios)
		} else if (params.campoAlteracao.equals("departamento")) {
			alteraDepartamento(funcionarios)
		} else if (params.campoAlteracao.equals("cargo")) {
			alteraCargo(funcionarios)
		} else if (params.campoAlteracao.equals("horario")) {
			alteraHorario(funcionarios)
		} else if (params.campoAlteracao.equals("dadosFunc")) {
			alteraDadosFunc(funcionarios)
		} else if (params.campoAlteracao.equals("salario")) {
			alteraSalario(funcionarios)
		} else if (params.campoAlteracao.equals("dataCadastral")) {
			alteraDataCadastral(funcionarios)
		} else if (params.campoAlteracao.equals("tipoFuncionario")) {
			alteraTipoFuncionario(funcionarios)
		} else if (params.campoAlteracao.equals("horasExtras")) {
			alteraHorasExtras(funcionarios)
		} else if (params.campoAlteracao.equals("beneficio")) {
			alteraBeneficios(funcionarios)
		} else if (params.campoAlteracao.equals("adicionalNoturno")) {
			alteraAdicionalNoturno(funcionarios)
		}
		
		render(view: "list", model: [funcionarioInstanceList: new ArrayList<Funcionario>(), funcionarioInstanceTotal: 0])
	}
	
	def alteraFilial(funcionarios) {
		def sucesso = 0
		def erro = 0
		funcionarios.each {
			Filial filial = Filial.get(params.filialId.toLong())
			Date novaDataInicio = new Date().parse("dd/MM/yyyy", params.filialDataInicio) 
			FuncionarioFilial ffAtivo = it.getFuncionarioFilialAtivo()
			
			if (ffAtivo == null) {
				FuncionarioFilial ffNovo = new FuncionarioFilial()
				ffNovo.funcionario = it
				ffNovo.filial = filial
				ffNovo.dtIniVig = novaDataInicio
				
				it.filiais.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else if (novaDataInicio.after(ffAtivo.dtIniVig)) {
				
				it.filial = filial
				ffAtivo.dtFimVig = novaDataInicio 
				
				FuncionarioFilial ffNovo = new FuncionarioFilial()
				ffNovo.funcionario = it
				ffNovo.filial = filial
				ffNovo.dtIniVig = novaDataInicio
				
				it.filiais.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else {
				erro++
			}
		}
		
		if (sucesso > 0) flash.message = sucesso + " funcion&aacute;rio(s) alterado(s) com sucesso"
		if (erro > 0) flash.error = erro + " funcion&aacute;rio(s) n&atilde;o foi(ram) alterado(s) por inconsist&ecirc;ncia na data in&iacute;cio"
	}
	
	def alteraDepartamento(funcionarios) {
		def sucesso = 0
		def erro = 0
		funcionarios.each {
			Setor setor = Setor.get(params.setorId)
			Date novaDataInicio = new Date().parse("dd/MM/yyyy", params.setorDataInicio)
			FuncionarioSetor ffAtivo = it.getFuncionarioSetorAtivo()
			
			if (ffAtivo == null) {
				
				FuncionarioSetor ffNovo = new FuncionarioSetor()
				ffNovo.funcionario = it
				ffNovo.setor = setor
				ffNovo.dtIniVig = novaDataInicio
				
				it.setores.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else if (novaDataInicio.after(ffAtivo.dtIniVig)) {
				it.setor = setor
				ffAtivo.dtFimVig = novaDataInicio
				
				FuncionarioSetor ffNovo = new FuncionarioSetor()
				ffNovo.funcionario = it
				ffNovo.setor = setor
				ffNovo.dtIniVig = novaDataInicio
				
				it.setores.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else {
				erro++
			}
		}
		
		if (sucesso > 0) flash.message = sucesso + " funcion&aacute;rio(s) alterado(s) com sucesso"
		if (erro > 0) flash.error = erro + " funcion&aacute;rio(s) n&atilde;o foi(ram) alterado(s) por inconsist&ecirc;ncia na data in&iacute;cio"
	}
	
	def	alteraCargo(funcionarios) {
		def sucesso = 0
		def erro = 0
		funcionarios.each {
			Cargo cargo = Cargo.get(params.cargoId.toLong())
			Date novaDataInicio = new Date().parse("dd/MM/yyyy", params.cargoDataInicio)
			FuncionarioCargo ffAtivo = it.getFuncionarioCargoAtivo()
			
			if (ffAtivo == null) {
				
				FuncionarioCargo ffNovo = new FuncionarioCargo()
				ffNovo.funcionario = it
				ffNovo.cargo = cargo
				ffNovo.dtIniVig = novaDataInicio
				
				it.cargos.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else if (novaDataInicio.after(ffAtivo.dtIniVig)) {
				it.cargo = cargo
				ffAtivo.dtFimVig = novaDataInicio
				
				FuncionarioCargo ffNovo = new FuncionarioCargo()
				ffNovo.funcionario = it
				ffNovo.cargo = cargo
				ffNovo.dtIniVig = novaDataInicio
				
				it.cargos.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else {
				erro++
			}
		}
		if (sucesso > 0) flash.message = sucesso + " funcion&aacute;rio(s) alterado(s) com sucesso"
		if (erro > 0) flash.error = erro + " funcion&aacute;rio(s) n&atilde;o foi(ram) alterado(s) por inconsist&ecirc;ncia na data in&iacute;cio"
	}
	
	@Transactional
	def	alteraHorario(funcionarios) {
		def sucesso = 0
		def erro = 0
		funcionarios.each {
			Horario horario = Horario.get(params.horarioId)
			
			Date novaDataInicio = null
			if (params.dtAdmComoDtIniHorario != null && params.dtAdmComoDtIniHorario.equals("S")) {
				novaDataInicio = it.dtAdm
			} else {
				novaDataInicio = new Date().parse("dd/MM/yyyy", params.horarioDataInicio)
			} 
			FuncionarioHorario ffAtivo = it.getFuncionarioHorarioAtivo()
			
			if (ffAtivo == null) {
				
				FuncionarioHorario ffNovo = new FuncionarioHorario()
				ffNovo.funcionario = it
				ffNovo.horario = horario
				ffNovo.sequencia = 1
				ffNovo.nrIndHorario = params.horarioIndice
				ffNovo.dtIniVig = novaDataInicio
				
				it.horarios.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else if (novaDataInicio.after(ffAtivo.dtIniVig)) {
				it.horario = horario
				ffAtivo.dtFimVig = novaDataInicio
				
				FuncionarioHorario ffNovo = new FuncionarioHorario()
				ffNovo.funcionario = it
				ffNovo.horario = horario
				ffNovo.nrIndHorario = params.horarioIndice
				ffNovo.dtIniVig = novaDataInicio
				
				it.horarios.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else {
				erro++
			}
		}
		if (sucesso > 0) flash.message = sucesso + " funcion&aacute;rio(s) alterado(s) com sucesso"
		if (erro > 0) flash.error = erro + " funcion&aacute;rio(s) n&atilde;o foi(ram) alterado(s) por inconsist&ecirc;ncia na data in&iacute;cio"
	}
	
	def alteraDadosFunc(funcionarios) {
		funcionarios.each {
			
			println "#####"
			println "params.obrigIntervalo: " 		+ params.obrigIntervalo
			println "params.intervFlex: " 			+ params.intervFlex
			println "params.sensivelFeriado: " 		+ params.sensivelFeriado
			println "params.rrt: " 					+ params.rrt
			println "params.calcAdicionalNoturno: " + params.calcAdicionalNoturno
			println "params.dsrRemunerado: " 		+ params.dsrRemunerado
			println "#####"
			
			it.idObrigIntervalo = params.obrigIntervalo
			it.idSensivelFeriado = params.sensivelFeriado
			it.idIntervFlex = params.intervFlex
			it.idRrt = params.rrt
			it.idAdcNot = params.calcAdicionalNoturno
			
			it.dsrRemunerado = params.dsrRemunerado ? Horarios.calculaMinutos(params.dsrRemunerado) : 0
			
			it.save(flush: true)
			
			
		}
		flash.message = "Funcion&aacute;rio(s) alterado(s) com sucesso"
	}
	
	def	alteraSalario(funcionarios) {
		funcionarios.each {
			it.vrSal = Double.valueOf(params.inputSalario.toString().replace('.','').replace(',','.'))
			it.save(flush: true)
		}
		flash.message = "Funcion&aacute;rio(s) alterado(s) com sucesso"
	}
	
	def	alteraDataCadastral(funcionarios) {
		
		Date novaDataAdmissao = new Date().parse("dd/MM/yyyy", params.inputDataAdmissao)
		Date novaDataRescisao = params.inputDataRescisao ? new Date().parse("dd/MM/yyyy", params.inputDataRescisao) : null
		
		funcionarios.each {
			it.dtAdm = novaDataAdmissao
			it.dtResc = novaDataRescisao
			it.situacaoFunc = (novaDataRescisao!=null) ? Situacaofunc.findByIdstfunc("D") : Situacaofunc.findByIdstfunc("A")
			it.save(flush: true)
		}
		
		flash.message = "Funcion&aacute;rio(s) alterado(s) com sucesso"
	}
	
	def	alteraTipoFuncionario(funcionarios) {
		funcionarios.each {
			Tipofunc tipoFunc = Tipofunc.get(params.tipoFuncionarioId)
			it.tipoFunc = tipoFunc
			it.save(flush: true)
		}
		flash.message = "Funcion&aacute;rio(s) alterado(s) com sucesso"
	}
	
	def	alteraHorasExtras(funcionarios) {
		def sucesso = 0
		def erro = 0
		funcionarios.each {
			ConfiguracaoHoraExtra horaExtra = ConfiguracaoHoraExtra.get(params.configHoraExtraId)
			Date novaDataInicio = null
			if (params.dtAdmComoDtIniConfigExtra != null && params.dtAdmComoDtIniConfigExtra.equals("S")) {
				novaDataInicio = it.dtAdm
			} else {
				novaDataInicio = new Date().parse("dd/MM/yyyy", params.configHoraExtraDataInicio)
			}
			
			
			FuncionarioHoraExtra ffAtivo = it.getFuncionarioHoraExtraAtivo()
			
			if (ffAtivo == null) {
				
				FuncionarioHoraExtra ffNovo = new FuncionarioHoraExtra()
				ffNovo.funcionario = it
				ffNovo.sequencia = 1
				ffNovo.configHoraExtra = horaExtra
				ffNovo.dtIniVig = novaDataInicio
				
				it.horasExtras.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else if (novaDataInicio.after(ffAtivo.dtIniVig)) {
				it.configuracaoHoraExtra = horaExtra
				ffAtivo.dtFimVig = novaDataInicio
				
				FuncionarioHoraExtra ffNovo = new FuncionarioHoraExtra()
				ffNovo.funcionario = it
				ffNovo.configHoraExtra = horaExtra
				ffNovo.dtIniVig = novaDataInicio
				
				it.horasExtras.add(ffNovo)
				it.save(flush: true)
				sucesso++
			} else {
				erro++
			}
		}
		if (sucesso > 0) flash.message = sucesso + " funcion&aacute;rio(s) alterado(s) com sucesso"
		if (erro > 0) flash.error = erro + " funcion&aacute;rio(s) n&atilde;o foi(ram) alterado(s) por inconsist&ecirc;ncia na data in&iacute;cio"
	}
	
	def	alteraBeneficios(funcionarios) {
		def sucesso = 0
		def erro = 0
		
		println "####" + params
		
		funcionarios.each {
			 
			Beneficio beneficio = Beneficio.get(params.beneficioId)
			Date novaDataInicio = new Date().parse("dd/MM/yyyy", params.beneficioDataInicio) 
			Integer quantidade = params.beneficioQuantidade.toInteger()
			FuncionarioBeneficio ffAtivo = it.getFuncionarioBeneficioAtivo()
			
			if (ffAtivo != null && !ffAtivo.beneficio.id.equals(beneficio.id)) {
				ffAtivo = null
			}
			
			if (ffAtivo == null) {
				
				FuncionarioBeneficio ffNovo = new FuncionarioBeneficio()
				ffNovo.funcionario = it
				ffNovo.sequencia = 1
				ffNovo.beneficio = beneficio
				ffNovo.dtIniVig = novaDataInicio
				ffNovo.quantidade = quantidade
				ffNovo.save(flush: true)
				
				it.beneficios.add(ffNovo)
				it.save(flush: true)
				sucesso++
				
			} else if (novaDataInicio.after(ffAtivo.dtIniVig)) {
				ffAtivo.dtFimVig = novaDataInicio
				
				FuncionarioBeneficio ffNovo = new FuncionarioBeneficio()
				ffNovo.funcionario = it
				ffNovo.beneficio = beneficio
				ffNovo.dtIniVig = novaDataInicio
				ffNovo.quantidade = quantidade
				
				it.beneficios.add(ffNovo)
				it.save(flush: true)
				sucesso++
			} else {
				erro++
			}
		}
		if (sucesso > 0) flash.message = sucesso + " funcion&aacute;rio(s) alterado(s) com sucesso"
		if (erro > 0) flash.error = erro + " funcion&aacute;rio(s) n&atilde;o foi(ram) alterado(s) por inconsist&ecirc;ncia na data in&iacute;cio"
	}
	
	def	alteraAdicionalNoturno(funcionarios) {
		
	}
	
}