package cadastros

import org.springframework.dao.DataIntegrityViolationException

class FeriadosRegionaisController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [feriadosRegionaisInstanceList: FeriadosRegionais.list(params), feriadosRegionaisInstanceTotal: FeriadosRegionais.count()]
    }

    def delete() {
        def feriadosRegionaisInstance = FeriadosRegionais.get(params.id)
        if (!feriadosRegionaisInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FeriadosRegionais', feriadosRegionaisInstance.id])
        } else {
            try {
                feriadosRegionaisInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['FeriadosRegionais'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['FeriadosRegionais'])
            }
        }

        redirect(action: "list")
    }

    def create() {
		def feriadosRegionaisInstance = new FeriadosRegionais()
        [feriadosRegionaisInstance: feriadosRegionaisInstance, filiais: feriadosRegionaisInstance.getFeriadosRegionaisFiliais()]
    }

    def edit() {
        def feriadosRegionaisInstance = FeriadosRegionais.get(params.id)
        if (!feriadosRegionaisInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FeriadosRegionais', feriadosRegionaisInstance.id])
            redirect(action: "list")
            return
        }

        [feriadosRegionaisInstance: feriadosRegionaisInstance, filiais: feriadosRegionaisInstance.getFeriadosRegionaisFiliais()]
    }

    def save() {
        def feriadosRegionaisInstance = new FeriadosRegionais(params)
		
		feriadosRegionaisInstance.dtFer = new Date().parse("dd/MM/yyyy", params.dataFeriado)
		
		if (params.idParcial) {
			feriadosRegionaisInstance.idParcial = 'S'
		} else {
			feriadosRegionaisInstance.idParcial = 'N'
		}
		
        if (!feriadosRegionaisInstance.save()) {
            render(view: "create", model: [feriadosRegionaisInstance: feriadosRegionaisInstance])
            return
        }
		
		incluirFeriadoRegionalFilial(params, feriadosRegionaisInstance)

		flash.message = message(code: 'default.created.message', args: ['FeriadosRegionais'])
        redirect(action: "create")
    }

    def update() {
        def feriadosRegionaisInstance = FeriadosRegionais.get(params.id)
		
		if (params.version) {
            def version = params.version.toLong()
            if (feriadosRegionaisInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['FeriadosRegionais'])
                render(view: "edit", model: [feriadosRegionaisInstance: feriadosRegionaisInstance])
                return
            }
        }

        feriadosRegionaisInstance.properties = params
		
		feriadosRegionaisInstance.dtFer = new Date().parse("dd/MM/yyyy", params.dataFeriado)
		
		if (params.idParcial) {
			feriadosRegionaisInstance.idParcial = 'S'
		} else {
			feriadosRegionaisInstance.idParcial = 'N'
		}

        if (!feriadosRegionaisInstance.save()) {
            render(view: "edit", model: [feriadosRegionaisInstance: feriadosRegionaisInstance])
            return
        }
		
		atualizarFeriadoRegionalFilial(params, feriadosRegionaisInstance)

		flash.message = message(code: 'default.updated.message', args: ['FeriadosRegionais'])
        redirect(action: "list")
    }
	
	def incluirFeriadoRegionalFilial(params, feriadoRegionalParametro){
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialId.getClass()) }
		def filiaisArray = isArray ? params.filialId : [params.filialId]
		
		FeriadosRegionaisFilial feriadoRegionalFilial
		Filial filial
		filiaisArray.eachWithIndex { it, i ->
			
			def temFeriado = isArray ? params.temFilialFeriadoRegional[i] : params.temFilialFeriadoRegional
			
			if (temFeriado == 'true') {
				filial = Filial.get(it)
				feriadoRegionalFilial = new FeriadosRegionaisFilial( feriadosRegionais: feriadoRegionalParametro, filial: filial, status: '0', version: 0)
				if (!feriadoRegionalFilial.save()){
					feriadoRegionalFilial.errors.each { item ->
						println(item)
					}
				}
			}
		}
		
	}
	
	def atualizarFeriadoRegionalFilial(params, feriadoRegionalParametro){
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialId.getClass()) }
		def filiaisArray = isArray ? params.filialId : [params.filialId]
		
		def feriadoRegionalFilial
		Filial filial
		filiaisArray.eachWithIndex { it, i ->
			
			filial = Filial.get(it)
			feriadoRegionalFilial = FeriadosRegionaisFilial.find("from FeriadosRegionaisFilial where feriadosRegionais = :feriado and filial = :filial", 
					[feriado: feriadoRegionalParametro, filial: filial])

			def temFeriado = isArray ? params.temFilialFeriadoRegional[i] : params.temFilialFeriadoRegional
			
			if (temFeriado=='true') {
				
				if (feriadoRegionalFilial){
					// nada
				} else {
					feriadoRegionalFilial = new FeriadosRegionaisFilial( feriadosRegionais: feriadoRegionalParametro, filial: filial, status: '0', version: 0)
				}
				
				if (!feriadoRegionalFilial.save()){
					feriadoRegionalFilial.errors.each { item ->
						println(item)
					}
				}

			} else {
				if (feriadoRegionalFilial){
					feriadoRegionalFilial.delete()
				}
			}
		}
		
	}
}