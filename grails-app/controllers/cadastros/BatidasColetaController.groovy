package cadastros

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile


class BatidasColetaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def dataSource
	def detectDataBaseService

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
		
		def usuario = session['usuario']
		def filiais = usuario.getFiliais()
		def relogios = Relogio.findAll("from Relogio r where r.filial in (:filiais) order by r.dscRel", [filiais: filiais])
        
		[batidasColetaInstanceList: BatidasColeta.list(params), 
			batidasColetaInstanceTotal: BatidasColeta.count(), 
			filiais: filiais,
			relogios: relogios]
    }
	
	@Transactional
	def uploadImage () {
        def fileName
        def inputStream
        if (params.qqfile instanceof CommonsMultipartFile) {
            fileName = params.qqfile?.originalFilename
            inputStream = params.qqfile.getInputStream()
        } else {
            fileName = params.qqfile
            inputStream = request.getInputStream()
        }
        //To avoid problems with spaces
        fileName = fileName.replaceAll(" ", "_")
 
        def bytes = inputStream.bytes
		def quantidadeLinhas = new String(bytes).count("\n")+1
		
		BatidasColeta batida = new BatidasColeta()
		batida.creation = new Date()
		batida.relogio = Relogio.get(params.relogioId.toLong())
		batida.usuario = session['usuario']
		
		ArquivoBatidaColeta arquivo = new ArquivoBatidaColeta()
		arquivo.conteudo = new String(bytes)
		arquivo.batidasColeta = batida
		
		batida.save()
		arquivo.save()

		render(status: response.SC_OK, text:"{success: true, quantidadeLinhas: "  + quantidadeLinhas + "}")
    }
	
	def processar() {
		
		def logs = []
		
		if (params.importarIndividual) {
			
			println "### " + params.funcionarioId.toLong()
			
			Funcionario func = Funcionario.findByCrcFunc(params.funcionarioId.toLong())
			Date dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicioFuncionario)
			Date dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFimFuncionario)
			
			chamaProcedureProcessarFuncionario(func, dtInicio, dtFim)
			
			def resultsLogs = Log.findAll("from Log l where l.funcionario = :f and l.dtBat between :dtini and :dtfim order by l.dtBat, l.funcionario.nomFunc ",
				[f: func, dtini: dtInicio, dtfim: dtFim])
			
			resultsLogs.each {
				
				logs.add([dtBat: new SimpleDateFormat("dd/MM/yyyy").format(it.dtBat),
						  dtLog: new SimpleDateFormat("dd/MM/yyyy").format(it.dtLog),
						  funcionario: it.funcionario.nomFunc,
						  mensagem: it.mensagem.descricao])
				
			}
			
		} else {
			
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialId.getClass()) }
			def filiaisArray = isArray ? params.filialId : [params.filialId]
			def datasInicioArray = isArray ? params.dtInicioFilial : [params.dtInicioFilial]
			def datasFimArray = isArray ? params.dtFimFilial : [params.dtFimFilial]
			
			filiaisArray.eachWithIndex { f, i ->
				
				def paramFilialMarcada = "temFilial" + f.toLong()
				def marcada = params[paramFilialMarcada]
				if (marcada != null) {
					Filial filial = Filial.get(f.toLong())
					Date dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(datasInicioArray[i])
					Date dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(datasFimArray[i])
					
					chamaProcedureProcessarFilial(filial, dtInicio, dtFim)
					
					def resultsLogs = Log.findAll("from Log l where l.funcionario.filial = :f and l.dtBat between :dtini and :dtfim order by l.dtBat, l.funcionario.nomFunc ",
						[f: filial, dtini: dtInicio, dtfim: dtFim])
					
					resultsLogs.each {
						
						logs.add([dtBat: new SimpleDateFormat("dd/MM/yyyy").format(it.dtBat),
								  dtLog: new SimpleDateFormat("dd/MM/yyyy").format(it.dtLog),
								  funcionario: it.funcionario.nomFunc,
								  mensagem: it.mensagem.descricao])
						
					}
				}
			}
		
		}
		
		render logs as JSON
	}
	
	def chamaProcedureProcessarFuncionario(funcionario, dataInicio, dataFim) {
		
		def seqFunc = funcionario.id

		def template = new JdbcTemplate(dataSource)

		if (detectDataBaseService.isPostgreSql()) {
			def dtInicioFormatada = new SimpleDateFormat("yyyy-MM-dd").format(dataInicio)
			def dtFimFormatada = new SimpleDateFormat("yyyy-MM-dd").format(dataFim)
			def chamada = "select pr_processar(-1::integer, -1::integer, ${seqFunc}::integer, '${dtInicioFormatada}'::date, '${dtFimFormatada}'::date);"
			def valor = template.queryForObject(chamada, Integer.class)
		} else if (detectDataBaseService.isOracle()) {
			def dtIni = new SimpleDateFormat("dd/MM/yyyy").format(dataInicio)
			def dtFim = new SimpleDateFormat("dd/MM/yyyy").format(dataFim)
			Sql sql = new Sql(dataSource)
			def chamada = "call pkg_phponto.pr_processar(-1, -1, ${seqFunc}, '${dtIni}', '${dtFim}')"
			sql.call(chamada)
		}
	}
	
	def chamaProcedureProcessarFilial(filial, dataInicio, dataFim) {
		
		def seqEmpresa = filial.empresa.id
		def seqFilial = filial.id

		def template = new JdbcTemplate(dataSource)

		if (detectDataBaseService.isPostgreSql()) {
			def dtInicioFormatada = new SimpleDateFormat("yyyy-MM-dd").format(dataInicio)
			def dtFimFormatada = new SimpleDateFormat("yyyy-MM-dd").format(dataFim)
			def chamada = "select pr_processar(${seqEmpresa}::integer, ${seqFilial}::integer, -1::integer, '${dtInicioFormatada}'::date, '${dtFimFormatada}'::date);"
			def valor = template.queryForObject(chamada, Integer.class)
		} else if (detectDataBaseService.isOracle()) {
			def dtIni = new SimpleDateFormat("dd/MM/yyyy").format(dataInicio)
			def dtFim = new SimpleDateFormat("dd/MM/yyyy").format(dataFim)
			Sql sql = new Sql(dataSource)
			def chamada = "call pkg_phponto.pr_processar(${seqEmpresa}, ${seqFilial}, -1, '${dtIni}', '${dtFim}')"
			sql.call(chamada)
		}
	}
	
	def salvarLog() {
		response.setHeader("Content-disposition", "attachment; filename=resultLog.csv");
		render(contentType: "text/csv", text: params.conteudoArquivo);
	}

    def delete() {
        def batidasColetaInstance = BatidasColeta.get(params.id)
        if (!batidasColetaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['BatidasColeta', batidasColetaInstance.id])
        } else {
            try {
                batidasColetaInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['BatidasColeta'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['BatidasColeta'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [batidasColetaInstance: new BatidasColeta()]
    }

    def edit() {
        def batidasColetaInstance = BatidasColeta.get(params.id)
        if (!batidasColetaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['BatidasColeta', batidasColetaInstance.id])
            redirect(action: "list")
            return
        }

        [batidasColetaInstance: batidasColetaInstance]
    }

    def save() {
        def batidasColetaInstance = new BatidasColeta(params)
        if (!batidasColetaInstance.save(flush: true)) {
            render(view: "create", model: [batidasColetaInstance: batidasColetaInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['BatidasColeta'])
        redirect(action: "create")
    }

    def update() {
        def batidasColetaInstance = BatidasColeta.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (batidasColetaInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['BatidasColeta'])
                render(view: "edit", model: [batidasColetaInstance: batidasColetaInstance])
                return
            }
        }

        batidasColetaInstance.properties = params

        if (!batidasColetaInstance.save(flush: true)) {
            render(view: "edit", model: [batidasColetaInstance: batidasColetaInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['BatidasColeta'])
        redirect(action: "list")
    }
}