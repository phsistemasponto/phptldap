package cadastros

import funcoes.Horarios


class AgendamentoProcessoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        [ agendamentos: AgendamentoProcesso.findAll() ]
    }
	
	def save() {
		
		println params
		
		def agendamentosFiliais = AgendamentoProcessoFilial.findAll()
		agendamentosFiliais.each { a -> a.delete(flush: true) }
		def agendamentos = AgendamentoProcesso.findAll()
		agendamentos.each { a -> a.delete(flush: true) }
		
		params.each { p ->
			if (p.key.toString().startsWith("diaSemanaId-")) {
				def identificador = p.key.toString().split("-")
				def indice = identificador[1]
				def diaSemana = p.value
				def horario = params.get("horaExecucaoId-" + indice)
				
				AgendamentoProcesso agenda = new AgendamentoProcesso()
				agenda.diaSemana = diaSemana.toInteger()
				agenda.horaExecucao = Horarios.calculaMinutos(horario)
				agenda.save(flush: true)
				
				def filiais = params.get("filialId-" + indice)
				def isArrayFiliais = [Collection, Object[]].any { it.isAssignableFrom(filiais.getClass()) }
				def filiaisArray = isArrayFiliais ? filiais : [filiais]
				filiaisArray.each { f ->
					AgendamentoProcessoFilial agendaFilial = new AgendamentoProcessoFilial()
					agendaFilial.agendamentoProcesso = agenda
					agendaFilial.filial = Filial.get(f.toLong())
					agendaFilial.save(flush: true)
					println agendaFilial.errors
				}
			}
		}
		
		flash.message = "Salvo com sucesso"
		redirect(action: "index", params: params, model: [ agendamentos: AgendamentoProcesso.findAll() ])
		
	}

    
}