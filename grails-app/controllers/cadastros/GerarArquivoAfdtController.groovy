package cadastros

import java.math.*
import java.text.*

import javax.servlet.ServletOutputStream

import funcoes.Horarios
import groovy.sql.Sql


class GerarArquivoAfdtController {
	
	def filtroPadraoService
	def dataSource
	
	def index() {
		
		if (params.dataInicio && params.dataFim) {
			
			def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
			
			def sql = """
					SELECT fil.dsc_fil
						,fil.cnpj_fil
						,fil.dsc_log 
						,fil.cmp_log
						,fil.nm_log
						,b.seq_func
						,f.mtr_func
						,f.crc_func
						,f.nom_func
						,f.dt_adm
						,f.pis_func
						,s.dsc_setor
						,c.dsc_cargo
						,b.dt_bat
						,b.hr_bat
						,b.seq_tip_bat
						,b.id_bloqueado
						,b.id_bat
						,d.hr_bat1
						,d.hr_bat2
						,d.hr_bat3
						,d.hr_bat4
						,d.hr_bat5 
						,d.hr_bat6
						,b1510.motivo_cartao_id
						,b1510.status
					FROM batidas b
					INNER JOIN funcionario f on b.seq_func = f.id
					INNER JOIN filial fil on f.filial_id = fil.id
					INNER JOIN setor s on f.setor_id = s.id
					INNER JOIN cargo c on f.cargo_id = c.id
					INNER JOIN dados d on d.seq_func = b.seq_func 
						             and d.dt_bat = b.dt_bat
					LEFT JOIN batidas1510 b1510 on b1510.seq_func = b.seq_func 
					                            and b1510.dt_bat = b.dt_bat 
					                            and b1510.hr_bat = b.hr_bat
					WHERE (1 = 1) 
				  """
			def where  = filtroPadraoService.montaFiltroPadraoSqlNativo(params)
				where += " and b.dt_bat between '" + params.dataInicio + "' and '" + params.dataFim + "' " 
			def order  = " ORDER BY b.seq_func, b.dt_bat, b.hr_bat "
			
			sql = sql.concat(where).concat(order)
			
			Sql consulta = new Sql(dataSource)
			def results = consulta.rows(sql)
			
			def fileTxt = ""
			def sequencialArquivo = 1
			
			// print header
			
			fileTxt += preencheZeros(sequencialArquivo.toString(), 9)  
			fileTxt += "1" // tipo registro
			fileTxt += "1" // tipo empregador (CNPJ)
			fileTxt += preencheZeros(somenteNumero(results[0].cnpj_fil.toString()), 14)
			fileTxt += preencheEspacos("", 12) // CEI do empregador
			fileTxt += preencheEspacos(results[0].dsc_fil, 150)
			fileTxt += new SimpleDateFormat("ddMMyyyy").format(dtInicio)
			fileTxt += new SimpleDateFormat("ddMMyyyy").format(dtFim)
			fileTxt += new SimpleDateFormat("ddMMyyyy").format(new Date())
			fileTxt += new SimpleDateFormat("HHmm").format(new Date())
			fileTxt += "\r\n"
			
			def funcIteracao = ""
			def dtIteracao = null
			def linhaEntrada = 1
			def linhaSaida = 1
			
			results.each { r ->
				
				// print detail
				
				sequencialArquivo++ 
				
				if (!funcIteracao.equals(r.seq_func) || (dtIteracao == null || !dtIteracao.equals(r.dt_bat))) {
					// se trocou funcionario, reseta registros de entrada e saida
					linhaEntrada = 1
					linhaSaida = 1
				}
				
				def lineDetail = ""
				
				lineDetail += preencheZeros(sequencialArquivo.toString(), 9)
				lineDetail += "2" // tipo registro
				lineDetail += new SimpleDateFormat("ddMMyyyy").format(r.dt_bat)
				lineDetail += Horarios.retornaHora(r.hr_bat).replace(":", "")
				lineDetail += preencheZeros(somenteNumero(r.pis_func.toString()), 12)
				lineDetail += preencheEspacos("", 17)
				lineDetail += r.id_bat
				
				if (r.id_bat.equals("E")) {
					lineDetail += preencheZeros(linhaEntrada.toString(), 2)
					linhaEntrada++
				} else {
					lineDetail += preencheZeros(linhaSaida.toString(), 2)
					linhaSaida++
				}
				
				lineDetail += retornaStatus(r.motivo_cartao_id.toString())
				lineDetail += preencheEspacos(r.status.toString(), 100)
				lineDetail += "\r\n"
				
				funcIteracao = r.seq_func
				dtIteracao = r.dt_bat
				fileTxt += lineDetail
			}
			
			// print trail 
			
			sequencialArquivo++
			
			fileTxt += preencheZeros(sequencialArquivo.toString(), 9)
			fileTxt += "9" // tipo registro
			fileTxt += "\r\n"

			String idFile = new SimpleDateFormat("ddMMyyyyHHmmsss").format(new Date())
			session.setAttribute(idFile, fileTxt)			
			
			render(status: response.SC_OK, text: idFile)
			
		}
		
	}
	
	def baixarArquivo() {
		String arquivo = session.getAttribute(params.baixarArquivoId)
		session.removeAttribute(params.baixarArquivoId)
		
		//get the output stream from the common property 'response'
		ServletOutputStream servletOutputStream = response.outputStream
 
		//set the byte content on the response. This determines what is shown in your browser window.
		response.setContentType("text/plain")
		response.setContentLength(arquivo.length())
	 
		response.setHeader('Content-disposition', 'attachment; filename='.concat("AFDT").concat(".txt"))
		response.setHeader('Expires', '0');
		response.setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
		servletOutputStream << arquivo.getBytes()
		servletOutputStream.flush()
	}
	
	public String somenteNumero(String valor) {
		String retorno = "";
		for (int i = 0; i < valor.length(); i++) {
			if (valor.charAt(i).isDigit()) {
				retorno += valor.charAt(i)
			}
		}
		return retorno;
	}
	
	public String preencheEspacos(String txt, Integer quantidade) {
		String retorno = ""; 
		int length = 0
		if (txt != null && !txt.isEmpty() && !txt.equals("null")) {
			retorno = retorno.concat(txt)
			length = retorno.length() 
		} 
		for (int i = 0; i < quantidade - length; i++) {
			retorno = retorno.concat(" ")
		}
		return retorno;
	}
	
	public String preencheZeros(String txt, Integer quantidade) {
		String retorno = "";
		int length = 0
		if (txt != null && !txt.isEmpty() && !txt.equals("null")) {
			retorno = retorno.concat(txt)
			length = retorno.length()
		}
		for (int i = 0; i < quantidade - length; i++) {
			retorno = "0".concat(retorno)
		}
		return retorno;
	}
	
	public String retornaStatus(String str) {
		if (str == null || str.isEmpty() || str.equals("null")) { 
			return "O"
		} else {
			return str
		}
	}
	
}