package cadastros

import grails.converters.JSON
import groovy.sql.Sql

import java.math.*
import java.text.*

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional


@Transactional

class PeriodoController {
	
	def periodoService
	def filtroPadraoService
	def detectDataBaseService
	def dataSource

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def periodoList = new ArrayList<Periodo>()
		int periodoCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			
			order = params.order ? params.order : "id"
			sortType = params.sortType ? params.sortType : "desc"
			def orderBy = "order by " + order + " " + sortType
			
			if (tipoFiltro > 0) {
				
				switch (tipoFiltro) {
					case 1:
						periodoList.addAll(
							Periodo.findAll(
								"from Periodo p where p.dscPr like :desc " + orderBy,
								[desc: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						periodoCount = Periodo.findAll(
								"from Periodo p where p.dscPr like :desc " + orderBy,
								[desc: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					case 2:
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
						periodoList.addAll(
							Periodo.findAll(
								"from Periodo p where :data between p.dtIniPr and p.dtFimPr " + orderBy,
								[data: sdf.parse(params.filtroData), max: params.max, offset: params.offset]))
						periodoCount = Periodo.findAll(
								"from Periodo p where :data between p.dtIniPr and p.dtFimPr " + orderBy,
								[data: sdf.parse(params.filtroData)]).size()
						break;
					default:
						break;
				}
			} else {
				periodoList.addAll(
					Periodo.findAll(
						"from Periodo p " + orderBy, [max: params.max, offset: params.offset]))
				periodoCount = Periodo.findAll(
						"from Periodo p " + orderBy).size()
			}
		}
		
		[periodoInstanceList: periodoList, periodoInstanceTotal: periodoCount, order: order, sortType: sortType]
    }

    def delete() {
        def periodoInstance = Periodo.get(params.id)
        if (!periodoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Periodo', periodoInstance.id])
        } else {
            try {
                periodoInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Periodo'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Periodo'])
            }
        }

        redirect(action: "list")
    }

    def create() {
		Periodo periodoInstance = new Periodo()
        [periodoInstance: periodoInstance, filiais: periodoInstance.getPeriodoFiliais()]
    }

    def edit() {
        def periodoInstance = Periodo.get(params.id)
        if (!periodoInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Periodo', periodoInstance.id])
            redirect(action: "list")
            return
        }

        [periodoInstance: periodoInstance, filiais: periodoInstance.getPeriodoFiliais()]
    }

    def save() {
        def periodoInstance = new Periodo()
	
					
		periodoInstance.dscPr= params.dscPr.toUpperCase()
		periodoInstance.apelPr= params.apelPr.toUpperCase()
						
		if (params.idStsPr){
			periodoInstance.idStsPr='S'
	    } else {
			periodoInstance.idStsPr='N'
	    }
		
		try {
			periodoInstance.dtIniPr= new SimpleDateFormat("dd/MM/yyyy").parse(params.dtIniPr)
		} catch (Exception e) {
		  	println (e)
		}
		
		try {
			periodoInstance.dtFimPr= new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFimPr)
		} catch (Exception e) {
		  	println (e)
		}
		
		
        if (!periodoInstance.save(flush: true)) {
            render(view: "create", model: [periodoInstance: periodoInstance])
            return
        }

		periodoService.incluirPeriodoFilial(params, periodoInstance)
		
		flash.message = message(code: 'default.created.message', args: ['Periodo'])
        redirect(action: "create")
    }

    def update() {
        def periodoInstance = Periodo.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (periodoInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Periodo'])
                render(view: "edit", model: [periodoInstance: periodoInstance])
                return
            }
        }

		periodoInstance.dscPr= params.dscPr
		periodoInstance.apelPr= params.apelPr
						
		if (params.idStsPr){
			periodoInstance.idStsPr='S'
	    } else {
			periodoInstance.idStsPr='N'
	    }
		
		try {
			periodoInstance.dtIniPr= new SimpleDateFormat("dd/MM/yyyy").parse(params.dtIniPr)
		} catch (Exception e) {
		  	println (e)
		}
		
		try {
			periodoInstance.dtFimPr= new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFimPr)
		} catch (Exception e) {
		  	println (e)
		}

        if (!periodoInstance.save(flush: true)) {
            render(view: "edit", model: [periodoInstance: periodoInstance])
            return
        }

		periodoService.AtualizarPeriodoFilial(params, periodoInstance)
		
		flash.message = message(code: 'default.updated.message', args: ['Periodo'])
        redirect(action: "list")
    }
	
	def consultaPorAno() {
		def r = []
		
		if (params.ano) {
			Integer ano = params.ano.toInteger()   
			
			def sqlPeriodos = ""
			def filial = (params.filial) ? Filial.get(params.filial.toLong()) : null
			def status = params.status
			
			if (detectDataBaseService.isPostgreSql()) {
				sqlPeriodos = """
								 select p.id as id, p.apel_pr as descricao  
								 from periodo p 
								 inner join periodo_filial pf on pf.periodo_id = p.id
								 inner join parametro_periodo_filial ppf on ppf.filial_id = pf.filial_id
								 inner join parametro_periodo pp on ppf.parametro_periodo_id = pp.id and pp.dia_inicio = date_part('day', p.dt_ini_pr)
								 where (1=1) 
	                          """
				sqlPeriodos += " and pf.filial_id in (" + filtroPadraoService.montaFiltroFiliaisDoUsuario() + ")"
				sqlPeriodos += " and date_part('year', p.dt_ini_pr) = " + ano
				if (filial != null) {
					sqlPeriodos += " and pf.filial_id = " + filial.id 
				}
				if (status) {
					sqlPeriodos += " and pf.status = '" + status + "' "
				}
				sqlPeriodos += " order by p.dt_ini_pr "
			} else if (detectDataBaseService.isOracle()) {
				sqlPeriodos = """
									 select p.id as id, p.apel_pr as descricao  
									 from periodo p 
									 inner join periodo_filial pf on pf.periodo_id = p.id
									 inner join parametro_periodo_filial ppf on ppf.filial_id = pf.filial_id
									 inner join parametro_periodo pp on ppf.parametro_periodo_id = pp.id and pp.dia_inicio = to_char(p.dt_ini_pr, 'dd')
									 where (1=1) 
		                          """
				sqlPeriodos += " and pf.filial_id in (" + filtroPadraoService.montaFiltroFiliaisDoUsuario() + ")"
				sqlPeriodos += " and to_char(p.dt_ini_pr, 'yyyy') = " + ano
				if (filial != null) {
					sqlPeriodos += " and pf.filial_id = " + filial.id
				}
				if (status) {
					sqlPeriodos += " and pf.status = '" + status + "' " 
				}
				sqlPeriodos += " order by p.dt_ini_pr "
			}
			
			
			Sql sql = new Sql(dataSource)
			def result = sql.rows(sqlPeriodos)
			
			
			if (detectDataBaseService.isPostgreSql()) {
				r.addAll(result)
			} else if (detectDataBaseService.isOracle()) {
				result.each { i ->
					r.add([id: i.ID, descricao: i.DESCRICAO])
				}
			}
			
		}
		
		render r as JSON
	}
	
	def consultaDatas() {
		Periodo periodo = Periodo.get(params.periodoId)
		def result = []
		if (periodo) {
			result = [id: periodo.id,
				dataInicio: new SimpleDateFormat("dd/MM/yyyy").format(periodo.dtIniPr),
				dataFim: new SimpleDateFormat("dd/MM/yyyy").format(periodo.dtFimPr)]
		}
		render result as JSON
	}
	
	def consultaUltimosMeses() {
		def meses = params.meses.toInteger()
		
		def result = []
		def periodos
		
		def sqlPeriodos = """
							 select p.id as id, p.apel_pr as \"apelPr\" 
							 from periodo p 
							 inner join periodo_filial pf on pf.periodo_id = p.id
							 inner join parametro_periodo_filial ppf on ppf.filial_id = pf.filial_id
							 inner join parametro_periodo pp on ppf.parametro_periodo_id = pp.id and pp.dia_inicio = date_part('day', p.dt_ini_pr)
							 where (1=1) 
                          """
		if (params.filial) {
			sqlPeriodos += " and pf.filial_id = " + params.filial
		} else {
			sqlPeriodos += " and pf.filial_id in (" + filtroPadraoService.montaFiltroFiliaisDoUsuario() + ")"
		}
		
		sqlPeriodos += " order by p.dt_ini_pr desc "
		sqlPeriodos += " limit " + meses
		
		Sql sql = new Sql(dataSource)
		periodos = sql.rows(sqlPeriodos)

		periodos.each {
			result.add([id: it.id, nome: it.apelPr])
		}
		
		render result as JSON
	}
}