package cadastros

import groovy.sql.Sql

import java.math.*
import java.text.*

import org.springframework.jdbc.core.JdbcTemplate


class FechamentoController {
	
	def filtroPadraoService
	def springSecurityService
	def dataSource
	def detectDataBaseService
	
	def index() {
		redirect(action: "list", params: params)
	}
	
	def list() {
		
		def order = params.order ? params.order : "f.id"
		def sortType = params.sortType ? params.sortType : "desc"
		
		def usuario = springSecurityService.currentUser
		def filiais = usuario.getFiliais()
		
		def periodos = new ArrayList<Periodo>()
		PeriodoFilial.findAll("from PeriodoFilial where filial in (:filiais) order by periodo.dtIniPr asc", [filiais: filiais]).each {
			if (!periodos.contains(it.periodo)) {
				periodos.add(it.periodo)
			}
		}
		def periodoSelecionado = null
		
		def funcs = new ArrayList<Funcionario>()
		
		if (params.periodo) {
			
			if (params.tipoFechamento) {
				
				periodoSelecionado = Periodo.get(params.periodo.toLong())
				
				if (params.tipoFechamento.equals("funcionario")) {
					
					def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)
					def dtIniPeriodo = periodoSelecionado.dtIniPr
					def dtFimPeriodo = periodoSelecionado.dtFimPr
					def clausePeriodo = " and ( "
						.concat(" (f.dtResc is null and f.dtAdm <= :fim) ")
						.concat(" or (f.dtResc is not null and (f.dtAdm >= :inicio or f.dtResc <= :fim) ) )")
					filtroPadrao += clausePeriodo
					
					def orderBy = "order by " + order + " " + sortType
					filtroPadrao += orderBy
					
					funcs.addAll(Funcionario.findAll(filtroPadrao, [inicio: dtIniPeriodo, fim: dtFimPeriodo]))
				}
				
			} else {
				flash.message = "Selecione um tipo de fechamento"
			}
			
		} else {
			flash.message = "Selecione um per&iacute;odo"
		}
		
		
		[funcionarioInstanceList: funcs, 
		 funcionarioInstanceTotal: funcs.size(),
		 filialInstanceList: filiais,
		 filialInstanceTotal: filiais.size(), 
		 periodos: periodos,
		 periodoSelecionado: periodoSelecionado,
		 order: order, 
		 sortType: sortType]
	}
	
	def concluirFilial() {
		try {
			chamarProcedureFechamento()
			criaPeriodoAutomatico()
		} catch (Exception e) {
			render(status: response.SC_INTERNAL_SERVER_ERROR)
		}
		render(status: response.SC_OK)
	}
	
	def concluirFuncionario() {
		try {
			chamarProcedureFechamento()
		} catch (Exception e) {
			render(status: response.SC_INTERNAL_SERVER_ERROR)
		}
		render(status: response.SC_OK)
	}
	
	def desfazerFechamentoFilial() {
		try {
			chamarProcedureDesfazerFechamento()
		} catch (Exception e) {
			render(status: response.SC_INTERNAL_SERVER_ERROR)
		}
		render(status: response.SC_OK)
	}
	
	def desfazerFechamentoFuncionario() {
		try {
			chamarProcedureDesfazerFechamento()
		} catch (Exception e) {
			render(status: response.SC_INTERNAL_SERVER_ERROR)
		}
		render(status: response.SC_OK)
	}
	
	def chamarProcedureFechamento() {
		def template = new JdbcTemplate(dataSource)
		
		def seqUsuario = springSecurityService.currentUser.id
		
		Periodo periodo = Periodo.get(params.periodo.toLong())
		
		def periodoFilial = null
		def seqPeriodo = null
		def dtInicio = null
		def dtFim = null
		def seqFilial = null
		def seqFuncionario = null
		
		if (params.tipoFechamento.equals("filial")) {
			
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialSelecionada.getClass()) }
			def filiaisArray = isArray ? params.filialSelecionada : [params.filialSelecionada]
			filiaisArray.each {
				Filial filial = Filial.get(it.toLong())
				PeriodoFilial pf = PeriodoFilial.findByPeriodoAndFilial(periodo, filial)
				
				seqPeriodo = pf.id
				dtInicio = pf.periodo.dtIniPr
				dtFim = pf.periodo.dtFimPr
				seqFilial = filial.id
				seqFuncionario = -1
		
				if (detectDataBaseService.isPostgreSql()) {
					template.queryForObject("select prc_processar_fechamentomes(${seqFilial}::integer, ${seqFuncionario}::integer, ${seqPeriodo}::integer, ${seqUsuario}::integer, '${dtInicio}'::date, '${dtFim}'::date)", Integer.class)
				} else if (detectDataBaseService.isOracle()) {
					Sql sql = new Sql(dataSource)
					def chamada = "call prc_processar_fechamentomes(${seqFilial}, ${seqFuncionario}, ${seqPeriodo}, ${seqUsuario}, '${dtInicio}', '${dtFim}')"
					sql.call(chamada)
				}
			}
			
		} else if (params.tipoFechamento.equals("funcionario")) {
		
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.funcionarioSelecionado.getClass()) }
			def funcionariosArray = isArray ? params.funcionarioSelecionado : [params.funcionarioSelecionado]
			funcionariosArray.each {
				Funcionario func = Funcionario.get(it.toLong())
				Filial filial = func.filial
				PeriodoFilial pf = PeriodoFilial.findByPeriodoAndFilial(periodo, filial)
				
				seqPeriodo = pf.id
				dtInicio = pf.periodo.dtIniPr
				dtFim = pf.periodo.dtFimPr
				seqFilial = filial.id
				seqFuncionario = func.id
		
				if (detectDataBaseService.isPostgreSql()) {
					template.queryForObject("select prc_processar_fechamentomes(${seqFilial}::integer, ${seqFuncionario}::integer, ${seqPeriodo}::integer, ${seqUsuario}::integer, '${dtInicio}'::date, '${dtFim}'::date)", Integer.class)
				} else if (detectDataBaseService.isOracle()) {
					Sql sql = new Sql(dataSource)
					def chamada = "call prc_processar_fechamentomes(${seqFilial}, ${seqFuncionario}, ${seqPeriodo}, ${seqUsuario}, '${dtInicio}', '${dtFim}')"
					sql.call(chamada)
				}
			}
		}
	}
	
	def chamarProcedureDesfazerFechamento() {
		
		def template = new JdbcTemplate(dataSource)
		
		def seqUsuario = springSecurityService.currentUser.id
		
		Periodo periodo = Periodo.get(params.periodo.toLong())
		
		def periodoFilial = null
		def seqPeriodo = null
		def dtInicio = null
		def dtFim = null
		def seqFilial = null
		def seqFuncionario = null
		
		if (params.tipoFechamento.equals("filial")) {
			
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialSelecionada.getClass()) }
			def filiaisArray = isArray ? params.filialSelecionada : [params.filialSelecionada]
			filiaisArray.each {
				Filial filial = Filial.get(it.toLong())
				PeriodoFilial pf = PeriodoFilial.findByPeriodoAndFilial(periodo, filial)
				
				seqPeriodo = pf.id
				dtInicio = pf.periodo.dtIniPr
				dtFim = pf.periodo.dtFimPr
				seqFilial = filial.id
				seqFuncionario = -1
		
				if (detectDataBaseService.isPostgreSql()) {
					template.queryForObject("select prc_desfazer_fechamento_mes(${seqFilial}::integer, ${seqFuncionario}::integer, ${seqPeriodo}::integer, ${seqUsuario}::integer, '${dtInicio}'::date, '${dtFim}'::date)", Integer.class)
				} else if (detectDataBaseService.isOracle()) {
					Sql sql = new Sql(dataSource)
					def chamada = "call prc_desfazer_fechamento_mes(${seqFilial}, ${seqFuncionario}, ${seqPeriodo}, ${seqUsuario}, '${dtInicio}', '${dtFim}')"
					sql.call(chamada)
				}
			}
			
		} else if (params.tipoFechamento.equals("funcionario")) {
		
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.funcionarioSelecionado.getClass()) }
			def funcionariosArray = isArray ? params.funcionarioSelecionado : [params.funcionarioSelecionado]
			funcionariosArray.each {
				Funcionario func = Funcionario.get(it.toLong())
				Filial filial = func.filial
				PeriodoFilial pf = PeriodoFilial.findByPeriodoAndFilial(periodo, filial)
				
				seqPeriodo = pf.id
				dtInicio = pf.periodo.dtIniPr
				dtFim = pf.periodo.dtFimPr
				seqFilial = filial.id
				seqFuncionario = func.id
		
				if (detectDataBaseService.isPostgreSql()) {
					template.queryForObject("select prc_desfazer_fechamento_mes(${seqFilial}::integer, ${seqFuncionario}::integer, ${seqPeriodo}::integer, ${seqUsuario}::integer, '${dtInicio}'::date, '${dtFim}'::date)", Integer.class)
				} else if (detectDataBaseService.isOracle()) {
					Sql sql = new Sql(dataSource)
					def chamada = "call prc_desfazer_fechamento_mes(${seqFilial}, ${seqFuncionario}, ${seqPeriodo}, ${seqUsuario}, '${dtInicio}', '${dtFim}')"
					sql.call(chamada)
				}
			}
		}
		
	}
	
	def criaPeriodoAutomatico() {
		
		if (params.tipoFechamento.equals("filial")) {
			
			def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filialSelecionada.getClass()) }
			def filiaisArray = isArray ? params.filialSelecionada : [params.filialSelecionada]
			filiaisArray.each {
				Filial filial = Filial.get(it.toLong())
				Periodo periodo = Periodo.get(params.periodo.toLong())
				ParametroPeriodoFilial parametroFilial = ParametroPeriodoFilial.findByFilial(filial) 
				if (parametroFilial) {
					
					ParametroPeriodo parametro = parametroFilial.parametroPeriodo
					
					Calendar calendar = new GregorianCalendar()
					calendar.setTime(periodo.dtIniPr)
					calendar.add(Calendar.MONTH, 1)
					
					Periodo periodoJaSalvo = Periodo.find("from Periodo where :data between dtIniPr and dtFimPr", [data: calendar.getTime()])
					
					if (periodoJaSalvo != null) {
						
						PeriodoFilial periodoFilialJaSalvo = PeriodoFilial.findByFilialAndPeriodo(filial, periodoJaSalvo)
						if (!periodoFilialJaSalvo) {
							
							PeriodoFilial pf = new PeriodoFilial()
							pf.periodo = periodoJaSalvo
							pf.filial = filial
							pf.save(flush: true)
							
						}
						
					} else {
						
						Periodo novoPeriodo = new Periodo()
						novoPeriodo.dscPr = new SimpleDateFormat(parametro.padraoNome).format(calendar.getTime()).toUpperCase()
						novoPeriodo.apelPr = new SimpleDateFormat(parametro.padraoNome).format(calendar.getTime()).toUpperCase()
						novoPeriodo.idStsPr = "N"
						
						calendar.set(Calendar.DAY_OF_MONTH, parametro.diaInicio)
						novoPeriodo.setDtIniPr(calendar.getTime())
						
						if (parametro.diaFimUltimoMes) {
							calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
						} else {
							calendar.set(Calendar.DAY_OF_MONTH, parametro.diaFim)
						}
						novoPeriodo.setDtFimPr(calendar.getTime())
						
						novoPeriodo.save(flush: true)
						
						PeriodoFilial pf = new PeriodoFilial()
						pf.periodo = novoPeriodo
						pf.filial = filial
						pf.status = "A"
						pf.save(flush: true)
					
					}
					
					
				}
			}
		}
	}
	
}