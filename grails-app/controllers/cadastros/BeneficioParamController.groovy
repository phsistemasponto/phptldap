package cadastros

import org.springframework.dao.DataIntegrityViolationException

import funcoes.Horarios

class BeneficioParamController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [beneficioParamInstanceList: BeneficioParam.list(params), beneficioParamInstanceTotal: BeneficioParam.count()]
    }

    def delete() {
        def beneficioParamInstance = BeneficioParam.get(params.id)
        if (!beneficioParamInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Beneficio Parametro', beneficioParamInstance.id])
        } else {
		
			def beneficiosOcorrencias = BeneficioParamOcorrencia.findAllByBeneficioParam(beneficioParamInstance)
			beneficiosOcorrencias.each { b ->
				b.delete(flush: true)
			}
			
			def beneficiosJustificativas = BeneficioParamJustificativa.findAllByBeneficioParam(beneficioParamInstance)
			beneficiosJustificativas.each { b ->
				b.delete(flush: true)
			}
			
			BeneficioParamFilial.findAll('from BeneficioParamFilial where beneficioParam = :b', [b: beneficioParamInstance]).each { pr ->
				pr.delete(flush: true)
			}
		
            try {
                beneficioParamInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Beneficio Parametro'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Beneficio Parametro'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [beneficioParamInstance: new BeneficioParam()]
    }

    def edit() {
        def beneficioParamInstance = BeneficioParam.get(params.id)
        if (!beneficioParamInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Beneficio Parametro', beneficioParamInstance.id])
            redirect(action: "list")
            return
        }

        [beneficioParamInstance: beneficioParamInstance]
    }

    def save() {
		
		def beneficioParamInstance = new BeneficioParam(params)
		
		configuraInstance(beneficioParamInstance)
		
        if (!beneficioParamInstance.save(flush: true)) {
            render(view: "create", model: [beneficioParamInstance: beneficioParamInstance])
            return
        }
		
		configuraJustificativaAbono(beneficioParamInstance)
		configuraOcorrencia(beneficioParamInstance)
		configuraFiliais(beneficioParamInstance)

		flash.message = message(code: 'default.created.message', args: ['Beneficio Parametro'])
        redirect(action: "create")
    }

    def update() {
        def beneficioParamInstance = BeneficioParam.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (beneficioParamInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Beneficio Parametro'])
                render(view: "edit", model: [beneficioParamInstance: beneficioParamInstance])
                return
            }
        }

        beneficioParamInstance.properties = params
		configuraInstance(beneficioParamInstance)
		
        if (!beneficioParamInstance.save(flush: true)) {
            render(view: "edit", model: [beneficioParamInstance: beneficioParamInstance])
            return
        }
		
		configuraJustificativaAbono(beneficioParamInstance)
		configuraOcorrencia(beneficioParamInstance)
		configuraFiliais(beneficioParamInstance)

		flash.message = message(code: 'default.updated.message', args: ['Beneficio Parametro'])
        redirect(action: "list")
    }
	
	def configuraInstance(BeneficioParam beneficioParamInstance) {
		
		if (params.qtExtras) {
			beneficioParamInstance.qtExtras = Horarios.calculaMinutos(params.qtExtras)
		}
		
		if (params.idBenDescontoDiario){
			beneficioParamInstance.idBenDescontoDiario='S'
		} else {
			beneficioParamInstance.idBenDescontoDiario='N'
		}
		
		if (params.idVerificaMesAnterior){
			beneficioParamInstance.idVerificaMesAnterior='S'
		} else {
			beneficioParamInstance.idVerificaMesAnterior='N'
		}
		
		if (params.idVerificaQtFaltas){
			beneficioParamInstance.idVerificaQtFaltas='S'
		} else {
			beneficioParamInstance.idVerificaQtFaltas='N'
		}
		
		if (params.idVerificaFaltasAbo){
			beneficioParamInstance.idVerificaFaltasAbo='S'
		} else {
			beneficioParamInstance.idVerificaFaltasAbo='N'
		}
		
		if (params.idVerificaExtras){
			beneficioParamInstance.idVerificaExtras='S'
		} else {
			beneficioParamInstance.idVerificaExtras='N'
		}
		
		if (params.idVerificaMesProcesso){
			beneficioParamInstance.idVerificaMesProcesso='S'
		} else {
			beneficioParamInstance.idVerificaMesProcesso='N'
		}
		
		if (params.idVerificaFeriado){
			beneficioParamInstance.idVerificaFeriado='S'
		} else {
			beneficioParamInstance.idVerificaFeriado='N'
		}
		
		if (params.idVerificaFolgaComp){
			beneficioParamInstance.idVerificaFolgaComp='S'
		} else {
			beneficioParamInstance.idVerificaFolgaComp='N'
		}
		
		if (params.idVerificaAfastamentos){
			beneficioParamInstance.idVerificaAfastamentos='S'
		} else {
			beneficioParamInstance.idVerificaAfastamentos='N'
		}
		
		beneficioParamInstance.verbaFolha = 'N'
	}
	
	def configuraOcorrencia(BeneficioParam beneficioParamInstance) {
		if (beneficioParamInstance.id != null) {
			def beneficios = BeneficioParamOcorrencia.findAllByBeneficioParam(beneficioParamInstance)
			beneficios.each { b ->
				b.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idOcorrencia.getClass()) }
		def arrayOcorrencia = isArray ? params.idOcorrencia : [params.idOcorrencia]
		def arrayQuantidadeOcorrencia = isArray ? params.qtdadeOcorrencia : [params.qtdadeOcorrencia]
		

		if (arrayOcorrencia != null) {
			arrayOcorrencia.eachWithIndex { j, i ->
				if (j != null) {
					BeneficioParamOcorrencia b = new BeneficioParamOcorrencia()
					b.ocorrencias = Ocorrencias.get(j.toLong())
					b.beneficioParam = beneficioParamInstance
					b.quantidade = arrayQuantidadeOcorrencia[i].toInteger()
					b.save()
					
					if (beneficioParamInstance.ocorrencias == null) {
						beneficioParamInstance.ocorrencias = new ArrayList<BeneficioParamOcorrencia>()
					}
					
					beneficioParamInstance.ocorrencias.add(b)
				}
			}
		}
		
	}
	
	def configuraJustificativaAbono(BeneficioParam beneficioParamInstance) {
		if (beneficioParamInstance.id != null) {
			def beneficios = BeneficioParamJustificativa.findAllByBeneficioParam(beneficioParamInstance)
			beneficios.each { b ->
				b.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idJustAbono.getClass()) }
		def arrayJustificativaAbono = isArray ? params.idJustAbono : [params.idJustAbono]
		def arrayQuantidadeAbono = isArray ? params.qtdadeJustAbono : [params.qtdadeJustAbono]
		

		if (arrayJustificativaAbono != null) {
			arrayJustificativaAbono.eachWithIndex { j, i ->
				if (j != null) {
					BeneficioParamJustificativa b = new BeneficioParamJustificativa()
					b.justificativas = Justificativas.get(j.toLong())
					b.beneficioParam = beneficioParamInstance
					b.quantidade = arrayQuantidadeAbono[i].toInteger()
					b.save()
					
					if (beneficioParamInstance.justificativasAbono == null) {
						beneficioParamInstance.justificativasAbono = new ArrayList<BeneficioParamJustificativa>()
					}
					
					beneficioParamInstance.justificativasAbono.add(b)
				}
			}
		}
		
	}
	
	def configuraFiliais(BeneficioParam beneficioParamInstance) {
		
		if (beneficioParamInstance.id != null) {
			BeneficioParamFilial.findAll('from BeneficioParamFilial where beneficioParam = :b', [b: beneficioParamInstance]).each { pr ->
				pr.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def filiaisArray = isArray ? params.filial : [params.filial]
		
		filiaisArray.eachWithIndex { f, i ->
			if (f != null) {
				BeneficioParamFilial bf = new BeneficioParamFilial()
				bf.filial = Filial.get(filiaisArray[i].toLong())
				bf.beneficioParam = beneficioParamInstance
				
				if (beneficioParamInstance.filiais == null) {
					beneficioParamInstance.filiais = new ArrayList<BeneficioParamFilial>()
				}
				
				beneficioParamInstance.filiais.add(bf)
			}
		}
	}
}