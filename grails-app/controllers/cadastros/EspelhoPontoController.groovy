package cadastros

import java.math.*
import java.text.*

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.transaction.annotation.Transactional

import acesso.Usuario
import funcoes.Horarios
import grails.converters.JSON
import groovy.sql.Sql


@Transactional
class EspelhoPontoController {

	def filtroPadraoService
	def abonoService
	def springSecurityService
	def dataSource
	def detectDataBaseService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		
		def order = params.order ? params.order : "f.id"
		def sortType = params.sortType ? params.sortType : "desc"

		def usuario = session['usuario']
		def filiais = usuario.getFiliais()

		def funcs = new ArrayList<Funcionario>()
		if (params.retornar) {
			funcs = session.getAttribute("funcs")
		}

		if (params.dataInicio && params.dataFim && (params.btnFiltro || params.order)) {

			def filtroPadrao = filtroPadraoService.montaFiltroPadrao(params)

			def dtIniPeriodo = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			def dtFimPeriodo = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
			
			def clausePeriodo  = " "
			
			if (params.filtroPadrao_NaoListaDemitidos.toBoolean()) {
				
				clausePeriodo = " and f.dtAdm <= :inicio and f.dtAdm <= :fim  "
				
			} else {
			
				clausePeriodo = " and ( "
					.concat(" (f.dtResc is null and f.dtAdm <= :fim) ")
					.concat(" or (f.dtResc is not null and (f.dtAdm >= :inicio or f.dtResc <= :fim) ) )")
								
			}
			filtroPadrao += clausePeriodo

			if (params.somenteComObservacao) {
				def clauseObservacao = " and exists ("
						.concat(" from ObservacaoFuncionario o where o.funcionario = f.id and o.dtBat between :inicio and :fim ) ")
				filtroPadrao += clauseObservacao
			}


			def orderBy = "order by " + order + " " + sortType
			filtroPadrao += orderBy

			funcs.addAll(Funcionario.findAll(filtroPadrao, [inicio: dtIniPeriodo, fim: dtFimPeriodo]))

			session.setAttribute("funcs", funcs)
		}


		[funcionarioInstanceList: funcs,
			funcionarioInstanceTotal: funcs.size(),
			order: order,
			sortType: sortType]
	}

	def exibir() {
		Funcionario funcionario = Funcionario.get(params.funcionarioId.toLong())
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)

		def dadosPrincipais = populaDadosPrincipais(funcionario, dataInicio, dataFim)
		def periodoFechado = populaPeriodoFechado(funcionario, dataInicio, dataFim)
		def dadosAbono = populaDadosAbonos(funcionario, dataInicio, dataFim)
		def dadosAfastamentos = populaDadosAfastamentos(funcionario)
		def dadosHorasExtras = populaDadosHorasExtras(funcionario, dataInicio, dataFim)
		def dadosTotalizadoresHorasExtras = populaDadosTotalizadoresHorasExtras(funcionario, dataInicio, dataFim)
		def dadosLancamentosDiarios = populaDadosLancamentosDiarios(funcionario, dataInicio, dataFim)
		def dadosBancoHoras = populaDadosBancoHoras(funcionario, dataInicio, dataFim)
		def dadosCorrigirBatidas = populaDadosCorrigirBatidas(funcionario, dataInicio, dataFim)

		[funcionario: funcionario, dadosPrincipais: dadosPrincipais, periodoFechado: periodoFechado, dadosAbono: dadosAbono,
			dadosAfastamentos: dadosAfastamentos, dadosHorasExtras: dadosHorasExtras, dadosTotalizadoresHorasExtras: dadosTotalizadoresHorasExtras,
			dadosLancamentosDiarios: dadosLancamentosDiarios, dadosBancoHoras: dadosBancoHoras, dadosCorrigirBatidas: dadosCorrigirBatidas]
	}

	def abonar() {
		
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)

		Usuario usuario = session['usuario']
		Funcionario funcionario = Funcionario.get(params.funcionarioId.toLong())
		Filial filial = funcionario.filial
		
		Date dataInicioAbono = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicioAbono)
		Integer quantidadeDias = params.quantidadeDiasAbono.toInteger()
		Integer quantidadeHoras = Horarios.calculaMinutos(params.cargaHorariaInicioAbono)
		Date dataFimAbono = dataInicioAbono + quantidadeDias - 1
		Justificativas justificativa = Justificativas.get(params.justificativaAbonoId.toLong())
		Cid cid = params.cidId ? Cid.get(params.cidId.toLong()) : null
		Integer maximoDias = justificativa.vrDiasValido ? justificativa.vrDiasValido.toInteger() : 0
		
		def consulta = "from Movimentos m "
			.concat("where m.ocorrencia.idTpOcor = 'D' ")
			.concat("  and m.ocorrencia.dscOcor not like 'DSR%' ")
			.concat("  and m.nrQtdeHr > 0 ")
			.concat("  and m.dtBat = :data ")
			.concat("  and m.funcionario = :funcionario ")
			.concat("order by m.dtBat ")
		Movimentos movimento = Movimentos.find(consulta, [funcionario: funcionario, data: dataInicioAbono])
		
		if (maximoDias > 0 && quantidadeDias > maximoDias) {
			
			flash.error = "Quantidade de dias informados ultrapassa o permitido para justificativa"
			
		} else {
			
			def retorno = abonoService.processaAbono(filial.id,
				funcionario.id,
				usuario.id,
				justificativa.id,
				dataInicioAbono,
				dataFimAbono,
				quantidadeDias,
				quantidadeHoras,
				movimento.nrQtdeHr == quantidadeHoras ? "0" : "1",
				params.observacaoAbono,
				cid)
			
			if (retorno == 0) {
				flash.message = "Nao existe periodo cadastrado para esta filial nesta data"
			} else if (retorno == 1) {
				flash.message = "O perido para esta data nao esta fechado"
			}			
			
		}

		redirecionaTelaPrincipal(funcionario, dataInicio, dataFim)
	}
	
	def desabonar() {
		
		Abonos abono = Abonos.get(params.idDesabono.toLong())
		Funcionario func = abono.funcionario
		Date dtInicio = abono.dtInicio
		Date dtFim = abono.dtFim
		
		abono.delete(flush: true)
		
		chamaProcedureProcessar(func, dtInicio, dtFim)
		redirecionaTelaPrincipal(func, dtInicio, dtFim)
		
	}

	def chamaProcedureProcessar(funcionario, dataInicio, dataFim) {

		def seqEmpresa = funcionario.filial.empresa.id 
		def seqFilial = funcionario.filial.id
		def seqFuncionario = funcionario.id

		def template = new JdbcTemplate(dataSource)

		if (detectDataBaseService.isPostgreSql()) {
			def dtInicioFormatada = new SimpleDateFormat("yyyy-MM-dd").format(dataInicio)
			def dtFimFormatada = new SimpleDateFormat("yyyy-MM-dd").format(dataFim)
			def chamada = "select pr_processar(${seqEmpresa}::integer, ${seqFilial}::integer, ${seqFuncionario}::integer, '${dtInicioFormatada}'::date, '${dtFimFormatada}'::date);"
			
			println "chamada: " + chamada
			
			def valor = template.queryForObject(chamada, Integer.class)
		} else if (detectDataBaseService.isOracle()) {
			def dtIni = new SimpleDateFormat("dd/MM/yyyy").format(dataInicio)
			def dtFim = new SimpleDateFormat("dd/MM/yyyy").format(dataFim)
			Sql sql = new Sql(dataSource)
			def chamada = "call pkg_phponto.pr_processar(${seqEmpresa}, ${seqFilial}, ${seqFuncionario}, '${dtIni}', '${dtFim}')" 
			sql.call(chamada)
		}


	}

	def mudancaHorario() {
		Funcionario funcionario = Funcionario.get(params.funcionarioId.toLong()) 
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)

		FuncionarioHorario ultimoAtivo = funcionario.getFuncionarioHorarioAtivo()
		Horario horario = Horario.get(params.mudancaHorarioId)
		Date dtInicioHorario = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicioHorario)
		Integer indiceHorario = params.indiceHorario.toInteger()
		Integer sequencia = ultimoAtivo != null ? (ultimoAtivo.sequencia + 1) : 0

		FuncionarioHorario fh = new FuncionarioHorario()
		fh.funcionario = funcionario
		fh.horario = horario
		fh.sequencia = sequencia
		fh.dtIniVig = dtInicioHorario
		fh.dtFimVig = null
		fh.nrIndHorario = indiceHorario

		if (ultimoAtivo != null) {
			ultimoAtivo.dtFimVig = dtInicioHorario
		}

		funcionario.horarios.add(fh)
		funcionario.horario = horario

		funcionario.save(flush: true)

		chamaProcedureProcessar(funcionario, dataInicio, dataFim)
		redirecionaTelaPrincipal(funcionario, dataInicio, dataFim)
	}

	def cadastroPrincipal() {
		redirect(controller: "funcionario", action: "edit", params: params)
	}

	def afastamentos() {
		Funcionario funcionario = Funcionario.get(params.funcionarioId.toLong())
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)

		Justificativas justificativa = Justificativas.get(params.justificativaAfastamentoId.toLong())
		Date dataInicioAfastamento = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicioAfastamento)
		Date dataFimAfastamento = params.dataFimAfastamento ? new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFimAfastamento) : null

		def afastamentosFuncionario = Afastamentos.findByFuncionario(funcionario)
		afastamentosFuncionario.each { a ->
			if ((dataInicioAfastamento.after(a.dataInicio) && dataInicioAfastamento.before(a.dataFim))
			|| (dataFimAfastamento != null && dataFimAfastamento.after(a.dataInicio) && a.dataFim != null && dataFimAfastamento.before(a.dataFim))) {
				flash.message = "J� existe um afastamento com estas datas"
				return
			}
		}

		Afastamentos a = new Afastamentos()
		a.funcionario = funcionario
		a.justificativas = justificativa
		a.dataInicio = dataInicioAfastamento
		a.dataFim = dataFimAfastamento

		a.save(flush: true)

		chamaProcedureProcessar(funcionario, dataInicio, dataFim)
		redirecionaTelaPrincipal(funcionario, dataInicio, dataFim)
	}

	def deleteAfastamentos() {

		Funcionario funcionario = Funcionario.get(params.funcionarioId.toLong())
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)

		def afastamentosInstance = Afastamentos.get(params.id)
		if (!afastamentosInstance) {
			flash.error = message(code: 'default.not.found.message', args: ['Afastamentos', afastamentosInstance.id])
		} else {
			def dataInicioAfastamento = afastamentosInstance.dataInicio
			def dataFimAfastamento = afastamentosInstance.dataFim
			def consulta = "from StatusFechamento s "
					.concat("where s.funcionario = :funcionario  ")
					.concat("  and (:data between s.periodo.dtIniPr and s.periodo.dtFimPr) ")

			def possuiRegistroNaDataInicio = !StatusFechamento.findAll(consulta, [funcionario: funcionario, data: dataInicioAfastamento]).isEmpty()
			def possuiRegistroNaDataFim = dataFimAfastamento != null && !StatusFechamento.findAll(consulta, [funcionario: funcionario, data: dataFimAfastamento]).isEmpty()
			def podeDeletar =  !possuiRegistroNaDataInicio && !possuiRegistroNaDataFim

			if (podeDeletar) {
				try {
					afastamentosInstance.delete(flush: true)
					flash.message = message(code: 'default.deleted.message', args: ['Afastamentos'])
				} catch (DataIntegrityViolationException e) {
					flash.error = message(code: 'default.not.deleted.message', args: ['Afastamentos'])
				}
			} else {
				flash.message = "Per�odo do afastamendo j� foi fechado"
			}
		}

		chamaProcedureProcessar(funcionario, dataInicio, dataFim)
		redirecionaTelaPrincipal(funcionario, dataInicio, dataFim)
	}

	def lancamentosDiarios() {

		Funcionario funcionario = Funcionario.get(params.funcionarioId.toLong())
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)

		ItensDiario.executeUpdate("delete from ItensDiario where funcionario = :funcionario and dtBat between :dtInicio and :dtFim",
				[funcionario: funcionario, dtInicio: dataInicio, dtFim: dataFim])

		params.each { p ->
			if (p.key.toString().startsWith("classificacaoCelulaLancamento-")) {
				def campos = p.key.toString().split("-")
				def dataStr = campos[1]
				def dataObj = new SimpleDateFormat("ddMMyyyy").parse(dataStr)
				def classificao = p.value.toString()

				if (classificao) {
					ItensDiario item = new ItensDiario()
					item.funcionario = funcionario
					item.dtBat = dataObj
					item.classificacao = Classificacao.get(classificao.toLong())
					if (item.classificacao.idTpDia.equals("TR")) {
						def itemHorario = params["itemHorarioCelulaLancamento-"+dataStr]
						item.perfilHorario = itemHorario ? Perfilhorario.get(itemHorario.toLong()) : null
					}
					item.qtdeChEfetiva = 0

					item.save(flush: true)
				}
			}
		}

		chamaProcedureProcessar(funcionario, dataInicio, dataFim)
		redirecionaTelaPrincipal(funcionario, dataInicio, dataFim)
	}

	def corrigirBatidas() {

		Funcionario funcionario = Funcionario.get(params.funcionarioId.toLong())
		Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		Date dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)
		
		List<Batidas> batidasAtualizar = new ArrayList<Batidas>();
		List<Batidas> batidasIncluir = new ArrayList<Batidas>();
		List<Batidas1510> batidas1510Incluir = new ArrayList<Batidas1510>();
		List<Batidas> batidasRemover = new ArrayList<Batidas>();
		
		params.each { p ->

			// update
			if (p.key.toString().startsWith("statusBatida-")) {
				def campos = p.key.toString().split("-")
				def idBatida = campos[1].toLong()

				def keyRemove = "excluirBatida-" + idBatida

				if (!params.containsKey(keyRemove)) {
					
					Batidas bat = Batidas.get(idBatida)
					if (!bat.idBat.equals(p.value.toString())) {
						bat.idBat = p.value.toString()
						batidasAtualizar.add(bat)
					}
					
				}
			}

			// inclusao
			if (p.key.toString().startsWith("incluirBatida-")) {
				def campos = p.key.toString().split("-")

				def identificadorDataBatida = campos[1].toString()
				def identificadorDataAtual = campos[2].toString()
				def dataBatida = new SimpleDateFormat("ddMMyyyy").parse(identificadorDataBatida)

				def keyMotivo = "idMotivoIncluirBatida-"+identificadorDataBatida.toString()+"-"+identificadorDataAtual
				def idMotivo = params[keyMotivo].toLong()

				def keyHoraBatida = "horaIncluirBatida-"+identificadorDataBatida.toString()+"-"+identificadorDataAtual
				def horaBatida = Horarios.calculaMinutos(params[keyHoraBatida].toString())

				def keyStatusBatida = "statusIncluirBatida-"+identificadorDataBatida.toString()+"-"+identificadorDataAtual
				def statusBatida = params[keyStatusBatida].toString().toUpperCase()

				Batidas bat = new Batidas()
				bat.funcionario = funcionario
				bat.dtBat = dataBatida
				bat.hrBat = horaBatida
				bat.idBat = statusBatida
				bat.idLido = "N"
				bat.status = ""
				batidasIncluir.add(bat)

				Batidas1510 bat1510 = new Batidas1510()
				bat1510.funcionario = funcionario
				bat1510.motivoCartao = MotivoCartao.get(idMotivo)
				bat1510.dtBat = bat.dtBat
				bat1510.hrBat = bat.hrBat
				bat1510.idBat = bat.idBat
				bat1510.status = bat.status
				batidas1510Incluir.add(bat1510)

			}

			// exclusao
			if (p.key.toString().startsWith("excluirBatida-")) {
				def campos = p.key.toString().split("-")
				def idBatida = campos[1].toLong()
				def keyMotivo = "idMotivoExcluirBatida-"+idBatida.toString()
				def idMotivo = params[keyMotivo].toLong()

				Batidas bat = Batidas.get(idBatida)
				batidasRemover.add(bat)

				Batidas1510 bat1510 = new Batidas1510()
				bat1510.funcionario = funcionario
				bat1510.motivoCartao = MotivoCartao.get(idMotivo)
				bat1510.dtBat = bat.dtBat
				bat1510.hrBat = bat.hrBat
				bat1510.idBat = bat.idBat
				bat1510.status = bat.status
				batidas1510Incluir.add(bat1510)
			}

		}
		
		batidasAtualizar.each { bat ->
			bat.save()
			println "### Atualizada batida id ${bat.id}, data ${bat.dtBat}, status ${bat.idBat}, hrbat ${bat.hrBat}"
		}
		
		batidasIncluir.each { bat ->
			bat.save()
			println "### Incluindo batida data ${bat.dtBat}, status ${bat.idBat}, hrbat ${bat.hrBat}"
		}
		
		batidas1510Incluir.each { bat ->
			bat.save()
			println "### Incluindo batida data ${bat.dtBat}, status ${bat.idBat}, hrbat ${bat.hrBat}"
		}
		
		batidasRemover.each { bat ->
			bat.delete()
			println "### Removendo batida data ${bat.dtBat}, status ${bat.idBat}, hrbat ${bat.hrBat}"
		}

		chamaProcedureProcessar(funcionario, dataInicio, dataFim)
		redirecionaTelaPrincipal(funcionario, dataInicio, dataFim)
	}
	
	def atualizaLeituraObservacao() {
		def dataObs = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataObservacao)
		def funcionario = Funcionario.get(params.funcionarioId.toLong())
		
		ObservacaoFuncionario obs = ObservacaoFuncionario.findByFuncionarioAndDtBat(funcionario, dataObs)
		obs.status = 'F'
		obs.save()
		
		render(status: response.SC_OK)
	}
	
	def chamarProcedure() {
		
		println "invocando chamarProcedure()"
		
		try {
			Funcionario funcionario = Funcionario.get(params.funcionarioId)
			
			println "funcionario: " + funcionario
			
			def dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
			def dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)
			
			println "dataInicio: " + dataInicio
			println "dataFim: " + dataFim
			
			chamaProcedureProcessar(funcionario, dataInicio, dataFim)
		} catch (Exception e) {
			render(status: response.SC_INTERNAL_SERVER_ERROR)
		}
		render(status: response.SC_OK)
	}

	public void redirecionaTelaPrincipal(funcionario, dataInicio, dataFim) {
		def dadosPrincipais = populaDadosPrincipais(funcionario, dataInicio, dataFim)
		def periodoFechado = populaPeriodoFechado(funcionario, dataInicio, dataFim)
		def dadosAbono = populaDadosAbonos(funcionario, dataInicio, dataFim)
		def dadosAfastamentos = populaDadosAfastamentos(funcionario)
		def dadosHorasExtras = populaDadosHorasExtras(funcionario, dataInicio, dataFim)
		def dadosTotalizadoresHorasExtras = populaDadosTotalizadoresHorasExtras(funcionario, dataInicio, dataFim)
		def dadosLancamentosDiarios = populaDadosLancamentosDiarios(funcionario, dataInicio, dataFim)
		def dadosBancoHoras = populaDadosBancoHoras(funcionario, dataInicio, dataFim)

		redirect(action: "exibir", params: params,
		model: [funcionario: funcionario, dadosPrincipais: dadosPrincipais, periodoFechado: periodoFechado, dadosAbono: dadosAbono,
			dadosAfastamentos: dadosAfastamentos, dadosHorasExtras: dadosHorasExtras, dadosTotalizadoresHorasExtras: dadosTotalizadoresHorasExtras,
			dadosLancamentosDiarios: dadosLancamentosDiarios, dadosBancoHoras: dadosBancoHoras])
	}

	def populaDadosPrincipais(funcionario, dtInicio, dtFim) {
		def dadosPrincipais = []
		 
		def dados = Dados.findAll("from Dados d left join fetch d.justificativa1 left join fetch d.justificativa2 left join fetch d.justificativa3 left join fetch d.justificativa4 where d.funcionario = :funcionario and d.dtBat between :dataInicio and :dataFim order by d.dtBat", 
			[funcionario: funcionario, dataInicio: dtInicio, dataFim: dtFim])
		def observacoes =  ObservacaoFuncionario.findAll("from ObservacaoFuncionario o where o.funcionario = :funcionario and o.dtBat between :dataInicio and :dataFim",
			[funcionario: funcionario, dataInicio: dtInicio, dataFim: dtFim])
		def movimentos = Movimentos.findAll("from Movimentos m where m.funcionario = :funcionario and m.dtBat between :dataInicio and :dataFim",
			[funcionario: funcionario, dataInicio: dtInicio, dataFim: dtFim])
		def abonos = Abonos.findAll("from Abonos a where a.funcionario = :funcionario and (a.dtInicio between :dataInicio and :dataFim or a.dtFim between :dataInicio and :dataFim) ",
			[funcionario: funcionario, dataInicio: dtInicio, dataFim: dtFim])

		dados.each { d->

			ObservacaoFuncionario obs = observacoes.find { o -> 
				new SimpleDateFormat("ddMMyyyy").format(o.dtBat).equals(new SimpleDateFormat("ddMMyyyy").format(d.dtBat)) 
			}  
			
			Movimentos mov = movimentos.find { o ->
				new SimpleDateFormat("ddMMyyyy").format(o.dtBat).equals(new SimpleDateFormat("ddMMyyyy").format(d.dtBat))
			}
			
			Abonos abono = abonos.find { a ->
				d.dtBat >= a.dtInicio && d.dtBat <= a.dtFim
			}
			
			Ocorrencias ocorrencia = mov?.ocorrencia
			def possuiDesconto = ocorrencia != null && ocorrencia.idTpOcor.equals("D") && !ocorrencia.dscOcor.startsWith("DSR")

			dadosPrincipais.add([
				identificador: d.id,
				dataBatida: new SimpleDateFormat("dd/MM/yyyy").format(d.dtBat),
				diaBatida: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(d.dtBat),
				horario: d.perfilhorario?.dscperfil,
				entrada1: d.hrBat1 ? Horarios.retornaHora(d.hrBat1) : "",
				saida1: d.hrBat2 ? Horarios.retornaHora(d.hrBat2) : "",
				entrada2: d.hrBat3 ? Horarios.retornaHora(d.hrBat3) : "",
				saida2: d.hrBat4 ? Horarios.retornaHora(d.hrBat4) : "",
				entrada3: d.hrBat5 ? Horarios.retornaHora(d.hrBat5) : "",
				saida3: d.hrBat6 ? Horarios.retornaHora(d.hrBat6) : "",
				entrada4: d.hrBat7 ? Horarios.retornaHora(d.hrBat7) : "",
				saida4: d.hrBat8 ? Horarios.retornaHora(d.hrBat8) : "",
				observacao: descricaoTipoDia(d, movimentos),
				observacaoFuncionario: obs ? obs.observacao : "",
				statusObservacaoFuncionario: obs ? obs.status : "A",
				stleitura: d.stleitura,
				possuiDesconto: possuiDesconto,
				possuiAbono: abono != null
			])
		}

		return dadosPrincipais
	}

	def populaPeriodoFechado(funcionario, dtInicio, dtFim) {

		def consulta = "from StatusFechamento s "
				.concat("where s.funcionario = :funcionario  ")
				.concat("  and (:data between s.periodo.dtIniPr and s.periodo.dtFimPr) ")

		def possuiRegistroNaDataInicio = !StatusFechamento.findAll(consulta, [funcionario: funcionario, data: dtInicio]).isEmpty()
		def possuiRegistroNaDataFim = !StatusFechamento.findAll(consulta, [funcionario: funcionario, data: dtFim]).isEmpty()
		def periodoFechado = possuiRegistroNaDataInicio || possuiRegistroNaDataFim

		return periodoFechado
	}

	def populaDadosAbonos(funcionario, dtInicio, dtFim) {

		def dadosAbono = []

		def consulta = "from Abonos a "
				.concat("where a.funcionario = :funcionario ")
				.concat("  and (a.dtInicio between :dtInicio and :dtFim or a.dtFim between :dtInicio and :dtFim) ")
				.concat("order by a.dtInicio ")

		def abonos = Abonos.findAll(consulta, [dtInicio: dtInicio, dtFim: dtFim, funcionario: funcionario])

		abonos.each { a ->
			dadosAbono.add([
				id: a.id,
				dtInicio: new SimpleDateFormat("dd/MM/yyyy").format(a.dtInicio),
				dtFim: new SimpleDateFormat("dd/MM/yyyy").format(a.dtFim),
				qtdadeDias: a.qtdadeDias,
				ocorrencia: a.ocorrencias.dscOcor,
				justificativa: a.justificativas.dscJust,
				observacao: a.obsJustificativa
			])
		}

		return dadosAbono
	}

	def populaDadosAfastamentos(funcionario) {

		def dadosAfastamentos = []
		def afastamentos = Afastamentos.findByFuncionario(funcionario)
		afastamentos.each { a ->
			dadosAfastamentos.add(
					[id: a.id,
						funcionario: funcionario.nomFunc,
						justificativa: a.justificativas.dscJust,
						dataInicio: new SimpleDateFormat("dd/MM/yyyy").format(a.dataInicio),
						dataFim: a.dataFim != null ? new SimpleDateFormat("dd/MM/yyyy").format(a.dataFim) : ""]
					)
		}

		return dadosAfastamentos
	}

	def populaDadosHorasExtras(funcionario, dataInicio, dataFim) {
		def dadosHorasExtras = []

		def template = new JdbcTemplate(dataSource)
		NumberFormat formatter = new DecimalFormat("0.00");

		def consulta = "from Movimentos m "
				.concat("where m.nrQtdeHr > 0 ")
				.concat("  and m.ocorrencia.idTpOcor = 'P' ")
				.concat("  and m.ocorrencia.tipohoraextras is not null ")
				.concat("  and m.dtBat between :dtInicio and :dtFim ")
				.concat("  and m.funcionario = :funcionario ")
				.concat("order by m.dtBat ")
		def movimentos = Movimentos.findAll(consulta, [dtInicio: dataInicio, dtFim: dataFim, funcionario: funcionario])

		movimentos.each { m ->
			def formula = m.ocorrencia.formulaOcor
			def salario = funcionario.vrSal
			def numHoras = (m.nrQtdeHr/60)
			def valor = 0
			if (detectDataBaseService.isPostgreSql()) {
				valor = template.queryForObject("select retornaValor('${formula}', ${salario}, ${numHoras})", Float.class)
			} else if (detectDataBaseService.isOracle()) {
				valor = template.queryForObject("select retorna_valor('${formula}', ${salario}, ${numHoras}) from dual", Float.class)
			}
			
			dadosHorasExtras.add(
					[data: m.dtBat,
						dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(m.dtBat),
						ocorrencia: m.ocorrencia.dscOcor,
						quantidadeHora: m.nrQtdeHr ? Horarios.retornaHora(m.nrQtdeHr) : "",
						valor: formatter.format(valor)]
					)
		}

		return dadosHorasExtras
	}

	def populaDadosTotalizadoresHorasExtras(funcionario, dataInicio, dataFim) {
		NumberFormat formatter = new DecimalFormat("0.00");
		return [totalExtras1: formatter.format(calculaTotalExtra(funcionario, dataInicio, dataFim, 1)),
			totalExtras2: formatter.format(calculaTotalExtra(funcionario, dataInicio, dataFim, 2))]
	}

	def populaDadosLancamentosDiarios(funcionario, dataInicio, dataFim) {
		def dadosLancamentosDiarios = []
		Date dataTemp = new java.util.Date(dataInicio.time)
		while (dataTemp.before(dataFim) || dataTemp.equals(dataFim)) {
			ItensDiario item = ItensDiario.findByFuncionarioAndDtBat(funcionario, dataTemp)
			dadosLancamentosDiarios.add(
					[dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(dataTemp),
						data: new SimpleDateFormat("dd").format(dataTemp),
						identificadorDia: new SimpleDateFormat("ddMMyyyy").format(dataTemp),
						classificacao: item?.classificacao?.id,
						itemHorario: item?.perfilHorario?.id]
					)
			dataTemp = dataTemp.plus(1)
		}
		return dadosLancamentosDiarios
	}

	def calculaTotalExtra(funcionario, dataInicio, dataFim, indiceExtra) {
		def template = new JdbcTemplate(dataSource)
		def totalExtras = 0
		def consultaExtras = "from Movimentos m "
				.concat("where m.nrQtdeHr > 0 ")
				.concat("  and m.ocorrencia.idTpOcor = 'P' ")
				.concat("  and m.ocorrencia.tipohoraextras is not null ")
				.concat("  and m.ocorrencia.tipohoraextras.vrIndHorExt = ").concat(indiceExtra.toString())
				.concat("  and m.dtBat between :dtInicio and :dtFim ")
				.concat("  and m.funcionario = :funcionario ")
				.concat("order by m.dtBat ")
		def movimentosExtras = Movimentos.findAll(consultaExtras, [dtInicio: dataInicio, dtFim: dataFim, funcionario: funcionario])

		movimentosExtras.each { m ->
			def formula = m.ocorrencia.formulaOcor
			def salario = funcionario.vrSal
			def numHoras = (m.nrQtdeHr/60)
			
			if (detectDataBaseService.isPostgreSql()) {
				totalExtras += template.queryForObject("select retornaValor('${formula}', ${salario}, ${numHoras})", Float.class)
			} else if (detectDataBaseService.isOracle()) {
				totalExtras += template.queryForObject("select retorna_valor('${formula}', ${salario}, ${numHoras}) from dual", Float.class)
			}
		}

		return totalExtras
	}

	def populaDadosBancoHoras(funcionario, dataInicio, dataFim) {
		def dadosBancoHoras = []
		def ocorrencias = OcorrenciasBh.findAll("from OcorrenciasBh where funcionario = :funcionario and dtBat between :dtInicio and :dtFim order by dtBat",
				[funcionario: funcionario, dtInicio: dataInicio, dtFim: dataFim])

		ocorrencias.each { o->
			dadosBancoHoras.add([
				data: new SimpleDateFormat("dd/MM/yyyy").format(o.dtBat),
				dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(o.dtBat),
				ocorrencia: o.ocorrencias.dscOcor,
				quantidadeHora: o.nrQtdeHr ? Horarios.retornaHora(o.nrQtdeHr) : "",
				lancamento: o.idTpLanc.equals("A") ? "AUTOM&Aacute;TICA" : "MANUAL",
				tipoOcorrencia: o.idTpOcorr.equals("D") ? "D&Eacute;BITO" : "CR&Eacute;DITO",
			])
		}

		return dadosBancoHoras
	}

	def populaDadosCorrigirBatidas(funcionario, dataInicio, dataFim) {
		def dadosCorrigirBatidas = []
		def sql = "from Batidas b "
				.concat("where b.funcionario = :funcionario ")
				.concat("  and (b.idBloqueado is null or b.idBloqueado = 'N') ")
				.concat("  and b.dtBat between :dtInicio and :dtFim ")
				.concat("order by b.dtBat asc, b.hrBat asc ")

		def batidas = Batidas.findAll(sql, [funcionario: funcionario, dtInicio: dataInicio, dtFim: dataFim])

		def resultadoMap = []
		
		Date dataTemp = new java.util.Date(dataInicio.time)
		while (dataTemp.before(dataFim) || dataTemp.equals(dataFim)) {
			def batidasDia = batidas.findAll { b -> b.dtBat.equals(dataTemp) } 
			if (batidasDia.isEmpty()) {
				dadosCorrigirBatidas.add([
					data: new SimpleDateFormat("dd/MM/yyyy").format(dataTemp),
					dia: new SimpleDateFormat("EEEE", new Locale("pt", "BR")).format(dataTemp),
					identificadorDia: new SimpleDateFormat("ddMMyyyy").format(dataTemp),
					listaBatidas: []
				])
			} else {
				def dataAnterior = null
				batidasDia.each() {
		
					if (dataAnterior!= null && it.dtBat.equals(dataAnterior)) {
						def listaBatidas = dadosCorrigirBatidas.get(dadosCorrigirBatidas.size()-1).listaBatidas
						listaBatidas.add([identificador: it.id,
							status: it.idBat,
							descStatus: it.idBat.equals("E") ? "ENTRADA" : "SA&Iacute;DA",
							horas: Horarios.retornaHora(it.hrBat)])
					} else {
						def listaBatidas = []
						listaBatidas.add([identificador: it.id,
							status: it.idBat,
							descStatus: it.idBat.equals("E") ? "ENTRADA" : "SA&Iacute;DA",
							horas: Horarios.retornaHora(it.hrBat)])
		
						dadosCorrigirBatidas.add([
							data: new SimpleDateFormat("dd/MM/yyyy").format(it.dtBat),
							dia: new SimpleDateFormat("EEEE", new Locale("pt", "BR")).format(it.dtBat),
							identificadorDia: new SimpleDateFormat("ddMMyyyy").format(it.dtBat),
							listaBatidas: listaBatidas
						])
					}
					dataAnterior = it.dtBat
				}
			}
			
			dataTemp = dataTemp.plus(1)
		}
		
		
		

		return dadosCorrigirBatidas
	}

	def descricaoTipoDia(d, movimentos) {
		String retorno = " "

		if (d.justificativa1 != null) {
			retorno += d.justificativa1.dscJust + " "
		} 
		if (d.justificativa2 != null) {
			retorno += d.justificativa2.dscJust + " "
		} 
		if (d.justificativa3 != null) {
			retorno += d.justificativa3.dscJust + " "
		}

		if (retorno.trim().isEmpty()) {
			if (d.idTpDia.equals("FE")) {
				retorno = "FERIADO"
			} else if (d.idTpDia.equals("CO")) {
				retorno = "COMPENSADO"
			} else if (d.idTpDia.equals("DS")) {
				retorno = "DSR"
			} else if (d.idTpDia.equals("FG")) {
				retorno = "FOLGA"
			}
		}


		if (retorno.trim().isEmpty()) {
			
			Movimentos m = movimentos.find { mov -> mov.dtBat.equals(d.dtBat) }
			if (m != null && !m.ocorrencia.idTpOcor.equals("P")) {
				retorno = m.ocorrencia.dscOcor
			}
		}

		return retorno
	}

	def exibirOcorrencias() {
		Dados d = Dados.get(params.dadoId.toLong())
		d.stleitura = "S"
		d.save(flush: true)
		
		Funcionario f = d.funcionario
		Date dtBat = d.dtBat
		def movimentos = Movimentos.findAllByFuncionarioAndDtBat(f, dtBat)
		def resultadoMap = []
		movimentos.each() {
			resultadoMap.add([id: it.id,
				situacaoAbo: it.idSituacaoAbono,
				horas: Horarios.retornaHora(it.nrQtdeHr),
				ocorrencia: it.ocorrencia.dscOcor])
		}
		
		if (resultadoMap) {
			render resultadoMap as JSON
		} else {
			[]
		}
	}
	
	def bloquearBatida() {
		Batidas b = Batidas.get(params.batida.toLong())
		b.idBloqueado = "S"
		b.save(flush: true)
		render(status: response.SC_OK)
	}
	
	def desbloquearBatida() {
		Batidas b = Batidas.get(params.batida.toLong())
		b.idBloqueado = "N"
		b.save(flush: true)
		render(status: response.SC_OK)
	}

	def exibirBatidas() {
		def resultadoMap = []

		Dados d = Dados.get(params.dadoId.toLong())
		Funcionario f = d.funcionario
		Date dtBat = d.dtBat

		def batidas = Batidas.findAllByFuncionarioAndDtBat(f, dtBat)
		def batidas1510 = Batidas1510.findAllByFuncionarioAndDtBat(f, dtBat)

		// adiciona batidas normais e inclusoes manuais
		batidas.each() {
			def incluidoManual = false
			for (bat1510 in batidas1510) {
				if (it.hrBat.equals(bat1510.hrBat)) {
					incluidoManual = true
					break
				}
			}
			resultadoMap.add([
				id: it.id,
				status: it.idBat,
				horas: Horarios.retornaHora(it.hrBat),
				operacao: incluidoManual ? "INC" : "",
				bloqueado: it.idBloqueado])
		}

		// adicionando removidos
		batidas1510.each {
			def removido = true
			for (bat in batidas) {
				if (it.hrBat.equals(bat.hrBat)) {
					removido = false
					break
				}
			}
			if (removido) {
				resultadoMap.add([
					id: it.id,
					status: it.idBat,
					horas: Horarios.retornaHora(it.hrBat),
					operacao: "REM",
					bloqueado: "S"])
			}
		}

		if (resultadoMap) {
			resultadoMap.sort { it.horas }
			render resultadoMap as JSON
		} else {
			[]
		}
	}
	
	def exibirMovimentoOcorrencia() {
		
		Funcionario funcionario = Funcionario.get(params.funcionario.toLong())
		Date dia = new SimpleDateFormat("dd/MM/yyyy").parse(params.dia)
		
		def consulta = "from Movimentos m "
				.concat("where m.ocorrencia.idTpOcor = 'D' ")
				.concat("  and m.ocorrencia.dscOcor not like 'DSR%' ")
				.concat("  and m.nrQtdeHr > 0 ")
				.concat("  and m.dtBat = :data ")
				.concat("  and m.funcionario = :funcionario ")
				.concat("order by m.dtBat ")
				
		Movimentos movimento = Movimentos.find(consulta, [funcionario: funcionario, data: dia])
		
		def resultado = []
		
		if (movimento) {
			resultado = [
				cargaHoraria: movimento.nrQtdeHr ? Horarios.retornaHora(movimento.nrQtdeHr) : "", 
				ocorrencia: movimento.ocorrencia.dscOcor]
		}
		
		render resultado as JSON
	}

}