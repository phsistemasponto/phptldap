package cadastros

import org.springframework.dao.DataIntegrityViolationException

import funcoes.Horarios



class ParamSistemaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def paramSistemaFilial

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [paramSistemaInstanceList: ParamSistema.list(params), paramSistemaInstanceTotal: ParamSistema.count()]
    }

    def delete() {
        def paramSistemaInstance = ParamSistema.get(params.id)
        if (!paramSistemaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ParamSistema', paramSistemaInstance.id])
        } else {
            try {
				
				def ParamSistemaFilialList = ParamSistemaFilial.findAllWhere(paramSistema: paramSistemaInstance)
				ParamSistemaFilialList.each {
					it.delete()
				}
				
                paramSistemaInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['ParamSistema'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['ParamSistema'])
            }
        }

        redirect(action: "list")
    }	
	
    def create() {
		ParamSistema paramSistemaInstance = new ParamSistema()
		[paramSistemaInstance: paramSistemaInstance, filiais: paramSistemaInstance.getParamSistemaFiliais()]
    }

    def edit() {
        def paramSistemaInstance = ParamSistema.get(params.id)
        if (!paramSistemaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['ParamSistema', paramSistemaInstance.id])
            redirect(action: "list")
            return
        }

        [paramSistemaInstance: paramSistemaInstance, filiais: paramSistemaInstance.getParamSistemaFiliais()]
    }

    def save() {
        def paramSistemaInstance = new ParamSistema(params)
		
		configuraInstance(paramSistemaInstance, params)
		
		if (!paramSistemaInstance.save(flush: true)) {
            render(view: "create", model: [paramSistemaInstance: paramSistemaInstance])
            return
        }
		
		incluirParamSistemaFilial(params,paramSistemaInstance)

		flash.message = message(code: 'default.created.message', args: ['ParamSistema'])
        redirect(action: "list")
    }

	private configuraInstance(ParamSistema paramSistemaInstance, Map params) {
		if (params.consideraFaltaIntegral){
			paramSistemaInstance.consideraFaltaIntegral='S'
		} else {
			paramSistemaInstance.consideraFaltaIntegral='N'
		}

		if (params.checkFaltaTotal){
			paramSistemaInstance.checkFaltaTotal='S'
		} else {
			paramSistemaInstance.checkFaltaTotal='N'
		}

		if (params.check1Falta){
			paramSistemaInstance.check1Falta='S'
		} else {
			paramSistemaInstance.check1Falta='N'
		}

		if (params.check2Falta){
			paramSistemaInstance.check2Falta='S'
		} else {
			paramSistemaInstance.check2Falta='N'
		}

		if (params.falta2event1){
			paramSistemaInstance.falta2event1='S'
		} else {
			paramSistemaInstance.falta2event1='N'
		}

		if (params.descontarAtrasoPelaExtra){
			paramSistemaInstance.descontarAtrasoPelaExtra='S'
		} else {
			paramSistemaInstance.descontarAtrasoPelaExtra='N'
		}

		if (params.checkAtraso){
			paramSistemaInstance.checkAtraso='S'
		} else {
			paramSistemaInstance.checkAtraso='N'
		}

		if (params.check1Atraso){
			paramSistemaInstance.check1Atraso='S'
		} else {
			paramSistemaInstance.check1Atraso='N'
		}

		if (params.check2Atraso){
			paramSistemaInstance.check2Atraso='S'
		} else {
			paramSistemaInstance.check2Atraso='N'
		}

		if (params.checkSaida){
			paramSistemaInstance.checkSaida='S'
		} else {
			paramSistemaInstance.checkSaida='N'
		}

		if (params.check1Saida){
			paramSistemaInstance.check1Saida='S'
		} else {
			paramSistemaInstance.check1Saida='N'
		}

		if (params.check2Saida){
			paramSistemaInstance.check2Saida='S'
		} else {
			paramSistemaInstance.check2Saida='N'
		}

		if (params.adicionalNoturnoNormal){
			paramSistemaInstance.adicionalNoturnoNormal='S'
		} else {
			paramSistemaInstance.adicionalNoturnoNormal='N'
		}

		paramSistemaInstance.horaIntervaloParaOutra = params.horaInicioAdicionalNoturnoFormatada ? 
			Horarios.calculaMinutos(params.horaIntervaloParaOutraFormatada) : 0
		paramSistemaInstance.horaParaIntervaloFolga = params.horaParaIntervaloFolgaFormatada ? 
			Horarios.calculaMinutos(params.horaParaIntervaloFolgaFormatada) : 0
		paramSistemaInstance.horaInicioAdicionalNoturno = params.horaInicioAdicionalNoturnoFormatada ? 
			Horarios.calculaMinutos(params.horaInicioAdicionalNoturnoFormatada) : 0
		paramSistemaInstance.horaFimAdicionalNoturno = params.horaFimAdicionalNoturnoFormatada ? 
			Horarios.calculaMinutos(params.horaFimAdicionalNoturnoFormatada) : 0
	}
	
	def incluirParamSistemaFilial(params,paramSistemaInstance){
		ParamSistemaFilial paramSistemaFilial
		Filial filial
		params.filialId.eachWithIndex { it, i ->
			println(params)
			if (params.temFilialPeriodo[i]=='true') {
				filial = Filial.get(it)
				paramSistemaFilial = new ParamSistemaFilial( filial: filial, paramSistema : paramSistemaInstance)
				if (!paramSistemaFilial.save()){
					println('erros!')
					paramSistemaFilial.errors.each { item ->
						println(item)
					}
				} else {
					println('salvo!!')
				}
			}
		}
		
	}

    def update() {
        def paramSistemaInstance = ParamSistema.get(params.id)
		
		if (params.version) {
            def version = params.version.toLong()
            if (paramSistemaInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['ParamSistema'])
                render(view: "edit", model: [paramSistemaInstance: paramSistemaInstance])
                return
            }
        }

        paramSistemaInstance.properties = params
		
		configuraInstance(paramSistemaInstance, params)

        if (!paramSistemaInstance.save(flush: true)) {
            render(view: "edit", model: [paramSistemaInstance: paramSistemaInstance])
            return
        }
		
		ParamSistemaFilial paramSistemaFilial = new ParamSistemaFilial()
		paramSistemaFilial.atualizarParamSistemaFilial(params, paramSistemaInstance.id)

		flash.message = message(code: 'default.updated.message', args: ['ParamSistema'])
        redirect(action: "list")
    }
}