package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional


@Transactional
class TipotoleranciaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [tipotoleranciaInstanceList: Tipotolerancia.list(params), tipotoleranciaInstanceTotal: Tipotolerancia.count()]
    }

    def delete() {
        def tipotoleranciaInstance = Tipotolerancia.get(params.id)
        if (!tipotoleranciaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Tipotolerancia', tipotoleranciaInstance.id])
        } else {
            try {
                tipotoleranciaInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Tipotolerancia'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Tipotolerancia'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [tipotoleranciaInstance: new Tipotolerancia()]
    }

    def edit() {
        def tipotoleranciaInstance = Tipotolerancia.get(params.id)
        if (!tipotoleranciaInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Tipotolerancia', tipotoleranciaInstance.id])
            redirect(action: "list")
            return
        }

        [tipotoleranciaInstance: tipotoleranciaInstance]
    }

    def save() {
        def tipotoleranciaInstance = new Tipotolerancia(params)
        if (!tipotoleranciaInstance.save(flush: true)) {
            render(view: "create", model: [tipotoleranciaInstance: tipotoleranciaInstance])
            return
        }
		
		configuraFiliais(tipotoleranciaInstance)

		flash.message = message(code: 'default.created.message', args: ['Tipotolerancia'])
        redirect(action: "create")
    }

    def update() {
        def tipotoleranciaInstance = Tipotolerancia.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (tipotoleranciaInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Tipotolerancia'])
                render(view: "edit", model: [tipotoleranciaInstance: tipotoleranciaInstance])
                return
            }
        }

        tipotoleranciaInstance.properties = params

        if (!tipotoleranciaInstance.save(flush: true)) {
            render(view: "edit", model: [tipotoleranciaInstance: tipotoleranciaInstance])
            return
        }
		
		configuraFiliais(tipotoleranciaInstance)

		flash.message = message(code: 'default.updated.message', args: ['Tipotolerancia'])
        redirect(action: "list")
    }
	
	def configuraFiliais(Tipotolerancia tipotoleranciaInstance) {
		
		if (tipotoleranciaInstance.id != null) {
			TipotoleranciaFilial.findAll('from TipotoleranciaFilial where tipotolerancia = :t', [t: tipotoleranciaInstance]).each { tt ->
				tt.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def filiaisArray = isArray ? params.filial : [params.filial]
		
		filiaisArray.eachWithIndex { f, i ->
			if (f != null) {
				TipotoleranciaFilial tt = new TipotoleranciaFilial()
				tt.filial = Filial.get(filiaisArray[i].toLong())
				tt.tipotolerancia = tipotoleranciaInstance
				
				if (tipotoleranciaInstance.filiais == null) {
					tipotoleranciaInstance.filiais = new ArrayList<TipotoleranciaFilial>()
				}
				
				tipotoleranciaInstance.filiais.add(tt)
			}
		}
	}
	
}