package cadastros

import java.math.*
import java.text.*

import javax.servlet.ServletOutputStream

import funcoes.Horarios
import groovy.sql.Sql


class GerarArquivoAcjefController {
	
	def filtroPadraoService
	def dataSource
	
	def index() {
		
		if (params.dataInicio && params.dataFim) {
			
			def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
			
			def sql = """
					select d.seq_func
						 ,f.filial_id
						 ,f.pis_func
						 ,fil.dsc_fil
						 ,fil.cnpj_fil
						 ,d.dt_bat
						 ,d.hr_bat1 
						 ,d.hr_bat2
						 ,d.hr_bat3
						 ,d.hr_bat4
						 ,d.hr_bat5
						 ,d.hr_bat6
						 ,d.hr_bat7
						 ,d.hr_bat8
						 ,d.hr_bat9
						 ,d.hr_bat10
						 ,d.hr_bat12
						 ,d.hr_bat13
						 ,d.hr_bat14
						 ,d.id_perfil_horario
						 ,d.nr_qtde_ch_prevista
						 ,ph.dscperfil
						 ,d.seq_ocor1
						 ,d.seq_ocor2
						 ,d.seq_ocor3
						 ,d.seq_ocor4
						 ,d.seq_ocor5
						 ,d.seq_ocor6
						 ,d.seq_justif1
						 ,d.seq_justif2
						 ,d.seq_justif3
						 ,d.seq_justif4
						 ,d.seq_justif5
						 ,d.seq_justif6
					from dados d
					inner join funcionario f on d.seq_func = f.id
					inner join filial fil on f.filial_id = fil.id
					inner join perfilhorario ph on d.id_perfil_horario = ph.id
					where (1=1) 
				  """
			def where  = filtroPadraoService.montaFiltroPadraoSqlNativo(params)
				where += " and d.dt_bat between '" + params.dataInicio + "' and '" + params.dataFim + "' " 
			def order  = " ORDER BY d.seq_func, d.dt_bat "
			
			sql = sql.concat(where).concat(order)
			
			Sql consulta = new Sql(dataSource)
			def results = consulta.rows(sql)
			
			def fileTxt = ""
			def sequencialArquivo = 1
			
			// print header
			
			fileTxt += preencheZeros(sequencialArquivo.toString(), 9)  
			fileTxt += "1" // tipo registro
			fileTxt += "1" // tipo empregador (CNPJ)
			fileTxt += preencheZeros(somenteNumero(results[0].cnpj_fil.toString()), 14)
			fileTxt += preencheEspacos("", 12) // CEI do empregador
			fileTxt += preencheEspacos(results[0].dsc_fil, 150)
			fileTxt += new SimpleDateFormat("ddMMyyyy").format(dtInicio)
			fileTxt += new SimpleDateFormat("ddMMyyyy").format(dtFim)
			fileTxt += new SimpleDateFormat("ddMMyyyy").format(new Date())
			fileTxt += new SimpleDateFormat("HHmm").format(new Date())
			fileTxt += "\r\n"
			
			// print horarios
			
			def func = results[0].seq_func
			def sqlHorarios = """
					select distinct ph.id as idhorario, ph.dscperfil as dschorario
					from dados d
					inner join funcionario f on d.seq_func = f.id
					inner join perfilhorario ph on d.id_perfil_horario = ph.id
					where f.id = ${func}
				  """
			sqlHorarios += " and d.dt_bat between '" + params.dataInicio + "' and '" + params.dataFim + "' "

			Sql consultaHorarios = new Sql(dataSource)
			def resultHorarios = consultaHorarios.rows(sqlHorarios)
			
			resultHorarios.each { r ->
				sequencialArquivo++
				def lineHorario = ""
				def horarios = r.dschorario.split(" ")
				
				lineHorario += preencheZeros(sequencialArquivo.toString(), 9)
				lineHorario += "2" // tipo registro
				lineHorario += preencheZeros(r.idhorario.toString(), 4)
				if (horarios.length == 4) {
					lineHorario += horarios[0].replace(":", "")
					lineHorario += horarios[3].replace(":", "")
					lineHorario += horarios[1].replace(":", "")
					lineHorario += horarios[2].replace(":", "")
				} else {
					lineHorario += horarios[0].replace(":", "")
					lineHorario += horarios[1].replace(":", "")
				}
				
				lineHorario += "\r\n"
				
				fileTxt += lineHorario
			}
			
			results.each { r ->
				
				// print detail
				
				sequencialArquivo++
				
				def lineDetail = ""
				
				lineDetail += preencheZeros(sequencialArquivo.toString(), 9)
				lineDetail += "3" // tipo registro
				lineDetail += preencheZeros(somenteNumero(r.pis_func.toString()), 12)
				lineDetail += new SimpleDateFormat("ddMMyyyy").format(r.dt_bat)
				lineDetail += Horarios.retornaHora(r.hr_bat1 ? r.hr_bat1.toLong() : 0l).replace(":", "")
				lineDetail += preencheZeros(r.id_perfil_horario ? r.id_perfil_horario.toString() : "", 4)
				lineDetail += Horarios.retornaHora(r.nr_qtde_ch_prevista ? r.nr_qtde_ch_prevista.toLong(): 0l).replace(":", "")
				lineDetail += Horarios.retornaHora(retornaHorasNoturnas(r.seq_func, r.dt_bat)).replace(":", "")
				lineDetail += retornaHorasExtrasBanco(r.seq_func, r.dt_bat)
				
				lineDetail += "\r\n"
				
				fileTxt += lineDetail
			}
			
			// print trail 
			
			sequencialArquivo++
			
			fileTxt += preencheZeros(sequencialArquivo.toString(), 9)
			fileTxt += "9" // tipo registro
			fileTxt += "\r\n"

			String idFile = new SimpleDateFormat("ddMMyyyyHHmmsss").format(new Date())
			session.setAttribute(idFile, fileTxt)			
			
			render(status: response.SC_OK, text: idFile)
			
		}
		
	}
	
	def retornaHorasNoturnas(func, dtBat) {
		def sql = """
					select m.seq_func, m.dt_bat, m.nr_qtde_hr
					from movimentos m
					inner join funcionario f on m.seq_func = f.id
					inner join param_sistema_filial psf on psf.filial_id = f.filial_id
					inner join param_sistema ps on psf.param_sistema_id = ps.id and m.seq_ocor = ps.ev_adicional_noturno
					where ps.ev_adicional_noturno = 15
				  """
		sql += " and m.seq_func = " + func
		sql += " and m.dt_bat = '" + dtBat + "'"
		
		Sql consulta = new Sql(dataSource)
		def result = consulta.rows(sql)
		
		if (result) {
			result[0].nr_qtde_hr
		} else {
			0
		}
		
	}
	
	def retornaHorasExtrasBanco(func, dtBat) {

		def formatedDate = new SimpleDateFormat("yyyy-MM-dd").format(dtBat)
		
		def linha = ""
		def linha21 = "0000"
		def linhaComp = "N"
		
		def sql = """
					SELECT dt_bat
						,nr_qtde_hr
						,horas_bh
						,tip_ext
						,nm_acjef
						,vr_ind_hor_ext
						,tipo
					FROM (
						SELECT m.dt_bat
							,m.nr_qtde_hr
							,0 AS horas_bh
							,the.tip_ext
							,the.nm_acjef
							,the.vr_ind_hor_ext
							,1 AS tipo
						FROM movimentos m
						INNER JOIN ocorrencias o on m.seq_ocor = m.id
						INNER JOIN tipohoraextras the on o.tipohoraextras_id = the.id
						WHERE m.seq_func = ${func}
							AND m.dt_bat = ${dtBat}
						
						UNION
						
						SELECT obh.dt_bat
							,obh.nr_qtde_hr
							,0 AS horas_bh
							,the.tip_ext
							,the.nm_acjef
							,the.vr_ind_hor_ext
							,2 AS tipo
						FROM ocorrencias_bh obh
						INNER JOIN ocorrencias o on obh.seq_ocor = o.id
						INNER JOIN tipohoraextras the on o.tipohoraextras_id = the.id
						WHERE obh.seq_func = ${func}
							AND obh.dt_bat = ${dtBat}
						
						UNION
						
						SELECT m.dt_bat
							,m.nr_qtde_hr
							,obh.nr_qtde_hr AS horas_bh
							,' ' AS tip_ext
							,'' AS nm_acjef
							,0 AS vr_ind_hor_ext
							,3 AS tipo
						FROM movimentos m
						INNER JOIN funcionario f on m.seq_func = f.id
						INNER JOIN ocorrencias o on m.seq_ocor = o.id
						LEFT JOIN ocorrencias_bh obh on obh.seq_func = m.seq_func
						     AND obh.dt_bat = m.dt_bat
						     AND obh.seq_ocor = o.id
						     AND obh.id_tp_lanc = 'A'
						     AND obh.id_tp_ocorr = 'D'
						LEFT JOIN param_sistema_filial psf on psf.filial_id = f.filial_id
						LEFT JOIN param_sistema psa on psf.param_sistema_id = psa.id
						      AND m.seq_ocor = psa.ev_atraso
						LEFT JOIN param_sistema psft on psf.param_sistema_id = psft.id
						      AND m.seq_ocor = psft.ev_falta_total
						LEFT JOIN param_sistema psf1 on psf.param_sistema_id = psf1.id
						      AND m.seq_ocor = psf1.ev1falta
						WHERE m.seq_func = ${func}
							AND m.dt_bat = ${dtBat}
							AND o.id_tp_ocor = 'D'
						) a
					ORDER BY a.vr_ind_hor_ext,tipo
				  """

		Sql consulta = new Sql(dataSource)
		def result = consulta.rows(sql)

		if (result) {
			
			def linhaExt1 = ""
			def linhaExt2 = ""
			def linhaExt3 = ""
			def linhaExt4 = ""
			def linhaPerc1 = ""
			def linhaPerc2 = ""
			def linhaPerc3 = ""
			def linhaPerc4 = ""
			def linhaTipo1 = ""
			def linhaTipo2 = ""
			def linhaTipo3 = ""
			def linhaTipo4 = ""
			def linhaBH = ""
			
			result.each { r->
				
				if (r.tipo.toString().equals("1")) {
					if (linhaExt1.equals("")) {
						linhaExt1 = Horarios.retornaHora(r.nr_qtde_hr.toLong()).replace(":", "")
						linhaPerc1 = r.nm_acjef
						linhaTipo1 = r.tip_ext
					} else if (linhaExt2.equals("")) {
						linhaExt2 = Horarios.retornaHora(r.nr_qtde_hr.toLong()).replace(":", "")
						linhaPerc2 = r.nm_acjef
						linhaTipo2 = r.tip_ext
					} else if (linhaExt3.equals("")) {
						linhaExt3 = Horarios.retornaHora(r.nr_qtde_hr.toLong()).replace(":", "")
						linhaPerc3 = r.nm_acjef
						linhaTipo3 = r.tip_ext
					} else if (linhaExt4.equals("")) {
						linhaExt4 = Horarios.retornaHora(r.nr_qtde_hr.toLong()).replace(":", "")
						linhaPerc4 = r.nm_acjef
						linhaTipo4 = r.tip_ext
					}    
				} else if (r.tipo.toString().equals("2")) {
					linhaBH = Horarios.retornaHora(r.nr_qtde_hr.toLong()).replace(":", "")
				} else if (r.tipo.toString().equals("3")) {
					if (r.nr_qtde_hr != null && r.nr_qtde_hr.toLong() > 0) {
						linha21 = Horarios.retornaHora(r.nr_qtde_hr.toLong()).replace(":", "")
						linhaComp = "N"
					} else if (r.horas_bh != null && r.horas_bh.toLong() > 0) {
						linha21 = Horarios.retornaHora(r.horas_bh.toLong()).replace(":", "")
						linhaComp = "2"
					}  else {
						linha21 = Horarios.retornaHora(0l).replace(":", "")
						linhaComp = "N"
					}
				}	
			}
			
			if (linhaExt1.equals("")) {
				linhaExt1 = "0000"
				linhaPerc1 = "0000"
				linhaTipo1 = " "
			}
			
			if (linhaExt2.equals("")) {
				linhaExt2 = "0000"
				linhaPerc2 = "0000"
				linhaTipo2 = " "
			}
			
			if (linhaExt3.equals("")) {
				linhaExt3 = "0000"
				linhaPerc3 = "0000"
				linhaTipo3 = " "
			}
			
			if (linhaExt4.equals("")) {
				linhaExt4 = "0000"
				linhaPerc4 = "0000"
				linhaTipo4 = " "
			}
			
			linha = linhaExt1 + linhaPerc1 + linhaTipo1 + linhaExt2 + linhaPerc2 + linhaTipo2 + linhaExt3 + linhaPerc3 + linhaTipo3 + linhaExt4 + linhaPerc4 + linhaTipo4
			
			if (!linhaBH.equals("")) {
				linha += "0000" + "1" + linhaBH
			} else if (linhaComp.equals("N")) {
				linha += linha21 + "0" + "0000"
			} else {
				linha += "0000" + "2" + linha21
			}
			
		} else {
			if (linhaComp.equals("N")) {
				linha = "00000000 00000000 00000000 00000000 " + linha21 + "0" + "0000"
			} else {
				linha = "00000000 00000000 00000000 00000000 " + "0000" + "2" + linha21
			}
		}
		
		return linha

	}
	
	def baixarArquivo() {
		String arquivo = session.getAttribute(params.baixarArquivoId)
		session.removeAttribute(params.baixarArquivoId)
		
		//get the output stream from the common property 'response'
		ServletOutputStream servletOutputStream = response.outputStream
 
		//set the byte content on the response. This determines what is shown in your browser window.
		response.setContentType("text/plain")
		response.setContentLength(arquivo.length())
	 
		response.setHeader('Content-disposition', 'attachment; filename='.concat("ACJEF").concat(".txt"))
		response.setHeader('Expires', '0');
		response.setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
		servletOutputStream << arquivo.getBytes()
		servletOutputStream.flush()
	}
	
	public String somenteNumero(String valor) {
		String retorno = "";
		for (int i = 0; i < valor.length(); i++) {
			if (valor.charAt(i).isDigit()) {
				retorno += valor.charAt(i)
			}
		}
		return retorno;
	}
	
	public String preencheEspacos(String txt, Integer quantidade) {
		String retorno = ""; 
		int length = 0
		if (txt != null && !txt.isEmpty() && !txt.equals("null")) {
			retorno = retorno.concat(txt)
			length = retorno.length() 
		} 
		for (int i = 0; i < quantidade - length; i++) {
			retorno = retorno.concat(" ")
		}
		return retorno;
	}
	
	public String preencheZeros(String txt, Integer quantidade) {
		String retorno = "";
		int length = 0
		if (txt != null && !txt.isEmpty() && !txt.equals("null")) {
			retorno = retorno.concat(txt)
			length = retorno.length()
		}
		for (int i = 0; i < quantidade - length; i++) {
			retorno = "0".concat(retorno)
		}
		return retorno;
	}
	
	
	
}