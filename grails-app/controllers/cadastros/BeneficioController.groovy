package cadastros

import grails.converters.JSON
import groovy.sql.Sql

import java.text.SimpleDateFormat

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional


@Transactional
class BeneficioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def dataSource

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		
		
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def beneficioList = new ArrayList<Beneficio>()
		int beneficioCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			
			order = params.order ? params.order : "id"
			sortType = params.sortType ? params.sortType : "desc"
			def orderBy = "order by " + order + " " + sortType
			
			if (tipoFiltro > 0) {
				
				switch (tipoFiltro) {
					case 1:
						beneficioList.addAll(
							Beneficio.findAll(
								"from Beneficio c where upper(c.descricao) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						beneficioCount = Beneficio.findAll(
								"from Beneficio c where upper(c.descricao) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					case 2:
						beneficioList.addAll(
							Beneficio.findAll(
								"from Beneficio c where upper(c.tipoDeBeneficio.descricao) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						beneficioCount = Beneficio.findAll(
								"from Beneficio c where upper(c.tipoDeBeneficio.descricao) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					default:
						break;
				}
			} else {
				beneficioList.addAll(
					Beneficio.findAll(
						"from Beneficio c " + orderBy,
						[max: params.max, offset: params.offset]))
				beneficioCount = Beneficio.findAll(
						"from Beneficio c " + orderBy).size()
			}
		}
		
		[beneficioInstanceList: beneficioList, beneficioInstanceTotal: beneficioCount, order: order, sortType: sortType]
		
		
//        params.max = Math.min(params.max ? params.int('max') : 20, 100)
//        [beneficioInstanceList: Beneficio.list(params), beneficioInstanceTotal: Beneficio.count()]
    }

    def delete() {
        def beneficioInstance = Beneficio.get(params.id)
        if (!beneficioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Beneficio', beneficioInstance.id])
        } else {
            try {
                beneficioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Beneficio'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Beneficio'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [beneficioInstance: new Beneficio()]
    }

    def edit() {
        def beneficioInstance = Beneficio.get(params.id)
        if (!beneficioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Beneficio', beneficioInstance.id])
            redirect(action: "list")
            return
        }

		List<ValorBeneficio> valores = ValorBeneficio.findAll('from ValorBeneficio where beneficio=:beneficio order by sequencia', [beneficio: beneficioInstance])
        [beneficioInstance: beneficioInstance, 
		 quantidadeRegistros: beneficioInstance.valores.size()-1, 
		 sequencia: (beneficioInstance.valores) ? beneficioInstance.valores.size()+1 : 0, 
		 valores: valores ]
    }

	@Transactional
    def save() {
        def beneficioInstance = new Beneficio(params)
		
		beneficioInstance.tipoDeBeneficio = TipoDeBeneficio.get(params.tipoDeBeneficioId)
		beneficioInstance.valores = new ArrayList<ValorBeneficio>()
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idvben.getClass()) }
		
		if (isArray){
			
			for (x in 0..params.idvben.size()-1){

				if (params.seqval[x] != null && params.seqval[x] != ""
				&& params.idvben[x] != null && params.idvben[x] != ""
				&& params.dtInicio[x] != null && params.dtInicio[x] != "") {
					
					def valor = new ValorBeneficio(
						beneficio: beneficioInstance,
						sequencia: params.seqval[x],
						valor: params.idvben[x],
						dataInicio: new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio[x]),
						dataFim: params.dtFim[x] != "" ? new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim[x]) : null)
					beneficioInstance.valores.add(valor)
					
				}
				
			}

		} else {
			
			if (params.seqval != null && params.seqval != ""
				&& params.idvben != null && params.idvben != ""
				&& params.dtInicio != null && params.dtInicio != "") {
				
				def valor = new ValorBeneficio(
					beneficio: beneficioInstance,
					sequencia: params.seqval,
					valor: params.idvben,
					dataInicio: new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio),
					dataFim: params.dtFim != "" ? new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim) : null)
				beneficioInstance.valores.add(valor)
				
			}
			
		}
		
		if (beneficioInstance.valores.isEmpty()) {
			flash.message = "Devem ser informados valores pro beneficio"
			render(view: "create", model: [beneficioInstance: beneficioInstance])
			return
		}
		
		configuraFiliais(beneficioInstance)
		
		if (!beneficioInstance.save(flush: true)) {
            render(view: "create", model: [beneficioInstance: beneficioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Beneficio'])
        redirect(action: "list")
    }

	@Transactional
    def update() {
        def beneficioInstance = Beneficio.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (beneficioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Beneficio'])
                render(view: "edit", model: [beneficioInstance: beneficioInstance])
                return
            }
        }
		
		def valores = ValorBeneficio.findAll ('from ValorBeneficio where beneficio=:beneficio',[beneficio: beneficioInstance])
		valores.each() {
			ValorBeneficio valor = ValorBeneficio.get(it.id)
			beneficioInstance.valores.remove(valor)
			valor.delete()
		}

        beneficioInstance.properties = params
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.idvben.getClass()) }
		
		if (isArray){
			
			for (x in 0..params.idvben.size()-1){

				if (params.seqval[x] != null && params.seqval[x] != ""
				&& params.idvben[x] != null && params.idvben[x] != ""
				&& params.dtInicio[x] != null && params.dtInicio[x] != "") {
					
					def valor = new ValorBeneficio(
						beneficio: beneficioInstance,
						sequencia: params.seqval[x],
						valor: params.idvben[x],
						dataInicio: new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio[x]),
						dataFim: params.dtFim[x] != "" ? new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim[x]) : null)
					beneficioInstance.valores.add(valor)
					
				}
				
			}

		} else {
			
			if (params.seqval != null && params.seqval != ""
				&& params.idvben != null && params.idvben != ""
				&& params.dtInicio != null && params.dtInicio != "") {
				
				def valor = new ValorBeneficio(
					beneficio: beneficioInstance,
					sequencia: params.seqval,
					valor: params.idvben,
					dataInicio: new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio),
					dataFim: params.dtFim != "" ? new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim) : null)
				beneficioInstance.valores.add(valor)
				
			}
			
		}
		
		if (beneficioInstance.valores.isEmpty()) {
			flash.message = "Devem ser informados valores pro beneficio"
			render(view: "create", model: [beneficioInstance: beneficioInstance])
			return
		}
		
		configuraFiliais(beneficioInstance)
		
		if (!beneficioInstance.save(flush: true)) {
            render(view: "edit", model: [beneficioInstance: beneficioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Beneficio'])
        redirect(action: "list")
    }
	
	def configuraFiliais(Beneficio beneficioInstance) {
		
		if (beneficioInstance.id != null) {
			BeneficioFilial.findAll('from BeneficioFilial where beneficio = :b', [b: beneficioInstance]).each { pr ->
				pr.delete(flush: true)
			}
		}
		
		def isArray = [Collection, Object[]].any { it.isAssignableFrom(params.filial.getClass()) }
		def filiaisArray = isArray ? params.filial : [params.filial]
		
		filiaisArray.eachWithIndex { f, i ->
			if (f != null) {
				BeneficioFilial bf = new BeneficioFilial()
				bf.filial = Filial.get(filiaisArray[i].toLong())
				bf.beneficio = beneficioInstance
				
				if (beneficioInstance.filiais == null) {
					beneficioInstance.filiais = new ArrayList<BeneficioFilial>()
				}
				
				beneficioInstance.filiais.add(bf)
			}
		}
	}
	
	def consultaPorTipoBeneficio() {
		TipoDeBeneficio tipoDeBeneficio = TipoDeBeneficio.get(params.tipoDeBeneficio.toLong())
		def beneficios = Beneficio.findAllByTipoDeBeneficio(tipoDeBeneficio)
		
		def result = []
		beneficios.each {
			result.add([id: it.id, nome: it.descricao])
		}
		
		render result as JSON
	}
	
	def consultaDatasProcessamento() {
		
		def benefParam = "0"
		if (params.beneficios) {
			benefParam = params.beneficios
		}
		
		def result = []
		
		def consulta  = "select distinct "
			consulta += "to_char(dt_inicio, 'yyyy-MM-dd') || '#' ||  to_char(dt_fim, 'yyyy-MM-dd') as id, "
			consulta += "to_char(dt_inicio, 'dd/MM/yyyy') || ' a ' ||  to_char(dt_fim, 'dd/MM/yyyy') as descricao "
		    consulta += "from beneficio_movimentos "
			consulta += "where beneficio_id in (" + benefParam + ") "
		
			
		Sql sql = new Sql(dataSource)
		def resultsSql = []
		
		try {
			resultsSql = sql.rows(consulta)
		} catch(Exception e) {
			e.printStackTrace()
		}
		
		resultsSql.each {
			result.add([id: it[0], data: it[1]])
		}
			
		render result as JSON
	}
	
}