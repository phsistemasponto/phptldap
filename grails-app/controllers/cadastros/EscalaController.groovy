package cadastros

import grails.converters.JSON

import java.math.*
import java.text.*

import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.apache.commons.codec.binary.Base64
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.context.ApplicationContext
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.transaction.annotation.Transactional

import acesso.Usuario
import acesso.UsuarioGrupoFuncionario


@Transactional
class EscalaController {

	def springSecurityService
	def relatoriosService
	def jasperService
	def filtroPadraoService
	def dataSource
	def detectDataBaseService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		Set<EscalaHorarios> escalas = new HashSet<EscalaHorarios>()
		
		Usuario usuario = springSecurityService.currentUser
		escalas.addAll(EscalaHorarios.findAllByUsuario(usuario))
		
		List<UsuarioGrupoFuncionario> grupos = usuario.getUsuarioGrupos()
		if (grupos) {
			List<Grupo> gruposFuncionarios = new ArrayList<Grupo>()
			grupos.each {
				gruposFuncionarios.add(it.grupoFuncionario)
			}
			
			gruposFuncionarios.each { gf ->
				List<UsuarioGrupoFuncionario> outrosGrupos = UsuarioGrupoFuncionario.findAllByGrupoFuncionario(gf)
				List<Usuario> usuariosNoMesmoGrupo = outrosGrupos.collectAll { og -> og.usuario }
				usuariosNoMesmoGrupo.each { u ->
					escalas.addAll(EscalaHorarios.findAllByUsuario(u))
				}
			}  
			
		}
		
		[escalasHorarios: escalas]
	}
	
	def buscaDataEscala() {
		def resultadoMap = []
		
		def dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
		def escala = EscalaData.findByDtInicio(dataInicio)
		
		if (escala) {
			def escalaId = escala.id
			def escalaDataFim = new SimpleDateFormat("dd/MM/yyyy").format(escala.dtFim)
			def escalaDescricao = escala.legendaMes
			def escalaStatus = escala.status.equals("A") ? "ABERTO" : "FECHADO"
			resultadoMap.add([escalaId: escalaId, escalaDataFim: escalaDataFim, escalaDescricao: escalaDescricao, status: escalaStatus])
		}
	
		if (resultadoMap) {
			render resultadoMap as JSON
		} else {
			[]
		}
	}
	
	def lancarEscala() {
		
		// salva ou atualiza escala
		def escala = new EscalaData()
		if (params.escalaDataId) {
			escala = EscalaData.get(params.escalaDataId.toLong())
		} else {
			escala.dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataInicio)
			escala.status = 'A'
		}
		
		escala.dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataFim)
		escala.legendaMes = params.descricao
		escala.save(flush: true)

		// manuten��o de perfil hor�rios
		def isArrayEscHorarios = [Collection, Object[]].any { it.isAssignableFrom(params.escId.getClass()) }
		def escHorariosArray = isArrayEscHorarios ? params.escId : [params.escId]
		
		def isArrayPerfHorarios = [Collection, Object[]].any { it.isAssignableFrom(params.escIdHorario.getClass()) }
		def escPerfilHorariosArray = isArrayPerfHorarios ? params.escIdHorario : [params.escIdHorario]
		
		def isArrayLetraLegenda = [Collection, Object[]].any { it.isAssignableFrom(params.escLetra.getClass()) }
		def escLetraLegendaArray = isArrayLetraLegenda ? params.escLetra : [params.escLetra]
		
		def usuario = springSecurityService.currentUser
		def escalasHorariosSalvas = EscalaHorarios.findAllByUsuario(usuario)
		def escalasHorariosIdsSalvas = escalasHorariosSalvas.collect { it.id }
		
		def escalaHorariosDeletados = escalasHorariosIdsSalvas.findAll { !escHorariosArray.contains(it.toString()) } 
		escalaHorariosDeletados.each {
			EscalaHorarios eh = EscalaHorarios.get(it.toLong())
			eh.delete(flush: true)
		}
		
		escPerfilHorariosArray.eachWithIndex { p, i ->
			Perfilhorario ph = Perfilhorario.get(p.toLong())
			
			EscalaHorarios esc = EscalaHorarios.findByUsuarioAndPerfilhorario(usuario, ph)
			if (esc == null) {
				esc = new EscalaHorarios()
				esc.perfilhorario = ph
				esc.usuario = usuario
			}
			esc.idLetra = escLetraLegendaArray[i]
			esc.save(flush: true)
		}
		
		
		def diasLancamentos = []
		Date dataTemp = escala.dtInicio
		while (dataTemp.before(escala.dtFim) || dataTemp.equals(escala.dtFim)) {
			diasLancamentos.add([dtObj: dataTemp,
				data: new SimpleDateFormat("dd").format(dataTemp), 
				dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(dataTemp)])
			dataTemp = dataTemp.plus(1)
		}
		
		def dadosLancamentos = []
		def grupos = UsuarioGrupoFuncionario.findAllByUsuario(usuario).collect { it.grupoFuncionario }
		grupos.each { g->
			def funcionarios = GrupoFuncionario.findAllByGrupo(g).collect { gf -> gf.funcionario }
			def escalaItensDiarios = EscalaItensDiario.findAll("from EscalaItensDiario i where i.funcionario in (:funcionarios) and i.dtBat between :dtInicio and :dtFim",
				[funcionarios: funcionarios, dtInicio: escala.dtInicio, dtFim: escala.dtFim])
			
			funcionarios.each { f ->
				def lancamentosEscalas = []
				diasLancamentos.each { d ->""
					def escalaItem = escalaItensDiarios.findAll { e -> e.dtBat.equals(d.dtObj) && e.funcionario.equals(f) }
					def siglaEscala = escalaItem ? escalaItem.escalaHorarios.idLetra : "" 
					siglaEscala = siglaEscala.toString().replace("[", "").replace("]", "").replace(", ", " ").trim()
					lancamentosEscalas.add([siglaEscala: siglaEscala, data: new SimpleDateFormat("ddMMyyyy").format(d.dtObj)])
				}
				dadosLancamentos.add([funcionarioId: f.id, 
					funcionarioCracha: f.crcFunc, 
					funcionarioNome: f.nomFunc, 
					grupo: g.dscGrupo,
					lancamentos: lancamentosEscalas])
			}
			
		}
		
		[horarios: EscalaHorarios.findAllByUsuario(usuario), 
			diasLancamentos: diasLancamentos, 
			dadosLancamentos: dadosLancamentos,
			statusEscala: escala.status]
	}
	
	def aplicarLancamentoEscala() {
		
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)
		
		def funcionarios = []
		def usuario = springSecurityService.currentUser
		def grupos = UsuarioGrupoFuncionario.findAllByUsuario(usuario).collect { it.grupoFuncionario }
		grupos.each { g ->
			funcionarios.addAll(GrupoFuncionario.findAllByGrupo(g).collect { gf -> gf.funcionario })
		}
		
		def classificacao = Classificacao.findByIdTpDia("TR")
		def horarios = EscalaHorarios.findAllByUsuario(usuario)
		
		EscalaItensDiario.executeUpdate("delete EscalaItensDiario i where i.funcionario in (:funcionarios) and i.dtBat between :dtInicio and :dtFim ",
			[funcionarios: funcionarios, dtInicio: dtInicio, dtFim: dtFim])
		
		ItensDiario.executeUpdate("delete ItensDiario i where i.funcionario in (:funcionarios) and i.dtBat between :dtInicio and :dtFim ",
			[funcionarios: funcionarios, dtInicio: dtInicio, dtFim: dtFim])
		
//		def escalaItensDiarios = EscalaItensDiario.findAll("from EscalaItensDiario i where i.funcionario in (:funcionarios) and i.dtBat between :dtInicio and :dtFim ",
//			[funcionarios: funcionarios, dtInicio: dtInicio, dtFim: dtFim])
		
		Map<Funcionario, Map<Date, List<EscalaHorarios>>> mapeamento = new HashMap<Funcionario, Map<Date, List<EscalaHorarios>>>()
		
		params.each { p ->
			if (p.key.toString().startsWith("hiddenLancDia-")) {
				
				def campos = p.key.toString().split("-")
				Funcionario func = Funcionario.get(campos[1].toLong())
				Date data = new SimpleDateFormat("ddMMyyyy").parse(campos[2])
				def letras = p.value.toString()
				def letrasArray = letras.split(" ")
				
				if (letrasArray) {
					
					Map<Date, List<EscalaHorarios>> mapeamentoFuncionario = mapeamento.get(func)
					if (mapeamentoFuncionario == null) {
						mapeamentoFuncionario = new HashMap<Date, List<EscalaHorarios>>()
						mapeamento.put(func, mapeamentoFuncionario)
					}
					
					List<EscalaHorarios> escalas = mapeamentoFuncionario.get(data)
					if (escalas == null) {
						escalas = new ArrayList<EscalaHorarios>()
						mapeamentoFuncionario.put(data, escalas)
					}
					
					letrasArray.each { letra ->
						
						EscalaHorarios escala = horarios.find { h -> h.idLetra.equals(letra) }
						escalas.add(escala)
						
					}
					
				}
				
			}
		}
		
		mapeamento.each { func, map ->
			
			map.each { data, escalas ->
				
				escalas.each { esc -> 
					
					if (esc != null) {
					
						EscalaItensDiario newRegistroEscala = new EscalaItensDiario()
						newRegistroEscala.funcionario = func
						newRegistroEscala.escalaHorarios = esc
						newRegistroEscala.dtBat = data
						newRegistroEscala.status = "A"
						newRegistroEscala.save(flush: true)
						
						ItensDiario newItensDiarios = new ItensDiario()
						newItensDiarios.funcionario = func
						newItensDiarios.dtBat = data
						newItensDiarios.classificacao = classificacao
						newItensDiarios.perfilHorario = newRegistroEscala.escalaHorarios.perfilhorario
						newItensDiarios.qtdeChEfetiva = 0
						newItensDiarios.save(flush: true)
						
					}
					
				}
				
			}
			
		}
		
		funcionarios.each { f ->
			def seqEmpresa = f.filial.empresa.id
			def seqFilial = f.filial.id
			def seqFuncionario = f.id
	
			def template = new JdbcTemplate(dataSource)
	
			if (detectDataBaseService.isPostgreSql()) {
				def valor = template.queryForObject("select pr_processar(${seqEmpresa}::integer, ${seqFilial}::integer, ${seqFuncionario}::integer, '${dtInicio}'::date, '${dtFim}'::date);", Integer.class)
			} else if (detectDataBaseService.isOracle()) {
				// TODO chamar
			}
		}
		
		redirect(action: "list", params: params)
	}

	def encerrarPeriodo() {
	
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)
		
		def escala = EscalaData.findByDtInicio(dtInicio)
		escala.dtFechamento = new Date()
		escala.status = "F"
		escala.save(flush: true)
		
		def funcionarios = []
		def usuario = springSecurityService.currentUser
		def grupos = UsuarioGrupoFuncionario.findAllByUsuario(usuario).collect { it.grupoFuncionario }
		grupos.each { g ->
			funcionarios.addAll(GrupoFuncionario.findAllByGrupo(g).collect { gf -> gf.funcionario })
		}
		
		def escalaItensDiarios = EscalaItensDiario.findAll("from EscalaItensDiario i where i.funcionario in (:funcionarios) and i.dtBat between :dtInicio and :dtFim",
			[funcionarios: funcionarios, dtInicio: dtInicio, dtFim: dtFim])
		
		escalaItensDiarios.each { e ->
			e.status = "F"
			e.save(flush: true)
		}
		
		redirect(action: "list", params: params)
	}
	
	def enviarEmail() {
		
		def funcionarios = []
		def usuario = springSecurityService.currentUser
		def grupos = UsuarioGrupoFuncionario.findAllByUsuario(usuario).collect { it.grupoFuncionario }
		grupos.each { g ->
			funcionarios.addAll(GrupoFuncionario.findAllByGrupo(g).collect { gf -> gf.funcionario })
		}
		
		funcionarios.each { f ->
			
			Filial filial = f.filial
			Empresa empresa = filial.empresa
			
			Properties properties = System.getProperties()
			properties.setProperty("mail.smtp.host", empresa.smtpServidor)
			properties.setProperty("mail.smtp.port", empresa.smtpPorta.toString())
			properties.setProperty("mail.smtp.user", empresa.smtpUsuario)
			properties.setProperty("mail.smtp.password", empresa.smtpSenha);
			
			Session session = Session.getDefaultInstance(properties)
			
			try {
				MimeMessage message = new MimeMessage(session)
				message.setFrom(new InternetAddress(empresa.emailSmtp))
				message.addRecipient(Message.RecipientType.TO,new InternetAddress(f.emlFunc))
				message.setSubject("Per�odo encerrado!")
				message.setText("Periodo encerrado de ${params.dtInicio} a ${params.dtFim} finalizado.");

				Transport.send(message)
			} catch( MessagingException mex ) {
				mex.printStackTrace()
			}
		}
		
	}
	
	def generateReport() {
		
		def results = generateResult()
		
		if (results.isEmpty()) {
			render(status: response.SC_BAD_REQUEST)
		}
		
		def format = JasperExportFormat.PDF_FORMAT
		def contentType = format.mimeTyp
		def fileName = 'report.'.concat(format.extension)
		def reportName = "relatorioEscalas"
		def parameters = generateParameters()
		
		JasperReportDef reportDef = new JasperReportDef(
			name: reportName,
			fileFormat: format,
			reportData: results,
			parameters: parameters
		)

		ByteArrayOutputStream saida = new ByteArrayOutputStream();
		
		JasperPrint printer = jasperService.generatePrinter(reportDef);
		JRExporter exp = jasperService.generateExporter(reportDef);
		
		exp.setParameter(JRXlsExporterParameter.JASPER_PRINT, printer);
		exp.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		exp.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, true);
		exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
		exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		exp.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, saida);
		exp.exportReport();
		
		def binaryBase64 = new String(Base64.encodeBase64(saida.toByteArray()))
		def reportData = [reportData: binaryBase64, format: "PDF"]
		
		session.setAttribute("reportData", binaryBase64)
		session.setAttribute("reportFormat", "PDF")
		
		render reportData as JSON
	}
	
	def generateResult() {
		
		def results = []
		
		def escala = EscalaData.get(params.escalaDataId.toLong())
		def dtInicio = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtInicio)
		def dtFim = new SimpleDateFormat("dd/MM/yyyy").parse(params.dtFim)
		
		def diasLancamentos = []
		Date dataTemp = escala.dtInicio
		while (dataTemp.before(escala.dtFim) || dataTemp.equals(escala.dtFim)) {
			diasLancamentos.add([dtObj: dataTemp,
				data: new SimpleDateFormat("dd").format(dataTemp),
				dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(dataTemp)])
			dataTemp = dataTemp.plus(1)
		}
		
		def usuario = springSecurityService.currentUser
		def grupos = UsuarioGrupoFuncionario.findAllByUsuario(usuario).collect { it.grupoFuncionario }
		grupos.each { g ->
			
			def nomeGrupo = g.dscGrupo.concat(" - ").concat(escala.legendaMes)
			
			
			def funcionarios = GrupoFuncionario.findAllByGrupo(g).collect { gf -> gf.funcionario }
			def escalaItensDiarios = EscalaItensDiario.findAll("from EscalaItensDiario i where i.funcionario in (:funcionarios) and i.dtBat between :dtInicio and :dtFim",
				[funcionarios: funcionarios, dtInicio: escala.dtInicio, dtFim: escala.dtFim])
			
			funcionarios.each { f ->
				def lancamentosEscalas = []
				diasLancamentos.each { d ->
					
					def escalaItem = escalaItensDiarios.findAll { e -> e.dtBat.equals(d.dtObj) && e.funcionario.equals(f) }
					def siglaEscala = escalaItem ? escalaItem.escalaHorarios.idLetra : ""
					siglaEscala = siglaEscala.toString().replace("[", "").replace("]", "").replace(", ", " ").trim()
					lancamentosEscalas.add([siglaEscala: siglaEscala, 
						 						   data: new SimpleDateFormat("dd").format(d.dtObj),
													dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(d.dtObj)])
				}
				
				def dataSourceLancamentos = new JRBeanCollectionDataSource(lancamentosEscalas)
				
				def mapFunc = [nomeGrupo: nomeGrupo, 
					     nomeFuncionario: f.nomFunc, 
				    matriculaFuncionario: f.mtrFunc, 
					  lancamentosEscalas: dataSourceLancamentos]
				
				results.add(mapFunc)
			}
			
			
		}
		
		results
	}
	
	def generateParameters() {
		
		Usuario usuario = session['usuario']
		ApplicationContext appContext = grailsAttributes.getApplicationContext()
		String baseFolder = appContext.getResource("/").getFile().toString()
		
		def escala = EscalaData.get(params.escalaDataId.toLong())
		def diasLancamentos = []
		Date dataTemp = escala.dtInicio
		while (dataTemp.before(escala.dtFim) || dataTemp.equals(escala.dtFim)) {
			diasLancamentos.add([dtObj: dataTemp,
				data: new SimpleDateFormat("dd").format(dataTemp),
				dia: new SimpleDateFormat("EEE", new Locale("pt", "BR")).format(dataTemp)])
			dataTemp = dataTemp.plus(1)
		}
		
		def escalas = []
		def escalasHorariosSalvas = EscalaHorarios.findAllByUsuario(usuario)
		escalasHorariosSalvas.each { e->
			escalas.add([sigla: e.idLetra, horario: e.perfilhorario.dscperfil])
		}
		
		def parameters = [
			DT_INICIO: params.dtInicio,
			DT_FIM: params.dtFim,
			LOGOMARCA: relatoriosService.montaLogomarca(usuario, appContext),
			DIAS_LANCAMENTO: new JRBeanCollectionDataSource(diasLancamentos),
			ESCALAS: new JRBeanCollectionDataSource(escalas),
			SUBREPORT_DIR: baseFolder.concat(File.separator).concat("reports").concat(File.separator)
		]
		parameters
	}
	

}