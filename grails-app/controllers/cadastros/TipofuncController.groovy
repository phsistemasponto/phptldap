package cadastros

import javax.persistence.Transient;

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;

@Transactional
class TipofuncController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [tipofuncInstanceList: Tipofunc.list(params), tipofuncInstanceTotal: Tipofunc.count()]
    }

    def delete() {
        def tipofuncInstance = Tipofunc.get(params.id)
        if (!tipofuncInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Tipofunc', tipofuncInstance.id])
        } else {
            try {
                tipofuncInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Tipofunc'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Tipofunc'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [tipofuncInstance: new Tipofunc()]
    }

    def edit() {
        def tipofuncInstance = Tipofunc.get(params.id)
        if (!tipofuncInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Tipofunc', tipofuncInstance.id])
            redirect(action: "list")
            return
        }

        [tipofuncInstance: tipofuncInstance]
    }

    def save() {
        def tipofuncInstance = new Tipofunc(params)
        if (!tipofuncInstance.save(flush: true)) {
            render(view: "create", model: [tipofuncInstance: tipofuncInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Tipofunc'])
        redirect(action: "create")
    }

    def update() {
        def tipofuncInstance = Tipofunc.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (tipofuncInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Tipofunc'])
                render(view: "edit", model: [tipofuncInstance: tipofuncInstance])
                return
            }
        }

        tipofuncInstance.properties = params

        if (!tipofuncInstance.save(flush: true)) {
            render(view: "edit", model: [tipofuncInstance: tipofuncInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Tipofunc'])
        redirect(action: "list")
    }
}