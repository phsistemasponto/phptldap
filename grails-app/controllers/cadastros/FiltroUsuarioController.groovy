package cadastros

import org.springframework.dao.DataIntegrityViolationException

class FiltroUsuarioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [filtroUsuarioInstanceList: FiltroUsuario.list(params), filtroUsuarioInstanceTotal: FiltroUsuario.count()]
    }

    def delete() {
        def filtroUsuarioInstance = FiltroUsuario.get(params.id)
        if (!filtroUsuarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FiltroUsuario', filtroUsuarioInstance.id])
        } else {
            try {
                filtroUsuarioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['FiltroUsuario'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['FiltroUsuario'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [filtroUsuarioInstance: new FiltroUsuario()]
    }

    def edit() {
        def filtroUsuarioInstance = FiltroUsuario.get(params.id)
        if (!filtroUsuarioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['FiltroUsuario', filtroUsuarioInstance.id])
            redirect(action: "list")
            return
        }

        [filtroUsuarioInstance: filtroUsuarioInstance]
    }

    def save() {
		def usuario = session['usuario']
		def filtroUsuarioInstance
		
		if (params.filtroId) {
			filtroUsuarioInstance = FiltroUsuario.get(params.filtroId.toLong())
			filtroUsuarioInstance.properties = params
		} else {
			filtroUsuarioInstance = new FiltroUsuario(params)
		}
		
		filtroUsuarioInstance.usuario = usuario
		
		if (!filtroUsuarioInstance.save(flush: true)) {
			render(view: "create", model: [filtroUsuarioInstance: filtroUsuarioInstance])
			return
		}
		
		render(status: response.SC_OK, text: filtroUsuarioInstance.id)
		
    }

    def update() {
        def filtroUsuarioInstance = FiltroUsuario.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (filtroUsuarioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['FiltroUsuario'])
                render(view: "edit", model: [filtroUsuarioInstance: filtroUsuarioInstance])
                return
            }
        }

        filtroUsuarioInstance.properties = params

        if (!filtroUsuarioInstance.save(flush: true)) {
            render(view: "edit", model: [filtroUsuarioInstance: filtroUsuarioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['FiltroUsuario'])
        redirect(action: "list")
    }
}