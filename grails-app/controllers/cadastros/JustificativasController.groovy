package cadastros

import grails.converters.JSON

import org.springframework.dao.DataIntegrityViolationException

class JustificativasController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def justificativasList = new ArrayList<Justificativas>()
		int justificativasCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			if (tipoFiltro > 0) {
				order = params.order ? params.order : "dscJust"
				sortType = params.sortType ? params.sortType : "desc"
				def orderBy = "order by " + order + " " + sortType
				switch (tipoFiltro) {
					case 1:
						justificativasList.addAll(
							Justificativas.findAll(
								"from Justificativas c where upper(c.dscJust) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						justificativasCount = Justificativas.findAll(
								"from Justificativas c where upper(c.dscJust) like :nome "  + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					case 2:
						justificativasList.addAll(
							Justificativas.findAll(
								"from Justificativas c where upper(c.idTpJust) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						justificativasCount = Justificativas.findAll(
								"from Justificativas c where upper(c.idTpJust) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					default:
						break;
				}
			} else {
				flash.message = "Selecione um tipo de filtro"
			}
		}
		
		[justificativasInstanceList: justificativasList, justificativasInstanceTotal: justificativasCount, order: order, sortType: sortType]
    }

    def delete() {
        def justificativasInstance = Justificativas.get(params.id)
        if (!justificativasInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Justificativas', justificativasInstance.id])
        } else {
            try {
                justificativasInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Justificativas'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Justificativas'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [justificativasInstance: new Justificativas()]
    }

    def edit() {
        def justificativasInstance = Justificativas.get(params.id)
        if (!justificativasInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Justificativas', justificativasInstance.id])
            redirect(action: "list")
            return
        }

        [justificativasInstance: justificativasInstance]
    }

    def save() {
        def justificativasInstance = new Justificativas(params)
		
		if (params.idLimpaDesc){
			justificativasInstance.idLimpaDesc='S'
		} else {
			justificativasInstance.idLimpaDesc='N'
		}

		if (params.idCidsn){
			justificativasInstance.idCidsn='S'
		} else {
			justificativasInstance.idCidsn='N'
		}

		if (params.idNewMatricula){
			justificativasInstance.idNewMatricula='S'
		} else {
			justificativasInstance.idNewMatricula='N'
		}

		if (params.idAdcionalnot){
			justificativasInstance.idAdcionalnot='S'
		} else {
			justificativasInstance.idAdcionalnot='N'
		}

		if (params.idDebitaBh){
			justificativasInstance.idDebitaBh='S'
		} else {
			justificativasInstance.idDebitaBh='N'
		}
	
		if (params.idSuspensao){
			justificativasInstance.idSuspensao='S'
		} else {
			justificativasInstance.idSuspensao='N'
		}

        if (!justificativasInstance.save(flush: true)) {
            render(view: "create", model: [justificativasInstance: justificativasInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Justificativas'])
        redirect(action: "create")
    }

    def update() {
        def justificativasInstance = Justificativas.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (justificativasInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Justificativas'])
                render(view: "edit", model: [justificativasInstance: justificativasInstance])
                return
            }
        }

        justificativasInstance.properties = params
		if (params.idLimpaDesc){
			justificativasInstance.idLimpaDesc='S'
		} else {
			justificativasInstance.idLimpaDesc='N'
		}

		if (params.idCidsn){
			justificativasInstance.idCidsn='S'
		} else {
			justificativasInstance.idCidsn='N'
		}

		if (params.idNewMatricula){
			justificativasInstance.idNewMatricula='S'
		} else {
			justificativasInstance.idNewMatricula='N'
		}

		if (params.idAdcionalnot){
			justificativasInstance.idAdcionalnot='S'
		} else {
			justificativasInstance.idAdcionalnot='N'
		}

		if (params.idDebitaBh){
			justificativasInstance.idDebitaBh='S'
		} else {
			justificativasInstance.idDebitaBh='N'
		}

		if (params.idSuspensao){
			justificativasInstance.idSuspensao='S'
		} else {
			justificativasInstance.idSuspensao='N'
		}


        if (!justificativasInstance.save(flush: true)) {
            render(view: "edit", model: [justificativasInstance: justificativasInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Justificativas'])
        redirect(action: "list")
    }
	
	def lovResultadoJustificativaEspelhoPonto = {

		def justificativa = Justificativas.findAll("from Justificativas j where j.idTpJust = :tipoJustificativa and j.dscJust like :nome ", 
			[nome: (params.stringBusca + "%"), tipoJustificativa: params.tipoJustificativa]) 
		def resultado = []
		
		if (justificativa) {
			justificativa.each { j ->  
				resultado.add([id: j.id, nome: j.dscJust, exigeCid: j.idCidsn])
			}
		}
		
		render resultado as JSON
	}

	def buscaIdJustificativaEspelhoPonto = {
		def justificativa = Justificativas.find("from Justificativas j where j.idTpJust = :tipoJustificativa  and j.id = :codigo ", 
			[codigo: params.codigo.toLong(), tipoJustificativa: params.tipoJustificativa]) 
		def resultado = []
		
		if (justificativa) {
			resultado = [id: justificativa.id, nome: justificativa.dscJust, exigeCid: justificativa.idCidsn]
		}
		
		render resultado as JSON
	}
	
	
}