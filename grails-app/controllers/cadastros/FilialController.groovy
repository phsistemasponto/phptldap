
package cadastros
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional

@Transactional
class FilialController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		params.offset = params.offset ? params.int('offset') : 0
		
		def filialList = new ArrayList<Filial>()
		int filialCount = 0
		def order = ""
		def sortType = ""
		
		if (params.tipoFiltro && !params.tipoFiltro.isEmpty()) {
			int tipoFiltro = params.tipoFiltro.toInteger()
			
			order = params.order ? params.order : "id"
			sortType = params.sortType ? params.sortType : "desc"
			def orderBy = "order by " + order + " " + sortType
			
			if (tipoFiltro > 0) {
				
				switch (tipoFiltro) {
					case 1:
						filialList.addAll(
							Filial.findAll(
								"from Filial f where f.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong(), max: params.max, offset: params.offset]))
						filialCount = Filial.findAll(
								"from Filial f where f.id = :codigo " + orderBy,
								[codigo: params.filtro.toLong()]).size()
						break;
					case 2:
						filialList.addAll(
							Filial.findAll(
								"from Filial f where upper(f.dscFil) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%", max: params.max, offset: params.offset]))
						filialCount = Filial.findAll(
								"from Filial f where upper(f.dscFil) like :nome " + orderBy,
								[nome: "%" + params.filtro.toUpperCase() + "%"]).size()
						break;
					default:
						break;
				}
			} else {
				filialList.addAll(
					Filial.findAll(
						"from Filial f " + orderBy,
						[max: params.max, offset: params.offset]))
				filialCount = Filial.findAll(
						"from Filial f " + orderBy).size()
			}
		}
		
		[filialInstanceList: filialList, filialInstanceTotal: filialCount, order: order, sortType: sortType]
    }

    def delete() {
        def filialInstance = Filial.get(params.id)
        if (!filialInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Filial', filialInstance.id])
        } else {
            try {
                filialInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Filial'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Filial'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [filialInstance: new Filial()]
    }

    def edit() {
        def filialInstance = Filial.get(params.id)
        if (!filialInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Filial', filialInstance.id])
            redirect(action: "list")
            return
        }

        [filialInstance: filialInstance]
    }

    def save() {
        def filialInstance = new Filial(params)
		
		if (params.idHabilitada){
				filialInstance.idHabilitada='S'
		} else {
			    filialInstance.idHabilitada='N'
		}
	
			if (params.idPgHrextAdm){
			filialInstance.idPgHrextAdm='S'
		} else {
			filialInstance.idPgHrextAdm='N'
		}

        if (!filialInstance.save(flush: true)) {
            render(view: "create", model: [filialInstance: filialInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Filial'])
        redirect(action: "create")
    }

    def update() {
        def filialInstance = Filial.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (filialInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Filial'])
                render(view: "edit", model: [filialInstance: filialInstance])
                return
            }
        }

        filialInstance.properties = params
		
		if (params.idHabilitada){
				filialInstance.idHabilitada='S'
		} else {
				filialInstance.idHabilitada='N'
		}
		if (params.idPgHrextAdm){
			filialInstance.idPgHrextAdm='S'
	    } else {
			filialInstance.idPgHrextAdm='N'
	    }
		
		

        if (!filialInstance.save(flush: true)) {
            render(view: "edit", model: [filialInstance: filialInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Filial'])
        redirect(action: "list")
    }
}