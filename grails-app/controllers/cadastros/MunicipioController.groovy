package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;

@Transactional
class MunicipioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [municipioInstanceList: Municipio.list(params), municipioInstanceTotal: Municipio.count()]
    }

    def delete() {
        def municipioInstance = Municipio.get(params.id)
        if (!municipioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Municipio', municipioInstance.id])
        } else {
            try {
                municipioInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Municipio'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Municipio'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [municipioInstance: new Municipio()]
    }

    def edit() {
        def municipioInstance = Municipio.get(params.id)
        if (!municipioInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Municipio', municipioInstance.id])
            redirect(action: "list")
            return
        }

        [municipioInstance: municipioInstance]
    }

    def save() {
        def municipioInstance = new Municipio(params)
        if (!municipioInstance.save(flush: true)) {
            render(view: "create", model: [municipioInstance: municipioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Municipio'])
        redirect(action: "create")
    }

    def update() {
        def municipioInstance = Municipio.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (municipioInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Municipio'])
                render(view: "edit", model: [municipioInstance: municipioInstance])
                return
            }
        }

        municipioInstance.properties = params

        if (!municipioInstance.save(flush: true)) {
            render(view: "edit", model: [municipioInstance: municipioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Municipio'])
        redirect(action: "list")
    }
}