package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;


@Transactional

class OcorrenciasController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [ocorrenciasInstanceList: Ocorrencias.list(params), ocorrenciasInstanceTotal: Ocorrencias.count()]
    }

    def delete() {
        def ocorrenciasInstance = Ocorrencias.get(params.id)
        if (!ocorrenciasInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Ocorrencias', ocorrenciasInstance.id])
        } else {
            try {
                ocorrenciasInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Ocorrencias'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Ocorrencias'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [ocorrenciasInstance: new Ocorrencias()]
    }

    def edit() {
        def ocorrenciasInstance = Ocorrencias.get(params.id)
        if (!ocorrenciasInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Ocorrencias', ocorrenciasInstance.id])
            redirect(action: "list")
            return
        }

        [ocorrenciasInstance: ocorrenciasInstance]
    }

    def save() {
        def ocorrenciasInstance = new Ocorrencias(params)
        if (!ocorrenciasInstance.save(flush: true)) {
            render(view: "create", model: [ocorrenciasInstance: ocorrenciasInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Ocorrencias'])
        redirect(action: "create")
    }

    def update() {
        def ocorrenciasInstance = Ocorrencias.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (ocorrenciasInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Ocorrencias'])
                render(view: "edit", model: [ocorrenciasInstance: ocorrenciasInstance])
                return
            }
        }

        ocorrenciasInstance.properties = params

        if (!ocorrenciasInstance.save(flush: true)) {
            render(view: "edit", model: [ocorrenciasInstance: ocorrenciasInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Ocorrencias'])
        redirect(action: "list")
    }
}