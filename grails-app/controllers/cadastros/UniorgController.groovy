package cadastros

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;


@Transactional

class UniorgController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [uniorgInstanceList: Uniorg.list(params), uniorgInstanceTotal: Uniorg.count()]
    }

    def delete() {
        def uniorgInstance = Uniorg.get(params.id)
        if (!uniorgInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Uniorg', uniorgInstance.id])
        } else {
            try {
                uniorgInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: ['Uniorg'])
            } catch (DataIntegrityViolationException e) {
                flash.error = message(code: 'default.not.deleted.message', args: ['Uniorg'])
            }
        }

        redirect(action: "list")
    }

    def create() {
        [uniorgInstance: new Uniorg()]
    }

    def edit() {
        def uniorgInstance = Uniorg.get(params.id)
        if (!uniorgInstance) {
            flash.error = message(code: 'default.not.found.message', args: ['Uniorg', uniorgInstance.id])
            redirect(action: "list")
            return
        }

        [uniorgInstance: uniorgInstance]
    }

    def save() {
        def uniorgInstance = new Uniorg(params)
        if (!uniorgInstance.save(flush: true)) {
            render(view: "create", model: [uniorgInstance: uniorgInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: ['Uniorg'])
        redirect(action: "create")
    }

    def update() {
        def uniorgInstance = Uniorg.get(params.id)
        if (params.version) {
            def version = params.version.toLong()
            if (uniorgInstance.version > version) {
                flash.error = message(code: 'default.optimistic.locking.failure', args: ['Uniorg'])
                render(view: "edit", model: [uniorgInstance: uniorgInstance])
                return
            }
        }

        uniorgInstance.properties = params

        if (!uniorgInstance.save(flush: true)) {
            render(view: "edit", model: [uniorgInstance: uniorgInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: ['Uniorg'])
        redirect(action: "list")
    }
}