dataSource {
    pooled = true
    driverClassName = System.getenv('PHPONTO.DATASOURCE.DRIVER').toString().trim()
    username = ""
    password = ""
}
hibernate {
    cache.use_second_level_cache = false
    cache.use_query_cache = false
}
// environment specific settings
environments {
	
    development {
        dataSource {
			
			dbCreate = "none" // one of 'create', 'create-drop', 'update', 'validate', ''
			logSql = true
			
			println ""
			println ""
			println "<DATASOURCE_DEV>"
			println "Driver: " + System.getenv('PHPONTO.DATASOURCE.DRIVER')
			println "URL: " + System.getenv('PHPONTO.DATASOURCE.URL')
			println "User: " + System.getenv('PHPONTO.DATASOURCE.USERNAME')
			println "</DATASOURCE_DEV>"
			println ""
			println ""
			
            url = System.getenv('PHPONTO.DATASOURCE.URL')
            username = System.getenv('PHPONTO.DATASOURCE.USERNAME')
			password = System.getenv('PHPONTO.DATASOURCE.PASSWORD') 
        }
		
    }
    test {
        dataSource {
			dbCreate = "none" // one of 'create', 'create-drop', 'update', 'validate', ''
			logSql = true
			
			println ""
			println ""
			println "<DATASOURCE_TEST>"
			println "Driver: " + System.getenv('PHPONTO.DATASOURCE.DRIVER')
			println "URL: " + System.getenv('PHPONTO.DATASOURCE.URL')
			println "User: " + System.getenv('PHPONTO.DATASOURCE.USERNAME')
			println "</DATASOURCE_TEST>"
			println ""
			println ""
			
            url = System.getenv('PHPONTO.DATASOURCE.URL')
            username = System.getenv('PHPONTO.DATASOURCE.USERNAME')
			password = System.getenv('PHPONTO.DATASOURCE.PASSWORD')
        }
    }
	
    production {
        dataSource {
			dbCreate = "none" // one of 'create', 'create-drop', 'update', 'validate', ''
			logSql = true
			
			println ""
			println ""
			println "<DATASOURCE_PROD>"
			println "Driver: " + System.getenv('PHPONTO.DATASOURCE.DRIVER')
			println "URL: " + System.getenv('PHPONTO.DATASOURCE.URL')
			println "User: " + System.getenv('PHPONTO.DATASOURCE.USERNAME')
			println "</DATASOURCE_PROD>"
			println ""
			println ""
			
            url = System.getenv('PHPONTO.DATASOURCE.URL')
            username = System.getenv('PHPONTO.DATASOURCE.USERNAME')
			password = System.getenv('PHPONTO.DATASOURCE.PASSWORD')
        }
		
	}
}
