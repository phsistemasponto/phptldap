package phponto

import groovy.sql.Sql

import java.text.SimpleDateFormat

import acesso.Administracao
import acesso.Usuario

class AgendadoJob {
	
	def dataSource
	
	
    static triggers = {
      cron name: 'agendadoTrigger', cronExpression: "0 0 1 * * ?"
    }

    def execute() {
		verificaInatividadeSenha()
        verificaInatividadeLogin()
    }
	
	def verificaInatividadeSenha() {
		
		println "#Invocando rotina de verifica��o de inatividade de senha"
		
		Administracao config = Administracao.get(1)
		def periodicidade = config.periodicidade.toInteger()
		
		if (periodicidade == 0) {
			println "#Rotina desabilitada"
			return
		}
		
		println "#Bucando usuarios sem alterar senha ha ${periodicidade} dias"
		
		def hoje = new Date()
		def dataLimite = hoje - periodicidade
		def dtLimiteParam = new SimpleDateFormat("yyyy-MM-dd").format(dataLimite)
		
		println "#Data limite: ${dataLimite}"
		
		def query = """
					select distinct h.login 
                     from historico_senha h
					where (
						select data 
						from historico_senha hi
						where hi.login = h.login
				order by hi.data desc limit 1) <  """
		
		query += "'" + dtLimiteParam + "'"
		
		Sql sql = new Sql(dataSource)
		def results = []
				
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
		}
		
		println "#Resultado: ${results} "
		
		results.each {
			Usuario user = Usuario.findByUsername(it['login'])
			user.passwordExpired = true
			user.save()
			println "#Expirando senha do usuario usuario: ${user.username} "
		}
		
		
	}
	
	def verificaInatividadeLogin() {
		
		println "#Invocando rotina de verifica��o de inatividade de usu�rios"
		
		Administracao config = Administracao.get(1)
		def periodoInativo = config.periodoInativo.toInteger()
		
		if (periodoInativo == 0) {
			println "#Rotina desabilitada"
			return
		}
		
		println "#Bucando usuarios sem login ha ${periodoInativo} dias"
		
		def hoje = new Date()
		def dataLimite = hoje - periodoInativo
		def dtLimiteParam = new SimpleDateFormat("yyyy-MM-dd").format(dataLimite)
		
		println "#Data limite: ${dataLimite}"
		
		def query = """ 
					select distinct h.login 
                     from historico_login h
					where (
						select data 
						from historico_login hi
						where hi.login = h.login
						and hi.sucesso = true
				order by hi.data desc limit 1) <  """
		
		query += "'" + dtLimiteParam + "'"
		
		Sql sql = new Sql(dataSource)
		def results = []
				
		try {
			results = sql.rows(query)
		} catch(Exception e) {
			e.printStackTrace()
		}
		
		println "#Resultado: ${results} "
		
		results.each {
			Usuario user = Usuario.findByUsername(it['login'])
			user.accountLocked = true
			user.save()
			println "#Bloqueando usuario: ${user.username} "
		}
		
		
	}
}
