package apoio

class UsuarioTagLib {

	def springSecurityService

	def usuario = { attrs ->

		def usuario = session['usuario']
		def outUsuario = 'Convidado'
		
		try {
			if(!usuario) {
				usuario = springSecurityService.getCurrentUser()
				session['usuario'] = usuario
			}
			
			outUsuario = usuario.nome
		} catch (Exception e){
			outUsuario = 'Convidado'
		}
		

		out << outUsuario
	}
}