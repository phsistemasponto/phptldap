
package apoio

import acesso.GrupoAcesso
import cadastros.HorarioSimulado


class AlertasTagLib {

	def springSecurityService
	def grupoAcessoService
	def usuario
	
	boolean transactional = true

	def alertasApp = { attrs ->
		
		def alerta = ""

		usuario = session['usuario']
		
		if (usuario == null) {
			return false
		}
		
		GrupoAcesso grupo = grupoAcessoService.retornaGrupoAcesso(usuario)
		
		if (grupo.descricao.equals("ADMINISTRADOR")) {
			
			// alerta de horarios
			def horarios = HorarioSimulado.findAll("from HorarioSimulado c where c.flag = '1'")
			if (horarios != null && horarios.size() > 0) {
				alerta += "<div class='alert alert-info' role='status'>"
				alerta += "Novas simula&ccedil;&otilde;es de hor&aacute;rios foram criadas."
				alerta += "<a href='" + createLink(uri: "/horarioSimulado/index") + "'>Clique para acessar</a>."
				alerta += "</div>"
			}
			
		}

		out << alerta
	}
		
}