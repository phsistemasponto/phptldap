package apoio

import acesso.Menu

class AtalhosTagLib {

	def springSecurityService
	def grupoAcessoService
	def menu = ''
	def hotkeys = ''
	def mapeamentos
	def mapementosNetos
	def usuario
	def perfis
	def listPerfis
	Menu menuItem

	def atalhosApp = { attrs ->

		menu = session['atalhos']
		usuario = session['usuario']
		
		if (usuario == null) {
			return false
		}
		
		perfis = grupoAcessoService.retornaPerfis(usuario)
		listPerfis = perfis as List
		
		if(!usuario) {
			usuario = springSecurityService.getCurrentUser()
			session['usuario'] = usuario
			menu=null
		}
		
		if ( (!menu) ) {
			
			mapeamentos = Menu.findAll("from Menu menu, Perfil p \
			                           where menu.configAttribute = p.authority and p in (:perfis) and menu.atalho = true \
			                           order by menu.ordem ",[perfis: listPerfis])


			menu = "<ul class='nav'>"
			mapeamentos.each {
				menuItem = (Menu)it[0]
				menu += "<li>"
				menu += "<a href='" + createLink(uri: it[0].url) + "'>"
				menu += "<img src='" + createLink(controller:'menu', action:'image', id: it[0].id) + "' style='max-height: 19px;vertical-align: middle;' title='" + it[0].comandoAtalho + "'/> "
				menu += "<span style='vertical-align: middle;display: inline-block;height: 100%;' title='" + it[0].comandoAtalho + "'>${it[0].nome}</span> </a>"
			}
			
			menu += "</ul>"
			session['atalhos'] = menu
		}

		out << menu
	}
	
	
	def hotkeysApp = { attrs ->
		
		hotkeys = session['hotkeys']
		usuario = session['usuario']
		
		if (usuario == null) {
			return false
		}
		
		perfis = grupoAcessoService.retornaPerfis(usuario)
		listPerfis = perfis as List
		
		if(!usuario) {
			usuario = springSecurityService.getCurrentUser()
			session['usuario'] = usuario
			hotkeys=null
		}
		
		if ( (!hotkeys) ) {
			
			mapeamentos = Menu.findAll("from Menu menu, Perfil p \
									   where menu.configAttribute = p.authority and p in (:perfis) and menu.atalho = true \
									   order by menu.ordem ",[perfis: listPerfis])


			hotkeys = "<script>"
			mapeamentos.each {
				menuItem = (Menu)it[0]
				hotkeys += "shortcut.add('" + it[0].comandoAtalho + "',function() { location.href = '" + createLink(uri: it[0].url) + "'; });"
			}
			
			hotkeys += "</script>"
			session['hotkeys'] = hotkeys
		}

		out << hotkeys
	}
		
}