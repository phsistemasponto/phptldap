package apoio

import acesso.Menu

class MenuTagLib {

	def springSecurityService
	def grupoAcessoService
	def menu = ''
	def mapeamentos
	def mapementosNetos
	def usuario
	def perfis
	def listPerfis
	Menu menuItem

	def menuApp = { attrs ->

		menu = session['menu']
		usuario = session['usuario']
		
		if (usuario == null) {
			return false
		}
		
		perfis = grupoAcessoService.retornaPerfis(usuario)
		listPerfis = perfis as List
		
		if(!usuario) {
			usuario = springSecurityService.getCurrentUser()
			session['usuario'] = usuario
			menu=null
		}
		
		if ( (!menu) ) {
			
			mapeamentos = Menu.findAll("from Menu menu, Perfil p \
			                           where menu.configAttribute = p.authority and p in (:perfis) and menu.pai is null \
			                           order by menu.ordem ",[perfis: listPerfis])


			menu = "<ul class='nav'>"
			mapeamentos.each {
				menuItem = (Menu)it[0]
				menu += "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle' href='#'>${it[0].nome}<b class='caret'></b></a>"
				menu+=filhos(menuItem.id) 
			}
			
			menu += "</ul>"
			menu += "<div onclick='showCalculadora();' class='pull-right calc_diminuir' style='background-image: url(" + createLink(uri: "/images/cal.png") + ");float: right;cursor: pointer;height: 30px;position: absolute;right: 0px;background-color: #a19210;background-repeat: no-repeat;background-position: 10px;'><span style='margin:0 35px;display: block;font-weight:900;'>CALCULADORA<span></div>"
			session['menu'] = menu
		}

		out << menu
	}

	private filhos = { idPai ->

		def resultado = "<ul class='dropdown-menu'>"
		

		
		Menu menuPai = Menu.get(idPai)
		
		def filhos = Menu.findAll("from Menu menu, Perfil p \
			where menu.configAttribute = p.authority and p in (:perfis) and menu.pai= :menuPai \
			order by menu.ordem",[perfis: listPerfis, menuPai: menuPai])
		if (!filhos){
			return '</li>'
		}
		filhos.each {
			resultado += "<li><a href='" + createLink(uri: it[0].url) + "'>${it[0].nome}</a>"
			resultado += netos(it[0].id, it[0].nome)
		}
		
		resultado+='</ul></li>'

		return resultado
	}
	private netos = { idPai, nomePai ->
		def menuPai = Menu.get(idPai)
		
		def mapeamentosNetos = Menu.findAll("from Menu menu, Perfil p \
			where menu.configAttribute = p.authority and p in (:perfis) and menu.pai = :menuPai \
			order by menu.ordem",[perfis: listPerfis, menuPai: menuPai])

		
		if (!mapeamentosNetos){
			return '</li>'
		}
		def resultado = '<ul class="dropdown-menu">'
					
		mapeamentosNetos.each {
			resultado += "<li><a href='" + createLink(uri: it[0].url) + "'>${it[0].nome}</a></li>"
		}
		resultado += "</ul><li>"
		
		return resultado
	}
		
}