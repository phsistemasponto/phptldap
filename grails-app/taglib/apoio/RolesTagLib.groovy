package apoio

class RolesTagLib {
    def springSecurityService

    def currentUserRoleSelect = { attrs ->
		def principal = springSecurityService.getPrincipal() 
		def roles = principal.authorities
		
		attrs.from = roles
		attrs.optionKey = attrs.optionKey ?: 'authority'
		attrs.optionValue = attrs.optionValue ?: 'authority'
		
		out << g.select(attrs)
    }
}