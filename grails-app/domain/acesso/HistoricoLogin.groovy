package acesso



class HistoricoLogin {

	String login
	Date data
	Date dataEncerramento
	Boolean sucesso
	String sessionId

	static constraints = {
		login nullable: false, blank: false
		data nullable: false, blank: false
		dataEncerramento nullable: true, blank: true
		sucesso nullable: false, blank: false
		sessionId nullable: true, blank: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "historico_login_seq"]
	}
	
}
