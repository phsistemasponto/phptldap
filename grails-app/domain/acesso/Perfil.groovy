package acesso

class Perfil {

	String authority
	String textoExibicao

	Usuario usuario
	Boolean possuiPerfil
	
	GrupoAcesso grupo
	Boolean possuiPerfilGrupo
	
	static transients = ['usuario','possuiPerfil','grupo','possuiPerfilGrupo']
	
	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
		textoExibicao blank: true, nullable: true, maxSize: 100
	}
	
	Boolean getPossuiPerfil(){
		if (this.usuario){
			def usuarioPerfil = UsuarioPerfil.findByUsuarioAndPerfil(this.usuario, this)
			if (usuarioPerfil){
				true
			} else {
				false
			}
		} else {
			false
		}
	}
	
	Boolean getPossuiPerfilGrupo(){
		if (this.grupo){
			def grupoPerfil = GrupoPerfil.findByGrupoAndPerfil(this.grupo, this)
			if (grupoPerfil){
				true
			} else {
				false
			}
		} else {
			false
		}
	}

	
}
