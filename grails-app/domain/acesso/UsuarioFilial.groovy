package acesso

import cadastros.*

class UsuarioFilial {
	
	Usuario usuario
	Filial filial
	
	static belongsTo = [usuario: Usuario]
	
	static constraints = {
		filial blank: false, nullable: false
		usuario blank: false, unique: ['filial']
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "usuario_filial_seq"]
	}
	
	static void removeAll(Usuario usuario) {
		executeUpdate 'DELETE FROM UsuarioFilial WHERE usuario=:usuario', [usuario: usuario]
	}
}
