package acesso

import cadastros.*

class UsuarioSetor {
	
	Usuario usuario
	Setor setor
	
	static belongsTo = [usuario: Usuario]
	
	static constraints = {
		setor blank: false, nullable: false
		usuario blank: false, unique: ['setor']
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "usuario_setor_seq"]
	}
	
	static void removeAll(Usuario usuario) {
		executeUpdate 'DELETE FROM UsuarioSetor WHERE usuario=:usuario', [usuario: usuario]
	}
}
