package acesso

import org.hibernate.envers.Audited

import cadastros.Filial
import cadastros.Funcionario


@Audited
class Usuario {

	transient springSecurityService
	transient grupoAcessoService

	String nome
	String email
	String username
	String password
	Boolean enabled
	Boolean accountExpired
	Boolean accountLocked
	Boolean passwordExpired
	Funcionario funcionario

	static constraints = {
		username 	blank: false, unique: true
		password 	blank: false
		funcionario nullable: true
	}

	static mapping = {
		password column: 'senha'
		id generator: "sequence", params:[sequence: "usuario_seq"]
	}

	Set<Perfil> getAuthorities() {
		def perfis = grupoAcessoService.retornaPerfis(this)
		perfis
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}

	List getPerfisUsuario(){
		def perfis = GrupoAcesso.listOrderByDescricao()
		def item = []
		def listaRetorno = []
		def temPerfil
		def usuarioPerfil
		perfis.each(){
			usuarioPerfil = UsuarioGrupo.findByUsuarioAndGrupo(this, it)
			if (usuarioPerfil){
				temPerfil = true
			} else {
				temPerfil = false
			}
			item = [perfil: it, temPerfil: temPerfil]
			listaRetorno.add(item)
		}
		return listaRetorno
	}

	List getFuncionarios(){
		def funcionarios = UsuarioFuncionario.findAllByUsuario(this)
	}
	
	List getUsuarioFiliais() {
		UsuarioFilial.findAllByUsuario(Usuario.get(this.id))
	}
	
	List getFiliais() {
		def filiaisUsuario = UsuarioFilial.findAllByUsuario(Usuario.get(this.id))
		def filiais = new ArrayList<Filial>()
		filiaisUsuario.each {
			filiais.add(it.filial)
		}
		filiais
	}
	
	List getUsuarioSetores() {
		UsuarioSetor.findAllByUsuario(Usuario.get(this.id))
	}
	
	List getUsuarioGrupos() {
		UsuarioGrupoFuncionario.findAllByUsuario(Usuario.get(this.id))
	}
	
	List getSetor() {
		def setoresUsuario = UsuarioSetor.findAllByUsuario(Usuario.get(this.id))
		def setores = new ArrayList<Filial>()
		setoresUsuario.each {
			setores.add(it.setor)
		}
		setores
	}
	
	Boolean possuiFilialQueVisualizaDocumento() {
		boolean retorno = false
		def setores = UsuarioFilial.findAllByUsuario(this)
		setores.each {
			if (it.filial.empresa.idDoc == 'S') {
				retorno = true
			}
		}
		return retorno
	}
	
	Boolean possuiFilialQueVisualizaSalario() {
		boolean retorno = false
		def setores = UsuarioFilial.findAllByUsuario(this)
		setores.each {
			if (it.filial.empresa.idSal == 'S') {
				retorno = true
			}
		}
		return retorno
	}
	
	GrupoAcesso getAcesso() {
		if (id) {
			def gu = UsuarioGrupo.findByUsuario(this)
			gu != null ? gu.grupo : null
		} else {
			null
		}
	}
}
