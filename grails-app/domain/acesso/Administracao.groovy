package acesso

class Administracao {

	Long periodicidade
	Long quantidadeCaracteres
	String tipoCaracter
	Long qtdSenhasRepetidas
	Long diasAviso
	Long intervaloRepeticao
	String sincronismo
	Long tentativasLogin
	String historicoLogin
	String historicoTrans
	Long periodoInativo
	Long qtdMaxCaracRep
	
	
	static constraints = {
		
		tipoCaracter maxSize: 1, blank: false, inList: ["L","N","T"] // L-Letras, N-Números, T: Letras e números
		sincronismo maxSize: 1, blank: false, inList: ["S","N"] //S - Sim, N-Não
		historicoLogin maxSize: 1, blank: false, unique: true, inList: ["S","N"] //S - Sim, N-Não 
		historicoTrans maxSize: 1, blank: false, inList: ["S","N"] //S - Sim, N-Não
		
	}
	
	static String CODIGO_SENHA_SOMENTE_LETRAS = "L"
	static String CODIGO_SENHA_SOMENTE_NUMEROS = "N"
	static String CODIGO_SENHA_LETRAS_E_NUMEROS = "T"
	
	def senhaDeveTerSomenteNumeros() {
		return tipoCaracter.equals(CODIGO_SENHA_SOMENTE_NUMEROS)
	}
	
	def senhaDeveTerSomenteLetras() {
		return tipoCaracter.equals(CODIGO_SENHA_SOMENTE_LETRAS)
	}
	
	def senhaDeveTerLetrasENumeros() {
		return tipoCaracter.equals(CODIGO_SENHA_LETRAS_E_NUMEROS)
	}


}
