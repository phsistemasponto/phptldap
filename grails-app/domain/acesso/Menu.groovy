package acesso

class Menu {

	String nome
	String url
	String configAttribute
	
	Boolean menu
	Boolean atalho
	String comandoAtalho
	Long ordem
	Menu pai

	String toString() {
		"${this.nome}"
	}
	
	static hasMany = [
		icone: MenuIcone
	]

	static constraints = {
		nome maxSize:120, nullable:false, blank:false
		configAttribute maxSize:255, nullable:false, blank:false, unique:true
		comandoAtalho maxSize:10, nullable:true, blank:true
		pai nullable: true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "menu_seq"]
		pai lazy: false
	}

	static belongsTo = [pai: Menu]
}

