package acesso


class GrupoAcesso {

	String descricao

	Usuario usuario
	Boolean possuiPerfil	
	
	static transients = ['usuario','possuiPerfil']
		
	static constraints = {
		descricao maxSize: 50, blank: false
	}
	
	static mapping = {
		id generator: "sequence", params:[sequence: "grupoacesso_seq"]
	}
	
	List getPerfisGrupo(){
		def perfis = Perfil.listOrderByTextoExibicao()
		def item = []
		def listaRetorno = []
		def temPerfil
		def grupoPerfil
		perfis.each(){
			grupoPerfil = GrupoPerfil.findByGrupoAndPerfil(this, it)
			if (grupoPerfil){
				temPerfil = true
			} else {
				temPerfil = false
			}
			item = [perfil: it, temPerfil: temPerfil]
			listaRetorno.add(item)
		}
		return listaRetorno
	}
	
	Boolean getPossuiPerfil(){
		if (this.usuario){
			def usuarioPerfil = UsuarioGrupo.findByUsuarioAndGrupo(this.usuario, this)
			if (usuarioPerfil){
				true
			} else {
				false
			}
		} else {
			false
		}
	}
	
	String toString() {
		"${this.descricao}"
	}

}
