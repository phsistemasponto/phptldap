package acesso

import org.apache.commons.lang.builder.HashCodeBuilder
import cadastros.*

class UsuarioFuncionario implements Serializable {

	Usuario usuario
	Funcionario funcionario

	boolean equals(other) {
		if (!(other instanceof UsuarioFuncionario)) {
			return false
		}

		other.usuario?.id == usuario?.id &&
			other.funcionario?.id == funcionario?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (usuario) builder.append(usuario.id)
		if (funcionario) builder.append(funcionario.id)
		builder.toHashCode()
	}

	static UsuarioFuncionario get(long usuarioId, long funcionarioId) {
		find 'from UsuarioFuncionario where usuario.id=:usuarioId and funcionario.id=:funcionarioId',
			[usuarioId: usuarioId, funcionarioId: funcionarioId]
	}

	static UsuarioFuncionario create(Usuario usuario, Funcionario funcionario, boolean flush = false) {
		new UsuarioFuncionario(usuario: usuario, funcionario: funcionario).save(flush: flush, insert: true)
	}

	static boolean remove(Usuario usuario, Funcionario funcionario, boolean flush = false) {
		UsuarioFuncionario instance = UsuarioFuncionario.findByUsuarioAndFuncionario(usuario, funcionario)
		if (!instance) {
			return false
		}

		instance.delete(flush: flush)
		true
	}

	static void removeAll(Usuario usuario) {
		executeUpdate 'DELETE FROM UsuarioFuncionario WHERE usuario=:usuario', [usuario: usuario]
	}

	static void removeAll(Funcionario funcionario) {
		executeUpdate 'DELETE FROM UsuarioFuncionario WHERE funcionario=:funcionario', [funcionario: funcionario]
	}

	static mapping = {
		id composite: ['funcionario', 'usuario']
		version false
	}
}
