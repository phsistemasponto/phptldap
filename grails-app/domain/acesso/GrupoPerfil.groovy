package acesso

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder

class GrupoPerfil implements Serializable {

	GrupoAcesso grupo
	Perfil perfil

	boolean equals(other) {
		if (!(other instanceof GrupoPerfil)) {
			return false
		}

		other.grupo?.id == usuario?.id &&
			other.perfil?.id == perfil?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (grupo) builder.append(grupo.id)
		if (perfil) builder.append(perfil.id)
		builder.toHashCode()
	}

	static UsuarioPerfil get(long grupoId, long perfilId) {
		find 'from GrupoPerfil where grupo.id=:grupoId and perfil.id=:perfilId',
			[grupoId: grupoId, perfilId: perfilId]
	}

	static GrupoPerfil create(GrupoAcesso grupo, Perfil perfil, boolean flush = false) {
		new GrupoPerfil(grupo: grupo, perfil: perfil).save(flush: flush, insert: true)
	}

	static boolean remove(GrupoAcesso grupo, Perfil perfil, boolean flush = false) {
		GrupoPerfil instance = UsuarioPerfil.findByGrupoAcessoAndPerfil(grupo, perfil)
		if (!instance) {
			return false
		}

		instance.delete(flush: flush)
		true
	}

	static void removeAll(GrupoAcesso grupo) {
		executeUpdate 'DELETE FROM GrupoPerfil WHERE grupo=:grupo', [grupo: grupo]
	}

	static void removeAll(Perfil perfil) {
		executeUpdate 'DELETE FROM UsuarioPerfil WHERE perfil=:perfil', [perfil: perfil]
	}

	static mapping = {
		id composite: ['perfil', 'grupo']
		version false
	}
}
