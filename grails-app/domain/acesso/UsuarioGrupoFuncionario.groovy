package acesso

import cadastros.Grupo

class UsuarioGrupoFuncionario implements Serializable {

	Usuario usuario
	Grupo grupoFuncionario
	
	static belongsTo = [usuario: Usuario]

	static mapping = {
		id generator: "sequence", params:[sequence: "usuario_grupo_funcionario_seq"]
	}
	
	static void removeAll(Usuario usuario) {
		executeUpdate 'DELETE FROM UsuarioGrupoFuncionario WHERE usuario=:usuario', [usuario: usuario]
	}
	
}