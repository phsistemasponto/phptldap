package acesso

import org.hibernate.envers.RevisionEntity
import org.hibernate.envers.RevisionNumber
import org.hibernate.envers.RevisionTimestamp

import comum.SpringSecurityRevisionListener

@RevisionEntity(SpringSecurityRevisionListener.class)
class UserRevisionEntity {

	@RevisionNumber
	Long id

	@RevisionTimestamp
	Long timestamp

	Usuario usuario
	
	Date data

	static constraints = { usuario(nullable: true) }

	static transients = ['revisionDate']

	public Date getRevisionDate() {
		return new Date(timestamp);
	}
}
