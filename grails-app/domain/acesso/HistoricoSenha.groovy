package acesso

import org.hibernate.envers.Audited


@Audited
class HistoricoSenha {
	
	transient springSecurityService

	String login
	Date data
	String password

	static constraints = {
		login nullable: false, blank: false
		data nullable: false, blank: false
		password nullable: false, blank: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "historico_login_seq"]
	}
	
	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
	
}
