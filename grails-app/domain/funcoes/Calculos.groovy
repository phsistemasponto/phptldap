package funcoes

import java.text.NumberFormat



class Calculos {

	static mapWith = "none"

	public static Double retornarValor(String formula, Double salario, Double extras, Double dsr) {
		
		NumberFormat nf = NumberFormat.getInstance()
		nf.setMinimumIntegerDigits(1)
		nf.setMinimumFractionDigits(2)
		nf.setMaximumFractionDigits(2)
		nf.setGroupingUsed(false)
		
		String salarioStr = nf.format(salario)
		String extrasStr = nf.format(extras)
		String dsrStr = nf.format(dsr)
		
		String exp = formula.replace("SALARIO", salarioStr)
		exp = exp.replace("HORAS", extrasStr)
		
		Double retorno = Eval.me(exp.replace(",", ".")) 
		retorno  
	}
	
}
