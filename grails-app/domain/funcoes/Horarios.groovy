package funcoes


class Horarios {

	static mapWith = "none"

	public static Long calculaMinutos(String horaString){
		int separatorPosition = horaString.indexOf(":") 
		Long hora = Integer.parseInt(horaString.substring(0, separatorPosition))
		Long minutos = Integer.parseInt(horaString.substring(separatorPosition+1))
		Long minutosRetorno = (hora * 60) + minutos  
	}
	
	public static String retornaHora(Long qtdMinutos){
		if (qtdMinutos == null) {
			qtdMinutos = 0
		}
		
		Integer modMinutos = qtdMinutos >= 0 ? qtdMinutos : -(qtdMinutos)
		String hora
		String minutos
		if (modMinutos<60){
			hora = '00'
			minutos = String.format("%02d", modMinutos)
		} else {
			def minutosDiferenca = modMinutos % 60
			int horasInteiras = (modMinutos - minutosDiferenca) / 60
			hora = String.format("%02d", horasInteiras)
			minutos = String.format("%02d", minutosDiferenca)
		}
		
		minutos = (minutos)?minutos:'00'
		
		def sign = qtdMinutos >= 0 ? "" : "- "
		
		return sign+hora+':'+minutos
	}

	
}
