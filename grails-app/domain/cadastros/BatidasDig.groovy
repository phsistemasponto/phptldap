package cadastros

import org.hibernate.envers.Audited

@Audited
class BatidasDig {
	
	Funcionario funcionario
	Date data
	Long entrada1
	Long saida1
	Long entrada2
	Long saida2
	Long entrada3
	Long saida3

	static constraints = {
		funcionario nullable:false
		data nullable:false
		entrada1 nullable:true
		saida1 nullable:true
		entrada2 nullable:true
		saida2 nullable:true
		entrada3 nullable:true
		saida3 nullable:true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "batidas_dig_seq"]
		data type: "date"
	}

}