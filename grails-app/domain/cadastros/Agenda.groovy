package cadastros

import org.hibernate.envers.Audited

@Audited
class Agenda {
	
	String descricao

	static constraints = {
		descricao maxSize:512, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "agenda_seq"]
	}
	
	static hasMany = [horarios: AgendaHorario, filiais: AgendaFilial]

	String toString() {
		"${this.descricao}"
	}
	
}
