package cadastros

import org.hibernate.envers.Audited

@Audited
class BeneficioFuncOcor implements Serializable {

	Beneficio beneficio
	Funcionario funcionario
	Ocorrencias ocorrencias
	Date dtInicio
	Date dtFim
	Date dtProc
	Integer quantidade

	static mapping = {
		id generator: "sequence", params:[sequence: "BeneficioFuncOcor_seq"]
		dtInicio type: "date"
		dtFim type: "date"
	}
	
	
}
