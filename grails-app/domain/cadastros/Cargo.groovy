package cadastros

import org.hibernate.envers.Audited

@Audited
class Cargo {
	String dscCargo
	String idfolgaincrementada
	String idpghrext
	String cbo



	static constraints = {
		dscCargo maxSize:80, nullable:false, blank:false
		cbo maxSize:12, nullable:true,blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "Cargo_seq"]

	}
	

	String toString() {
		"${this.dscCargo}"
	}


}
