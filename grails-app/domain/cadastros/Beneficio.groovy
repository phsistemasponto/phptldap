package cadastros

import org.hibernate.envers.Audited

@Audited
class Beneficio {
	
	TipoDeBeneficio tipoDeBeneficio
	String descricao
	
	static hasMany = [valores: ValorBeneficio, , filiais: BeneficioFilial]
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "beneficio_seq"]
	}

	String toString() {
		"${this.descricao}"
	}
	
	ValorBeneficio getValorAtivo() {
		ValorBeneficio retorno = new ValorBeneficio()
		valores.each {
			if (it.dataFim == null) {
				retorno = it
			}
		}
		return retorno
	}

}