package cadastros

import org.hibernate.envers.Audited;

import funcoes.Horarios;

@Audited
class Bathorario {
	Perfilhorario perfilhorario
	Long seqbathorario
	String idbat
	Long hrbat
	
	String horaFormatada
	String idbatFormatado
	
	static transients = ['horaFormatada','idbatFormatado']
	
	static belongsTo = [perfilhorario: Perfilhorario]

	static constraints = {
		idbat maxSize:2 , nullable: false, blank: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "bathorario_seq"]
		perfilhorario lazy: false
	}


	String toString() {
		"${this.idbat}"
	}
	
	String getIdbatFormatado(){
		return (idbat=='E')?'Entrada':'Saida'
	}

	String getHoraFormatada(){
		return Horarios.retornaHora(hrbat)
	}

}
