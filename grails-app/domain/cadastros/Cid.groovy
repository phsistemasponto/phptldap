package cadastros

import org.hibernate.envers.Audited

@Audited
class Cid {
	
	String codCid
	String dscCid

	static constraints = {
		codCid maxSize:10, nullable:false, blank:false
		dscCid maxSize:512, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "cid_seq"]
	}

	String toString() {
		"${this.codCid} - ${this.dscCid}"
	}
	
}
