package cadastros



import java.sql.Blob

import org.hibernate.envers.Audited

@Audited
class EmpresaFoto {
	
	Empresa empresa
	Blob conteudo

	static mapping = {
		id generator: "sequence", params:[sequence: "empresa_foto_seq"]
		conteudo type: 'blob'
	}
	
}
