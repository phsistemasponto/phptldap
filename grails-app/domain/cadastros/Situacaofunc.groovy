package cadastros

import org.hibernate.envers.Audited;

@Audited
class Situacaofunc {	
		String dscstfunc
		String idstfunc
				
	
		static constraints = {
			dscstfunc maxSize:30, nullable:false, blank:false
			idstfunc maxSize:2, nullable:false, blank:false
			idstfunc maxSize:12, nullable:true, blank:false
		}
	
		static mapping = {
			id generator: "sequence", params:[sequence: "sitfunc_seq"]
	
		}
		
	
		String toString() {
			"${this.dscstfunc}"
		}
	

}
