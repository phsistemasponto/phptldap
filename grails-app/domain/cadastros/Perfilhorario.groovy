package cadastros

import org.hibernate.envers.Audited;

import funcoes.Horarios

@Audited
class Perfilhorario {
	Tipotolerancia tipotolerancia
	String dscperfil
	Long cargahor
	String idvira
	
	String cargaHorariaHoras
	String viraString

	static hasMany=[bathorarios: Bathorario]

	static transients = ['cargaHorariaHoras', 'viraString']
	
	static constraints = {
		dscperfil maxSize:80 , nullable: false, blank: false
		idvira maxSize:2 , nullable: true, blank: false
		
		
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "perfilhorario_seq"]
		tipotolerancia lazy: false
	}


	String toString() {
		"${this.dscperfil}"	
		
	}
	
	String getCargaHorariaHoras(){
		Horarios.retornaHora(this.cargahor)
	}
	
	String getViraString(){
		(this.idvira=='S')?'Sim':'Não'
	}
}
