package cadastros

import org.hibernate.envers.Audited

@Audited
class ParamRelatorioBancoHorasOcorrencia {
	
	ParamRelatorioBancoHoras param
	Ocorrencias ocorrencia
	
	static constraints = {
		
	}

	static mapping = {
		table "p_rel_bcohr_ocor"
		id generator: "sequence", params:[sequence: "p_rel_bcohr_ocor_seq"]
	}

	String toString() {
		"${this.ocorrencia}"
	}

}