package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class ParamRelatorioHorasExtras {
	
	String descricao
	Usuario usuario
	
	static hasMany = [ocorrencias: ParamRelatorioHorasExtrasOcorrencia]
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		table "p_rel_he"
		id generator: "sequence", params:[sequence: "p_rel_he_seq"]
	}

	String toString() {
		"${this.descricao}"
	}

}