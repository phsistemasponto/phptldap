package cadastros

import org.hibernate.envers.Audited

@Audited
class AgendaFilial {
	
	Agenda agenda
	Filial filial 

	static constraints = {
		agenda nullable:false
		filial nullable:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "agenda_filial_seq"]
	}
	
	static void removeAll(Agenda agenda) {
		executeUpdate 'DELETE FROM AgendaFilial WHERE agenda=:agenda', [agenda: agenda]
	}
	
}
