package cadastros

import org.hibernate.envers.Audited;

@Audited
class ObservacaoSldBh {

	String observacao
	
	
	static constraints = {
		observacao maxSize: 100, nullable: false, blank: false

	}

	static mapping = {
		id generator: "sequence", params:[sequence: "observacao_sld_Bh_seq"]
		
	}

}
