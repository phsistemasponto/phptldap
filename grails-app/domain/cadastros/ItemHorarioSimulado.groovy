package cadastros

import java.math.*
import java.text.*

import org.hibernate.envers.Audited

import funcoes.Horarios

@Audited
class ItemHorarioSimulado {
	
	HorarioSimulado horarioSimulado
	Classificacao classificacao
	Perfilhorario perfilhorario
	
	Integer nrDiaSemana
	Long sequencia
	
	Long hora1
	Long hora2
	Long hora3
	Long hora4
	Long cargaHoraria
	
	String diaSemana
	String hora1Str
	String hora2Str
	String hora3Str
	String hora4Str
	String cargaHorariaStr

	static belongsTo = [horarioSimulado: HorarioSimulado] 
	static transients = ['diaSemana','hora1Str','hora2Str','hora3Str','hora4Str','cargaHorariaStr']
	
	static constraints = {
		horarioSimulado nullable: false
		classificacao nullable: false
		nrDiaSemana nullable: false
		hora1 nullable: true
		hora2 nullable: true
		hora3 nullable: true
		hora4 nullable: true
		cargaHoraria nullable: true
		perfilhorario nullable: true
	}

	static mapping = {
		table "item_hor_simul"
		id generator: "sequence", params:[sequence: "item_hor_simul_seq"]
		horarioSimulado lazy: false
		classificacao lazy: false
	}
	
	String getDiaSemana() {
		def dia = ['Seg','Ter','Qua','Qui','Sex','Sab','Dom']
		dia[nrDiaSemana-1]
	}
	
	String getHora1Str() {
		return Horarios.retornaHora(hora1)
	}
	
	String getHora2Str() {
		return Horarios.retornaHora(hora2)
	}
	
	String getHora3Str() {
		return Horarios.retornaHora(hora3)
	}
	
	String getHora4Str() {
		return Horarios.retornaHora(hora4)
	}
	
	String getCargaHorariaStr() {
		return Horarios.retornaHora(cargaHoraria)
	}
	
	static void removeAll(HorarioSimulado horarioSimulado) {
		executeUpdate 'DELETE FROM ItemHorarioSimulado WHERE horarioSimulado=:horarioSimulado', [horarioSimulado: horarioSimulado]
	}
}
