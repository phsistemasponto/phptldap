package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class ParamRelatorioBancoHoras {
	
	String descricao
	Usuario usuario
	
	static hasMany = [ocorrencias: ParamRelatorioBancoHorasOcorrencia]
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		table "p_rel_bcohr"
		id generator: "sequence", params:[sequence: "p_rel_bcohr_seq"]
	}

	String toString() {
		"${this.descricao}"
	}

}