package cadastros

import java.text.DecimalFormat
import java.text.NumberFormat

import org.hibernate.envers.Audited

@Audited
class Orcamento {
	
	Filial filial
	Periodo periodo
	Double valor
	Boolean liberado
	
	String valorFormatado
	
	static transients=['valorFormatado']
	
	static constraints = {
		valor nullable: false
		liberado nullable: false, default: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "orcamento_seq"]
	}
	
	String toString(){
		"${this.filial} ${this.periodo}"
	}
	
	String getValorFormatado(){
		try {
			NumberFormat formatter = new DecimalFormat("0.00");
			formatter.format(this.valor)
		} catch (Exception e){
			"0,00"
		}
	}

}