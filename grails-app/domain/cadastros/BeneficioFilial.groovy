package cadastros

import org.hibernate.envers.Audited;

@Audited

class BeneficioFilial implements Serializable {

	Beneficio beneficio
	Filial filial

	static constraints = { beneficio unique: 'filial' }

	static mapping = {
		id generator: "sequence", params:[sequence: "BeneficioFilial_seq"]
	}

	String getDscBeneficio() {
		return beneficio.descricao
	}
}
