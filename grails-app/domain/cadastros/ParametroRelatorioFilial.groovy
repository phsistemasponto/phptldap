package cadastros

import org.hibernate.envers.Audited

@Audited
class ParametroRelatorioFilial {
	
	ParametroRelatorio parametroRelatorio
	Filial filial
	
	static mapping = {
		id generator: "sequence", params:[sequence: "parametro_relatorio_filial_seq"]
	}
	
	String toString() {
		"${this.parametroRelatorio.descricao}"
	}

}