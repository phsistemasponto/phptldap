package cadastros

import org.hibernate.envers.Audited;

@Audited

class PeriodoFilial implements Serializable {
		
			Periodo periodo
			Filial filial
			String status
		
		
			static PeriodoFilial get(long periodoId, long filialId) {
				find 'from PeriodosFilial where periodo.id=:periodoId and filial.id=:filialId',
					[usuarioId: periodoId, filialId: filialId]
			}
		
			static PeriodoFilial create(Periodo periodo, Filial filial, boolean flush = false) {
				new PeriodoFilial(Periodos: periodo, Filial: filial).save(flush: flush, insert: true)
			}
		
			static constraints = {
				periodo unique: 'filial'
			}
			
			static mapping = {
				id generator: "sequence", params:[sequence: "PeriodoFilial_seq"]
				
			}
			
			String getDscPeriodo() {
				return periodo.dscPr
			}
		

}
