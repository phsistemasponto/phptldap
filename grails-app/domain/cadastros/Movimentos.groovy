package cadastros

import org.hibernate.envers.Audited

@Audited
class Movimentos {

	Funcionario funcionario
	Ocorrencias ocorrencia
	Date dtBat
	Integer nrQtdeHr
	String idSituacaoAbono

	static constraints = {
		nrQtdeHr maxSize:1, nullable:false, blank:false
		idSituacaoAbono maxSize:1, nullable:true,blank:true
		funcionario(unique: ['ocorrencia', 'dtBat'])
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "movimentos_seq"]
		funcionario column: "seq_func"
		ocorrencia column: "seq_ocor"
		dtBat type: "date"
		nrQtdeHr defaultValue: 0
	}
}
