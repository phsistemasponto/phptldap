package cadastros

import org.hibernate.envers.Audited

@Audited
class ParametroCriticaExtras {
	
	String admExtra50
	String admExtra100
	String naoAdmExtra50
	String naoAdmExtra100
	
	Integer bancoPosDescontoComp
	Integer bancoNegMaiorComp
	
	static hasMany = [filiais: ParametroCriticaExtrasFilial]
	
	static constraints = {
		admExtra50 nullable: false, blank: false
		admExtra100 nullable: false, blank: false
		naoAdmExtra50 nullable: false, blank: false
		naoAdmExtra100 nullable: false, blank: false
    }
	
	static mapping = {
		table "param_crit_ext"
		id generator: "sequence", params:[sequence: "param_crit_ext_seq"]
	}
	
}
