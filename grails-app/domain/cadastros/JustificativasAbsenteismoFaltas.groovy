package cadastros

import org.hibernate.envers.Audited


@Audited
class JustificativasAbsenteismoFaltas {

	Justificativas justificativas

	static mapping = {
		table "just_absen_falta"
		id generator: "sequence", params:[sequence: "just_absen_falta_seq"]
	}
}
