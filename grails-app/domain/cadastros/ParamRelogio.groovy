package cadastros

import org.hibernate.envers.Audited

@Audited
class ParamRelogio {

	Integer sequencial
	Integer posicaoInicial
	Integer posicaoFinal
	
	static hasOne = [indice: IndiceLayoutRelogio]

	static belongsTo = [relogio: Relogio]
	
	static constraints = {
		sequencial nullable: false
		posicaoInicial nullable: false
		posicaoFinal nullable: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "param_relogio_seq"]
	}

	String toString() {
		"${this.indice?.descricao}"
	}
}
