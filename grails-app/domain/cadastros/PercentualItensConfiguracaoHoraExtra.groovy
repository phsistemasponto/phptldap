package cadastros

import org.hibernate.envers.Audited

import funcoes.Horarios

@Audited
class PercentualItensConfiguracaoHoraExtra {
	
	ItensConfiguracaoHoraExtra itemConfig
	Tipohoraextras tipoHora
	Long sequencial
	
	Integer horaInicio
	Integer horaFim
	
	String horaInicioFormatada
	String horaFimFormatada
	
	static belongsTo = [itemConfig: ItensConfiguracaoHoraExtra]
	
	static constraints = {
		itemConfig nullable: false, blank: false
		tipoHora nullable: false, blank: false
		horaInicio nullable: false, blank: false
		horaFim nullable: false, blank: false
	}
	
   static transients = ['horaInicioFormatada','horaFimFormatada']

   static mapping = {
	   table "perc_itens_config_he"
	   id generator: "sequence", params:[sequence: "perc_itens_config_he_seq"]
   }

   String toString() {
	   "${this.horaInicio}"
   }
   
   String getHoraInicioFormatada() {
	   return horaInicio != null ? Horarios.retornaHora(horaInicio) : ''
   }
   
   String  getHoraFimFormatada() {
	   return horaFim != null ? Horarios.retornaHora(horaFim) : ''
   }



}
