package cadastros

import org.hibernate.envers.Audited

@Audited
class IndiceLayoutRelogio {

	String descricao
	
	static hasMany = [parametro: ParamRelogio]
	
	static constraints = {
		descricao maxSize:60, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "indice_layout_relogio_seq"]
	}

	String toString() {
		"${this.descricao}"
	}
	
}
