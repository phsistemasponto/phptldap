package cadastros

import org.hibernate.envers.Audited



@Audited
class Classificacao {
	String dscClass
	String idTpDia
	String aplClass
	String corExibicao

   static constraints = {
	   dscClass maxSize:30 , nullable: false, blank: false
	   idTpDia maxSize:3 , nullable: false, blank: false
	   aplClass maxSize:12 , nullable: false, blank: false
	   corExibicao maxSize:6, nullable: true, blank: true
	   }

   static mapping = {
	   id generator: "sequence", params:[sequence: "classificacao_seq"]
	   
	   
   }
   
   String toString() {
	   "${this.dscClass}"
   }



}
