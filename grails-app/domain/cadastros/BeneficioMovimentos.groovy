package cadastros

import org.hibernate.envers.Audited

@Audited
class BeneficioMovimentos implements Serializable {

	Funcionario funcionario
	Beneficio beneficio
	Integer quantidade
	Date dtInicio 
	Date dtFim
	Integer diasUteis

	static mapping = {
		id generator: "sequence", params:[sequence: "BeneficioMovimentos_seq"]
		dtInicio type: "date"
		dtFim type: "date"
	}
	
	
}
