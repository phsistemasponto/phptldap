package cadastros

import org.hibernate.envers.Audited

@Audited
class Dados {

	Funcionario funcionario
	Horario horario
	Perfilhorario perfilhorario
	Date dtBat
	
	Integer hrBat1
	Integer hrBat2
	Integer hrBat3
	Integer hrBat4
	Integer hrBat5
	Integer hrBat6
	Integer hrBat7
	Integer hrBat8
	Integer hrBat9
	Integer hrBat10
	Integer hrBat11
	Integer hrBat12
	Integer hrBat13
	Integer hrBat14
	
	Integer nrQtdeChPrevista
	Integer nrQtdeChEfetivada
	String idTpDia
	Integer nrDesconto
	
	Justificativas justificativa1 
	Justificativas justificativa2
	Justificativas justificativa3
	Justificativas justificativa4
	Justificativas justificativa5
	Justificativas justificativa6
	
	Ocorrencias ocorrencia1 
	Ocorrencias ocorrencia2
	Ocorrencias ocorrencia3
	Ocorrencias ocorrencia4
	Ocorrencias ocorrencia5
	Ocorrencias ocorrencia6
	
	String  stleitura
	
	static constraints = {
		idTpDia maxSize:5, nullable:false, blank:false
		stleitura maxSize:1, nullable:true,blank:false
		hrBat1 nullable:true
		hrBat2 nullable:true
		hrBat3 nullable:true
		hrBat4 nullable:true
		hrBat5 nullable:true
		hrBat6 nullable:true
		hrBat7 nullable:true
		hrBat8 nullable:true
		hrBat9 nullable:true
		hrBat10 nullable:true
		hrBat11 nullable:true
		hrBat12 nullable:true
		hrBat13 nullable:true
		hrBat14 nullable:true
		justificativa1 nullable:true
		justificativa2 nullable:true
		justificativa3 nullable:true
		justificativa4 nullable:true
		justificativa5 nullable:true
		justificativa6 nullable:true
		ocorrencia1 nullable:true
		ocorrencia2 nullable:true
		ocorrencia3 nullable:true
		ocorrencia4 nullable:true
		ocorrencia5 nullable:true
		ocorrencia6 nullable:true
		
		nrQtdeChPrevista nullable:true
		nrQtdeChEfetivada nullable:true
		nrDesconto nullable:true
		perfilhorario nullable:true
		
		funcionario unique: 'dtBat'
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "dados_seq"]
		funcionario column: "seq_func"
		horario column: "seq_horario"
		perfilhorario column: "id_perfil_horario"
		dtBat type: "date"
		justificativa1 column: "seq_justif1"
		justificativa2 column: "seq_justif2"
		justificativa3 column: "seq_justif3"
		justificativa4 column: "seq_justif4"
		justificativa5 column: "seq_justif5"
		justificativa6 column: "seq_justif6"
		ocorrencia1 column: "seq_ocor1"
		ocorrencia2 column: "seq_ocor2"
		ocorrencia3 column: "seq_ocor3"
		ocorrencia4 column: "seq_ocor4"
		ocorrencia5 column: "seq_ocor5"
		ocorrencia6 column: "seq_ocor6"
	}
	
}
