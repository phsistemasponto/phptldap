package cadastros

import org.hibernate.envers.Audited;

@Audited
class Tipohoraextras {
	
	String dscTpHorExt
	Long vrIndHorExt
	String formulaTpExt
	String tipExt
	String nmAcjef


	static constraints = {
		dscTpHorExt maxSize:30, nullable:false, blank:false
		formulaTpExt maxSize:40, nullable:true, blank:false
		tipExt maxSize:1, nullable:false, blank:false
		nmAcjef maxSize:10, nullable:false, blank:false
		
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "tphorext_seq"]

	}
	

	String toString() {
		"${this.dscTpHorExt}"
	}


}
