package cadastros

import org.hibernate.envers.Audited;

@Audited
class Setor {
	
	Filial filial
	String dscSetor
	String aplSetor
	
	 static constraints = {
		  dscSetor maxSize:80, nullable:false, blank:false
		  aplSetor maxSize:80, nullable: true, blank: false
		  
}







static mapping = {
	id generator: "sequence", params:[sequence: "setor_seq"]
	filial lazy: false
}

String toString() {
	"${this.dscSetor}"
}



}
