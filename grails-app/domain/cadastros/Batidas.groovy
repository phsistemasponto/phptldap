package cadastros

import org.hibernate.envers.Audited

@Audited
class Batidas {
	
	Funcionario funcionario
	Relogio relogio
	Date dtBat
	Integer hrBat
	String status
    String idLido
	String idBloqueado
	String idBat
	Integer idObs
	Integer seqTipBat


	static constraints = {
		relogio nullable:true
		status maxSize:1, nullable:true
		idBat maxSize:1, nullable:true
		idBloqueado maxSize:1, nullable:true
		idLido maxSize:1, nullable:true
		seqTipBat  nullable:true
		idObs  nullable:true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "batidas_seq"]
		funcionario column: "seq_func"
		dtBat type: "date"
	}
	
	

}
