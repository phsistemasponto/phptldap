package cadastros

import java.text.SimpleDateFormat

import org.apache.commons.lang.builder.HashCodeBuilder
import org.hibernate.envers.Audited;


@Audited

class FuncionarioHorario implements Serializable{

	Funcionario funcionario
	Horario horario
	Long sequencia
	Date dtIniVig
	Date dtFimVig
	Long nrIndHorario
	
	String dataInicioFormatada
	String dataFimFormatada

    static constraints = {
        dtFimVig nullable: true
		nrIndHorario nullable: true
    }

    static mapping = {
		id generator: "sequence", params:[sequence: "funcionario_horario_seq"]
        version false
    }
	
	static transients = ['dataInicioFormatada','dataFimFormatada']

    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append funcionario
        builder.append horario
        builder.toHashCode()
    }
	
	String getDataInicioFormatada() {
		new SimpleDateFormat("dd/MM/yyyy").format(dtIniVig)
	}
	
	String getDataFimFormatada() {
		dtFimVig ? new SimpleDateFormat("dd/MM/yyyy").format(dtFimVig) : ''
	}


}
