package cadastros

import org.hibernate.envers.Audited

@Audited
class Tipotolerancia {
	String dsctolerancia
	Long nrTolAntEnt
	Long nrTolAntSai
	Long nrTolAtrEnt
	Long nrTolAtrSai

	static hasMany = [ filiais: TipotoleranciaFilial ]

	static constraints = {
		dsctolerancia maxSize:30 , nullable: false, blank: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "tipotole_seq"]
	}


	String toString() {
		"${this.dsctolerancia}"
	}
}
