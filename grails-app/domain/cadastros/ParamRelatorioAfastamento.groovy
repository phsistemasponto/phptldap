package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class ParamRelatorioAfastamento {
	
	String descricao
	Usuario usuario
	
	static hasMany = [justificativas: ParamRelatorioAfastamentoJustificativa]
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		table "p_rel_af"
		id generator: "sequence", params:[sequence: "p_rel_af_seq"]
	}

	String toString() {
		"${this.descricao}"
	}

}