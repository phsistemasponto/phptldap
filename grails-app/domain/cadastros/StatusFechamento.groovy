package cadastros

import org.hibernate.envers.Audited


class StatusFechamento {

	Funcionario funcionario
	Periodo periodo
	Date dataCriacao
	Long codUsu
	
		
	
	static mapping = {
		id generator: "sequence", params:[sequence: "status_fechamento_seq"]
		funcionario column: "seq_func"
		periodo column: "seq_pr"
	}

	
	
	
}
