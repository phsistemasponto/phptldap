package cadastros

import org.hibernate.envers.Audited;

@Audited
class Uniorg {
		String codUniorg
		String dscUniorg	
		
	
	
		static constraints = {
			codUniorg maxSize:12, nullable:false, blank:false
			dscUniorg maxSize:80, nullable:false, blank:false
		}
	
		static mapping = {
			id generator: "sequence", params:[sequence: "uniorg_seq"]
	
		}
		
	
		String toString() {
			"${this.dscUniorg}"
		}
	

}
