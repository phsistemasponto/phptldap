package cadastros

import org.hibernate.envers.Audited;

@Audited
class Tipofunc {
	String dsctpfunc
	String idtpfunc

	static constraints = {
		dsctpfunc maxSize:20, nullable: false, blank: false
		idtpfunc maxSize:2, nullable: false, blank: false
	}


	static mapping = {
		id generator: "sequence", params:[sequence: "tpfunc_seq"]
	}

	String toString(){
		"${this.dsctpfunc}"
	}
}
