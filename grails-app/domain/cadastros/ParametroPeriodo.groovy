package cadastros

import org.hibernate.envers.Audited

@Audited
class ParametroPeriodo {
	
	String padraoNome
	Integer diaInicio
	Boolean diaFimUltimoMes
	Integer diaFim
	
	static hasMany = [filiais: ParametroPeriodoFilial]
	
	static constraints = {
		padraoNome nullable: false, blank: false
		diaInicio nullable: false
		diaFimUltimoMes nullable: false
		diaFim nullable: true, blank: true
    }
	
	static mapping = {
		id generator: "sequence", params:[sequence: "parametro_periodo_seq"]
	}
	
}
