package cadastros

import org.hibernate.envers.Audited;

@Audited
class SaldoBh {
	
	Funcionario funcionario
	Periodo periodo
	Ocorrencias ocorrencias
	Long vrSldAnt
	Long vrSldAtual
	Long vrSldMes
	Long vrCred
	Long vrDeb
	Long vrPago
	
	
	
	
	static mapping = {
		id generator: "sequence", params:[sequence: "saldo_bh_seq"]
		funcionario column: "seq_func"
		ocorrencias column: "seq_ocor"
		periodo column: "seq_pr"
		vrSldAtual defaultValue: 0
		vrSldMes defaultValue: 0
		vrSldAnt defaultValue: 0
		}

	

}
