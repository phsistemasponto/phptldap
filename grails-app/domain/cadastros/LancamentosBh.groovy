package cadastros

import java.text.SimpleDateFormat

import org.hibernate.envers.Audited

import acesso.Usuario
import funcoes.Horarios

@Audited

class LancamentosBh {

	Funcionario funcionario
	Ocorrencias ocorrencias
	Usuario usuario

	Date dtLanc
	Date dtRef

	Long nrQtdeHr

	String idTpLanc
	String observacao

	String idTpLancString
	String dtLancExibicao
	String dtRefExibicao
	String nrQtdeHrExibicao

	static transients = ['idTpLancString', 'dtLancExibicao', 'dtRefExibicao', 'nrQtdeHrExibicao']


	static constraints = {
		idTpLanc  maxSize:1, nullable: false, blank: false
		dtRef nullable: true
		observacao maxSize:200, nullable: true, blank: true
	}


	static mapping = {
		id generator: "sequence", params:[sequence: "lancamentos_bh_seq"]
		funcionario column: "seq_func"
		ocorrencias column: "seq_ocor"
		observacao column: "obs"
		dtLanc type: "date"
		dtRef type: "date"
		nrQtdeHr defaultValue: 0
	}
	
	public String getIdTpLancString() {
		return idTpLanc =! null ? (idTpLanc.equals("C") ? "Cr�dito" : "D�bito") : "";
	}
	
	public String getDtLancExibicao() {
		if (this.dtLanc){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(this.dtLanc)
		} else {
			""
		}
	}
	
	public String getDtRefExibicao() {
		if (this.dtRef){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(this.dtRef)
		} else {
			""
		}
	}
	
	public String getNrQtdeHrExibicao() {
		return nrQtdeHr ? Horarios.retornaHora(nrQtdeHr) : ''
	}
}
