package cadastros

import org.hibernate.envers.Audited

@Audited
class BeneficioParamJustificativa {

	BeneficioParam beneficioParam
	Justificativas justificativas
	Integer quantidade
	
	static constraints = {
		quantidade nullable:false, blank:false
	}

	static mapping = {
		table "ben_param_just"
		id generator: "sequence", params:[sequence: "ben_param_just_seq"]
	}
	
}