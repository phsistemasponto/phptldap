package cadastros

import org.hibernate.envers.Audited

@Audited
class BeneficioFuncFltsAbonadas implements Serializable {

	Beneficio beneficio
	Funcionario funcionario
	Ocorrencias ocorrencias
	Justificativas justificativas
	Date dtInicio
	Date dtFim
	Date dtProc
	Integer quantidade

	static mapping = {
		table "ben_func_flt_abon"
		id generator: "sequence", params:[sequence: "ben_func_flt_abon_seq"]
		dtInicio type: "date"
		dtFim type: "date"
	}
	
}
