package cadastros


import java.text.SimpleDateFormat

import org.hibernate.envers.Audited

@Audited
class FeriadosRegionais {

	Date dtFer
	String dscFer
	String tipo
	String idParcial

	String dtFerExibicao

	static constraints = {
		dscFer maxSize:80, nullable:false, blank:false
		tipo maxSize:1, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "feriados_regionais_seq"]
	}
	
	static transients = ['dtFerExibicao']
	
	String getDtFerExibicao(){
		if (this.dtFer){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(this.dtFer)
		} else {
			""
		}
	}
	
	List getFeriadosRegionaisFiliais(){
		def filiais = Filial.listOrderByUnicodFilial()
		def item = []
		def listaRetorno = []
		def temFilial
		FeriadosRegionaisFilial feriadoRegionalFilial
		filiais.each(){
			if (this.id){
				feriadoRegionalFilial = FeriadosRegionaisFilial.find("from FeriadosRegionaisFilial where feriadosRegionais = :feriado and filial = :filial", 
					[feriado: this, filial: it])
				temFilial = 'true'
			} else {
				feriadoRegionalFilial = null
				temFilial = 'false'
			}
			temFilial=(feriadoRegionalFilial)?'true':'false'
			item = [filial: it, temFilial: temFilial]
			listaRetorno.add(item)
		}
		return listaRetorno
	}
	
}
