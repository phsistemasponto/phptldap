package cadastros

import org.hibernate.envers.Audited

@Audited
class ArquivoFolhaLayout {
  
  ArquivoFolha arquivofolha
  ArquivoFolhaCampo arquivofolhacampo
  Integer layoutLinha
  Integer layoutInicio
  Integer layoutfim
  String  layoutcampofixo
  String  layoutpreenchimento
  Integer layoutdirecao
  
  static constraints = {
	  
	  layoutcampofixo maxSize:80, nullable:true,blank:false
	  layoutpreenchimento maxSize:4, nullable:true,blank:false
  }

  static mapping = {
	  id generator: "sequence", params:[sequence: "arquivofolhalayout_seq"]
	  
  }

  
  
  
  
}
