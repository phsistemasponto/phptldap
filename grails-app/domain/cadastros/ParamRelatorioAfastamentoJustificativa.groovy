package cadastros

import org.hibernate.envers.Audited

@Audited
class ParamRelatorioAfastamentoJustificativa {
	
	ParamRelatorioAfastamento param
	Justificativas justificativa
	
	static constraints = {
		
	}

	static mapping = {
		table "p_rel_af_just"
		id generator: "sequence", params:[sequence: "p_rel_af_just_seq"]
	}

	String toString() {
		"${this.justificativa}"
	}

}