package cadastros

import java.text.SimpleDateFormat

import org.apache.commons.lang.builder.HashCodeBuilder
import org.hibernate.envers.Audited;


@Audited
class FuncionarioFilial implements Serializable{

	Funcionario funcionario
	Filial filial
	Date dtIniVig
	Date dtFimVig
	
	String dataInicioFormatada
	String dataFimFormatada

    static constraints = {
        dtFimVig nullable: true
    }

    static mapping = {
		id generator: "sequence", params:[sequence: "funcionario_filial_seq"]
        version false
    }
	
	static transients = ['dataInicioFormatada','dataFimFormatada']

    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append funcionario
        builder.append filial
        builder.toHashCode()
    }
	
	String getDataInicioFormatada() {
		new SimpleDateFormat("dd/MM/yyyy").format(dtIniVig)
	}
	
	String getDataFimFormatada() {
		dtFimVig ? new SimpleDateFormat("dd/MM/yyyy").format(dtFimVig) : ''
	}


}
