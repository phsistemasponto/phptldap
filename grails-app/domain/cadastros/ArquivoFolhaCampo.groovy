package cadastros

import org.hibernate.envers.Audited

@Audited

class ArquivoFolhaCampo {
	
	String dscCampo
	
	static constraints = {
		dscCampo maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "Arquivofolhacampo_seq"]

	}

}
