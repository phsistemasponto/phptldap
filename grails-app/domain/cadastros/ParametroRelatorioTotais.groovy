package cadastros

import org.hibernate.envers.Audited

@Audited
class ParametroRelatorioTotais {
	
	String descricao
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		table "param_rel_totais"
		id generator: "sequence", params:[sequence: "param_rel_totais_seq"]
	}

	String toString() {
		"${this.id} - ${this.descricao}"
	}

}