package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class Grupo {
	
	Usuario usuario
	String dscGrupo
	
	static constraints = {
		dscGrupo maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "grupo_seq"]
	}
	

	String toString() {
		"${this.dscGrupo}"
	}

}
