package cadastros



import funcoes.Horarios;


class AdcNoturno {
	Ocorrencias ocorrencias
	String dscAdc
	Long hrInicio
	Long hrFim
	Long minTol
	
	String hrInicioFormatada
	String hrFimFormatado
	
	static transients = ['hrInicioFormatada','hrFimFormatado']
	
	
	static constraints = {
		dscAdc maxSize:20 , nullable: false, blank: false
		hrInicio nullable: false
		hrFim nullable: false
		minTol nullable: false
		 
		
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "adcnot_seq"]
		ocorrencias lazy: false
	}


	String toString() {
		"${this.dscAdc}"
	}
	
	
	String getHrInicioFormatada(){
		return Horarios.retornaHora(hrInicio)
	}
	
	String getHrFimcioFormatada(){
		return Horarios.retornaHora(hrFim)
	}

	


}
