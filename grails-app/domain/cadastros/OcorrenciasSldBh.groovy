package cadastros
import org.hibernate.envers.Audited;
@Audited
class OcorrenciasSldBh {
	
	Funcionario funcionario
	ObservacaoSldBh observacaoSldBh
	Periodo periodo
	Periodo periodoProc
	Long nrQtdeHr
	String idTpLanc
	String observacao
	
	static constraints = {
		idTpLanc  maxSize:1, nullable: true, blank: false
		observacao maxSize:200, nullable: true, blank: false
			}

	
	
	static mapping = {
		id generator: "sequence", params:[sequence: "ocorrencias_sld_bh_seq"]
		funcionario column: "seq_func"
		periodoProc column: "seq_pr_proc"
		periodo column: "seq_pr"
		nrQtdeHr defaultValue :0
		
		}


}
