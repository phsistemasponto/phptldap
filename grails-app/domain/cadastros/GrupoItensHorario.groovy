package cadastros

import org.hibernate.envers.Audited

@Audited
class GrupoItensHorario {
	
	GrupoHorario grupoHorario
	Integer horario

	static constraints = {
		horario nullable:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "grupo_itens_horario_seq"]
	}
	
	static void removeAll(GrupoHorario grupoHorario) {
		executeUpdate 'DELETE FROM GrupoItensHorario WHERE grupoHorario=:grupoHorario', [grupoHorario: grupoHorario]
	}
	
}
