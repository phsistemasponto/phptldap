package cadastros

import org.hibernate.envers.Audited

@Audited
class Abonos {
	
	Funcionario funcionario
	Date dtInicio
	Date dtFim
	Integer qtdadeDias
	Ocorrencias ocorrencias
	String idSituacaoAbono
	Integer nrQtdeHrJustif
	Justificativas justificativas
	Date dtJustificativa
	String obsJustificativa
	Ocorrencias ocorrenciasOld
	String cidId

	static constraints = {
		ocorrenciasOld nullable:true
		cidId nullable:true
		obsJustificativa nullable:true, blank: true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "abonos_seq"]
		dtInicio type: "date"
		dtFim type: "date"
		dtJustificativa type: "date"
		funcionario column: "seq_func"
		ocorrencias column: "seq_ocor"
		justificativas column: "seq_justif"
		ocorrenciasOld column: "seq_ocor_old"
	}

}
