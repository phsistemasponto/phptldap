package cadastros

import org.hibernate.envers.Audited;

@Audited
class TipoDeBeneficio {
	String descricao
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "TipoDeBeneficio_seq"]
	}
	

	String toString() {
		"${this.descricao}"
	}


}
