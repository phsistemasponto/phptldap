package cadastros

import org.hibernate.envers.Audited

import funcoes.Horarios

@Audited
class AgendamentoProcesso {

	Integer diaSemana
	Long horaExecucao
	
	static constraints = {
		diaSemana nullable:false
		horaExecucao nullable:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "agendamento_processo_seq"]
	}
	
	static hasMany = [filiais: AgendamentoProcessoFilial]
	
	String getDiaSemanaExibicao() {
		if (diaSemana != null) { 
			if (diaSemana == 0) {
				return "Domingo"
			} else if (diaSemana == 1) {
				return "Segunda"
			} else if (diaSemana == 2) {
				return "Ter�a"
			} else if (diaSemana == 3) {
				return "Quarta"
			} else if (diaSemana == 4) {
				return "Quinta"
			} else if (diaSemana == 5) {
				return "Sexta"
			} else if (diaSemana == 6) {
				return "S�bado"
			}
		} 
		return ""
	}
	
	String getHoraExecucaoExibicao() {
		if (horaExecucao) {
			return Horarios.retornaHora(horaExecucao)
		}
		return ""
	}
	
}
