package cadastros

import java.text.*

import org.apache.commons.lang.builder.HashCodeBuilder
import org.hibernate.envers.Audited


@Audited
class FuncionarioBeneficioExtra implements Serializable{

	Funcionario funcionario
	Beneficio beneficio
	Date dtBeneficio
	Date dtTransacao
	Long quantidade
	
	static constraints = {
	    dtBeneficio nullable: false
		dtTransacao nullable: false
		quantidade nullable: false
	}

	static mapping = {
		table "func_ben_ext"
		id generator: "sequence", params:[sequence: "func_ben_ext_seq"]
	    version false
	}

	int hashCode() {
	    def builder = new HashCodeBuilder()
	    builder.append funcionario
	    builder.append beneficio
	    builder.append dtIniVig
	    builder.toHashCode()
	}
	
	String toString() {
		"Funcionario: ${this.funcionario} Beneficio : ${this.beneficio}"
	}

}
