package cadastros

import org.hibernate.envers.Audited;

@Audited
class Municipio {
	String codMun
	String descricao
	String sgl_uf

	static constraints = {
		codMun blank: false, nullable: false, maxSize: 12
		descricao blank: false, nullable: false, maxSize: 80
		sgl_uf blank: true, nullable: true, maxSize: 2
		

	}

	static mapping = {
		id generator: "sequence", params:[sequence: "municipio_seq"]

	}
	
	String toString() {
		"${this.descricao}"
	}


}
