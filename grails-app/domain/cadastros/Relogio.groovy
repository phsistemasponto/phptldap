package cadastros

import org.hibernate.envers.Audited

@Audited
class Relogio {

	Filial filial
	String dscRel
	String fabricante
	String idOnline
	String idAfd
	String idAfdt
	String idEntSai
	String conteudoArquivo

	static hasMany = [parametros: ParamRelogio]
	
	static constraints = {
		dscRel maxSize:60, nullable:false, blank:false
		fabricante maxSize:40, nullable:true, blank:false
		idOnline maxSize:1, nullable:false
		idAfd maxSize:1, nullable:false
		idAfdt maxSize:1, nullable:false
		idEntSai maxSize:1, nullable:false
		conteudoArquivo maxSize:250, nullable:true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "relogio_seq"]
		filial lazy: false
	}

	String toString() {
		"${this.dscRel}"
	}
	
}