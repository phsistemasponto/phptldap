package cadastros

import org.hibernate.envers.Audited


@Audited
class Justificativas {

	String dscJust
	String idLimpaDesc
	String idDebitaBh
	String idTpJust
	Long seqTpBh
	String seqOutSistem
	String idEscala
	String idAdcionalnot
	String idNewMatricula
	String idCidsn
	Long vrAumentoHr
	Long vrDiminuiHr
	Long vrDiasValido
	String idSuspensao

	String idTpJustString

	static transients = ['idTpJustString']

	static constraints = {
		dscJust maxSize:40, nullable:false, blank:false
		idLimpaDesc blank: true, nullable: true, maxSize: 2
		idDebitaBh blank: true, nullable: true, maxSize: 2
		idTpJust blank: true, nullable: true, maxSize: 3
		seqOutSistem blank: true, nullable: true, maxSize: 12
		seqTpBh nullable: true
		idEscala nullable: true ,maxSize: 3
		idAdcionalnot blank: true, nullable: true, maxSize: 2
		idNewMatricula  blank: true, nullable: true, maxSize: 2
		idCidsn blank: true, nullable: true, maxSize: 2
		vrAumentoHr nullable: true
		vrDiminuiHr nullable: true
		vrDiasValido nullable: true
		idSuspensao blank: true, nullable: true, maxSize: 2
	}







	static mapping = {
		id generator: "sequence", params:[sequence: "justificativa_seq"]
		vrDiasValido defaultValue: 0
	}

	String toString() {
		"${this.dscJust}"
	}

	String getIdTpJustString(){
		(this.idTpJust=='AB')?'ABONOS':'AFASTAMENTOS'
	}
}
