package cadastros

import org.hibernate.envers.Audited

@Audited
class ObservacaoFuncionario {
	
	Funcionario funcionario
	Date dtBat
	String observacao
	String status

	static constraints = {
		funcionario nullable: false, unique: ['dtBat']
		observacao maxSize: 2048, nullable: false, blank: false
		status maxSize: 1, nullable: false, blank: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "observacao_funcionario_seq"]
		status defaultValue: "'A'"
	}
	
	String toString() {
		"${this.funcionario.nomFunc} - ${this.dtBat} - ${this.observacao}"
	}


}
