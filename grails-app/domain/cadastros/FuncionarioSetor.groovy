package cadastros

import java.text.SimpleDateFormat

import org.apache.commons.lang.builder.HashCodeBuilder
import org.hibernate.envers.Audited;

@Audited
class FuncionarioSetor implements Serializable{

	Funcionario funcionario
	Setor setor
	Date dtIniVig
	Date dtFimVig
	
	String dataInicioFormatada
	String dataFimFormatada

    static constraints = {
        dtFimVig nullable: true
    }

    static mapping = {
		id generator: "sequence", params:[sequence: "funcionario_setor_seq"]
        version false
    }
	
	static transients = ['dataInicioFormatada','dataFimFormatada']

    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append funcionario
        builder.append setor
        builder.toHashCode()
    }
	
	String getDataInicioFormatada() {
		new SimpleDateFormat("dd/MM/yyyy").format(dtIniVig)
	}
	
	String getDataFimFormatada() {
		dtFimVig ? new SimpleDateFormat("dd/MM/yyyy").format(dtFimVig) : ''
	}


}
