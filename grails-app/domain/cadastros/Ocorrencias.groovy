package cadastros

import org.hibernate.envers.Audited;

@Audited
class Ocorrencias {
	String dscOcor
	String idTpOcor 
	String formulaOcor
	String formulaOcorBh
	Long indiceBh 
	Long indiceExt
	Tipohoraextras tipohoraextras
	
	String idTpOcorString
	
			
		static transients = ['idTpOcorString']
	

	static constraints = {
			   dscOcor maxSize:60, nullable: false, blank: false
			   idTpOcor  maxSize:1, nullable: false, blank: false
			   formulaOcor maxSize:150, nullable: true, blank: false
			   formulaOcorBh maxSize:150, nullable: true, blank: false		   
			   indiceExt nullable: true;
			   indiceBh nullable: true;
			   tipohoraextras nullable: true;

	}

	
	static mapping = {
		id generator: "sequence", params:[sequence: "ocorrencias_seq"]
		tipohoraextras lazy: false
		}
	
	String toString(){
		"${this.dscOcor}"
	}

	String getIdTpOcorString(){
		(this.idTpOcor=='P')?'PROVENTOS':'DESCONTOS'
	}

}
