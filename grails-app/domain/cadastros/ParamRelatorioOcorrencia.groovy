package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class ParamRelatorioOcorrencia {
	
	String descricao
	Usuario usuario
	
	static hasMany = [ocorrencias: ParamRelatorioOcorrenciaOcorrencia]
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		table "p_rel_ocor"
		id generator: "sequence", params:[sequence: "p_rel_ocor_seq"]
	}

	String toString() {
		"${this.descricao}"
	}

}