package cadastros

import org.hibernate.envers.Audited

@Audited
class ParamRelatorioHorasExtrasOcorrencia {
	
	ParamRelatorioHorasExtras param
	Ocorrencias ocorrencia
	
	static constraints = {
		
	}

	static mapping = {
		table "p_rel_he_ocor"
		id generator: "sequence", params:[sequence: "p_rel_he_ocor_seq"]
	}

	String toString() {
		"${this.ocorrencia}"
	}

}