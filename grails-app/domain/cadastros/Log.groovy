package cadastros

import org.hibernate.envers.Audited


@Audited
class Log {

	Funcionario funcionario
	LogMen mensagem
	Date dtLog
	Date dtBat

	static mapping = {
		id generator: "sequence", params:[sequence: "log_seq"]
		funcionario column: "seq_func"
		mensagem column: "id_log"
		dtLog type: "date"
		dtBat type: "date"
	}

	String toString() {
		"${this.id}"
	}
	
}
