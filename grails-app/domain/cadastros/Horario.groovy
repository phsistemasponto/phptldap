package cadastros

import java.text.SimpleDateFormat

import org.hibernate.envers.Audited;

import funcoes.Horarios

@Audited
class Horario {
     String dscHorario
	 Date dtIniVig
	 
	 Long cargaHorariaTotal
	 String cargaHorariaTotalHoras
	 
	 static hasMany = [itensHorario: Itemhorario]
	 static transients = ['dtIniVigFormatada', 'cargaHorariaTotal', , 'cargaHorariaTotalHoras']
	 
	static constraints = {
		dscHorario maxSize:80 , nullable: false, blank: false
		dtIniVig nullable: false
		}

	
	
	static mapping = {		
		id generator: "sequence", params:[sequence: "horario_seq"]
		dtIniVig type: "date"
		 
		
		
	}
	
	
	private String getDataFormatada(Date data){
		try {
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(data)
		} catch(Exception e) {
			return ""
		}
		}
	

	String toString() {
		"${this.dscHorario}"
	}

	String getDtIniVigFormatada(){
		getDataFormatada(this.dtIniVig)
	}
	
	Long getCargaHorariaTotal() {
		def totalizacao = Itemhorario.executeQuery('select sum(qtdechefetiva) from Itemhorario i where i.horario =:horario',[horario: this])
		if (this.id){
			return totalizacao[0]
		} else {
			return 0
		}
	}

	String getCargaHorariaTotalHoras() {
		Horarios.retornaHora(this.getCargaHorariaTotal())
	}	
}
