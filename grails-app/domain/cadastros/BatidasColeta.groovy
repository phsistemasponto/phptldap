package cadastros

import acesso.Usuario

class BatidasColeta {

	Date creation
	Relogio relogio
	Usuario usuario

	static mapping = {
		id generator: "sequence", params:[sequence: "batida_coleta_seq"]
	}
	
	static hasOne = {
		arquivo: ArquivoBatidaColeta
	}
}