package cadastros

import org.hibernate.envers.Audited;

@Audited
class ConfiguracaoHoraExtra {

	String descricao
	String horaExtraAntesExpediente
	String horaExtraIntervaloRefeicao
	
	static hasMany = [itens: ItensConfiguracaoHoraExtra]

	static constraints = {
		descricao maxSize:40, nullable: false, blank: false
		horaExtraAntesExpediente maxSize:1, nullable: false, blank: false
		horaExtraIntervaloRefeicao maxSize:1, nullable: false, blank: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "config_he_seq"]
	}

	String toString() {
		"${this.descricao}"
	}
}
