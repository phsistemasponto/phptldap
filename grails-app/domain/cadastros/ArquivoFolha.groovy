package cadastros

import org.hibernate.envers.Audited

@Audited
class ArquivoFolha {
	
	String dscArquivo
	
	static hasMany = [layouts: ArquivoFolhaLayout]
	
	static constraints = {
		dscArquivo maxSize:80, nullable:false, blank:false		
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "ArquivoFolha_seq"]

	}

	
}
