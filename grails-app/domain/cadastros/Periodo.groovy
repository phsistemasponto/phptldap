package cadastros

import java.text.SimpleDateFormat

import org.hibernate.envers.Audited

@Audited
class Periodo {
	String dscPr
	String apelPr
	Date dtIniPr
	Date dtFimPr
	String idStsPr
	String idStsBh
	
	static transients = ['dtIniPrFormatada', 'dtFimPrFormatada']
	
	static constraints = {
		  dscPr maxSize:80, nullable:false, blank:false
		  apelPr maxSize:40, nullable:true, blank:false
		  dtIniPr nullable:false
		  dtFimPr nullable:false
		  idStsPr maxSize:1,nullable:true
		  idStsBh maxSize:1,nullable:true
	}



	static mapping = {
		id generator: "sequence", params:[sequence: "periodos_seq"]
		dtIniPr type: "date"
		dtFimPr type: "date"
	}

	private String getDataFormatada(Date data){
		try {
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(data)
		} catch(Exception e) {
			return ""
		}
	}	

	String toString() {
		"${this.dscPr}"
	}


   String getDtIniPrFormatada(){
	   getDataFormatada(this.dtIniPr)
   }

   String getDtFimPrFormatada(){
	   getDataFormatada(this.dtFimPr)
   }

   List getPeriodoFiliais(){
	   def filiais = Filial.listOrderByUnicodFilial()
	   def item = []
	   def listaRetorno = []
	   def temFilial
	   def statusPeriodo
	   PeriodoFilial periodoFilial
	   filiais.each(){
		   if (this.id){
			   periodoFilial = PeriodoFilial.findByPeriodoAndFilial(this, it)
			   temFilial = 'true'
		   } else {
		   	    periodoFilial = null
				temFilial = 'false'
		   }
		   temFilial=(periodoFilial)?'true':'false'
		   println('TEMFILIAL: '+temFilial)
		   statusPeriodo=(temFilial=='true')?periodoFilial.status:'A'
		   item = [filial: it, temFilial: temFilial, statusPeriodo: statusPeriodo]
		   listaRetorno.add(item)
	   }
	   return listaRetorno
   }
   
}
