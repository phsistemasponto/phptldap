package cadastros



import org.hibernate.envers.Audited

import funcoes.Horarios

@Audited
class Empresa {
	
	String dscEmp
	String smtpServidor
	Long smtpPorta
	String emailSmtp
	String nomeSmtp
	String smtpUsuario
	String smtpSenha
	Long cargaInterJor
	Long cargaLimtSemanalNot
	Long cargaLimtMensal
	Long cargaLimtSemanal
	String chekAbonos
	String idTpPesq
	String idCriaSeq
	String idDoc
	String idSal

	String cargaInterJorHoras
	String cargaLimtSemanalNotHoras
	String cargaLimtMensalHoras
	String cargaLimtSemanalHoras

	static transients = [
		'cargaInterJorHoras',
		'cargaLimtSemanalNotHoras',
		'cargaLimtMensalHoras',
		'cargaLimtSemanalHoras'
	]
	
	static hasMany = [
		foto: EmpresaFoto
	]

	static constraints = {
		dscEmp maxSize:80, nullable:false, blank:false
		smtpServidor blank: true, nullable: true, maxSize: 80
		emailSmtp blank: true, nullable: true, maxSize: 80
		nomeSmtp blank: true, nullable: true, maxSize: 80
		smtpUsuario blank: true, nullable: true, maxSize: 80
		smtpSenha blank: true, nullable: true, maxSize: 80
		chekAbonos maxSize:2
		idTpPesq maxSize:20
		idCriaSeq maxSize:2
		idDoc maxSize:2
		idSal maxSize:2
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "empresa_seq"]
	}

	String toString() {
		"${this.dscEmp}"
	}

	String getCargaInterJorHoras(){
		Horarios.retornaHora(this.cargaInterJor)
	}


	String getCargaLimtSemanalNotHoras(){
		Horarios.retornaHora(this.cargaLimtSemanalNot)
	}


	String getCargaLimtMensalHoras(){
		Horarios.retornaHora(this.cargaLimtMensal)
	}


	String getCargaLimtSemanalHoras(){
		Horarios.retornaHora(this.cargaLimtSemanal)
	}
}
