package cadastros

import org.hibernate.envers.Audited


@Audited
class LogMen {

	String descricao

	static constraints = {
		descricao maxSize:30, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "log_men_seq"]
	}

	String toString() {
		"${this.descricao}"
	}
	
}
