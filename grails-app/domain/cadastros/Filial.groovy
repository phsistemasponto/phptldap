package cadastros

import org.hibernate.envers.Audited

@Audited
class Filial {
	Empresa empresa
	Municipio municipio
	String dscFil
	String dscFantFil
	String cnpjFil
	String dscLog
	String cmpLog
	String nmLog
	String baiLog
	String cepLog
	String atvFil
	String unicodFilial
	String idHabilitada
	String idPgHrextAdm
    String dscFantFilString
	
	static transients = ['dscFantFilString']

	static constraints = {
			   dscFil maxSize:80, nullable: false, blank: false
			   cnpjFil  maxSize:60, nullable: false, blank: false
			   dscFantFil maxSize:80, nullable: false, blank: false
			   dscLog maxSize:60, nullable: true, blank: false
			   cmpLog maxSize:60, nullable: true, blank: false
			   nmLog maxSize:6, nullable: true, blank: false
			   baiLog maxSize:60, nullable: true, blank: false
			   cepLog maxSize:15, nullable: true, blank: false
			   atvFil maxSize:40, nullable: true, blank: false
			   unicodFilial maxSize:12, nullable: true, blank: false
			   idHabilitada maxSize:12, nullable: true, blank: false
			   idHabilitada nullable: true
			   idPgHrextAdm nullable: true
			   municipio nullable: true
			   nmLog nullable: true
			   

	}

	
	static mapping = {
		id generator: "sequence", params:[sequence: "filial_seq"]
		empresa lazy: false
		municipio lazy: false
	}
	
	String toString(){
		"${this.dscFil}" +" - "+"${this.unicodFilial}"
	}
	
	String statusFechamento(Periodo periodo) {
		def status = "Aberto"
		if (periodo) {
			def result = PeriodoFilial.find("from PeriodoFilial pf where pf.periodo = :periodo and pf.filial = :filial ",
				[periodo: periodo, filial: this])
			if (result != null) { 
				status = result.status.equals("E") ? "Fechado" : "Aberto"
			}
		}
		status 
	}

}
