package cadastros

import org.hibernate.envers.Audited


@Audited
class JustificativasAbsenteismoSuspensao {

	Justificativas justificativas

	static mapping = {
		table "just_absen_suspensao"
		id generator: "sequence", params:[sequence: "just_absen_suspensao_seq"]
	}
	
}
