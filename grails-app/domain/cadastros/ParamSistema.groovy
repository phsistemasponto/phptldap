package cadastros



import funcoes.Horarios



class ParamSistema {
	
	Integer horaIntervaloParaOutra
	Integer horaParaIntervaloFolga
	Integer evRrt
	Integer nrDias
	String 	evPagamentoBh
	
	Integer dtInicioPeriodo
	Integer dtFimPeriodo 
	String 	mascPeriodo
	
	String 	consideraFaltaIntegral
	Integer evFaltaTotal
	Integer ev1Falta
	Integer ev2Falta
	String	checkFaltaTotal
	String 	check1Falta
	String 	check2Falta
	String 	falta2event1
	
	String 	descontarAtrasoPelaExtra
	Integer evAtraso
	Integer ev1Atraso
	Integer ev2Atraso
	String 	checkAtraso
	String 	check1Atraso
	String 	check2Atraso
	
	Integer evSaida
	Integer ev1Saida
	Integer ev2Saida
	String 	checkSaida
	String 	check1Saida
	String 	check2Saida
	
	Integer evDsr
	Integer evDsrd
	
	String	adicionalNoturnoNormal
	Integer	evAdicionalNoturno
	Integer horaInicioAdicionalNoturno
	Integer horaFimAdicionalNoturno
	
	String horaIntervaloParaOutraFormatada
	String horaParaIntervaloFolgaFormatada
	String horaInicioAdicionalNoturnoFormatada
	String horaFimAdicionalNoturnoFormatada
	
	String evRrtNome
	String evFaltaTotalNome
	String ev1FaltaNome
	String ev2FaltaNome
	String evAtrasoNome
	String ev1AtrasoNome
	String ev2AtrasoNome
	String evSaidaNome
	String ev1SaidaNome
	String ev2SaidaNome
	String evDsrNome
	String evDsrdNome
	String evAdicionalNoturnoNome
	
	static transients = ['horaIntervaloParaOutraFormatada','horaParaIntervaloFolgaFormatada',
		'horaInicioAdicionalNoturnoFormatada','horaFimAdicionalNoturnoFormatada',
		'evRrtNome','evFaltaTotalNome','ev1FaltaNome','ev2FaltaNome','evAtrasoNome','ev1AtrasoNome','ev2AtrasoNome',
		'evSaidaNome','ev1SaidaNome','ev2SaidaNome','evDsrNome','evDsrdNome','evAdicionalNoturnoNome']
	
    static constraints = {
		evRrt nullable: true, blank: true
		nrDias nullable: true, blank: true, defaultValue: 0
		evPagamentoBh nullable: true, blank: true, maxSize:6
		mascPeriodo maxSize:14
		consideraFaltaIntegral maxSize:1, nullable: false, blank: false
		checkFaltaTotal maxSize:1, nullable: false, blank: false
		check1Falta maxSize:1, nullable: false, blank: false
		check2Falta maxSize:1, nullable: false, blank: false
		falta2event1 maxSize:1, nullable: false, blank: false
		checkAtraso maxSize:1, nullable: false, blank: false
		check1Atraso maxSize:1, nullable: false, blank: false
		check2Atraso maxSize:1, nullable: false, blank: false
		checkSaida maxSize:1, nullable: false, blank: false
		check1Saida maxSize:1, nullable: false, blank: false
		check2Saida maxSize:1, nullable: false, blank: false
		adicionalNoturnoNormal maxSize:1, nullable: false, blank: false
		evFaltaTotal nullable: true
		ev1Falta nullable: true
		ev2Falta nullable: true
		evAtraso nullable: true
		ev1Atraso nullable: true
		ev2Atraso nullable: true
		evSaida nullable: true
		ev1Saida nullable: true
		ev2Saida nullable: true
		evDsr nullable: true
		evDsrd nullable: true
    }
	
	List getParamSistemaFiliais(){
		
		def filiais = Filial.listOrderByUnicodFilial()
		def item = []
		def listaRetorno = []
		def temFilial
		def statusPeriodo
		ParamSistemaFilial paramSistemaFilial
		filiais.each(){
			if (this.id){
				paramSistemaFilial = ParamSistemaFilial.findByParamSistemaAndFilial(this,it)
				temFilial = 'true'
		   } else {
		   	    paramSistemaFilial = null
				temFilial = 'false'
		   }
		   
		   temFilial=(paramSistemaFilial)?'true':'false'
		   //statusPeriodo=(temFilial=='true')?paramSistemaFilial.status:'A'
		   item = [filial: it, temFilial: temFilial, statusPeriodo: statusPeriodo]
		   listaRetorno.add(item)
		}
		return listaRetorno
	}

	String getHoraIntervaloParaOutraFormatada(){
		return horaIntervaloParaOutra ? Horarios.retornaHora(horaIntervaloParaOutra) : ''
	}
	
	String getHoraParaIntervaloFolgaFormatada(){
		return horaParaIntervaloFolga ? Horarios.retornaHora(horaParaIntervaloFolga) : ''
	}
	
	String getHoraInicioAdicionalNoturnoFormatada(){
		return horaInicioAdicionalNoturno ? Horarios.retornaHora(horaInicioAdicionalNoturno) : ''
	}
	
	String getHoraFimAdicionalNoturnoFormatada(){
		return horaFimAdicionalNoturno ? Horarios.retornaHora(horaFimAdicionalNoturno) : ''
	}
	
	String getEvRrtNome() {
		def resultado = Ocorrencias.get(evRrt)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEvFaltaTotalNome() {
		def resultado = Ocorrencias.get(evFaltaTotal)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEv1FaltaNome() {
		def resultado = Ocorrencias.get(ev1Falta)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEv2FaltaNome() {
		def resultado = Ocorrencias.get(ev2Falta)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEvAtrasoNome() {
		def resultado = Ocorrencias.get(evAtraso)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEv1AtrasoNome () {
		def resultado = Ocorrencias.get(ev1Atraso)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEv2AtrasoNome() {
		def resultado = Ocorrencias.get(ev2Atraso)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEvSaidaNome() {
		def resultado = Ocorrencias.get(evSaida)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEv1SaidaNome() {
		def resultado = Ocorrencias.get(ev1Saida)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEv2SaidaNome() {
		def resultado = Ocorrencias.get(ev2Saida)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEvDsrNome() {
		def resultado = Ocorrencias.get(evDsr)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEvDsrdNome() {
		def resultado = Ocorrencias.get(evDsrd)
		return (resultado) ? resultado.dscOcor : ''
	}
	
	String getEvAdicionalNoturnoNome() {
		def resultado = Ocorrencias.get(evAdicionalNoturno)
		return (resultado) ? resultado.dscOcor : ''
	}
	
}
