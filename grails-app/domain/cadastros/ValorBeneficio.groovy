package cadastros

import java.text.DecimalFormat
import java.text.SimpleDateFormat

import org.hibernate.envers.Audited;
@Audited
class ValorBeneficio {
	
	Beneficio beneficio
	Long sequencia
	Double valor
	Date dataInicio
	Date dataFim
	
	String valorFormatado
	String dataInicioFormatada
	String dataFimFormatada
	
	static constraints = {
		beneficio nullable:false
		valor maxSize:80, nullable:false, blank:false
		dataInicio nullable: false, blank: false
		dataFim nullable: true, blank: true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "ValorBeneficio_seq"]
		dataInicio type: "date"
		dataFim type: "date"
	}
	
	static transients = ['dataInicioFormatada','dataFimFormatada','valorFormatado']
	
	static belongsTo = [beneficio: Beneficio]
	
	String toString() {
		"${this.valor}"
	}
	
	String getDataInicioFormatada() {
		new SimpleDateFormat("dd/MM/yyyy").format(dataInicio)
	}
	
	String getDataFimFormatada() {
		dataFim ? new SimpleDateFormat("dd/MM/yyyy").format(dataFim) : ''
	}
	
	String getValorFormatado() {
		new DecimalFormat("0.00").format(valor)
	}

}
