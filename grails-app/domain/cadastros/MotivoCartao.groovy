package cadastros

import org.hibernate.envers.Audited

@Audited
class MotivoCartao {
	
	String dscMotivo

	static constraints = {
		dscMotivo maxSize:200, nullable: false, blank: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "motivo_cartao_seq"]
	}

	String toString(){
		"${this.dscMotivo}"
	}
}
