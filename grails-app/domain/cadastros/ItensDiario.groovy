package cadastros

import org.hibernate.envers.Audited

@Audited
class ItensDiario {

	Funcionario funcionario
	Date dtBat
	Classificacao classificacao
	Perfilhorario perfilHorario
	Integer qtdeChEfetiva
	
	static constraints = {
		funcionario unique: 'dtBat'
		perfilHorario nullable: true
		qtdeChEfetiva nullable:true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "itens_diario_seq"]
		funcionario column: "seq_func"
		classificacao column: "seq_class"
		perfilHorario column: "id_perfil_horario"
		dtBat type: "date"	
	}
	
}