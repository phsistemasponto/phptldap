package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class ParamRelatorioFechamento {
	
	String descricao
	Usuario usuario
	
	static hasMany = [ocorrencias: ParamRelatorioFechamentoOcorrencia]
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		table "p_rel_fech"
		id generator: "sequence", params:[sequence: "p_rel_fech_seq"]
	}

	String toString() {
		"${this.descricao}"
	}

}