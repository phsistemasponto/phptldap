package cadastros

import org.hibernate.envers.Audited


@Audited
class ParametroPeriodoFilial {
	
	ParametroPeriodo parametroPeriodo
	Filial filial
	
	static mapping = {
		id generator: "sequence", params:[sequence: "parametro_periodo_filial_seq"]
	}
	
}
