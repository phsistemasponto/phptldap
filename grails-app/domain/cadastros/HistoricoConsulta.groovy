package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class HistoricoConsulta {
	
	Usuario usuario
	String consulta
	
	static constraints = {
		consulta nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "Historico_Consulta_seq"]
		consulta type: 'text'
	}

	String toString() {
		"${this.consulta}"
	}

}