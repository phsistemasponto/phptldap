package cadastros

import org.hibernate.envers.Audited;
import cadastros.Funcionario;
import cadastros.PeriodoFilial;
import acesso.Usuario;

@Audited

class PgSaldoBh {
	
	
	Funcionario funcionario
	Periodo periodo
	Usuario usuario
	Long qtHr
	Double vrHr
	
	
	static mapping = {
		id generator: "sequence", params:[sequence: "pg_sld_bh_seq"]
		funcionario column: "seq_func"
		periodo column: "seq_pr"
		qtHr defaultValue :0
		vrHr defaultValue :0
		}


}
