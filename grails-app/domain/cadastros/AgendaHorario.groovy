package cadastros

import org.hibernate.envers.Audited

@Audited
class AgendaHorario {
	
	Agenda agenda
	GrupoHorario horario 
	Integer diaSemana

	static constraints = {
		agenda nullable:false
		horario nullable:false
		diaSemana nullable:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "agenda_horario_seq"]
	}
	
	static void removeAll(Agenda agenda) {
		executeUpdate 'DELETE FROM AgendaHorario WHERE agenda=:agenda', [agenda: agenda]
	}
	
}
