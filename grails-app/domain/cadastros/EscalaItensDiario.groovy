package cadastros

import org.hibernate.envers.Audited

@Audited
class EscalaItensDiario {

	Funcionario funcionario
	EscalaHorarios escalaHorarios
	Date dtBat
	String status 
	
	static constraints = {
		funcionario(unique: ['dtBat', 'escalaHorarios'])
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "itens_diario_seq"]
		funcionario column: "seq_func"
		escalaHorarios column: "id_t_esc_horarios"
		dtBat type: "date"	
		status defaultValue: "'A'"
	}
	
}