package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario
import funcoes.Horarios

@Audited
class HorarioSimulado {
	
	Usuario usuarioSimulado
	Usuario usuarioCriado
	Date dtSimulado
	Date dtCriado
	String observacao
	String escala
	String flag
	Long cargaHoraria
	String historicoHorario
	
	String cargaTotalStr
	
	static hasMany = [itensHorario: ItemHorarioSimulado]
	static transients = ['cargaTotalStr']
	 
	static constraints = {
		usuarioSimulado nullable: false
		dtSimulado nullable: false
		flag nullable: false
		cargaHoraria nullable: false
		usuarioCriado nullable: true
		dtCriado nullable: true
	}
	
	static mapping = {		
		table "hor_simul"
		id generator: "sequence", params:[sequence: "hor_simul_seq"]
		dtSimulado type: "date"
	}
	
	String getCargaTotalStr() {
		return Horarios.retornaHora(cargaHoraria)
	}
	
}
