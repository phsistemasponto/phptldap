package cadastros

import org.hibernate.envers.Audited

@Audited
class BeneficioParamFilial implements Serializable {

	BeneficioParam beneficioParam
	Filial filial

	static constraints = { beneficioParam unique: 'filial' }

	static mapping = {
		id generator: "sequence", params:[sequence: "BeneficioParamFilial_seq"]
	}
}
