package cadastros


class Batidas1510 {
	
	Funcionario funcionario
	MotivoCartao motivoCartao
	Date dtBat
	Integer hrBat
	String idBat
	String status
	
	static constraints = {
		status nullable:true,blank:true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "batidas_1510_seq"]
		funcionario column: "seq_func"
		dtBat type: "date"
	}
	
	

}
