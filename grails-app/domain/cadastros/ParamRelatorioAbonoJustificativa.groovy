package cadastros

import org.hibernate.envers.Audited

@Audited
class ParamRelatorioAbonoJustificativa {
	
	ParamRelatorioAbono param
	Justificativas justificativa
	
	static constraints = {
		
	}

	static mapping = {
		table "p_rel_ab_just"
		id generator: "sequence", params:[sequence: "p_rel_ab_just_seq"]
	}

	String toString() {
		"${this.justificativa}"
	}

}