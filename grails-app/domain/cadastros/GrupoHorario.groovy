package cadastros

import org.hibernate.envers.Audited

@Audited
class GrupoHorario {
	
	String descricao

	static constraints = {
		descricao maxSize:512, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "grupo_horario_seq"]
	}
	
	static hasMany = [itens: GrupoItensHorario]

	String toString() {
		"${this.descricao}"
	}
	
}
