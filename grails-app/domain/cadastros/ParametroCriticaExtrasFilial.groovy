package cadastros

import org.hibernate.envers.Audited


@Audited
class ParametroCriticaExtrasFilial {
	
	ParametroCriticaExtras parametroCriticaExtras
	Filial filial
	
	static mapping = {
		table "param_crit_ext_filial"
		id generator: "sequence", params:[sequence: "param_crit_ext_filial_seq"]
	}
	
}
