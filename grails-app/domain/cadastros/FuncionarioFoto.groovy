package cadastros

import java.sql.Blob

import org.hibernate.envers.Audited

@Audited
class FuncionarioFoto implements Serializable{

	Funcionario funcionario
	Blob conteudo

	static mapping = {
		id generator: "sequence", params:[sequence: "funcionario_foto_seq"]
		conteudo type: 'blob'
	}


}
