package cadastros

import java.util.Date;

import org.hibernate.envers.Audited;


@Audited
class FeriadosNacionais {

	
	
	
			Long dia
			Long mes
			String dscFer
		
				static constraints = {
				
					dscFer maxSize:80, nullable:false, blank:false
					dia(unique: ['mes'])
					
					
					
			}
		
			static mapping = {
				id generator: "sequence", params:[sequence: "feriados_nacionais_seq"]
		
			}
		
}
		
	

