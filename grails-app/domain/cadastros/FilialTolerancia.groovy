package cadastros


import java.io.Serializable;
import org.apache.commons.lang.builder.HashCodeBuilder
import org.hibernate.envers.Audited;
import cadastros.Filial;
import cadastros.Tipotolerancia;
import java.util.Date;

	@Audited

		class FilialTolerancia implements Serializable{
	

		Filial filial
		Tipotolerancia tipotolerancia
		Date dtIniVig
		Date dtFimVig
		
		String dataInicioFormatada
		String dataFimFormatada
	
		static constraints = {
			dtFimVig nullable: true
		}
	
		static mapping = {
			id generator: "sequence", params:[sequence: "filial_tolerancia_seq"]
			version false
		}
		
		static transients = ['dataInicioFormatada','dataFimFormatada']
	
		int hashCode() {
			def builder = new HashCodeBuilder()
			builder.append filial
			builder.apeend tipotolerancia
			builder.toHashCode()
		}
		
		
		
	
	    
}
