package cadastros

import org.hibernate.envers.Audited

import funcoes.Horarios

@Audited
class ItensConfiguracaoHoraExtra {
	
	ConfiguracaoHoraExtra configuracao
	Classificacao classificacao
	Long sequencial
	
	Integer tolerancia
	Integer limitePorDia
	Integer diaEspecificoSemana
	String 	tipoConfiguracao
	Integer qtdadeHorasDescontar
	Integer limiteHorasDesconto
	Integer limiteQuantidadeEfetuadaDesconto
	
	String toleranciaFormatada
	String limitePorDiaFormatada
	String qtdadeHorasDescontarFormatada
	String limiteHorasDescontoFormatada
	String limiteQuantidadeEfetuadaDescontoFormatada
	
	static hasMany = [percentuais: PercentualItensConfiguracaoHoraExtra]
	
	static belongsTo = [configuracao: ConfiguracaoHoraExtra]

	static constraints =  {
		tolerancia nullable: true, blank: true
		limitePorDia nullable: false, blank: false
		diaEspecificoSemana nullable: true, blank: true
		tipoConfiguracao maxSize:1, nullable: false, blank: false
		qtdadeHorasDescontar nullable: true, blank: true
		limiteHorasDesconto nullable: true, blank: true
		limiteQuantidadeEfetuadaDesconto nullable: true, blank: true
	}
	
	static mapping = {
		table "itens_config_he"
		diaEspecificoSemana column: "dia_espec_semana"
		qtdadeHorasDescontar column: "qt_hrs_descontar"
		limiteHorasDesconto column: "lim_hrs_desconto"
		limiteQuantidadeEfetuadaDesconto column: "lim_qt_efet_des"
		id generator: "sequence", params:[sequence: "itens_config_he_seq"]
	}
	
	static transients = ['toleranciaFormatada','limitePorDiaFormatada', 'qtdadeHorasDescontarFormatada',
		'limiteHorasDescontoFormatada', 'limiteQuantidadeEfetuadaDescontoFormatada']

	String toString() {
		"${this.id}"
	}
	
	String getToleranciaFormatada() {
		return tolerancia != null ? Horarios.retornaHora(tolerancia) : ''
	}
	
	String getLimitePorDiaFormatada() {
		return limitePorDia != null ? Horarios.retornaHora(limitePorDia) : ''
	}
	
	String getQtdadeHorasDescontarFormatada() {
		return qtdadeHorasDescontar != null ? Horarios.retornaHora(qtdadeHorasDescontar) : ''
	}
	
	String getLimiteHorasDescontoFormatada() {
		return limiteHorasDesconto != null ? Horarios.retornaHora(limiteHorasDesconto) : ''
	}
	
	String getLimiteQuantidadeEfetuadaDescontoFormatada() {
		return limiteQuantidadeEfetuadaDesconto != null ? Horarios.retornaHora(limiteQuantidadeEfetuadaDesconto) : ''
	}
	
}