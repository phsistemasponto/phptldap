package cadastros


class GeracaoFolha {
	
	Folha folha
	Date dtGeracao
	String arquivo

	static mapping = {
		id generator: "sequence", params:[sequence: "geracao_folha_seq"]
		arquivo type: 'text'
	}

	String toString() {
		"${this.arquivo}"
	}

}