package cadastros

import java.text.*

import org.hibernate.envers.Audited


@Audited
class FuncionarioSemBeneficio implements Serializable{

	Funcionario funcionario
	TipoDeBeneficio tipoBeneficio
	Date dtProcessamento
	Date dtInicioDesconto
	Date dtFimDesconto

	static mapping = {
		id generator: "sequence", params:[sequence: "funcionario_sem_beneficio_seq"]
		dtInicioDesconto type: "date"
		dtFimDesconto type: "date"
	}

}
