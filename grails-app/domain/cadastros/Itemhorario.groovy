package cadastros

import java.math.*
import java.text.*

import org.hibernate.envers.Audited

import funcoes.Horarios

@Audited
class Itemhorario {
	
	Horario horario
	Classificacao classificacao
	Perfilhorario perfilhorario
	Long seqitemhorario
	Long nrdiasemana
	Date dtinivig
	Long qtdechefetiva
	String idvira

	String diaSemana
	String cargaHorariaHoras

	static belongsTo = [horario: Horario]
	static transients = ['diaSemana','cargaHorariaHoras'] 
	
	static constraints = {
		idvira maxSize:2 , nullable: false, blank: false
		dtinivig nullable: false
		perfilhorario nullable: true
		qtdechefetiva nullable: true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "itemhorario_seq"]
		horario lazy: false
		classificacao lazy: false
		perfilhorario lazy: false
	}


	String getDiaSemana() {
		
		def dia = ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.dtinivig);  
		dia[cal.get(Calendar.DAY_OF_WEEK)-1]
		
	}
	
	String getDtinivigFormatada(){
		getDataFormatada(this.dtinivig)
	}
	
	private String getDataFormatada(Date data){
		try {
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(data)
		} catch(Exception e) {
			return ""
		}
	}
	
	String getCargaHorariaHoras(){
		try {
			Horarios.retornaHora((this.qtdechefetiva)?this.qtdechefetiva:0)
		} catch (Exception e){
			'00:00'
		}
	}
}
