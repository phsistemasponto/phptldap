package cadastros

import org.apache.commons.lang.builder.HashCodeBuilder


class FolhaFilial implements Serializable {

	Folha folha
	Filial filial

	boolean equals(other) {
		if (!(other instanceof FolhaFilial)) {
			return false
		}

		other.Folha?.id == folha?.id &&
				other.Filial?.id == filial?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (folha) builder.append(folha.id)
		if (filial) builder.append(filial.id)
		builder.toHashCode()
	}

	static FolhaFilial get(long folhaId, long filialId) {
		find 'from FolhaFilial where folha.id=:folhaId and filial.id=:filialId',
				[usuarioId: folhaId, filialId: filialId]
	}

	static FolhaFilial create(Folha folha, Filial filial, boolean flush = false) {
		new FolhaFilial(Folha: folha, Filial: filial).save(flush: flush, insert: true)
	}
	
	static void removeAll(Folha folha) {
		executeUpdate 'DELETE FROM FolhaFilial WHERE folha=:folha', [folha: folha]
	}

	static void removeAll(Filial filial) {
		executeUpdate 'DELETE FROM FolhaFilial WHERE filial=:filial', [filial: filial]
	}
	
	def atualizarFolhaFilial(params, folha){
		def isArrayFilial = [Collection, Object[]].any { it.isAssignableFrom(params.filialId.getClass()) }
		def filialArray = isArrayFilial ? params.filialId : [params.filialId]
		
		filialArray.eachWithIndex { it, i ->
			
			def folhaInstance = Folha.get(folha)
			Filial filial = Filial.get(it) 
			FolhaFilial folhaFilial = FolhaFilial.find("from FolhaFilial where folha = :folha and filial = :filial", [folha: folhaInstance, filial: filial])
			
			if (params.temFilialPeriodo[i]=="true" || params.temFilialPeriodo[i]=="t") {
				
				if (!folhaFilial){
					folhaFilial = new FolhaFilial()
					folhaFilial.filial = filial
					folhaFilial.folha = folhaInstance
				}
								
				if (!folhaFilial.save()){
					folhaFilial.errors.each { item ->
						println(item)
					}
				}

			} else {
				if (folhaFilial){
					folhaFilial.delete()
				}
			}
		}
		
	}

	static mapping = {
		id composite: ['folha', 'filial']
		version false
	}
}
