package cadastros

import org.hibernate.envers.Audited;

@Audited

class OcorrenciasBh {
        
	    Funcionario funcionario
		Ocorrencias ocorrencias  	
		Date dtBat
		String idTpLanc
		String idTpOcorr
		Long nrQtdeHr
		String observacao
			
		String idTpLancString
		String idTpOcorrString
		
				
			static transients = ['idTpLancString','idTpOcorrString']
		
	
		static constraints = {
				   idTpLanc  maxSize:1, nullable: true, blank: false
				   idTpOcorr maxSize:1, nullable: true, blank: false
				   observacao maxSize:200, nullable: true, blank: true
				   	}
	
		
		static mapping = {
			id generator: "sequence", params:[sequence: "ocorrenciasBh_seq"]
			funcionario column: "seq_func"
			ocorrencias column: "seq_ocor"
			dtBat type: "date"
			nrQtdeHr defaultValue: 0
			
			}
		
			

}
