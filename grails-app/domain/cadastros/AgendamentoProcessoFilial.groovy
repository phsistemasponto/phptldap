package cadastros

import org.hibernate.envers.Audited

@Audited
class AgendamentoProcessoFilial {

	AgendamentoProcesso agendamentoProcesso
	Filial filial
	
	static mapping = {
		table "ag_proc_fil"
		id generator: "sequence", params:[sequence: "ag_proc_fil_seq"]
	}
	
}
