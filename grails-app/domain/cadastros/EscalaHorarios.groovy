package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class EscalaHorarios {

	Usuario usuario
	Perfilhorario perfilhorario
	String idLetra
	
	static constraints = {
		idLetra maxSize:3, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "escala_horarios_seq"]
		usuario column: "id_usuario"
		perfilhorario column: "id_perfil_horario"
	}
	
	String toString() {
		"${this.idLetra} - ${this.perfilhorario.dscperfil}"
	}
	
}
