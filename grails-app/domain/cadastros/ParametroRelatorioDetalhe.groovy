package cadastros

import org.hibernate.envers.Audited

@Audited
class ParametroRelatorioDetalhe {
	
	ParametroRelatorio parametroRelatorio
	ParametroRelatorioTotais parametroRelatorioTotais
	Ocorrencias ocorrencias
	String descricao
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
		descricao nullable:false, blank:false
	}

	static mapping = {
		table "param_rel_detalhe"
		id generator: "sequence", params:[sequence: "param_rel_detalhe_seq"]
	}

	String toString() {
		"${this.descricao}"
	}

}