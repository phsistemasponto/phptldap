package cadastros

import java.text.*

import org.apache.commons.lang.builder.HashCodeBuilder
import org.hibernate.envers.Audited;


@Audited
class FuncionarioBeneficio implements Serializable{

	Funcionario funcionario
	Beneficio beneficio
	Long sequencia
	Date dtIniVig
	Date dtFimVig
	Long quantidade

	String dataInicioFormatada
	String dataFimFormatada	
	
	static transients = ['dataInicioFormatada','dataFimFormatada']

	static constraints = {
	    dtFimVig nullable: true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "funcionario_beneficio_seq"]
	    version false
	}

	int hashCode() {
	    def builder = new HashCodeBuilder()
	    builder.append funcionario
	    builder.append beneficio
	    builder.append dtIniVig
	    builder.toHashCode()
	}
	
	String getDataInicioFormatada() {
		if (this.dtIniVig){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			this.dataInicioFormatada = formatterDate.format(this.dtIniVig)
			return this.dataInicioFormatada
		} else {
			return ""
		}
	}
	
	String getDataFimFormatada() {
		if (this.dtFimVig){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			this.dataFimFormatada = formatterDate.format(this.dtFimVig)
			return this.dataFimFormatada
		} else {
			return ""
		}
	}
	
	String toString() {
		"Beneficio : ${this.funcionario}"
	}

}
