package cadastros

import org.hibernate.envers.Audited;

@Audited

class EventosFolha {

	Folha folha
	Ocorrencias ocorrencias
	Integer sequencial
	String dscEvento
	String expHoras
	String expDias
	String sumEvento
	String expHorasHex
	String tipoEvento

	String expHorasFormatada
	String expDiasFormatada
	String sumEventoFormatada
	String expHorasHexFormatada


	static transients = [
		'expHorasFormatada',
		'expDiasFormatada',
		'sumEventoFormatada',
		'expHorasHexFormatada'
	]

	static belongsTo = [folha: Folha]

	static constraints = {
		dscEvento maxSize:10 , nullable: false, blank: false
		expHoras maxSize:2 , nullable: true, blank: false
		expDias maxSize:2 , nullable: true, blank: false
		sumEvento maxSize:2 , nullable: true, blank: false
		expHorasHex maxSize:2 , nullable: true, blank: false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "eventosFolha_seq"]
		folha lazy: false
		ocorrencias lazy: false
	}


	String toString() {
		"${this.dscEvento}"
	}

	String getExpHorasFormatada(){
		return (expHoras=='S')?'Sim':'Não'
	}

	String getExpDiasFormatada(){
		return (expDias=='S')?'Sim':'Não'
	}

	String getSumEventoFormatada(){
		return (sumEvento=='S')?'Sim':'Não'
	}

	String getExpHorasHexFormatada(){
		return (expHorasHex=='S')?'Sim':'Não'
	}
}
