package cadastros

import org.hibernate.envers.Audited

import acesso.Usuario

@Audited
class ParamRelatorioAbono {
	
	String descricao
	Usuario usuario
	
	static hasMany = [justificativas: ParamRelatorioAbonoJustificativa]
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		table "p_rel_ab"
		id generator: "sequence", params:[sequence: "p_rel_ab_seq"]
	}

	String toString() {
		"${this.descricao}"
	}

}