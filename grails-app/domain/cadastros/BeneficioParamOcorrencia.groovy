package cadastros

import org.hibernate.envers.Audited

@Audited
class BeneficioParamOcorrencia {

	BeneficioParam beneficioParam
	Ocorrencias ocorrencias
	Integer quantidade
	
	static constraints = {
		quantidade nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "BeneficioParamOcorrencia_seq"]
	}
	
}