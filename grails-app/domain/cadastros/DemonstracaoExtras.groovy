package cadastros

import java.text.DecimalFormat
import java.text.NumberFormat

import org.hibernate.envers.Audited

@Audited
class DemonstracaoExtras {
	
	Funcionario funcionario
	Periodo periodo
	
	Integer bancoAnt
	Integer comp
	Integer extra1
	Integer extra2
	Integer preCritica
	Integer idPago
	Integer status
	String idFechado
	
	Float sldExtra1
	Float sldExtra2
	Float vrReceber
	Float vrTotal
	
	Date dtCreation
	Integer codUsu
	
	static constraints = {
		bancoAnt nullable: true, blank: true
		comp nullable: true, blank: true
		extra1 nullable: true, blank: true
		extra2 nullable: true, blank: true
		preCritica nullable: true, blank: true
		idPago nullable: true, blank: true
		status nullable: true, blank: true
		idFechado maxSize:1, nullable: true, blank: true
		sldExtra1 nullable: true, blank: true
		sldExtra2 nullable: true, blank: true
		vrReceber nullable: true, blank: true
		vrTotal nullable: true, blank: true
	}
	
   static mapping = {
	   id generator: "sequence", params:[sequence: "demonstracao_extras_seq"]
	   status defaultValue: 0
	   funcionario column: "seq_func"
	   periodo column: "seq_pr"
   }

   String toString() {
	   "${this.funcionario} + ' ' + ${this.periodo}"
   }
   
   String getVrRealExibicao(){
	   try {
		   def vrReal = (sldExtra1 + sldExtra2)
		   NumberFormat formatter = new DecimalFormat("0.00");
		   formatter.format(vrReal)
	   } catch (Exception e){
		   "0,00"
	   }
   }
   
   String getVrReceberExibicao(){
	   try {
		   NumberFormat formatter = new DecimalFormat("0.00");
		   formatter.format(vrReceber)
	   } catch (Exception e){
		   "0,00"
	   }
   }

}
