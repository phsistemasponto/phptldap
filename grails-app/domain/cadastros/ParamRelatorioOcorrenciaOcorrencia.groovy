package cadastros

import org.hibernate.envers.Audited

@Audited
class ParamRelatorioOcorrenciaOcorrencia {
	
	ParamRelatorioOcorrencia param
	Ocorrencias ocorrencia
	
	static constraints = {
		
	}

	static mapping = {
		table "p_rel_ocor_oc"
		id generator: "sequence", params:[sequence: "p_rel_ocor_oc_seq"]
	}

	String toString() {
		"${this.ocorrencia}"
	}

}