package cadastros

import java.math.*
import java.text.*

import org.hibernate.envers.Audited

import funcoes.Horarios




@Audited
class Funcionario implements Serializable {

	Filial filial
	Cargo cargo 
	Setor setor 
	Uniorg uniorg
	Tipofunc tipoFunc
	Situacaofunc situacaoFunc
	Horario horario
	ConfiguracaoHoraExtra configuracaoHoraExtra
	
	String mtrFunc
	Long crcFunc
	String nomFunc
	String emlFunc
	Date dtNscFunc
	String pisFunc
	
	Long seqUniorg
	String dscUniorg
	
	String idAdcNot
	String idObrigIntervalo
	String idSensivelFeriado
	String idIntervFlex
	String idRrt
	
	Date   dtAdm
	Date   dtResc
	Double vrSal
	Long   dsrRemunerado
	String dsrRemuneradoExibicao
	
	String docRgNr
	String docRgEmi
	String docRgEmiUf
	Date   docRgDtEmi
	
	String docCrtPrf
	String docCrtPrfSer
	String docCrtPrfUf
	Date   docCrtPrfEmi
	
	String docCpf
	String docRegPrf
	
	String dscLog
	String nrLog
	String cmpLog
	String baiLog
	String cepLog
	String fonResFun
	String fonCelFun
	String fonComFun
	
  	String idCheckEcal

	String dtAdmExibicao
	String dtRescExibicao
	String dtNascExibicao
	String docRgDtEmiExibicao
	String docCrtPrfEmiExibicao
	String vrSalExibicao
	
	static hasMany = [
		horasExtras: FuncionarioHoraExtra,
		beneficios: FuncionarioBeneficio,
		horarios: FuncionarioHorario,
		foto: FuncionarioFoto,
		filiais: FuncionarioFilial,
		setores: FuncionarioSetor,
		cargos: FuncionarioCargo
	]
	
    static constraints = {
		mtrFunc maxSize:12, nullable:false
        crcFunc nullable:false
        nomFunc maxSize:80, nullable: false, blank: false
		idObrigIntervalo maxSize:1,nullable:false,blank:false
		docCrtPrf maxSize:16, nullable: true, blank: true
		docCrtPrfSer maxSize:16, nullable: true, blank: true
		emlFunc maxSize:100, nullable: true, blank: true
		idSensivelFeriado maxSize:1
		idAdcNot maxSize:1
		idIntervFlex maxSize:1
		pisFunc maxSize:15, nullable: true, blank: true 			
		vrSal nullable: true, blank: true	
		dsrRemunerado nullable: true, blank: true	
	    seqUniorg nullable: true, blank: true
		uniorg nullable: true, blank: true
	  	idRrt nullable: true, blank: true
	  	idCheckEcal nullable: true, blank: true
		dtResc nullable: true
		docRgNr maxSize:15, nullable: true, blank: true
		docRgEmi maxSize:15, nullable: true, blank: true
		docRgEmiUf maxSize:2, nullable: true, blank: true
		docRgDtEmi nullable: true, blank: true
		docCrtPrf maxSize:15, nullable: true, blank: true
		docCrtPrfSer maxSize:15, nullable: true, blank: true
		docCrtPrfUf maxSize:15, nullable: true, blank: true
		docCrtPrfEmi nullable: true, blank: true
		docCpf maxSize:15, nullable: true, blank: true
		docRegPrf maxSize:15, nullable: true, blank: true
		dscLog maxSize:520, nullable: true, blank: true
		nrLog maxSize:15, nullable: true, blank: true
		cmpLog maxSize:15, nullable: true, blank: true
		baiLog maxSize:15, nullable: true, blank: true
		cepLog maxSize:15, nullable: true, blank: true
		fonResFun maxSize:15, nullable: true, blank: true
		fonCelFun maxSize:15, nullable: true, blank: true
		fonComFun maxSize:15, nullable: true, blank: true
		horario nullable: true, blank: true
		configuracaoHoraExtra nullable: true, blank: true
    }

	static transients=['dscUniorg',
		'dtAdmExibicao','dtRescExibicao','dtNascExibicao',
		'vrSalExibicao','docRgDtEmiExibicao','docCrtPrfEmiExibicao', 'dsrRemuneradoExibicao']

    static mapping = {
        version false
        id generator: "sequence", params:[sequence: "funcionario_seq"]
		filial lazy:false
		foto lazy: true
    }

    String toString(){
		"${this.nomFunc}"
	}

	String getDtAdmExibicao(){
		if (this.dtAdm){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(this.dtAdm)
		} else {
			""
		}
	}
	
	String getDtRescExibicao(){
		if (this.dtResc){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(this.dtResc)
		} else {
			""
		}
	}

	String getDtNascExibicao(){	
		if (this.dtNscFunc){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(this.dtNscFunc)
		} else {
			""
		}
	}
	
	String getDocRgDtEmiExibicao() {
		if (this.docRgDtEmi){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(this.docRgDtEmi)
		} else {
			""
		}
	}
	
	String getDocCrtPrfEmiExibicao() {
		if (this.docCrtPrfEmi){
			SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy")
			formatterDate.format(this.docCrtPrfEmi)
		} else {
			""
		}
	}
	
	String getVrSalExibicao(){
		try {
			NumberFormat formatter = new DecimalFormat("0.00"); 
			formatter.format(this.vrSal)
		} catch (Exception e){
			"0,00"
		}
	}	
	
	String getDsrRemuneradoExibicao(){
		return dsrRemunerado ? Horarios.retornaHora(dsrRemunerado) : ''
	}
	
	FuncionarioHoraExtra getFuncionarioHoraExtraAtivo() {
		FuncionarioHoraExtra retorno = null
		horasExtras.each {
			if (it.dtFimVig == null) {
				retorno = it
			}
		}
		return retorno
	}
	
	FuncionarioBeneficio getFuncionarioBeneficioAtivo() {
		FuncionarioBeneficio retorno = null
		beneficios.each {
			if (it.dtFimVig == null) {
				retorno = it
			}
		}
		return retorno
	}
	
	FuncionarioHorario getFuncionarioHorarioAtivo() {
		FuncionarioHorario retorno = null
		horarios.each {
			if (it.dtFimVig == null) {
				retorno = it
			}
		}
		return retorno
	}
	
	FuncionarioFilial getFuncionarioFilialAtivo() {
		FuncionarioFilial retorno = null
		filiais.each {
			if (it.dtFimVig == null) {
				retorno = it
			}
		}
		return retorno
	}
	
	FuncionarioSetor getFuncionarioSetorAtivo() {
		FuncionarioSetor retorno = null
		setores.each {
			if (it.dtFimVig == null) {
				retorno = it
			}
		}
		return retorno
	}
	
	FuncionarioCargo getFuncionarioCargoAtivo() {
		FuncionarioCargo retorno = null
		cargos.each {
			if (it.dtFimVig == null) {
				retorno = it
			}
		}
		return retorno
	}
	
	String statusFechamento(Periodo periodo) {
		def status = "Aberto"
		if (periodo) {
			def result = StatusFechamento.findAll("from StatusFechamento s where s.periodo = :periodo and s.funcionario = :funcionario ",
				[periodo: periodo, funcionario: this])
			if (result != null && !result.isEmpty()) {
				status = "Fechado"
			}
		}
		status
	}
	
}