package cadastros

import org.hibernate.envers.Audited

@Audited
class TipotoleranciaFilial implements Serializable {

	Tipotolerancia tipotolerancia
	Filial filial

	static constraints = { tipotolerancia unique: 'filial' }

	static mapping = {
		id generator: "sequence", params:[sequence: "TipotoleranciaFilial_seq"]
	}
	
}
