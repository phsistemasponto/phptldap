package cadastros

import org.hibernate.envers.Audited

@Audited
class ParametroRelatorio {
	
	String descricao
	
	static hasMany = [detalhes: ParametroRelatorioDetalhe, filiais: ParametroRelatorioFilial]
	
	static constraints = {
		descricao maxSize:80, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "parametro_relatorio_seq"]
	}

	String toString() {
		"${this.descricao}"
	}

}