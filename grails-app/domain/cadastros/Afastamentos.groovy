package cadastros

import org.hibernate.envers.Audited

@Audited
class Afastamentos {
	
	Funcionario funcionario
	Justificativas justificativas
	Date dataInicio
	Date dataFim

	static constraints = {
		dataFim nullable:true
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "afastamentos_seq"]
		dataInicio type: "date"
		dataFim type: "date"
	}
	
	String toString() {
		"${this.funcionario} - ${this.justificativas} - ${this.dataInicio} - ${this.dataFim}"
	}

}
