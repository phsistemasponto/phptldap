package cadastros

import org.hibernate.envers.Audited

@Audited
class ParamRelatorioFechamentoOcorrencia {
	
	ParamRelatorioFechamento param
	Ocorrencias ocorrencia
	
	static constraints = {
		
	}

	static mapping = {
		table "p_rel_fec_ocor"
		id generator: "sequence", params:[sequence: "p_rel_fec_ocor_seq"]
	}

	String toString() {
		"${this.ocorrencia}"
	}

}