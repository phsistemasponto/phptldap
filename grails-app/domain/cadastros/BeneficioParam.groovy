package cadastros

import org.hibernate.envers.Audited

import funcoes.Horarios

@Audited
class BeneficioParam {

	TipoDeBeneficio tipoDeBeneficio
	String verbaFolha
	String idFuncioDemitido
	String idBenIncremento
	String idBenDescontoDiario
	String idVerificaMesAnterior
	String idVerificaQtFaltas
	Integer qtFaltas
	String idVerificaFaltasAbo
	String idVerificaExtras
	Integer qtExtras
	String idVerificaMesProcesso
	String idVerificaFeriado
	String idVerificaFolgaComp
	String idVerificaAfastamentos
	
	String qtExtrasExibicao
	
	static transients = ['qtExtrasExibicao']
	
	static hasMany = [filiais: BeneficioParamFilial, 
					  justificativasAbono: BeneficioParamJustificativa,
					  ocorrencias: BeneficioParamOcorrencia]

	static constraints = {
		verbaFolha maxSize:15, nullable:false, blank:false
		idFuncioDemitido maxSize:1, nullable:true, blank:false
		idBenIncremento maxSize:1, nullable:true, blank:false
		idBenDescontoDiario maxSize:1, nullable:true, blank:false
		idVerificaMesAnterior maxSize:1, nullable:true, blank:false
		idVerificaQtFaltas maxSize:1, nullable:true, blank:false
		idVerificaFaltasAbo maxSize:1, nullable:true, blank:false
		idVerificaExtras maxSize:1, nullable:true, blank:false
		idVerificaMesProcesso maxSize:1, nullable:true, blank:false
		idVerificaFeriado maxSize:1, nullable:true, blank:false
		idVerificaFolgaComp maxSize:1, nullable:true, blank:false
		idVerificaAfastamentos maxSize:1, nullable:true, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "BeneficioParam_seq"]
	}
	
	String getQtExtrasExibicao(){
		return qtExtras != null ? Horarios.retornaHora(qtExtras) : ""
	}
}



