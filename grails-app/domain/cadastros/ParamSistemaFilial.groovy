package cadastros



class ParamSistemaFilial {
	
	ParamSistema paramSistema
	Filial filial

    static constraints = {
		paramSistema(unique: ['filial'])
    }
	
	def atualizarParamSistemaFilial(params, paramSistema){
		def paramSistemaFilial
		
			
		println('params: '+params)
		Filial filial
		params.filialId.eachWithIndex { it, i ->
			
			filial = Filial.get(it)
			def paramSistemaInstance = ParamSistema.get(paramSistema)
			paramSistemaFilial = ParamSistemaFilial.findByFilialAndParamSistema(filial, paramSistemaInstance)
			
			if (params.temFilialPeriodo[i]=='true') {
				
				if (!paramSistemaFilial){
					paramSistemaFilial = new ParamSistemaFilial()
					paramSistemaFilial.filial = filial
					paramSistemaFilial.paramSistema = paramSistemaInstance
				}
								
				if (!paramSistemaFilial.save()){
					paramSistemaFilial.errors.each { item ->
						println(item)
					}
				}

			} else {
				if (paramSistemaFilial){
					paramSistemaFilial.delete()
				}
			}
		}
		
	}
}
