package cadastros

import org.hibernate.envers.Audited

@Audited
class GrupoFuncionario {
	
	Grupo grupo
	Funcionario funcionario
	
	static mapping = {
		id generator: "sequence", params:[sequence: "grupo_funcionario_seq"]
	}
	
	String toString() {
		"${this.grupo.dscGrupo}"
	}

}
