package cadastros

import org.hibernate.envers.Audited;

@Audited
class Folha {

	String dscFol
	String nomArq
	String email

	static constraints = {
		dscFol maxSize:60, nullable: false, blank: false
		nomArq maxSize:60, nullable: true, blank: false
		email  email: true
	}

	static hasMany = [eventos: EventosFolha]

	static mapping = {
		id generator: "sequence", params:[sequence: "folha_seq"]
	}
	
	List getFolhaFiliais(){
		
		def filiais = Filial.listOrderByUnicodFilial()
		def item = []
		def listaRetorno = []
		def temFilial
		def statusPeriodo
		FolhaFilial folhaFilial
		filiais.each(){
			if (this.id) {
				folhaFilial = FolhaFilial.find("from FolhaFilial where folha = :folha and filial = :filial", [folha: this, filial: it])
				temFilial = 'true'
		   } else {
		   		folhaFilial = null
				temFilial = 'false'
		   }
		   
		   temFilial=(folhaFilial)?'true':'false'
		   item = [filial: it, temFilial: temFilial, statusPeriodo: statusPeriodo]
		   listaRetorno.add(item)
		}
		return listaRetorno
	}

	String toString(){
		"${this.dscFol}"
	}
}
