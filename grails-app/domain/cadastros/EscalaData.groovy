package cadastros

import org.hibernate.envers.Audited

@Audited
class EscalaData {

	Date dtInicio
	Date dtFim
	Date dtFechamento
	String legendaMes
	String status
	
	static constraints = {
		dtInicio nullable:false, blank:false
		dtFim nullable:false, blank:false
		dtFechamento nullable:true, blank:true
		legendaMes maxSize:300, nullable:false, blank:false
		status maxSize:1, nullable:false, blank:false
	}

	static mapping = {
		id generator: "sequence", params:[sequence: "escala_data_seq"]
		status defaultValue: "'A'"
		dtInicio type: "date"
		dtFim type: "date"
		dtFechamento type: "date"
	}
	
}
