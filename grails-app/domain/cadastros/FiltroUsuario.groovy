package cadastros

import acesso.Usuario

class FiltroUsuario {

	String nomeFiltro
	String filtroFilial
	String filtroCargo
	String filtroDepartamento
	String filtroFuncionario
	String filtroTipoFuncionario
	String filtroHorario
	String filtroSubUniorg
	Usuario usuario

	static mapping = {
		id generator: "sequence", params:[sequence: "filtro_usuario_seq"]
	}
}