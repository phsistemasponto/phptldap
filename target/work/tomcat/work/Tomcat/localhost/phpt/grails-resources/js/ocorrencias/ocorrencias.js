var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dscOcor").focus();
})

var txtOcorrencia;
var txtDescricao;

function buscaIdOcorrencia(inputOcorrencia, descricaoOcorrencia) {
	
	txtOcorrencia = inputOcorrencia;
	txtDescricao = descricaoOcorrencia;
	
	var stringBusca = $j(inputOcorrencia).val();

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/busca/buscaIdOcorrencia",
		data : {
			codigo : stringBusca
		},
		dataType : "json",
		success : function(json) {
			
			$j(inputOcorrencia).empty();
			$j(descricaoOcorrencia).empty();
			
			$j(inputOcorrencia).val(json.id);
			$j(descricaoOcorrencia).val(json.nome);
			
		},
		error : function(xhr) {
			
			$j(inputOcorrencia).val('');
			$j(descricaoOcorrencia).val('');
			
		}
	});
}

function buscarLovOcorrencia(inputOcorrencia, descricaoOcorrencia) {
	
	txtOcorrencia = inputOcorrencia;
	txtDescricao = descricaoOcorrencia;
	
	if ($j('#'+inputOcorrencia).attr('disabled') != 'disabled') {
		showBuscaInterna(inputOcorrencia, descricaoOcorrencia, 'cadastros.Ocorrencias', 'id', 'dscOcor', false);
	}

}

