var $j = jQuery.noConflict();

function generateReport(formato) {
	
	$j('input[name=_format]').val(formato);
	
	$j('#carregando').show();
	
	$j('.form-horizontal').submit(function(e) {
		
		var formObj = $j(this);
		var formURL = formObj.attr("action");
		var formData = new FormData(this);
		
		$j.ajax({
			url: formURL,
			type: 'POST',
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			dataType : "json",
			success: function(json) {
				
				$j('#carregando').hide();
				
				var urlPrintReport = context + "/reportPrint/printReport";
				window.open(urlPrintReport, '_blank');
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$j('#carregando').hide();
				if (jqXHR.status == 400) {
					alert('Sem registros');
				} else {
					alert('Erro na geração do relatório: ' + errorThrown);
				}
			},
			complete: function(jqXHR, textStatus) {
				$j('#carregando').hide();
			}
		});
		e.preventDefault(); //Prevent Default action.
		e.unbind();
	});
}

function showSalvarFiltro() {
	$j("#divModalSalvarFiltro").dialog({
		 width: 400,
		 height: 200,
		 modal: true,
		 draggable: true
	});
}

function showSalvarFiltroJustificativa() {
	$j("#divModalSalvarFiltroJustificativa").dialog({
		 width: 400,
		 height: 200,
		 modal: true,
		 draggable: true
	});
}

function salvaFiltroReport() {
	
	var nomeFiltro = $j('#filtroNomeReport').val();
	var ocorrencias = $j("input[name=ocorrenciasId]").map(function () {return this.value;}).get().join(",");
	var formUrl = context + $j('#urlSalvarFiltro').val();
	var formIndex = context + $j('#urlSalvarFiltro').val().substring(0,$j('#urlSalvarFiltro').val().lastIndexOf('/'));
	
	$j.ajax({
		async: false,
		type: "POST",
		url: formUrl,
		data: { nomeFiltro: nomeFiltro, ocorrencias: ocorrencias },
		dataType:"json",
		success: function(json) {
			alert('Filtro salvo com sucesso!');
			window.location.replace(formIndex);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert('Erro ao salvar filtro: ' + errorThrown);
		}
	});
	
}

function salvaFiltroJustificativaReport() {
	
	var nomeFiltro = $j('#filtroNomeReport').val();
	var justificativas = $j("input[name=justificativaId]").map(function () {return this.value;}).get().join(",");
	var formUrl = context + $j('#urlSalvarFiltro').val();
	var formIndex = context + $j('#urlSalvarFiltro').val().substring(0,$j('#urlSalvarFiltro').val().lastIndexOf('/'));
	
	$j.ajax({
		async: false,
		type: "POST",
		url: formUrl,
		data: { nomeFiltro: nomeFiltro, justificativas: justificativas },
		dataType:"json",
		success: function(json) {
			alert('Filtro salvo com sucesso!');
			window.location.replace(formIndex);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert('Erro ao salvar filtro: ' + errorThrown);
		}
	});
	
}