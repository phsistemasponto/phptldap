var $j = jQuery.noConflict();

function order(order, sortType) {
	$j('#order').val(order);
	$j('#sortType').val(sortType);
	$j('#formPesquisa').submit();
}

function popularRetornoLov(id,descricao){
	$j('#'+$j("#campoIdRetorno").val()).val(id);
	$j('#'+$j("#campoDescricaoRetorno").val()).val(descricao);
}

function buscaId(classeBusca, campoId, campoDescricao, inputIdRetorno, inputDescricaoRetorno, buscaFilial, idFilial){
	var stringBusca = $j('#'+inputIdRetorno).val();
	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaId",	
	   data: {classeBusca: classeBusca, campoId: campoId, campoDescricao: campoDescricao, stringBusca: stringBusca, buscaFilial: buscaFilial, filial: idFilial},
	   dataType:"json",
	   success:function(json){
	      $j('#'+inputIdRetorno).empty();
	      $j('#'+inputDescricaoRetorno).empty();
		  $j('#'+inputIdRetorno).val(json.id);			
		  $j('#'+inputDescricaoRetorno).val(json.descricao);						
	    }
	  ,
	  error:function(xhr){
	      $j('#'+inputIdRetorno).empty();
	      $j('#'+inputDescricaoRetorno).empty();	
	  }
	});	
}

/** LOV FILTRO PADRÃO */

function buscaIdFiltroUsuario(inputFiltroUsuario) {
	
	var stringBusca = $j(inputFiltroUsuario).val();
	
	if (stringBusca == null || stringBusca.trim().length == 0) return;

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaIdFiltroUsuario",	
	   data: {
		   codigo: stringBusca
	   },
	   dataType:"json",
	   success:function(json){
		   $j('#lovFiltroIdBanco').val(json.id);
		   $j('#nomeFiltro').val(json.nome);
		   $j('#filtroFilialId').val(json.filtroFilial);
		   $j('#filtroCargoId').val(json.filtroCargo);
		   $j('#filtroDepartamentoId').val(json.filtroDepartamento);
		   $j('#filtroFuncionarioId').val(json.filtroFuncionario);
		   $j('#filtroTipoFuncionarioId').val(json.filtroTipoFuncionario);
		   $j('#filtroHorarioId').val(json.filtroHorario);
		   $j('#filtroSubUniOrgId').val(json.filtroSubUniorg);
	   },
	   error:function(xhr){
		  $j('#lovFiltroIdBanco').val('');
		  $j('#nomeFiltro').val('');
		  $j('#filtroFilialId').val('');
		  $j('#filtroCargoId').val('');
		  $j('#filtroDepartamentoId').val('');
		  $j('#filtroFuncionarioId').val('');
		  $j('#filtroTipoFuncionarioId').val('');
		  $j('#filtroHorarioId').val('');
		  $j('#filtroSubUniOrgId').val('');
	   }
	});	
}

function showFiltroPadrao() {
	$j("#divLovPadrao").dialog({
		 width: 500,
		 height: 460,
		 modal: true
	});
}

function showBuscaInterna(campoIdRetorno, campoDescricaoRetorno, classeBusca, campoId, campoDescricao, multi, callback, filtraFiliaisUsuario) {
	
	$j("#campoIdRetorno").val(campoIdRetorno);
	$j("#campoDescricaoRetorno").val(campoDescricaoRetorno);
	$j("#classeBusca").val(classeBusca);
	$j("#campoId").val(campoId);
	$j("#campoDescricao").val(campoDescricao);
	$j("#multi").val(multi != null ? multi : true);
	$j("#callback").val(callback);
	$j("#filtraFiliaisUsuario").val(filtraFiliaisUsuario);
	
	$j("#divLovBuscaInterna").dialog({
		 width: 550,
		 height: 450,
		 modal: true
	});
	
	$j("#dscBusca").val('');
	$j("#btnLovInternoMarcaTodos").hide();
	$j("#btnLovInternoDesmarcaTodos").hide();
	$j("#btnLovInternoOk").hide();
	
	$j("#resultadoBusca").empty();
}

function buscarLovInternoResultado(){

	var campoIdRetorno = $j("#campoIdRetorno").val();
	var campoDescricaoRetorno = $j("#campoDescricaoRetorno").val();
	var classeBusca = $j("#classeBusca").val();
	var dscBusca = $j("#dscBusca").val();
	var campoId = $j("#campoId").val();
	var campoDescricao = $j("#campoDescricao").val();
	var multi = $j("#multi").val() === 'true';
	var callback = $j("#callback").val();
	var filtraFiliaisUsuario = $j("#filtraFiliaisUsuario").val();
	
	// campos especificos:
	var tipoJustificativaEspelhoPonto = $j("#tipoJustificativaEspelhoPonto").val();
	
	var filtraFilialEspecifica = $j("#filtraFilialEspecifica").val();
	if (filtraFilialEspecifica == null || filtraFilialEspecifica.trim().length == 0) {
		filtraFilialEspecifica = false;
	}
	var filialEspecifica = $j("#filialId").val();
	
	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/lovResultado",
	   data: {classeBusca: classeBusca, 
		   	  stringBusca: dscBusca, 
		   	  campoId: campoId, 
		   	  campoDescricao: campoDescricao, 
		   	  filtraFiliaisUsuario: filtraFiliaisUsuario,
		   	  tipoJustificativaEspelhoPonto: tipoJustificativaEspelhoPonto,
		   	  filtraFilialEspecifica: filtraFilialEspecifica,
		   	  filialEspecifica: filialEspecifica },
	   dataType:"json",
	   success:function(json){
	      $j("#resultadoBusca").empty();
		  if (json.length > 0){
			var tableHead = "<table id='resultado' class='table table-striped table-bordered table-condensed'>";
			    tableHead+= "	<thead>";
		        tableHead+= "		<tr>";
		        tableHead+= "  			<th style='width: 5%;'>&nbsp;</th>";
		        tableHead+= "  			<th style='width: 15%;'>Código</th>";
		        tableHead+= "  			<th style='width: 80%;'>Nome</th>";
		        
		   if (classeBusca == 'cadastros.Horario') {
			    tableHead+= "  			<th style='width: 10%;'>Horas</th>";
		   }
		   
		        tableHead+= "		</tr>";
		        tableHead+= "	</thead>";
		        tableHead+= "	<tbody>";
				tableHead+= "	</tbody>";
			    tableHead+= "</table>";
			$j("#resultadoBusca").append(tableHead);
		  } else {
			  alert('Busca sem resultados!'); 
			 $j("#resultadoBusca").append('<p>Nenhum resultado encontrado.</p>');
		  }
		  var rowTable;
	      $j.each(json, function(index, item){
			rowTable  = '<tr>';
			rowTable += '	<td>';
			if (multi) {
				rowTable += '		<input type="checkbox" name="itemLovInterno" id="itemLovInterno" value="' + item.id + '"/>';
			} else {
				rowTable += '		<input type="radio" name="itemLovInterno" id="itemLovInterno" value="' + item.id + '"/>';
			}
			rowTable += '	</td>';
			rowTable += '	<td>';
			rowTable += 		item.id;
			rowTable += '	</td>';
			rowTable += '	<td>';
			rowTable += 		item.descricao;
			rowTable += '	</td>';
			if (classeBusca == 'cadastros.Horario') {
				rowTable += '	<td>';
				rowTable += retornaTotalHoras(item.id);
				rowTable += '	</td>';
		    }
			rowTable += '</tr>';
	        $j("#resultado tbody").append(rowTable);
	      });
	      
	      if (multi) {
	    	  $j('#btnLovInternoMarcaTodos').show();
		      $j('#btnLovInternoDesmarcaTodos').show();  
	      }
	      
	      $j('#btnLovInternoOk').show();
	      
	      populaPreSelecionados();
	      
	  },
	  error:function(xhr){
	    $j("#resultadoBusca").empty();
		alert('Busca sem resultados!');
	  }
	});
}

function marcaTodosLovInterno() {
	$j('input[name^="itemLovInterno"]').attr("checked","true");
}

function desmarcaTodosLovInterno() {
	$j('input[name^="itemLovInterno"]').removeAttr("checked");
}

function populaPreSelecionados() {
	var nomeCampo = $j("#campoIdRetorno").val();
	var refCampo = '#' + nomeCampo;
	var campo = $j(refCampo);
	var valores = campo.val().split(',');
	$j.each(valores, function(index, value) {
		var check = '#itemLovInterno[value="' + value + '"]';
		$j(check).attr('checked', true);
	});
}

function popularRetornoLovInterno(){
	var multi = $j("#multi").val() === 'true';
	var callback = $j("#callback").val();
	
	var retorno = '';
	var descricaoRetorno = '';
	var selecao = $j('#itemLovInterno:checked');
	
	$j.each(selecao, function() {
		retorno += $j(this).attr('value');
		retorno += ','
		descricaoRetorno += $j(this).parent().next().next().text().trim();
		descricaoRetorno += ','
	});
	retorno = retorno.substr(0, retorno.length - 1);
	descricaoRetorno = descricaoRetorno.substr(0, descricaoRetorno.length - 1);
	
	$j('#'+$j("#campoIdRetorno").val()).val(retorno);
	$j('#'+$j("#campoDescricaoRetorno").val()).val(descricaoRetorno);
	
	$j('#divLovBuscaInterna').dialog('close');
	
	if (multi) {
		$j('#btnLovInternoMarcaTodos').hide();
	    $j('#btnLovInternoDesmarcaTodos').hide();
	}
	
	if (callback != null && callback != '') {
		eval(callback);
	}
	
    $j('#btnLovInternoOk').hide();
	
    buscaIdFiltroUsuario('#lovFiltroId');
}

/** Traz valores dos hiddens do form principal pros campos de texto do modal */
function configuraFiltroPadrao() {
	
	limpaFiltroPadrao();
	
	$j('#lovFiltroId').val($j('#filtroId').val());
	$j('#nomeFiltro').val($j('#filtroDesc').val());
	$j('#filtroFilialId').val($j('#filtroPadrao_FilialId').val());
	$j('#filtroCargoId').val($j('#filtroPadrao_CargoId').val());
	$j('#filtroDepartamentoId').val($j('#filtroPadrao_DepartamentoId').val());
	$j('#filtroFuncionarioId').val($j('#filtroPadrao_FuncionarioId').val());
	$j('#filtroTipoFuncionarioId').val($j('#filtroPadrao_TipoFuncionarioId').val());
	$j('#filtroHorarioId').val($j('#filtroPadrao_HorarioId').val());
	$j('#filtroSubUniOrgId').val($j('#filtroPadrao_SubUniOrgId').val());
	if ($j('#filtroPadrao_NaoListaDemitidos').val() != null && $j('#filtroPadrao_NaoListaDemitidos').val() == 'true') {
		$j('#filtroNaoListaDemitidos').attr("checked","true");
	} else {
		$j('#filtroNaoListaDemitidos').removeAttr("checked");
	}
}

/** Aplica filtro em hiddens do form principal */
function aplicaFiltroPadrao() {
	
	var form = $j('#formPesquisa');
	if (form == null || form.length == 0) {
		form = $j('form');
	}
	
	// remove todos
	form.find('input[name^="filtroPadrao_"]').remove();
	
	$j('#filtroId').val($j('#lovFiltroId').val());
	
	var nomePreenchido = $j('#nomeFiltro').val().trim().length > 0;
	var nomeFiltro = nomePreenchido ? $j('#nomeFiltro').val() : geraNomeFiltro();
	$j('#filtroDesc').val(nomeFiltro);
	
	var filial = '<input type=\"hidden\" id=\"filtroPadrao_FilialId\" name=\"filtroPadrao_FilialId\" value=\"' + $j('#filtroFilialId').val() + '\"/>';
	var cargo = '<input type=\"hidden\" id=\"filtroPadrao_CargoId\" name=\"filtroPadrao_CargoId\" value=\"' + $j('#filtroCargoId').val() + '\"/>';
	var departamento = '<input type=\"hidden\" id=\"filtroPadrao_DepartamentoId\" name=\"filtroPadrao_DepartamentoId\" value=\"' + $j('#filtroDepartamentoId').val() + '\"/>';
	var func = '<input type=\"hidden\" id=\"filtroPadrao_FuncionarioId\" name=\"filtroPadrao_FuncionarioId\" value=\"' + $j('#filtroFuncionarioId').val() + '\"/>';
	var tipoFunc = '<input type=\"hidden\" id=\"filtroPadrao_TipoFuncionarioId\" name=\"filtroPadrao_TipoFuncionarioId\" value=\"' + $j('#filtroTipoFuncionarioId').val() + '\"/>';
	var horario = '<input type=\"hidden\" id=\"filtroPadrao_HorarioId\" name=\"filtroPadrao_HorarioId\" value=\"' + $j('#filtroHorarioId').val() + '\"/>';
	var uniorg = '<input type=\"hidden\" id=\"filtroPadrao_SubUniOrgId\" name=\"filtroPadrao_SubUniOrgId\" value=\"' + $j('#filtroSubUniOrgId').val() + '\"/>';
	var naoListaDemitidos = '<input type=\"hidden\" id=\"filtroPadrao_NaoListaDemitidos\" name=\"filtroPadrao_NaoListaDemitidos\" value=\"' + $j('#filtroNaoListaDemitidos').is(':checked') + '\"/>';
	
	
	
	form.append(filial);
	form.append(cargo);
	form.append(departamento);
	form.append(func);
	form.append(tipoFunc);
	form.append(horario);
	form.append(uniorg);
	form.append(naoListaDemitidos);
	
	limpaFiltroPadrao();
	
	$j('input[value="undefined"]').val('')
	
	$j('#divLovPadrao').dialog('close');
	
}

function geraNomeFiltro() {
	var nomeFiltro = '';
	
	if ($j('#filtroFilialId').val() != null && $j('#filtroFilialId').val().trim().length > 0) {
		nomeFiltro += 'Filial (' + $j('#filtroFilialId').val() + ') ' 
	}
	
	if ($j('#filtroCargoId').val() != null && $j('#filtroCargoId').val().trim().length > 0) {
		nomeFiltro += 'Cargo (' + $j('#filtroCargoId').val() + ') ' 
	}
	
	if ($j('#filtroDepartamentoId').val() != null && $j('#filtroDepartamentoId').val().trim().length > 0) {
		nomeFiltro += 'Departamento (' + $j('#filtroDepartamentoId').val() + ') ' 
	}
	
	if ($j('#filtroFuncionarioId').val() != null && $j('#filtroFuncionarioId').val().trim().length > 0) {
		nomeFiltro += 'Funcionário (' + $j('#filtroFuncionarioId').val() + ') ' 
	}
	
	if ($j('#filtroTipoFuncionarioId').val() != null && $j('#filtroTipoFuncionarioId').val().trim().length > 0) {
		nomeFiltro += 'Tipo funcionário (' + $j('#filtroTipoFuncionarioId').val() + ') ' 
	}
	
	if ($j('#filtroHorarioId').val() != null && $j('#filtroHorarioId').val().trim().length > 0) {
		nomeFiltro += 'Horário (' + $j('#filtroHorarioId').val() + ') ' 
	}
	
	if ($j('#filtroSubUniOrgId').val() != null && $j('#filtroSubUniOrgId').val().trim().length > 0) {
		nomeFiltro += 'Sub UniOrg (' + $j('#filtroSubUniOrgId').val() + ') ' 
	}
	
	return nomeFiltro;
}

function limpaFiltroPadrao() {
	$j('#lovFiltroId').val('');
	$j('#nomeFiltro').val('');
	$j('#filtroFilialId').val('');
	$j('#filtroCargoId').val('');
	$j('#filtroDepartamentoId').val('');
	$j('#filtroFuncionarioId').val('');
	$j('#filtroTipoFuncionarioId').val('');
	$j('#filtroHorarioId').val('');
	$j('#filtroSubUniOrgId').val('');
}

function salvaFiltroPadrao(){
	if ($j('#nomeFiltro').val() == null || $j('#nomeFiltro').val() == '') {
		alert('Informe um nome para o filtro');
		return;
	}
	
	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/filtroUsuario/save",	
	   data: {
		   filtroId: $j('#lovFiltroId').val(),
		   nomeFiltro: $j('#nomeFiltro').val(), 
		   filtroFilial: $j('#filtroFilialId').val(),
		   filtroCargo: $j('#filtroCargoId').val(),
		   filtroDepartamento: $j('#filtroDepartamentoId').val(),
		   filtroFuncionario: $j('#filtroFuncionarioId').val(),
		   filtroTipoFuncionario: $j('#filtroTipoFuncionarioId').val(),
		   filtroHorario: $j('#filtroHorarioId').val(),
		   filtroSubUniorg: $j('#filtroSubUniOrgId').val()
	   },
	   dataType:"json",
	   complete:function(json){
		  alert('Filtro salvo com sucesso!');	
		  $j('#lovFiltroId').val(json.responseText);
		  
	    }
	});	
}

function consultaFiltroPadraoAjax() {
	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaFiltroPadrao",	
	   data: {
		   filtroPadrao_FilialId: $j('#filtroPadrao_FilialId').val(),
		   filtroPadrao_CargoId: $j('#filtroPadrao_CargoId').val(),
		   filtroPadrao_DepartamentoId: $j('#filtroPadrao_DepartamentoId').val(),
		   filtroPadrao_FuncionarioId: $j('#filtroPadrao_FuncionarioId').val(),
		   filtroPadrao_TipoFuncionarioId: $j('#filtroPadrao_TipoFuncionarioId').val(),
		   filtroPadrao_HorarioId: $j('#filtroPadrao_HorarioId').val(),
		   filtroPadrao_SubUniOrgId: $j('#filtroPadrao_SubUniOrgId').val(),
		   filtroPadrao_NaoListaDemitidos: $j('#filtroPadrao_NaoListaDemitidos').val()
	   },
	   dataType:"json",
	   success:function(json){
		   $j("#funcionarioTable tbody").empty();
		   var rowTable;
		   $j.each(json, function(index, item){
			   rowTable  = ''
			   rowTable  = '<tr id="rowTableFuncionario" class="even">';
			   rowTable += '	<td>';
			   rowTable += '		<input type="hidden" id="funcionarioIdTable" name="funcionarioIdTable" value="' + item.id + '" />';
			   rowTable += '		<input type="text" id="funcionarioMatriculaTable" name="funcionarioMatriculaTable" style="width:90%" value="' + item.matricula + '" readOnly="true"/>';
			   rowTable += '	</td>';
			   rowTable += '	<td>';
			   rowTable += '		<input type="text" id="funcionarioNomeTable" name="funcionarioNomeTable" style="width:90%" value="' + item.nome + '" readOnly="true"/>';
			   rowTable += '	</td>';
			   rowTable += '	<td>';
			   rowTable += '		<i class="icon-remove" onclick="excluirFuncionario(this);" style="cursor: pointer" title="Excluir registro"></i>';
			   rowTable += '	</td>';
			   rowTable += '</tr>';
			   $j("#funcionarioTable tbody").append(rowTable);
		   });
	    }
	});	
}

function retornaTotalHoras(idHorario){
	var totalHoras = '00:00';
	
	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/horarios/retornaQuantidadeHoras",
		data : {
			idHora: idHorario
		},
		dataType : "json",
		success : function(json) {
			totalHoras = json.total
		},
		error : function(xhr) {
			totalHoras = '00:00';		
		}
	});
	
	return totalHoras;
	
}