var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#nome").focus();
	 
	 $j("#dtNascimento").setMask("date"); 
	 $j("#dtNascimento").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
})

function atualizaPermissao(item) {
	var input = $j('#rowTablePerfil'+item).children('td').eq(0).children('[id^=permissao]');
	if (input.val()=='false'){
		input.val('true');
	} else {
		input.val('false');
	}
}

function marcarTodos(){
	$j('input[name^="temPerfil"]').attr("checked","true");
	$j('input[name^="permissao"]').val("true");
}
function desmarcarTodos(){
	$j('input[name^="temPerfil"]').removeAttr("checked");
	$j('input[name^="permissao"]').val("false");
	
}

// inicio -- buscas e LOV'S setor

function buscaIdSetor() {
	var setorId = $j('#setorId').val();

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/busca/buscaIdSetor",
		data : {
			codigo : setorId
		},
		dataType : "json",
		success : function(json) {
			$j('#setorId').empty();
			$j('#setorNome').empty();
			$j('#setorId').val(json.id);
			$j('#setorNome').val(json.nome);
		},
		error : function(xhr) {
			$j('#setorId').empty();
			$j('#setorNome').empty();
		}
	});
}

function buscarLovSetor() {
	showBuscaInterna('setorId', 'setorNome', 'cadastros.Setor', 'id', 'dscSetor', false);
}

// fim - buscas setor

// Inclusão e exclusão de Filial/Setor

function incluirFilial(){
	if ($j("#filialId").val() == '') {
		alert('Informe a Filial!');
	} else {
		var ultimo = $j('#sequenciaFilial').val();
		
		var filiais;
		var nomes;
		
		if ($j('#filialId').val().indexOf(',') > 0) {
			filiais = $j('#filialId').val().split(',');
			nomes = $j('#filialNome').val().split(',');
		} else {
			filiais = [$j('#filialId').val()];
			nomes = [$j('#filialNome').val()];
		}
		
		for (var i=0; i < filiais.length; i++) {
			var stringAppend  = "";
			stringAppend += '<tr id=\"rowTableFilial' + ultimo + '\" class=\"even\" >';
			stringAppend += '	<td>';
			stringAppend += '		<input type=\"hidden\" id=\"filialIdTable\" value=\"' + filiais[i] + '\" style=\"width:90%\" name=\"filialIdTable\" readonly/>'; 
			stringAppend += '		<input type=\"text\" id=\"filialIdDisplay\" value=\"' + filiais[i] + '\" style=\"width:90%\" name=\"filialIdDisplay\" readonly />';
			stringAppend += '	</td>';
			stringAppend += '	<td>';
			stringAppend += '		<input type=\"text\" id=\"filialNomeTable\" value=\"' + nomes[i] + '\" style=\"width:90%\" name=\"filialNomeTable\" readonly />';
			stringAppend += '	</td>'; 
			stringAppend += '	<td>';
			stringAppend += '		<i class=\"icon-remove\" onclick=\"excluirFilial('+ ultimo + ');\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>';
			stringAppend += '	</td>';
			stringAppend += '</tr>';
			
			$j('#filialTable > tbody:last').append(stringAppend);
		}
		
		ultimo = parseInt(ultimo) + 1;
		$j('#sequenciaFilial').val(ultimo);
	}	
}

function excluirFilial(elemento){
	$j('#rowTableFilial' + elemento).remove()
}

function incluirSetor() {
	if ($j("#setorId").val() == '') {
		alert('Informe o Setor!');
	} else {
		var ultimo = $j('#sequenciaSetor').val();
		
		var stringAppend  = "";
		stringAppend += '<tr id=\"rowTableSetor' + ultimo + '\" class=\"even\" >';
		stringAppend += '	<td>';
		stringAppend += '		<input type=\"hidden\" id=\"setorIdTable\" value=\"' + $j('#setorId').val() + '\" style=\"width:90%\" name=\"setorIdTable\" readonly/>'; 
		stringAppend += '		<input type=\"text\" id=\"setorIdDisplay\" value=\"' + $j('#setorId').val() + '\" style=\"width:90%\" name=\"setorIdDisplay\" readonly />';
		stringAppend += '	</td>';
		stringAppend += '	<td>';
		stringAppend += '		<input type=\"text\" id=\"setorNomeTable\" value=\"' + $j('#setorNome').val() + '\" style=\"width:90%\" name=\"setorNomeTable\" readonly />';
		stringAppend += '	</td>'; 
		stringAppend += '	<td>';
		stringAppend += '		<i class=\"icon-remove\" onclick=\"excluirSetor('+ ultimo + ');\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>';
		stringAppend += '	</td>';
		stringAppend += '</tr>';
		
		$j('#setorTable > tbody:last').append(stringAppend);
		
		ultimo = parseInt(ultimo) + 1;
		$j('#sequenciaSetor').val(ultimo);
	}	
}

function excluirSetor(elemento){
	$j('#rowTableSetor' + elemento).remove()
}


// Fim --  Inclusão e exclusão de Filial/Setor


//Inclusão e exclusão de Filial/Setor

function incluirFuncionario(){
	if ($j("#funcionarioId").val() == '') {
		alert('Informe o Funcionário!');
	} else {
		
		if (verificarExistenciaFuncionarioUsuario($j("#funcionarioId").val())==false){
		
			var stringAppend = "<tr id=\"rowTableFuncionario" 
					+ "\" class=\"even\" >"
					+ "<td><input type=\"hidden\" id=\"funcionarioIdTable\" value=\""
					+ $j('#funcionarioId').val() + "\" style=\"width:90%\" name=\"funcionarioIdTable\" readOnly=\"true\"/>" 
					+ "<input type=\"text\" id=\"filialMatriculaTable\" value=\""
					+ $j('#funcionarioMatricula').val() + "\" style=\"width:90%\" name=\"funcionarioMatriculaTable\" readOnly=\"true\"/></td>"
					+ "<td><input type=\"text\" id=\"funcionarioNomeTable\" value=\""
					+ $j('#funcionarioNome').val() + "\" style=\"width:90%\" name=\"funcionarioNomeTable\" readOnly=\"true\"/></td>" 
					//+ "<td><i class=\"icon-remove\" onclick=\"excluirFuncionario("+ ultimo + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i></td>+</tr>";
					+ "<td><i class=\"icon-remove\" onclick=\"excluirFuncionario(this);\" style=\"cursor:pointer\" title=\"Excluir registro\"></i></td>+</tr>";
			$j('#funcionarioTable > tbody:last').append(stringAppend);
	
			$j('#funcionarioMatricula').val('');
			$j('#funcionarioNome').val('');
		} else {
			alert('Funcionário já incluído!');
			$j('#funcionarioMatricula').val('');
			$j('#funcionarioNome').val('');
	
		}
		
	}	
}

function excluirFuncionario(elemento){
	$j(elemento).parent().parent().remove();
}



function verificarExistenciaFuncionarioUsuario(id){
	var retorno = false
	$j('#funcionarioTable tbody tr').each(
			function() {
				var tr = $j(this);
				if (id == tr.children('td').eq(0).children('[id^=funcionarioIdTable]').val()){
					retorno = true
				} 
			});	
	return retorno		
}

// Fim --  Inclusão e exclusão de Filial/Setor


function incluirGrupo() {
	if ($j("#grupoId").val() == '') {
		alert('Informe o Grupo!');
	} else {
		var ultimo = $j('#sequenciaGrupo').val();
		
		var stringAppend  = "";
		stringAppend += '<tr id=\"rowTableGrupo' + ultimo + '\" class=\"even\" >';
		stringAppend += '	<td>';
		stringAppend += '		<input type=\"hidden\" id=\"grupoIdTable\" value=\"' + $j('#grupoId').val() + '\" style=\"width:90%\" name=\"grupoIdTable\" readonly/>'; 
		stringAppend += '		<input type=\"text\" id=\"grupoIdDisplay\" value=\"' + $j('#grupoId').val() + '\" style=\"width:90%\" name=\"grupoIdDisplay\" readonly />';
		stringAppend += '	</td>';
		stringAppend += '	<td>';
		stringAppend += '		<input type=\"text\" id=\"grupoNomeTable\" value=\"' + $j('#grupoDescricao').val() + '\" style=\"width:90%\" name=\"grupoNomeTable\" readonly />';
		stringAppend += '	</td>'; 
		stringAppend += '	<td>';
		stringAppend += '		<i class=\"icon-remove\" onclick=\"excluirGrupo('+ ultimo + ');\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>';
		stringAppend += '	</td>';
		stringAppend += '</tr>';
		
		$j('#grupoTable > tbody:last').append(stringAppend);
		
		ultimo = parseInt(ultimo) + 1;
		$j('#sequenciaGrupo').val(ultimo);
	}	
}

function excluirGrupo(elemento){
	$j('#rowTableGrupo' + elemento).remove()
}

function alterarSenha() {
	if ($j('#login').val() == null || $j('#login').val().trim() == '') {
		alert('Informe o login');
		return false;
	}
	
	if ($j('#senha').val() == null || $j('#senha').val().trim() == '') {
		alert('Informe a senha');
		return false;
	}
	
	if ($j('#confirmSenha').val() == null || $j('#confirmSenha').val().trim() == '') {
		alert('Informe a confirmação de senha');
		return false;
	} 
	
	if ($j('#senha').val() != $j('#confirmSenha').val()) {
		alert('Confirmação da senha está incorreta');
		return false;
	}
	
	$j('#formAlterarSenha').submit();
}

function alterarSenhaUsuario() {
	if ($j('#senha').val() == null || $j('#senha').val().trim() == '') {
		alert('Informe a senha');
		return false;
	}
	
	if ($j('#confirmSenha').val() == null || $j('#confirmSenha').val().trim() == '') {
		alert('Informe a confirmação de senha');
		return false;
	} 
	
	if ($j('#senha').val() != $j('#confirmSenha').val()) {
		alert('Confirmação da senha está incorreta');
		return false;
	}
	
	$j('#formAlterarSenha').submit();
}

function reiniciarSenha() {
	if ($j('#login').val() == null || $j('#login').val().trim() == '') {
		alert('Informe o login');
		return false;
	}
	
	if ($j('#dtNascimento').val() == null || $j('#dtNascimento').val().trim() == '') {
		alert('Informe a data de nascimento');
		return false;
	} 
	
	$j('#formReiniciarSenha').submit();
}