var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 if (!($j('#id').length)){
		 desmarcarTodos();
	 } 
	
	 $j('#dataFeriado').setMask('date');
	 $j("#dataFeriado").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 alteraTipoFeriado();
});

function atualizaFilial(item) {
	var input = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=temFilialFeriadoRegional]');
	if (input.val()=='false'){
		input.val('true');
	} else {
		input.val('false');
	}
}

function marcarTodos(){
	$j('input[name^="temFilial"]').attr("checked","true");
	$j('input[name^="temFilialFeriadoRegional"]').val("true");
}

function desmarcarTodos(){
	$j('input[name^="temFilial"]').removeAttr("checked");
	$j('input[name^="temFilialFeriadoRegional"]').val("false");
}

function alteraTipoFeriado() {
	var tipo = $j('#tipo').val();
	if (tipo == 'M') {
		$j('#idParcial').attr('disabled', false);
	} else {
		$j('#idParcial').attr('disabled', true);
		$j('#idParcial').removeAttr("checked");
	}
}