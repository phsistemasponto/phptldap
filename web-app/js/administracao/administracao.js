var $j = jQuery.noConflict();

$j(document).ready(function() {
	$j('#cargahor').setMask('integer');
	$j('#diasAviso').setMask('integer');
	$j('#intervaloRepeticao').setMask('integer');
	$j('#periodicidade').setMask('integer');
	$j('#periodoInativo').setMask('integer');
	$j('#qtdMaxCaracRep').setMask('integer');
	$j('#qtdSenhasRepetidas').setMask('integer');
	$j('#quantidadeCaracteres').setMask('integer');
	$j('#tentativasLogin').setMask('integer');	
})
