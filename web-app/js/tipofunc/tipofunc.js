var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dsctpfunc").focus();
});


function buscaIdTipoFuncionario(){
	var stringBusca = $j('#tipoFuncionarioId').val();

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaId",	
	   data: {
		   classeBusca: 'cadastros.Tipofunc', 
		   campoId: 'id', 
		   campoDescricao: 'dsctpfunc', 
		   stringBusca: stringBusca, 
		   buscaFilial: false, 
		   filial: 0
	   },
	   dataType:"json",
	   success:function(json){
	      $j('#tipoFuncionarioId').empty();
	      $j('#tipoFuncionarioDescricao').empty();
		  $j('#tipoFuncionarioId').val(json.id);			
		  $j('#tipoFuncionarioDescricao').val(json.descricao);						
	    }
	  ,
	  error:function(xhr){
	      $j('#tipoFuncionarioId').empty();
	      $j('#tipoFuncionarioDescricao').empty();
	  }
	});	
}

function buscarLovTipoFuncionario(){
	showBuscaInterna('tipoFuncionarioId', 'tipoFuncionarioDescricao', 'cadastros.Tipofunc', 'id', 'dsctpfunc', false);
}