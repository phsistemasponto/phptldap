var $j = jQuery.noConflict();

$j(document).ready(function() {

	$j.mask.masks.numero = {
	    mask: '9999999'
	}
	
	$j("#nomFunc").focus();
	
	$j("#dataNscFunc").setMask("date"); 
    $j("#dataAdm").setMask("date");
    $j("#dataResc").setMask("date");
    $j("#docRgDataEmi").setMask("date");
    $j("#docCrtPrfDataEmi").setMask("date");
    $j("#dataInicioHorario").setMask("date");
    $j("#dataInicioConfiguracaoHoraExtra").setMask("date");
    $j("#dataInicioBeneficio").setMask("date");
    
    $j("#dataNscFunc").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
    $j("#dataAdm").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
    $j("#dataResc").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
    $j("#docRgDataEmi").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
    $j("#docCrtPrfDataEmi").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
    $j("#dataInicioHorario").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
    $j("#dataInicioConfiguracaoHoraExtra").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
    $j("#dataInicioBeneficio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
    
    $j("#crcFunc").setMask("numero");
    $j("#filialId").setMask("numero");
    $j("#setorId").setMask("numero");
    $j("#tipoFuncionarioId").setMask("numero");
	$j("#cargoId").setMask("numero");
	$j("#subUniorgId").setMask("numero");
	$j("#indiceHorario").setMask("numero");
	$j("#beneficioQuantidade").setMask("numero");
	
	$j('#dsrRemunerado').setMask('time');
	
	$j("#vrSal").setMask("decimal");
	
	if (($j("#filialId").val()!='')) {
		$j('#setorId').attr('disabled', false);
		$j('#setorBotaoBusca').attr('disabled', false);
	}
	
	var file = document.getElementById('files'); 
	if (file != null) {
		file.addEventListener('change', handleFileSelect, false);
	}
	
});

function handleFileSelect(evt) {
    var files = evt.target.files;

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }
      
      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          document.getElementById('divFoto').innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('mudouFoto').value = 'true';
          if (document.getElementById('video') != null) {
        	  document.getElementById('video').remove();  
          }
          
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
}


function buscaIdFuncionario(){
	var stringBusca = $j('#funcionarioId').val();

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaId",	
	   data: {
		   classeBusca: 'cadastros.Funcionario', 
		   campoId: 'crcFunc', 
		   campoDescricao: 'nomFunc', 
		   stringBusca: stringBusca, 
		   buscaFilial: false, 
		   filial: 0
	   },
	   dataType:"json",
	   success:function(json){
	      $j('#funcionarioId').empty();
	      $j('#funcionarioNome').empty();
		  $j('#funcionarioId').val(json.id);			
		  $j('#funcionarioNome').val(json.descricao);						
	    }
	  ,
	  error:function(xhr){
	      $j('#funcionarioId').empty();
	      $j('#funcionarioNome').empty();
	  }
	});	
}

//refatorar e mudar para setor.js
function buscarLovFuncionario(){
	showBuscaInterna('funcionarioId', 'funcionarioNome', 'cadastros.Funcionario', 'crcFunc', 'nomFunc', false);
}



////////////////////////
// FUNÇÕES DE HORÁRIO //
////////////////////////

function buscaIdHorario(){
	var stringBusca = $j('#horarioId').val();

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaId",	
	   data: {
		   classeBusca: 'cadastros.Horario', 
		   campoId: 'id', 
		   campoDescricao: 'dscHorario', 
		   stringBusca: stringBusca, 
		   buscaFilial: false, 
		   filial: 0
	   },
	   dataType:"json",
	   success:function(json){
	      $j('#horarioId').empty();
	      $j('#horarioDescricao').empty();
		  $j('#horarioId').val(json.id);			
		  $j('#horarioDescricao').val(json.descricao);						
	    }
	  ,
	  error:function(xhr){
	      $j('#horarioId').empty();
	      $j('#horarioDescricao').empty();
	  }
	});	
}

function buscarLovHorario(){
	showBuscaInterna('horarioId', 'horarioDescricao', 'cadastros.Horario', 'id', 'dscHorario', false);
}

function incluirHorario () {
	if ($j("#horarioId").val() == '' || $j("#horarioDescricao").val() == '') {
		alert('Informe o horário!');
	} else if ($j("#dataInicioHorario").val() == '') {
		alert('Informe a data início!');
	} else if ($j("#indiceHorario").val() == '') {
		alert('Informe o índice!');
	} else {
		
		var indice = parseInt($j('#horarioTable > tbody > tr:last > td:first > input[name="sequencialTempHorario"]').val()) + 1;
		
		var stringAppend = "<tr id=\"rowTableTempHorario" + indice + "\" class=\"even\">"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"nomeHorario\" name=\"nomeHorario\" value=\"" + $j('#horarioDescricao').val() + "\" readonly=\"true\" style=\"width:80%\" />"
			+ "<input type=\"hidden\" id=\"idHorario\" name=\"idHorario\" value=\"" + $j('#horarioId').val() +"\"/>"
			+ "<input type=\"hidden\" id=\"sequencialTempHorario\" name=\"sequencialTempHorario\" value=\"" + indice +"\"/>"
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"dtInicioHorario\" name=\"dtInicioHorario\" value=\"" + $j('#dataInicioHorario').val() + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"dtFimHorario\" name=\"dtFimHorario\" value=\"\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"idcHorario\" name=\"idcHorario\" value=\"" + $j('#indiceHorario').val() + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<i class=\"icon-remove\" onclick=\"excluirHorario("+ indice + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>" 
			+ "</td>"
			
			+ "</tr>";
		
		$j('#horarioTable > tbody > tr:last > td:last > i').remove();
		$j('#horarioTable > tbody > tr:last > td:eq(2) > input[name="dtFimHorario"]').val($j("#dataInicioHorario").val());
		
		$j('#horarioTable > tbody:last').append(stringAppend);
		
		resetCamposHorario();
	}
		
}

function resetCamposHorario() {
	$j('#horarioId').val('');
	$j('#horarioDescricao').val('');
	$j('#dataInicioHorario').val('');
	$j('#indiceHorario').val('');
}

function excluirHorario (indice) {
	$j('#rowTableTempHorario' + indice).remove();
	
	$j('#horarioTable > tbody > tr:last > td:last').append(
			"<i class=\"icon-remove\" onclick=\"excluirHorario("+ (indice-1) + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>");
	$j('#horarioTable > tbody > tr:last > td:eq(2) > input[name="dtFimHorario"]').val('');
}


//////////////////////////////////////////
//FUNÇÕES DE CONFIGURAÇÃO HORAS EXTRAS //
////////////////////////////////////////

function buscaIdConfiguracaoHoraExtra(){
	var stringBusca = $j('#configHoraExtraId').val();

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaId",	
	   data: {
		   classeBusca: 'cadastros.ConfiguracaoHoraExtra', 
		   campoId: 'id', 
		   campoDescricao: 'descricao', 
		   stringBusca: stringBusca
	   },
	   dataType:"json",
	   success:function(json){
	      $j('#configHoraExtraId').empty();
	      $j('#configuracaoHoraExtraDescricao').empty();
		  $j('#configHoraExtraId').val(json.id);			
		  $j('#configuracaoHoraExtraDescricao').val(json.descricao);						
	    }
	  ,
	  error:function(xhr){
	      $j('#configHoraExtraId').empty();
	      $j('#configuracaoHoraExtraDescricao').empty();
	  }
	});	
}

function buscarLovConfiguracaoHoraExtra(){
	showBuscaInterna('configHoraExtraId', 'configuracaoHoraExtraDescricao', 'cadastros.ConfiguracaoHoraExtra', 'id', 'descricao', false);
}


function incluirConfiguracaoHoraExtra () {
	if ($j("#configHoraExtraId").val() == '' || $j("#configuracaoHoraExtraDescricao").val() == '') {
		alert('Informe a configuração hora extra!');
	} else if ($j("#dataInicioConfiguracaoHoraExtra").val() == '') {
		alert('Informe a data início!');
	} else {
		
		var indice = parseInt($j('#configuracaoHoraExtraTable > tbody > tr:last > td:first > input[name="sequencialTempConfiguracaoHoraExtra"]').val()) + 1;
		
		var stringAppend = "<tr id=\"rowTableTempConfiguracaoHoraExtra" + indice + "\" class=\"even\">"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"nomeConfiguracaoHoraExtra\" name=\"nomeConfiguracaoHoraExtra\" value=\"" + $j('#configuracaoHoraExtraDescricao').val() + "\" readonly=\"true\" style=\"width:80%\" />"
			+ "<input type=\"hidden\" id=\"idConfiguracaoHoraExtra\" name=\"idConfiguracaoHoraExtra\" value=\"" + $j('#configHoraExtraId').val() +"\"/>"
			+ "<input type=\"hidden\" id=\"sequencialTempConfiguracaoHoraExtra\" name=\"sequencialTempConfiguracaoHoraExtra\" value=\"" + indice +"\"/>"
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"dtInicioConfigHoraExtra\" name=\"dtInicioConfigHoraExtra\" value=\"" + $j('#dataInicioConfiguracaoHoraExtra').val() + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"dtFimConfigHoraExtra\" name=\"dtFimConfigHoraExtra\" value=\"\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<i class=\"icon-remove\" onclick=\"excluirConfiguracaoHoraExtra("+ indice + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>" 
			+ "</td>"
			
			+ "</tr>";
		
		$j('#configuracaoHoraExtraTable > tbody > tr:last > td:last > i').remove();
		$j('#configuracaoHoraExtraTable > tbody > tr:last > td:eq(2) > input[name="dtFimConfigHoraExtra"]').val($j("#dataInicioConfiguracaoHoraExtra").val());
		
		$j('#configuracaoHoraExtraTable > tbody:last').append(stringAppend);
		
		resetCamposConfiguracaoHoraExtra();
	}
		
}

function resetCamposConfiguracaoHoraExtra() {
	$j('#configHoraExtraId').val('');
	$j('#configuracaoHoraExtraDescricao').val('');
	$j('#dataInicioConfiguracaoHoraExtra').val('');
}

function excluirConfiguracaoHoraExtra (indice) {
	$j('#rowTableTempConfiguracaoHoraExtra' + indice).remove();
	
	$j('#configuracaoHoraExtraTable > tbody > tr:last > td:last').append(
			"<i class=\"icon-remove\" onclick=\"excluirConfiguracaoHoraExtra("+ (indice-1) + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>");
	$j('#configuracaoHoraExtraTable > tbody > tr:last > td:eq(2) > input[name="dtFimConfigHoraExtra"]').val('');
}


///////////////////////////
//FUNÇÕES DE BENEFICIOS //
//////////////////////////

function incluirBeneficio () {
	if ($j("#beneficioId").val() == '' || $j("#beneficioDescricao").val() == '') {
		alert('Informe o benefício!');
	} else if ($j("#beneficioQuantidade").val() == '') {
		alert('Informe a quantidade!');
	} else if ($j("#dataInicioBeneficio").val() == '') {
		alert('Informe a data início!');
	} else {
		
		var indice = parseInt($j('#configuracaoBeneficioTable > tbody > tr:last > td:first > input[name="sequencialTempBeneficio"]').val()) + 1;
		var beneficioId = $j('#beneficioId').val();
		
		var stringAppend = "<tr id=\"rowTableTempBeneficio" + indice + "\" class=\"even\">"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"nomeBeneficio\" name=\"nomeBeneficio\" value=\"" + $j('#beneficioDescricao').val() + "\" readonly=\"true\" style=\"width:80%\" />"
			+ "<input type=\"hidden\" id=\"idBeneficio\" name=\"idBeneficio\" value=\"" + beneficioId + "\"/>"
			+ "<input type=\"hidden\" id=\"sequencialTempBeneficio\" name=\"sequencialTempBeneficio\" value=\"" + indice +"\"/>"
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"qtBeneficio\" name=\"qtBeneficio\" value=\"" + $j('#beneficioQuantidade').val() + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"dtInicioBeneficio\" name=\"dtInicioBeneficio\" value=\"" + $j('#dataInicioBeneficio').val() + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"dtFimBeneficio-" + indice + "\" name=\"dtFimBeneficio-" + indice + "\" value=\"\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<i class=\"icon-remove\" onclick=\"excluirBeneficio("+ indice + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>" 
			+ "</td>"
			
			+ "</tr>";

		
		// percorrer tr's, ver o primeiro q ta null e preencher data
		$j('#configuracaoBeneficioTable > tbody > tr').each(
				function() {
					var tr = $j(this);
					var beneficioRowId = $j(tr.children(':first')).children('input[name=idBeneficio]').val();
					var beneficioRowDataFim = $j(tr.children()[3]).children('input[name^=dtFimBeneficio]').val();
					if (beneficioRowId == beneficioId && beneficioRowDataFim == '') {
						$j(tr.children()[3]).children('input[name^=dtFimBeneficio]').val($j("#dataInicioBeneficio").val());
					}
				}
		);
		
		$j('#configuracaoBeneficioTable > tbody:last').append(stringAppend);
		
		resetCamposBeneficio();
	}
		
}

function resetCamposBeneficio() {
	$j('#beneficioId').val('');
	$j('#beneficioDescricao').val('');
	$j('#dataInicioBeneficio').val('');
	$j('#beneficioQuantidade').val('');
	$j('#beneficioValor').val('');
}

function excluirBeneficio (indice) {
	var beneficioId =  $j('#rowTableTempBeneficio' + indice + ' > td:first > input[name=idBeneficio]').val();
	$j('#rowTableTempBeneficio' + indice).remove();
	var ultimoBeneficio = null;
	$j('#configuracaoBeneficioTable > tbody > tr').each(
			function() {
				var tr = $j(this);
				var beneficioRowId = $j(tr.children(':first')).children('input[name=idBeneficio]').val();
				if (beneficioRowId == beneficioId) {
					ultimoBeneficio = tr;
				}
			}
	);
	
	if (ultimoBeneficio != null) {
		$j(ultimoBeneficio.children()[3]).children('input[name^=dtFimBeneficio]').val('');
	}
}

function redirectEspelhoPonto() {
	$j('#formEspelhoPonto').submit();
}

function SomenteNumero(e){
	 var tecla=(window.event)?event.keyCode:e.which;
	 if((tecla>47 && tecla<58)) return true;
	 else{
	 if (tecla==8 || tecla==0) return true;
	 else  return false;
	 }
	}
