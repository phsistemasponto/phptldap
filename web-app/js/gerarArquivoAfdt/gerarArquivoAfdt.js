var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j("#dataInicio").focus();
	 
});

function validateForm() {
	
	if ($j("#dataInicio").val() == null || $j("#dataInicio").val().trim() == '') {
		alert('Informe a data início');
		return false;
	}
	
	if ($j("#dataFim").val() == null || $j("#dataFim").val().trim() == '') {
		alert('Informe a data fim');
		return false;
	}
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	return true;
}

function gerarArquivo() {
	$j('#carregando').show();
	
	$j('.form-horizontal').submit(function(e) {
		
		var formObj = $j(this);
		var formURL = formObj.attr("action");
		var formData = new FormData(this);
		
		$j.ajax({
			url: formURL,
			type: 'POST',
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			dataType : "json",
			success: function(json) {
				$j('#carregando').hide();
				$j('#baixarArquivo').show();
				$j('#baixarArquivoId').val(json);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$j('#carregando').hide();
				if (jqXHR.status == 400) {
					alert('Sem registros');
				} else {
					alert('Erro na geração do arquivo: ' + errorThrown);
				}
			},
			complete: function(jqXHR, textStatus) {
				$j('#carregando').hide();
			}
		});
		e.preventDefault(); //Prevent Default action.
		e.unbind();
	});
}

function hideBaixar() {
	$j('#baixarArquivo').hide();
}