var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	$j('#dataInicioInput').setMask('date');
	$j('#dataFimInput').setMask('date');
	$j("#dataInicioInput").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	$j('#dataFimInput').datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	$j('#valorInput').maskMoney({decimal:',', thousands:'.'});
	 
});

function incluirValor(){
	if ($j("#valorInput").val() == '') {
		alert('Informe o valor!');
	} else if ($j("#dataInicioInput").val() == '') {
		alert('Informe a data início!');
	} else {
		var ultimo = $j('#sequencia').val();
		
		if (ultimo!='1'){
			var penultimo = parseInt(ultimo)-1;
			$j('#rowTableValorBeneficio' + penultimo).children('td').eq(3).children('i').remove();
			$j('#rowTableValorBeneficio' + penultimo).children('td').eq(2).children('input').val($j("#dataInicioInput").val());
		}
		
		var stringAppend = "<tr id=\"rowTableValorBeneficio" + ultimo + "\" class=\"even\" >"
				
				+ "<td><input type=\"text\" id=\"idvben\" readonly=\"true\" value=\""
				+ $j('#valorInput').val() + "\" style=\"width:30%\" name=\"idvben\"/>" 
				+ "<input type=\"hidden\" id=\"seqval\" name=\"seqval\" value=\"" + ultimo + "\"/></td>"
				
				+ "<td><input type=\"text\" id=\"dtInicio\" name=\"dtInicio\" value=\""
				+ $j('#dataInicioInput').val() + "\" readOnly=\"true\" style=\"width:30%\"/></td>"
				
				+ "<td><input type=\"text\" id=\"dtFim\" name=\"dtFim\" readOnly=\"true\" style=\"width:30%\"/></td>"
				
				+ "<td><i class=\"icon-remove\" onclick=\"excluirValor("+ ultimo + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i></td>+</tr>";
	
		$j('#valorBeneficioTable > tbody:last').append(stringAppend);
		
		ultimo = parseInt(ultimo) + 1;
		$j('#sequencia').val(ultimo);
	}	
}

function excluirValor(elemento){
	var penultimo = parseInt(elemento)-1
	if (penultimo>0){
		$j('#rowTableValorBeneficio' + penultimo).children('td').eq(3).append(
				"<i class=\"icon-remove\" onclick=\"excluirValor("+ penultimo + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>");
	}
	$j('#rowTableValorBeneficio' + elemento).remove()
	$j('#sequencia').val(elemento)
}


function buscaIdBeneficio(){
	var stringBusca = $j('#beneficioId').val();

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaIdBeneficio",	
	   data: {
		   classeBusca: 'cadastros.Beneficio', 
		   campoId: 'id', 
		   campoDescricao: 'descricao', 
		   stringBusca: stringBusca,
		   codigo: stringBusca
	   },
	   dataType:"json",
	   success:function(json){
	      $j('#beneficioId').empty();
	      $j('#beneficioDescricao').empty();
	      $j('#beneficioValor').empty();
		  
	      $j('#beneficioId').val(json.id);			
		  $j('#beneficioDescricao').val(json.nome);
		  $j('#beneficioValor').val(json.valor);
	    }
	  ,
	  error:function(xhr){
	      $j('#beneficioId').val('');
	      $j('#beneficioDescricao').val('');
	      $j('#beneficioValor').val('');
	  }
	});	
}

function buscarLovBeneficio() {
	showBuscaInterna('beneficioId', 'beneficioDescricao', 'cadastros.Beneficio', 'id', 'descricao', false, 'buscaIdBeneficio()');
}

function marcarTodos() {
	$j('input[name^="filial"]').attr("checked",true);
}

function desmarcarTodos() {
	$j('input[name^="filial"]').attr("checked",false);
}