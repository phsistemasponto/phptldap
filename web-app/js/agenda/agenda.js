var $j = jQuery.noConflict();

$j(document).ready(function() {
	alimentaDias();
});

function alimentaDias() {
	
	if ($j('#diaSalvo-0') != null) {
		$j('#grupoHorario-0').val($j('#diaSalvo-0').val());
	}
	
	if ($j('#diaSalvo-1') != null) {
		$j('#grupoHorario-1').val($j('#diaSalvo-1').val());
	}
	
	if ($j('#diaSalvo-2') != null) {
		$j('#grupoHorario-2').val($j('#diaSalvo-2').val());
	}
	
	if ($j('#diaSalvo-3') != null) {
		$j('#grupoHorario-3').val($j('#diaSalvo-3').val());
	}
	
	if ($j('#diaSalvo-4') != null) {
		$j('#grupoHorario-4').val($j('#diaSalvo-4').val());
	}
	
	if ($j('#diaSalvo-5') != null) {
		$j('#grupoHorario-5').val($j('#diaSalvo-5').val());
	}
	
	if ($j('#diaSalvo-6') != null) {
		$j('#grupoHorario-6').val($j('#diaSalvo-6').val());
	}
}

function marcarTodos() {
	$j('input[name^="filial"]').attr("checked",true);
}

function desmarcarTodos() {
	$j('input[name^="filial"]').attr("checked",false);
}