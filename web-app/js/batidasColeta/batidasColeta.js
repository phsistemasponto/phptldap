var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	$j("#dataInicioInput").setMask("date");
	$j("#dataFimInput").setMask("date");
	
	$j("#dataInicioInput").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	$j('#dataFimInput').datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	
	$j("#formBatidas").submit(function(e) {
		var formObj = $j(this);
		var formURL = formObj.attr("action");
		var formData = new FormData(this);
		
		$j('#carregando').show();
		
		$j.ajax({
			url: formURL,
			type: 'POST',
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			dataType : "json",
			success: function(json) {
				$j('#carregando').hide();
				$j('#tableResultLog > tbody').empty();
				$j('#conteudoArquivo').val();
				var conteudoArquivo = '';
				$j.each(json, function(index, item) {
					conteudoArquivo += item.dtBat + ', ' + item.dtLog + ', ' + item.funcionario + ', ' + item.mensagem + '\n'; 
					var append =  '<tr>';
						append += '	<td>';
						append += item.dtBat;
						append += '	</td>';
						append += '	<td>';
						append += item.dtLog;
						append += '	</td>';
						append += '	<td>';
						append += item.funcionario;
						append += '	</td>';
						append += '	<td>';
						append += item.mensagem;
						append += '	</td>';
						append += '</tr>';
					$j("#tableResultLog > tbody").append(append);
				});
				$j("#divLogBatidas").dialog({width: 800, height: 500, modal: true});
				$j('#conteudoArquivo').val(conteudoArquivo);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$j('#carregando').hide();
				alert('Erro ao invocar rotina');
			},
			complete: function(jqXHR, textStatus) {
				$j('#carregando').hide();
				alert('Rotina invocada com sucesso');
			}
		});
		e.preventDefault(); //Prevent Default action.
		e.unbind();
	}); 
	
	alteraControleBatida();
	 
});

function incluirDatas() {
	var dtInicio = $j('#dataInicioInput').val();
	var dtFim = $j('#dataFimInput').val();
	
	var importarIndividual = $j("#importarIndividual").is(':checked');
	
	if (importarIndividual) {
		
		$j('input#dtInicioFuncionario').each(
			function() {
				var input = $j(this);
				input.val(dtInicio);
			}
		);	
		
		$j('input#dtFimFuncionario').each(
			function() {
				var input = $j(this);
				input.val(dtFim);
			}
		);
			
	} else {
		
		$j('input#dtInicioFilial').each(
			function() {
				var input = $j(this);
				input.val(dtInicio);
			}
		);	
		
		$j('input#dtFimFilial').each(
			function() {
				var input = $j(this);
				input.val(dtFim);
			}
		);
		
	}
	
	
	
}

function alteraControleBatida() {
	var importarIndividual = $j("#importarIndividual").is(':checked');
	
	if (importarIndividual) {
		$j('#filiaisTable').hide();
		$j('#funcionarioTable').show();
	} else {
		$j('#filiaisTable').show();
		$j('#funcionarioTable').hide();
	}
	
}

function marcarTodos(){
	$j('input[name^="temFilial"]').attr("checked","true");
}

function desmarcarTodos(){
	$j('input[name^="temFilial"]').removeAttr("checked");
	
}

function processarBatidas() {
	$j('#formBatidas').submit();
}