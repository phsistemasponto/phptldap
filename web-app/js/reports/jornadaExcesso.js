var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j('#cargaHoraria').setMask('time');
	 $j('#tolerancia').setMask('time');
	 
	 $j('#cargaHorariaDiv').hide();
	 $j('#toleranciaDiv').hide();
	 
	 $j('#relatorio').on('change', function() {
		 
		 $j('#cargaHorariaDiv').hide();
		 $j('#toleranciaDiv').hide();
		 
		 if (this.value == '0') {
			 $j('#cargaHorariaDiv').show();
		 } else if (this.value == '2' || this.value == '4') {
			 $j('#toleranciaDiv').show();
		 }
	 });
	 
});

function validateReport() {
	
	if ($j("#dataInicio").val() == null || $j("#dataInicio").val().trim() == '') {
		alert('Informe a data início');
		return false;
	}
	
	if ($j("#dataFim").val() == null || $j("#dataFim").val().trim() == '') {
		alert('Informe a data fim');
		return false;
	}
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	return true;
}