var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j('#classificacao').parent().parent().hide();
	 $j('#agrupamento').parent().parent().hide();
	 
	 $j('#relatorio').on('change', function() {
		 if (this.value == '2' || this.value == '3') {
			 $j('#classificacao').parent().parent().show();
			 $j('#agrupamento').parent().parent().hide();
		 } else if (this.value == '5') {
			 $j('#classificacao').parent().parent().hide();
			 $j('#agrupamento').parent().parent().show();
		 } else {
			 $j('#classificacao').parent().parent().hide();
			 $j('#agrupamento').parent().parent().hide();
		 }
	 });
	 
});

function validateReport() {
	
	if ($j("#dataInicio").val() == null || $j("#dataInicio").val().trim() == '') {
		alert('Informe a data início');
		return false;
	}
	
	if ($j("#dataFim").val() == null || $j("#dataFim").val().trim() == '') {
		alert('Informe a data fim');
		return false;
	}
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	return true;
}