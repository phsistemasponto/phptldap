var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j('#tipoRelatorio').on('change', function() {
		 if (this.value == '0') {
			 $j('#agrupamento').parent().parent().hide();
		 } else {
			 $j('#agrupamento').parent().parent().show();
		 }
	 });
	 
	 if ($j('#tipoRelatorio').val() == '0') {
		 $j('#agrupamento').parent().parent().hide();
	 } else {
		 $j('#agrupamento').parent().parent().show();
	 }
	 
});

function mudaAno() {
	$j('#periodo').empty();
	$j('#periodo').append('<option value="">..</option>');
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaPorAno",
		data: {ano: $j('#ano').val(), status: 'E' },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var option = '<option value="' + item.id + '">' + item.descricao + '</option>';
				$j('#periodo').append(option);
			});
		},
		error:function(xhr){
		}
	});	
	
}

function mudaPeriodo() {
	
	$j('#dataInicio').val('');
	$j('#dataFim').val('');
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaDatas",
		data: {periodoId: $j('#periodo').val() },
		dataType:"json",
		success:function(json){
			$j('#dataInicio').val(json.dataInicio);
			$j('#dataFim').val(json.dataFim);
		},
		error:function(xhr){
		}
	});	
}

function validateReport() {
	
	if ($j("#dataInicio").val() == null || $j("#dataInicio").val().trim() == '') {
		alert('Informe a data início');
		return false;
	}
	
	if ($j("#dataFim").val() == null || $j("#dataFim").val().trim() == '') {
		alert('Informe a data fim');
		return false;
	}
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	if ($j("#periodo").val() == null || $j("#periodo").val().trim() == '') {
		alert('Informe o período');
		return false;
	}
	
	return true;
}

function excluirOcorrencia(id) {
	$j('#rowTableOcorrencia' + id).remove();
}

function aplicaFiltro() {
	
	
	$j('#tbodyOcor').empty();
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/relatorioFechamento/aplicarFiltro",
		data: {paramRelat: $j('#filtroSalvo').val() },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var tr = '<tr id="rowTableOcorrencia' + item.id + '">';
				    tr +='	<td>';
				    tr +=		item.id;
				    tr +='		<input type="hidden" name="ocorrenciasId" id="ocorrenciasId" value="'+ item.id +'">';
				    tr +='	</td>';
				    tr +='	<td>' + item.descricao + '</td>';
				    tr +='	<td>' + item.formula + '</td>';
				    tr +='	<td>';
				    tr +='		<i class="icon-remove" onclick="excluirOcorrencia(' + item.id + ');" style="cursor: pointer" title="Excluir registro"></i>';
				    tr +='	</td>';
				    tr +='</tr>';
				$j('#tbodyOcor').append(tr);
			});
		},
		error:function(xhr){
		}
	});
	
}