var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j('#diferencaHoras').setMask('time');
	 
	 $j('#verificacaoPresencaDiv').hide();
	 $j('#agrupadoPorFilialDiv').hide();
	 $j('#diferencaHorasDiv').hide();
	 
	 $j('#relatorio').on('change', function() {
		 
		 $j('#verificacaoPresencaDiv').hide();
		 $j('#agrupadoPorFilialDiv').hide();
		 $j('#diferencaHorasDiv').hide();
		 
		 if (this.value == '0') {
			 $j('#verificacaoPresencaDiv').show();
		 } else if (this.value == '1') {
			 $j('#agrupadoPorFilialDiv').show();
		 } else if (this.value == '5') {
			 $j('#diferencaHorasDiv').show();
		 }
	 });
	 
});

function validateReport() {
	
	if ($j("#dataInicio").val() == null || $j("#dataInicio").val().trim() == '') {
		alert('Informe a data início');
		return false;
	}
	
	if ($j("#dataFim").val() == null || $j("#dataFim").val().trim() == '') {
		alert('Informe a data fim');
		return false;
	}
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	return true;
}

function excluirOcorrencia(id) {
	$j('#rowTableOcorrencia' + id).remove();
}