var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j.mask.masks.numero = {
		    mask: '99'
	 }
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j("#meses").setMask("numero");
	 
	 $j('#relatorio').on('change', function() {
		 
		 $j('#divDatas').hide();
		 $j('#divControles').hide();
		 $j('#divPlanilha').hide();
		 
		 if (this.value == '0') {
			 $j('#divDatas').show();
			 $j('#divControles').show();
		 } else if (this.value == '1') {
			 $j('#divPlanilha').show();
		 } else if (this.value == '2') {
			 $j('#divPlanilha').show();
		 } else if (this.value == '3') {
			 $j('#divPlanilha').show();
		 } else if (this.value == '4') {
			 $j('#divPlanilha').show();
		 } else if (this.value == '5') {
			 $j('#divPlanilha').show();
		 }
	 });
	 
	 $j('#tipoRelatorio').on('change', function() {
		 if (this.value == '0') {
			 $j('#agrupamento').parent().parent().hide();
		 } else {
			 $j('#agrupamento').parent().parent().show();
		 }
	 });
	 
	 if ($j('#tipoRelatorio').val() == '0') {
		 $j('#agrupamento').parent().parent().hide();
	 } else {
		 $j('#agrupamento').parent().parent().show();
	 }
	 
});

function mudaMeses() {
	$j('#tbodyPeriodos').empty();
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaUltimosMeses",
		data: {meses: $j('#meses').val() },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var tr = '<tr>';
				    tr +='	<td><input type="checkbox" name="periodosId" id="periodosId" value="' + item.id + '" checked="checked"/></td>';
				    tr +='	<td>' + item.nome + '</td>';
				    tr +='</tr>';
				$j('#tbodyPeriodos').append(tr);
			});
		},
		error:function(xhr){
		}
	});	
	
}

function validateReport() {
	
	if ($j('#relatorio').val() == '0') {
		if ($j("#dataInicio").val() == null || $j("#dataInicio").val().trim() == '') {
			alert('Informe a data início');
			return false;
		}
		
		if ($j("#dataFim").val() == null || $j("#dataFim").val().trim() == '') {
			alert('Informe a data fim');
			return false;
		}
	}
	
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	return true;
}

function excluirOcorrencia(id) {
	$j('#rowTableOcorrencia' + id).remove();
}

function buscaIdJustificativaFalta() {
	buscaId('cadastros.Justificativas', 'id', 'dscJust', 'justFaltaId', 'justFaltaDesc', false, 0);
}

function buscaInternaJustificativaFalta() {
	showBuscaInterna('justFaltaId', 'justFaltaDesc', 'cadastros.Justificativas', 'id', 'dscJust', false);
}

function buscaIdJustificativaSuspensao() {
	buscaId('cadastros.Justificativas', 'id', 'dscJust', 'justSuspensaoId', 'justSuspensaoDesc', false, 0);
}

function buscaInternaJustificativaSuspensao() {
	showBuscaInterna('justSuspensaoId', 'justSuspensaoDesc', 'cadastros.Justificativas', 'id', 'dscJust', false);
}

function incluirFalta() {
	incluirRegistro('#justFaltaId', '#justFaltaDesc', '#faltasTable', 'justificativasFaltasId');
}

function incluirSuspensao() {
	incluirRegistro('#justSuspensaoId', '#justSuspensaoDesc', '#suspensaoTable', 'justificativasSuspensaoId');
}

function incluirRegistro(campoId, campoDescricao, tabela, campoHidden) {
	var id = $j(campoId).val();
	var desc = $j(campoDescricao).val();
	var tbody = $j(tabela + ' > tbody');
	var indice = $j(tabela + ' > tbody > tr').length;
	
	var html  = '<tr>';
		html += '	<td>';
		html += desc;	
		html += '	</td>';
		html += '	<td>';
		html += '		<input type="hidden" id="' + campoHidden + '" name="' + campoHidden + '" value="' + id + '"/>';
		html += '		<i class="icon-remove" style="cursor:pointer" title="Excluir registro" onclick="excluirRegistro(' + indice + ', \'' + tabela + '\');"></i>';
		html += '	</td>';
		html += '</tr>';
		
	tbody.append(html);
		
	$j(campoId).val('');
	$j(campoDescricao).val('');
}

function excluirRegistro(indice, tabela) {
	$j(tabela + ' tbody tr:nth-child(' + indice + ')').remove();
}