var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j("#dataInicio").focus();
	 
	 $j('#tipoRelatorio').on('change', function() {
		 if (this.value == '0') {
			 $j('#agrupamento').parent().parent().hide();
		 } else {
			 $j('#agrupamento').parent().parent().show();
		 }
	 });
	 
	 if ($j('#tipoRelatorio').val() == '0') {
		 $j('#agrupamento').parent().parent().hide();
	 } else {
		 $j('#agrupamento').parent().parent().show();
	 }
	 
});

function validateReport() {
	
	if ($j("#dataInicio").val() == null || $j("#dataInicio").val().trim() == '') {
		alert('Informe a data inÃ­cio');
		return false;
	}
	
	if ($j("#dataFim").val() == null || $j("#dataFim").val().trim() == '') {
		alert('Informe a data fim');
		return false;
	}
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	return true;
}

function excluirJustificativa(id) {
	$j('#rowTableJustificativa' + id).remove();
}


function aplicaFiltro() {
	
	
	$j('#tbodyJust').empty();
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/relatorioAfastamentos/aplicarFiltro",
		data: {paramRelat: $j('#filtroSalvo').val() },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var tr = '<tr id="rowTableJustificativa' + item.id + '">';
				    tr +='	<td>';
				    tr +=		item.id;
				    tr +='		<input type="hidden" name="justificativaId" id="justificativaId" value="'+ item.id +'">';
				    tr +='	</td>';
				    tr +='	<td>' + item.descricao + '</td>';
				    tr +='	<td>';
				    tr +='		<i class="icon-remove" onclick="excluirJustificativa(' + item.id + ');" style="cursor: pointer" title="Excluir registro"></i>';
				    tr +='	</td>';
				    tr +='</tr>';
				
				$j('#tbodyJust').append(tr);
			});
		},
		error:function(xhr){
		}
	});
	
}