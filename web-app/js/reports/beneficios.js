var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
});

function mudaTipoBeneficio() {
	
	$j('#tbodyBeneficio').empty();
	$j('#tbodyDatas').empty();
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/beneficio/consultaPorTipoBeneficio",
		data: {tipoDeBeneficio: $j('#tipoBeneficio').val() },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var tr = '<tr>';
				    tr +='	<td><input type="checkbox" name="beneficiosId" id="beneficiosId" value="' + item.id + '" onclick="mudaBeneficio()"/></td>';
				    tr +='	<td>' + item.nome + '</td>';
				    tr +='</tr>';
				$j('#tbodyBeneficio').append(tr);
			});
		},
		error:function(xhr){
		}
	});
}

function mudaBeneficio() {
	$j('#tbodyDatas').empty();
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/beneficio/consultaDatasProcessamento",
		data: {beneficios: $j("input[name=beneficiosId]:checked").map(function () {return this.value;}).get().join(",") },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var tr = '<tr>';
				    tr +='	<td><input type="radio" name="dataProc" id="dataProc" value="' + item.id + '"/></td>';
				    tr +='	<td>' + item.data + '</td>';
				    tr +='</tr>';
				$j('#tbodyDatas').append(tr);
			});
		},
		error:function(xhr){
		}
	});
}

function validateReport() {
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	if ($j('#tipoBeneficio').val() == null || $j('#tipoBeneficio').val().trim() == '') {
		alert('Informe o tipo de benefÃ­cio');
		return false;
	}
	
	if ($j("#dataInicio").val() == null || $j("#dataInicio").val().trim() == '') {
		alert('Informe a data inÃ­cio');
		return false;
	}
	
	if ($j("#dataFim").val() == null || $j("#dataFim").val().trim() == '') {
		alert('Informe a data fim');
		return false;
	}
	
	return true;
}

function excluirOcorrencia(id) {
	$j('#rowTableOcorrencia' + id).remove();
}