var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j.mask.masks.numero = {
		    mask: '99'
	 }
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j("#meses").setMask("numero");
	 
	 $j('#relatorio').on('change', function() {
		 
		 resetForm();
		 
		 if (this.value == '0') {
			 $j('#divPesquisa').show();
			 $j('#divPlanilha').show();
			 inicializaPlanilha();
		 } else if (this.value == '1') {
			 $j('#divPlanilha').show();
			 inicializaPlanilha();
			 $j('#divOcorrencia').show();
		 } else if (this.value == '2') {
			 $j('#divDtInicio').show();
			 $j('#divDtFim').show();
			 $j('#divOcorrencia').show();
		 } else if (this.value == '3') {
			 $j('#divPrimeirosDias').show();
			 $j('#divPlanilha').show();
			 inicializaPlanilha();
		 } else if (this.value == '4') {
			 $j('#divTipoValores').show();
			 $j('#divPlanilha').show();
			 inicializaPlanilha();
		 } else if (this.value == '5') {
			 $j('#divDtInicio').show();
			 $j('#divDtFim').show();
		 } else if (this.value == '6') {
			 $j('#divAgrupamento').show();
		 }
		 
	 });
	 
	 $j('#pesquisa').on('change', function() {
		 
		 if (this.value == '2') {
			 $j('#divValorEmMoeda').show();
		 } else {
			 $j('#divValorEmMoeda').hide();
		 } 
		 
	 });
	 
});

function resetForm() {
	$j('#divPesquisa').hide();
	$j('#divValorEmMoeda').hide();
	$j('#divPlanilha').hide();
	$j('#divOcorrencia').hide();
	$j('#divDtInicio').hide();
	$j('#divDtFim').hide();
	$j('#divPrimeirosDias').hide();
	$j('#divTipoValores').hide();
	$j('#divAgrupamento').hide();
	
	$j('#tbodyPeriodos').empty();

	$j('#pesquisa').val('');
	$j('#valorEmMoeda').val('');
	$j('#meses').val('');
	$j('#dataInicio').val('');
	$j('#dataFim').val('');
	$j('#primeirosDias').val('');
	$j('#tipoValores').val('');
	$j('#agrupamento').val('');
}

function inicializaPlanilha() {
	$j('#meses').val('1');
	mudaMeses();
}

function mudaMeses() {
	$j('#tbodyPeriodos').empty();
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaUltimosMeses",
		data: {meses: $j('#meses').val() },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var tr = '<tr>';
				    tr +='	<td><input type="checkbox" name="periodosId" id="periodosId" value="' + item.id + '" checked="checked"/></td>';
				    tr +='	<td>' + item.nome + '</td>';
				    tr +='</tr>';
				$j('#tbodyPeriodos').append(tr);
			});
		},
		error:function(xhr){
		}
	});	
	
}

function validateReport() {
	
	if ($j("#filtroDesc").val() == null || $j("#filtroDesc").val().trim() == '') {
		alert('Informe o filtro');
		return false;
	}
	
	return true;
}

function excluirOcorrencia(id) {
	$j('#rowTableOcorrencia' + id).remove();
}