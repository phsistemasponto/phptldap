var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#descricao").focus();
})

function atualizaPermissao(item) {
	var input = $j('#rowTablePerfil'+item).children('td').eq(0).children('[id^=permissao]');
	if (input.val()=='false'){
		input.val('true');
	} else {
		input.val('false');
	}
}

function marcarTodos(){
	$j('input[name^="temPerfil"]').attr("checked","true");
	$j('input[name^="permissao"]').val("true");
}
function desmarcarTodos(){
	$j('input[name^="temPerfil"]').removeAttr("checked");
	$j('input[name^="permissao"]').val("false");
	
}
