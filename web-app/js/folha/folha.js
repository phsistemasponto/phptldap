var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dscFol").focus();
	 
})

function incluirEventoFolha(){
	if ($j("#evtFolha").val() == '') {
		alert('Informe a Descrição do Evento!');
	} else if ($j("#ocorrencias").val() == '') {
		alert('Informe a Ocorrência!');
	} else {
		var indice = parseInt($j('#folhaTable > tbody > tr:last > td:first> input[name="sequencialTemp"]').val()) + 1;
		
		var stringAppend = "<tr id=\"rowTableFolha" + indice + "\" class=\"even\">"
			+ "<td>" 
			+ "<input type=\"text\" id=\"nomeOcorrencia\" name=\"nomeOcorrencia\" value=\"" + $j('#ocorrencias option:selected').text() + "\" readonly=\"true\" style=\"width:80%\" />"
			+ "<input type=\"hidden\" id=\"idOcorrencia\" name=\"idOcorrencia\" value=\"" + $j('#ocorrencias').val() +"\"/>"
			+ "<input type=\"hidden\" id=\"sequencialTemp\" name=\"sequencialTemp\" value=\"" + indice +"\"/>"
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"exportaDia\" name=\"exportaDia\" value=\"" + ($j('#idExportaDias').is(':checked') ? 'Sim' : 'Não') + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"exportaHora\" name=\"exportaHora\" value=\"" + ($j('#idExportaHoras').is(':checked') ? 'Sim' : 'Não') + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"exportaHexa\" name=\"exportaHexa\" value=\"" + ($j('#idExportaHexa').is(':checked') ? 'Sim' : 'Não') + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"somaEvento\" name=\"somaEvento\" value=\"" + ($j('#idSomaEvento').is(':checked') ? 'Sim' : 'Não') + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"eventoFolha\" name=\"eventoFolha\" value=\"" + $j('#evtFolha').val() + "\" readOnly=\"true\" style=\"width:80%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"nomeTipoEvento\" name=\"nomeTipoEvento\" value=\"" + $j('#idTpEvtFolha option:selected').text() + "\" readonly=\"true\" style=\"width:80%\" />"
			+ "<input type=\"hidden\" id=\"idTipoEvento\" name=\"idTipoEvento\" value=\"" + $j('#idTpEvtFolha').val() +"\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<i class=\"icon-remove\" onclick=\"excluirEventoFolha("+ indice + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>" 
			+ "</td>"
			
			+ "</tr>";
	
		$j('#folhaTable > tbody:last').append(stringAppend);
		
		resetaCampos();
		
		$j('#ocorrencias').focus();
	}	
	
}

function excluirEventoFolha(indice) {
	$j('#rowTableFolha' + indice).remove();
}

function resetaCampos() {
	$j('#ocorrencias').val('');
	$j('#idExportaDias').attr('checked', false);
	$j('#idExportaHoras').attr('checked', false);
	$j('#idExportaHexa').attr('checked', false);
	$j('#idSomaEvento').attr('checked', false);
	$j('#evtFolha').val('');
	$j('#idTpEvtFolha').val('');
}



/** Filiais */

function marcarTodos(){
	$j('input[name^="temFilial"]').attr("checked","true");
	$j('input[name^="temFilialPeriodo"]').val("true");
}
function desmarcarTodos(){
	$j('input[name^="temFilial"]').removeAttr("checked");
	$j('input[name^="temFilialPeriodo"]').val("false");
	
}

function atualizaFilial(item) {
	var input = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=temFilialPeriodo]');
	if (input.val()=='false'){
		input.val('true');
	} else {
		input.val('false');
	}
}

function atualizaFilialBotao(item) {
	var input = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=temFilialPeriodo]');
	var inputStatus = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=statusPeriodo]');
	if (input.val()=='false'){
		input.val('true');
		inputStatus.val('A');
	} else {
		input.val('false');
		inputStatus.val('X');
	}
}

function fechaReabrePeriodo(item){
	var inputStatus = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=statusPeriodo]');

	if (inputStatus.val()=='A'){
		inputStatus.val('F');
	} else if (inputStatus.val()=='F'){
		inputStatus.val('A');
	}
	
}

function validateFolha() {
	if ($j('#folhaTable > tbody > tr').size() == 1) {
		alert('Cadastre um evento');
		return false;
	}
	
	return true;
}