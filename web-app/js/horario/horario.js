var $j = jQuery.noConflict();



$j(document).ready(function() {
	 $j("#dscHorario").focus();
	 $j("#dtIniVig").setMask('date');
	 $j("#dtIniVig").datepicker({showOn: "button", buttonText: '<i class="icon-calendar"></i>'});	
	 habilitarHorario();	 
})

function popularRetornoLovPerfilHorario() {
	buscaIdPerfilHorario();
}

function buscarLovPerfilHorario() {
	showBuscaInterna('horarioId', 'horarioDescricao', 'cadastros.Perfilhorario', 'id', 'dscperfil', false, 'popularRetornoLovPerfilHorario()');
}

function buscaIdPerfilHorario() {
	var stringBusca = $j('#horarioId').val();

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/busca/buscaIdPerfilHorario",
		data : {
			codigo : stringBusca
		},
		dataType : "json",
		success : function(json) {
			$j('#horarioId').empty();
			$j('#horarioDescricao').empty();
			$j('#horarioCargaHoraria').empty();
			$j('#horarioCargaHorariaHoras').empty();
			
			$j('#horarioId').val(json.id);
			$j('#horarioDescricao').val(json.descricao);
			$j('#horarioCargaHoraria').val(json.cargahor);
			$j('#horarioCargaHorariaHoras').val(json.cargaHorariaHoras);
		},
		error : function(xhr) {
			$j('#horarioId').empty();
			$j('#horarioDescricao').empty();
			$j('#horarioCargaHoraria').empty();
			$j('#horarioCargaHorariaHoras').empty();
		}
	});
}

function incluirItemHorario(){
	if ($j('#dtIniVig').val()==''){
		
		alert('Informe uma data início!');
		abort();
		
	}
	
	var tipoClassificacao
	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/classificacao/buscaTipoClassificacao",
		data : {
			id: $j('#classificacao').val()
		},
		dataType : "json",
		success : function(json) {
			tipoClassificacao = json.tipoClassificacao	
		},
		error : function(xhr) {
			tipoClassificacao = ''
		}
	});

	if (($j("#horarioId").val() == '') && (tipoClassificacao == 'TR')) {
		
		alert('Informe o horário!');
		
	} else {
		
		var quantidade = $j('#itemHorarioTable tr').length-1;
		var ultimo = parseInt(quantidade)+1
		var dtIniVig = $j("#dtIniVig").val();
		var dia = dtIniVig.substring(0,2)
		var mes = dtIniVig.substring(3,5)
		var ano = dtIniVig.substring(6,10)
		
		var soma = parseInt(quantidade) * 86400000;
		var dataInicioVigencia = new Date(parseInt(ano),parseInt(mes)-1,parseInt(dia))
		
		var valorData = dataInicioVigencia.getTime()+soma;
		var novaData = new Date(valorData);
		var diaInteiro= novaData.getDay()+1;
		
		var semana=new Array(6);
		semana[0]='Dom';
		semana[1]='Seg';
		semana[2]='Ter';
		semana[3]='Qua';
		semana[4]='Qui';
		semana[5]='Sex';
		semana[6]='Sáb';
		
		var stringAppend =  "<tr id=\"rowTableItemHorario" + ultimo + "\" class=\"even\" >";
			stringAppend += "	<td> ";
			stringAppend += "		<input name=\"seqhorario\" style=\"width:90%\" value=\""+ultimo+"\" readOnly=\"true\"/> ";
			stringAppend += "	</td> ";
			stringAppend += "	<td> ";
			stringAppend += "		<input type=\"hidden\" name=\"dataOcorrencia\" value=\""+ ("0"+novaData.getDate()).slice(-2)+"/"+("0"+(novaData.getMonth()+1)).slice(-2)+"/"+novaData.getFullYear() +"\" /> ";
			stringAppend += "		<input type=\"hidden\" name=\"nrDiaSemana\" value=\""+diaInteiro+"\" /> ";
			stringAppend += "		<input name=\"diaSemana\" style=\"width:90%\" value=\""+semana[diaInteiro-1]+"\" readOnly=\"true\"/> ";
			stringAppend += "	</td> ";
			stringAppend += "	<td> ";
			stringAppend += "		<input type=\"hidden\" name=\"idPerfilhorario\" value=\""+$j('#horarioId').val()+"\" /> ";
			stringAppend += "		<input name=\"dscPerfilhorario\" style=\"width:90%\" value=\""+$j("#horarioDescricao").val()+"\" readOnly=\"true\"/> ";
			stringAppend += "	</td> ";
			stringAppend += "	<td> ";
			stringAppend += "		<input type=\"hidden\" name=\"idClassificacao\" value=\""+$j("#classificacao").val()+"\" /> ";
			stringAppend += "		<input name=\"dscClassificacao\" style=\"width:90%\" value=\""+$j("#classificacao").find('option').filter(':selected').text()+"\" readOnly=\"true\" /> ";
			stringAppend += "	</td> ";
			stringAppend += "	<td> ";
			stringAppend += "		<input type=\"hidden\" name=\"cargaHoraria\" value=\""+$j('#horarioCargaHoraria').val()+"\" /> ";
			stringAppend += "		<input name=\"cargaHorariaHoras\" style=\"width:90%\" value=\""+$j('#horarioCargaHorariaHoras').val()+"\" readOnly=\"true\" />";
			stringAppend += "	</td> ";
			stringAppend += "	<td> ";
			stringAppend += "		<i class=\"icon-remove\" onclick=\"excluirItemHorario("+ ultimo + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>";
			stringAppend += "	</td> ";
			stringAppend += "</tr>";
	
		$j('#itemHorarioTable > tbody:last').append(stringAppend);

		if (ultimo!='1'){
			var penultimo = parseInt(ultimo)-1
			$j('#rowTableItemHorario' + penultimo).children('td').eq(5).children('i').remove();
		}		
		
		$j('#sequencia').val(ultimo);
		atualizaCargaHorariaTotal('I',$j('#horarioCargaHoraria').val());
	}	
}

function excluirItemHorario(elemento){
	var penultimo = parseInt(elemento)-1
	var cargaHorariaExcluir  = $j('#rowTableItemHorario' + elemento).children('td').eq(4).children('input#cargaHoraria').val()
	
	
	$j('#horarioCargaHoraria').val()
	if (penultimo>0){
		$j('#rowTableItemHorario' + penultimo).children('td').eq(5).append("<i class=\"icon-remove\" onclick=\"excluirItemHorario("+ penultimo + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>");
	}
	$j('#rowTableItemHorario' + elemento).remove()
	$j('#sequencia').val(elemento)
	if (cargaHorariaExcluir==''){cargaHorariaExcluir='0'}
	atualizaCargaHorariaTotal('E', cargaHorariaExcluir);
}

function habilitarHorario(){
	
	$j('#horarioId').val("");
	$j('#horarioDescricao').val("");
	$j('#horarioCargaHoraria').val("");
	$j('#horarioCargaHorariaHoras').val("");
	
	var tipoClassificacao
	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/classificacao/buscaTipoClassificacao",
		data : {
			id: $j('#classificacao').val()
		},
		dataType : "json",
		success : function(json) {
			tipoClassificacao = json.tipoClassificacao	
		},
		error : function(xhr) {
			tipoClassificacao = ''
		}
	});
	
	if (tipoClassificacao!='TR'){
		$j('#horarioId').attr("disabled", "true");
		$j('#itemHorarioBotaoBusca').removeAttr("onclick");
	} else {
		$j('#horarioId').removeAttr("disabled");
		$j('#itemHorarioBotaoBusca').attr("onclick", "buscarLovPerfilHorario();")
	}
	
}

function atualizaCargaHorariaTotal(tipo, qtd){
	var cargaHorariaTotal 
	if ($j('#cargaHorariaTotal').val()==''){
		cargaHorariaTotal = 0
	} else {
		cargaHorariaTotal = parseInt($j('#cargaHorariaTotal').val())
	}
	if (tipo=='E'){
		cargaHorariaTotal = cargaHorariaTotal - parseInt(qtd)
	} else {
		cargaHorariaTotal = cargaHorariaTotal + parseInt(qtd)
	}
	$j('#cargaHorariaTotal').val(cargaHorariaTotal)
	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/horarios/retornaCargaHorariaHoras",
		data : {
			qtdMinutos : cargaHorariaTotal
		},
		dataType : "json",
		success : function(json) {
			$j('#cargaHorariaTotalHoras').empty();
			$j('#cargaHorariaTotalHoras').val(json.cargaHorariaHoras);
		},
		error : function(xhr) {
			$j('#cargaHorariaTotalHoras').empty();		
		}
	});
	
}

function calculaMinutos(horaString) {
	var hora = parseInt(horaString.substring(0, 2));
	var minutos = parseInt(horaString.substring(3, 5));
	var minutosRetorno = (hora * 60) + minutos;
	return minutosRetorno;
}

function visualizaIndice(horario, dataInicio, indice) {
	if ($j(horario).val() == null || $j(horario).val() == '') {
		alert('Informe o horário');
	} else if ($j(dataInicio).val() == null || $j(dataInicio).val() == '') {
		alert('Informe a data início');
	} else if ($j(indice).val() == null || $j(indice).val() == '') {
		alert('Informe o índice');
	} else if (parseInt($j(indice).val()) <= 0) {
		alert('Índice deve ser maior que zero');
	} else {
		
		
		$j.ajax({
			async: false,
			type: "POST",
			url:context+"/itemhorario/listaHorario",
			data: {horarioId: $j(horario).val(), 
				   dataInicioHorario: $j(dataInicio).val(),
				   indiceHorario: $j(indice).val()
				   },
			dataType:"json",
			success:function(json){
				$j('#tableIndiceHorario > tbody').empty();
				$j.each(json, function(index, item) {
					var html =  '<tr>';
						html += '<td>' + item.data + '</td>';
						html += '<td>' + item.dia + '</td>';
						html += '<td>' + item.horario + '</td>';
						html += '<td>' + item.indice + '</td>';
						html += '</tr>';
					$j('#tableIndiceHorario > tbody:last').append(html);
				});
				$j("#divLovListIndiceHorario").dialog({
					 width: 700,
					 height: 500,
					 modal: true,
					 draggable: true
				});
			},
			error:function(xhr){
			}
		});	
		
	}
}