var $j = jQuery.noConflict();

$j(document).ready(function() {
	 
	controlaTipoDeBeneficio();
	controlaOpcaoFiltro();
	
	$j("#dataInicioDesconto").setMask("date");
	$j("#dataFimDesconto").setMask("date");
	$j("#dataInicioCompra").setMask("date");
	$j("#dataFimCompra").setMask("date");
	
	$j("#dataInicioDesconto").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	$j("#dataFimDesconto").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	$j("#dataInicioCompra").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	$j("#dataFimCompra").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	
	$j("#formFiliais").submit(submeteFormAssincrono);
	$j("#formFuncionarios").submit(submeteFormAssincrono);
	
})

function controlaTipoDeBeneficio() {
	var valorBeneficio = $j('#tipoBeneficio').val();
	if (valorBeneficio == '') {
		$j('#divOpcaoFiltro').hide();
		$j("#dataInicioDesconto").val('');
		$j("#dataFimDesconto").val('');
		$j("#dataInicioCompra").val('');
		$j("#dataFimCompra").val('');
	} else {
		$j('#divOpcaoFiltro').show();
	}
}

function controlaOpcaoFiltro() {
	var value = $j('input[name="opcaoFiltro"]:checked').val();
	if (value == 'filial') {
		$j('#divFiltroPadrao').hide();
	} else if (value == 'funcionario') {
		$j('#divFiltroPadrao').show();
	}
}

function submeteFormAssincrono(e) {
	var formObj = $j(this);
	var formURL = formObj.attr("action");
	var formData = new FormData(this);
	
	$j('#carregando').show();
	
	$j.ajax({
		url: formURL,
		type: 'POST',
		data:  formData,
		mimeType:"multipart/form-data",
		contentType: false,
		cache: false,
		processData:false,
		dataType : "json",
		success: function(json) {
			$j('#carregando').hide();
			alert('Rotina invocada com sucesso');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$j('#carregando').hide();
			alert('Erro ao invocar rotina');
		},
		complete: function(jqXHR, textStatus) {
			$j('#carregando').hide();
		}
	});
	e.preventDefault(); //Prevent Default action.
	e.unbind();
}

function marcarTodasFiliais() {
	$j('input[name^="filial"]').attr("checked",true);
}

function desmarcarTodasFiliais() {
	$j('input[name^="filial"]').attr("checked",false);
}

function marcarTodosFuncionarios() {
	$j('input[name^="funcionario"]').attr("checked",true);
}

function desmarcarTodosFuncionarios() {
	$j('input[name^="funcionario"]').attr("checked",false);
}