var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dscSetor").focus();
})

function buscaIdSetor(){
	
	var stringBusca = $j('#setorId').val();
	
	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaId",	
	   data: {classeBusca: 'cadastros.Setor', campoId: 'id', campoDescricao: 'dscSetor', stringBusca: stringBusca, buscaFilial: false, filial: null},
	   dataType:"json",
	   success:function(json){
	      $j('#setorId').empty();
	      $j('#setorDescricao').empty();
		  $j('#setorId').val(json.id);			
		  $j('#setorDescricao').val(json.descricao);
	    }
	  ,
	  error:function(xhr){
	      $j('#setorId').empty();
	      $j('#setorDescricao').empty();
	  }
	});
}

function buscaIdSetorComFilial() {
	
	var stringBusca = $j('#setorId').val();
	var filialId = $j('#filialId').val()

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaIdSetorComFilial",	
	   data: {classeBusca: 'cadastros.Setor', campoId: 'id', campoDescricao: 'dscSetor', stringBusca: stringBusca, buscaFilial: true, filial: filialId},
	   dataType:"json",
	   success:function(json){
	      $j('#setorId').empty();
	      $j('#setorDescricao').empty();
		  $j('#setorId').val(json.id);			
		  $j('#setorDescricao').val(json.descricao);
	    }
	  ,
	  error:function(xhr){
	      $j('#setorId').empty();
	      $j('#setorDescricao').empty();
	  }
	});
}

function buscarLovSetor(){
	$j("#filtraFilialEspecifica").val(true);
	showBuscaInterna('setorId', 'setorDescricao', 'cadastros.Setor', 'id', 'dscSetor', false);
}