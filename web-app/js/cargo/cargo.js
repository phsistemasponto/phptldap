var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dscCargo").focus();
});

function buscaIdCargo(){
	var stringBusca = $j('#cargoId').val();

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaId",	
	   data: {
		   classeBusca: 'cadastros.Cargo', 
		   campoId: 'id', 
		   campoDescricao: 'dscCargo', 
		   stringBusca: stringBusca, 
		   buscaFilial: false, 
		   filial: 0
	   },
	   dataType:"json",
	   success:function(json){
	      $j('#cargoId').empty();
	      $j('#cargoDescricao').empty();
		  $j('#cargoId').val(json.id);			
		  $j('#cargoDescricao').val(json.descricao);						
	    }
	  ,
	  error:function(xhr){
	      $j('#cargoId').empty();
	      $j('#cargoDescricao').empty();
	  }
	});	
}

//refatorar e mudar para setor.js
function buscarLovCargo(){
	showBuscaInterna('cargoId', 'cargoDescricao', 'cadastros.Cargo', 'id', 'dscCargo', false);
}