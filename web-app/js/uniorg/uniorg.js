var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dscUniorg").focus();
});

function buscaIdUniorg(){
	var stringBusca = $j('#subUniorgId').val();

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/busca/buscaId",	
	   data: {
		   classeBusca: 'cadastros.Uniorg', 
		   campoId: 'id', 
		   campoDescricao: 'dscUniorg', 
		   stringBusca: stringBusca, 
		   buscaFilial: false, 
		   filial: 0
	   },
	   dataType:"json",
	   success:function(json){
	      $j('#subUniorgId').empty();
	      $j('#subUniorgDescricao').empty();
		  $j('#subUniorgId').val(json.id);			
		  $j('#subUniorgDescricao').val(json.descricao);						
	    }
	  ,
	  error:function(xhr){
	      $j('#subUniorgId').empty();
	      $j('#subUniorgDescricao').empty();
	  }
	});	
}

//refatorar e mudar para setor.js
function buscarLovUniorg(){
	showBuscaInterna('subUniorgId', 'subUniorgDescricao', 'cadastros.Uniorg', 'id', 'dscUniorg', false);
}