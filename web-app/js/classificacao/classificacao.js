var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dscClass").focus();
})

var txtClassificacao;
var txtDescricao;
var registrosSuprimir;

function buscaIdClassificacao(inputClassificacao, descricaoClassificacao, excludes) {
	
	txtClassificacao = inputClassificacao;
	txtDescricao = descricaoClassificacao;
	registrosSuprimir = excludes;
	
	var stringBusca = $j(inputClassificacao).val();

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/busca/buscaIdClassificacao",
		data : {
			codigo : stringBusca,
			suprimir: registrosSuprimir
		},
		dataType : "json",
		success : function(json) {
			
			$j(inputClassificacao).empty();
			$j(descricaoClassificacao).empty();
			
			$j(inputClassificacao).val(json.id);
			$j(descricaoClassificacao).val(json.nome);
			
		},
		error : function(xhr) {
			
			$j(inputClassificacao).val('');
			$j(descricaoClassificacao).val('');
			
		}
	});
}

function buscarLovClassificacao(inputClassificacao, descricaoClassificacao, excludes) {
	
	txtClassificacao = inputClassificacao;
	txtDescricao = descricaoClassificacao;
	registrosSuprimir = excludes;
	
	if ($j(inputClassificacao).attr('disabled') != 'disabled') {
		showBuscaInterna(inputClassificacao, descricaoClassificacao, 'cadastros.Classificacao', 'id', 'dscClass', false);
	}

}