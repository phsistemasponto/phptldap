var $j = jQuery.noConflict();

$j(document).ready(function() {
	$j('#hora').setMask('time');
})

function incluir() {
	
	if ($j("#diaSemana").val() == '') {
		alert('Deve informar o dia da semana');
		return;
	}
	
	if ($j("#hora").val() == '') {
		alert('Deve informar o horário');
		return;
	}
	
	if ($j("#filiais").val() == null) {
		alert('Deve informar a(s) filial(is)');
		return;
	} 
	
	var indice = $j("#agendamentoTable > tbody > tr").length + 1;
	

	
	var html = '<tr id="rowTableAgendamento-' + indice + '" style="background-color: #00AFFF"> ';
	   html += '	<td style="background-color: #00AFFF"> ';
	   html += '		<input name="diaSemanaDisplay" value="' + $j("#diaSemana option:selected").text() + '" readonly="true" id="diaSemanaDisplay" type="text"/> ';
	   html += '		<input type="hidden" name="diaSemanaId-' + indice + '" id="diaSemanaId-' + indice + '" value="' + $j("#diaSemana").val() + '"/> ';
	   html += '	</td> ';
	   html += '	<td style="background-color: #00AFFF"> ';
	   html += '		<input name="horaExecucaoDisplay" value="' + $j("#hora").val() + '" readonly="true" id="horaExecucaoDisplay" type="text"/> ';
	   html += '		<input type="hidden" name="horaExecucaoId-' + indice + '" id="horaExecucaoId-' + indice + '" value="' + $j("#hora").val() + '"/> ';
	   html += '	</td> ';
	   html += '	<td style="background-color: #00AFFF"> ';
	   html += '		<i class="icon-remove" onclick="excluirAgendamento(' + indice + ');" style="cursor: pointer" title="Excluir registro"></i> ';
	   html += '	</td> ';
	   html += '</tr> ';
	   html += '<tr> ';
	   html += '	<td colspan="3" style="padding:20px"> ';
	   html += '	<table class="table table-striped table-bordered table-condensed"> ';
	   html += '		<thead> ';
	   html += '			<tr> ';
	   html += '				<th>Filial</th> ';
	   html += '			</tr> ';
	   html += '		</thead> ';
	   html += '		<tbody> ';
	   
	   $j.each($j("#filiais").val(), function(index, item) {
		   
	   html += '		<tr> ';   
	   html += '			<td> ';
	   html += 					$j("#filiais option[value=" + item +"]").text();
	   html += '				<input type="hidden" name="filialId-' + indice + '" id="filialId-' + indice + '" value="' + item + '"/> ';
	   html += '			</td> ';
	   html += '		</tr> ';   
		   
	   });
	   
	   html += '		</tbody> ';
	   html += '	</table> ';
	   html += '</tr> ';
	   
	$j('#agendamentoTable > tbody:last').append(html);
	
	
}

function excluirAgendamento(indice) {
	$j('#rowTableAgendamento-' + indice).next().remove();
	$j('#rowTableAgendamento-' + indice).remove();
}