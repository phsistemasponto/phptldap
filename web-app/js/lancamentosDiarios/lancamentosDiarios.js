var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	$j("#dataInicio").setMask("date"); 
	$j("#dataFim").setMask("date");
	
	$j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	$j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});

	$j(".celulaLancamentoDiario").click(clickCelulaLancamentoDiario);
	$j(".celulaLancamentoDiarioGeral").click(clickCelulaLancamentoDiarioGeral);
	
	drawCanvasClassificacoesLancamentosDiarios();

});

// Lancamentos diarios

function buscaIdClassificacaoLancamentosDiarios() {
	buscaIdClassificacao('#classificacaoId', '#classificacaoDescricao', '0');
	verificaHabilitacaoItemHorario();
}

function showModalClassificacaoLancamentosDiarios() {
	showBuscaInterna('classificacaoId', 'classificacaoDescricao', 'cadastros.Classificacao', 'id', 'dscClass', false, 'verificaHabilitacaoItemHorario()');
}

function buscaIdPerfilhorarioLancamentosDiarios() {
	buscaId('cadastros.Perfilhorario', 'id', 'dscperfil', 'itemHorarioId', 'itemHorarioDescricao', false);
}

function showModalPerfilhorarioLancamentosDiarios() {
	var habilita = $j('#classificacaoId').val().trim() != '' && $j('#classificacaoId').val() == '1';
	
	if (habilita) {
		showBuscaInterna('itemHorarioId', 'itemHorarioDescricao', 'cadastros.Perfilhorario', 'id', 'dscperfil', false);
	}
}

function buscaCodigoClassificacao() {
	
	var classificacaoId = $j('#classificacaoId').val().trim();
	if (classificacaoId == '') {
		return '';
	}
	
	var retorno = '';

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/classificacao/buscaTipoClassificacao",
		data : {
			id: classificacaoId
		},
		dataType : "json",
		success : function(json) {
			
			retorno = json.tipoClassificacao;
			
		},
		error : function(xhr) {
			
			
			
		}
	});
	
	return retorno;
	
}

function verificaHabilitacaoItemHorario() {
	var codigoClassificacao = buscaCodigoClassificacao();
	var habilita = codigoClassificacao == 'TR';
	
	if (habilita) {
		$j('#itemHorarioId').removeAttr("disabled");
		$j('#itemHorarioBotaoBusca').removeAttr("disabled");
		$j('#itemHorarioBotaoBusca').attr('onclick', 'showModalPerfilhorarioLancamentosDiarios();');
	} else {
		$j('#itemHorarioId').attr('disabled', 'disabled');
		$j('#itemHorarioBotaoBusca').attr('disabled', 'disabled');
		$j('#itemHorarioBotaoBusca').attr('onclick', '');
	}

    $j('#itemHorarioId').val('');
    $j('#itemHorarioDescricao').val('');
}

function drawCanvasClassificacoesLancamentosDiarios() {
	$j('canvas').each(
			function() {
				if ($j(this).attr('id').lastIndexOf('canvasClassificacao', 0) == 0
						|| $j(this).attr('id').lastIndexOf('canvasCelulaLancamento', 0) == 0) {
					
					var canvas = document.getElementById($j(this).attr('id'));
					var codClassificacao = resolveCodigoClassificacao($j(this));
					if (codClassificacao != '') {
						var corClassificacao = $j('#corClassificacao-'+codClassificacao).val();
						var context = canvas.getContext('2d');
					    var centerX = canvas.width / 2;
					    var centerY = canvas.height / 2;
					    var radius = 10;
					    
					    context.beginPath();
					    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
					    context.fillStyle = '#'+corClassificacao;
					    context.fill();
					    context.lineWidth = 1;
					    context.strokeStyle = '#000000';
					    context.stroke();
					}
				}
			}
	);
}

function resolveCodigoClassificacao(obj) {
	if (obj.attr('id').lastIndexOf('canvasClassificacao', 0) == 0) {
		return obj.attr('id').split('-')[1];
	} else {
		var funcionario = obj.attr('id').split('-')[1];
		var data = obj.attr('id').split('-')[2];
		return $j('#classificacaoCelulaLancamento-'+funcionario+'-'+data).val();
	}
}

function clickCelulaLancamentoDiario() {
	var celula = $j(this);
	var classificacaoSelecionada = $j('#classificacaoId').val();
	var itemHorarioSelecionado = $j('#itemHorarioId').val();
	
	var corClassificacao = $j('#corClassificacao-'+classificacaoSelecionada).val();
	var canvas = document.getElementById($j(this).children()[0].id);
	var context = canvas.getContext('2d');
	var inputClassificacao = $j(this).children()[1];
	var inputItemHorario = $j(this).children()[2];
	
	if (inputClassificacao.value == '' && corClassificacao != null && corClassificacao != '') {
	    var centerX = canvas.width / 2;
	    var centerY = canvas.height / 2;
	    var radius = 10;
	    
	    context.beginPath();
	    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
	    context.fillStyle = '#'+corClassificacao;
	    context.fill();
	    context.lineWidth = 1;
	    context.strokeStyle = '#000000';
	    context.stroke();
	    
	    inputClassificacao.value = classificacaoSelecionada;
	    inputItemHorario.value = itemHorarioSelecionado;
	    
	} else {
		context.beginPath();
		context.clearRect(0, 0, canvas.width, canvas.height);
		
		inputClassificacao.value = '';
		inputItemHorario.value = '';
	}
	
}

function clickCelulaLancamentoDiarioGeral() {
	var celula = $j(this);
	var identificadorDia = celula.attr('id').split('-')[1];
	var hiddenControle = $j('#hiddenLancamentoDiarioGeral-'+identificadorDia);
	var controle = hiddenControle.val();
	var adicionou = false;
	
	var classificacaoSelecionada = $j('#classificacaoId').val();
	var itemHorarioSelecionado = $j('#itemHorarioId').val();
	var corClassificacao = $j('#corClassificacao-'+classificacaoSelecionada).val();
	
	$j('.celulaLancamentoDiario').each(function() {
		
		if ($j(this).attr('id').endsWith(identificadorDia)) {
			
			var canvas = document.getElementById($j(this).children()[0].id);
			var context = canvas.getContext('2d');
			var inputClassificacao = $j(this).children()[1];
			var inputItemHorario = $j(this).children()[2];
			
			if (controle == '' && classificacaoSelecionada != '') {
				
				var centerX = canvas.width / 2;
			    var centerY = canvas.height / 2;
			    var radius = 10;
			    
			    context.beginPath();
			    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
			    context.fillStyle = '#'+corClassificacao;
			    context.fill();
			    context.lineWidth = 1;
			    context.strokeStyle = '#000000';
			    context.stroke();
			    
			    inputClassificacao.value = classificacaoSelecionada;
			    inputItemHorario.value = itemHorarioSelecionado;
			    adicionou = true;
			    
				
			} else {
				
				context.beginPath();
				context.clearRect(0, 0, canvas.width, canvas.height);
				
				inputClassificacao.value = '';
				inputItemHorario.value = '';
				adicionou = false;
				
			}
			
		}
	});
	
	if (adicionou) {
		hiddenControle.val(classificacaoSelecionada);
	} else {
		hiddenControle.val('');
	}
	
	
}