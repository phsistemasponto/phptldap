var $j = jQuery.noConflict();

$j(document).ready(function() {
	mudaPeriodo();
	
	$j("#formPesquisa").submit(function(e) {
		
		if ($j('#filial').val() == null || $j('#filial').val() == '') {
			alert('Selecione uma filial');
			return false;
		}
		
		if ($j('#periodo').val() == null || $j('#periodo').val() == '') {
			alert('Selecione um período');
			return false;
		}
		
		return true;
		
	});
});

function mudaFilial() {
	var filial = $j('#filial').val();
	
	if (filial != '') {
		$j.ajax({
			async: false,
			type: "POST",
			url:context+"/criticasExtras/buscaPeriodos",
			data: {filial: filial},
			dataType:"json",
			success:function(json){
				$j('#periodo').html('')
				$j('#periodo').append('<option value="">..</option>');
				
				$j.each(json, function(index, item){
					var opt = '<option value="' + item.id + '">' + item.dscPr + '</option>';
					$j('#periodo').append(opt);
				});
			},
			error:function(xhr){}
		});
	} else {
		$j('#periodo').html('')
		$j('#periodo').append('<option value="">..</option>');
	}
}

function mudaAno() {
	$j('#periodo').empty();
	$j('#periodo').append('<option value="">..</option>');
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaPorAno",
		data: {ano: $j('#ano').val(), filial: $j('#filial').val(), status: 'A' },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var option = '<option value="' + item.id + '">' + item.descricao + '</option>';
				$j('#periodo').append(option);
			});
		},
		error:function(xhr){
		}
	});	
	
}

function mudaPeriodo() {
	
	$j('#dataInicio').val('');
	$j('#dataFim').val('');
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaDatas",
		data: {periodoId: $j('#periodo').val() },
		dataType:"json",
		success:function(json){
			$j('#dataInicio').val(json.dataInicio);
			$j('#dataFim').val(json.dataFim);
		},
		error:function(xhr){
		}
	});	
}