var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	$j.mask.masks.numero = {
		mask: '9999'
	}
	
	$j('#qtFaltas').setMask('numero');
	$j('#qtExtras').setMask('time');
	 
});

function buscaJustificativaId() {
	buscaId('cadastros.Justificativas', 'id', 'dscJust', 'justificativaId', 'justificativaNome', false);
}

function buscaJustificativaLov() {
	showBuscaInterna('justificativaId', 'justificativaNome', 'cadastros.Justificativas', 'id', 'dscJust', false);
}

function incluirJustificativaAbono() {
	
	var justificativaId = $j('#justificativaId').val();
	var justificativaNome = $j('#justificativaNome').val();
	var quantidade = $j('#quantidade').val();
	
	if (justificativaId == null || justificativaId == '' || justificativaNome == null || justificativaNome == '') {
		alert('Informe a justificativa');
		return;
	}
	
	if (quantidade == null || quantidade == '') {
		alert('Informe a quantidade');
		return;
	}
	
	var indice = 0;
	if ($j('#justificativasTable tbody tr:last').size() > 0) {
		indice = parseInt($j('#justificativasTable tbody tr:last').attr('id').split('-')[1]) + 1;
	}
	
	var html  = '<tr id="rowTableItemJustificativa-' + indice + '">';
		html += '	<td>' + justificativaNome + '</td>';
		html += '	<td>' + quantidade + '</td>';
		html += '	<td>';
		html += '		<input type="hidden" name="idJustAbono" id="idJustAbono" value="' + justificativaId + '"/>';
		html += '		<input type="hidden" name="qtdadeJustAbono" id="qtdadeJustAbono" value="' + quantidade + '"/>';
		html += '		<i class="icon-remove" onclick="excluirJustificativa(' + indice + ');" style="cursor: pointer" title="Excluir registro"></i>';
		html += '	</td>';
		html += '</tr>';
		
	$j('#justificativasTable tbody').append(html);
	
	$j('#justificativaId').val('');
	$j('#justificativaNome').val('');
	$j('#quantidade').val('');
}

function excluirJustificativa(indice) {
	$j('#rowTableItemJustificativa-' + indice).remove();
}

function buscaOcorrenciaId() {
	buscaId('cadastros.Ocorrencias', 'id', 'dscOcor', 'ocorrenciaId', 'ocorrenciaNome', false);
}

function buscaOcorrenciaLov() {
	showBuscaInterna('ocorrenciaId', 'ocorrenciaNome', 'cadastros.Ocorrencias', 'id', 'dscOcor', false);
}

function incluirOcorrencia() {
	
	var ocorrenciaId = $j('#ocorrenciaId').val();
	var ocorrenciaNome = $j('#ocorrenciaNome').val();
	var quantidade = $j('#quantidadeOcorrencia').val();
	
	if (ocorrenciaId == null || ocorrenciaId == '' || ocorrenciaNome == null || ocorrenciaNome == '') {
		alert('Informe a ocorrência');
		return;
	}
	
	if (quantidade == null || quantidade == '') {
		alert('Informe a quantidade');
		return;
	}
	
	var indice = 0;
	if ($j('#ocorrenciasTable tbody tr:last').size() > 0) {
		indice = parseInt($j('#ocorrenciasTable tbody tr:last').attr('id').split('-')[1]) + 1;
	}
	
	var html  = '<tr id="rowTableItemOcorrencia-' + indice + '">';
		html += '	<td>' + ocorrenciaNome + '</td>';
		html += '	<td>' + quantidade + '</td>';
		html += '	<td>';
		html += '		<input type="hidden" name="idOcorrencia" id="idOcorrencia" value="' + ocorrenciaId + '"/>';
		html += '		<input type="hidden" name="qtdadeOcorrencia" id="qtdadeOcorrencia" value="' + quantidade + '"/>';
		html += '		<i class="icon-remove" onclick="excluirOcorrencia(' + indice + ');" style="cursor: pointer" title="Excluir registro"></i>';
		html += '	</td>';
		html += '</tr>';
		
	$j('#ocorrenciasTable tbody').append(html);
	
	$j('#ocorrenciaId').val('');
	$j('#ocorrenciaNome').val('');
	$j('#quantidadeOcorrencia').val('');
}

function excluirOcorrencia(indice) {
	$j('#rowTableItemOcorrencia-' + indice).remove();
}

function marcarTodos() {
	$j('input[name^="filial"]').attr("checked",true);
}

function desmarcarTodos() {
	$j('input[name^="filial"]').attr("checked",false);
}

