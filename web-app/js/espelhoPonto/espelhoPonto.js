var $j = jQuery.noConflict();
var innerFormSubmit = '';

$j(document).ready(function() {
	 	
	 innerFormSubmit = $j('#formResubmit').html();
	 $j('#formResubmit').html('');
	
	 $j.mask.masks.numero = {
		    mask: '9999999'
	 }
	 
	 $j("#dataInicio").focus();
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
	 $j("#dataInicioAfastamento").setMask("date"); 
	 $j("#dataFimAfastamento").setMask("date");
	 $j("#dataInicioAbono").setMask("date");
	 $j('#cargaHorariaInicioAbono').setMask('time');
	 
	 $j("#quantidadeDiasAbono").setMask("numero");
 
	 $j('input[id*="data"][type="text"]').datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j(".linhaFuncionario").mouseenter(function() {
		$j(this).css({'cursor': 'pointer'});
	 });
	 
	 $j(".linhaFuncionario").click(exibirFuncionario);
	 
	 $j(".linhaDado").mouseenter(function() {
		$j(this).css({'cursor': 'pointer'});
	 });
	 
	 $j(".linhaDado").click(exibirOcorrenciasEBatidas);
	 
	 $j('input[name^="checkAbono"]').click(clickCheckAbono);
	 
	 $j(".celulaLancamentoDiario").click(clickCelulaLancamentoDiario);
	 
	 $j(".btnAlterarStatusBatidas").click(clickAlterarStatusBatidas);
	 $j(".btnIncluirBatidas").click(clickIncluirBatidas);
	 $j(".checkExcluirBatidas").click(clickExcluirBatidas);
	 
	 $j("#formProcessar").submit(function(e) {
			var formObj = $j(this);
			var formURL = formObj.attr("action");
			var formData = new FormData(this);
			
			$j('#carregando').show();
			
			$j.ajax({
				url: formURL,
				type: 'POST',
				data:  formData,
				mimeType:"multipart/form-data",
				contentType: false,
				cache: false,
				processData:false,
				dataType : "json",
				success: function(json) {
					$j('#carregando').hide();
					
					$j('#formResubmit').html(innerFormSubmit);
					$j('#formResubmit').submit();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$j('#carregando').hide();
					alert('Erro ao invocar rotina');
				},
				complete: function(jqXHR, textStatus) {
					$j('#carregando').hide();
				}
			});
			e.preventDefault(); //Prevent Default action.
			e.unbind();
	});
	 
	 drawCanvasClassificacoesLancamentosDiarios();
	
});


function exibirFuncionario() {
	var funcId = $j(this).attr('id').split('-')[1];
	$j("#funcionarioId").val(funcId);
	$j("#dtInicio").val($j("#dataInicio").val());
	$j("#dtFim").val($j("#dataFim").val());
	$j('#formPesquisa input').each(
    		function() {
    			if ($j(this).val() != null && $j(this).val() != '' && $j(this).attr('id') != 'btnFiltro') {
    				var value = $j(this).val();
    				if ($j(this).attr('type') == 'checkbox' && !$j(this).is(':checked')) {
    					value = '';
    				}
    	    		var param = '<input type="hidden" name="formPesquisa_' + $j(this).attr('id') 
    	    			+ '" id="formPesquisa_' + $j(this).attr('id') + '" value="' + value + '"/>';
    	    		$j('#formFuncionarios').append(param);
    	    	}
	    		
	    	}
    );
	
	$j('#formFuncionarios').submit();
}

function clickLinhaDado(event) {
	switch (event.which) {
	    case 1:
	    	exibirOcorrenciasEBatidas();
	        break;
	    case 2:
	        break;
	    case 3:
	        alert('Right Mouse button pressed.');
	        break;
	    default:
	        alert('You have a strange Mouse!');
	}
}

function exibirOcorrenciasEBatidas() {
	var dadoId = $j(this).attr('id').split('-')[1];
	
	var data = $j(this).children('.linhaDadoDataBatida').text().trim();
	
	colorirCorrigirBatidas(data);

	$j(this).css({'background-color': '#d9edf7'});

	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/espelhoPonto/exibirOcorrencias",
		data: { dadoId: dadoId },
		dataType:"json",
		success:function(json){
    		$j("#tabelaOcorrencias tbody").empty();
    		var rowTable;
    		$j.each(json, function(index, item){
        		rowTable  = '<tr>';
        		rowTable += '	<td style="padding: 4px; border: 1px solid #eee;">';
        		rowTable += item.ocorrencia ;
        		rowTable += '	</td>';
        		rowTable += '	<td style="padding: 4px; border: 1px solid #eee;">';
        		rowTable += item.horas ;
        		rowTable += '	</td>';
        		rowTable += '</tr>';
            	$j("#tabelaOcorrencias tbody").append(rowTable);
        	});
        },
        error:function(xhr){
            $j("#tabelaOcorrencias tbody").empty();
        }
    });

	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/espelhoPonto/exibirBatidas",
		data: { dadoId: dadoId },
		dataType:"json",
		success:function(json){
    		$j("#tabelaBatidas tbody").empty();
    		
    		var rowTable;
    		
    		$j.each(json, function(index, item){
    			
    			var style = '';
        		if (item.operacao == 'INC') {
        			style = 'background-color: green';
        		} else if (item.operacao == 'REM') {
        			style = 'background-color: red';
        		}
        		
        		var styleButtonLock = '';
        		if (item.bloqueado == 'S') {
        			styleButtonLock = 'icon-ban-circle';
        		} else {
        			styleButtonLock = 'icon-lock';
        		}
        		
        		var actionButtonLock = '';
        		if (item.bloqueado == 'S') {
        			actionButtonLock = 'javascript:desbloquearBatida(' +  item.id + ');';
        		} else {
        			actionButtonLock = 'javascript:bloquearBatida(' +  item.id + ');';
        		}
        		
        		var titleButton = '';
        		if (item.bloqueado == 'S') {
        			titleButton = 'Desbloquear batida';
        		} else {
        			titleButton = 'Bloquear batida';
        		}
    			
        		rowTable  = '<tr style="'+ style + '">';
        		rowTable += '	<td style="padding: 4px; border: 1px solid #DDDDDD;">';
        		rowTable += item.status ;
        		rowTable += '	</td>';
        		rowTable += '	<td style="padding: 4px; border: 1px solid #DDDDDD;">';
        		rowTable += item.horas ;
        		rowTable += '	</td>';
        		rowTable += '	<td style="padding: 4px; border: 1px solid #DDDDDD;">';
        		rowTable += '		<button class="btn" style="height: 25px;margin-left: 10px;" onclick="' + actionButtonLock + '" title="' + titleButton + '"> ';
        		rowTable += '			<i class="' + styleButtonLock + '"></i> ';
        		rowTable += '		</button> ';
        		rowTable += '	</td>';
        		rowTable += '</tr>';
            	$j("#tabelaBatidas tbody").append(rowTable);
            	
            	
        	});
        },
        error:function(xhr){
            $j("#tabelaBatidas tbody").empty();
        }
        
    });
	
	var heightTable = $j("#tabelaOcorrencias").parent().parent().parent().height();
	var heightCelula = parseInt(heightTable/2)-2; 
	
	$j("#tabelaOcorrencias").parent().css({'padding-left': '20px'});
	$j("#tabelaOcorrencias").parent().css({'vertical-align': 'top'});
	$j("#tabelaOcorrencias").parent().css({'height': '20px'});
	
	$j("#tabelaBatidas").parent().css({'padding-left': '20px'});
	$j("#tabelaBatidas").parent().css({'vertical-align': 'top'});
}

function bloquearBatida(idBatida) {
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/espelhoPonto/bloquearBatida",
		data: { batida: idBatida },
		dataType:"json",
		success:function(json){
			alert('Batida bloqueada com sucesso!');
        },
        error:function(xhr){
            alert('Erro ao bloquar batida');
        }
        
    });
	
}

function desbloquearBatida(idBatida) {
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/espelhoPonto/desbloquearBatida",
		data: { batida: idBatida },
		dataType:"json",
		success:function(json){
			alert('Batida desbloqueada com sucesso!');
        },
        error:function(xhr){
            alert('Erro ao desbloquar batida');
        }
        
    });
	
}

// Lov de Justificativa para espelho de ponto
function buscaIdJustificativaEspelhoPonto(campo, descricao, tipoJustificativa) {
	
	var stringBusca = $j(campo).val();

	$j.ajax({
	   async: false,
	   type: "POST",
	   url:context+"/justificativas/buscaIdJustificativaEspelhoPonto",	
	   data: {
		   classeBusca: 'cadastros.Justificativas', 
		   campoId: 'id', 
		   campoDescricao: 'dscJust', 
		   stringBusca: stringBusca,
		   codigo: stringBusca,
		   tipoJustificativa: tipoJustificativa
	   },
	   dataType:"json",
	   success:function(json){
	      $j(campo).empty();
	      $j(descricao).empty();
		  
	      $j(campo).val(json.id);			
		  $j(descricao).val(json.nome);
		  
		  verificarHabilitacaoCID(json.exigeCid);
	    }
	  ,
	  error:function(xhr){
		  $j(campo).empty();
	      $j(descricao).empty();
	  }
	});	
}

function buscarLovJustificativaEspelhoPonto(tipoJustificativa) {
	if (tipoJustificativa == 'AB') {
		showBuscaInterna('justificativaAbonoId', 'justificativaAbonoDescricao', 'cadastros.Justificativas', 'id', 'dscJust', false, 'verificaHabilitacaoCIDAbono()');
	} else if (tipoJustificativa == 'AF') {
		showBuscaInterna('justificativaAfastamentoId', 'justificativaAfastamentoDescricao', 'cadastros.Justificativas', 'id', 'dscJust', false);
	}
	$j("#tipoJustificativaEspelhoPonto").val(tipoJustificativa);
}

// Abono
function showLovAbono(data) {
	if ($j('#periodoFechado').val() === 'false') {
		$j("#divLovAbono").dialog({
			 width: 700,
			 height: 500,
			 modal: true,
			 draggable: true
		});
		
		if (data != null && data != '') {
			$j('#divPanelAbono').show();
			$j('#dataInicioAbono').val(data);
			buscaMovimentoDia();
		} else {
			$j('#divPanelAbono').hide();
		}
		
	} else {
		alert('Período fechado.');
	}
}

function buscaMovimentoDia() {
	
	$j('#cargaHorariaInicioAbono').val('');
    $j('#ocorrenciaInicioAbono').val('');
	
	$j.ajax({
		   async: false,
		   type: "POST",
		   url:context+"/espelhoPonto/exibirMovimentoOcorrencia",	
		   data: { dia: $j('#dataInicioAbono').val(), funcionario: $j('#funcionarioId').val() },
		   dataType:"json",
		   success:function(json){
			  $j('#cargaHorariaInicioAbono').val('');
			  $j('#ocorrenciaInicioAbono').val('');
			  
		      $j('#cargaHorariaInicioAbono').val(json.cargaHoraria);
		      $j('#ocorrenciaInicioAbono').val(json.ocorrencia);
		    }
		  ,
		  error:function(xhr){
			  $j('#cargaHorariaInicioAbono').val('');
			    $j('#ocorrenciaInicioAbono').val('');
		  }
		});	
	
}

function marcaTodosAbono() {
	$j('input[name^="checkAbono"]').attr("checked","true");
	$j('input[name^="checkAbono"]').each(function() {
		var check = $j(this);
		var colunaInput = check.parent().parent().children('.colunaHorasAbonadas');
		var movimento = $j(this).val();
		var txt = $j(colunaInput).prev().text(); 
		var input = '<input type="text" style="width:40px;" id="horasAbonadas-' + movimento + '" name="horasAbonadas-' + movimento + '" value="' + txt + '"/>';
		colunaInput.append(input);
		$j(colunaInput).children('input').setMask('time');
	});
}

function desmarcaTodosAbono() {
	$j('input[name^="checkAbono"]').removeAttr("checked");
	$j('input[name^="checkAbono"]').each(function() {
		var check = $j(this);
		var colunaInput = check.parent().parent().children('.colunaHorasAbonadas');
		$j(colunaInput).children('input').remove();
	});
}

function clickCheckAbono() {
	var check = $j(this);
	var colunaInput = check.parent().parent().children('.colunaHorasAbonadas');
	if ($j(this).attr('checked') == 'checked') {
		var movimento = $j(this).val();
		var txt = $j(colunaInput).prev().text(); 
		var input = '<input type="text" style="width:40px;" id="horasAbonadas-' + movimento + '" name="horasAbonadas-' + movimento + '" value="' + txt + '" style="font-weight: bold;"/>';
		colunaInput.append(input);
		$j(colunaInput).children('input').setMask('time');
		$j(colunaInput).children('input').on('input', function() {
			var valor = $j(this).val();
			var maximo = $j(this).parent().prev().text();
			var valorInt = calculaMinutos(valor);
			var maximoInt = calculaMinutos(maximo);
			
			if (valorInt > maximoInt) {
				alert('Quantidade de horas deve ser menor que o máximo');
				$j(this).val(maximo);
			}
		});
	} else {
		$j(colunaInput).children('input').remove();
	}
}

function verificarHabilitacaoCID(habilitaCID) {
	habilitaCampoCID(habilitaCID == 'S');
}

function verificaHabilitacaoCIDAbono() {
	var habilitaCID = null;
	
	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/justificativas/buscaIdJustificativaEspelhoPonto",
		data : {
			codigo : $j('#justificativaAbonoId').val(),
			tipoJustificativa: 'AB'
		},
		dataType : "json",
		success : function(json) {
			habilitaCID = json.exigeCid;
		}, 
		error : function(xhr) { }
	});
	
	habilitaCampoCID(habilitaCID == 'S');
}

function buscaIdCidAbono() {
	buscaId('cadastros.Cid', 'id', 'dscCid', 'cidId', 'cidDescricao', false);
}

function showModalCidAbono() {
	if ($j('#cidId').attr('disabled') == null) {
		showBuscaInterna('cidId', 'cidDescricao', 'cadastros.Cid', 'id', 'dscCid', false);
	}
}

function habilitaCampoCID(habilita) {
	if (habilita) {
		$j('#cidId').removeAttr('disabled');
		$j('#divCidAbono').show();
	} else {
		$j('#cidId').attr('disabled', !habilita);
		$j('#divCidAbono').hide();
	}
	$j('#cidId').val('');
	$j('#cidDescricao').val('');
}

// Mudança horario
function showLovMudancaHorario() {
	if ($j('#periodoFechado').val() === 'false') {
		$j("#divLovMudancaHorario").dialog({
			 width: 600,
			 height: 250,
			 modal: true,
			 draggable: true
		});
	} else {
		alert('Período fechado.');
	}
}

// Casdastro principal

function redirectToCadastroPrincipal() {
	$j('.formCadastroPrincipal').submit();
}

//Afastamentos
function showLovAfastamentos() {
	if ($j('#periodoFechado').val() === 'false') {
		$j("#divLovAfastamentos").dialog({
			 width: 600,
			 height: 550,
			 modal: true,
			 draggable: true
		});
	} else {
		alert('Período fechado.');
	}
}

function excluirAfastamento(msg, url, valor) {
	if (confirm(msg))
		$j.form(url, {id: valor, funcionarioId: $j('#funcionarioId').val(), dtInicio: $j('#dtInicio').val(), dtFim: $j('#dtFim').val()}).submit();
}

// Horas extras
function showLovHorasExtras() {
	$j("#divLovHorasExtras").dialog({
		 width: 600,
		 height: 550,
		 modal: true,
		 draggable: true
	});
}

// Lancamentos diarios
function showLovLancamentosDiarios() {
	if ($j('#periodoFechado').val() === 'false') {
		$j("#divLovLancamentosDiarios").dialog({
			 width: 800,
			 height: 600,
			 modal: true,
			 draggable: true
		});
	} else {
		alert('Período fechado.');
	}
	
}

function buscaIdClassificacaoLancamentosDiarios() {
	buscaIdClassificacao('#classificacaoId', '#classificacaoDescricao', '0');
	verificaHabilitacaoItemHorario();
}

function showModalClassificacaoLancamentosDiarios() {
	showBuscaInterna('classificacaoId', 'classificacaoDescricao', 'cadastros.Classificacao', 'id', 'dscClass', false, 'verificaHabilitacaoItemHorario()');
}

function buscaIdPerfilhorarioLancamentosDiarios() {
	buscaId('cadastros.Perfilhorario', 'id', 'dscperfil', 'itemHorarioId', 'itemHorarioDescricao', false);
}

function showModalItemHorarioLancamentosDiarios() {
	var habilita = $j('#classificacaoId').val().trim() != '' && $j('#classificacaoId').val() == '1';
	
	if (habilita) {
		showBuscaInterna('itemHorarioId', 'itemHorarioDescricao', 'cadastros.Perfilhorario', 'id', 'dscperfil', false);
	}
}

function buscaCodigoClassificacao() {
	
	var classificacaoId = $j('#classificacaoId').val().trim();
	if (classificacaoId == '') {
		return '';
	}
	
	var retorno = '';

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/classificacao/buscaTipoClassificacao",
		data : {
			id: classificacaoId
		},
		dataType : "json",
		success : function(json) {
			
			retorno = json.tipoClassificacao;
			
		},
		error : function(xhr) {
			
			
			
		}
	});
	
	return retorno;
	
}

function verificaHabilitacaoItemHorario() {
	var codigoClassificacao = buscaCodigoClassificacao();
	var habilita = codigoClassificacao == 'TR';
	
	if (habilita) {
		$j('#divItemHorario').show();
		$j('#itemHorarioBotaoBusca').removeAttr("disabled");
		$j('#itemHorarioBotaoBusca').attr("onclick", "showModalItemHorarioLancamentosDiarios();");
	} else {
		$j('#divItemHorario').hide();
		$j('#itemHorarioBotaoBusca').removeAttr("disabled", "disabled");
		$j('#itemHorarioBotaoBusca').attr("onclick", "");
	}

    $j('#itemHorarioId').val('');
    $j('#itemHorarioDescricao').val('');
}

function drawCanvasClassificacoesLancamentosDiarios() {
	$j('canvas').each(
			function() {
				if ($j(this).attr('id').lastIndexOf('canvasClassificacao', 0) == 0
						|| $j(this).attr('id').lastIndexOf('canvasCelulaLancamento', 0) == 0) {
					
					var canvas = document.getElementById($j(this).attr('id'));
					var codClassificacao = resolveCodigoClassificacao($j(this));
					if (codClassificacao != '') {
						var corClassificacao = $j('#corClassificacao-'+codClassificacao).val();
						var context = canvas.getContext('2d');
					    var centerX = canvas.width / 2;
					    var centerY = canvas.height / 2;
					    var radius = 10;
					    
					    context.beginPath();
					    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
					    context.fillStyle = '#'+corClassificacao;
					    context.fill();
					    context.lineWidth = 1;
					    context.strokeStyle = '#000000';
					    context.stroke();
					}
				}
			}
	);
}

function resolveCodigoClassificacao(obj) {
	if (obj.attr('id').lastIndexOf('canvasClassificacao', 0) == 0) {
		return obj.attr('id').split('-')[1];
	} else {
		var data = obj.attr('id').split('-')[1];
		return $j('#classificacaoCelulaLancamento-'+data).val();
	}
}

function clickCelulaLancamentoDiario() {
	var celula = $j(this);
	var classificacaoSelecionada = $j('#classificacaoId').val();
	var itemHorarioSelecionado = $j('#itemHorarioId').val();
	
	var corClassificacao = $j('#corClassificacao-'+classificacaoSelecionada).val();
	var canvas = document.getElementById($j(this).children()[0].id);
	var context = canvas.getContext('2d');
	var inputClassificacao = $j(this).children()[1];
	var inputItemHorario = $j(this).children()[2];
	
	if (inputClassificacao.value == '' && corClassificacao != null && corClassificacao != '') {
	    var centerX = canvas.width / 2;
	    var centerY = canvas.height / 2;
	    var radius = 10;
	    
	    context.beginPath();
	    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
	    context.fillStyle = '#'+corClassificacao;
	    context.fill();
	    context.lineWidth = 1;
	    context.strokeStyle = '#000000';
	    context.stroke();
	    
	    inputClassificacao.value = classificacaoSelecionada;
	    inputItemHorario.value = itemHorarioSelecionado;
	    
	} else {
		context.beginPath();
		context.clearRect(0, 0, canvas.width, canvas.height);
		
		inputClassificacao.value = '';
		inputItemHorario.value = '';
	}
	
}

//Banco horas
function showLovBancoHoras() {
	$j("#divLovBancoHoras").dialog({
		 width: 800,
		 height: 550,
		 modal: true,
		 draggable: true
	});
}

// Corrigir batidas

function showLovCorrigirBatidas() {
	if ($j('#periodoFechado').val() === 'false') {
		$j("#divLovCorrigirBatidas").dialog({
			 width: 1050,
			 height: 550,
			 modal: true,
			 draggable: true
		});
	} else {
		alert('Período fechado.');
	}
}

function clickIncluirBatidas() {
	var idDia = $j(this).parent().parent().attr('id').split('-')[1];
	var date = new Date();
	var dateStr = date.getFullYear() 
	 	+ '' + date.getMonth() 
	 	+ '' + date.getDate() 
	 	+ '' + date.getHours() 
	 	+ '' + date.getMinutes()
	 	+ '' + date.getSeconds()
	 	+ '' + date.getMilliseconds();
	var tableInterna = $j(this).parent().next().find('table');
	var ultimoStatus = $j(this).parent().next().find('table > tbody > tr:last > td:first > input').val();
	var innerHtml = '<tr id="rowStatus-' + idDia + '">';
	   
	innerHtml += '	<td style="width:100px;">';
	innerHtml += '		<select name="statusIncluirBatida-' + idDia + '-' + dateStr + '" name="statusIncluirBatida-' + idDia + '-' + dateStr + '" style="width:100%;">';
	innerHtml += '			<option value="E" ' + (ultimoStatus == 'S' ? 'selected' : '') + '>ENTRADA</option>';
	innerHtml += '			<option value="S" ' + (ultimoStatus == 'E' ? 'selected' : '') + '>SAÍDA</option>';
	innerHtml += '		</select>';
	innerHtml += '	</td>';
   
	innerHtml += '	<td style="width:60px;">';
	innerHtml += '		<input type="text" name="horaIncluirBatida-' + idDia + '-' + dateStr + '" id="horaIncluirBatida-' + idDia + '-' + dateStr + '" style="width:80%">';
	innerHtml += '	</td>';
   
	innerHtml += '	<td style="width:70px;">';
	innerHtml += '		<input type="checkbox" name="incluirBatida-' + idDia + '-' + dateStr + '" id="excluirBatida-' + idDia + '-' + dateStr + '" class="checkIncluirBatidas" readonly="readonly" checked="checked"> Incluir';
	innerHtml += '	</td>';
   
	innerHtml += '	<td>';
	innerHtml += '		<div id="divMotivo-' + idDia + '">';
	innerHtml += '			<b>Motivo:</b> ';
	innerHtml += '			<input type="text" name="idMotivoIncluirBatida-' + idDia + '-' + dateStr + '" id="idMotivoIncluirBatida-' + idDia + '-' + dateStr + '" style="width:40px" onchange="buscaIdMotivoCorrigirBatidas(\'idMotivoIncluirBatida-' + idDia + '-' + dateStr + '\', \'descMotivoIncluirBatida-' + idDia + '-' + dateStr + '\');" required="true">';
	innerHtml += '			<i class="icon-search" style="cursor: pointer" id="filtroBusca" onclick="showModalMotivoCorrigirBatidas(\'idMotivoIncluirBatida-' + idDia + '-' + dateStr + '\', \'descMotivoIncluirBatida-' + idDia + '-' + dateStr + '\');"></i>&nbsp;';
	innerHtml += '			<input type="text" name="descMotivoIncluirBatida-' + idDia + '-' + dateStr + '" id="descMotivoIncluirBatida-' + idDia + '-' + dateStr + '" readonly="true" style="width:200px">';
	innerHtml += '		</div>';	
	innerHtml += '	</td>';
	
	tableInterna.append(innerHtml);
	
	$j('input[name|="horaIncluirBatida"]').setMask('time');
}

function buscaIdMotivoCorrigirBatidas(campoId, campoDescricao) {
	buscaId('cadastros.MotivoCartao', 'id', 'dscMotivo', campoId, campoDescricao, false);
}

function showModalMotivoCorrigirBatidas(campoId, campoDescricao) {
	showBuscaInterna(campoId, campoDescricao, 'cadastros.MotivoCartao', 'id', 'dscMotivo', false);
}

function clickExcluirBatidas() {
	var check = $j(this);
	var batidaId = $j(this).attr('id').split('-')[1];
	var divMotivo = '#divMotivo-'+batidaId;
	var inputMotivo = '#idMotivoExcluirBatida-'+batidaId; 
	
	if (check.is(':checked')) {
		$j(divMotivo).show();
		$j(inputMotivo).removeAttr('disabled');
	} else {
		$j(divMotivo).hide();
		$j(inputMotivo).attr('disabled', true);
	}
}

function clickAlterarStatusBatidas() {
	var identificadorDia = $j(this).parent().parent().attr('id').split('-')[1];
	
	confirmaAlteraStatus(
		function todosOsDias() {
			
			var ultimoStatus = null;
			var ultimaBatida = null;
			
			$j('tr[id|="rowDia"]').each(function() { 
				
				var linhaDia = $j(this).attr('id').split('-')[1];
				var dataLinha = transformaIdentificadorDia(linhaDia);
				var dataSelecao = transformaIdentificadorDia(identificadorDia);
				if (dataLinha >= dataSelecao) {
					
					var identificadorLinhas = 'tr[id="rowStatus-'+linhaDia+'"]';
					
					$j(identificadorLinhas).each(function () {
						
						var hidden = $j(this).find('input[name|="statusBatida"]');
						var spanDesc = $j(this).find('span[id|="descStatusBatida"]');
						
						var hora = $j(this).children('td[style="width:60px;"]').text().trim();
						var dia = $j(this).parent().parent().parent().prev().prev().prev().prev().text().trim();
						
						var data = transformaLinhaBatida(dia, hora);
						
						if (ultimoStatus == null) {
							ultimoStatus = hidden.val(); 
						}
						
						if (ultimaBatida != null) {
							var timeDiff =  data - ultimaBatida;
							var horasDiff = (timeDiff/3600000);
							if (horasDiff >= 48) {
								ultimoStatus = 'S';
							}
						}
						
						if (ultimoStatus == 'E') {
							hidden.val('S');
							ultimoStatus = 'S';
							spanDesc.text('SAÍDA');
						} else {
							hidden.val('E');
							ultimoStatus = 'E';
							spanDesc.text('ENTRADA');
						}
						
						ultimaBatida = data;
						
					});
					
				}
			}); 
			
		}, function diaAtual() {
			var ultimoStatus = null;
			var identificadorLinhas = 'tr[id="rowStatus-'+identificadorDia+'"]';
			
			$j(identificadorLinhas).each(function () {
				
				var hidden = $j(this).find('input[name|="statusBatida"]');
				var spanDesc = $j(this).find('span[id|="descStatusBatida"]');
				
				if (ultimoStatus == null) {
					ultimoStatus = hidden.val(); 
				}
				
				if (ultimoStatus == 'E') {
					hidden.val('S');
					ultimoStatus = 'S';
					spanDesc.text('SAÍDA');
				} else {
					hidden.val('E');
					ultimoStatus = 'E';
					spanDesc.text('ENTRADA');
				}
			});
		} 
	)
}

function confirmaAlteraStatus(yesFn, noFn) {
    var confirmBox = $j("#confirmBox");
    confirmBox.find(".yes,.no").unbind().click(function()
    {
    	confirmBox.dialog('close');
    });
    confirmBox.find(".yes").click(yesFn);
    confirmBox.find(".no").click(noFn);
    
    confirmBox.dialog({
		 width: 250,
		 height: 150,
		 modal: true,
		 draggable: true
	});
}

function transformaIdentificadorDia(identificadorDia) {
	var dataFormatada = identificadorDia.substring(4,8) + '/' + identificadorDia.substring(2,4) + '/' + identificadorDia.substring(0,2);
	return new Date(dataFormatada);
}

function transformaLinhaBatida(dia, hora) {
	//2013-03-01T01:10:00
	var dataFormatada = dia.substring(6,10) + '-' + dia.substring(3,5) + '-' + dia.substring(0,2) + 'T' + hora + ':00';
	return new Date(dataFormatada);
}

function colorirCorrigirBatidas(data) {
	var dtStr = data.toString();
	var dtIdentificador = dtStr.replace('/', '').replace('/', ''); 
	$j('tr[id|="rowDia"]').css({'background-color': '#EEEEEE'});
	$j('#rowDia-'+dtIdentificador ).css({'background-color': '#DDDDDD'});
}


// Observação funcionário
function showLovIncluirObservacao(data) {
	$j('#dataIncluirObservacao').val(data);
	$j("#observacaoFuncionario").val('');
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/espelhoPontoFuncionario/verificaObservacao",
		data: { dataObservacao: data },
		dataType:"json",
		success:function(json){
    		$j.each(json, function(index, item){
    			$j("#observacaoFuncionario").val(item.observacao);
        	});
        },
        error:function(xhr){
        	$j("#observacaoFuncionario").val('');
        }
    });
	
	$j("#divLovIncluirObservacaoFuncionario").dialog({
		 width: 700,
		 height: 450,
		 modal: true,
		 draggable: true
	});
}

function showLovVisualizarObservacao(img, data, obs) {
	var srcImage = img.src; 
	var pathImages = srcImage.substring(0, srcImage.lastIndexOf('/')) + '/';
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/espelhoPonto/atualizaLeituraObservacao",
		data: { dataObservacao: data, funcionarioId: $j('#funcionarioId').val() },
		dataType:"json",
		success:function(json){
			$j('#dataIncluirObservacao').val(data);
			$j("#observacaoFuncionario").val(obs);
			$j("#observacaoFuncionario").attr('readonly', 'readonly');
			$j('#btnIncluirObservacao').hide();
			$j("#divLovIncluirObservacaoFuncionario").attr('title', 'Visualizar observação');
			
			$j("#divLovIncluirObservacaoFuncionario").dialog({
				 width: 700,
				 height: 450,
				 modal: true,
				 draggable: true
			});
			img.src = pathImages + 'msg_read1.png'
        },
        error:function(xhr){
        	alert('Erro ao atualizar status da observação');
        }
    });
	
}

function processar() {
	$j('#formProcessar').submit();
}
