var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#cnpjFil").focus();
})

function buscaIdFilial() {
	var stringBusca = $j('#filialId').val();
	var empresaId = $j('#empresaId').val();

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/busca/buscaIdFilial",
		data : {
			codigo : stringBusca
		},
		dataType : "json",
		success : function(json) {
			$j('#setorId').val('');
			$j('#setorNome').val('');			
			
			$j('#filialId').empty();
			$j('#filialNome').empty();
			
			$j('#filialId').val(json.id);
			$j('#filialNome').val(json.nome);
			
			verificarHabilitacaoSetor();
		},
		error : function(xhr) {
			$j('#setorId').val('');
			$j('#setorNome').val('');			
			$j('#filialId').val('');
			$j('#filialNome').val('');
			
			verificarHabilitacaoSetor();
		}
	});
}

function buscarMultiLovFilial() {
	showBuscaInterna('filialId', 'filialNome', 'cadastros.Filial', 'id', 'dscFil', true);
}

function buscarMultiLovFilialSemFiltrarPorUsuario() {
	showBuscaInterna('filialId', 'filialNome', 'cadastros.Filial', 'id', 'dscFil', true, '', 'false');
} 

function buscarLovFilial() {
	showBuscaInterna('filialId', 'filialNome', 'cadastros.Filial', 'id', 'dscFil', false, 'verificarHabilitacaoSetor()');
}

function verificarHabilitacaoSetor() {
	habilitaSetor($j('#filialId').val().trim() != '');
}

function habilitaSetor(habilita) {
	$j('#setorId').attr('disabled', !habilita);
	$j('#setorBotaoBusca').attr('disabled', !habilita);

    $j('#setorId').val('');
    $j('#setorDescricao').val('');
}

function marcaTodosLov() {
	$j('input[name^="itemLov"]').attr("checked","true");
}

function desmarcaTodosLov() {
	$j('input[name^="itemLov"]').removeAttr("checked");
}
