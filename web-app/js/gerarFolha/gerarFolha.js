var $j = jQuery.noConflict();

$j(document).ready(function() {
	 
	controlaOpcaoFiltro();
	
	$j("#formFiliais").submit(submeteFormAssincrono);
	$j("#formFuncionarios").submit(submeteFormAssincrono);
	
	mudaAno();
	mudaPeriodo();
	
});

function controlaOpcaoFiltro() {
	var value = $j('input[name="opcaoFiltro"]:checked').val();
	if (value == 'filial') {
		$j('#divFiltroPadrao').hide();
	} else if (value == 'funcionario') {
		$j('#divFiltroPadrao').show();
	}
}

function submeteFormAssincrono(e) {
	var formObj = $j(this);
	var formURL = formObj.attr("action");
	var formData = new FormData(this);
	
	$j('#carregando').show();
	
	$j.ajax({
		url: formURL,
		type: 'POST',
		data:  formData,
		mimeType:"multipart/form-data",
		contentType: false,
		cache: false,
		processData:false,
		dataType : "json",
		success: function(json) {
			$j('#carregando').hide();
			if (json == null) {
				alert('Rotina invocada com sucesso. E-mail enviado.');
			} else {
				$j('#baixarArquivo').show();
				$j('#baixarArquivoId').val(json);
			}
			
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$j('#carregando').hide();
			
			if (jqXHR.status == 403) {
				// SC_FORBIDDEN
				alert('Exportação para esse tipo de folha não é válido');
			} else if (jqXHR.status == 502) {
				// SC_BAD_GATEWAY
				alert('Impossível enviar e-mail. Configurações de SMTP inválidas.');
			}
			
		},
		complete: function(jqXHR, textStatus) {
			$j('#carregando').hide();
		}
	});
	e.preventDefault(); //Prevent Default action.
	e.unbind();
}

function mudaAno() {
	$j('#periodo').empty();
	$j('#periodo').append('<option value="">..</option>');
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaPorAno",
		data: {ano: $j('#ano').val(), status: 'E' },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var option = '<option value="' + item.id + '">' + item.descricao + '</option>';
				$j('#periodo').append(option);
			});
		},
		error:function(xhr){
		}
	});	
	
}

function mudaPeriodo() {
	
	$j('#dataInicio').val('');
	$j('#dataFim').val('');
	
	var periodoId = $j('#periodo').val(); 
	if (periodoId == null || periodoId == '') {
		periodoId = $j('#cmpPeriodo').val();
		$j('#periodo').val(periodoId);
	}
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaDatas",
		data: {periodoId: periodoId },
		dataType:"json",
		success:function(json){
			$j('#dataInicio').val(json.dataInicio);
			$j('#dataFim').val(json.dataFim);
		},
		error:function(xhr){
		}
	});	
}

function marcarTodasFiliais() {
	$j('input[name^="filial"]').attr("checked",true);
}

function desmarcarTodasFiliais() {
	$j('input[name^="filial"]').attr("checked",false);
}

function marcarTodosFuncionarios() {
	$j('input[name^="funcionario"]').attr("checked",true);
}

function desmarcarTodosFuncionarios() {
	$j('input[name^="funcionario"]').attr("checked",false);
}

function hideBaixar() {
	$j('#baixarArquivo').hide();
}