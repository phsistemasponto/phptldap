var $j = jQuery.noConflict();

$j(document).ready(function() {
	$j('.valor').maskMoney({decimal:',', thousands:'.'});
	mudaPeriodo();
});

function mudaPeriodo() {
	var periodo = $j('#periodo').val();
	if (periodo != '') {
		var dtInicioId = '#dtInicialPeriodo' + periodo;
		var dtFimId = '#dtFinalPeriodo' + periodo;
		$j('#dtInicio').val($j(dtInicioId).val());
		$j('#dtFim').val($j(dtFimId).val());
	} else {
		$j('#dtInicio').val('');
		$j('#dtFim').val('');
	}
}