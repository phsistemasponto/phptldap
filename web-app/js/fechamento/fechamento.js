var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	mudaAno();
	mudaPeriodo();
	
	$j("#formFiliais").submit(function(e) {
		var formObj = $j(this);
		var formURL = formObj.attr("action");
		var formData = new FormData(this);
		
		$j('#carregando').show();
		
		$j.ajax({
			url: formURL,
			type: 'POST',
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			dataType : "json",
			success: function(json) {
				$j('#carregando').hide();
				mudaStatusFilial();
				alert('Rotina invocada com sucesso');
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$j('#carregando').hide();
				alert('Erro ao invocar rotina');
			},
			complete: function(jqXHR, textStatus) {
				$j('#carregando').hide();
			}
		});
		e.preventDefault(); //Prevent Default action.
		e.unbind();
	});
	
	$j("#formFuncionarios").submit(function(e) {
		var formObj = $j(this);
		var formURL = formObj.attr("action");
		var formData = new FormData(this);
		
		$j('#carregando').show();
		
		$j.ajax({
			url: formURL,
			type: 'POST',
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			dataType : "json",
			success: function(json) {
				$j('#carregando').hide();
				mudaStatusFuncionario();
				alert('Rotina invocada com sucesso');
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$j('#carregando').hide();
				alert('Erro ao invocar rotina');
			},
			complete: function(jqXHR, textStatus) {
				$j('#carregando').hide();
			}
		});
		e.preventDefault(); //Prevent Default action.
		e.unbind();
	});
	
	
	
});

function mudaAno() {
	$j('#periodo').empty();
	$j('#periodo').append('<option value="">..</option>');
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaPorAno",
		data: { ano: $j('#ano').val() },
		dataType:"json",
		success:function(json){
			$j.each(json, function(index, item){
				var option = '<option value="' + item.id + '">' + item.descricao + '</option>';
				$j('#periodo').append(option);
			});
		},
		error:function(xhr){
		}
	});	
	
}

function mudaPeriodo() {
	
	$j('#dataInicio').val('');
	$j('#dataFim').val('');
	
	var periodoId = $j('#periodo').val(); 
	if (periodoId == null || periodoId == '') {
		periodoId = $j('input[name=periodo]').val();
		$j('#periodo').val(periodoId);
	}
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/periodo/consultaDatas",
		data: {periodoId: periodoId },
		dataType:"json",
		success:function(json){
			$j('#dataInicio').val(json.dataInicio);
			$j('#dataFim').val(json.dataFim);
		},
		error:function(xhr){
		}
	});	
}

function controlaFiltroPadrao() {
	var value = $j('input[name="tipoFechamento"]:checked').val();
	if (value == 'filial') {
		$j('#divFiltroPadrao').hide();
	} else if (value == 'funcionario') {
		$j('#divFiltroPadrao').show();
	}
}

function mudaStatusFilial() {
	$j.each($j('input[name=filialSelecionada]'), function(index, item){
		var checked = item.checked;
		var value = item.value;
		if (checked) {
			var td = $j('#columnFilial-'+value);
			td.empty();
			if ($j("#formFiliais").attr("action") == (context + '/fechamento/concluirFilial')) {
				td.append('<span class="label label-important">Fechado</span>');
			} else {
				td.append('<span class="label label-success">Aberto</span>');
			}
		}
	});
}

function mudaStatusFuncionario() {
	$j.each($j('input[name=funcionarioSelecionado]'), function(index, item){
		var checked = item.checked;
		var value = item.value;
		if (checked) {
			var td = $j('#columnFuncionario-'+value);
			td.empty();
			if ($j("#formFuncionarios").attr("action") == (context + '/fechamento/concluirFuncionario')) {
				td.append('<span class="label label-important">Fechado</span>');
			} else {
				td.append('<span class="label label-success">Aberto</span>');
			}
		}
	});
}

function desfazerFechamentoFilial() {
	var url = context + '/fechamento/desfazerFechamentoFilial';
	$j("#formFiliais").attr("action", url);
	$j("#formFiliais").submit();
}

function desfazerFechamentoFuncionario() {
	var url = context + '/fechamento/desfazerFechamentoFuncionario';
	$j("#formFuncionarios").attr("action", url);
	$j("#formFuncionarios").submit();
}

function marcarTodosFiliais() {
	$j('input[name^="filialSelecionada"]').attr("checked","true");
}

function desmarcarTodosFiliais() {
	$j('input[name^="filialSelecionada"]').removeAttr("checked");
}