var $j = jQuery.noConflict();

$j(document).ready(function() {
	
});

function buscaOcorrenciaId() {
	buscaId('cadastros.Ocorrencias', 'id', 'dscOcor', 'ocorrenciaId', 'ocorrenciaDescricao', false, 0);
}

function lovOcorrencia() {
	showBuscaInterna('ocorrenciaId', 'ocorrenciaDescricao', 'cadastros.Ocorrencias', 'id', 'dscOcor', false, null);
}

function incluirDetalhe() {
	var totalizador = $j("#totalizador").val();
	var totalizadorDescricao = $j("#totalizador option:selected").text();
	var ocorrenciaId = $j("#ocorrenciaId").val();
	var ocorrenciaDescricao = $j("#ocorrenciaDescricao").val();
	var descricao = $j("#descricaoDetalhe").val();
	
	if (totalizador == '') {
		alert('Informe o totalizador');
		return;
	}
	
	if (ocorrenciaId == '' || ocorrenciaDescricao == '') {
		alert('Informe a ocorrência');
		return;
	}
	
	if (descricao == '') {
		alert('Informe a descrição');
		return;
	}
	
	var indice = 0;
	var ultimoTr = $j('#detalhesTable > tbody > tr:last');
	if (ultimoTr != null && ultimoTr.attr('id') != null) {
		indice = parseInt(ultimoTr.attr('id').split('-')[1]);
		indice++;
	}
	
	var stringAppend  = "<tr id='rowDetalhe-" + indice + "'>";
		stringAppend +=	"	<td>";
		stringAppend +=	"		<input name='idTotalizador' type='hidden' value='" + totalizador + "'/>";
		stringAppend +=	"		<input name='dscTotalizador' type='text' readonly='' value='" + totalizadorDescricao + "' style='width: 80%'/>";
		stringAppend +=	"	</td>";
		stringAppend +=	"	<td>";
		stringAppend +=	"		<input name='idOcorParametroDetalhe' type='hidden' value='" + ocorrenciaId + "'/>";
		stringAppend +=	"		<input name='dscOcorParametroDetalhe' type='text' readonly='' value='" + ocorrenciaDescricao + "' style='width: 80%'/>";
		stringAppend +=	"	</td>";
		stringAppend +=	"	<td>";
		stringAppend +=	"		<input name='descricaoParametroDetalhe' type='text' readonly='' value='" + descricao + "' style='width: 80%'/>";
		stringAppend +=	"	</td>";
		stringAppend +=	"	<td>";
		stringAppend +=	"		<i class='icon-remove' onclick='excluirDetalhe(" + indice + ");' style='cursor: pointer' title='Excluir registro'></i>";
		stringAppend +=	"	</td>";
		stringAppend +=	"</tr>";
		
	
	$j('#detalhesTable > tbody').append(stringAppend);
	
	$j("#totalizador").val('');
	$j("#ocorrenciaId").val('');
	$j("#ocorrenciaDescricao").val('');
	$j("#descricaoDetalhe").val('');
	
}

function excluirDetalhe(indice) {
	var identificador = '#rowDetalhe-' + indice;
	$j(identificador).remove();
}

function marcarTodos() {
	$j('input[name^="filial"]').attr("checked",true);
}

function desmarcarTodos() {
	$j('input[name^="filial"]').attr("checked",false);
}