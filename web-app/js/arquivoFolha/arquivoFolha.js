var $j = jQuery.noConflict();

$j(document).ready(function() {
	$j('#layoutInicio').setMask('integer');
	$j('#layoutfim').setMask('integer');
	$j('#layoutdirecao').setMask('integer');
});


function incluirLayout() {
	
	var campo = $j('#campo');
	var optionSelectedCampo = $j("#campo option:selected");
	var layoutLinha = $j('#layoutLinha');
	var optionSelectedLayoutLinha = $j("#layoutLinha option:selected");
	var layoutInicio = $j('#layoutInicio');
	var layoutfim = $j('#layoutfim');
	var layoutcampofixo = $j('#layoutcampofixo');
	var layoutpreenchimento = $j('#layoutpreenchimento');
	var layoutdirecao = $j('#layoutdirecao');
	
	if (campo.val() == null) {
		alert('Informe o campo');
		return;
	}
	
	if (layoutLinha.val() == null || layoutLinha.val() == '') {
		alert('Informe o layout linha');
		return;
	}
	
	if (layoutInicio.val() == null || layoutInicio.val() == '') {
		alert('Informe o layout início');
		return;
	}
	
	if (layoutfim.val() == null || layoutfim.val() == '') {
		alert('Informe o layout fim');
		return;
	}
	
	if (layoutcampofixo.val() == null || layoutcampofixo.val() == '') {
		alert('Informe o layout campo fixo');
		return;
	}
	
	if (layoutpreenchimento.val() == null || layoutpreenchimento.val() == '') {
		alert('Informe o layout preenchimento');
		return;
	}
	
	if (layoutdirecao.val() == null || layoutdirecao.val() == '') {
		alert('Informe o layout direção');
		return;
	}
	
	var indice = 0;
	if ($j('#layoutsTable tbody tr:last').size() > 0) {
		indice = parseInt($j('#layoutsTable tbody tr:last').attr('id').split('-')[1]) + 1;
	}
	
	var html  = '<tr id="rowTableLayout-' + indice + '">';
		html += '	<td>' + optionSelectedCampo.text() + '</td>';
		html += '	<td>' + optionSelectedLayoutLinha.text() + '</td>';
		html += '	<td>' + layoutInicio.val() + '</td>';
		html += '	<td>' + layoutfim.val() + '</td>';
		html += '	<td>' + layoutcampofixo.val() + '</td>';
		html += '	<td>' + layoutpreenchimento.val() + '</td>';
		html += '	<td>' + layoutdirecao.val() + '</td>';
		html += '	<td>';
		html += '		<input type="hidden" id="cmpArquivofolhacampo" name="cmpArquivofolhacampo" value="' + campo.val() + '"/>';
		html += '		<input type="hidden" id="cmpLayoutLinha" name="cmpLayoutLinha" value="' + layoutLinha.val() + '"/>';
		html += '		<input type="hidden" id="cmpLayoutInicio" name="cmpLayoutInicio" value="' + layoutInicio.val() + '"/>';
		html += '		<input type="hidden" id="cmpLayoutfim" name="cmpLayoutfim" value="' + layoutfim.val() + '"/>';
		html += '		<input type="hidden" id="cmpLayoutcampofixo" name="cmpLayoutcampofixo" value="' + layoutcampofixo.val() + '"/>';
		html += '		<input type="hidden" id="cmpLayoutpreenchimento" name="cmpLayoutpreenchimento" value="' + layoutpreenchimento.val() + '"/>';
		html += '		<input type="hidden" id="cmpLayoutdirecao" name="cmpLayoutdirecao" value="' + layoutdirecao.val() + '"/>';
		html += '		<i class="icon-remove" onclick="excluirLayout(' + indice + ');" style="cursor: pointer" title="Excluir registro"></i>';
		html += '	</td>';
		html += '</tr>';
		
	$j('#layoutsTable tbody').append(html);
		
	campo.val('');
	layoutLinha.val('');
	layoutInicio.val('');
	layoutfim.val('');
	layoutcampofixo.val('');
	layoutpreenchimento.val('');
	layoutdirecao.val('');
	
}

function excluirLayout(indice) {
	$j('#rowTableLayout-' + indice).remove();
}