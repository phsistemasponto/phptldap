var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	$j('#displayCalc').bind('keypress', function(e) {
		var code = e.keyCode || e.which;
		if ((code >= 48 && code <= 57) || code == 58) {
			return true;
		} else if (code == 42) {
			clickBtnCalcOper('*');
			return false;
		} else if (code == 43) {
			clickBtnCalcOper('+');
			return false;
		} else if (code == 45) {
			clickBtnCalcOper('-');
			return false;
		} else if (code == 13) {
			clickBtnCalcResult();
			return false;
		} else if (code == 8) {
			var display = $j('#displayCalc').val(); 
			if (display.length() > 0 && display.substr(display.length()-2) == '_\n') {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
		
		
	});
	 
});

function showCalculadora() {
	$j("#divCalculadora").dialog({
		 width: 246,
		 modal: true,
		 draggable: true,
		 title:"Calculadora"
	});
}

function clickBtnCalc(btnVal) {
	var val = $j('#displayCalc').val();
	$j('#displayCalc').val(val+btnVal);
}

function clickBtnCalcOper(btnVal) {
	var val = $j('#displayCalc').val();
	if (ehExpressaoCompleta(val)) {
		var result = calcular();
		$j('#displayCalc').val(val + ' =\n_____________________\n' + result + ' ' + btnVal + '\n');
	} else {
		$j('#displayCalc').val(val + ' ' + btnVal + '\n');
	}
}

function clickBtnCalcResult() {
	var val = $j('#displayCalc').val();
	var result = calcular();
	$j('#displayCalc').val(val + ' =\n_____________________\n' + result);
}

function limparCalculadora() {
	$j('#displayCalc').val('');
}

function calcular() {
	var valueDisplay = $j('#displayCalc').val();
	
	valueDisplay = extraiExpressao(valueDisplay); 
	
	var primeiroOperando = '';
	var segundoOperando = '';
	var operador = '';
	
	for (i=0; i < valueDisplay.length; i++) {
		if (ehOperador(valueDisplay[i])) {
			if (operador != '') {
				// calcula expressao parcial
				primeiroOperando = avaliaExpressao(primeiroOperando, operador, segundoOperando);
				operador = '';
				segundoOperando = '';
			}
			operador = valueDisplay[i];
			continue;
		}
		
		if (operador == '') {
			primeiroOperando += valueDisplay[i];
		} else {
			segundoOperando += valueDisplay[i];
		}
	}
	
	var result = avaliaExpressao(primeiroOperando, operador, segundoOperando);
	var transformaEmDecimal = $j('#resultDecimal').is(':checked');
	
	if (ehHora(result) && transformaEmDecimal) {
		result = transformaHoraEmDecimal(result);
	}
	
	return result;
}

function extraiExpressao(display) {
	return display.split("\n").join("").split(" ").join("");
}

function avaliaExpressao(primeiroOperando, operador, segundoOperando) {
	var expressaoTemHora = false;
	
	if (ehHora(primeiroOperando)) {
		primeiroOperando = transformaHorasToMinutos(primeiroOperando);
		expressaoTemHora = true;
	}
	
	if (ehHora(segundoOperando)) {
		segundoOperando = transformaHorasToMinutos(segundoOperando);
		expressaoTemHora = true;
	}
	
	var expressao = primeiroOperando + operador + segundoOperando;
	var resultado = eval(expressao);
	
	if (expressaoTemHora) {
		resultado = transformaMinutosToHoras(resultado);
	}
	
	return ''+resultado;
}

function ehOperador(caractere) {
	return caractere == '+' || caractere == '-' || caractere == '*' || caractere == '%';
}

function ehHora(operando) {
	return operando.indexOf(':') > 0;
}

function ehExpressaoCompleta(display) {
	var ultimaExpressao = display;
	if (display.lastIndexOf('_') >= 0) {
		ultimaExpressao = display.substr(display.lastIndexOf('_'));
	}
	return ultimaExpressao.indexOf('+') >= 0 
		|| ultimaExpressao.indexOf('-') >= 0
		|| ultimaExpressao.indexOf('*') >= 0
		|| ultimaExpressao.indexOf('%') >= 0;
}

function transformaHorasToMinutos(horas) {
	var retorno = 0;
	if (horas.length >= 4) {
		var horasCalc = parseInt(horas.substr(0, horas.indexOf(':')));
		var minutosCalc = parseInt(horas.substr(horas.indexOf(':')+1, horas.length));
		retorno = (horasCalc * 60) + minutosCalc;
	} else {
		alert('Formato de hora inválido');
	}
	return retorno;
}

function transformaMinutosToHoras(qtdMinutos) {
	var modMinutos = qtdMinutos >= 0 ? qtdMinutos : -(qtdMinutos);
	var hora;
	var minutos;
	if (modMinutos < 60){
		hora = '00';
		minutos = '' + qtdMinutos;
		if (minutos.length == 1) {
			minutos = '0' + minutos;
		}
	} else {
		var minutosDiferenca = modMinutos % 60;
		var horasInteiras = (modMinutos - minutosDiferenca) / 60;
		hora = '' + horasInteiras;
		minutos = '' + minutosDiferenca;
		
		if (hora.length == 1) {
			hora = '0' + hora;
		}
		
		if (minutos.length == 1) {
			minutos = '0' + minutos;
		}
	}
	
	var sinal = qtdMinutos >= 0 ? '' : '-'
	
	return sinal+hora+':'+minutos
}

function transformaHoraEmDecimal(horas) {
	var retorno = '';
	if (horas.length >= 4) {
		var horasCalc = parseInt(horas.substr(0, horas.indexOf(':')));
		var minutosCalc = parseInt(horas.substr(horas.indexOf(':')+1, horas.length));
		var decimalMinute = ((minutosCalc/60)*100);
		retorno = horasCalc + '.' + parseInt(decimalMinute);
	} else {
		alert('Formato de hora inválido');
	}
	return retorno;
}