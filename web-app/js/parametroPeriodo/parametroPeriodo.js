var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	$j.mask.masks.numero = {
		mask: '99'
	}
	
	$j("#diaInicio").setMask("numero");
	$j("#diaFim").setMask("numero");
	
});

function changeUltimoDiaMes() {
	if ($j("#diaFimUltimoMes").is(":checked")) {
		$j('#divDiaFim').hide();
		$j('#diaFim').val('');
	} else {
		$j('#divDiaFim').show();
		$j('#diaFim').val('');
	}
}

function marcarTodos() {
	$j('input[name^="filial"]').attr("checked",true);
}

function desmarcarTodos() {
	$j('input[name^="filial"]').attr("checked",false);
}

function criarPeriodo() {
	$j('form').attr("action",context+'/parametroPeriodo/criarPeriodo');
	$j('form').submit();
}