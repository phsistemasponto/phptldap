var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 if (!($j('#id').length)){
		 desmarcarTodos();
	 } 
	
	 /* Mascaras */
	 $j('#horaIntervaloParaOutraFormatada').focus();
	 $j('#horaIntervaloParaOutraFormatada').setMask('time');
	 $j('#horaParaIntervaloFolgaFormatada').setMask('time');
	 $j('#evRrt').setMask('integer');
	 $j('#nrDias').setMask('integer');
	 
	 $j('#dtInicioPeriodo').setMask('integer');
	 $j('#dtFimPeriodo').setMask('integer');
	 
	 $j('#evFaltaTotal').setMask('integer');
	 $j('#ev1Falta').setMask('integer');
	 $j('#ev2Falta').setMask('integer');
	 
	 $j('#evAtraso').setMask('integer');
	 $j('#ev1Atraso').setMask('integer');
	 $j('#ev2Atraso').setMask('integer');
	 
	 $j('#evSaida').setMask('integer');
	 $j('#ev1Saida').setMask('integer');
	 $j('#ev2Saida').setMask('integer');
	 
	 $j('#evDsr').setMask('integer');
	 $j('#evDsrd').setMask('integer');
	 
	 $j('#evDsr').setMask('integer');
	 $j('#horaInicioAdicionalNoturnoFormatada').setMask('time');
	 $j('#horaFimAdicionalNoturnoFormatada').setMask('time');
	 
	 /* Desabilitando eventos */
	 if ($j('#checkFaltaTotal').val() != 'S') {
		 $j('#evFaltaTotal').attr('disabled', true); 
	 }
	 if ($j('#check1Falta').val() != 'S') {
		 $j('#ev1Falta').attr('disabled', true); 
	 }
	 if ($j('#check2Falta').val() != 'S') {
		 $j('#ev2Falta').attr('disabled', true); 
	 }
	 
	 if ($j('#checkAtraso').val() != 'S') {
		 $j('#evAtraso').attr('disabled', true); 
	 }
	 if ($j('#check1Atraso').val() != 'S') {
		 $j('#ev1Atraso').attr('disabled', true); 
	 } 
	 if ($j('#check2Atraso').val() != 'S') {
		 $j('#ev2Atraso').attr('disabled', true); 
	 }
	 
	 if ($j('#checkSaida').val() != 'S') {
		 $j('#evSaida').attr('disabled', true); 
	 }
	 if ($j('#check1Saida').val() != 'S') {
		 $j('#ev1Saida').attr('disabled', true); 
	 } 
	 if ($j('#check2Saida').val() != 'S') {
		 $j('#ev2Saida').attr('disabled', true); 
	 } 
	 
});

function habilitaEvento(check, evento) {
	if ($j(check).is(':checked')) {
		$j(evento).attr('disabled', false);
		$j(evento).val('');
	} else {
		$j(evento).attr('disabled', true);
	}
}


/** Filiais */


function atualizaFilial(item) {
	var input = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=temFilialPeriodo]');
	if (input.val()=='false'){
		input.val('true');
	} else {
		input.val('false');
	}
}

function marcarTodos(){
	$j('input[name^="temFilial"]').attr("checked","true");
	$j('input[name^="temFilialPeriodo"]').val("true");
}

function desmarcarTodos(){
	$j('input[name^="temFilial"]').removeAttr("checked");
	$j('input[name^="temFilialPeriodo"]').val("false");
}

function atualizaFilialBotao(item) {
	var input = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=temFilialPeriodo]');
	var inputStatus = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=statusPeriodo]');
	var botao = $j('#botao'+item);
	if (input.val()=='false'){
		input.val('true');
		inputStatus.val('A');
		botao.text('Aberto');
		botao.attr('class','btn btn-warning btn-mini');
	} else {
		input.val('false');
		inputStatus.val('X');
		botao.text('Aberto');
		botao.attr('class','btn btn-disabled btn-mini');
	}
	

}

function fechaReabrePeriodo(item){
	var inputStatus = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=statusPeriodo]');
	var botao = $j('#botao'+item);

	if (inputStatus.val()=='A'){
		inputStatus.val('F');
		botao.text('Fechado');
		botao.attr('class','btn btn-success btn-mini');
	} else if (inputStatus.val()=='F'){
		inputStatus.val('A');
		botao.text('Aberto');
		botao.attr('class','btn btn-warning btn-mini');
	}
	
}