var $j = jQuery.noConflict();

$j(document).ready(function() {
	 if (!($j('#id').length)){
		 desmarcarTodos();
	 }
	 
	 $j("#dscPr").focus();
	 $j('#dtIniPr').setMask('date');
	 $j('#dtFimPr').setMask('date');
	 $j('#filtroData').setMask('date');
	 
	 $j("#dtIniPr").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j('#dtFimPr').datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j('#filtroData').datepicker();
	 
	 alteraFiltro();
	 
})


function atualizaFilial(item) {
	var input = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=temFilialPeriodo]');
	if (input.val()=='false'){
		input.val('true');
	} else {
		input.val('false');
	}
}

function marcarTodos(){
	$j('input[name^="temFilial"]').attr("checked","true");
	$j('input[name^="temFilialPeriodo"]').val("true");
}
function desmarcarTodos(){
	$j('input[name^="temFilial"]').removeAttr("checked");
	$j('input[name^="temFilialPeriodo"]').val("false");
	
}

function atualizaFilialBotao(item) {
	var input = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=temFilialPeriodo]');
	var inputStatus = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=statusPeriodo]');
	var botao = $j('#botao'+item);
	if (input.val()=='false'){
		input.val('true');
		inputStatus.val('A');
		botao.text('Aberto');
		botao.attr('class','btn btn-warning btn-mini');
	} else {
		input.val('false');
		inputStatus.val('X');
		botao.text('Aberto');
		botao.attr('class','btn btn-disabled btn-mini');
	}
	

}


function fechaReabrePeriodo(item){
	var inputStatus = $j('#rowTableFilial'+item).children('td').eq(0).children('[id^=statusPeriodo]');
	var botao = $j('#botao'+item);

	if (inputStatus.val()=='A'){
		inputStatus.val('F');
		botao.text('Fechado');
		botao.attr('class','btn btn-success btn-mini');
	} else if (inputStatus.val()=='F' || inputStatus.val()=='E') {
		inputStatus.val('A');
		botao.text('Aberto');
		botao.attr('class','btn btn-warning btn-mini');
	}
	
}

function alteraFiltro() {
	var filtro = $j('#tipoFiltro').val();
	
	$j('#filtro').hide();
	$j('#filtroData').hide();
	
	if (filtro == 1) {
		$j('#filtro').show();
	} else if (filtro == 2) {
		$j('#filtroData').show();
	}
	
}