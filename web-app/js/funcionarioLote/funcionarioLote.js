var $j = jQuery.noConflict();

$j(document).ready(function() {
	$j('div[id^="ajuste_"]').hide();

	$j('input[id*="Data"]').setMask("date");
	$j('input[id*="Data"]').datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});

	$j("#inputSalario").setMask("decimal");
	$j("#horarioIndice").setMask("numero");
	$j("#beneficioQuantidade").setMask("numero");
	
	$j('input[name=campoAlteracao]').click(function(){
    	
		$j('div[id^="ajuste_"]').hide();
		$j('#areasAjustes input').val('');
		
		nameArea = $j(this).val();

		divHabilitado = '#ajuste_' + nameArea;
		$j(divHabilitado).show();
		
		if (nameArea == 'filial' || nameArea == 'departamento') {
			divFilial = '#ajuste_campo_filial';
			$j(divFilial).show();
		}
	});
});

function alteraCheckHorario() {
	var checked = $j('#dtAdmComoDtIniHorario').is(':checked');
	if (checked) {
		$j('#divDtIniHorario').hide();
		$j('#horarioDataInicio').val('');
	} else {
		$j('#divDtIniHorario').show();
		$j('#horarioDataInicio').val('');
	}
}


function alteraCheckConfigExtra() {
	var checked = $j('#dtAdmComoDtIniConfigExtra').is(':checked');
	if (checked) {
		$j('#divDtIniConfigExtra').hide();
		$j('#configHoraExtraDataInicio').val('');
	} else {
		$j('#divDtIniConfigExtra').show();
		$j('#configHoraExtraDataInicio').val('');
	}
}

function marcarTodosFuncionarios() {
	$j('input[name^="funcionarioSelecionado"]').attr("checked","true");
}

function desmarcarTodosFuncionarios() {
	$j('input[name^="funcionarioSelecionado"]').removeAttr("checked");
}

function submeteAlteracao() {
	if (validaAlteracao()) {
		$j('#formAlteracao').submit();
	}
}

function validaAlteracao() {
	if ($j('#campoAlteracao:checked').val() == null || $j('#campoAlteracao:checked').val() == '') {
		alert('Selecione um tipo de alteração');
		return false;
	}
	
	var possuiErros = false;
	
	$j('input:visible').each(
		function() {
			var input = $j(this);
			if (input.attr('id') == 'inputDataRescisao' || input.attr('type') == 'checkbox') {
				return;
			}
			if (input.val() == null || input.val().trim() == '') {
				possuiErros = true;
			}
		}
	);
	
	if (possuiErros) {
		alert('Todos os campos são obrigatórios');
		return false;
	}
	
	return true;
}