var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	$j("#descricao").focus();
	$j('#classificacaoId').setMask('integer');
	$j('#tolerancia').setMask('time');
	$j('#limitePorDia').setMask('time');
	$j('#qtdadeHorasDescontar').setMask('time');
	$j('#limiteHorasDesconto').setMask('time');
	$j('#limiteQuantidadeEfetuadaDesconto').setMask('time');
	$j('#tipoHoraExtraId').setMask('integer');
	$j('#horaIni').setMask('time');
	$j('#horaFim').setMask('time');
	 
});

function incluirPercentual() {
	if ($j("#tipoHoraExtraId").val() == '' || $j("#tipoHoraExtraNome").val() == '') {
		alert('Informe o tipo de hora extra!');
	} else if ($j("#horaIni").val() == '') {
		alert('Informe a hora início!');
	} else if ($j("#horaFim").val() == '') {
		alert('Informe a hora fim!');
	} else {
		
		var indice = parseInt($j('#percentualTable > tbody > tr:last > td:first> input[name="sequencialTemp"]').val()) + 1;
		
		var stringAppend = "<tr id=\"rowTableTempPercentual" + indice + "\" class=\"even\">"
			+ "<td>" 
			+ "<input type=\"text\" id=\"nomeTipoHeTemp\" name=\"nomeTipoHeTemp\" value=\"" + $j('#tipoHoraExtraNome').val() + "\" readonly=\"true\" style=\"width:30%\" />"
			+ "<input type=\"hidden\" id=\"idTipoHeTemp\" name=\"idTipoHeTemp\" value=\"" + $j('#tipoHoraExtraId').val() +"\"/>"
			+ "<input type=\"hidden\" id=\"sequencialTemp\" name=\"sequencialTemp\" value=\"" + indice +"\"/>"
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"horaIniTemp\" name=\"horaIniTemp\" value=\"" + $j('#horaIni').val() + "\" readOnly=\"true\" style=\"width:30%\"/>" 
			+ "</td>"
			+ "<td>" 
			+ "<input type=\"text\" id=\"horaFimTemp\" name=\"horaFimTemp\" value=\"" + $j('#horaFim').val() + "\" readOnly=\"true\" style=\"width:30%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<i class=\"icon-remove\" onclick=\"excluirPercentual("+ indice + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>" 
			+ "</td>"
			
			+ "</tr>";
		
		$j('#percentualTable > tbody:last').append(stringAppend);
		
		$j('#incPercent').focus();
		
		resetCamposPercentual();
	}
}

function excluirPercentual(indice) {
	$j('#rowTableTempPercentual' + indice).remove();
}


function incluirItem() {
	if ($j("#classificacaoId").val() == '' || $j("#classificacaoNome").val() == '') {
		alert('Informe a classificação!');
	} else if ($j("#limitePorDia").val() == '') {
		alert('Informe o limite por dia!');
	} else if ($j("#diaEspecificoSemana").val() == '') {
		alert('Informe o dia específico semana!');
	} else if ($j("#tipoConfiguracao").val() == '') {
		alert('Informe o tipo de configuração!');
	} else {
		
		var indice = parseInt($j('tr[id^="rowTableItem"]:last > td:first> input[name="sequencialItem"]').val()) + 1;
		
		var stringAppend = "<tr id=\"rowTableItem" + indice + "\" class=\"even\" style=\"background-color: #00AFFF\">"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"classificacaoNomeItem\" name=\"classificacaoNomeItem\" value=\"" + $j('#classificacaoNome').val() + "\" readonly=\"true\" />"
			+ "<input type=\"hidden\" id=\"classificacaoIdItem\" name=\"classificacaoIdItem\" value=\"" + $j('#classificacaoId').val() +"\"/>"
			+ "<input type=\"hidden\" id=\"sequencialItem\" name=\"sequencialItem\" value=\"" + indice +"\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"toleranciaItem\" name=\"toleranciaItem\" value=\"" + $j('#tolerancia').val() + "\" readOnly=\"true\" />" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"limitePorDiaItem\" name=\"limitePorDiaItem\" value=\"" + $j('#limitePorDia').val() + "\" readOnly=\"true\" />" 
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"diaEspecificoSemanaItem\" name=\"diaEspecificoSemanaItem\" value=\"" + $j('#diaEspecificoSemana option:selected').text() + "\" readOnly=\"true\" />"
			+ "<input type=\"hidden\" id=\"diaEspecificoSemanaItemValue\" name=\"diaEspecificoSemanaItemValue\" value=\"" + $j('#diaEspecificoSemana').val() + "\"/>"
			+ "<input type=\"hidden\" id=\"tipoConfiguracaoItem\" name=\"tipoConfiguracaoItem\" value=\"" + $j('#tipoConfiguracao').val() +"\"/>"
			+ "<input type=\"hidden\" id=\"qtdadeHorasDescontarItem\" name=\"qtdadeHorasDescontarItem\" value=\"" + $j('#qtdadeHorasDescontar').val() +"\"/>"
			+ "<input type=\"hidden\" id=\"limiteHorasDescontoItem\" name=\"limiteHorasDescontoItem\" value=\"" + $j('#limiteHorasDesconto').val() +"\"/>"
			+ "<input type=\"hidden\" id=\"limiteQuantidadeEfetuadaDescontoItem\" name=\"limiteQuantidadeEfetuadaDescontoItem\" value=\"" + $j('#limiteQuantidadeEfetuadaDesconto').val() +"\"/>"
			+ "</td>"
			
			+ "<td><i class=\"icon-remove\" onclick=\"excluirItem("+ indice + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i></td>" 
			
			+ "</tr>"
			
			+ "<tr>"
			+ "<td colspan=\"5\" style=\"padding:20px\">"
			+ "<table>"
			+ "<thead>"
				+ "<tr>"
					+ "<th>Tipo Hora</th>"
					+ "<th>Hora Inicio</th>"
					+ "<th>Hora fim</th>"
				+ "</tr>"
			+ "</thead>"
			+ "<tbody>";
			
		$j('input[name="sequencialTemp"]').each(function(seq) {
			if (seq > 0) {
				
				var nomeTipoHE = $j('input[name="nomeTipoHeTemp"]').toArray()[seq-1].value;
				var idTipoHE = $j('input[name="idTipoHeTemp"]').toArray()[seq-1].value;
				var sequencial = $j('input[name="sequencialTemp"]').toArray()[seq-1].value;
				
				var horaIni = $j('input[name="horaIniTemp"]').toArray()[seq-1].value;
				var horaFim = $j('input[name="horaFimTemp"]').toArray()[seq-1].value;
				
				stringAppend = stringAppend
					+ "<tr>"
					
					+ "<td>"
					+ "<input type=\"text\" id=\"nomeTipoHe" + seq + "item" + indice +  "\" " 
										+ " name=\"nomeTipoHe" + seq + "item" + indice +  "\" " 
										+ " value=\"" + nomeTipoHE + "\" readonly=\"true\" />"
					+ "<input type=\"hidden\" id=\"idTipoHe" + seq + "item" + indice +  "\" " 
										+ " name=\"idTipoHe" + seq + "item" + indice +  "\" " 
										+ " value=\"" + idTipoHE + "\" readonly=\"true\" />"
					+ "<input type=\"hidden\" id=\"sequencial" + seq + "item" + indice +  "\" " 
										+ " name=\"sequencial" + seq + "item" + indice +  "\" " 
										+ " value=\"" + sequencial + "\" readonly=\"true\" />"
					+ "</td>"
					
					+ "<td>"
					+ "<input type=\"text\" id=\"horaIni" + seq + "item" + indice +  "\" " 
										+ " name=\"horaIni" + seq + "item" + indice +  "\" " 
										+ " value=\"" + horaIni + "\" readonly=\"true\" />"
					+ "</td>"
					
					+ "<td>"
					+ "<input type=\"text\" id=\"horaFim" + seq + "item" + indice +  "\" " 
										+ " name=\"horaFim" + seq + "item" + indice +  "\" " 
										+ " value=\"" + horaFim + "\" readonly=\"true\" />"
					+ "</td>"
					
					+ "</tr>";
			}
		});
			
		stringAppend = stringAppend + "</tbody></table>"
			+ "</td>"
			+ "</tr>";
	
		$j('#itemTable > tbody:last').append(stringAppend);
		
		resetCamposItem();
		
		$j('#incItem').focus();
		
	}
}

function excluirItem(indice) {
	$j('#rowTableItem' + indice).next().remove();
	$j('#rowTableItem' + indice).remove();
}

function resetCamposItem() {
	$j('#classificacaoId').val('');
	$j('#classificacaoNome').val('');
	$j('#tolerancia').val('');
	$j('#limitePorDia').val('');
	$j('#qtdadeHorasDescontar').val('');
	$j('#limiteHorasDesconto').val('');
	$j('#limiteQuantidadeEfetuadaDesconto').val('');
	$j('#tipoHoraExtraId').val('');
	$j('#tipoHoraExtraNome').val('');
	$j('#horaIni').val('');
	$j('#horaFim').val('');
	$j('#percentualTable > tbody > tr[class="even"]').remove();
}

function resetCamposPercentual() {
	$j('#tipoHoraExtraId').val('');
	$j('#tipoHoraExtraNome').val('');
	$j('#horaIni').val('');
	$j('#horaFim').val('');
}

function suprimirClassificacoesUsadas() {
	var classificacoes = '';
	var inputs = $j('input[name="classificacaoIdItem"]');
	if (inputs != null && inputs.length > 0) {
		for (var i = 0; i < inputs.length; i++) {
			classificacoes = classificacoes.concat(inputs[i].value);
			classificacoes = classificacoes.concat(',');
		}
	}
	if (classificacoes.length > 1) {
		return classificacoes.substr(0,classificacoes.length-1);
	} else {
		return '0'
	}
}

function suprimirTipoHorasUsadas() {
	var tiposHoras = '';
	var inputs = $j('input[name="idTipoHeTemp"]');
	if (inputs != null && inputs.length > 0) {
		for (var i = 0; i < inputs.length; i++) {
			tiposHoras = tiposHoras.concat(inputs[i].value);
			tiposHoras = tiposHoras.concat(',');
		}
	}
	if (tiposHoras.length > 1) {
		return tiposHoras.substr(0,tiposHoras.length-1);
	} else {
		return '0'
	}
	
}