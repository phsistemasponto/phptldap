var $j = jQuery.noConflict();

$j(document).ready(function() {

	$j("#tipotolerancia").focus();
	$j('#hrbatInput').setMask('time');
    
})

function incluirBatida(){
	if ($j("#hrbat").val() == '') {
		alert('Informe a hora da batida!');
	} else if ($j("#hrbatInput").val() == '00:00') {
		alert('Hora inválida!');	
	} else {
		var ultimo = $j('#sequencia').val();
		
		if (ultimo!='1'){
			var penultimo = parseInt(ultimo)-1
			$j('#rowTableBathorario' + penultimo).children('td').eq(3).children('i').remove();
		}
		
		var stringAppend = "<tr id=\"rowTableBathorario" + ultimo
				+ "\" class=\"even\" >"
				+ "<td><input type=\"text\" id=\"seqbathorario\" readonly=\"true\" value=\""
				+ $j('#sequencia').val() + "\" style=\"width:90%\" name=\"seqbathorario\" /></td>" 
				+ "<td><input type=\"text\" id=\"idbat\" readonly=\"true\" value=\""
				+ $j('#idbatInput').find('option').filter(':selected').text() + "\" style=\"width:90%\" name=\"idbat\"/></td>"				
				+ "<td><input type=\"text\" id=\"hrbat\" name=\"hrbat\" value=\""
				+ $j('#hrbatInput').val() + "\"  readOnly=\"true\" style=\"width:90%\"/></td>"				
				+ "<td><i class=\"icon-remove\" onclick=\"excluirBatida("+ ultimo + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i></td>+</tr>";
	
		$j('#bathorarioTable > tbody:last').append(stringAppend);
		
		ultimo = parseInt(ultimo) + 1;
		$j('#sequencia').val(ultimo);
		
		$j('#hrbatInput').val('');
		$j('#hrbatInput').focus();
		
		if ($j('#idbatInput').val() == 'E') {
			$j('#idbatInput').val('S');
		} else {
			$j('#idbatInput').val('E');
		}
	}	
}

function excluirBatida(elemento){
	var penultimo = parseInt(elemento)-1
	if (penultimo>0){
		$j('#rowTableBathorario' + penultimo).children('td').eq(3).append("<i class=\"icon-remove\" onclick=\"excluirBatida("+ penultimo + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>");
	}
	$j('#rowTableBathorario' + elemento).remove()
	$j('#sequencia').val(elemento)
}