var $j = jQuery.noConflict();

$j(document).ready(function() {
})

var txtTipoHora;
var txtDescricao;
var registrosSuprimir;

function buscaIdTipoHora(inputTipoHora, descricaoTipoHora, excludes) {
	
	txtTipoHora = inputTipoHora;
	txtDescricao = descricaoTipoHora;
	registrosSuprimir = excludes;
	
	var stringBusca = $j(inputTipoHora).val();

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/busca/buscaIdTipoHora",
		data : {
			codigo : stringBusca,
			suprimir: registrosSuprimir
		},
		dataType : "json",
		success : function(json) {
			
			$j(inputTipoHora).empty();
			$j(descricaoTipoHora).empty();
			
			$j(inputTipoHora).val(json.id);
			$j(descricaoTipoHora).val(json.nome);
			
		},
		error : function(xhr) {
			
			$j(inputTipoHora).val('');
			$j(descricaoTipoHora).val('');
			
		}
	});
}

function buscarLovTipoHora(inputTipoHora, descricaoTipoHora, excludes) {
	
	txtTipoHora = inputTipoHora;
	txtDescricao = descricaoTipoHora;
	registrosSuprimir = excludes;
	
	if ($j('#'+inputTipoHora).attr('disabled') != 'disabled') {
		showBuscaInterna(inputTipoHora, descricaoTipoHora, 'cadastros.Tipohoraextras', 'id', 'dscTpHorExt', false);
	}

}