var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	 $j("#dataInicio").focus();
	
	 $j("#dataInicio").setMask("date"); 
	 $j("#dataFim").setMask("date");
	 
	 $j("#dataInicio").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 $j("#dataFim").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	 
	 $j(".celulaDadosLancamentos").click(lancamentoHorario);
	 $j(".celulaDadosLancamentos").mouseenter(function() {
		$j(this).css({'cursor': 'pointer'});
	 });
	 
	 if ($j('#statusEscala').val() == 'F') {
		 $j('#btnAplicar').hide();
		 $j('#btnEncerrar').hide();
		 $j('#btnRelatorio').attr('style', '');
	 }
});

function buscaDataEscala() {
	
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/escala/buscaDataEscala",
		data: { dataInicio: $j('#dataInicio').val() },
		dataType:"json",
		success:function(json){
    		$j.each(json, function(index, item){
    			$j('#escalaDataId').val(item.escalaId);
        		$j('#dataFim').val(item.escalaDataFim);
            	$j('#descricao').val(item.escalaDescricao);
            	$j('#status').val(item.status);
        	});
        },
        error:function(xhr){
        	$j('#escalaDataId').val('');
        	$j('#dataFim').val('');
        	$j('#descricao').val('');
        	$j('#status').val(item.status);
        }
    });
	
}

function incluirItemHorario() {
	
	if ($j('#itemHorarioId').val() == null || $j('#itemHorarioId').val() == '' 
		|| $j('#itemHorarioDescricao').val() == null || $j('#itemHorarioDescricao').val() == '') {
		
		alert('Preencha o item horário');
		return;
	}
	
	if ($j('#letraLegenda').val() == null || $j('#letraLegenda').val() == '') {
		
		alert('Preencha a letra legenda');
		return;
	}
	
	var lastIndex = 0;
	if ($j('#itemHorarioTable tbody').children().length > 0) {
		lastIndex = $j('#itemHorarioTable tbody tr:last').attr('id').split('-')[1];
	}
	var newIndex = parseInt(lastIndex) + 1;
	
	var newRow  = '<tr id="rowEscalaHorario-' + newIndex + '">';
		newRow += '	<td>';
		newRow += '		<input type="hidden" name="escIdHorario" id="escIdHorario" value="' + $j('#itemHorarioId').val() + '"/>';
		newRow += '		<input type="text" name="escDescHorario" id="escDescHorario" value="' + $j('#itemHorarioDescricao').val() + '" readonly="readonly"/>';
		newRow += '	</td>';
		
		newRow += '	<td>';
		newRow += '		<input type="text" name="escLetra" id="escLetra" value="' + $j('#letraLegenda').val() + '" readonly="readonly"/>';
		newRow += '	</td>';
		
		newRow += '	<td>';
		newRow += '		<i class="icon-remove" onclick="excluirItemHorario(' + newIndex + ');" style="cursor: pointer" title="Excluir registro"></i>';
		newRow += '	</td>';
		
	
	$j('#itemHorarioTable tbody').append(newRow);
	
	$j('#itemHorarioId').val('');
	$j('#itemHorarioDescricao').val('');
	$j('#letraLegenda').val('');
}

function excluirItemHorario(indice) {
	$j('#rowEscalaHorario-' + indice).remove();
}

function avancaLancarEscalas() {
	
	if ($j('#dataInicio').val() == null || $j('#dataInicio').val() == '') {
		alert('Informe a data início');
		return;
	}
	
	if ($j('#dataFim').val() == null || $j('#dataFim').val() == '') {
		alert('Informe a data fim');
		return;
	}
	
	if ($j('#descricao').val() == null || $j('#descricao').val() == '') {
		alert('Informe a descrição');
		return;
	}
	
	if ($j('#itemHorarioTable tbody').children().length == 0) {
		alert('Informe um item horário');
		return;
	}
	
	$j('#formEscala').submit();
	
}

function lancamentoHorario() {
	var letraSelectHorario = $j("#perfilHorario option:selected").text().split('-')[0].trim();
	var marcaTodos = $j("#marcaTodos").is(':checked');
	if (marcaTodos) {
		var indice = parseInt($j(this).index()) + 1;
		$j(this).parent().parent().children().each( function(i, tr) {
			var td = $j(tr).find('td:nth-child('+ indice + ')');
			var content = $j(td).text().trim(); 
			if (content.indexOf(letraSelectHorario) > -1) {
				$j(td).find('span').text($j(td).find('span').text().replace(letraSelectHorario, '').trim());
				$j(td).find('input').val($j(td).find('input').val().replace(letraSelectHorario, '').trim());
			} else {
				$j(td).find('span').text($j(td).find('span').text().concat(' ' + letraSelectHorario).trim());
				$j(td).find('input').val($j(td).find('input').val().concat(' ' + letraSelectHorario).trim());
			}
		})
	} else {
		var content = $j(this).text().trim();
		if (content.indexOf(letraSelectHorario) > -1) {
			$j(this).find('span').text($j(this).find('span').text().replace(letraSelectHorario, '').trim());
			$j(this).find('input').val($j(this).find('input').val().replace(letraSelectHorario, '').trim());
		} else {
			$j(this).find('span').text($j(this).find('span').text().concat(' ' + letraSelectHorario).trim());
			$j(this).find('input').val($j(this).find('input').val().concat(' ' + letraSelectHorario).trim());
		}
		
	}
}

function encerrarPeriodo() {
	$j('#formEncerrarPeriodo').submit();
}

function enviarEmail() {
	$j.ajax({
		async: false,
		type: "POST",
		url:context+"/escala/enviarEmail",
		data: { dtInicio: $j('#dtInicio').val(), dtFim: $j('#dtFim').val() },
		dataType:"json",
		success:function(json){
    		alert('E-mail enviado com sucesso!');
        },
        error:function(xhr){
        	alert('Falha ao enviar e-mail!');
        }
    });
}

function generateReport() {
	
	$j('#carregando').show();
	
	$j('#formReportEscala').submit(function(e) {
		
		var formObj = $j(this);
		var formURL = formObj.attr("action");
		var formData = new FormData(this);
		
		$j.ajax({
			url: formURL,
			type: 'POST',
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			dataType : "json",
			success: function(json) {
				
				$j('#carregando').hide();
				
				var urlPrintReport = context + "/reportPrint/printReport";
				window.open(urlPrintReport, '_blank');
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$j('#carregando').hide();
				if (jqXHR.status == 400) {
					alert('Sem registros');
				} else {
					alert('Erro na geração do relatório: ' + errorThrown);
				}
			},
			complete: function(jqXHR, textStatus) {
				$j('#carregando').hide();
			}
		});
		e.preventDefault(); //Prevent Default action.
		e.unbind();
	});
	
	$j('#formReportEscala').submit();
}