var $j = jQuery.noConflict();

$j(document).ready(function() {
	$j('#hora1').setMask('time');
	$j('#hora2').setMask('time');
	$j('#hora3').setMask('time');
	$j('#hora4').setMask('time');
	
	habilitarHorario();	 
});


function incluirItemHorario(){
	if ($j('#classificacao').val()==''){
		
		alert('Informe uma classificação!');
		abort();
		
	}
	
	var tipoClassificacao
	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/classificacao/buscaTipoClassificacao",
		data : {
			id: $j('#classificacao').val()
		},
		dataType : "json",
		success : function(json) {
			tipoClassificacao = json.tipoClassificacao	
		},
		error : function(xhr) {
			tipoClassificacao = ''
		}
	});
	
	if (tipoClassificacao == 'TR') {
		
		if ($j("#hora1").val() == '') {
			alert('Informe a 1a entrada!');
			return;
		}
		
		if ($j("#hora2").val() == '') {
			alert('Informe a 1a saída!');
			return;
		}
		
	}
	
	var sequencia = $j('#itemHorarioTable tr').length;
	var dia = parseInt(sequencia) % 7;
	if (dia == 0) {
		dia = 7;
	}
	
	var semana=new Array(6);
	semana[0]='Seg';
	semana[1]='Ter';
	semana[2]='Qua';
	semana[3]='Qui';
	semana[4]='Sex';
	semana[5]='Sáb';
	semana[6]='Dom';
	
	var stringAppend =  "<tr id=\"rowTableItemHorario" + sequencia + "\" class=\"even\" >";
		stringAppend += "	<td> ";
		stringAppend += "		<input type=\"hidden\" name=\"sequencia\" value=\"" + sequencia + "\" /> ";
		stringAppend += "		<input type=\"hidden\" name=\"nrDiaSemana-" + sequencia + "\" value=\"" + dia + "\" /> ";
		stringAppend += "		<input name=\"diaSemana\" style=\"width:90%\" value=\"" + semana[dia-1] + "\" readOnly=\"true\"/> ";
		stringAppend += "	</td> ";
		stringAppend += "	<td> ";
		stringAppend += "		<input type=\"hidden\" name=\"idClassificacao-" + sequencia + "\" value=\"" + $j("#classificacao").val() + "\" /> ";
		stringAppend += "		<input name=\"dscClassificacao\" style=\"width:90%\" value=\"" + $j("#classificacao").find('option').filter(':selected').text() + "\" readOnly=\"true\" /> ";
		stringAppend += "	</td> ";
		stringAppend += "	<td> ";
		stringAppend += "		<input name=\"itemHora1-" + sequencia + "\" style=\"width:90%\" value=\"" + $j('#hora1').val() + "\" readOnly=\"true\" />";
		stringAppend += "	</td> ";
		stringAppend += "	<td> ";
		stringAppend += "		<input name=\"itemHora2-" + sequencia + "\" style=\"width:90%\" value=\"" + $j('#hora2').val() + "\" readOnly=\"true\" />";
		stringAppend += "	</td> ";
		stringAppend += "	<td> ";
		stringAppend += "		<input name=\"itemHora3-" + sequencia + "\" style=\"width:90%\" value=\"" + $j('#hora3').val() + "\" readOnly=\"true\" />";
		stringAppend += "	</td> ";
		stringAppend += "	<td> ";
		stringAppend += "		<input name=\"itemHora4-" + sequencia + "\" style=\"width:90%\" value=\"" + $j('#hora4').val() + "\" readOnly=\"true\" />";
		stringAppend += "	</td> ";
		stringAppend += "	<td> ";
		stringAppend += "		<input name=\"itemCarga-" + sequencia + "\" style=\"width:90%\" value=\"" + $j('#carga').val() + "\" readOnly=\"true\" />";
		stringAppend += "	</td> ";
		stringAppend += "	<td> ";
		stringAppend += "		<i class=\"icon-remove\" onclick=\"excluirItemHorario("+ sequencia + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>";
		stringAppend += "	</td> ";
		stringAppend += "</tr>";
	
	$j('#itemHorarioTable > tbody:last').append(stringAppend);
	
	if (sequencia > 1) {
		var penultimo = parseInt(sequencia)-1;
		$j('#rowTableItemHorario' + penultimo).children('td').eq(7).children('i').remove();
	}
	
	atualizaCampoCargaHorariaTotal();

}

function habilitarHorario() {
	
	$j('#hora1').val("");
	$j('#hora2').val("");
	$j('#hora3').val("");
	$j('#hora4').val("");
	$j('#carga').val("");
	
	var tipoClassificacao
	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/classificacao/buscaTipoClassificacao",
		data : {
			id: $j('#classificacao').val()
		},
		dataType : "json",
		success : function(json) {
			tipoClassificacao = json.tipoClassificacao	
		},
		error : function(xhr) {
			tipoClassificacao = ''
		}
	});
	
	if (tipoClassificacao!='TR'){
		$j('#hora1').attr("disabled", "true");
		$j('#hora2').attr("disabled", "true");
		$j('#hora3').attr("disabled", "true");
		$j('#hora4').attr("disabled", "true");
	} else {
		$j('#hora1').removeAttr("disabled");
		$j('#hora2').removeAttr("disabled");
		$j('#hora3').removeAttr("disabled");
		$j('#hora4').removeAttr("disabled");
	}
	
}

function atualizaCargaHoraria(){
	var hora1 = calculaMinutos($j('#hora1').val());
	var hora2 = calculaMinutos($j('#hora2').val());
	var hora3 = calculaMinutos($j('#hora3').val());
	var hora4 = calculaMinutos($j('#hora4').val());
	
	var cargaHoraria = calculaCargaHoraria(hora1, hora2, hora3, hora4);
	$j('#carga').val(formataMinutos(cargaHoraria));
}

function excluirItemHorario(elemento){
	var penultimo = parseInt(elemento)-1
	
	if (penultimo > 0) {
		$j('#rowTableItemHorario' + penultimo)
			.children('td')
			.eq(7)
			.append("<i class=\"icon-remove\" onclick=\"excluirItemHorario("+ penultimo + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>");
	}
	
	$j('#rowTableItemHorario' + elemento).remove();
	
	atualizaCampoCargaHorariaTotal();
}

function atualizaCampoCargaHorariaTotal() {
	var minutos = 0;
	$j('input[name^=itemCarga]').each(function() {
		var input = $j(this);
		var hora = input.val();
		var m = calculaMinutos(hora);
		minutos += m;
	});	
	$j('#cargaHorariaTotal').val(formataMinutos(minutos));
}

function calculaMinutos(horaString) {
	var minutosRetorno = 0;
	if (horaString.length == 5) {
		var hora = parseInt(horaString.substring(0, 2));
		var minutos = parseInt(horaString.substring(3, 5));
		var minutosRetorno = (hora * 60) + minutos;
	}
	return minutosRetorno;
}

function formataMinutos(minutos) {
	var horas = '';
	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/horarios/retornaCargaHorariaHoras",
		data : {
			qtdMinutos : minutos
		},
		dataType : "json",
		success : function(json) {
			horas = json.cargaHorariaHoras;
		},
		error : function(xhr) {
			horas = '';		
		}
	});
	return horas;
}

function calculaAdicionalNoturno(hora1, hora2, extrasMinima, status) {
	
	var adicional = 0
	
	if (hora2 < hora1) {
		hora2 = hora2 + 1440;
	}
	
	if (hora1 == 0 || hora2 == 0 || (hora1 >= 300 && hora2 <= 1320)) {
		adicional = 0;
	} else if ((hora1 < 300 && hora2 <= 300) || (hora1 >= 1320 && hora2 <= 1740)) {
		if (hora2 - hora1 <= extrasMinima) {
			adicional = 0;
		} else {
			adicional = hora2 - hora1;
		}
	} else if (hora1 < 300) {
		if (300 - hora1 < extrasMinima) {
			adicional = 0;
		} else {
			adicional = 300 - hora1;
		}
	} else if (hora1 <= 1320 && hora2 > 1320 && hora2 <= 1740) {
		if (hora2 - 1320 <= extrasMinima) {
			adicional = 0;
		} else {
			adicional = horas2 - 1320;
		}
	} else if (hora1 <= 1320) {
		adicional = 420;
	} else if (hora1 >= 1320) {
		if (1740 - hora1 <= extrasMinima) {
			adicional = 0
		} else {
			adicional = 1740 - hora1
		}
	}
	
	if (status == 'S') {
		adicional = adicional * (60 / 52.5);
	} 
	
	return adicional;
}


function calculaCargaHoraria(hora1, hora2, hora3, hora4) {
	
	var adicional = 0;
	var valor1 = 0;
	var valor2 = 0;
	var diferenca = 0;
	
	adicional = calculaAdicionalNoturno(hora1, hora2, 0, 'S');
	adicional = parseInt(adicional);
	
	if (adicional > 0) {
		diferenca = calculaAdicionalNoturno(hora1, hora2, 0, 'N');
		if (hora2 > hora1) {
			valor1 = hora2 - hora1;
		} else {
			valor1 = (hora2 + 1440) - hora1;
		}
		valor1 = valor1 - diferenca;
		valor1 = valor1 + adicional;
	} else {
		if (hora2 > hora1) {
			valor1 = hora2 - hora1;
		} else {
			valor1 = (hora2 + 1440) - hora1;
		}
		adicional = 0;
		diferenca = 0;
	}
	
	if (hora3 > 0 && hora4 > 0) {
		adicional = calculaAdicionalNoturno(hora3, hora4, 0, 'S');
		if (adicional > 0) {
			diferenca = calculaAdicionalNoturno(hora3, hora4, 0, 'N');
			if (hora4 > hora3) {
				valor2 = hora4 - hora3;
			} else {
				valor2 = (hora4 + 1440) - hora3;
			}
			valor2 = valor2 - diferenca;
			valor2 = valor2 + adicional;
		} else {
			if (hora4 > hora3) {
				valor2 = hora4 - hora3;
			} else {
				valor2 = (hora4 + 1440) - hora3;
			}
			adicional = 0;
			diferenca = 0;
		}
	}
	
	return valor1 + valor2;
}


function rejeitarHorario() {
	
	if (confirm('Tem certeza que deseja rejeitar o horário?')) {
		$j('form').attr('action', context + '/horarioSimulado/rejeitar');
		$j('form').submit();
	}
}

function gerarHorario() {
	$j("#divConfirmGerarHorario").dialog({
		 width: 300,
		 height: 300,
		 modal: true,
		 draggable: true
	});
}

function confirmaGerarHorario() {

	if ($j('#nomeHorario').val()==''){
		alert('Informe o nome do horário!');
		abort();
	}
	
	$j('#formConfirmarGerar').submit();

}