var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dscEmp").focus();
	 
	 /*
	 $j('#cargaInterJor').setMask('time');
	 $j('#cargaLimtMensal').setMask('time');
	 $j('#cargaLimtSemanal').setMask('time');
	 $j('#cargaLimtSemanalNot').setMask('time');
	 */
	 
	var file = document.getElementById('files'); 
	if (file != null) {
		file.addEventListener('change', handleFileSelect, false);
	}
	 
});

function handleFileSelect(evt) {
    var files = evt.target.files;

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }
      
      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          document.getElementById('divFoto').innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('mudouFoto').value = 'true';
          if (document.getElementById('video') != null) {
        	  document.getElementById('video').remove();  
          }
          
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
}