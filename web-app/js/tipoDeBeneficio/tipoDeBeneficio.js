var $j = jQuery.noConflict();

$j(document).ready(function() {
	 $j("#dscBeneficio").focus();
})

var txtBeneficio;
var txtDescricao;

function buscaIdTipoBeneficio(inputBeneficio, descricaoBeneficio) {
	
	txtBeneficio = inputBeneficio;
	txtDescricao = descricaoBeneficio;
	
	var stringBusca = $j(inputBeneficio).val();

	$j.ajax({
		async : false,
		type : "POST",
		url : context + "/busca/buscaIdTipoBeneficio",
		data : {
			codigo : stringBusca
		},
		dataType : "json",
		success : function(json) {
			
			$j(inputBeneficio).empty();
			$j(descricaoBeneficio).empty();
			
			$j(inputBeneficio).val(json.id);
			$j(descricaoBeneficio).val(json.nome);
			
		},
		error : function(xhr) {
			
			$j(inputBeneficio).val('');
			$j(descricaoBeneficio).val('');
			
		}
	});
}

function buscarLovTipoBeneficio(inputBeneficio, descricaoBeneficio) {
	
	txtBeneficio = inputBeneficio;
	txtDescricao = descricaoBeneficio;
	
	if ($j(inputBeneficio).attr('disabled') != 'disabled') {
		
		showBuscaInterna('tipoDeBeneficioId', 'tipoDeBeneficioNome', 'cadastros.TipoDeBeneficio', 'id', 'descricao', false);
		
	}

}