var $j = jQuery.noConflict();

$j(document).ready(function() {
	$j("#dtLancamento").setMask("date");
	$j("#dtLancamento").datepicker({showOn: "button", buttonText : '<i class="icon-calendar"></i>'});
	
	/*
	$j('#horas').setMask('time');
	*/
	
	$j("#dtLancamento").focus();
});

function buscaIdFuncionario() {
	buscaId('cadastros.Funcionario', 'id', 'nomFunc', 'funcionarioId', 'funcionarioDescricao', false, null);
}

function buscarLovFuncionario() {
	showBuscaInterna('funcionarioId', 'funcionarioDescricao', 'cadastros.Funcionario', 'id', 'nomFunc', false);
}

function buscaIdOcorrencia() {
	buscaId('cadastros.Ocorrencias', 'id', 'dscOcor', 'ocorrenciaId', 'ocorrenciaDescricao', false, null);
}

function buscarLovOcorrencia() {
	showBuscaInterna('ocorrenciaId', 'ocorrenciaDescricao', 'cadastros.Ocorrencias', 'id', 'dscOcor', false);
}