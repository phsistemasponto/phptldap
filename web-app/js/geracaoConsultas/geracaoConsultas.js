var $j = jQuery.noConflict();

$j(document).ready(function() {
	
	$j('#inputSql').val('');
	 
});

function executarSQL() {
	
	if ($j('#inputSql').val() ==  null || $j('#inputSql').val() == '') {
		alert('Informe o SQL');
		return;
	}
	
	$j('#carregando').show();
	$j('#resultadoSql thead').empty();
	$j('#resultadoSql tbody').empty();
	
	$j.ajax({
		type: 'POST',
		url: context + "/geracaoConsultas/executeSql",	
		data: { inputSql: $j('#inputSql').val() },
		dataType : "json",
		success: function(json) {
			$j('#carregando').hide();
			
			if (json.messageErro != null) {
				
				alert('Erro: ' + json.messageErro);
				
			} else if (json.qtRegistros != null) {
				
				alert('Linhas modificadas: ' + json.qtRegistros);
				$j("#selectHistorico").prepend("<option>" + $j('#inputSql').val() + "</option>");
				
			} else {
				
				$j('#resultadoSql thead').append('<tr>');
				$j.each(json.columNames, function(index, item){
					$j('#resultadoSql thead tr').append('<th>' + item + '</th>');
			    });
				$j('#resultadoSql thead').append('</tr>');
				
				$j.each(json.result, function(index, item){
					var tr = $j('#resultadoSql tbody').append('<tr>');
					$j.each(json.columNames, function(indexColumn, itemColumn) {
						tr.append('<td>' + item[itemColumn] + '</td>');
				    });
					$j('#resultadoSql tbody').append('</tr>');
			    });
				
				$j("#selectHistorico").prepend("<option ondblclick='copiaSqlHistorico()'>" + $j('#inputSql').val() + "</option>");
				
				$j("#liSQL").attr('class', '');
				$j("#liHistorico").attr('class', '');
				$j("#liResultado").attr('class', 'active');
				
				$j("#tabSQL").attr('class', 'tab-pane');
				$j("#tabHistorico").attr('class', 'tab-pane');
				$j("#tabResultado").attr('class', 'tab-pane active');
				
			}
			
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$j('#carregando').hide();
			alert('Erro ao executar consulta: ' + errorThrown);
		},
		complete: function(jqXHR, textStatus) {
			$j('#carregando').hide();
		}
	});
	
}

function exportarExcel() {
	
	if ($j('#inputSql').val() ==  null || $j('#inputSql').val() == '') {
		alert('Informe o SQL');
		return;
	}
	
	$j('#carregando').show();
	$j('#resultadoSql thead').empty();
	$j('#resultadoSql tbody').empty();
	
	$j.ajax({
		type: 'POST',
		url: context + "/geracaoConsultas/exportExcel",	
		data: { inputSql: $j('#inputSql').val() },
		dataType : "json",
		success: function(json) {
			$j('#carregando').hide();
			if (json.messageErro != null) {
				alert('Erro: ' + json.messageErro);
			} else {
				var urlPrintReport = context + "/reportPrint/printReport";
				window.open(urlPrintReport, '_blank');
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$j('#carregando').hide();
			alert('Erro ao executar exportacao: ' + errorThrown);
		},
		complete: function(jqXHR, textStatus) {
			$j('#carregando').hide();
		}
	});
	
}

function copiaSqlHistorico() {
	var consulta = $j("#selectHistorico option:selected").text();
	
	$j('#inputSql').val(consulta);
	
	$j("#liSQL").attr('class', 'active');
	$j("#liHistorico").attr('class', '');
	$j("#liResultado").attr('class', '');
	
	$j("#tabSQL").attr('class', 'tab-pane active');
	$j("#tabHistorico").attr('class', 'tab-pane');
	$j("#tabResultado").attr('class', 'tab-pane');
}