var $j = jQuery.noConflict();

$j(document).ready(function() {

	$j("#filial").focus();

	$j('#inicioParam').setMask('integer');
	$j('#fimParam').setMask('integer');
	
	document.getElementById('files').addEventListener('change', handleFileSelect, false);
	
	var conteudoArquivo = $j('#conteudoArquivo').val();
	document.getElementById('list').innerHTML = conteudoArquivo;
	
});

function incluirParametro() {
	if ($j("#inicioParam").val() == '') {
		alert('Informe a posição inicial!');
	} else if ($j("#fimParam").val() == '') {
		alert('Informe a posição final!');
	} else {
		
		var indiceId = $j('#indice').val();
		var repetido = false;
		$j('input[name=idIndice]').each( function(i, input) { 
			if (indiceId === input.value) {
				repetido = true;
			}
		});
		
		if (repetido) {
			alert('Índice já foi incluído');
			return;
		}
		
		var indice = parseInt($j('#parametroTable > tbody > tr:last > td:first> input[name="sequencialTemp"]').val()) + 1;
		
		var stringAppend = "<tr id=\"rowTableParametro" + indice + "\" class=\"even\">"
			+ "<td>" 
			+ "<input type=\"text\" id=\"nomeIndice\" name=\"nomeIndice\" value=\"" + $j('#indice option:selected').text() + "\" readonly=\"true\" style=\"width:30%\" />"
			+ "<input type=\"hidden\" id=\"idIndice\" name=\"idIndice\" value=\"" + indiceId +"\"/>"
			+ "<input type=\"hidden\" id=\"sequencialTemp\" name=\"sequencialTemp\" value=\"" + indice +"\"/>"
			+ "</td>"
			
			+ "<td>" 
			+ "<input type=\"text\" id=\"posIni\" name=\"posIni\" value=\"" + $j('#inicioParam').val() + "\" readOnly=\"true\" style=\"width:30%\"/>" 
			+ "</td>"
			+ "<td>" 
			+ "<input type=\"text\" id=\"posFim\" name=\"posFim\" value=\"" + $j('#fimParam').val() + "\" readOnly=\"true\" style=\"width:30%\"/>" 
			+ "</td>"
			
			+ "<td>" 
			+ "<i class=\"icon-remove\" onclick=\"excluirParametro("+ indice + ");\" style=\"cursor:pointer\" title=\"Excluir registro\"></i>" 
			+ "</td>"
			
			+ "</tr>";
		
		$j('#parametroTable > tbody:last').append(stringAppend);
		
		resetaCampos();
		
		$j('#incParametro').focus();
		
	}
	
}

function excluirParametro(indice) {
	$j('#rowTableParametro' + indice).remove();
}

function resetaCampos() {
	$j('#indice').val('');
	$j('#inicioParam').val('');
	$j('#fimParam').val('');
}


function handleFileSelect(evt) {
    var files = evt.target.files;

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('text.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
        	// Render thumbnail
	    	var conteudo = e.target.result.split('\n');
	        var str = '';
	        for (i = 0; i < 1; i++) {
	      	  if (i < conteudo.length) 
	      		  str = str + conteudo[i].replace('\r', '');
	        }
	        document.getElementById('list').innerHTML = str;
        	$j('#conteudoArquivo').val(str);
        	
        	var strTodo = '';
	        for (i = 0; i < 3; i++) {
	      	  if (i < conteudo.length) 
	      		strTodo = strTodo + conteudo[i].replace('\r', '<br>');
	        }
	        document.getElementById('listTodo').innerHTML = strTodo;
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsText(f);
    }
}

function configuraPosicoes() {
	var t = getSelectionCharOffsetsWithin(document.getElementById('list'));
	$j('#inicioParam').val(t.start);
	$j('#fimParam').val(t.end);
}

function getSelectionCharOffsetsWithin(element) {
    var start = 0, end = 0;
    var sel, range, priorRange;
    if (typeof window.getSelection != "undefined") {
        range = window.getSelection().getRangeAt(0);
        start = range.startOffset;
        end = range.endOffset;
    } else if (typeof document.selection != "undefined" &&
            (sel = document.selection).type != "Control") {
        range = sel.createRange();
        priorRange = document.body.createTextRange();
        priorRange.moveToElementText(element);
        priorRange.setEndPoint("EndToStart", range);
        start = priorRange.text.length;
        end = start + range.text.length;
    }
    
    return {
        start: start,
        end: end
    };
    
}