var $j = jQuery.noConflict();

$j(document).ready(function() {
	$j('#pis').val('');
	$j('#pis').focus();
});

function baterPonto() {
	if (($j('#cpf').val() == null || $j('#cpf').val().trim() == '')
			&& ($j('#pis').val() == null || $j('#pis').val().trim() == '')) {
		alert('Informe o cpf ou pis');
		return false;
	}
	
	$j('#formBatidaPonto').submit();
}