<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="relatorioFuncionariosPresentes" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="6371dec9-38ea-4a56-9dce-a8ad44420f65">
	<property name="ireport.zoom" value="1.3636363636363635"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DT_INICIO" class="java.lang.String"/>
	<parameter name="DT_FIM" class="java.lang.String"/>
	<parameter name="LOGOMARCA" class="java.io.InputStream"/>
	<queryString>
		<![CDATA[SELECT fil.id AS seq_filial
	,f.id AS seq_func
	,f.crc_func AS cracha_func
	,fil.dsc_fil AS dsc_filial
	,s.dsc_setor AS dsc_setor
	,c.dsc_cargo AS dsc_cargo
	,f.nom_func AS nome_funcionario
	,d.nr_qtde_ch_prevista AS qtd_prevista
	,d.nr_qtde_ch_efetivada AS qtd_efetivada
	,bat.hrbat AS hora_batida
	,to_char(d.dt_bat, 'dd/MM/yyyy') AS data_batida
FROM dados d
INNER JOIN funcionario f ON d.seq_func = f.id
INNER JOIN filial fil ON f.filial_id = fil.id
INNER JOIN cargo c ON f.cargo_id = c.id
INNER JOIN setor s ON f.setor_id = s.id
LEFT JOIN (
	SELECT b.seq_func AS func
		,b.dt_bat AS dtbat
		,min(b.hr_bat) AS hrbat
	FROM batidas b
	WHERE b.id_bat = 'E'
	GROUP BY b.seq_func
		,b.dt_bat
	) bat ON bat.func = f.id
	AND bat.dtbat = d.dt_bat
WHERE bat.hrbat > 0
	AND d.id_tp_dia <> 'TR'
ORDER BY fil.dsc_fil
	,s.dsc_setor
	,f.id
	,d.dt_bat]]>
	</queryString>
	<field name="seq_filial" class="java.lang.Long"/>
	<field name="seq_func" class="java.lang.Long"/>
	<field name="cracha_func" class="java.lang.Long"/>
	<field name="dsc_filial" class="java.lang.String"/>
	<field name="dsc_setor" class="java.lang.String"/>
	<field name="dsc_cargo" class="java.lang.String"/>
	<field name="nome_funcionario" class="java.lang.String"/>
	<field name="qtd_prevista" class="java.lang.Integer"/>
	<field name="qtd_efetivada" class="java.lang.Integer"/>
	<field name="hora_batida" class="java.lang.Integer"/>
	<field name="data_batida" class="java.lang.String"/>
	<variable name="FUNCIONARIOS_COUNT" class="java.lang.Integer" resetType="Group" resetGroup="Setor" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{seq_func}]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<group name="Setor">
		<groupExpression><![CDATA[$F{dsc_setor}]]></groupExpression>
		<groupHeader>
			<band height="27">
				<rectangle>
					<reportElement x="0" y="6" width="276" height="20" backcolor="#00CC33" uuid="a1b8f664-8ccb-4c82-8ab8-0aa89a124521"/>
				</rectangle>
				<textField>
					<reportElement x="0" y="7" width="276" height="20" forecolor="#FFFFFF" uuid="9f0b4319-fd1a-4746-a46d-6607fba26a7e"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph leftIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{dsc_setor}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField>
					<reportElement x="0" y="0" width="276" height="20" uuid="f1477e8b-5a1c-48c7-8a23-2107c6b2aef6"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph leftIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA["Total de funcionários: " + $V{FUNCIONARIOS_COUNT}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="90" splitType="Stretch">
			<rectangle>
				<reportElement x="0" y="0" width="555" height="70" uuid="d70faf55-b49f-43e8-b394-7162d63cb716"/>
			</rectangle>
			<line>
				<reportElement x="0" y="50" width="555" height="1" uuid="76097016-9f70-4ac1-b53e-03562d6896f8"/>
			</line>
			<staticText>
				<reportElement x="151" y="50" width="254" height="20" uuid="3472dec4-024f-4f97-a164-5a46f3c6bd01"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[Relatório de funcionários presentes]]></text>
			</staticText>
			<line>
				<reportElement x="150" y="0" width="1" height="50" uuid="c2801b51-fcc8-4659-9b80-3bf069af0f73"/>
			</line>
			<textField>
				<reportElement x="486" y="2" width="30" height="16" uuid="becea8ef-054d-4020-94fd-ca835e68e688"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER} + " de "]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="376" y="3" width="100" height="16" uuid="71f8dd35-4383-41a8-9452-67f3546e877b"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Página..... :]]></text>
			</staticText>
			<textField evaluationTime="Report">
				<reportElement x="486" y="17" width="60" height="16" uuid="d021043c-8227-49c3-8b48-72cd562a2e85"/>
				<textElement>
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd/MM/yyyy").format(new Date())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="376" y="18" width="100" height="16" uuid="c22fe158-f022-4602-a534-f5d62594bf30"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Data..... :]]></text>
			</staticText>
			<staticText>
				<reportElement x="376" y="34" width="100" height="16" uuid="64dca8bf-5795-442d-a911-1188460c73ac"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Hora..... :]]></text>
			</staticText>
			<textField evaluationTime="Report">
				<reportElement x="486" y="34" width="60" height="16" uuid="de2e5322-df0e-4df1-b7e6-a5faec3773a9"/>
				<textElement>
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("HH:mm").format(new Date())]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="518" y="2" width="28" height="16" uuid="0b8ec734-5117-471c-8e8e-c72d599a64fe"/>
				<textElement>
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" x="10" y="50" width="141" height="20" uuid="bca34ee6-a13d-426e-b351-1290e4bd899c"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA["Período de " + $P{DT_INICIO} + " á " + $P{DT_FIM}]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Center" isUsingCache="true" onErrorType="Blank">
				<reportElement positionType="Float" x="0" y="2" width="150" height="45" uuid="40a3783c-2f51-434e-9ed4-b366b2d11ccc"/>
				<imageExpression><![CDATA[$P{LOGOMARCA}]]></imageExpression>
			</image>
			<rectangle>
				<reportElement x="0" y="70" width="555" height="20" uuid="09fa48f6-c383-4616-b84c-20a575cbb5bf"/>
			</rectangle>
			<staticText>
				<reportElement x="0" y="70" width="44" height="20" uuid="2319154b-accf-43af-b4b3-957f0676588d"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[Seq]]></text>
			</staticText>
			<staticText>
				<reportElement x="44" y="70" width="232" height="20" uuid="c9c9ea5c-c942-47e9-8dcd-c742a6fe795f"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[Nome]]></text>
			</staticText>
			<staticText>
				<reportElement x="276" y="70" width="149" height="20" uuid="b2885035-12ab-4ed8-a41e-ca25cb35f80e"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[Cargo]]></text>
			</staticText>
			<staticText>
				<reportElement x="425" y="70" width="65" height="20" uuid="8e1a71f6-4581-48b2-b0b6-c0368144da6d"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Horário]]></text>
			</staticText>
			<staticText>
				<reportElement x="490" y="70" width="65" height="20" uuid="d33e1d66-f890-43e3-8c21-6c4da3865117"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Data]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="44" height="20" uuid="eed82ea1-9c94-4cc4-b05a-e46272c3f55b"/>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{seq_func}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="44" y="0" width="232" height="20" uuid="95717998-9ea7-45ad-9cab-499f52422cf9"/>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nome_funcionario}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="276" y="0" width="149" height="20" uuid="2c515765-681a-490f-9831-fa51c83fbea0"/>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{dsc_cargo}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="425" y="0" width="65" height="20" uuid="d4620ef9-7821-4ce9-9e5d-8274e4c1788d"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[funcoes.Horarios.retornaHora($F{hora_batida})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="490" y="0" width="65" height="20" uuid="f3535178-2f48-4a5c-be71-4b4412f49ad4"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{data_batida}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
